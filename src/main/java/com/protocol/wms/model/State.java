/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="state")
public class State {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stateId;
	
	private String stateName;
	
	private Integer status;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@OneToMany(targetEntity=CompanyDepo.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private List<CompanyDepo> companyDepoList;
	
	@OneToMany(targetEntity=TransporterDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private List<TransporterDetails> transporterDetailsList;
	
	@OneToMany(targetEntity=Stockist.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private List<Stockist> stockistList;

	@OneToMany(targetEntity=InwardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private List<InwardCourierRegistration> inwardCourierRegistrationList;
	
	
	@OneToMany(targetEntity=OutWardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private List<OutWardCourierRegistration> outWardCourierRegistrations;
	
	@OneToMany(targetEntity=Company.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stateId",referencedColumnName="stateId")
	private List<Company> companyList;
	
	/**
	 * @return the stateId
	 */
	public Integer getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the companyDepoList
	 */
	public List<CompanyDepo> getCompanyDepoList() {
		return companyDepoList;
	}

	/**
	 * @param companyDepoList the companyDepoList to set
	 */
	public void setCompanyDepoList(List<CompanyDepo> companyDepoList) {
		this.companyDepoList = companyDepoList;
	}

	/**
	 * @return the transporterDetailsList
	 */
	public List<TransporterDetails> getTransporterDetailsList() {
		return transporterDetailsList;
	}

	/**
	 * @param transporterDetailsList the transporterDetailsList to set
	 */
	public void setTransporterDetailsList(
			List<TransporterDetails> transporterDetailsList) {
		this.transporterDetailsList = transporterDetailsList;
	}

	/**
	 * @return the stockistList
	 */
	public List<Stockist> getStockistList() {
		return stockistList;
	}

	/**
	 * @param stockistList the stockistList to set
	 */
	public void setStockistList(List<Stockist> stockistList) {
		this.stockistList = stockistList;
	}

	public List<InwardCourierRegistration> getInwardCourierRegistrationList() {
		return inwardCourierRegistrationList;
	}

	public void setInwardCourierRegistrationList(
			List<InwardCourierRegistration> inwardCourierRegistrationList) {
		this.inwardCourierRegistrationList = inwardCourierRegistrationList;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}
}
