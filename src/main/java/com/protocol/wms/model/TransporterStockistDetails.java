package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

public class TransporterStockistDetails 
{
private Integer stockistId;
private String stockistName;

private String stockistAddress;

private Long stockistMobileNo;

private String stockistEmailId;

private String stockistFaxNo;

private String stockistContactPerson;

private String stockistPartner ;

private String stockistLocation;

private String stockistCST;

private String stockistVAT;

private String stockistPanNo;

private Integer status;

private String city;

private String company;

private Date submitDate;

private Double ratePerCase;

private Double ratePerLR;

private Double claimRates;

private Double claimRatePerLR;

private Integer transporterDetailsId;

private Integer transporterStationDetailsId;
 
private Integer stockistApprovalStatus; 

public Integer getStockistApprovalStatus() {
	return stockistApprovalStatus;
}

public void setStockistApprovalStatus(Integer stockistApprovalStatus) {
	this.stockistApprovalStatus = stockistApprovalStatus;
}

public Integer getTransporterDetailsId() {
	return transporterDetailsId;
}

/**
 * @param status the status to set
 */
public void setTransporterDetailsId(Integer transporterDetailsId) {
	this.transporterDetailsId = transporterDetailsId;
}

public Integer getTransporterStationDetailsId() {
	return transporterStationDetailsId;
}

/**
 * @param status the status to set
 */
public void setTransporterStationDetailsId(Integer transporterStationDetailsId) {
	this.transporterStationDetailsId = transporterStationDetailsId;
}



public Double getRatePerCase() {
	return ratePerCase;
}

/**
 * @param stockistName the stockistName to set
 */
public void setRatePerCase(Double ratePerCase) {
	this.ratePerCase = ratePerCase;
}

public Double getRatePerLR() {
	return ratePerLR;
}

/**
 * @param stockistName the stockistName to set
 */
public void setRatePerLR(Double ratePerLR) {
	this.ratePerLR = ratePerLR;
}


public Double getClaimRates() {
	return claimRates;
}

/**
 * @param stockistName the stockistName to set
 */
public void setClaimRates(Double claimRates) {
	this.claimRates = claimRates;
}


public Double getClaimRatePerLR() {
	return claimRatePerLR;
}

/**
 * @param stockistName the stockistName to set
 */
public void setClaimRatePerLR(Double claimRatePerLR) {
	this.claimRatePerLR = claimRatePerLR;
}
public String getStockistName() {
	return stockistName;
}

/**
 * @param stockistName the stockistName to set
 */
public void setStockistName(String stockistName) {
	this.stockistName = stockistName;
}

public Integer getStockistId() {
	return stockistId;
}

/**
 * @param stockistName the stockistName to set
 */
public void setStockistId(Integer stockistId) {
	this.stockistId = stockistId;
}

public String getStockistAddress() {
	return stockistAddress;
}

/**
 * @param stockistAddress the stockistAddress to set
 */
public void setStockistAddress(String stockistAddress) {
	this.stockistAddress = stockistAddress;
}

/**
 * @return the stockistMobileNo
 */
public Long getStockistMobileNo() {
	return stockistMobileNo;
}

/**
 * @param stockistMobileNo the stockistMobileNo to set
 */
public void setStockistMobileNo(Long stockistMobileNo) {
	this.stockistMobileNo = stockistMobileNo;
}

/**
 * @return the stockistEmailId
 */
public String getStockistEmailId() {
	return stockistEmailId;
}

/**
 * @param stockistEmailId the stockistEmailId to set
 */
public void setStockistEmailId(String stockistEmailId) {
	this.stockistEmailId = stockistEmailId;
}

/**
 * @return the stockistFaxNo
 */
public String getStockistFaxNo() {
	return stockistFaxNo;
}

/**
 * @param stockistFaxNo the stockistFaxNo to set
 */
public void setStockistFaxNo(String stockistFaxNo) {
	this.stockistFaxNo = stockistFaxNo;
}

/**
 * @return the companyList
 */

/**
 * @return the stockistContactPerson
 */
public String getStockistContactPerson() {
	return stockistContactPerson;
}

/**
 * @param stockistContactPerson the stockistContactPerson to set
 */
public void setStockistContactPerson(String stockistContactPerson) {
	this.stockistContactPerson = stockistContactPerson;
}

/**
 * @return the stockistPartner
 */
public String getStockistPartner() {
	return stockistPartner;
}

/**
 * @param stockistPartner the stockistPartner to set
 */
public void setStockistPartner(String stockistPartner) {
	this.stockistPartner = stockistPartner;
}

/**
 * @return the stockistLocation
 */
public String getStockistLocation() {
	return stockistLocation;
}

/**
 * @param stockistLocation the stockistLocation to set
 */
public void setStockistLocation(String stockistLocation) {
	this.stockistLocation = stockistLocation;
}

/**
 * @return the stockistCST
 */
public String getStockistCST() {
	return stockistCST;
}

/**
 * @param stockistCST the stockistCST to set
 */
public void setStockistCST(String stockistCST) {
	this.stockistCST = stockistCST;
}

/**
 * @return the stockistVAT
 */
public String getStockistVAT() {
	return stockistVAT;
}

/**
 * @param stockistVAT the stockistVAT to set
 */
public void setStockistVAT(String stockistVAT) {
	this.stockistVAT = stockistVAT;
}

/**
 * @return the stockistPanNo
 */
public String getStockistPanNo() {
	return stockistPanNo;
}

/**
 * @param stockistPanNo the stockistPanNo to set
 */
public void setStockistPanNo(String stockistPanNo) {
	this.stockistPanNo = stockistPanNo;
}


public String getCity() {
	return city;
}

/**
 * @param stockistPanNo the stockistPanNo to set
 */
public void setCity(String city) {
	this.city = city;
}


public String getCompany() {
	return company;
}

/**
 * @param stockistPanNo the stockistPanNo to set
 */
public void setCompany(String company) {
	this.company = company;
}
/**
 * @return the status
 */
public Integer getStatus() {
	return status;
}

/**
 * @param status the status to set
 */
public void setStatus(Integer status) {
	this.status = status;
}

/**
 * @return the submitDate
 */
public Date getSubmitDate() {
	return submitDate;
}

/**
 * @param submitDate the submitDate to set
 */
public void setSubmitDate(Date submitDate) {
	this.submitDate = submitDate;
}


}
