/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardproductorderentryregistration")
public class OutWardProductOrderEntryRegistration {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardProductOrderEnteryRegistrationId;
	
	private Integer quantity;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="outWardOrderEnteryRegistrationId")
	private OutWardOrderEntryRegistration outWardOrderEntryRegistration;
	
	@ManyToOne
	@JoinColumn(name="companyProductId")
	private CompanyProduct companyProduct;

	public Integer getOutWardProductOrderEnteryRegistrationId() {
		return outWardProductOrderEnteryRegistrationId;
	}

	public void setOutWardProductOrderEnteryRegistrationId(
			Integer outWardProductOrderEnteryRegistrationId) {
		this.outWardProductOrderEnteryRegistrationId = outWardProductOrderEnteryRegistrationId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}


	public CompanyProduct getCompanyProduct() {
		return companyProduct;
	}

	public void setCompanyProduct(CompanyProduct companyProduct) {
		this.companyProduct = companyProduct;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public OutWardOrderEntryRegistration getOutWardOrderEntryRegistration() {
		return outWardOrderEntryRegistration;
	}

	public void setOutWardOrderEntryRegistration(
			OutWardOrderEntryRegistration outWardOrderEntryRegistration) {
		this.outWardOrderEntryRegistration = outWardOrderEntryRegistration;
	}
	
	
	
}
