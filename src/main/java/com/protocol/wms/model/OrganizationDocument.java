/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="organizationdocument")
public class OrganizationDocument {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer organizationDocumentId;
	
//	private String imagePath;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="documentListTypeId")
	private DocumentListType documentListType;
	
	private Integer status;
	
	@OneToMany(targetEntity=OrganizationDocumentImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationDocumentId",referencedColumnName="organizationDocumentId")
	private List<OrganizationDocumentImagePath> organizationDocumentImagePathList;
	
	/**
	 * @return the organizationDocumentId
	 */
	public Integer getOrganizationDocumentId() {
		return organizationDocumentId;
	}

	/**
	 * @param organizationDocumentId the organizationDocumentId to set
	 */
	public void setOrganizationDocumentId(Integer organizationDocumentId) {
		this.organizationDocumentId = organizationDocumentId;
	}


	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the documentListType
	 */
	public DocumentListType getDocumentListType() {
		return documentListType;
	}

	/**
	 * @param documentListType the documentListType to set
	 */
	public void setDocumentListType(DocumentListType documentListType) {
		this.documentListType = documentListType;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<OrganizationDocumentImagePath> getOrganizationDocumentImagePathList() {
		return organizationDocumentImagePathList;
	}

	public void setOrganizationDocumentImagePathList(
			List<OrganizationDocumentImagePath> organizationDocumentImagePathList) {
		this.organizationDocumentImagePathList = organizationDocumentImagePathList;
	}
}
