/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * @author admin
 *
 */
@Entity
@Table(name="userdetails")
public class UserDetails {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer userDetailsId;
	
	private String userName;
	
	private String password;
	
	@ManyToOne
	@JoinColumn(name="userTypeId")
	private UserType userType;

	@OneToOne(mappedBy="userDetails")
	private Organization organization;
	
	@OneToOne(mappedBy="userDetails")
	private Company company;
	
	@OneToOne(mappedBy="userDetails")
	private Stockist stockist;
	
	@OneToOne(mappedBy="userDetails")
	private TransporterDetails transporterDetails;
	
	@OneToOne(mappedBy="userDetails")
	private EmployeeDetails employeeDetails;
	
	
	/**
	 * @return the userDetailsId
	 */
	public Integer getUserDetailsId() {
		return userDetailsId;
	}

	/**
	 * @param userDetailsId the userDetailsId to set
	 */
	public void setUserDetailsId(Integer userDetailsId) {
		this.userDetailsId = userDetailsId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the userType
	 */
	public UserType getUserType() {
		return userType;
	}

	/**
	 * @param userType the userType to set
	 */
	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the stockist
	 */
	public Stockist getStockist() {
		return stockist;
	}

	/**
	 * @param stockist the stockist to set
	 */
	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	/**
	 * @return the transporterDetails
	 */
	public TransporterDetails getTransporterDetails() {
		return transporterDetails;
	}

	/**
	 * @param transporterDetails the transporterDetails to set
	 */
	public void setTransporterDetails(TransporterDetails transporterDetails) {
		this.transporterDetails = transporterDetails;
	}

	/**
	 * @return the employeeDetails
	 */
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	/**
	 * @param employeeDetails the employeeDetails to set
	 */
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	
	
	
	
}
