/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardtriptransporterdetails")
public class OutWardTripTransporterDetails {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardTripTransporterDetailsId;
	
	@ManyToOne
	@JoinColumn(name="transporterDetailsId")
	private TransporterDetails transporterDetails;
	
	@ManyToOne
	@JoinColumn(name="outWardTripId")
	private OutWardTrip outWardTrip;

	public Integer getOutWardTripTransporterDetailsId() {
		return outWardTripTransporterDetailsId;
	}

	public void setOutWardTripTransporterDetailsId(
			Integer outWardTripTransporterDetailsId) {
		this.outWardTripTransporterDetailsId = outWardTripTransporterDetailsId;
	}

	public TransporterDetails getTransporterDetails() {
		return transporterDetails;
	}

	public void setTransporterDetails(TransporterDetails transporterDetails) {
		this.transporterDetails = transporterDetails;
	}

	public OutWardTrip getOutWardTrip() {
		return outWardTrip;
	}

	public void setOutWardTrip(OutWardTrip outWardTrip) {
		this.outWardTrip = outWardTrip;
	}
	
	
	
}
