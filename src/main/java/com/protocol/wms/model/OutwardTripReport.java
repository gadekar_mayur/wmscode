/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardtripreport")
public class OutwardTripReport {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outwardTripReportId;
	
	private String transporter;
	
	private String stockist;
	
	private String company;
	
	private Integer noOfCases;
	
	private String invoiceNo;
	
	private String invoicePaybleAmount;
	
	private String orgnization;

	private String tripDate;
	
	private String tripNo;
	
	private Integer totalTripCases;
	
	private String cartingAgent;
	
	private String vehicleNo;
	
	private String dispatchBy;
	
	private String driverNo;
	
	private String loadingCharges;
	
	@ManyToOne
	@JoinColumn(name="outWardTripId")
	private OutWardTrip outWardTrip;
	
	public Integer getOutwardTripReportId() {
		return outwardTripReportId;
	}

	public void setOutwardTripReportId(Integer outwardTripReportId) {
		this.outwardTripReportId = outwardTripReportId;
	}

	public String getTransporter() {
		return transporter;
	}

	public void setTransporter(String transporter) {
		this.transporter = transporter;
	}

	public String getStockist() {
		return stockist;
	}

	public void setStockist(String stockist) {
		this.stockist = stockist;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Integer getNoOfCases() {
		return noOfCases;
	}

	public void setNoOfCases(Integer noOfCases) {
		this.noOfCases = noOfCases;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	
	public String getInvoicePaybleAmount() {
		return invoicePaybleAmount;
	}

	public void setInvoicePaybleAmount(String invoicePaybleAmount) {
		this.invoicePaybleAmount = invoicePaybleAmount;
	}

	public String getOrgnization() {
		return orgnization;
	}

	public void setOrgnization(String orgnization) {
		this.orgnization = orgnization;
	}

	public OutWardTrip getOutWardTrip() {
		return outWardTrip;
	}

	public void setOutWardTrip(OutWardTrip outWardTrip) {
		this.outWardTrip = outWardTrip;
	}

	public String getTripDate() {
		return tripDate;
	}

	public void setTripDate(String tripDate) {
		this.tripDate = tripDate;
	}

	public String getTripNo() {
		return tripNo;
	}

	public void setTripNo(String tripNo) {
		this.tripNo = tripNo;
	}

	public Integer getTotalTripCases() {
		return totalTripCases;
	}

	public void setTotalTripCases(Integer totalTripCases) {
		this.totalTripCases = totalTripCases;
	}

	public String getCartingAgent() {
		return cartingAgent;
	}

	public void setCartingAgent(String cartingAgent) {
		this.cartingAgent = cartingAgent;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getDispatchBy() {
		return dispatchBy;
	}

	public void setDispatchBy(String dispatchBy) {
		this.dispatchBy = dispatchBy;
	}

	public String getDriverNo() {
		return driverNo;
	}

	public void setDriverNo(String driverNo) {
		this.driverNo = driverNo;
	}

	public String getLoadingCharges() {
		return loadingCharges;
	}

	public void setLoadingCharges(String loadingCharges) {
		this.loadingCharges = loadingCharges;
	}




	
}
