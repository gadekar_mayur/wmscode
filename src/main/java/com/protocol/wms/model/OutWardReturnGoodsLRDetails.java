/**
 * 
 */
package com.protocol.wms.model;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardreturngoodslrdetails")
public class OutWardReturnGoodsLRDetails {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardReturnGoodsLRDetailsId;
	
	private String lrNo;
	
	private Integer noOfCases;
	
	private String claimNo;
	
	private Float claimAmount;
	
	//private String claimImagePath;
	
	//private String lrImagePath;
	
	private Integer status;
	
	private java.util.Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="outWardReturnGoodsRegistrationId")
	private OutWardReturnGoodsRegistration outWardReturnGoodsRegistration;
	
	@OneToMany(targetEntity=OutWardReturnGoodsLRDetailsLrImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardReturnGoodsLRDetailsId",referencedColumnName="outWardReturnGoodsLRDetailsId")
	private List<OutWardReturnGoodsLRDetailsLrImagePath> outWardReturnGoodsLRDetailsLrImagePathList;
	
	@OneToMany(targetEntity=OutWardReturnGoodsLRDetailsClaimImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardReturnGoodsLRDetailsId",referencedColumnName="outWardReturnGoodsLRDetailsId")
	private List<OutWardReturnGoodsLRDetailsClaimImagePath> outWardReturnGoodsLRDetailsClaimImagePathList;
	
	/*@OneToMany(targetEntity=OutWardReturnGoodsCreditNote.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardReturnGoodsLRDetailsId",referencedColumnName="outWardReturnGoodsLRDetailsId")
	private List<OutWardReturnGoodsCreditNote> outWardReturnGoodsCreditNote;*/
	
	/*@OneToMany(targetEntity=OutWardReturnGoodsProductRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardReturnGoodsLRDetailsId",referencedColumnName="outWardReturnGoodsLRDetailsId")
	private List<OutWardReturnGoodsProductRegistration> outWardReturnGoodsProductRegistration;*/

	public Integer getOutWardReturnGoodsLRDetailsId() {
		return outWardReturnGoodsLRDetailsId;
	}

	public void setOutWardReturnGoodsLRDetailsId(
			Integer outWardReturnGoodsLRDetailsId) {
		this.outWardReturnGoodsLRDetailsId = outWardReturnGoodsLRDetailsId;
	}

	public String getLrNo() {
		return lrNo;
	}

	public void setLrNo(String lrNo) {
		this.lrNo = lrNo;
	}

	public Integer getNoOfCases() {
		return noOfCases;
	}

	public void setNoOfCases(Integer noOfCases) {
		this.noOfCases = noOfCases;
	}

	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	/*public String getClaimImagePath() {
		return claimImagePath;
	}

	public void setClaimImagePath(String claimImagePath) {
		this.claimImagePath = claimImagePath;
	}

	public String getLrImagePath() {
		return lrImagePath;
	}

	public void setLrImagePath(String lrImagePath) {
		this.lrImagePath = lrImagePath;
	}*/

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public OutWardReturnGoodsRegistration getOutWardReturnGoodsRegistration() {
		return outWardReturnGoodsRegistration;
	}

	public void setOutWardReturnGoodsRegistration(
			OutWardReturnGoodsRegistration outWardReturnGoodsRegistration) {
		this.outWardReturnGoodsRegistration = outWardReturnGoodsRegistration;
	}

	/*public List<OutWardReturnGoodsCreditNote> getOutWardReturnGoodsCreditNote() {
		return outWardReturnGoodsCreditNote;
	}

	public void setOutWardReturnGoodsCreditNote(
			List<OutWardReturnGoodsCreditNote> outWardReturnGoodsCreditNote) {
		this.outWardReturnGoodsCreditNote = outWardReturnGoodsCreditNote;
	}

	public List<OutWardReturnGoodsProductRegistration> getOutWardReturnGoodsProductRegistration() {
		return outWardReturnGoodsProductRegistration;
	}

	public void setOutWardReturnGoodsProductRegistration(
			List<OutWardReturnGoodsProductRegistration> outWardReturnGoodsProductRegistration) {
		this.outWardReturnGoodsProductRegistration = outWardReturnGoodsProductRegistration;
	}*/

	/**
	 * @return the claimAmount
	 */
	public Float getClaimAmount() {
		return claimAmount;
	}

	/**
	 * @param claimAmount the claimAmount to set
	 */
	public void setClaimAmount(Float claimAmount) {
		this.claimAmount = claimAmount;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<OutWardReturnGoodsLRDetailsLrImagePath> getOutWardReturnGoodsLRDetailsLrImagePathList() {
		return outWardReturnGoodsLRDetailsLrImagePathList;
	}

	public void setOutWardReturnGoodsLRDetailsLrImagePathList(
			List<OutWardReturnGoodsLRDetailsLrImagePath> outWardReturnGoodsLRDetailsLrImagePathList) {
		this.outWardReturnGoodsLRDetailsLrImagePathList = outWardReturnGoodsLRDetailsLrImagePathList;
	}

	public List<OutWardReturnGoodsLRDetailsClaimImagePath> getOutWardReturnGoodsLRDetailsClaimImagePathList() {
		return outWardReturnGoodsLRDetailsClaimImagePathList;
	}

	public void setOutWardReturnGoodsLRDetailsClaimImagePathList(
			List<OutWardReturnGoodsLRDetailsClaimImagePath> outWardReturnGoodsLRDetailsClaimImagePathList) {
		this.outWardReturnGoodsLRDetailsClaimImagePathList = outWardReturnGoodsLRDetailsClaimImagePathList;
	}
	
}
