/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="stockistchequereturn")
public class StockistChequeReturn {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stockistChequeReturnId;
	
	@Type(type="text")
	private String returnReason;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="stockistChequeDepositId")
	private StockistChequeDeposit stockistChequeDeposit;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;

	public Integer getStockistChequeReturnId() {
		return stockistChequeReturnId;
	}

	public void setStockistChequeReturnId(Integer stockistChequeReturnId) {
		this.stockistChequeReturnId = stockistChequeReturnId;
	}

	public String getReturnReason() {
		return returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public StockistChequeDeposit getStockistChequeDeposit() {
		return stockistChequeDeposit;
	}

	public void setStockistChequeDeposit(StockistChequeDeposit stockistChequeDeposit) {
		this.stockistChequeDeposit = stockistChequeDeposit;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	
}
