/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="companybank")
public class CompanyBank {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer companyBankId;
	
	private String companyBankName;
	
	private String companyBankBranch;
	
	private String companyBankAccountNo;
	
	private String companyBankIfscCode;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;

	@OneToMany(targetEntity=AdvancedChequeInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyBankId",referencedColumnName="companyBankId")
	private List<AdvancedChequeInformation> advancedChequeInformationList;
	
	@OneToMany(targetEntity=StockistChequeDeposit.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyBankId",referencedColumnName="companyBankId")
	private List<StockistChequeDeposit> stockistChequeDepositList;
	
	/**
	 * @return the companyBankId
	 */
	public Integer getCompanyBankId() {
		return companyBankId;
	}

	/**
	 * @param companyBankId the companyBankId to set
	 */
	public void setCompanyBankId(Integer companyBankId) {
		this.companyBankId = companyBankId;
	}

	/**
	 * @return the companyBankName
	 */
	public String getCompanyBankName() {
		return companyBankName;
	}

	/**
	 * @param companyBankName the companyBankName to set
	 */
	public void setCompanyBankName(String companyBankName) {
		this.companyBankName = companyBankName;
	}

	/**
	 * @return the companyBankBranch
	 */
	public String getCompanyBankBranch() {
		return companyBankBranch;
	}

	/**
	 * @param companyBankBranch the companyBankBranch to set
	 */
	public void setCompanyBankBranch(String companyBankBranch) {
		this.companyBankBranch = companyBankBranch;
	}

	/**
	 * @return the companyBankAccountNo
	 */
	public String getCompanyBankAccountNo() {
		return companyBankAccountNo;
	}

	/**
	 * @param companyBankAccountNo the companyBankAccountNo to set
	 */
	public void setCompanyBankAccountNo(String companyBankAccountNo) {
		this.companyBankAccountNo = companyBankAccountNo;
	}

	/**
	 * @return the companyBankIfscCode
	 */
	public String getCompanyBankIfscCode() {
		return companyBankIfscCode;
	}

	/**
	 * @param companyBankIfscCode the companyBankIfscCode to set
	 */
	public void setCompanyBankIfscCode(String companyBankIfscCode) {
		this.companyBankIfscCode = companyBankIfscCode;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the advancedChequeInformationList
	 */
	public List<AdvancedChequeInformation> getAdvancedChequeInformationList() {
		return advancedChequeInformationList;
	}

	/**
	 * @param advancedChequeInformationList the advancedChequeInformationList to set
	 */
	public void setAdvancedChequeInformationList(
			List<AdvancedChequeInformation> advancedChequeInformationList) {
		this.advancedChequeInformationList = advancedChequeInformationList;
	}
	
	
}
