/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardgatepass")
public class OutWardGatePass{ 

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardGatePassId;
	
//	private String lrImagePath;
	
	private Date submitDate;
	
	private String LRNo;
	
	private Integer status;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;

	@OneToMany(targetEntity=OutWardGatePassLrImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardGatePassId",referencedColumnName="outWardGatePassId")
	private List<OutWardGatePassLrImagePath> outWardGatePassLrImagePathList;
	
	public Integer getOutWardGatePassId() {
		return outWardGatePassId;
	}

	public void setOutWardGatePassId(Integer outWardGatePassId) {
		this.outWardGatePassId = outWardGatePassId;
	}


	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public String getLRNo() {
		return LRNo;
	}

	public void setLRNo(String lRNo) {
		LRNo = lRNo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<OutWardGatePassLrImagePath> getOutWardGatePassLrImagePathList() {
		return outWardGatePassLrImagePathList;
	}

	public void setOutWardGatePassLrImagePathList(
			List<OutWardGatePassLrImagePath> outWardGatePassLrImagePathList) {
		this.outWardGatePassLrImagePathList = outWardGatePassLrImagePathList;
	}
}
