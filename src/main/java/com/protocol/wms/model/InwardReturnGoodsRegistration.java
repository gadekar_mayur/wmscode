/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="inwardreturngoodsregistration")
public class InwardReturnGoodsRegistration {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer inwardReturnGoodRegistrationId;
	
	private Date returnGoodsRegistrationDate;
	
	private Time time;
	
	private String driverName;
	
	private Float transportationCharges;
	
	private String driverNo;
	
	private Integer checkingDoneStatus;
	
	private Integer creditNoteStatus;
	
	private Integer status;
	
	private java.util.Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="employeeDetailsId")
	private EmployeeDetails employeeDetails;
	
	@ManyToOne
	@JoinColumn(name="transporterDetailsId")
	private TransporterDetails transporterDetails;
	
	@OneToMany(targetEntity=InwardReturnGoodsRegistrationLRDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="inwardReturnGoodRegistrationId",referencedColumnName="inwardReturnGoodRegistrationId")
	private List<InwardReturnGoodsRegistrationLRDetails> inwardReturnGoodsRegistrationLRDetailList;
	
	
	@OneToMany(targetEntity=CheckingDone.class,cascade=CascadeType.ALL)
	@JoinColumn(name="inwardReturnGoodRegistrationId",referencedColumnName="inwardReturnGoodRegistrationId")
	private List<CheckingDone> checkingDoneList;
	
//	@OneToMany(targetEntity=CreditNote.class,cascade=CascadeType.ALL)
//	@JoinColumn(name="inwardReturnGoodRegistrationId",referencedColumnName="inwardReturnGoodRegistrationId")
//	private List<CreditNote> creditNoteList;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="creditNoteId",referencedColumnName="creditNoteId")
	private CreditNote creditNote;
	
	public Integer getInwardReturnGoodRegistrationId() {
		return inwardReturnGoodRegistrationId;
	}

	public void setInwardReturnGoodRegistrationId(
			Integer inwardReturnGoodRegistrationId) {
		this.inwardReturnGoodRegistrationId = inwardReturnGoodRegistrationId;
	}

	public Date getReturnGoodsRegistrationDate() {
		return returnGoodsRegistrationDate;
	}

	public void setReturnGoodsRegistrationDate(Date returnGoodsRegistrationDate) {
		this.returnGoodsRegistrationDate = returnGoodsRegistrationDate;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public Float getTransportationCharges() {
		return transportationCharges;
	}

	public void setTransportationCharges(Float transportationCharges) {
		this.transportationCharges = transportationCharges;
	}

	public String getDriverNo() {
		return driverNo;
	}

	public void setDriverNo(String driverNo) {
		this.driverNo = driverNo;
	}

	public Integer getCheckingDoneStatus() {
		return checkingDoneStatus;
	}

	public void setCheckingDoneStatus(Integer checkingDoneStatus) {
		this.checkingDoneStatus = checkingDoneStatus;
	}

	public Integer getCreditNoteStatus() {
		return creditNoteStatus;
	}

	public void setCreditNoteStatus(Integer creditNoteStatus) {
		this.creditNoteStatus = creditNoteStatus;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Stockist getStockist() {
		return stockist;
	}

	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public TransporterDetails getTransporterDetails() {
		return transporterDetails;
	}

	public void setTransporterDetails(TransporterDetails transporterDetails) {
		this.transporterDetails = transporterDetails;
	}

	public List<InwardReturnGoodsRegistrationLRDetails> getInwardReturnGoodsRegistrationLRDetailList() {
		return inwardReturnGoodsRegistrationLRDetailList;
	}

	public void setInwardReturnGoodsRegistrationLRDetailList(
			List<InwardReturnGoodsRegistrationLRDetails> inwardReturnGoodsRegistrationLRDetailList) {
		this.inwardReturnGoodsRegistrationLRDetailList = inwardReturnGoodsRegistrationLRDetailList;
	}

	public List<CheckingDone> getCheckingDoneList() {
		return checkingDoneList;
	}

	public void setCheckingDoneList(List<CheckingDone> checkingDoneList) {
		this.checkingDoneList = checkingDoneList;
	}

//	public List<CreditNote> getCreditNoteList() {
//		return creditNoteList;
//	}
//
//	public void setCreditNoteList(List<CreditNote> creditNoteList) {
//		this.creditNoteList = creditNoteList;
//	}

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public CreditNote getCreditNote() {
		return creditNote;
	}

	public void setCreditNote(CreditNote creditNote) {
		this.creditNote = creditNote;
	}
	
	
	
}
