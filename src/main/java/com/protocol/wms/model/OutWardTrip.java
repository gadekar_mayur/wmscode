/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardtrip")
public class OutWardTrip {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardTripId;
	
	private String tripNo;
	
	private Double loadingCharges;
	
	private String dispatchBy;
	
	private String vehicleNo;
	
	private String driverNo;
	
	private Integer noOfCases;
	
	private Integer status;
	
//	private Integer getPassStatus;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="cartingAgentId")
	private CartingAgent cartingAgent;
	
//	@ManyToOne
//	@JoinColumn(name="companyId")
//	private Company company;
	
//	@ManyToOne
//	@JoinColumn(name="transporterDetailsId")
//	private TransporterDetails transporterDetails;
	
	
//	@ManyToOne
//	@JoinColumn(name="outWardGatePassId")
//	private OutWardGatePass outWardGatePass;

	@OneToMany(targetEntity=OutWardTripTransporterDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardTripId",referencedColumnName="outWardTripId")
	private List<OutWardTripTransporterDetails> outWardTripTransporterDetailList;
	
	
	@OneToMany(targetEntity=OutwardTripReport.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardTripId",referencedColumnName="outWardTripId")
	private List<OutwardTripReport> outwardTripReportList;
	
	public Integer getOutWardTripId() {
		return outWardTripId;
	}

	public void setOutWardTripId(Integer outWardTripId) {
		this.outWardTripId = outWardTripId;
	}

	public String getTripNo() {
		return tripNo;
	}

	public void setTripNo(String tripNo) {
		this.tripNo = tripNo;
	}


	public Double getLoadingCharges() {
		return loadingCharges;
	}

	public void setLoadingCharges(Double loadingCharges) {
		this.loadingCharges = loadingCharges;
	}

	public String getDispatchBy() {
		return dispatchBy;
	}

	public void setDispatchBy(String dispatchBy) {
		this.dispatchBy = dispatchBy;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getDriverNo() {
		return driverNo;
	}

	public void setDriverNo(String driverNo) {
		this.driverNo = driverNo;
	}

	public Integer getNoOfCases() {
		return noOfCases;
	}

	public void setNoOfCases(Integer noOfCases) {
		this.noOfCases = noOfCases;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

//	public Integer getGetPassStatus() {
//		return getPassStatus;
//	}
//
//	public void setGetPassStatus(Integer getPassStatus) {
//		this.getPassStatus = getPassStatus;
//	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<OutWardTripTransporterDetails> getOutWardTripTransporterDetailList() {
		return outWardTripTransporterDetailList;
	}

	public void setOutWardTripTransporterDetailList(
			List<OutWardTripTransporterDetails> outWardTripTransporterDetailList) {
		this.outWardTripTransporterDetailList = outWardTripTransporterDetailList;
	}

	public CartingAgent getCartingAgent() {
		return cartingAgent;
	}

	public void setCartingAgent(CartingAgent cartingAgent) {
		this.cartingAgent = cartingAgent;
	}

//	public Company getCompany() {
//		return company;
//	}
//
//	public void setCompany(Company company) {
//		this.company = company;
//	}

//	public TransporterDetails getTransporterDetails() {
//		return transporterDetails;
//	}
//
//	public void setTransporterDetails(TransporterDetails transporterDetails) {
//		this.transporterDetails = transporterDetails;
//	}

//	public OutWardGatePass getOutWardGatePass() {
//		return outWardGatePass;
//	}
//
//	public void setOutWardGatePass(OutWardGatePass outWardGatePass) {
//		this.outWardGatePass = outWardGatePass;
//	}
	
	public List<OutwardTripReport> getOutwardTripReportList() {
		return outwardTripReportList;
	}

	public void setOutwardTripReportList(
			List<OutwardTripReport> outwardTripReportList) {
		this.outwardTripReportList = outwardTripReportList;
	}

	
}
