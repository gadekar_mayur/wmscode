/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author Sudhakar
 *
 */
//@Entity
@Table(name="outwardreturngoodsproductregistration")
public class OutWardReturnGoodsProductRegistration {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardReturnGoodsProductRegistrationId;
	
	private String productName;
	
	private Integer claimQuantity;
	
	private Integer receivedQuantity;
	
	private String batch;
	
	private Date mfgDate;
	
	private Date expiryDate;
	
	private String mfgCompany;
	
	private Integer status;
	
	@Type(type="text")
	private String reason;
	
	private Integer quantityVariance;
	
	/*@ManyToOne
	@JoinColumn(name="outWardReturnGoodsLRDetailsId")
	private OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails;
*/
	public Integer getOutWardReturnGoodsProductRegistrationId() {
		return outWardReturnGoodsProductRegistrationId;
	}

	public void setOutWardReturnGoodsProductRegistrationId(
			Integer outWardReturnGoodsProductRegistrationId) {
		this.outWardReturnGoodsProductRegistrationId = outWardReturnGoodsProductRegistrationId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getClaimQuantity() {
		return claimQuantity;
	}

	public void setClaimQuantity(Integer claimQuantity) {
		this.claimQuantity = claimQuantity;
	}

	public Integer getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(Integer receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getMfgDate() {
		return mfgDate;
	}

	public void setMfgDate(Date mfgDate) {
		this.mfgDate = mfgDate;
	}

	public String getMfgCompany() {
		return mfgCompany;
	}

	public void setMfgCompany(String mfgCompany) {
		this.mfgCompany = mfgCompany;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getQuantityVariance() {
		return quantityVariance;
	}

	public void setQuantityVariance(Integer quantityVariance) {
		this.quantityVariance = quantityVariance;
	}

	/*public OutWardReturnGoodsLRDetails getOutWardReturnGoodsLRDetails() {
		return outWardReturnGoodsLRDetails;
	}

	public void setOutWardReturnGoodsLRDetails(
			OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails) {
		this.outWardReturnGoodsLRDetails = outWardReturnGoodsLRDetails;
	}
*/
	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
