/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="companyorganizationbank")
public class CompanyOrganizationBank {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer companyOrganizationBankId;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="organizationBankId")
	private OrganizationBank organizationBank;

	/**
	 * @return the companyOrganizationBankId
	 */
	public Integer getCompanyOrganizationBankId() {
		return companyOrganizationBankId;
	}

	/**
	 * @param companyOrganizationBankId the companyOrganizationBankId to set
	 */
	public void setCompanyOrganizationBankId(Integer companyOrganizationBankId) {
		this.companyOrganizationBankId = companyOrganizationBankId;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the organizationBank
	 */
	public OrganizationBank getOrganizationBank() {
		return organizationBank;
	}

	/**
	 * @param organizationBank the organizationBank to set
	 */
	public void setOrganizationBank(OrganizationBank organizationBank) {
		this.organizationBank = organizationBank;
	}
	
}
