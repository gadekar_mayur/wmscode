/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="documentlisttype")
public class DocumentListType {	

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer documentListTypeId;
	
	private String documentName;
	
	private Integer status;
	
	@OneToMany(targetEntity=OrganizationDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="documentListTypeId",referencedColumnName="documentListTypeId")
	private List<OrganizationDocument> organizationDocumentsList;
	
	@OneToMany(targetEntity=CompanyDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="documentListTypeId",referencedColumnName="documentListTypeId")
	private List<CompanyDocument> companyDocumentList;
	
//	@OneToMany(targetEntity=EmployeeDetails.class,cascade=CascadeType.ALL)
//	@JoinColumn(name="documentListTypeId",referencedColumnName="documentListTypeId")
//	private List<EmployeeDetails> employeeDetailsList;
	
	@OneToMany(targetEntity=TransporterDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="documentListTypeId",referencedColumnName="documentListTypeId")
	private List<TransporterDocument> transporterDocumentList;
	
	@OneToMany(targetEntity=StockistDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="documentListTypeId",referencedColumnName="documentListTypeId")
	private List<StockistDocument> stockistDocumentList;

	/**
	 * @return the documentListTypeId
	 */
	public Integer getDocumentListTypeId() {
		return documentListTypeId;
	}

	/**
	 * @param documentListTypeId the documentListTypeId to set
	 */
	public void setDocumentListTypeId(Integer documentListTypeId) {
		this.documentListTypeId = documentListTypeId;
	}

	/**
	 * @return the documentName
	 */
	public String getDocumentName() {
		return documentName;
	}

	/**
	 * @param documentName the documentName to set
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the organizationDocumentsList
	 */
	public List<OrganizationDocument> getOrganizationDocumentsList() {
		return organizationDocumentsList;
	}

	/**
	 * @param organizationDocumentsList the organizationDocumentsList to set
	 */
	public void setOrganizationDocumentsList(
			List<OrganizationDocument> organizationDocumentsList) {
		this.organizationDocumentsList = organizationDocumentsList;
	}

	/**
	 * @return the companyDocumentList
	 */
	public List<CompanyDocument> getCompanyDocumentList() {
		return companyDocumentList;
	}

	/**
	 * @param companyDocumentList the companyDocumentList to set
	 */
	public void setCompanyDocumentList(List<CompanyDocument> companyDocumentList) {
		this.companyDocumentList = companyDocumentList;
	}


	/**
	 * @return the transporterDocumentList
	 */
	public List<TransporterDocument> getTransporterDocumentList() {
		return transporterDocumentList;
	}

	/**
	 * @param transporterDocumentList the transporterDocumentList to set
	 */
	public void setTransporterDocumentList(
			List<TransporterDocument> transporterDocumentList) {
		this.transporterDocumentList = transporterDocumentList;
	}

	/**
	 * @return the stockistDocumentList
	 */
	public List<StockistDocument> getStockistDocumentList() {
		return stockistDocumentList;
	}

	/**
	 * @param stockistDocumentList the stockistDocumentList to set
	 */
	public void setStockistDocumentList(List<StockistDocument> stockistDocumentList) {
		this.stockistDocumentList = stockistDocumentList;
	}
}
