/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="transporterdetails")
public class TransporterDetails {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer transporterDetailsId;
	
	private String transporterName;
	
	private String transporterAddress;
	
	private String transporterPanNo;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="cityId")
	private City city;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private State state;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@OneToMany(targetEntity=TransporterStationDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="transporterDetailsId",referencedColumnName="transporterDetailsId")
	private List<TransporterStationDetails> transporterStationDetailsList;
	
	@OneToMany(targetEntity=TransporterDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="transporterDetailsId",referencedColumnName="transporterDetailsId")
	private List<TransporterDocument> transporterDocumentList;
	
	@OneToMany(targetEntity=Stockist.class,cascade=CascadeType.ALL)
	@JoinColumn(name="transporterDetailsId",referencedColumnName="transporterDetailsId")
	private List<Stockist> stockistList;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="userDetailsId")
	private UserDetails userDetails;
	
	@ManyToOne
	@JoinColumn(name="districtId")
	private District district;

	@OneToMany(targetEntity=InwardGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="transporterDetailsId",referencedColumnName="transporterDetailsId")
	private List<InwardGoodsRegistration> inwardGoodsRegistrationList;
	
	
	@OneToMany(targetEntity=InwardReturnGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="transporterDetailsId",referencedColumnName="transporterDetailsId")
	private List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList;
	
//	@OneToMany(targetEntity=OutWardTrip.class,cascade=CascadeType.ALL)
//	@JoinColumn(name="transporterDetailsId",referencedColumnName="transporterDetailsId")
//	private List<OutWardTrip> outWardTripList;
	
	@OneToMany(targetEntity=OutWardTripTransporterDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="transporterDetailsId",referencedColumnName="transporterDetailsId")
	private List<OutWardTripTransporterDetails> outWardTripTransporterDetailList;
	
	/**
	 * @return the transporterDetailsId
	 */
	public Integer getTransporterDetailsId() {
		return transporterDetailsId;
	}

	/**
	 * @param transporterDetailsId the transporterDetailsId to set
	 */
	public void setTransporterDetailsId(Integer transporterDetailsId) {
		this.transporterDetailsId = transporterDetailsId;
	}

	/**
	 * @return the transporterName
	 */
	public String getTransporterName() {
		return transporterName;
	}

	/**
	 * @param transporterName the transporterName to set
	 */
	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}

	/**
	 * @return the transporterAddress
	 */
	public String getTransporterAddress() {
		return transporterAddress;
	}

	/**
	 * @param transporterAddress the transporterAddress to set
	 */
	public void setTransporterAddress(String transporterAddress) {
		this.transporterAddress = transporterAddress;
	}

	/**
	 * @return the transporterPanNo
	 */
	public String getTransporterPanNo() {
		return transporterPanNo;
	}

	/**
	 * @param transporterPanNo the transporterPanNo to set
	 */
	public void setTransporterPanNo(String transporterPanNo) {
		this.transporterPanNo = transporterPanNo;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the city
	 */
	public City getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(City city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the transporterStationDetailsList
	 */
	public List<TransporterStationDetails> getTransporterStationDetailsList() {
		return transporterStationDetailsList;
	}

	/**
	 * @param transporterStationDetailsList the transporterStationDetailsList to set
	 */
	public void setTransporterStationDetailsList(
			List<TransporterStationDetails> transporterStationDetailsList) {
		this.transporterStationDetailsList = transporterStationDetailsList;
	}

	/**
	 * @return the transporterDocumentList
	 */
	public List<TransporterDocument> getTransporterDocumentList() {
		return transporterDocumentList;
	}

	/**
	 * @param transporterDocumentList the transporterDocumentList to set
	 */
	public void setTransporterDocumentList(
			List<TransporterDocument> transporterDocumentList) {
		this.transporterDocumentList = transporterDocumentList;
	}

	/**
	 * @return the stockistList
	 */
	public List<Stockist> getStockistList() {
		return stockistList;
	}

	/**
	 * @param stockistList the stockistList to set
	 */
	public void setStockistList(List<Stockist> stockistList) {
		this.stockistList = stockistList;
	}

	/**
	 * @return the userDetails
	 */
	public UserDetails getUserDetails() {
		return userDetails;
	}

	/**
	 * @param userDetails the userDetails to set
	 */
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public List<InwardGoodsRegistration> getInwardGoodsRegistrationList() {
		return inwardGoodsRegistrationList;
	}

	public void setInwardGoodsRegistrationList(
			List<InwardGoodsRegistration> inwardGoodsRegistrationList) {
		this.inwardGoodsRegistrationList = inwardGoodsRegistrationList;
	}

	public List<InwardReturnGoodsRegistration> getInwardReturnGoodsRegistrationList() {
		return inwardReturnGoodsRegistrationList;
	}

	public void setInwardReturnGoodsRegistrationList(
			List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList) {
		this.inwardReturnGoodsRegistrationList = inwardReturnGoodsRegistrationList;
	}
	
	
	

}
