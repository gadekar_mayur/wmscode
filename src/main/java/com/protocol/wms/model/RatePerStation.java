/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="rateperstation")
public class RatePerStation {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer ratePerStationId;
	
	private Double ratePerCase;
	
	private Double ratePerLR;
	
	private Double ddPerCase;
	
	private Double labourPerCase;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="transporterStationDetailsId")
	private TransporterStationDetails transporterStationDetails;

	/**
	 * @return the ratePerStationId
	 */
	public Integer getRatePerStationId() {
		return ratePerStationId;
	}

	/**
	 * @param ratePerStationId the ratePerStationId to set
	 */
	public void setRatePerStationId(Integer ratePerStationId) {
		this.ratePerStationId = ratePerStationId;
	}

	/**
	 * @return the ratePerCase
	 */
	public Double getRatePerCase() {
		return ratePerCase;
	}

	/**
	 * @param ratePerCase the ratePerCase to set
	 */
	public void setRatePerCase(Double ratePerCase) {
		this.ratePerCase = ratePerCase;
	}

	/**
	 * @return the ratePerLR
	 */
	public Double getRatePerLR() {
		return ratePerLR;
	}

	/**
	 * @param ratePerLR the ratePerLR to set
	 */
	public void setRatePerLR(Double ratePerLR) {
		this.ratePerLR = ratePerLR;
	}

	/**
	 * @return the ddPerCase
	 */
	public Double getDdPerCase() {
		return ddPerCase;
	}

	/**
	 * @param ddPerCase the ddPerCase to set
	 */
	public void setDdPerCase(Double ddPerCase) {
		this.ddPerCase = ddPerCase;
	}

	/**
	 * @return the labourPerCase
	 */
	public Double getLabourPerCase() {
		return labourPerCase;
	}

	/**
	 * @param labourPerCase the labourPerCase to set
	 */
	public void setLabourPerCase(Double labourPerCase) {
		this.labourPerCase = labourPerCase;
	}

	/**
	 * @return the transporterStationDetails
	 */
	public TransporterStationDetails getTransporterStationDetails() {
		return transporterStationDetails;
	}

	/**
	 * @param transporterStationDetails the transporterStationDetails to set
	 */
	public void setTransporterStationDetails(
			TransporterStationDetails transporterStationDetails) {
		this.transporterStationDetails = transporterStationDetails;
	}
}
