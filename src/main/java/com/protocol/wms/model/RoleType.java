/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="roletype")
public class RoleType {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer roleTypeId;
	
	private String roleName;
	
	private Integer status;
	
	
	@OneToMany(targetEntity=Company.class,cascade=CascadeType.ALL)
	@JoinColumn(name="roleTypeId",referencedColumnName="roleTypeId")
	private List<Company> companyList;
	
	@OneToMany(targetEntity=EmployeeDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="roleTypeId",referencedColumnName="roleTypeId")
	private List<EmployeeDetails> employeeDetailsList;

	/**
	 * @return the roleTypeId
	 */
	public Integer getRoleTypeId() {
		return roleTypeId;
	}

	/**
	 * @param roleTypeId the roleTypeId to set
	 */
	public void setRoleTypeId(Integer roleTypeId) {
		this.roleTypeId = roleTypeId;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the companyList
	 */
	public List<Company> getCompanyList() {
		return companyList;
	}

	/**
	 * @param companyList the companyList to set
	 */
	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	/**
	 * @return the employeeDetailsList
	 */
	public List<EmployeeDetails> getEmployeeDetailsList() {
		return employeeDetailsList;
	}

	/**
	 * @param employeeDetailsList the employeeDetailsList to set
	 */
	public void setEmployeeDetailsList(List<EmployeeDetails> employeeDetailsList) {
		this.employeeDetailsList = employeeDetailsList;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
