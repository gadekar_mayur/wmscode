/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="employeedocument")
public class EmployeeDocument {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer employeeDocumentId;
	
	private String imagePath;
	
//	@ManyToOne
//	@JoinColumn(name="companyId")
//	private Company company;
	
	@ManyToOne
	@JoinColumn(name="employeeDetailsId")
	private EmployeeDetails employeeDetails;
	
	@ManyToOne
	@JoinColumn(name="documentListTypeId")
	private DocumentListType documentListType;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	private Integer status;
	
	private Date submitDate;

	@OneToMany(targetEntity=EmployeeDocumentImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="employeeDocumentId",referencedColumnName="employeeDocumentId")
	private List<EmployeeDocumentImagePath> employeeDocumentImagePathList;
	
	/**
	 * @return the employeeDocumentId
	 */
	public Integer getEmployeeDocumentId() {
		return employeeDocumentId;
	}

	/**
	 * @param employeeDocumentId the employeeDocumentId to set
	 */
	public void setEmployeeDocumentId(Integer employeeDocumentId) {
		this.employeeDocumentId = employeeDocumentId;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}


	
	/**
	 * @return the documentListType
	 */
	public DocumentListType getDocumentListType() {
		return documentListType;
	}

	/**
	 * @param documentListType the documentListType to set
	 */
	public void setDocumentListType(DocumentListType documentListType) {
		this.documentListType = documentListType;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public List<EmployeeDocumentImagePath> getEmployeeDocumentImagePathList() {
		return employeeDocumentImagePathList;
	}

	public void setEmployeeDocumentImagePathList(
			List<EmployeeDocumentImagePath> employeeDocumentImagePathList) {
		this.employeeDocumentImagePathList = employeeDocumentImagePathList;
	}
}
