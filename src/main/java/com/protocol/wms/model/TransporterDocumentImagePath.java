package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="transporterdocumentimagepath")
public class TransporterDocumentImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer transporterDocumentImagePathId;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="transporterDocumentId")
	private TransporterDocument transporterDocument;

	public Integer getTransporterDocumentImagePathId() {
		return transporterDocumentImagePathId;
	}

	public void setTransporterDocumentImagePathId(
			Integer transporterDocumentImagePathId) {
		this.transporterDocumentImagePathId = transporterDocumentImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public TransporterDocument getTransporterDocument() {
		return transporterDocument;
	}

	public void setTransporterDocument(TransporterDocument transporterDocument) {
		this.transporterDocument = transporterDocument;
	}
}
