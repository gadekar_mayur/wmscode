/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="organizationbank")
public class OrganizationBank {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer organizationBankId;
	
	private String organizationBankName;
	
	private String organizationBankBranch;
	
	private String organizationBankAccountNo;
	
	private String organizationBankIfscCode;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@OneToMany(targetEntity=CompanyOrganizationBank.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationBankId",referencedColumnName="organizationBankId")
	private List<CompanyOrganizationBank> companyOrganizationBankList;

	@ManyToMany(mappedBy="organizationBankList",cascade=CascadeType.ALL)
	private List<Company> companyList;
	
	@OneToMany(targetEntity=OutWardAdvancedChequeInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationBankId",referencedColumnName="organizationBankId")
	private List<OutWardAdvancedChequeInformation> outWardAdvancedChequeInformation;
	
	/**
	 * @return the companyList
	 */
	public List<Company> getCompanyList() {
		return companyList;
	}

	/**
	 * @param companyList the companyList to set
	 */
	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	/*@ManyToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinTable(name="ORGANIZATIONBANK_COMPANY_JOIN"
	,joinColumns={@JoinColumn(name="organizationBankId")}
	,inverseJoinColumns={@JoinColumn(name="companyId")})
	private List<Company> companyList;
	
	*//**
	 * @return the companyList
	 *//*
	public List<Company> getCompanyList() {
		return companyList;
	}

	*//**
	 * @param companyList the companyList to set
	 *//*
	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}
*/
	/**
	 * @return the organizationBankId
	 */
	public Integer getOrganizationBankId() {
		return organizationBankId;
	}

	/**
	 * @param organizationBankId the organizationBankId to set
	 */
	public void setOrganizationBankId(Integer organizationBankId) {
		this.organizationBankId = organizationBankId;
	}

	/**
	 * @return the organizationBankName
	 */
	public String getOrganizationBankName() {
		return organizationBankName;
	}

	/**
	 * @param organizationBankName the organizationBankName to set
	 */
	public void setOrganizationBankName(String organizationBankName) {
		this.organizationBankName = organizationBankName;
	}

	/**
	 * @return the organizationBankBranch
	 */
	public String getOrganizationBankBranch() {
		return organizationBankBranch;
	}

	/**
	 * @param organizationBankBranch the organizationBankBranch to set
	 */
	public void setOrganizationBankBranch(String organizationBankBranch) {
		this.organizationBankBranch = organizationBankBranch;
	}

	/**
	 * @return the organizationBankAccountNo
	 */
	public String getOrganizationBankAccountNo() {
		return organizationBankAccountNo;
	}

	/**
	 * @param organizationBankAccountNo the organizationBankAccountNo to set
	 */
	public void setOrganizationBankAccountNo(String organizationBankAccountNo) {
		this.organizationBankAccountNo = organizationBankAccountNo;
	}

	/**
	 * @return the organizationBankIfscCode
	 */
	public String getOrganizationBankIfscCode() {
		return organizationBankIfscCode;
	}

	/**
	 * @param organizationBankIfscCode the organizationBankIfscCode to set
	 */
	public void setOrganizationBankIfscCode(String organizationBankIfscCode) {
		this.organizationBankIfscCode = organizationBankIfscCode;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the companyOrganizationBankList
	 */
	public List<CompanyOrganizationBank> getCompanyOrganizationBankList() {
		return companyOrganizationBankList;
	}

	/**
	 * @param companyOrganizationBankList the companyOrganizationBankList to set
	 */
	public void setCompanyOrganizationBankList(
			List<CompanyOrganizationBank> companyOrganizationBankList) {
		this.companyOrganizationBankList = companyOrganizationBankList;
	}

	public List<OutWardAdvancedChequeInformation> getOutWardAdvancedChequeInformation() {
		return outWardAdvancedChequeInformation;
	}

	public void setOutWardAdvancedChequeInformation(
			List<OutWardAdvancedChequeInformation> outWardAdvancedChequeInformation) {
		this.outWardAdvancedChequeInformation = outWardAdvancedChequeInformation;
	}
	
	
	
}
