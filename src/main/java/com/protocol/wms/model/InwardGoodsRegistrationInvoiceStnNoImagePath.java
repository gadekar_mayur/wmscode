/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="inwardgoodsregistrationinvoicestnnoimagepath")
public class InwardGoodsRegistrationInvoiceStnNoImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer InwardGoodsRegistrationInvoiceStnNoImagePathId;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="InwardGoodsRegistrationInvoiceDetailsId")
	private InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetails;

	/**
	 * @return the inwardGoodsRegistrationInvoiceStnNoImagePathId
	 */
	public Integer getInwardGoodsRegistrationInvoiceStnNoImagePathId() {
		return InwardGoodsRegistrationInvoiceStnNoImagePathId;
	}

	/**
	 * @param inwardGoodsRegistrationInvoiceStnNoImagePathId the inwardGoodsRegistrationInvoiceStnNoImagePathId to set
	 */
	public void setInwardGoodsRegistrationInvoiceStnNoImagePathId(
			Integer inwardGoodsRegistrationInvoiceStnNoImagePathId) {
		InwardGoodsRegistrationInvoiceStnNoImagePathId = inwardGoodsRegistrationInvoiceStnNoImagePathId;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the inwardGoodsRegistrationInvoiceDetails
	 */
	public InwardGoodsRegistrationInvoiceDetails getInwardGoodsRegistrationInvoiceDetails() {
		return inwardGoodsRegistrationInvoiceDetails;
	}

	/**
	 * @param inwardGoodsRegistrationInvoiceDetails the inwardGoodsRegistrationInvoiceDetails to set
	 */
	public void setInwardGoodsRegistrationInvoiceDetails(
			InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetails) {
		this.inwardGoodsRegistrationInvoiceDetails = inwardGoodsRegistrationInvoiceDetails;
	}
	
	
}
