/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="menu")
public class Menu {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer menuId;
	
	private String menuName;
	
	@OneToMany(targetEntity=SubMenu.class,cascade=CascadeType.ALL)
	@JoinColumn(name="menuId",referencedColumnName="menuId")
	private List<SubMenu> subMenuList;
	
	@OneToMany(targetEntity=MenuAuthentication.class,cascade=CascadeType.ALL)
	@JoinColumn(name="menuId",referencedColumnName="menuId")
	private List<MenuAuthentication> menuAuthenticationList;

	/**
	 * @return the menuId
	 */
	public Integer getMenuId() {
		return menuId;
	}

	/**
	 * @param menuId the menuId to set
	 */
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	/**
	 * @return the menuName
	 */
	public String getMenuName() {
		return menuName;
	}

	/**
	 * @param menuName the menuName to set
	 */
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	/**
	 * @return the subMenuList
	 */
	public List<SubMenu> getSubMenuList() {
		return subMenuList;
	}

	/**
	 * @param subMenuList the subMenuList to set
	 */
	public void setSubMenuList(List<SubMenu> subMenuList) {
		this.subMenuList = subMenuList;
	}

	/**
	 * @return the menuAuthenticationList
	 */
	public List<MenuAuthentication> getMenuAuthenticationList() {
		return menuAuthenticationList;
	}

	/**
	 * @param menuAuthenticationList the menuAuthenticationList to set
	 */
	public void setMenuAuthenticationList(
			List<MenuAuthentication> menuAuthenticationList) {
		this.menuAuthenticationList = menuAuthenticationList;
	}
	
	
}
