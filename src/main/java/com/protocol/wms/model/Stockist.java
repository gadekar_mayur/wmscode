/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="stockist")
public class Stockist {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stockistId;
	
	private String stockistName;
	
	private String stockistAddress;
	
	private Long stockistMobileNo;
	
	private String stockistEmailId;
	
	private String stockistFaxNo;
	
	private String stockistContactPerson;
	
	private String stockistPartner ;
	
	private String stockistLocation;
	
	private String stockistCST;
	
	private String stockistVAT;
	
	private String stockistPanNo;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="transporterDetailsId")
	private TransporterDetails transporterDetails;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private State state;
	
	@ManyToOne
	@JoinColumn(name="cityId")
	private City city;
	
	@ManyToOne
	@JoinColumn(name="districtId")
	private District district;
	
	
	///------------------mayur for company--------------------------//
/*	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
*/
	@ManyToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinTable(name="STOCKIST_COMPANY_JOIN"
	,joinColumns={@JoinColumn(name="stockistId")}
	,inverseJoinColumns={@JoinColumn(name="companyId")})
	private List<Company> companyList;
	/*@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;*/

	@OneToMany(targetEntity=DrugLicence.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<DrugLicence> drugLicenceList;
	
	@OneToMany(targetEntity=StockistBankDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<StockistBankDetails> stockistBankDetailsList;
	
	@OneToMany(targetEntity=StockistDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<StockistDocument> stockistDocumentList;
	
	@OneToMany(targetEntity=StockistCompany.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<StockistCompany> stockistCompanyList;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="userDetailsId")
	private UserDetails userDetails;
	
	
	@OneToMany(targetEntity=InwardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<InwardCourierRegistration> inwardCourierRegistrationList;

	@OneToMany(targetEntity=InwardReturnGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList;
	
	
	@OneToMany(targetEntity=CheckingDone.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<CheckingDone> checkingDoneList;
	
	@OneToMany(targetEntity=OutWardReturnGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegistration;
	
	@OneToMany(targetEntity=OutWardOrderEntryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<OutWardOrderEntryRegistration> outWardOrderEnteryRegistrations;
	
	
	@OneToMany(targetEntity=AdvancedChequeInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<AdvancedChequeInformation> advancedChequeInformationList;
	
	@OneToMany(targetEntity=OutWardAdvancedChequeInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<OutWardAdvancedChequeInformation> outWardAdvancedChequeInformation;
	
	
	@OneToMany(targetEntity=StockistChequeDeposit.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<StockistChequeDeposit> stockistChequeDepositList;
	
	@OneToMany(targetEntity=RemainingChequeAmount.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<RemainingChequeAmount> remainingChequeAmountList;
	
	@OneToMany(targetEntity=OutWardDispatchEnteryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistId",referencedColumnName="stockistId")
	private List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList;
	
/*	@OneToOne(mappedBy="stockist")
	private TransporterStockistTransfer transporterStockistTransfer;
	
	
	
	public TransporterStockistTransfer getTransporterStockistTransfer() {
		return transporterStockistTransfer;
	}

	public void setTransporterStockistTransfer(TransporterStockistTransfer transporterStockistTransfer) {
		this.transporterStockistTransfer = transporterStockistTransfer;
	}*/

	/**
	 * @return the stockistId
	 */
	public Integer getStockistId() {
		return stockistId;
	}

	/**
	 * @param stockistId the stockistId to set
	 */
	public void setStockistId(Integer stockistId) {
		this.stockistId = stockistId;
	}

	/**
	 * @return the stockistName
	 */
	public String getStockistName() {
		return stockistName;
	}

	/**
	 * @param stockistName the stockistName to set
	 */
	public void setStockistName(String stockistName) {
		this.stockistName = stockistName;
	}

	/**
	 * @return the stockistAddress
	 */
	public String getStockistAddress() {
		return stockistAddress;
	}

	/**
	 * @param stockistAddress the stockistAddress to set
	 */
	public void setStockistAddress(String stockistAddress) {
		this.stockistAddress = stockistAddress;
	}

	/**
	 * @return the stockistMobileNo
	 */
	public Long getStockistMobileNo() {
		return stockistMobileNo;
	}

	/**
	 * @param stockistMobileNo the stockistMobileNo to set
	 */
	public void setStockistMobileNo(Long stockistMobileNo) {
		this.stockistMobileNo = stockistMobileNo;
	}

	/**
	 * @return the stockistEmailId
	 */
	public String getStockistEmailId() {
		return stockistEmailId;
	}

	/**
	 * @param stockistEmailId the stockistEmailId to set
	 */
	public void setStockistEmailId(String stockistEmailId) {
		this.stockistEmailId = stockistEmailId;
	}

	/**
	 * @return the stockistFaxNo
	 */
	public String getStockistFaxNo() {
		return stockistFaxNo;
	}

	/**
	 * @param stockistFaxNo the stockistFaxNo to set
	 */
	public void setStockistFaxNo(String stockistFaxNo) {
		this.stockistFaxNo = stockistFaxNo;
	}

	/**
	 * @return the companyList
	 */
	public List<Company> getCompanyList() {
		return companyList;
	}

	/**
	 * @param companyList the companyList to set
	 */
	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	/**
	 * @return the stockistContactPerson
	 */
	public String getStockistContactPerson() {
		return stockistContactPerson;
	}

	/**
	 * @param stockistContactPerson the stockistContactPerson to set
	 */
	public void setStockistContactPerson(String stockistContactPerson) {
		this.stockistContactPerson = stockistContactPerson;
	}

	/**
	 * @return the stockistPartner
	 */
	public String getStockistPartner() {
		return stockistPartner;
	}

	/**
	 * @param stockistPartner the stockistPartner to set
	 */
	public void setStockistPartner(String stockistPartner) {
		this.stockistPartner = stockistPartner;
	}

	/**
	 * @return the stockistLocation
	 */
	public String getStockistLocation() {
		return stockistLocation;
	}

	/**
	 * @param stockistLocation the stockistLocation to set
	 */
	public void setStockistLocation(String stockistLocation) {
		this.stockistLocation = stockistLocation;
	}

	/**
	 * @return the stockistCST
	 */
	public String getStockistCST() {
		return stockistCST;
	}

	/**
	 * @param stockistCST the stockistCST to set
	 */
	public void setStockistCST(String stockistCST) {
		this.stockistCST = stockistCST;
	}

	/**
	 * @return the stockistVAT
	 */
	public String getStockistVAT() {
		return stockistVAT;
	}

	/**
	 * @param stockistVAT the stockistVAT to set
	 */
	public void setStockistVAT(String stockistVAT) {
		this.stockistVAT = stockistVAT;
	}

	/**
	 * @return the stockistPanNo
	 */
	public String getStockistPanNo() {
		return stockistPanNo;
	}

	/**
	 * @param stockistPanNo the stockistPanNo to set
	 */
	public void setStockistPanNo(String stockistPanNo) {
		this.stockistPanNo = stockistPanNo;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	////////////---------------mayur for company-------------------------
	
	/*public Company getCompany() {
		return company;
	}*/

	/**
	 * @param organization the organization to set
	 */
	/*public void setCompany(Company company) {
		this.company = company;
	}*/

	/**
	 * @return the transporterDetails
	 */
	public TransporterDetails getTransporterDetails() {
		return transporterDetails;
	}

	/**
	 * @param transporterDetails the transporterDetails to set
	 */
	public void setTransporterDetails(TransporterDetails transporterDetails) {
		this.transporterDetails = transporterDetails;
	}

	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @return the city
	 */
	public City getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(City city) {
		this.city = city;
	}

	/**
	 * @return the district
	 */
	public District getDistrict() {
		return district;
	}

	/**
	 * @param district the district to set
	 */
	public void setDistrict(District district) {
		this.district = district;
	}

	/**
	 * @return the company
	 *//*
	public Company getCompany() {
		return company;
	}

	*//**
	 * @param company the company to set
	 *//*
	public void setCompany(Company company) {
		this.company = company;
	}*/

	/**
	 * @return the drugLicenceList
	 */
	public List<DrugLicence> getDrugLicenceList() {
		return drugLicenceList;
	}

	/**
	 * @param drugLicenceList the drugLicenceList to set
	 */
	public void setDrugLicenceList(List<DrugLicence> drugLicenceList) {
		this.drugLicenceList = drugLicenceList;
	}

	/**
	 * @return the stockistBankDetailsList
	 */
	public List<StockistBankDetails> getStockistBankDetailsList() {
		return stockistBankDetailsList;
	}

	/**
	 * @param stockistBankDetailsList the stockistBankDetailsList to set
	 */
	public void setStockistBankDetailsList(
			List<StockistBankDetails> stockistBankDetailsList) {
		this.stockistBankDetailsList = stockistBankDetailsList;
	}

	/**
	 * @return the stockistDocumentList
	 */
	public List<StockistDocument> getStockistDocumentList() {
		return stockistDocumentList;
	}

	/**
	 * @param stockistDocumentList the stockistDocumentList to set
	 */
	public void setStockistDocumentList(List<StockistDocument> stockistDocumentList) {
		this.stockistDocumentList = stockistDocumentList;
	}

	/**
	 * @return the stockistCompanyList
	 */
	public List<StockistCompany> getStockistCompanyList() {
		return stockistCompanyList;
	}

	/**
	 * @param stockistCompanyList the stockistCompanyList to set
	 */
	public void setStockistCompanyList(List<StockistCompany> stockistCompanyList) {
		this.stockistCompanyList = stockistCompanyList;
	}

	/**
	 * @return the userDetails
	 */
	public UserDetails getUserDetails() {
		return userDetails;
	}

	/**
	 * @param userDetails the userDetails to set
	 */
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public List<InwardCourierRegistration> getInwardCourierRegistrationList() {
		return inwardCourierRegistrationList;
	}

	public void setInwardCourierRegistrationList(
			List<InwardCourierRegistration> inwardCourierRegistrationList) {
		this.inwardCourierRegistrationList = inwardCourierRegistrationList;
	}

	public List<InwardReturnGoodsRegistration> getInwardReturnGoodsRegistrationList() {
		return inwardReturnGoodsRegistrationList;
	}

	public void setInwardReturnGoodsRegistrationList(
			List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList) {
		this.inwardReturnGoodsRegistrationList = inwardReturnGoodsRegistrationList;
	}

	public List<CheckingDone> getCheckingDoneList() {
		return checkingDoneList;
	}

	public void setCheckingDoneList(List<CheckingDone> checkingDoneList) {
		this.checkingDoneList = checkingDoneList;
	}

	public List<OutWardReturnGoodsRegistration> getOutWardReturnGoodsRegistration() {
		return outWardReturnGoodsRegistration;
	}

	public void setOutWardReturnGoodsRegistration(
			List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegistration) {
		this.outWardReturnGoodsRegistration = outWardReturnGoodsRegistration;
	}

	public List<OutWardOrderEntryRegistration> getOutWardOrderEnteryRegistrations() {
		return outWardOrderEnteryRegistrations;
	}

	public void setOutWardOrderEnteryRegistrations(
			List<OutWardOrderEntryRegistration> outWardOrderEnteryRegistrations) {
		this.outWardOrderEnteryRegistrations = outWardOrderEnteryRegistrations;
	}

	public List<AdvancedChequeInformation> getAdvancedChequeInformationList() {
		return advancedChequeInformationList;
	}

	public void setAdvancedChequeInformationList(
			List<AdvancedChequeInformation> advancedChequeInformationList) {
		this.advancedChequeInformationList = advancedChequeInformationList;
	}

	public List<OutWardAdvancedChequeInformation> getOutWardAdvancedChequeInformation() {
		return outWardAdvancedChequeInformation;
	}

	public void setOutWardAdvancedChequeInformation(
			List<OutWardAdvancedChequeInformation> outWardAdvancedChequeInformation) {
		this.outWardAdvancedChequeInformation = outWardAdvancedChequeInformation;
	}

	public List<StockistChequeDeposit> getStockistChequeDepositList() {
		return stockistChequeDepositList;
	}

	public void setStockistChequeDepositList(
			List<StockistChequeDeposit> stockistChequeDepositList) {
		this.stockistChequeDepositList = stockistChequeDepositList;
	}

	public List<RemainingChequeAmount> getRemainingChequeAmountList() {
		return remainingChequeAmountList;
	}

	public void setRemainingChequeAmountList(
			List<RemainingChequeAmount> remainingChequeAmountList) {
		this.remainingChequeAmountList = remainingChequeAmountList;
	}

	public List<OutWardDispatchEnteryRegistration> getOutWardDispatchEnteryRegistrationList() {
		return outWardDispatchEnteryRegistrationList;
	}

	public void setOutWardDispatchEnteryRegistrationList(
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList) {
		this.outWardDispatchEnteryRegistrationList = outWardDispatchEnteryRegistrationList;
	}
	
	
	
}
