/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="submenu")
public class SubMenu {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer subMenuId;
	
	private String subMenuName;
	
	private String subMenuFunctionName;
	
	@ManyToOne
	@JoinColumn(name="menuId")
	private Menu menu;
	
	@OneToMany(targetEntity=SubMenuAuthentication.class,cascade=CascadeType.ALL)
	@JoinColumn(name="subMenuId",referencedColumnName="subMenuId")
	private List<SubMenuAuthentication> subMenuAuthenticationList;

	/**
	 * @return the subMenuId
	 */
	public Integer getSubMenuId() {
		return subMenuId;
	}

	/**
	 * @param subMenuId the subMenuId to set
	 */
	public void setSubMenuId(Integer subMenuId) {
		this.subMenuId = subMenuId;
	}

	/**
	 * @return the subMenuName
	 */
	public String getSubMenuName() {
		return subMenuName;
	}

	/**
	 * @param subMenuName the subMenuName to set
	 */
	public void setSubMenuName(String subMenuName) {
		this.subMenuName = subMenuName;
	}

	/**
	 * @return the subMenuFunctionName
	 */
	public String getSubMenuFunctionName() {
		return subMenuFunctionName;
	}

	/**
	 * @param subMenuFunctionName the subMenuFunctionName to set
	 */
	public void setSubMenuFunctionName(String subMenuFunctionName) {
		this.subMenuFunctionName = subMenuFunctionName;
	}

	/**
	 * @return the menu
	 */
	public Menu getMenu() {
		return menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	/**
	 * @return the subMenuAuthenticationList
	 */
	public List<SubMenuAuthentication> getSubMenuAuthenticationList() {
		return subMenuAuthenticationList;
	}

	/**
	 * @param subMenuAuthenticationList the subMenuAuthenticationList to set
	 */
	public void setSubMenuAuthenticationList(
			List<SubMenuAuthentication> subMenuAuthenticationList) {
		this.subMenuAuthenticationList = subMenuAuthenticationList;
	}
}
