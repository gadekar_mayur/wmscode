/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;



/**
 * @author admin
 *
 */
@Entity
@Table(name="claimrates")
public class ClaimRates {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer claimRatesId;
	
	private Double claimRates;
	
	private Double ratePerLR;
	
	private Double ddPerCase;
	
	private Double labourPerCase;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="transporterStationDetailsId")
	private TransporterStationDetails transporterStationDetails;

	/**
	 * @return the claimRatesId
	 */
	public Integer getClaimRatesId() {
		return claimRatesId;
	}

	/**
	 * @param claimRatesId the claimRatesId to set
	 */
	public void setClaimRatesId(Integer claimRatesId) {
		this.claimRatesId = claimRatesId;
	}

	/**
	 * @return the claimRates
	 */
	public Double getClaimRates() {
		return claimRates;
	}

	/**
	 * @param claimRates the claimRates to set
	 */
	public void setClaimRates(Double claimRates) {
		this.claimRates = claimRates;
	}

	/**
	 * @return the ratePerLR
	 */
	public Double getRatePerLR() {
		return ratePerLR;
	}

	/**
	 * @param ratePerLR the ratePerLR to set
	 */
	public void setRatePerLR(Double ratePerLR) {
		this.ratePerLR = ratePerLR;
	}

	/**
	 * @return the ddPerCase
	 */
	public Double getDdPerCase() {
		return ddPerCase;
	}

	/**
	 * @param ddPerCase the ddPerCase to set
	 */
	public void setDdPerCase(Double ddPerCase) {
		this.ddPerCase = ddPerCase;
	}

	/**
	 * @return the labourPerCase
	 */
	public Double getLabourPerCase() {
		return labourPerCase;
	}

	/**
	 * @param labourPerCase the labourPerCase to set
	 */
	public void setLabourPerCase(Double labourPerCase) {
		this.labourPerCase = labourPerCase;
	}

	/**
	 * @return the transporterStationDetails
	 */
	public TransporterStationDetails getTransporterStationDetails() {
		return transporterStationDetails;
	}

	/**
	 * @param transporterStationDetails the transporterStationDetails to set
	 */
	public void setTransporterStationDetails(
			TransporterStationDetails transporterStationDetails) {
		this.transporterStationDetails = transporterStationDetails;
	}

}
