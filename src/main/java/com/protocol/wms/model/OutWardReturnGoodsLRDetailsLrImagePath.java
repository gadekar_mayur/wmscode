package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="outwardreturngoodslrdetailslrimagepath")
public class OutWardReturnGoodsLRDetailsLrImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardReturnGoodsLRDetailsLrImagePathId ;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="outWardReturnGoodsLRDetailsId")
	private OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails;

	public Integer getOutWardReturnGoodsLRDetailsLrImagePathId() {
		return outWardReturnGoodsLRDetailsLrImagePathId;
	}

	public void setOutWardReturnGoodsLRDetailsLrImagePathId(
			Integer outWardReturnGoodsLRDetailsLrImagePathId) {
		this.outWardReturnGoodsLRDetailsLrImagePathId = outWardReturnGoodsLRDetailsLrImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public OutWardReturnGoodsLRDetails getOutWardReturnGoodsLRDetails() {
		return outWardReturnGoodsLRDetails;
	}

	public void setOutWardReturnGoodsLRDetails(
			OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails) {
		this.outWardReturnGoodsLRDetails = outWardReturnGoodsLRDetails;
	}
	
	
	
}
