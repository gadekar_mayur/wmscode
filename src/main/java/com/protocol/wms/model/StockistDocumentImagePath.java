package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="stockistdocumentimagepath")
public class StockistDocumentImagePath {
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stockistDocumentImagePathId ;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="stockistDocumentId")
	private StockistDocument stockistDocument;

	public Integer getStockistDocumentImagePathId() {
		return stockistDocumentImagePathId;
	}

	public void setStockistDocumentImagePathId(Integer stockistDocumentImagePathId) {
		this.stockistDocumentImagePathId = stockistDocumentImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public StockistDocument getStockistDocument() {
		return stockistDocument;
	}

	public void setStockistDocument(StockistDocument stockistDocument) {
		this.stockistDocument = stockistDocument;
	}
	
}
