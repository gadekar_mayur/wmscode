/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="employeedetails")
public class EmployeeDetails {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer employeeDetailsId;
	
	private String name;
	
	private String gender;
	
	private String qualification;
	
	private String maritalStats;
	
	private Long telephone;
	
	private Long mobileNo;
	
	private String emailId;
	
	private String currentAddress;
	
	private String pinCode;
	
	private String permanetAddress;
	
	private String panNo;
	
	private Date DOJ;
	
	private Date DOB;
	
	private Double basicSalary;
	
	private String imagePath;
	
	private String addressProofPath;
	
	private Integer status;
	
	private java.util.Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="bloodGroupId")
	private BloodGroup bloodGroup;
	
	@ManyToOne
	@JoinColumn(name="roleTypeId")
	private RoleType roleType;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@OneToMany(targetEntity=EmployeeDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="employeeDetailsId",referencedColumnName="employeeDetailsId")
	private List<EmployeeDocument> employeeDocumentList;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="userDetailsId")
	private UserDetails userDetails;
	
	@OneToMany(targetEntity=InwardReturnGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="employeeDetailsId",referencedColumnName="employeeDetailsId")
	private List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList;
	
	@OneToMany(targetEntity=CheckingDone.class,cascade=CascadeType.ALL)
	@JoinColumn(name="employeeDetailsId",referencedColumnName="employeeDetailsId")
	private List<CheckingDone> checkingDoneList;

	@OneToMany(targetEntity=OutWardReturnGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="employeeDetailsId",referencedColumnName="employeeDetailsId")
	private List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegistration;
	
	@OneToMany(targetEntity=EmployeeDetailsImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="employeeDetailsId",referencedColumnName="employeeDetailsId")
	private List<EmployeeDetailsImagePath> EmployeeDetailsImagePathList;

	@OneToMany(targetEntity=EmployeeDetailsAddressProofPath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="employeeDetailsId",referencedColumnName="employeeDetailsId")
	private List<EmployeeDetailsAddressProofPath> employeeDetailsAddressProofPathList;
	
	public Integer getEmployeeDetailsId() {
		return employeeDetailsId;
	}

	public void setEmployeeDetailsId(Integer employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getMaritalStats() {
		return maritalStats;
	}

	public void setMaritalStats(String maritalStats) {
		this.maritalStats = maritalStats;
	}

	public Long getTelephone() {
		return telephone;
	}

	public void setTelephone(Long telephone) {
		this.telephone = telephone;
	}

	public Long getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCurrentAddress() {
		return currentAddress;
	}

	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPermanetAddress() {
		return permanetAddress;
	}

	public void setPermanetAddress(String permanetAddress) {
		this.permanetAddress = permanetAddress;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public Date getDOJ() {
		return DOJ;
	}

	public void setDOJ(Date dOJ) {
		DOJ = dOJ;
	}

	public Date getDOB() {
		return DOB;
	}

	public void setDOB(Date dOB) {
		DOB = dOB;
	}

	public Double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(Double basicSalary) {
		this.basicSalary = basicSalary;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getAddressProofPath() {
		return addressProofPath;
	}

	public void setAddressProofPath(String addressProofPath) {
		this.addressProofPath = addressProofPath;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}



	

	public BloodGroup getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(BloodGroup bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public RoleType getRoleType() {
		return roleType;
	}

	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List<EmployeeDocument> getEmployeeDocumentList() {
		return employeeDocumentList;
	}

	public void setEmployeeDocumentList(List<EmployeeDocument> employeeDocumentList) {
		this.employeeDocumentList = employeeDocumentList;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public List<InwardReturnGoodsRegistration> getInwardReturnGoodsRegistrationList() {
		return inwardReturnGoodsRegistrationList;
	}

	public void setInwardReturnGoodsRegistrationList(
			List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList) {
		this.inwardReturnGoodsRegistrationList = inwardReturnGoodsRegistrationList;
	}

	public List<CheckingDone> getCheckingDoneList() {
		return checkingDoneList;
	}

	public void setCheckingDoneList(List<CheckingDone> checkingDoneList) {
		this.checkingDoneList = checkingDoneList;
	}

	public List<OutWardReturnGoodsRegistration> getOutWardReturnGoodsRegistration() {
		return outWardReturnGoodsRegistration;
	}

	public void setOutWardReturnGoodsRegistration(
			List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegistration) {
		this.outWardReturnGoodsRegistration = outWardReturnGoodsRegistration;
	}

	public List<EmployeeDetailsImagePath> getEmployeeDetailsImagePathList() {
		return EmployeeDetailsImagePathList;
	}

	public void setEmployeeDetailsImagePathList(
			List<EmployeeDetailsImagePath> employeeDetailsImagePathList) {
		EmployeeDetailsImagePathList = employeeDetailsImagePathList;
	}

	public List<EmployeeDetailsAddressProofPath> getEmployeeDetailsAddressProofPathList() {
		return employeeDetailsAddressProofPathList;
	}

	public void setEmployeeDetailsAddressProofPathList(
			List<EmployeeDetailsAddressProofPath> employeeDetailsAddressProofPathList) {
		this.employeeDetailsAddressProofPathList = employeeDetailsAddressProofPathList;
	}
}
