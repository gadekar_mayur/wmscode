/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="inwardgoodsregistrationclaimpicturesimagepath")
public class InwardGoodsRegistrationClaimPicturesImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer InwardGoodsRegistrationClaimPicturesImagePathId ;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="inwardGoodsRegistrationClaimLetterId")
	private InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetter;

	/**
	 * @return the inwardGoodsRegistrationClaimPicturesImagePathId
	 */
	public Integer getInwardGoodsRegistrationClaimPicturesImagePathId() {
		return InwardGoodsRegistrationClaimPicturesImagePathId;
	}

	/**
	 * @param inwardGoodsRegistrationClaimPicturesImagePathId the inwardGoodsRegistrationClaimPicturesImagePathId to set
	 */
	public void setInwardGoodsRegistrationClaimPicturesImagePathId(
			Integer inwardGoodsRegistrationClaimPicturesImagePathId) {
		InwardGoodsRegistrationClaimPicturesImagePathId = inwardGoodsRegistrationClaimPicturesImagePathId;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the inwardGoodsRegistrationClaimLetter
	 */
	public InwardGoodsRegistrationClaimLetter getInwardGoodsRegistrationClaimLetter() {
		return inwardGoodsRegistrationClaimLetter;
	}

	/**
	 * @param inwardGoodsRegistrationClaimLetter the inwardGoodsRegistrationClaimLetter to set
	 */
	public void setInwardGoodsRegistrationClaimLetter(
			InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetter) {
		this.inwardGoodsRegistrationClaimLetter = inwardGoodsRegistrationClaimLetter;
	}
	
}
