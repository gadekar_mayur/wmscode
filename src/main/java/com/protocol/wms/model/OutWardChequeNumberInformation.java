/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardchequenumberinformation")
public class OutWardChequeNumberInformation {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardChequeNumberInformationId;
	
	private String chequeNumber;
	
	private Integer status;
	
	@ManyToOne
	@JoinColumn(name="outWardAdvancedChequeInformationId")
	private OutWardAdvancedChequeInformation outWardAdvancedChequeInformation;

	public Integer getOutWardChequeNumberInformationId() {
		return outWardChequeNumberInformationId;
	}

	public void setOutWardChequeNumberInformationId(
			Integer outWardChequeNumberInformationId) {
		this.outWardChequeNumberInformationId = outWardChequeNumberInformationId;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public OutWardAdvancedChequeInformation getOutWardAdvancedChequeInformation() {
		return outWardAdvancedChequeInformation;
	}

	public void setOutWardAdvancedChequeInformation(
			OutWardAdvancedChequeInformation outWardAdvancedChequeInformation) {
		this.outWardAdvancedChequeInformation = outWardAdvancedChequeInformation;
	}
	
	
	
}
