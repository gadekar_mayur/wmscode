/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="inwardgoodsregistrationclaimletter")
public class InwardGoodsRegistrationClaimLetter {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer inwardGoodsRegistrationClaimLetterId;
	
	//private String claimLetterImagePath;
	
	//private String mobileImagePath;
	
	private Date submitDate;
	
	@Type(type="text")
	private String description;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="InwardGoodsRegistrationId")
	private InwardGoodsRegistration inwardGoodsRegistration;

	@OneToMany(targetEntity=InwardGoodsRegistrationClaimLetterImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="inwardGoodsRegistrationClaimLetterId",referencedColumnName="inwardGoodsRegistrationClaimLetterId")
	private List<InwardGoodsRegistrationClaimLetterImagePath> inwardGoodsRegistrationClaimLetterImagePathList;
	
	@OneToMany(targetEntity=InwardGoodsRegistrationClaimPicturesImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="inwardGoodsRegistrationClaimLetterId",referencedColumnName="inwardGoodsRegistrationClaimLetterId")
	private List<InwardGoodsRegistrationClaimPicturesImagePath> inwardGoodsRegistrationClaimPicturesImagePathList;
	
	public Integer getInwardGoodsRegistrationClaimLetterId() {
		return inwardGoodsRegistrationClaimLetterId;
	}

	public void setInwardGoodsRegistrationClaimLetterId(
			Integer inwardGoodsRegistrationClaimLetterId) {
		this.inwardGoodsRegistrationClaimLetterId = inwardGoodsRegistrationClaimLetterId;
	}

	/*public String getClaimLetterImagePath() {
		return claimLetterImagePath;
	}

	public void setClaimLetterImagePath(String claimLetterImagePath) {
		this.claimLetterImagePath = claimLetterImagePath;
	}

	public String getMobileImagePath() {
		return mobileImagePath;
	}

	public void setMobileImagePath(String mobileImagePath) {
		this.mobileImagePath = mobileImagePath;
	}*/

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public InwardGoodsRegistration getInwardGoodsRegistration() {
		return inwardGoodsRegistration;
	}

	public void setInwardGoodsRegistration(
			InwardGoodsRegistration inwardGoodsRegistration) {
		this.inwardGoodsRegistration = inwardGoodsRegistration;
	}

	/**
	 * @return the inwardGoodsRegistrationClaimLetterImagePathList
	 */
	public List<InwardGoodsRegistrationClaimLetterImagePath> getInwardGoodsRegistrationClaimLetterImagePathList() {
		return inwardGoodsRegistrationClaimLetterImagePathList;
	}

	/**
	 * @param inwardGoodsRegistrationClaimLetterImagePathList the inwardGoodsRegistrationClaimLetterImagePathList to set
	 */
	public void setInwardGoodsRegistrationClaimLetterImagePathList(
			List<InwardGoodsRegistrationClaimLetterImagePath> inwardGoodsRegistrationClaimLetterImagePathList) {
		this.inwardGoodsRegistrationClaimLetterImagePathList = inwardGoodsRegistrationClaimLetterImagePathList;
	}

	/**
	 * @return the inwardGoodsRegistrationClaimPicturesImagePathList
	 */
	public List<InwardGoodsRegistrationClaimPicturesImagePath> getInwardGoodsRegistrationClaimPicturesImagePathList() {
		return inwardGoodsRegistrationClaimPicturesImagePathList;
	}

	/**
	 * @param inwardGoodsRegistrationClaimPicturesImagePathList the inwardGoodsRegistrationClaimPicturesImagePathList to set
	 */
	public void setInwardGoodsRegistrationClaimPicturesImagePathList(
			List<InwardGoodsRegistrationClaimPicturesImagePath> inwardGoodsRegistrationClaimPicturesImagePathList) {
		this.inwardGoodsRegistrationClaimPicturesImagePathList = inwardGoodsRegistrationClaimPicturesImagePathList;
	}
}
