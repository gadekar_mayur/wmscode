/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardreturngoodsregistration")
public class OutWardReturnGoodsRegistration {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardReturnGoodsRegistrationId;
	
	private Date registrationDate;
	
	private Time time;
	
	private String driverName;
	
	private Float transportationCharges;
	
	private String driverNo;
	
	@Type(type="text")
	private String reason;
	
	private java.util.Date submitDate;
	
	private Integer status;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="employeeDetailsId")
	private EmployeeDetails employeeDetails;
	
	@ManyToOne
	@JoinColumn(name="transporterDetailsId")
	private TransporterDetails transporterDetails;
	
	@OneToMany(targetEntity=OutWardReturnGoodsLRDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardReturnGoodsRegistrationId",referencedColumnName="outWardReturnGoodsRegistrationId")
	private List<OutWardReturnGoodsLRDetails> outWardReturnGoodsLRDetails;

	public Integer getOutWardReturnGoodsRegistrationId() {
		return outWardReturnGoodsRegistrationId;
	}

	public void setOutWardReturnGoodsRegistrationId(
			Integer outWardReturnGoodsRegistrationId) {
		this.outWardReturnGoodsRegistrationId = outWardReturnGoodsRegistrationId;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public Float getTransportationCharges() {
		return transportationCharges;
	}

	public void setTransportationCharges(Float transportationCharges) {
		this.transportationCharges = transportationCharges;
	}

	public String getDriverNo() {
		return driverNo;
	}

	public void setDriverNo(String driverNo) {
		this.driverNo = driverNo;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Stockist getStockist() {
		return stockist;
	}

	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	/**
	 * @return the transporterDetails
	 */
	public TransporterDetails getTransporterDetails() {
		return transporterDetails;
	}

	/**
	 * @param transporterDetails the transporterDetails to set
	 */
	public void setTransporterDetails(TransporterDetails transporterDetails) {
		this.transporterDetails = transporterDetails;
	}

	public List<OutWardReturnGoodsLRDetails> getOutWardReturnGoodsLRDetails() {
		return outWardReturnGoodsLRDetails;
	}

	public void setOutWardReturnGoodsLRDetails(
			List<OutWardReturnGoodsLRDetails> outWardReturnGoodsLRDetails) {
		this.outWardReturnGoodsLRDetails = outWardReturnGoodsLRDetails;
	}
	
}
