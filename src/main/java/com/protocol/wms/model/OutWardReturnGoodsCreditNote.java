/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardreturngoodscreditnote")
public class OutWardReturnGoodsCreditNote {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardReturnGoodsCreditNoteId;
	
	private String creditNoteNo;
	
	private Double creditNoteAmount;
	
	//private String creditNoteImagePath;
	
	private String refNoOrClaimNo;
	
	private String remark;
	
	private Date submitDate;
	
	private Integer status;
	
	@OneToMany(targetEntity=OutWardReturnGoodsCreditNoteCreditNoteImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardReturnGoodsCreditNoteId",referencedColumnName="outWardReturnGoodsCreditNoteId")
	private List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> outWardReturnGoodsCreditNoteCreditNoteImagePathList;
	
	/*@ManyToOne
	@JoinColumn(name="outWardReturnGoodsLRDetailsId")
	private OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails;*/

	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	/**
	 * @return the outWardReturnGoodsCreditNoteId
	 */
	public Integer getOutWardReturnGoodsCreditNoteId() {
		return outWardReturnGoodsCreditNoteId;
	}

	/**
	 * @param outWardReturnGoodsCreditNoteId the outWardReturnGoodsCreditNoteId to set
	 */
	public void setOutWardReturnGoodsCreditNoteId(
			Integer outWardReturnGoodsCreditNoteId) {
		this.outWardReturnGoodsCreditNoteId = outWardReturnGoodsCreditNoteId;
	}

	public String getCreditNoteNo() {
		return creditNoteNo;
	}

	public void setCreditNoteNo(String creditNoteNo) {
		this.creditNoteNo = creditNoteNo;
	}

	public Double getCreditNoteAmount() {
		return creditNoteAmount;
	}

	public void setCreditNoteAmount(Double creditNoteAmount) {
		this.creditNoteAmount = creditNoteAmount;
	}

	/*public String getCreditNoteImagePath() {
		return creditNoteImagePath;
	}

	public void setCreditNoteImagePath(String creditNoteImagePath) {
		this.creditNoteImagePath = creditNoteImagePath;
	}*/

	/**
	 * @return the refNoOrClaimNo
	 */
	public String getRefNoOrClaimNo() {
		return refNoOrClaimNo;
	}

	/**
	 * @param refNoOrClaimNo the refNoOrClaimNo to set
	 */
	public void setRefNoOrClaimNo(String refNoOrClaimNo) {
		this.refNoOrClaimNo = refNoOrClaimNo;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> getOutWardReturnGoodsCreditNoteCreditNoteImagePathList() {
		return outWardReturnGoodsCreditNoteCreditNoteImagePathList;
	}

	public void setOutWardReturnGoodsCreditNoteCreditNoteImagePathList(
			List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> outWardReturnGoodsCreditNoteCreditNoteImagePathList) {
		this.outWardReturnGoodsCreditNoteCreditNoteImagePathList = outWardReturnGoodsCreditNoteCreditNoteImagePathList;
	}

	/*public OutWardReturnGoodsLRDetails getOutWardReturnGoodsLRDetails() {
		return outWardReturnGoodsLRDetails;
	}

	public void setOutWardReturnGoodsLRDetails(
			OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails) {
		this.outWardReturnGoodsLRDetails = outWardReturnGoodsLRDetails;
	}*/
	
}
