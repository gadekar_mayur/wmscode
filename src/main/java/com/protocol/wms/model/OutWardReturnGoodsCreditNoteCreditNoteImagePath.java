package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="outwardreturngoodscreditnotecreditnoteimagepath")
public class OutWardReturnGoodsCreditNoteCreditNoteImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardReturnGoodsCreditNoteCreditNoteImagePathId;
	
	private String imagePath;
	
	@ManyToOne 
	@JoinColumn(name="outWardReturnGoodsCreditNoteId")
	private OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote;

	public Integer getOutWardReturnGoodsCreditNoteCreditNoteImagePathId() {
		return outWardReturnGoodsCreditNoteCreditNoteImagePathId;
	}

	public void setOutWardReturnGoodsCreditNoteCreditNoteImagePathId(
			Integer outWardReturnGoodsCreditNoteCreditNoteImagePathId) {
		this.outWardReturnGoodsCreditNoteCreditNoteImagePathId = outWardReturnGoodsCreditNoteCreditNoteImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public OutWardReturnGoodsCreditNote getOutWardReturnGoodsCreditNote() {
		return outWardReturnGoodsCreditNote;
	}

	public void setOutWardReturnGoodsCreditNote(
			OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote) {
		this.outWardReturnGoodsCreditNote = outWardReturnGoodsCreditNote;
	}
}
