/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardorderentryregistration")
public class OutWardOrderEntryRegistration {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardOrderEnteryRegistrationId;
	
	private String orderNo;
	
	
	private Date orderDate;
	
	private java.util.Date submitDate;
	
	@Type(type="text")
	private String remark;
	
	private Integer status;
	
	private Integer invoiceStatus;
	
	private Integer productStatus;
	
	private String orderId;
	private Integer companyIndex;

	
	
//	private String orderCopyPath;
	
	@ManyToOne
	@JoinColumn(name="orderModeId")
	private OrderMode orderMode;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@OneToMany(targetEntity=OutWardProductOrderEntryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardOrderEnteryRegistrationId",referencedColumnName="outWardOrderEnteryRegistrationId")
	private List<OutWardProductOrderEntryRegistration> outWardProductOrderEnteryRegistrations;
	
	@OneToMany(targetEntity=OutWardOrderInvoiceEnteryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardOrderEnteryRegistrationId",referencedColumnName="outWardOrderEnteryRegistrationId")
	private List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistration;

	@OneToMany(targetEntity=OutWardOrderEntryRegistrationOrderCopyPath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardOrderEnteryRegistrationId",referencedColumnName="outWardOrderEnteryRegistrationId")
	private List<OutWardOrderEntryRegistrationOrderCopyPath> outWardOrderEntryRegistrationOrderCopyPathList;
	
	public Integer getOutWardOrderEnteryRegistrationId() {
		return outWardOrderEnteryRegistrationId;
	}

	public void setOutWardOrderEnteryRegistrationId(
			Integer outWardOrderEnteryRegistrationId) {
		this.outWardOrderEnteryRegistrationId = outWardOrderEnteryRegistrationId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	

	public Integer getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(Integer invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public OrderMode getOrderMode() {
		return orderMode;
	}

	public void setOrderMode(OrderMode orderMode) {
		this.orderMode = orderMode;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Stockist getStockist() {
		return stockist;
	}

	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	public List<OutWardProductOrderEntryRegistration> getOutWardProductOrderEnteryRegistrations() {
		return outWardProductOrderEnteryRegistrations;
	}

	public void setOutWardProductOrderEnteryRegistrations(
			List<OutWardProductOrderEntryRegistration> outWardProductOrderEnteryRegistrations) {
		this.outWardProductOrderEnteryRegistrations = outWardProductOrderEnteryRegistrations;
	}

	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistration() {
		return outWardOrderInvoiceEnteryRegistration;
	}

	public void setOutWardOrderInvoiceEnteryRegistration(
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistration) {
		this.outWardOrderInvoiceEnteryRegistration = outWardOrderInvoiceEnteryRegistration;
	}

	public Integer getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(Integer productStatus) {
		this.productStatus = productStatus;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public List<OutWardOrderEntryRegistrationOrderCopyPath> getOutWardOrderEntryRegistrationOrderCopyPathList() {
		return outWardOrderEntryRegistrationOrderCopyPathList;
	}

	public void setOutWardOrderEntryRegistrationOrderCopyPathList(
			List<OutWardOrderEntryRegistrationOrderCopyPath> outWardOrderEntryRegistrationOrderCopyPathList) {
		this.outWardOrderEntryRegistrationOrderCopyPathList = outWardOrderEntryRegistrationOrderCopyPathList;
	}

//	public String getOrderIdBasedOnCompany() {
//		return orderIdBasedOnCompany;
//	}
//
//	public void setOrderIdBasedOnCompany(String orderIdBasedOnCompany) {
//		this.orderIdBasedOnCompany = orderIdBasedOnCompany;
//	}

	public Integer getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(Integer companyIndex) {
		this.companyIndex = companyIndex;
	}
	
}
