package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="inwardreturngoodsregistrationlrdetailsclaimcopypath")
public class InwardReturnGoodsRegistrationLRDetailsClaimCopyPath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer inwardReturnGoodsRegistrationLRDetailsClaimCopyPathId ;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="InwardReturnGoodsRegistrationLRDetailsId")
	private InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails;

	public Integer getInwardReturnGoodsRegistrationLRDetailsClaimCopyPathId() {
		return inwardReturnGoodsRegistrationLRDetailsClaimCopyPathId;
	}

	public void setInwardReturnGoodsRegistrationLRDetailsClaimCopyPathId(
			Integer inwardReturnGoodsRegistrationLRDetailsClaimCopyPathId) {
		this.inwardReturnGoodsRegistrationLRDetailsClaimCopyPathId = inwardReturnGoodsRegistrationLRDetailsClaimCopyPathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public InwardReturnGoodsRegistrationLRDetails getInwardReturnGoodsRegistrationLRDetails() {
		return inwardReturnGoodsRegistrationLRDetails;
	}

	public void setInwardReturnGoodsRegistrationLRDetails(
			InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails) {
		this.inwardReturnGoodsRegistrationLRDetails = inwardReturnGoodsRegistrationLRDetails;
	}
	
}
