/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="companycreditanddiscount")
public class CompanyCreditAndDiscount {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer companyCreditAndDiscountId;
	private Integer localDays;
	private Float localPercentage;
	private Integer outsideStationDays;
	private Float outsideStationPercentage;
	private Date submitDate;
	private Integer status;
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;

	/**
	 * @return the companyCreditAndDiscountId
	 */
	public Integer getCompanyCreditAndDiscountId() {
		return companyCreditAndDiscountId;
	}

	/**
	 * @param companyCreditAndDiscountId the companyCreditAndDiscountId to set
	 */
	public void setCompanyCreditAndDiscountId(Integer companyCreditAndDiscountId) {
		this.companyCreditAndDiscountId = companyCreditAndDiscountId;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the localDays
	 */
	public Integer getLocalDays() {
		return localDays;
	}

	/**
	 * @param localDays the localDays to set
	 */
	public void setLocalDays(Integer localDays) {
		this.localDays = localDays;
	}

	/**
	 * @return the localPercentage
	 */
	public Float getLocalPercentage() {
		return localPercentage;
	}

	/**
	 * @param localPercentage the localPercentage to set
	 */
	public void setLocalPercentage(Float localPercentage) {
		this.localPercentage = localPercentage;
	}

	/**
	 * @return the outsideStationDays
	 */
	public Integer getOutsideStationDays() {
		return outsideStationDays;
	}

	/**
	 * @param outsideStationDays the outsideStationDays to set
	 */
	public void setOutsideStationDays(Integer outsideStationDays) {
		this.outsideStationDays = outsideStationDays;
	}

	/**
	 * @return the outsideStationPercentage
	 */
	public Float getOutsideStationPercentage() {
		return outsideStationPercentage;
	}

	/**
	 * @param outsideStationPercentage the outsideStationPercentage to set
	 */
	public void setOutsideStationPercentage(Float outsideStationPercentage) {
		this.outsideStationPercentage = outsideStationPercentage;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
