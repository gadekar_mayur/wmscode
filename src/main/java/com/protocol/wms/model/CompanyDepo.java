/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="companydepo")
public class CompanyDepo {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer companyDepoId;
	
	private String depoName;
	
	private String contactPerson;
	
	private String designation;
	
	private String address;
	
	private Long telePhoneNo;
	
	private Long mobileNo;
	
	private String emailId;
	
	private String webSite;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="cityId")
	private City city;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private State state;
	
	@ManyToOne
	@JoinColumn(name="districtId")
	private District district;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@OneToMany(targetEntity=InwardGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyDepoId",referencedColumnName="companyDepoId")
	private List<InwardGoodsRegistration> inwardGoodsRegistrationList;

	@OneToMany(targetEntity=OutWardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyDepoId",referencedColumnName="companyDepoId")
	private List<OutWardCourierRegistration> outWardCourierRegistrations;
	
	/**
	 * @return the companyDepoId
	 */
	public Integer getCompanyDepoId() {
		return companyDepoId;
	}

	/**
	 * @param companyDepoId the companyDepoId to set
	 */
	public void setCompanyDepoId(Integer companyDepoId) {
		this.companyDepoId = companyDepoId;
	}

	/**
	 * @return the depoName
	 */
	public String getDepoName() {
		return depoName;
	}

	/**
	 * @param depoName the depoName to set
	 */
	public void setDepoName(String depoName) {
		this.depoName = depoName;
	}

	/**
	 * @return the contactPerson
	 */
	public String getContactPerson() {
		return contactPerson;
	}

	/**
	 * @param contactPerson the contactPerson to set
	 */
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the telePhoneNo
	 */
	public Long getTelePhoneNo() {
		return telePhoneNo;
	}

	/**
	 * @param telePhoneNo the telePhoneNo to set
	 */
	public void setTelePhoneNo(Long telePhoneNo) {
		this.telePhoneNo = telePhoneNo;
	}

	/**
	 * @return the mobileNo
	 */
	public Long getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo the mobileNo to set
	 */
	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the webSite
	 */
	public String getWebSite() {
		return webSite;
	}

	/**
	 * @param webSite the webSite to set
	 */
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the city
	 */
	public City getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(City city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @return the district
	 */
	public District getDistrict() {
		return district;
	}

	/**
	 * @param district the district to set
	 */
	public void setDistrict(District district) {
		this.district = district;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
}
