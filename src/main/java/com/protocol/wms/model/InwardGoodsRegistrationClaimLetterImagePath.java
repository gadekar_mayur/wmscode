/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="inwardgoodsregistrationclaimletterimagepath")
public class InwardGoodsRegistrationClaimLetterImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer InwardGoodsRegistrationClaimLetterImagePathId ;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="inwardGoodsRegistrationClaimLetterId")
	private InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetter;

	/**
	 * @return the inwardGoodsRegistrationClaimLetterImagePathId
	 */
	public Integer getInwardGoodsRegistrationClaimLetterImagePathId() {
		return InwardGoodsRegistrationClaimLetterImagePathId;
	}

	/**
	 * @param inwardGoodsRegistrationClaimLetterImagePathId the inwardGoodsRegistrationClaimLetterImagePathId to set
	 */
	public void setInwardGoodsRegistrationClaimLetterImagePathId(
			Integer inwardGoodsRegistrationClaimLetterImagePathId) {
		InwardGoodsRegistrationClaimLetterImagePathId = inwardGoodsRegistrationClaimLetterImagePathId;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the inwardGoodsRegistrationClaimLetter
	 */
	public InwardGoodsRegistrationClaimLetter getInwardGoodsRegistrationClaimLetter() {
		return inwardGoodsRegistrationClaimLetter;
	}

	/**
	 * @param inwardGoodsRegistrationClaimLetter the inwardGoodsRegistrationClaimLetter to set
	 */
	public void setInwardGoodsRegistrationClaimLetter(
			InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetter) {
		this.inwardGoodsRegistrationClaimLetter = inwardGoodsRegistrationClaimLetter;
	}
	
	
}
