/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardadvancedchequeinformation")
public class OutWardAdvancedChequeInformation {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardAdvancedChequeInformationId;
	
	private Date courierReceivedDate;
	
	private Integer noOfCheque;
	
	private java.util.Date submitDate;
	
	private Integer status;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="organizationBankId")
	private OrganizationBank organizationBank;
	

	@OneToMany(targetEntity=OutWardChequeNumberInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardAdvancedChequeInformationId",referencedColumnName="outWardAdvancedChequeInformationId")
	private List<OutWardChequeNumberInformation> outWardChequeNumberInformation;


	public Integer getOutWardAdvancedChequeInformationId() {
		return outWardAdvancedChequeInformationId;
	}


	public void setOutWardAdvancedChequeInformationId(
			Integer outWardAdvancedChequeInformationId) {
		this.outWardAdvancedChequeInformationId = outWardAdvancedChequeInformationId;
	}


	public Date getCourierReceivedDate() {
		return courierReceivedDate;
	}


	public void setCourierReceivedDate(Date courierReceivedDate) {
		this.courierReceivedDate = courierReceivedDate;
	}


	public Integer getNoOfCheque() {
		return noOfCheque;
	}


	public void setNoOfCheque(Integer noOfCheque) {
		this.noOfCheque = noOfCheque;
	}


	public java.util.Date getSubmitDate() {
		return submitDate;
	}


	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}


	public Stockist getStockist() {
		return stockist;
	}


	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}


	public OrganizationBank getOrganizationBank() {
		return organizationBank;
	}


	public void setOrganizationBank(OrganizationBank organizationBank) {
		this.organizationBank = organizationBank;
	}


	public List<OutWardChequeNumberInformation> getOutWardChequeNumberInformation() {
		return outWardChequeNumberInformation;
	}


	public void setOutWardChequeNumberInformation(
			List<OutWardChequeNumberInformation> outWardChequeNumberInformation) {
		this.outWardChequeNumberInformation = outWardChequeNumberInformation;
	}
	
	
	
}
