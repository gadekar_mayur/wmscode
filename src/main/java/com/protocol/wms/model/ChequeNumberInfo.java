/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="chequenumberinfo")
public class ChequeNumberInfo {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer chequeNumberInfoId;
	
	private String chequeNumber;
	
	private Integer status;
	
	private Integer depositStatus;
	
	@ManyToOne
	@JoinColumn(name="advancedChequeInformationId")
	private AdvancedChequeInformation advancedChequeInformation;

	@OneToMany(targetEntity=StockistChequeDeposit.class,cascade=CascadeType.ALL)
	@JoinColumn(name="chequeNumberInfoId",referencedColumnName="chequeNumberInfoId")
	private List<StockistChequeDeposit> stockistChequeDepositList;
	
	@OneToMany(targetEntity=InvoicePaymentInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="chequeNumberInfoId",referencedColumnName="chequeNumberInfoId")
	private List<InvoicePaymentInformation> invoicePaymentInformationList;
	
	@OneToMany(targetEntity=RemainingChequeAmount.class,cascade=CascadeType.ALL)
	@JoinColumn(name="chequeNumberInfoId",referencedColumnName="chequeNumberInfoId")
	private List<RemainingChequeAmount> remainingChequeAmountList;
	
	/**
	 * @return the chequeNumberInfoId
	 */
	public Integer getChequeNumberInfoId() {
		return chequeNumberInfoId;
	}

	/**
	 * @param chequeNumberInfoId the chequeNumberInfoId to set
	 */
	public void setChequeNumberInfoId(Integer chequeNumberInfoId) {
		this.chequeNumberInfoId = chequeNumberInfoId;
	}

	/**
	 * @return the chequeNumber
	 */
	public String getChequeNumber() {
		return chequeNumber;
	}

	/**
	 * @param chequeNumber the chequeNumber to set
	 */
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the advancedChequeInformation
	 */
	public AdvancedChequeInformation getAdvancedChequeInformation() {
		return advancedChequeInformation;
	}

	/**
	 * @param advancedChequeInformation the advancedChequeInformation to set
	 */
	public void setAdvancedChequeInformation(
			AdvancedChequeInformation advancedChequeInformation) {
		this.advancedChequeInformation = advancedChequeInformation;
	}

	public Integer getDepositStatus() {
		return depositStatus;
	}

	public void setDepositStatus(Integer depositStatus) {
		this.depositStatus = depositStatus;
	}

	public List<StockistChequeDeposit> getStockistChequeDepositList() {
		return stockistChequeDepositList;
	}

	public void setStockistChequeDepositList(
			List<StockistChequeDeposit> stockistChequeDepositList) {
		this.stockistChequeDepositList = stockistChequeDepositList;
	}

	public List<InvoicePaymentInformation> getInvoicePaymentInformationList() {
		return invoicePaymentInformationList;
	}

	public void setInvoicePaymentInformationList(
			List<InvoicePaymentInformation> invoicePaymentInformationList) {
		this.invoicePaymentInformationList = invoicePaymentInformationList;
	}

	public List<RemainingChequeAmount> getRemainingChequeAmountList() {
		return remainingChequeAmountList;
	}

	public void setRemainingChequeAmountList(
			List<RemainingChequeAmount> remainingChequeAmountList) {
		this.remainingChequeAmountList = remainingChequeAmountList;
	}
	
	
}
