package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="cartingagentimagepath")
public class CartingAgentImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cartingAgentImagePathId;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="cartingAgentId")
	private CartingAgent cartingAgent;

	public Integer getCartingAgentImagePathId() {
		return cartingAgentImagePathId;
	}

	public void setCartingAgentImagePathId(Integer cartingAgentImagePathId) {
		this.cartingAgentImagePathId = cartingAgentImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public CartingAgent getCartingAgent() {
		return cartingAgent;
	}

	public void setCartingAgent(CartingAgent cartingAgent) {
		this.cartingAgent = cartingAgent;
	}
}
