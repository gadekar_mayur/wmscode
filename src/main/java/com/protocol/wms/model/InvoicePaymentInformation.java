/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="invoicepaymentinformation")
public class InvoicePaymentInformation {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer invoicePaymentInformationId;
	
	private Double paidAmount;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="outWardOrderInvoiceEnteryRegistrationId")
	private OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistration;
	
	@ManyToOne
	@JoinColumn(name="chequeNumberInfoId")
	private ChequeNumberInfo chequeNumberInfo;

	public Integer getInvoicePaymentInformationId() {
		return invoicePaymentInformationId;
	}

	public void setInvoicePaymentInformationId(Integer invoicePaymentInformationId) {
		this.invoicePaymentInformationId = invoicePaymentInformationId;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public OutWardOrderInvoiceEnteryRegistration getOutWardOrderInvoiceEnteryRegistration() {
		return outWardOrderInvoiceEnteryRegistration;
	}

	public void setOutWardOrderInvoiceEnteryRegistration(
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistration) {
		this.outWardOrderInvoiceEnteryRegistration = outWardOrderInvoiceEnteryRegistration;
	}

	public ChequeNumberInfo getChequeNumberInfo() {
		return chequeNumberInfo;
	}

	public void setChequeNumberInfo(ChequeNumberInfo chequeNumberInfo) {
		this.chequeNumberInfo = chequeNumberInfo;
	}
	
	
}
