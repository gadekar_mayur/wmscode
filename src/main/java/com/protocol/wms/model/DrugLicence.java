/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="druglicence")
public class DrugLicence {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer DrugLicenceId;
	
	private String drugLicenceNo;
	
	private Date fromDate;
	
	private Date toDate;
	
	private String validity;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;

	/**
	 * @return the drugLicenceId
	 */
	public Integer getDrugLicenceId() {
		return DrugLicenceId;
	}

	/**
	 * @param drugLicenceId the drugLicenceId to set
	 */
	public void setDrugLicenceId(Integer drugLicenceId) {
		DrugLicenceId = drugLicenceId;
	}

	/**
	 * @return the drugLicenceNo
	 */
	public String getDrugLicenceNo() {
		return drugLicenceNo;
	}

	/**
	 * @param drugLicenceNo the drugLicenceNo to set
	 */
	public void setDrugLicenceNo(String drugLicenceNo) {
		this.drugLicenceNo = drugLicenceNo;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the stockist
	 */
	public Stockist getStockist() {
		return stockist;
	}

	/**
	 * @param stockist the stockist to set
	 */
	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	/**
	 * @return the validity
	 */
	public String getValidity() {
		return validity;
	}

	/**
	 * @param validity the validity to set
	 */
	public void setValidity(String validity) {
		this.validity = validity;
	}

}
