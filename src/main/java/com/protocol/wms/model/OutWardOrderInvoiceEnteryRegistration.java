/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardorderinvoiceenteryregistration")
public class OutWardOrderInvoiceEnteryRegistration {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardOrderInvoiceEnteryRegistrationId;
	
	private String invoiceNo;
	
	private Date invoiceDate;
	
	private Double grossAmount;
	
	private Double taxAmout;
	
	private Double vatAmount;
	
	private Double netAmount;
	
	private String discountIsTakenOrNot;
	
	private Integer noOfDays;
	
	private Integer dispatchEnteryRegistrationStatus;
	
	private java.util.Date submitDate;
	
	private Date firstDate;
	
	private Date lastDate;
	
	private Integer status;
	
	private Integer paymentStatus;
	
	private Double remainingPayment;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="outWardDispatchEnteryRegistrationId",referencedColumnName="outWardDispatchEnteryRegistrationId")
	private OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistration;
	
	@ManyToOne
	@JoinColumn(name="outWardOrderEnteryRegistrationId")
	private OutWardOrderEntryRegistration outWardOrderEnteryRegistration;

	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@OneToMany(targetEntity=OutWardDispatchEnteryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardOrderInvoiceEnteryRegistrationId",referencedColumnName="outWardOrderInvoiceEnteryRegistrationId")
	private List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList;
	
	
	public List<OutWardDispatchEnteryRegistration> getOutWardDispatchEnteryRegistrationList() {
		return outWardDispatchEnteryRegistrationList;
	}

	public void setOutWardDispatchEnteryRegistrationList(
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList) {
		this.outWardDispatchEnteryRegistrationList = outWardDispatchEnteryRegistrationList;
	}

	@OneToMany(targetEntity=InvoicePaymentInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="outWardOrderInvoiceEnteryRegistrationId",referencedColumnName="outWardOrderInvoiceEnteryRegistrationId")
	private List<InvoicePaymentInformation> invoicePaymentInformationList;
	
	public Integer getOutWardOrderInvoiceEnteryRegistrationId() {
		return outWardOrderInvoiceEnteryRegistrationId;
	}

	public void setOutWardOrderInvoiceEnteryRegistrationId(
			Integer outWardOrderInvoiceEnteryRegistrationId) {
		this.outWardOrderInvoiceEnteryRegistrationId = outWardOrderInvoiceEnteryRegistrationId;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Double getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(Double grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Double getTaxAmout() {
		return taxAmout;
	}

	public void setTaxAmout(Double taxAmout) {
		this.taxAmout = taxAmout;
	}

	public Double getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(Double vatAmount) {
		this.vatAmount = vatAmount;
	}

	public Double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}

	public String getDiscountIsTakenOrNot() {
		return discountIsTakenOrNot;
	}

	public void setDiscountIsTakenOrNot(String discountIsTakenOrNot) {
		this.discountIsTakenOrNot = discountIsTakenOrNot;
	}

	

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Integer getDispatchEnteryRegistrationStatus() {
		return dispatchEnteryRegistrationStatus;
	}

	public void setDispatchEnteryRegistrationStatus(
			Integer dispatchEnteryRegistrationStatus) {
		this.dispatchEnteryRegistrationStatus = dispatchEnteryRegistrationStatus;
	}

	public OutWardDispatchEnteryRegistration getOutWardDispatchEnteryRegistration() {
		return outWardDispatchEnteryRegistration;
	}

	public void setOutWardDispatchEnteryRegistration(
			OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistration) {
		this.outWardDispatchEnteryRegistration = outWardDispatchEnteryRegistration;
	}

	public OutWardOrderEntryRegistration getOutWardOrderEnteryRegistration() {
		return outWardOrderEnteryRegistration;
	}

	public void setOutWardOrderEnteryRegistration(
			OutWardOrderEntryRegistration outWardOrderEnteryRegistration) {
		this.outWardOrderEnteryRegistration = outWardOrderEnteryRegistration;
	}

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public Date getFirstDate() {
		return firstDate;
	}

	public void setFirstDate(Date firstDate) {
		this.firstDate = firstDate;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Integer paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Double getRemainingPayment() {
		return remainingPayment;
	}

	public void setRemainingPayment(Double remainingPayment) {
		this.remainingPayment = remainingPayment;
	}

	public List<InvoicePaymentInformation> getInvoicePaymentInformationList() {
		return invoicePaymentInformationList;
	}

	public void setInvoicePaymentInformationList(
			List<InvoicePaymentInformation> invoicePaymentInformationList) {
		this.invoicePaymentInformationList = invoicePaymentInformationList;
	}

	
	
}
