/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="ordermode")
public class OrderMode {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer orderModeId;
	
	private String orderModeName;
	
	private Integer status;
	
	@OneToMany(targetEntity=OutWardOrderEntryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="orderModeId",referencedColumnName="orderModeId")
	private List<OutWardOrderEntryRegistration> outWardOrderEnteryRegistrations;

	public Integer getOrderModeId() {
		return orderModeId;
	}

	public void setOrderModeId(Integer orderModeId) {
		this.orderModeId = orderModeId;
	}

	public String getOrderModeName() {
		return orderModeName;
	}

	public void setOrderModeName(String orderModeName) {
		this.orderModeName = orderModeName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<OutWardOrderEntryRegistration> getOutWardOrderEnteryRegistrations() {
		return outWardOrderEnteryRegistrations;
	}

	public void setOutWardOrderEnteryRegistrations(
			List<OutWardOrderEntryRegistration> outWardOrderEnteryRegistrations) {
		this.outWardOrderEnteryRegistrations = outWardOrderEnteryRegistrations;
	}
	
	
}
