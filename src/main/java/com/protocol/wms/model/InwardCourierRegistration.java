/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="inwardcourierregistration")
public class InwardCourierRegistration {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer InwardCourierRegistrationId;
	
	private String docketNo;
	
	//private String docketFileImagePath;
	
	@Type(type="text")
	private String remark;
	
	private String courierName;
	
	private String deliveryPerson;
	
	private Time time;
	
	private Date courierRegistrationDate;
	
	private java.util.Date submitDate;
	
	private Integer status;
	
	private String other;
	
	@ManyToOne
	@JoinColumn(name="particularsId")
	private Particulars particulars;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private State state;
	
	@ManyToOne
	@JoinColumn(name="districtId")
	private District district;
	
	@ManyToOne
	@JoinColumn(name="cityId")
	private City city;
	
	@OneToMany(targetEntity=InwardCourierRegistrationDocketFilePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="InwardCourierRegistrationId",referencedColumnName="InwardCourierRegistrationId")
	private List<InwardCourierRegistrationDocketFilePath> inwardCourierRegistrationDocketFilePathList;
	
	/*@OneToOne
	@JoinColumn(name="advancedChequeInformationId")
	private AdvancedChequeInformation AdvancedChequeInformation;*/
	
	public Integer getInwardCourierRegistrationId() {
		return InwardCourierRegistrationId;
	}

	public void setInwardCourierRegistrationId(Integer inwardCourierRegistrationId) {
		InwardCourierRegistrationId = inwardCourierRegistrationId;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	/*public String getDocketFileImagePath() {
		return docketFileImagePath;
	}

	public void setDocketFileImagePath(String docketFileImagePath) {
		this.docketFileImagePath = docketFileImagePath;
	}
*/
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCourierName() {
		return courierName;
	}

	public void setCourierName(String courierName) {
		this.courierName = courierName;
	}

	public String getDeliveryPerson() {
		return deliveryPerson;
	}

	public void setDeliveryPerson(String deliveryPerson) {
		this.deliveryPerson = deliveryPerson;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Date getCourierRegistrationDate() {
		return courierRegistrationDate;
	}

	public void setCourierRegistrationDate(Date courierRegistrationDate) {
		this.courierRegistrationDate = courierRegistrationDate;
	}

	/**
	 * @return the particulars
	 */
	public Particulars getParticulars() {
		return particulars;
	}

	/**
	 * @param particulars the particulars to set
	 */
	public void setParticulars(Particulars particulars) {
		this.particulars = particulars;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Stockist getStockist() {
		return stockist;
	}

	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	/**
	 * @return the submitDate
	 */
	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

/*	*//**
	 * @return the advancedChequeInformation
	 *//*
	public AdvancedChequeInformation getAdvancedChequeInformation() {
		return AdvancedChequeInformation;
	}

	*//**
	 * @param advancedChequeInformation the advancedChequeInformation to set
	 *//*
	public void setAdvancedChequeInformation(
			AdvancedChequeInformation advancedChequeInformation) {
		AdvancedChequeInformation = advancedChequeInformation;
	}*/

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}

	/**
	 * @param other the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}

	public List<InwardCourierRegistrationDocketFilePath> getInwardCourierRegistrationDocketFilePathList() {
		return inwardCourierRegistrationDocketFilePathList;
	}

	public void setInwardCourierRegistrationDocketFilePathList(
			List<InwardCourierRegistrationDocketFilePath> inwardCourierRegistrationDocketFilePathList) {
		this.inwardCourierRegistrationDocketFilePathList = inwardCourierRegistrationDocketFilePathList;
	}


}
