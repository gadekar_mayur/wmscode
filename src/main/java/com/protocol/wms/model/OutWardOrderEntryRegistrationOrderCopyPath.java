package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="outwardorderentryregistrationordercopypath")
public class OutWardOrderEntryRegistrationOrderCopyPath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardOrderEntryRegistrationOrderCopyPathId;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="outWardOrderEnteryRegistrationId")
	private OutWardOrderEntryRegistration outWardOrderEntryRegistration;

	public Integer getOutWardOrderEntryRegistrationOrderCopyPathId() {
		return outWardOrderEntryRegistrationOrderCopyPathId;
	}

	public void setOutWardOrderEntryRegistrationOrderCopyPathId(
			Integer outWardOrderEntryRegistrationOrderCopyPathId) {
		this.outWardOrderEntryRegistrationOrderCopyPathId = outWardOrderEntryRegistrationOrderCopyPathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public OutWardOrderEntryRegistration getOutWardOrderEntryRegistration() {
		return outWardOrderEntryRegistration;
	}

	public void setOutWardOrderEntryRegistration(
			OutWardOrderEntryRegistration outWardOrderEntryRegistration) {
		this.outWardOrderEntryRegistration = outWardOrderEntryRegistration;
	}
}
