package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="creditnotecreditcoteimagepath")
public class CreditNoteCreditNoteImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer creditNoteCreditNoteImagePathId ;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="creditNoteId")
	private CreditNote creditNote;

	public Integer getCreditNoteCreditNoteImagePathId() {
		return creditNoteCreditNoteImagePathId;
	}

	public void setCreditNoteCreditNoteImagePathId(
			Integer creditNoteCreditNoteImagePathId) {
		this.creditNoteCreditNoteImagePathId = creditNoteCreditNoteImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public CreditNote getCreditNote() {
		return creditNote;
	}

	public void setCreditNote(CreditNote creditNote) {
		this.creditNote = creditNote;
	}
}
