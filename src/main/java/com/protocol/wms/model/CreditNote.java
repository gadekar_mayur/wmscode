/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="creditnote")
public class CreditNote {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer creditNoteId;
	
	private String creditNoteNo;
	
	private Float creditNoteAmount;
	
	private Date submitDate;
	
	private Integer status;
	
	@Type(type="text")
	private String remark;
	
	private String creditNoteImagePath;
	
	@OneToMany(targetEntity=CreditNoteCreditNoteImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="creditNoteId",referencedColumnName="creditNoteId")
	private List<CreditNoteCreditNoteImagePath> creditNoteCreditNoteImagePathList;
	
//	@ManyToOne@JoinColumn(name="inwardReturnGoodRegistrationId")
//	private InwardReturnGoodsRegistration inwardReturnGoodsRegistration;

	public Integer getCreditNoteId() {
		return creditNoteId;
	}

	public void setCreditNoteId(Integer creditNoteId) {
		this.creditNoteId = creditNoteId;
	}

	public String getCreditNoteNo() {
		return creditNoteNo;
	}

	public void setCreditNoteNo(String creditNoteNo) {
		this.creditNoteNo = creditNoteNo;
	}

	public Float getCreditNoteAmount() {
		return creditNoteAmount;
	}

	public void setCreditNoteAmount(Float creditNoteAmount) {
		this.creditNoteAmount = creditNoteAmount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreditNoteImagePath() {
		return creditNoteImagePath;
	}

	public void setCreditNoteImagePath(String creditNoteImagePath) {
		this.creditNoteImagePath = creditNoteImagePath;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<CreditNoteCreditNoteImagePath> getCreditNoteCreditNoteImagePathList() {
		return creditNoteCreditNoteImagePathList;
	}

	public void setCreditNoteCreditNoteImagePathList(
			List<CreditNoteCreditNoteImagePath> creditNoteCreditNoteImagePathList) {
		this.creditNoteCreditNoteImagePathList = creditNoteCreditNoteImagePathList;
	}

//	public InwardReturnGoodsRegistration getInwardReturnGoodsRegistration() {
//		return inwardReturnGoodsRegistration;
//	}
//
//	public void setInwardReturnGoodsRegistration(
//			InwardReturnGoodsRegistration inwardReturnGoodsRegistration) {
//		this.inwardReturnGoodsRegistration = inwardReturnGoodsRegistration;
//	}
	
	

}
