package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="checkingdonecheckingslipimagepath")
public class CheckingDoneCheckingSlipImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer checkingDoneCheckingSlipImagePathId ;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="CheckingDoneId")
	private CheckingDone checkingDone;

	
	public Integer getCheckingDoneCheckingSlipImagePathId() {
		return checkingDoneCheckingSlipImagePathId;
	}

	public void setCheckingDoneCheckingSlipImagePathId(
			Integer checkingDoneCheckingSlipImagePathId) {
		this.checkingDoneCheckingSlipImagePathId = checkingDoneCheckingSlipImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public CheckingDone getCheckingDone() {
		return checkingDone;
	}

	public void setCheckingDone(CheckingDone checkingDone) {
		this.checkingDone = checkingDone;
	}
}
