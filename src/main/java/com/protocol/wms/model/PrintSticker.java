/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="printsticker")
public class PrintSticker {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer printStickerId;
	
	private String stockist;
	
	private String mobileNo;
	
	private String transporter;
	
	private String invoiceNo;
	
	private Integer noOfCases;
	
	private String orgnaization;
	
	@Type(type="text")
	private String stockistAddress;
	
	private String stockistCity;
	
	@OneToOne
	@JoinColumn(name="outWardDispatchEnteryRegistrationId")
	private OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistration;

	public Integer getPrintStickerId() {
		return printStickerId;
	}

	public void setPrintStickerId(Integer printStickerId) {
		this.printStickerId = printStickerId;
	}

	public String getStockist() {
		return stockist;
	}

	public void setStockist(String stockist) {
		this.stockist = stockist;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getTransporter() {
		return transporter;
	}

	public void setTransporter(String transporter) {
		this.transporter = transporter;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Integer getNoOfCases() {
		return noOfCases;
	}

	public void setNoOfCases(Integer noOfCases) {
		this.noOfCases = noOfCases;
	}

	public String getStockistAddress() {
		return stockistAddress;
	}

	public void setStockistAddress(String stockistAddress) {
		this.stockistAddress = stockistAddress;
	}

	public OutWardDispatchEnteryRegistration getOutWardDispatchEnteryRegistration() {
		return outWardDispatchEnteryRegistration;
	}

	public void setOutWardDispatchEnteryRegistration(
			OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistration) {
		this.outWardDispatchEnteryRegistration = outWardDispatchEnteryRegistration;
	}

	public String getOrgnaization() {
		return orgnaization;
	}

	public void setOrgnaization(String orgnaization) {
		this.orgnaization = orgnaization;
	}

	public String getStockistCity() {
		return stockistCity;
	}

	public void setStockistCity(String stockistCity) {
		this.stockistCity = stockistCity;
	}
	
	
}
