/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwardcourierregistration")
public class OutWardCourierRegistration {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardCourierRegistrationId;
	
	private Date courierRegistrationDate;
	
	private String docketNo;
	
	private String courierName;
	
	private String weight;
	
	private Integer quantity;
	
	private String deliveryPerson;
	
	private String courierPerson;
	
	private Time time;
	
	@Type(type="text")
	private String remark;
	
	private java.util.Date submitDate;
	
	private Integer status;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private State state;
	
	@ManyToOne
	@JoinColumn(name="districtId")
	private District district;
	
	@ManyToOne
	@JoinColumn(name="cityId")
	private City city;
	
	@ManyToOne
	@JoinColumn(name="companyDepoId")
	private CompanyDepo companyDepo;
	
	@ManyToOne
	@JoinColumn(name="particularsId")
	private Particulars particulars;
	
	@OneToOne
	@JoinColumn(name="outWardAdvancedChequeInformationId")
	private OutWardAdvancedChequeInformation outWardAdvancedChequeInformation;

	public Integer getOutWardCourierRegistrationId() {
		return outWardCourierRegistrationId;
	}

	public void setOutWardCourierRegistrationId(Integer outWardCourierRegistrationId) {
		this.outWardCourierRegistrationId = outWardCourierRegistrationId;
	}

	public Date getCourierRegistrationDate() {
		return courierRegistrationDate;
	}

	public void setCourierRegistrationDate(Date courierRegistrationDate) {
		this.courierRegistrationDate = courierRegistrationDate;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public String getCourierName() {
		return courierName;
	}

	public void setCourierName(String courierName) {
		this.courierName = courierName;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getDeliveryPerson() {
		return deliveryPerson;
	}

	public void setDeliveryPerson(String deliveryPerson) {
		this.deliveryPerson = deliveryPerson;
	}

	public String getCourierPerson() {
		return courierPerson;
	}

	public void setCourierPerson(String courierPerson) {
		this.courierPerson = courierPerson;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public CompanyDepo getCompanyDepo() {
		return companyDepo;
	}

	public void setCompanyDepo(CompanyDepo companyDepo) {
		this.companyDepo = companyDepo;
	}

	public Particulars getParticulars() {
		return particulars;
	}

	public void setParticulars(Particulars particulars) {
		this.particulars = particulars;
	}

	/**
	 * @return the stockist
	 */
	public Stockist getStockist() {
		return stockist;
	}

	/**
	 * @param stockist the stockist to set
	 */
	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	/**
	 * @return the outWardAdvancedChequeInformation
	 */
	public OutWardAdvancedChequeInformation getOutWardAdvancedChequeInformation() {
		return outWardAdvancedChequeInformation;
	}

	/**
	 * @param outWardAdvancedChequeInformation the outWardAdvancedChequeInformation to set
	 */
	public void setOutWardAdvancedChequeInformation(
			OutWardAdvancedChequeInformation outWardAdvancedChequeInformation) {
		this.outWardAdvancedChequeInformation = outWardAdvancedChequeInformation;
	}
	
}
