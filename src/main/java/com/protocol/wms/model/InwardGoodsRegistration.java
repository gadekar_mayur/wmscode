/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="inwardgoodsregistration")
public class InwardGoodsRegistration {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer InwardGoodsRegistrationId;
	
	private Date InwardGoodsRegistrationDate;
	
	private Integer noOfCases;
	
	private String LRNo;
	
	//private String LRImagePath;
	
	private String driverName;
	
	private Float unloadingCharges;
	
	private String unloaderName;
	
	private Time time;
	
	private String receivedBy;
	
	private Float tranportationCharges;
	
	private Float hamaliCharges;
	
	private String remark;
	
	private java.util.Date submitDate;
	
	private Integer status;
	
	private Integer claimLetterStatus;
	
	@OneToOne(mappedBy="inwardGoodsRegistration")
	private InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetter;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="companyDepoId")
	private CompanyDepo companyDepo;
	
	@ManyToOne
	@JoinColumn(name="transporterDetailsId")
	private TransporterDetails transporterDetails;
	
	@OneToMany(targetEntity=InwardGoodsRegistrationInvoiceDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="InwardGoodsRegistrationId",referencedColumnName="InwardGoodsRegistrationId")
	private List<InwardGoodsRegistrationInvoiceDetails> inwardGoodsRegistrationInvoiceDetailList;

	@OneToMany(targetEntity=InwardGoodsRegistrationLRImgPath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="InwardGoodsRegistrationId",referencedColumnName="InwardGoodsRegistrationId")
	private List<InwardGoodsRegistrationLRImgPath> inwardGoodsRegistrationLRImgPathList;
	
	public Integer getInwardGoodsRegistrationId() {
		return InwardGoodsRegistrationId;
	}

	public void setInwardGoodsRegistrationId(Integer inwardGoodsRegistrationId) {
		InwardGoodsRegistrationId = inwardGoodsRegistrationId;
	}

	public Date getInwardGoodsRegistrationDate() {
		return InwardGoodsRegistrationDate;
	}

	public void setInwardGoodsRegistrationDate(Date inwardGoodsRegistrationDate) {
		InwardGoodsRegistrationDate = inwardGoodsRegistrationDate;
	}

	public Integer getNoOfCases() {
		return noOfCases;
	}

	public void setNoOfCases(Integer noOfCases) {
		this.noOfCases = noOfCases;
	}

	public String getLRNo() {
		return LRNo;
	}

	public void setLRNo(String lRNo) {
		LRNo = lRNo;
	}

/*	public String getLRImagePath() {
		return LRImagePath;
	}

	public void setLRImagePath(String lRImagePath) {
		LRImagePath = lRImagePath;
	}*/

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public Float getUnloadingCharges() {
		return unloadingCharges;
	}

	public void setUnloadingCharges(Float unloadingCharges) {
		this.unloadingCharges = unloadingCharges;
	}

	public String getUnloaderName() {
		return unloaderName;
	}

	public void setUnloaderName(String unloaderName) {
		this.unloaderName = unloaderName;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public String getReceivedBy() {
		return receivedBy;
	}

	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}

	public Float getTranportationCharges() {
		return tranportationCharges;
	}

	public void setTranportationCharges(Float tranportationCharges) {
		this.tranportationCharges = tranportationCharges;
	}

	public Float getHamaliCharges() {
		return hamaliCharges;
	}

	public void setHamaliCharges(Float hamaliCharges) {
		this.hamaliCharges = hamaliCharges;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public CompanyDepo getCompanyDepo() {
		return companyDepo;
	}

	public void setCompanyDepo(CompanyDepo companyDepo) {
		this.companyDepo = companyDepo;
	}

	public TransporterDetails getTransporterDetails() {
		return transporterDetails;
	}

	public void setTransporterDetails(TransporterDetails transporterDetails) {
		this.transporterDetails = transporterDetails;
	}

	public List<InwardGoodsRegistrationInvoiceDetails> getInwardGoodsRegistrationInvoiceDetailList() {
		return inwardGoodsRegistrationInvoiceDetailList;
	}

	public void setInwardGoodsRegistrationInvoiceDetailList(
			List<InwardGoodsRegistrationInvoiceDetails> inwardGoodsRegistrationInvoiceDetailList) {
		this.inwardGoodsRegistrationInvoiceDetailList = inwardGoodsRegistrationInvoiceDetailList;
	}

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getClaimLetterStatus() {
		return claimLetterStatus;
	}

	public void setClaimLetterStatus(Integer claimLetterStatus) {
		this.claimLetterStatus = claimLetterStatus;
	}

	public InwardGoodsRegistrationClaimLetter getInwardGoodsRegistrationClaimLetter() {
		return inwardGoodsRegistrationClaimLetter;
	}

	public void setInwardGoodsRegistrationClaimLetter(
			InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetter) {
		this.inwardGoodsRegistrationClaimLetter = inwardGoodsRegistrationClaimLetter;
	}

	/**
	 * @return the inwardGoodsRegistrationLRImgPathList
	 */
	public List<InwardGoodsRegistrationLRImgPath> getInwardGoodsRegistrationLRImgPathList() {
		return inwardGoodsRegistrationLRImgPathList;
	}

	/**
	 * @param inwardGoodsRegistrationLRImgPathList the inwardGoodsRegistrationLRImgPathList to set
	 */
	public void setInwardGoodsRegistrationLRImgPathList(
			List<InwardGoodsRegistrationLRImgPath> inwardGoodsRegistrationLRImgPathList) {
		this.inwardGoodsRegistrationLRImgPathList = inwardGoodsRegistrationLRImgPathList;
	}

}
