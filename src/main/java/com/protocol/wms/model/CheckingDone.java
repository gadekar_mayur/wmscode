/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="checkingdone")
public class CheckingDone {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer CheckingDoneId;
	
	private Date checkingDoneDate;
	
	private String checkingSlipImagePath;
	
	private Time time;
	
	private String remark;
	
	private java.util.Date submitDate;
	
	@ManyToOne@JoinColumn(name="inwardReturnGoodRegistrationId")
	private InwardReturnGoodsRegistration inwardReturnGoodsRegistration;
	
	@ManyToOne@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne@JoinColumn(name="employeeDetailsId")
	private EmployeeDetails employeeDetails;

	@OneToMany(targetEntity=CheckingDoneCheckingSlipImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="CheckingDoneId",referencedColumnName="CheckingDoneId")
	private List<CheckingDoneCheckingSlipImagePath> checkingDoneCheckingSlipImagePathList;
	
	public Integer getCheckingDoneId() {
		return CheckingDoneId;
	}

	public void setCheckingDoneId(Integer checkingDoneId) {
		CheckingDoneId = checkingDoneId;
	}

	public Date getCheckingDoneDate() {
		return checkingDoneDate;
	}

	public void setCheckingDoneDate(Date checkingDoneDate) {
		this.checkingDoneDate = checkingDoneDate;
	}

	public String getCheckingSlipImagePath() {
		return checkingSlipImagePath;
	}

	public void setCheckingSlipImagePath(String checkingSlipImagePath) {
		this.checkingSlipImagePath = checkingSlipImagePath;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public InwardReturnGoodsRegistration getInwardReturnGoodsRegistration() {
		return inwardReturnGoodsRegistration;
	}

	public void setInwardReturnGoodsRegistration(
			InwardReturnGoodsRegistration inwardReturnGoodsRegistration) {
		this.inwardReturnGoodsRegistration = inwardReturnGoodsRegistration;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Stockist getStockist() {
		return stockist;
	}

	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public List<CheckingDoneCheckingSlipImagePath> getCheckingDoneCheckingSlipImagePathList() {
		return checkingDoneCheckingSlipImagePathList;
	}

	public void setCheckingDoneCheckingSlipImagePathList(
			List<CheckingDoneCheckingSlipImagePath> checkingDoneCheckingSlipImagePathList) {
		this.checkingDoneCheckingSlipImagePathList = checkingDoneCheckingSlipImagePathList;
	}
	
	
}
