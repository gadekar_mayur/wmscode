package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="cartingagentuploadeddocumentimagepath")
public class CartingAgentUploadedDocumentImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cartingAgentUploadedDocumentImagePathId ;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="cartingAgentUploadedDocumentId")
	private CartingAgentUploadedDocument cartingAgentUploadedDocument;

	public Integer getCartingAgentUploadedDocumentImagePathId() {
		return cartingAgentUploadedDocumentImagePathId;
	}

	public void setCartingAgentUploadedDocumentImagePathId(
			Integer cartingAgentUploadedDocumentImagePathId) {
		this.cartingAgentUploadedDocumentImagePathId = cartingAgentUploadedDocumentImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public CartingAgentUploadedDocument getCartingAgentUploadedDocument() {
		return cartingAgentUploadedDocument;
	}

	public void setCartingAgentUploadedDocument(
			CartingAgentUploadedDocument cartingAgentUploadedDocument) {
		this.cartingAgentUploadedDocument = cartingAgentUploadedDocument;
	}
	
}
