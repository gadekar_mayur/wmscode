package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="employeedocumentimagepath")
public class EmployeeDocumentImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer employeeDocumentImagePathId;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="employeeDocumentId")
	private EmployeeDocument employeeDocument;

	public Integer getEmployeeDocumentImagePathId() {
		return employeeDocumentImagePathId;
	}

	public void setEmployeeDocumentImagePathId(Integer employeeDocumentImagePathId) {
		this.employeeDocumentImagePathId = employeeDocumentImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public EmployeeDocument getEmployeeDocument() {
		return employeeDocument;
	}

	public void setEmployeeDocument(EmployeeDocument employeeDocument) {
		this.employeeDocument = employeeDocument;
	}
}
