/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="stockistbankdetails")
public class StockistBankDetails {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stockistBankDetailsId;
	
	private String StockistBankName;
	
	private String StockistBankBranchName;
	
	private String StockistBankaccountNo;
	
	private String StockistBankIfscCode;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@OneToMany(targetEntity=AdvancedChequeInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistBankDetailsId",referencedColumnName="stockistBankDetailsId")
	private List<AdvancedChequeInformation> advancedChequeInformationList;

	@OneToMany(targetEntity=StockistChequeDeposit.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistBankDetailsId",referencedColumnName="stockistBankDetailsId")
	private List<StockistChequeDeposit> stockistChequeDepositList;
	
	
	@OneToMany(targetEntity=RemainingChequeAmount.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistBankDetailsId",referencedColumnName="stockistBankDetailsId")
	private List<RemainingChequeAmount> remainingChequeAmountList;
	
	/**
	 * @return the stockistBankDetailsId
	 */
	public Integer getStockistBankDetailsId() {
		return stockistBankDetailsId;
	}

	/**
	 * @param stockistBankDetailsId the stockistBankDetailsId to set
	 */
	public void setStockistBankDetailsId(Integer stockistBankDetailsId) {
		this.stockistBankDetailsId = stockistBankDetailsId;
	}

	/**
	 * @return the stockistBankName
	 */
	public String getStockistBankName() {
		return StockistBankName;
	}

	/**
	 * @param stockistBankName the stockistBankName to set
	 */
	public void setStockistBankName(String stockistBankName) {
		StockistBankName = stockistBankName;
	}

	/**
	 * @return the stockistBankBranchName
	 */
	public String getStockistBankBranchName() {
		return StockistBankBranchName;
	}

	/**
	 * @param stockistBankBranchName the stockistBankBranchName to set
	 */
	public void setStockistBankBranchName(String stockistBankBranchName) {
		StockistBankBranchName = stockistBankBranchName;
	}

	/**
	 * @return the stockistBankaccountNo
	 */
	public String getStockistBankaccountNo() {
		return StockistBankaccountNo;
	}

	/**
	 * @param stockistBankaccountNo the stockistBankaccountNo to set
	 */
	public void setStockistBankaccountNo(String stockistBankaccountNo) {
		StockistBankaccountNo = stockistBankaccountNo;
	}

	/**
	 * @return the stockistBankIfscCode
	 */
	public String getStockistBankIfscCode() {
		return StockistBankIfscCode;
	}

	/**
	 * @param stockistBankIfscCode the stockistBankIfscCode to set
	 */
	public void setStockistBankIfscCode(String stockistBankIfscCode) {
		StockistBankIfscCode = stockistBankIfscCode;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the stockist
	 */
	public Stockist getStockist() {
		return stockist;
	}

	/**
	 * @param stockist the stockist to set
	 */
	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	public List<AdvancedChequeInformation> getAdvancedChequeInformationList() {
		return advancedChequeInformationList;
	}

	public void setAdvancedChequeInformationList(
			List<AdvancedChequeInformation> advancedChequeInformationList) {
		this.advancedChequeInformationList = advancedChequeInformationList;
	}

	public List<StockistChequeDeposit> getStockistChequeDepositList() {
		return stockistChequeDepositList;
	}

	public void setStockistChequeDepositList(
			List<StockistChequeDeposit> stockistChequeDepositList) {
		this.stockistChequeDepositList = stockistChequeDepositList;
	}

	public List<RemainingChequeAmount> getRemainingChequeAmountList() {
		return remainingChequeAmountList;
	}

	public void setRemainingChequeAmountList(
			List<RemainingChequeAmount> remainingChequeAmountList) {
		this.remainingChequeAmountList = remainingChequeAmountList;
	}
	
	
}
