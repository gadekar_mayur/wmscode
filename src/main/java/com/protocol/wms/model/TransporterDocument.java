/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="transporterdocument")
public class TransporterDocument {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer transporterDocumentId;
	
//	private String imagePath;
	
	private Integer status;
	
	private Date submitDate;
	
//	@ManyToOne
//	@JoinColumn(name="companyId")
//	private Company company;
	
	@ManyToOne
	@JoinColumn(name="transporterDetailsId")
	private TransporterDetails transporterDetails;
	
	@ManyToOne
	@JoinColumn(name="documentListTypeId")
	private DocumentListType documentListType;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;

	@OneToMany(targetEntity=TransporterDocumentImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="transporterDocumentId",referencedColumnName="transporterDocumentId")
	private List<TransporterDocumentImagePath> transporterDocumentImagePathList;
	
	/**
	 * @return the transporterDocumentId
	 */
	public Integer getTransporterDocumentId() {
		return transporterDocumentId;
	}

	/**
	 * @param transporterDocumentId the transporterDocumentId to set
	 */
	public void setTransporterDocumentId(Integer transporterDocumentId) {
		this.transporterDocumentId = transporterDocumentId;
	}


	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the transporterDetails
	 */
	public TransporterDetails getTransporterDetails() {
		return transporterDetails;
	}

	/**
	 * @param transporterDetails the transporterDetails to set
	 */
	public void setTransporterDetails(TransporterDetails transporterDetails) {
		this.transporterDetails = transporterDetails;
	}

	/**
	 * @return the documentListType
	 */
	public DocumentListType getDocumentListType() {
		return documentListType;
	}

	/**
	 * @param documentListType the documentListType to set
	 */
	public void setDocumentListType(DocumentListType documentListType) {
		this.documentListType = documentListType;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<TransporterDocumentImagePath> getTransporterDocumentImagePathList() {
		return transporterDocumentImagePathList;
	}

	public void setTransporterDocumentImagePathList(
			List<TransporterDocumentImagePath> transporterDocumentImagePathList) {
		this.transporterDocumentImagePathList = transporterDocumentImagePathList;
	}
}
