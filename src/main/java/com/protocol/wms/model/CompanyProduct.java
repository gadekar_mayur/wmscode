/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="companyproduct")
public class CompanyProduct {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer companyProductId;
	
	private String companyProductName;
	
	private String companyProductCode;
	
	private String companyProductType;
	
	private Double companyProductMRP;
	
	private Double companyProductPTD;
	
	private Integer companyProductBoxSize;
	
	private Integer companyProductUnitSize;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;

	@OneToMany(targetEntity=OutWardProductOrderEntryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyProductId",referencedColumnName="companyProductId")
	private List<OutWardProductOrderEntryRegistration> outWardProductOrderEnteryRegistrations;
	
	
	/**
	 * @return the companyProductId
	 */
	public Integer getCompanyProductId() {
		return companyProductId;
	}

	/**
	 * @param companyProductId the companyProductId to set
	 */
	public void setCompanyProductId(Integer companyProductId) {
		this.companyProductId = companyProductId;
	}

	/**
	 * @return the companyProductName
	 */
	public String getCompanyProductName() {
		return companyProductName;
	}

	/**
	 * @param companyProductName the companyProductName to set
	 */
	public void setCompanyProductName(String companyProductName) {
		this.companyProductName = companyProductName;
	}

	/**
	 * @return the companyProductCode
	 */
	public String getCompanyProductCode() {
		return companyProductCode;
	}

	/**
	 * @param companyProductCode the companyProductCode to set
	 */
	public void setCompanyProductCode(String companyProductCode) {
		this.companyProductCode = companyProductCode;
	}

	/**
	 * @return the companyProductType
	 */
	public String getCompanyProductType() {
		return companyProductType;
	}

	/**
	 * @param companyProductType the companyProductType to set
	 */
	public void setCompanyProductType(String companyProductType) {
		this.companyProductType = companyProductType;
	}

	/**
	 * @return the companyProductMRP
	 */
	public Double getCompanyProductMRP() {
		return companyProductMRP;
	}

	/**
	 * @param companyProductMRP the companyProductMRP to set
	 */
	public void setCompanyProductMRP(Double companyProductMRP) {
		this.companyProductMRP = companyProductMRP;
	}

	/**
	 * @return the companyProductPTD
	 */
	public Double getCompanyProductPTD() {
		return companyProductPTD;
	}

	/**
	 * @param companyProductPTD the companyProductPTD to set
	 */
	public void setCompanyProductPTD(Double companyProductPTD) {
		this.companyProductPTD = companyProductPTD;
	}

	/**
	 * @return the companyProductBoxSize
	 */
	public Integer getCompanyProductBoxSize() {
		return companyProductBoxSize;
	}

	/**
	 * @param companyProductBoxSize the companyProductBoxSize to set
	 */
	public void setCompanyProductBoxSize(Integer companyProductBoxSize) {
		this.companyProductBoxSize = companyProductBoxSize;
	}

	/**
	 * @return the companyProductUnitSize
	 */
	public Integer getCompanyProductUnitSize() {
		return companyProductUnitSize;
	}

	/**
	 * @param companyProductUnitSize the companyProductUnitSize to set
	 */
	public void setCompanyProductUnitSize(Integer companyProductUnitSize) {
		this.companyProductUnitSize = companyProductUnitSize;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
}
