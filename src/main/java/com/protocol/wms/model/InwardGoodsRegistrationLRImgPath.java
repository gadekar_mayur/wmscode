/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="inwardgoodsregistrationlrimgpath")
public class InwardGoodsRegistrationLRImgPath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer inwardGoodsRegistrationLRImgPathId ;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="InwardGoodsRegistrationId")
	private InwardGoodsRegistration inwardGoodsRegistration;

	/**
	 * @return the inwardGoodsRegistrationLRImgPathId
	 */
	public Integer getInwardGoodsRegistrationLRImgPathId() {
		return inwardGoodsRegistrationLRImgPathId;
	}

	/**
	 * @param inwardGoodsRegistrationLRImgPathId the inwardGoodsRegistrationLRImgPathId to set
	 */
	public void setInwardGoodsRegistrationLRImgPathId(
			Integer inwardGoodsRegistrationLRImgPathId) {
		this.inwardGoodsRegistrationLRImgPathId = inwardGoodsRegistrationLRImgPathId;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the inwardGoodsRegistration
	 */
	public InwardGoodsRegistration getInwardGoodsRegistration() {
		return inwardGoodsRegistration;
	}

	/**
	 * @param inwardGoodsRegistration the inwardGoodsRegistration to set
	 */
	public void setInwardGoodsRegistration(
			InwardGoodsRegistration inwardGoodsRegistration) {
		this.inwardGoodsRegistration = inwardGoodsRegistration;
	}
}
