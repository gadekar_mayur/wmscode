package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="outwardgatepasslrimagepath")
public class OutWardGatePassLrImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardGatePassLrImagePathId ;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="outWardGatePassId")
	private OutWardGatePass outWardGatePass;

	public Integer getOutWardGatePassLrImagePathId() {
		return outWardGatePassLrImagePathId;
	}

	public void setOutWardGatePassLrImagePathId(Integer outWardGatePassLrImagePathId) {
		this.outWardGatePassLrImagePathId = outWardGatePassLrImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public OutWardGatePass getOutWardGatePass() {
		return outWardGatePass;
	}

	public void setOutWardGatePass(OutWardGatePass outWardGatePass) {
		this.outWardGatePass = outWardGatePass;
	}
}
