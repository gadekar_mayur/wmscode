/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.util.List;

import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="stockistchequedeposit")
public class StockistChequeDeposit {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stockistChequeDepositId;
	
	private Date depositDate;
	
	@Type(type="text")
	private String remark;
	
	private Double chequeAmount;
	
	private Integer returnStatus;
	
	private java.util.Date submitDate;
	
	private Integer status;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="companyBankId")
	private CompanyBank companyBank;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="stockistBankDetailsId")
	private StockistBankDetails stockistBankDetails;
	
	@ManyToOne
	@JoinColumn(name="chequeNumberInfoId")
	private ChequeNumberInfo chequeNumberInfo;
	
	@OneToMany(targetEntity=StockistChequeReturn.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistChequeDepositId",referencedColumnName="stockistChequeDepositId")
	private List<StockistChequeReturn> stockistChequeReturnList;

	public Integer getStockistChequeDepositId() {
		return stockistChequeDepositId;
	}

	public void setStockistChequeDepositId(Integer stockistChequeDepositId) {
		this.stockistChequeDepositId = stockistChequeDepositId;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Double getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(Double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public Integer getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(Integer returnStatus) {
		this.returnStatus = returnStatus;
	}

	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public CompanyBank getCompanyBank() {
		return companyBank;
	}

	public void setCompanyBank(CompanyBank companyBank) {
		this.companyBank = companyBank;
	}

	public Stockist getStockist() {
		return stockist;
	}

	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	public StockistBankDetails getStockistBankDetails() {
		return stockistBankDetails;
	}

	public void setStockistBankDetails(StockistBankDetails stockistBankDetails) {
		this.stockistBankDetails = stockistBankDetails;
	}

	public ChequeNumberInfo getChequeNumberInfo() {
		return chequeNumberInfo;
	}

	public void setChequeNumberInfo(ChequeNumberInfo chequeNumberInfo) {
		this.chequeNumberInfo = chequeNumberInfo;
	}

	public List<StockistChequeReturn> getStockistChequeReturnList() {
		return stockistChequeReturnList;
	}

	public void setStockistChequeReturnList(
			List<StockistChequeReturn> stockistChequeReturnList) {
		this.stockistChequeReturnList = stockistChequeReturnList;
	}
	
	
}
