/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * @author admin
 *
 */
@Entity
@Table(name="advancedchequeinformation")
public class AdvancedChequeInformation {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer advancedChequeInformationId;
	private Date courierReceivedDate;
	private Integer noOfCheque;
	private java.util.Date submitDate;
	private Integer status;
	
	@OneToMany(targetEntity=ChequeNumberInfo.class,cascade=CascadeType.ALL)
	@JoinColumn(name="advancedChequeInformationId",referencedColumnName="advancedChequeInformationId")
	private List<ChequeNumberInfo> chequeNumberInfoList;

	@ManyToOne
	@JoinColumn(name="companyBankId")
	private CompanyBank CompanyBank;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="stockistBankDetailsId")
	private StockistBankDetails stockistBankDetails;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	/**
	 * @return the advancedChequeInformationId
	 */
	public Integer getAdvancedChequeInformationId() {
		return advancedChequeInformationId;
	}

	/**
	 * @param advancedChequeInformationId the advancedChequeInformationId to set
	 */
	public void setAdvancedChequeInformationId(Integer advancedChequeInformationId) {
		this.advancedChequeInformationId = advancedChequeInformationId;
	}

	/**
	 * @return the courierReceivedDate
	 */
	public Date getCourierReceivedDate() {
		return courierReceivedDate;
	}

	/**
	 * @param courierReceivedDate the courierReceivedDate to set
	 */
	public void setCourierReceivedDate(Date courierReceivedDate) {
		this.courierReceivedDate = courierReceivedDate;
	}

	/**
	 * @return the noOfCheque
	 */
	public Integer getNoOfCheque() {
		return noOfCheque;
	}

	/**
	 * @param noOfCheque the noOfCheque to set
	 */
	public void setNoOfCheque(Integer noOfCheque) {
		this.noOfCheque = noOfCheque;
	}

	/**
	 * @return the submitDate
	 */
	public java.util.Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(java.util.Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the chequeNumberInfoList
	 */
	public List<ChequeNumberInfo> getChequeNumberInfoList() {
		return chequeNumberInfoList;
	}

	/**
	 * @param chequeNumberInfoList the chequeNumberInfoList to set
	 */
	public void setChequeNumberInfoList(List<ChequeNumberInfo> chequeNumberInfoList) {
		this.chequeNumberInfoList = chequeNumberInfoList;
	}

	/**
	 * @return the companyBank
	 */
	public CompanyBank getCompanyBank() {
		return CompanyBank;
	}

	/**
	 * @param companyBank the companyBank to set
	 */
	public void setCompanyBank(CompanyBank companyBank) {
		CompanyBank = companyBank;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Stockist getStockist() {
		return stockist;
	}

	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	public StockistBankDetails getStockistBankDetails() {
		return stockistBankDetails;
	}

	public void setStockistBankDetails(StockistBankDetails stockistBankDetails) {
		this.stockistBankDetails = stockistBankDetails;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	
	
}
