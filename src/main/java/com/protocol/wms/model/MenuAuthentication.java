/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="menuauthentication")
public class MenuAuthentication {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer menuAuthenticationId;
	
	@ManyToOne
	@JoinColumn(name="userTypeId")
	private UserType userType;
	
	@ManyToOne
	@JoinColumn(name="menuId")
	private Menu menu;
	
	@OneToMany(targetEntity=SubMenuAuthentication.class,cascade=CascadeType.ALL)
	@JoinColumn(name="menuAuthenticationId",referencedColumnName="menuAuthenticationId")
	private List<SubMenuAuthentication> subMenuAuthenticationList;

	/**
	 * @return the menuAuthenticationId
	 */
	public Integer getMenuAuthenticationId() {
		return menuAuthenticationId;
	}

	/**
	 * @param menuAuthenticationId the menuAuthenticationId to set
	 */
	public void setMenuAuthenticationId(Integer menuAuthenticationId) {
		this.menuAuthenticationId = menuAuthenticationId;
	}

	/**
	 * @return the userType
	 */
	public UserType getUserType() {
		return userType;
	}

	/**
	 * @param userType the userType to set
	 */
	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	/**
	 * @return the menu
	 */
	public Menu getMenu() {
		return menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	/**
	 * @return the subMenuAuthenticationList
	 */
	public List<SubMenuAuthentication> getSubMenuAuthenticationList() {
		return subMenuAuthenticationList;
	}

	/**
	 * @param subMenuAuthenticationList the subMenuAuthenticationList to set
	 */
	public void setSubMenuAuthenticationList(
			List<SubMenuAuthentication> subMenuAuthenticationList) {
		this.subMenuAuthenticationList = subMenuAuthenticationList;
	}
}
