/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="organizationtripcount")
public class OrganizationTripCount {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer organizationTripCountId;
	
	private Integer tripCount;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId")
	private Organization organization;

	public Integer getOrganizationTripCountId() {
		return organizationTripCountId;
	}

	public void setOrganizationTripCountId(Integer organizationTripCountId) {
		this.organizationTripCountId = organizationTripCountId;
	}

	public Integer getTripCount() {
		return tripCount;
	}

	public void setTripCount(Integer tripCount) {
		this.tripCount = tripCount;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	
	
}
