/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="city")
public class City {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cityId;
	
	private String cityName;
	
	private Integer status;
	
	@OneToMany(targetEntity=CompanyDepo.class,cascade=CascadeType.ALL)
	@JoinColumn(name="cityId",referencedColumnName="cityId")
	private List<CompanyDepo> companyDepoList;
	
	@OneToMany(targetEntity=TransporterDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="cityId",referencedColumnName="cityId")
	private List<TransporterDetails> transporterDetailsList;
	
	@OneToMany(targetEntity=Stockist.class,cascade=CascadeType.ALL)
	@JoinColumn(name="cityId",referencedColumnName="cityId")
	private List<Stockist> stockistList;

	@ManyToOne
	@JoinColumn(name="districtId")
	private District district;
	
	
	@OneToMany(targetEntity=InwardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="cityId",referencedColumnName="cityId")
	private List<InwardCourierRegistration> inwardCourierRegistrationList;
	
	
	@OneToMany(targetEntity=OutWardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="cityId",referencedColumnName="cityId")
	private List<OutWardCourierRegistration> outWardCourierRegistrations;
	/**
	 * @return the district
	 */
	public District getDistrict() {
		return district;
	}

	/**
	 * @param district the district to set
	 */
	public void setDistrict(District district) {
		this.district = district;
	}
	
	/**
	 * @return the cityId
	 */
	public Integer getCityId() {
		return cityId;
	}

	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the companyDepoList
	 */
	public List<CompanyDepo> getCompanyDepoList() {
		return companyDepoList;
	}

	/**
	 * @param companyDepoList the companyDepoList to set
	 */
	public void setCompanyDepoList(List<CompanyDepo> companyDepoList) {
		this.companyDepoList = companyDepoList;
	}

	/**
	 * @return the transporterDetailsList
	 */
	public List<TransporterDetails> getTransporterDetailsList() {
		return transporterDetailsList;
	}

	/**
	 * @param transporterDetailsList the transporterDetailsList to set
	 */
	public void setTransporterDetailsList(
			List<TransporterDetails> transporterDetailsList) {
		this.transporterDetailsList = transporterDetailsList;
	}

	/**
	 * @return the stockistList
	 */
	public List<Stockist> getStockistList() {
		return stockistList;
	}

	/**
	 * @param stockistList the stockistList to set
	 */
	public void setStockistList(List<Stockist> stockistList) {
		this.stockistList = stockistList;
	}

	public List<InwardCourierRegistration> getInwardCourierRegistrationList() {
		return inwardCourierRegistrationList;
	}

	public void setInwardCourierRegistrationList(
			List<InwardCourierRegistration> inwardCourierRegistrationList) {
		this.inwardCourierRegistrationList = inwardCourierRegistrationList;
	}
	
	
}
