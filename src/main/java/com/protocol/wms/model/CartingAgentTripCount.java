/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="cartingagenttripcount")
public class CartingAgentTripCount {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cartingAgentTripCountId;
	
	private Integer tripCount;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="cartingAgentId")
	private CartingAgent cartingAgent;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;

	public Integer getCartingAgentTripCountId() {
		return cartingAgentTripCountId;
	}

	public void setCartingAgentTripCountId(Integer cartingAgentTripCountId) {
		this.cartingAgentTripCountId = cartingAgentTripCountId;
	}

	public Integer getTripCount() {
		return tripCount;
	}

	public void setTripCount(Integer tripCount) {
		this.tripCount = tripCount;
	}

	public CartingAgent getCartingAgent() {
		return cartingAgent;
	}

	public void setCartingAgent(CartingAgent cartingAgent) {
		this.cartingAgent = cartingAgent;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	
}
