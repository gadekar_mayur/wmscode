/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="cartingagentuploadeddocument")
public class CartingAgentUploadedDocument {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cartingAgentUploadedDocumentId;
	
	private String imagePath;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="cartingAgentId")
	private CartingAgent cartingAgent;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="documentListTypeId")
	private DocumentListType documentListType;

	@OneToMany(targetEntity=CartingAgentUploadedDocumentImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="cartingAgentUploadedDocumentId",referencedColumnName="cartingAgentUploadedDocumentId")
	private List<CartingAgentUploadedDocumentImagePath> cartingAgentUploadedDocumentImagePathList;
	
	/**
	 * @return the cartingAgentUploadedDocumentId
	 */
	public Integer getCartingAgentUploadedDocumentId() {
		return cartingAgentUploadedDocumentId;
	}

	/**
	 * @param cartingAgentUploadedDocumentId the cartingAgentUploadedDocumentId to set
	 */
	public void setCartingAgentUploadedDocumentId(
			Integer cartingAgentUploadedDocumentId) {
		this.cartingAgentUploadedDocumentId = cartingAgentUploadedDocumentId;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the cartingAgent
	 */
	public CartingAgent getCartingAgent() {
		return cartingAgent;
	}

	/**
	 * @param cartingAgent the cartingAgent to set
	 */
	public void setCartingAgent(CartingAgent cartingAgent) {
		this.cartingAgent = cartingAgent;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the documentListType
	 */
	public DocumentListType getDocumentListType() {
		return documentListType;
	}

	/**
	 * @param documentListType the documentListType to set
	 */
	public void setDocumentListType(DocumentListType documentListType) {
		this.documentListType = documentListType;
	}

	public List<CartingAgentUploadedDocumentImagePath> getCartingAgentUploadedDocumentImagePathList() {
		return cartingAgentUploadedDocumentImagePathList;
	}

	public void setCartingAgentUploadedDocumentImagePathList(
			List<CartingAgentUploadedDocumentImagePath> cartingAgentUploadedDocumentImagePathList) {
		this.cartingAgentUploadedDocumentImagePathList = cartingAgentUploadedDocumentImagePathList;
	}
}
