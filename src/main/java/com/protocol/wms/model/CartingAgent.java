/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="cartingagent")
public class CartingAgent {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cartingAgentId;
	
	private String name;
	
	private String mobileNo;
	
	private String imagePath;
	
	private String vehicleNumber;
	
	private String model;
	
	private String vehiclePhotoImagePath;
	
	private String address;
	
	private Date submitDate;
	
	private Integer status;
	
	@OneToOne(mappedBy="cartingAgent")
	private CartingAgentTripCount cartingAgentTripCount;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@OneToMany(targetEntity=OutWardTrip.class,cascade=CascadeType.ALL)
	@JoinColumn(name="cartingAgentId",referencedColumnName="cartingAgentId")
	private List<OutWardTrip> outWardTrip;

	/*@ManyToOne
	@JoinColumn(name="transporterDetailsId")
	private TransporterDetails transporterDetails;*/

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="userDetailsId")
	private UserDetails userDetails;
	
	@OneToMany(targetEntity=CartingAgentImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="cartingAgentId",referencedColumnName="cartingAgentId")
	private List<CartingAgentImagePath> cartingAgentImagePathList;
	
	@OneToMany(targetEntity=CartingAgentVehiclePhotoImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="cartingAgentId",referencedColumnName="cartingAgentId")
	private List<CartingAgentVehiclePhotoImagePath> cartingAgentVehiclePhotoImagePathList;

	public Integer getCartingAgentId() {
		return cartingAgentId;
	}


	public void setCartingAgentId(Integer cartingAgentId) {
		this.cartingAgentId = cartingAgentId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public String getImagePath() {
		return imagePath;
	}


	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}


	public String getVehicleNumber() {
		return vehicleNumber;
	}


	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public String getVehiclePhotoImagePath() {
		return vehiclePhotoImagePath;
	}


	public void setVehiclePhotoImagePath(String vehiclePhotoImagePath) {
		this.vehiclePhotoImagePath = vehiclePhotoImagePath;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public Date getSubmitDate() {
		return submitDate;
	}


	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public Organization getOrganization() {
		return organization;
	}


	public void setOrganization(Organization organization) {
		this.organization = organization;
	}


	/**
	 * @return the transporterDetails
	 *//*
	public TransporterDetails getTransporterDetails() {
		return transporterDetails;
	}

	*//**
	 * @param transporterDetails the transporterDetails to set
	 *//*
	public void setTransporterDetails(TransporterDetails transporterDetails) {
		this.transporterDetails = transporterDetails;
	}*/
	
	

	public CartingAgentTripCount getCartingAgentTripCount() {
		return cartingAgentTripCount;
	}


	public void setCartingAgentTripCount(CartingAgentTripCount cartingAgentTripCount) {
		this.cartingAgentTripCount = cartingAgentTripCount;
	}


	/**
	 * @return the userDetails
	 */
	public UserDetails getUserDetails() {
		return userDetails;
	}

	/**
	 * @param userDetails the userDetails to set
	 */
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public List<CartingAgentImagePath> getCartingAgentImagePathList() {
		return cartingAgentImagePathList;
	}

	public void setCartingAgentImagePathList(
			List<CartingAgentImagePath> cartingAgentImagePathList) {
		this.cartingAgentImagePathList = cartingAgentImagePathList;
	}


	public List<CartingAgentVehiclePhotoImagePath> getCartingAgentVehiclePhotoImagePathList() {
		return cartingAgentVehiclePhotoImagePathList;
	}


	public void setCartingAgentVehiclePhotoImagePathList(
			List<CartingAgentVehiclePhotoImagePath> cartingAgentVehiclePhotoImagePathList) {
		this.cartingAgentVehiclePhotoImagePathList = cartingAgentVehiclePhotoImagePathList;
	}
}
