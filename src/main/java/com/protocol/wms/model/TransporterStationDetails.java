/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;



/**
 * @author admin
 *
 */
@Entity
@Table(name="transporterstationdetails")
public class TransporterStationDetails {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer transporterStationDetailsId;
	
	private String transporterStationName;
	
	private String transporterStationEmailId;
	
	
	private String transporterStationAddress;
	
	private Long transporterStationPhoneNo;
	
	private Long transporterStationMobileNo;
	
	private Integer status;
	
	private Date submitDate;
	
	private String StationContactName;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="transporterDetailsId")
	private TransporterDetails transporterDetails;
	
	@OneToOne(mappedBy="transporterStationDetails")
	private ClaimRates claimRates;
	
	@OneToOne(mappedBy="transporterStationDetails")
	private RatePerStation ratePerStation;
	
	
/*	@OneToOne(mappedBy="transporterStationDetails")
	private TransporterStockistTransfer transporterStockistTransfer;
	
	public TransporterStockistTransfer getTransporterStockistTransfer() {
		return transporterStockistTransfer;
	}

	public void setTransporterStockistTransfer(TransporterStockistTransfer transporterStockistTransfer) {
		this.transporterStockistTransfer = transporterStockistTransfer;
	}*/

	/**
	 * @return the transporterStationDetailsId
	 */
	public Integer getTransporterStationDetailsId() {
		return transporterStationDetailsId;
	}

	/**
	 * @param transporterStationDetailsId the transporterStationDetailsId to set
	 */
	public void setTransporterStationDetailsId(Integer transporterStationDetailsId) {
		this.transporterStationDetailsId = transporterStationDetailsId;
	}

	/**
	 * @return the transporterStationName
	 */
	public String getTransporterStationName() {
		return transporterStationName;
	}

	/**
	 * @param transporterStationName the transporterStationName to set
	 */
	public void setTransporterStationName(String transporterStationName) {
		this.transporterStationName = transporterStationName;
	}

	/**
	 * @return the transporterStationEmailId
	 */
	public String getTransporterStationEmailId() {
		return transporterStationEmailId;
	}

	/**
	 * @param transporterStationEmailId the transporterStationEmailId to set
	 */
	public void setTransporterStationEmailId(String transporterStationEmailId) {
		this.transporterStationEmailId = transporterStationEmailId;
	}


	/**
	 * @return the transporterStationAddress
	 */
	public String getTransporterStationAddress() {
		return transporterStationAddress;
	}

	/**
	 * @param transporterStationAddress the transporterStationAddress to set
	 */
	public void setTransporterStationAddress(String transporterStationAddress) {
		this.transporterStationAddress = transporterStationAddress;
	}

	/**
	 * @return the transporterStationPhoneNo
	 */
	public Long getTransporterStationPhoneNo() {
		return transporterStationPhoneNo;
	}

	/**
	 * @param transporterStationPhoneNo the transporterStationPhoneNo to set
	 */
	public void setTransporterStationPhoneNo(Long transporterStationPhoneNo) {
		this.transporterStationPhoneNo = transporterStationPhoneNo;
	}

	/**
	 * @return the transporterStationMobileNo
	 */
	public Long getTransporterStationMobileNo() {
		return transporterStationMobileNo;
	}

	/**
	 * @param transporterStationMobileNo the transporterStationMobileNo to set
	 */
	public void setTransporterStationMobileNo(Long transporterStationMobileNo) {
		this.transporterStationMobileNo = transporterStationMobileNo;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the transporterDetails
	 */
	public TransporterDetails getTransporterDetails() {
		return transporterDetails;
	}

	/**
	 * @param transporterDetails the transporterDetails to set
	 */
	public void setTransporterDetails(TransporterDetails transporterDetails) {
		this.transporterDetails = transporterDetails;
	}

	/**
	 * @return the claimRates
	 */
	public ClaimRates getClaimRates() {
		return claimRates;
	}

	/**
	 * @param claimRates the claimRates to set
	 */
	public void setClaimRates(ClaimRates claimRates) {
		this.claimRates = claimRates;
	}

	/**
	 * @return the ratePerStation
	 */
	public RatePerStation getRatePerStation() {
		return ratePerStation;
	}

	/**
	 * @param ratePerStation the ratePerStation to set
	 */
	public void setRatePerStation(RatePerStation ratePerStation) {
		this.ratePerStation = ratePerStation;
	}

	public String getStationContactName() {
		return StationContactName;
	}

	public void setStationContactName(String stationContactName) {
		StationContactName = stationContactName;
	}
	
	
}
