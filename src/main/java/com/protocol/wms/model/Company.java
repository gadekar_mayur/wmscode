/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * @author admin
 *
 */
@Entity
@Table(name="company")
public class Company {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer companyId;
	
	private String companyName;
	
	private String companyAddress;
	
	private String companyPerson;
	
	private Long companyTelephone;
	
	private Long companyMobileNo;
	
	private String companyEmailId;
	
	private String companyWebSite;
	
	private String companyVat;
	
	private String companyFaxNumber;
	
	private String companyCST;
	
	private String companyServiceTaxNo;
	
	private String companyPanNo;
	
	private Integer status;
	
	private Date submitDate;
	
	private Integer companyCreditAndDiscountStatus;
	
	@ManyToOne
	@JoinColumn(name="roleTypeId")
	private RoleType roleType;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private State state;
	
	@ManyToOne
	@JoinColumn(name="districtId")
	private District district;
	
	@ManyToOne
	@JoinColumn(name="cityId")
	private City city;
	
	@OneToMany(targetEntity=CompanyBank.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<CompanyBank> companyBanksList;
	
	@OneToMany(targetEntity=CompanyOrganizationBank.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<CompanyOrganizationBank> companyOrganizationBankList;
	
	@OneToMany(targetEntity=CompanyExecutive.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<CompanyExecutive> companyExecutiveList;
	
	@OneToMany(targetEntity=CompanyDepo.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<CompanyDepo> companyDepoList;
	
	@OneToMany(targetEntity=CompanyDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<CompanyDocument> companyDocumentList;
	
	@OneToMany(targetEntity=CompanyProduct.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<CompanyProduct> companyProductList;
	
	@OneToMany(targetEntity=CompanyCreditAndDiscount.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<CompanyCreditAndDiscount> companyCreditAndDiscountList;
	
	@OneToMany(targetEntity=EmployeeDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<EmployeeDetails> employeeDetailsList;
	
//	@OneToMany(targetEntity=EmployeeDocument.class,cascade=CascadeType.ALL)
//	@JoinColumn(name="companyId",referencedColumnName="companyId")
//	private List<EmployeeDocument> employeeDocumentList;
	
//	@OneToMany(targetEntity=TransporterDocument.class,cascade=CascadeType.ALL)
//	@JoinColumn(name="companyId",referencedColumnName="companyId")
//	private List<TransporterDocument> transporterDocumentList;
	
	//@OneToMany(targetEntity=Stockist.class,cascade=CascadeType.ALL)
	//@JoinColumn(name="companyId",referencedColumnName="companyId")
	@ManyToMany(mappedBy="companyList",cascade=CascadeType.ALL)
	private List<Stockist> stockistList;
	//
	@ManyToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinTable(name="ORGANIZATIONBANK_COMPANY_JOIN"
	,joinColumns={@JoinColumn(name="companyId")}
	,inverseJoinColumns={@JoinColumn(name="organizationBankId")})
	private List<OrganizationBank> organizationBankList;
	
	//
	/*@ManyToMany(mappedBy="companyList",cascade=CascadeType.ALL)
	private List<OrganizationBank> organizationBankList;*/
	
	
	@OneToMany(targetEntity=InwardGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<InwardGoodsRegistration> inwardGoodsRegistrationList;
	
	@OneToMany(targetEntity=InwardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<InwardCourierRegistration> inwardCourierRegistrationList;
	
	
	@OneToMany(targetEntity=InwardReturnGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList;
	
	@OneToMany(targetEntity=OutWardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<OutWardCourierRegistration> outWardCourierRegistrations;
	
	@OneToMany(targetEntity=OutWardReturnGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegistration;
	
	@OneToMany(targetEntity=OutWardOrderEntryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<OutWardOrderEntryRegistration> outWardOrderEnteryRegistrations;
	
	
//	@OneToMany(targetEntity=OutWardTrip.class,cascade=CascadeType.ALL)
//	@JoinColumn(name="companyId",referencedColumnName="companyId")
//	private List<OutWardTrip> outWardTripList;
	
	@OneToMany(targetEntity=AdvancedChequeInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<AdvancedChequeInformation> advancedChequeInformationList;
	
	@OneToMany(targetEntity=OutWardAdvancedChequeInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<OutWardAdvancedChequeInformation> outWardAdvancedChequeInformation;
	
	@OneToMany(targetEntity=StockistChequeDeposit.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<StockistChequeDeposit> stockistChequeDepositList;
	
	
	@OneToMany(targetEntity=RemainingChequeAmount.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<RemainingChequeAmount> remainingChequeAmountList;
	
	@OneToMany(targetEntity=OutWardDispatchEnteryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList;
	
	
	/**
	 * @return the organizationBankList
	 */
	public List<OrganizationBank> getOrganizationBankList() {
		return organizationBankList;
	}

	/**
	 * @param organizationBankList the organizationBankList to set
	 */
	public void setOrganizationBankList(List<OrganizationBank> organizationBankList) {
		this.organizationBankList = organizationBankList;
	}

	@OneToMany(targetEntity=StockistBankDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<StockistBankDetails> stockistBankDetailsList;
	
	@OneToMany(targetEntity=StockistDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<StockistDocument> stockistDocumentList;
	
	@OneToMany(targetEntity=StockistCompany.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyId",referencedColumnName="companyId")
	private List<StockistCompany> stockistCompanyList;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="userDetailsId")
	private UserDetails userDetails;

	/**
	 * @return the companyId
	 */
	public Integer getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the companyAddress
	 */
	public String getCompanyAddress() {
		return companyAddress;
	}

	/**
	 * @param companyAddress the companyAddress to set
	 */
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	/**
	 * @return the companyPerson
	 */
	public String getCompanyPerson() {
		return companyPerson;
	}

	/**
	 * @param companyPerson the companyPerson to set
	 */
	public void setCompanyPerson(String companyPerson) {
		this.companyPerson = companyPerson;
	}

	/**
	 * @return the companyTelephone
	 */
	public Long getCompanyTelephone() {
		return companyTelephone;
	}

	/**
	 * @param companyTelephone the companyTelephone to set
	 */
	public void setCompanyTelephone(Long companyTelephone) {
		this.companyTelephone = companyTelephone;
	}

	/**
	 * @return the companyMobileNo
	 */
	public Long getCompanyMobileNo() {
		return companyMobileNo;
	}

	/**
	 * @param companyMobileNo the companyMobileNo to set
	 */
	public void setCompanyMobileNo(Long companyMobileNo) {
		this.companyMobileNo = companyMobileNo;
	}

	/**
	 * @return the companyEmailId
	 */
	public String getCompanyEmailId() {
		return companyEmailId;
	}

	/**
	 * @param companyEmailId the companyEmailId to set
	 */
	public void setCompanyEmailId(String companyEmailId) {
		this.companyEmailId = companyEmailId;
	}

	/**
	 * @return the companyWebSite
	 */
	public String getCompanyWebSite() {
		return companyWebSite;
	}

	/**
	 * @param companyWebSite the companyWebSite to set
	 */
	public void setCompanyWebSite(String companyWebSite) {
		this.companyWebSite = companyWebSite;
	}

	/**
	 * @return the companyVat
	 */
	public String getCompanyVat() {
		return companyVat;
	}

	/**
	 * @param companyVat the companyVat to set
	 */
	public void setCompanyVat(String companyVat) {
		this.companyVat = companyVat;
	}

	/**
	 * @return the companyFaxNumber
	 */
	public String getCompanyFaxNumber() {
		return companyFaxNumber;
	}

	/**
	 * @param companyFaxNumber the companyFaxNumber to set
	 */
	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}

	/**
	 * @return the companyCST
	 */
	public String getCompanyCST() {
		return companyCST;
	}

	/**
	 * @param companyCST the companyCST to set
	 */
	public void setCompanyCST(String companyCST) {
		this.companyCST = companyCST;
	}

	/**
	 * @return the companyServiceTaxNo
	 */
	public String getCompanyServiceTaxNo() {
		return companyServiceTaxNo;
	}

	/**
	 * @param companyServiceTaxNo the companyServiceTaxNo to set
	 */
	public void setCompanyServiceTaxNo(String companyServiceTaxNo) {
		this.companyServiceTaxNo = companyServiceTaxNo;
	}

	/**
	 * @return the companyPanNo
	 */
	public String getCompanyPanNo() {
		return companyPanNo;
	}

	/**
	 * @param companyPanNo the companyPanNo to set
	 */
	public void setCompanyPanNo(String companyPanNo) {
		this.companyPanNo = companyPanNo;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the roleType
	 */
	public RoleType getRoleType() {
		return roleType;
	}

	/**
	 * @param roleType the roleType to set
	 */
	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}

	/**
	 * @return the organizationg
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organizationg the organizationg to set
	 */
	public void setOrganizationg(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the companyBanksList
	 */
	public List<CompanyBank> getCompanyBanksList() {
		return companyBanksList;
	}

	/**
	 * @param companyBanksList the companyBanksList to set
	 */
	public void setCompanyBanksList(List<CompanyBank> companyBanksList) {
		this.companyBanksList = companyBanksList;
	}

	/**
	 * @return the companyOrganizationBankList
	 */
	public List<CompanyOrganizationBank> getCompanyOrganizationBankList() {
		return companyOrganizationBankList;
	}

	/**
	 * @param companyOrganizationBankList the companyOrganizationBankList to set
	 */
	public void setCompanyOrganizationBankList(
			List<CompanyOrganizationBank> companyOrganizationBankList) {
		this.companyOrganizationBankList = companyOrganizationBankList;
	}

	/**
	 * @return the companyExecutiveList
	 */
	public List<CompanyExecutive> getCompanyExecutiveList() {
		return companyExecutiveList;
	}

	/**
	 * @param companyExecutiveList the companyExecutiveList to set
	 */
	public void setCompanyExecutiveList(List<CompanyExecutive> companyExecutiveList) {
		this.companyExecutiveList = companyExecutiveList;
	}

	/**
	 * @return the companyDepoList
	 */
	public List<CompanyDepo> getCompanyDepoList() {
		return companyDepoList;
	}

	/**
	 * @param companyDepoList the companyDepoList to set
	 */
	public void setCompanyDepoList(List<CompanyDepo> companyDepoList) {
		this.companyDepoList = companyDepoList;
	}

	/**
	 * @return the companyDocumentList
	 */
	public List<CompanyDocument> getCompanyDocumentList() {
		return companyDocumentList;
	}

	/**
	 * @param companyDocumentList the companyDocumentList to set
	 */
	public void setCompanyDocumentList(List<CompanyDocument> companyDocumentList) {
		this.companyDocumentList = companyDocumentList;
	}

	/**
	 * @return the companyProductList
	 */
	public List<CompanyProduct> getCompanyProductList() {
		return companyProductList;
	}

	/**
	 * @param companyProductList the companyProductList to set
	 */
	public void setCompanyProductList(List<CompanyProduct> companyProductList) {
		this.companyProductList = companyProductList;
	}

	/**
	 * @return the companyCreditAndDiscountList
	 */
	public List<CompanyCreditAndDiscount> getCompanyCreditAndDiscountList() {
		return companyCreditAndDiscountList;
	}

	/**
	 * @param companyCreditAndDiscountList the companyCreditAndDiscountList to set
	 */
	public void setCompanyCreditAndDiscountList(
			List<CompanyCreditAndDiscount> companyCreditAndDiscountList) {
		this.companyCreditAndDiscountList = companyCreditAndDiscountList;
	}

	/**
	 * @return the employeeDetailsList
	 */
	public List<EmployeeDetails> getEmployeeDetailsList() {
		return employeeDetailsList;
	}

	/**
	 * @param employeeDetailsList the employeeDetailsList to set
	 */
	public void setEmployeeDetailsList(List<EmployeeDetails> employeeDetailsList) {
		this.employeeDetailsList = employeeDetailsList;
	}


	/**
	 * @return the stockistList
	 */
	public List<Stockist> getStockistList() {
		return stockistList;
	}

	/**
	 * @param stockistList the stockistList to set
	 */
	public void setStockistList(List<Stockist> stockistList) {
		this.stockistList = stockistList;
	}

	/**
	 * @return the stockistBankDetailsList
	 */
	public List<StockistBankDetails> getStockistBankDetailsList() {
		return stockistBankDetailsList;
	}

	/**
	 * @param stockistBankDetailsList the stockistBankDetailsList to set
	 */
	public void setStockistBankDetailsList(
			List<StockistBankDetails> stockistBankDetailsList) {
		this.stockistBankDetailsList = stockistBankDetailsList;
	}

	/**
	 * @return the stockistDocumentList
	 */
	public List<StockistDocument> getStockistDocumentList() {
		return stockistDocumentList;
	}

	/**
	 * @param stockistDocumentList the stockistDocumentList to set
	 */
	public void setStockistDocumentList(List<StockistDocument> stockistDocumentList) {
		this.stockistDocumentList = stockistDocumentList;
	}

	/**
	 * @return the stockistCompanyList
	 */
	public List<StockistCompany> getStockistCompanyList() {
		return stockistCompanyList;
	}

	/**
	 * @param stockistCompanyList the stockistCompanyList to set
	 */
	public void setStockistCompanyList(List<StockistCompany> stockistCompanyList) {
		this.stockistCompanyList = stockistCompanyList;
	}

	/**
	 * @return the userDetails
	 */
	public UserDetails getUserDetails() {
		return userDetails;
	}

	/**
	 * @param userDetails the userDetails to set
	 */
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public List<InwardGoodsRegistration> getInwardGoodsRegistrationList() {
		return inwardGoodsRegistrationList;
	}

	public void setInwardGoodsRegistrationList(
			List<InwardGoodsRegistration> inwardGoodsRegistrationList) {
		this.inwardGoodsRegistrationList = inwardGoodsRegistrationList;
	}

	public List<InwardCourierRegistration> getInwardCourierRegistrationList() {
		return inwardCourierRegistrationList;
	}

	public void setInwardCourierRegistrationList(
			List<InwardCourierRegistration> inwardCourierRegistrationList) {
		this.inwardCourierRegistrationList = inwardCourierRegistrationList;
	}

	public List<InwardReturnGoodsRegistration> getInwardReturnGoodsRegistrationList() {
		return inwardReturnGoodsRegistrationList;
	}

	public void setInwardReturnGoodsRegistrationList(
			List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList) {
		this.inwardReturnGoodsRegistrationList = inwardReturnGoodsRegistrationList;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<OutWardCourierRegistration> getOutWardCourierRegistrations() {
		return outWardCourierRegistrations;
	}

	public void setOutWardCourierRegistrations(
			List<OutWardCourierRegistration> outWardCourierRegistrations) {
		this.outWardCourierRegistrations = outWardCourierRegistrations;
	}

	public List<OutWardReturnGoodsRegistration> getOutWardReturnGoodsRegistration() {
		return outWardReturnGoodsRegistration;
	}

	public void setOutWardReturnGoodsRegistration(
			List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegistration) {
		this.outWardReturnGoodsRegistration = outWardReturnGoodsRegistration;
	}

	public List<OutWardOrderEntryRegistration> getOutWardOrderEnteryRegistrations() {
		return outWardOrderEnteryRegistrations;
	}

	public void setOutWardOrderEnteryRegistrations(
			List<OutWardOrderEntryRegistration> outWardOrderEnteryRegistrations) {
		this.outWardOrderEnteryRegistrations = outWardOrderEnteryRegistrations;
	}

//	public List<OutWardTrip> getOutWardTripList() {
//		return outWardTripList;
//	}
//
//	public void setOutWardTripList(List<OutWardTrip> outWardTripList) {
//		this.outWardTripList = outWardTripList;
//	}

	public List<AdvancedChequeInformation> getAdvancedChequeInformationList() {
		return advancedChequeInformationList;
	}

	public void setAdvancedChequeInformationList(
			List<AdvancedChequeInformation> advancedChequeInformationList) {
		this.advancedChequeInformationList = advancedChequeInformationList;
	}

	public List<OutWardAdvancedChequeInformation> getOutWardAdvancedChequeInformation() {
		return outWardAdvancedChequeInformation;
	}

	public void setOutWardAdvancedChequeInformation(
			List<OutWardAdvancedChequeInformation> outWardAdvancedChequeInformation) {
		this.outWardAdvancedChequeInformation = outWardAdvancedChequeInformation;
	}

	/**
	 * @return the companyCreditAndDiscountStatus
	 */
	public Integer getCompanyCreditAndDiscountStatus() {
		return companyCreditAndDiscountStatus;
	}

	/**
	 * @param companyCreditAndDiscountStatus the companyCreditAndDiscountStatus to set
	 */
	public void setCompanyCreditAndDiscountStatus(
			Integer companyCreditAndDiscountStatus) {
		this.companyCreditAndDiscountStatus = companyCreditAndDiscountStatus;
	}

	public List<StockistChequeDeposit> getStockistChequeDepositList() {
		return stockistChequeDepositList;
	}

	public void setStockistChequeDepositList(
			List<StockistChequeDeposit> stockistChequeDepositList) {
		this.stockistChequeDepositList = stockistChequeDepositList;
	}

	public List<RemainingChequeAmount> getRemainingChequeAmountList() {
		return remainingChequeAmountList;
	}

	public void setRemainingChequeAmountList(
			List<RemainingChequeAmount> remainingChequeAmountList) {
		this.remainingChequeAmountList = remainingChequeAmountList;
	}

	public List<OutWardDispatchEnteryRegistration> getOutWardDispatchEnteryRegistrationList() {
		return outWardDispatchEnteryRegistrationList;
	}

	public void setOutWardDispatchEnteryRegistrationList(
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList) {
		this.outWardDispatchEnteryRegistrationList = outWardDispatchEnteryRegistrationList;
	}
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	/**
	 * @return the organizationBankList
	 *//*
	public List<OrganizationBank> getOrganizationBankList() {
		return organizationBankList;
	}

	*//**
	 * @param organizationBankList the organizationBankList to set
	 *//*
	public void setOrganizationBankList(List<OrganizationBank> organizationBankList) {
		this.organizationBankList = organizationBankList;
	}*/
	
	

}
