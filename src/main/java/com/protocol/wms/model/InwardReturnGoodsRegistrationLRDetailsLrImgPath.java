package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="inwardreturngoodsregistrationlrdetailslrimgpath")
public class InwardReturnGoodsRegistrationLRDetailsLrImgPath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer inwardReturnGoodsRegistrationLRDetailsLrImgPathId ;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="InwardReturnGoodsRegistrationLRDetailsId")
	private InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails;

	public Integer getInwardReturnGoodsRegistrationLRDetailsLrImgPathId() {
		return inwardReturnGoodsRegistrationLRDetailsLrImgPathId;
	}

	public void setInwardReturnGoodsRegistrationLRDetailsLrImgPathId(
			Integer inwardReturnGoodsRegistrationLRDetailsLrImgPathId) {
		this.inwardReturnGoodsRegistrationLRDetailsLrImgPathId = inwardReturnGoodsRegistrationLRDetailsLrImgPathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public InwardReturnGoodsRegistrationLRDetails getInwardReturnGoodsRegistrationLRDetails() {
		return inwardReturnGoodsRegistrationLRDetails;
	}

	public void setInwardReturnGoodsRegistrationLRDetails(
			InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails) {
		this.inwardReturnGoodsRegistrationLRDetails = inwardReturnGoodsRegistrationLRDetails;
	}
	
}
