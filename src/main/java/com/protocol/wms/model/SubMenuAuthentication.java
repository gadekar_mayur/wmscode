/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="submenuauthentication")
public class SubMenuAuthentication {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer subMenuAuthenticationId;
	
	@ManyToOne
	@JoinColumn(name="menuAuthenticationId")
	private MenuAuthentication menuAuthentication;
	
	@ManyToOne
	@JoinColumn(name="subMenuId")
	private SubMenu subMenu;

	/**
	 * @return the subMenuAuthenticationId
	 */
	public Integer getSubMenuAuthenticationId() {
		return subMenuAuthenticationId;
	}

	/**
	 * @param subMenuAuthenticationId the subMenuAuthenticationId to set
	 */
	public void setSubMenuAuthenticationId(Integer subMenuAuthenticationId) {
		this.subMenuAuthenticationId = subMenuAuthenticationId;
	}

	/**
	 * @return the menuAuthentication
	 */
	public MenuAuthentication getMenuAuthentication() {
		return menuAuthentication;
	}

	/**
	 * @param menuAuthentication the menuAuthentication to set
	 */
	public void setMenuAuthentication(MenuAuthentication menuAuthentication) {
		this.menuAuthentication = menuAuthentication;
	}

	/**
	 * @return the subMenu
	 */
	public SubMenu getSubMenu() {
		return subMenu;
	}

	/**
	 * @param subMenu the subMenu to set
	 */
	public void setSubMenu(SubMenu subMenu) {
		this.subMenu = subMenu;
	}
}
