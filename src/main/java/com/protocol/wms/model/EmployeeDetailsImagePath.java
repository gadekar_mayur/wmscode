package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="employeedetailsimagepath")
public class EmployeeDetailsImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer employeeDetailsImagePathId ;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="employeeDetailsId")
	private EmployeeDetails employeeDetails;

	public Integer getEmployeeDetailsImagePathId() {
		return employeeDetailsImagePathId;
	}

	public void setEmployeeDetailsImagePathId(Integer employeeDetailsImagePathId) {
		this.employeeDetailsImagePathId = employeeDetailsImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	
	
}
