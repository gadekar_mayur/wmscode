package com.protocol.wms.model;

public class StockistApprovalPending
{
	private Integer TransporterStockistTransfer;
public Integer getTransporterStockistTransfer() {
		return TransporterStockistTransfer;
	}
	public void setTransporterStockistTransfer(Integer transporterStockistTransfer) {
		TransporterStockistTransfer = transporterStockistTransfer;
	}
private Integer StockistId;
private String StockistName;
private Integer FromTransporterId;
private String FromTransporterName;
private Integer ToTransporterId;
private String ToTransporterName;
private Integer TransporterStationId;
private String TransporterStationName;
private Double ActualRate;
private Double ActualLR;
private Double ClaimRate;
private Double ClaimLR;

private Double ToActualRate;
private Double ToActualLR;
private Double ToClaimRate;
private Double ToClaimLR;
private Double Difference;
private Double LrDifference;
public Double getLrDifference() {
	return LrDifference;
}
public void setLrDifference(Double lrDifference) {
	LrDifference = lrDifference;
}
private Integer status;
public Integer getStatus() {
	return status;
}
public void setStatus(Integer status) {
	this.status = status;
}
public Double getDifference() {
	return Difference;
}
public void setDifference(Double difference) {
	Difference = difference;
}
public Double getToActualRate() {
	return ToActualRate;
}
public void setToActualRate(Double toActualRate) {
	ToActualRate = toActualRate;
}
public Double getToActualLR() {
	return ToActualLR;
}
public void setToActualLR(Double toActualLR) {
	ToActualLR = toActualLR;
}
public Double getToClaimRate() {
	return ToClaimRate;
}
public void setToClaimRate(Double toClaimRate) {
	ToClaimRate = toClaimRate;
}
public Double getToClaimLR() {
	return ToClaimLR;
}
public void setToClaimLR(Double toClaimLR) {
	ToClaimLR = toClaimLR;
}
public Integer getStockistId() {
	return StockistId;
}
public void setStockistId(Integer stockistId) {
	StockistId = stockistId;
}
public String getStockistName() {
	return StockistName;
}
public void setStockistName(String stockistName) {
	StockistName = stockistName;
}
public Integer getFromTransporterId() {
	return FromTransporterId;
}
public void setFromTransporterId(Integer fromTransporterId) {
	FromTransporterId = fromTransporterId;
}
public String getFromTransporterName() {
	return FromTransporterName;
}
public void setFromTransporterName(String fromTransporterName) {
	FromTransporterName = fromTransporterName;
}
public Integer getToTransporterId() {
	return ToTransporterId;
}
public void setToTransporterId(Integer toTransporterId) {
	ToTransporterId = toTransporterId;
}
public String getToTransporterName() {
	return ToTransporterName;
}
public void setToTransporterName(String toTransporterName) {
	ToTransporterName = toTransporterName;
}
public Integer getTransporterStationId() {
	return TransporterStationId;
}
public void setTransporterStationId(Integer transporterStationId) {
	TransporterStationId = transporterStationId;
}
public String getTransporterStationName() {
	return TransporterStationName;
}
public void setTransporterStationName(String transporterStationName) {
	TransporterStationName = transporterStationName;
}
public Double getActualRate() {
	return ActualRate;
}
public void setActualRate(Double actualRate) {
	ActualRate = actualRate;
}
public Double getActualLR() {
	return ActualLR;
}
public void setActualLR(Double actualLR) {
	ActualLR = actualLR;
}
public Double getClaimRate() {
	return ClaimRate;
}
public void setClaimRate(Double claimRate) {
	ClaimRate = claimRate;
}
public Double getClaimLR() {
	return ClaimLR;
}
public void setClaimLR(Double claimLR) {
	ClaimLR = claimLR;
}

}
