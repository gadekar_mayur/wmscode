/**
 * 
 */
package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="remainingchequeamount")
public class RemainingChequeAmount {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer remainingChequeAmountId;
	
	private Double remainingChequeAmount;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="stockistBankDetailsId")
	private StockistBankDetails stockistBankDetails;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="chequeNumberInfoId")
	private ChequeNumberInfo chequeNumberInfo;
	
	private Integer status;

	public Integer getRemainingChequeAmountId() {
		return remainingChequeAmountId;
	}

	public void setRemainingChequeAmountId(Integer remainingChequeAmountId) {
		this.remainingChequeAmountId = remainingChequeAmountId;
	}

	public Double getRemainingChequeAmount() {
		return remainingChequeAmount;
	}

	public void setRemainingChequeAmount(Double remainingChequeAmount) {
		this.remainingChequeAmount = remainingChequeAmount;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Stockist getStockist() {
		return stockist;
	}

	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	public StockistBankDetails getStockistBankDetails() {
		return stockistBankDetails;
	}

	public void setStockistBankDetails(StockistBankDetails stockistBankDetails) {
		this.stockistBankDetails = stockistBankDetails;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public ChequeNumberInfo getChequeNumberInfo() {
		return chequeNumberInfo;
	}

	public void setChequeNumberInfo(ChequeNumberInfo chequeNumberInfo) {
		this.chequeNumberInfo = chequeNumberInfo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
