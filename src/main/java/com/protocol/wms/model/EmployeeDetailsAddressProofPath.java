package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="employeedetailsaddressproofpath")
public class EmployeeDetailsAddressProofPath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer EmployeeDetailsAddressProofPathId;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="employeeDetailsId")
	private EmployeeDetails employeeDetails;

	public Integer getEmployeeDetailsAddressProofPathId() {
		return EmployeeDetailsAddressProofPathId;
	}

	public void setEmployeeDetailsAddressProofPathId(
			Integer employeeDetailsAddressProofPathId) {
		EmployeeDetailsAddressProofPathId = employeeDetailsAddressProofPathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
		
}
