/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="InwardReturnProducts")
public class InwardReturnProducts {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer InwardReturnProductsId;
	

	private String productName;
	
	private Integer receivedQuantity;
	
	private Date mfgDate;
	
	private String mfgCompany;
	
	private Integer quantityVariance;
	
	private Integer claimQuatity;
	
	private String batch;
	
	private Date expiryDate;
	
	private String reason;
	
	private Integer status;
	
	@ManyToOne@JoinColumn(name="InwardReturnGoodsRegistrationLRDetailsId")
	private InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails;

	public Integer getInwardReturnProductsId() {
		return InwardReturnProductsId;
	}

	public void setInwardReturnProductsId(Integer inwardReturnProductsId) {
		InwardReturnProductsId = inwardReturnProductsId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(Integer receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

	public Date getMfgDate() {
		return mfgDate;
	}

	public void setMfgDate(Date mfgDate) {
		this.mfgDate = mfgDate;
	}

	public String getMfgCompany() {
		return mfgCompany;
	}

	public void setMfgCompany(String mfgCompany) {
		this.mfgCompany = mfgCompany;
	}

	public Integer getQuantityVariance() {
		return quantityVariance;
	}

	public void setQuantityVariance(Integer quantityVariance) {
		this.quantityVariance = quantityVariance;
	}

	public Integer getClaimQuatity() {
		return claimQuatity;
	}

	public void setClaimQuatity(Integer claimQuatity) {
		this.claimQuatity = claimQuatity;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public InwardReturnGoodsRegistrationLRDetails getInwardReturnGoodsRegistrationLRDetails() {
		return inwardReturnGoodsRegistrationLRDetails;
	}

	public void setInwardReturnGoodsRegistrationLRDetails(
			InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails) {
		this.inwardReturnGoodsRegistrationLRDetails = inwardReturnGoodsRegistrationLRDetails;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
}
