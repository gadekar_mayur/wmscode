/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="bloodgroup")
public class BloodGroup {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer bloodGroupId;
	
	private String bloodGroup;
	
	private Integer status;
	
	@OneToMany(targetEntity=EmployeeDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="bloodGroupId",referencedColumnName="bloodGroupId")
	private List<EmployeeDetails> employeeDetailsList;

	/**
	 * @return the bloodGroupId
	 */
	public Integer getBloodGroupId() {
		return bloodGroupId;
	}

	/**
	 * @param bloodGroupId the bloodGroupId to set
	 */
	public void setBloodGroupId(Integer bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}

	/**
	 * @return the bloodGroup
	 */
	public String getBloodGroup() {
		return bloodGroup;
	}

	/**
	 * @param bloodGroup the bloodGroup to set
	 */
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	/**
	 * @return the employeeDetailsList
	 */
	public List<EmployeeDetails> getEmployeeDetailsList() {
		return employeeDetailsList;
	}

	/**
	 * @param employeeDetailsList the employeeDetailsList to set
	 */
	public void setEmployeeDetailsList(List<EmployeeDetails> employeeDetailsList) {
		this.employeeDetailsList = employeeDetailsList;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
