package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="outwardreturngoodslrdetailsclaimimagepath")
public class OutWardReturnGoodsLRDetailsClaimImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardReturnGoodsLRDetailsClaimImagePathId ;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="outWardReturnGoodsLRDetailsId")
	private OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails;

	public Integer getOutWardReturnGoodsLRDetailsClaimImagePathId() {
		return outWardReturnGoodsLRDetailsClaimImagePathId;
	}

	public void setOutWardReturnGoodsLRDetailsClaimImagePathId(
			Integer outWardReturnGoodsLRDetailsClaimImagePathId) {
		this.outWardReturnGoodsLRDetailsClaimImagePathId = outWardReturnGoodsLRDetailsClaimImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public OutWardReturnGoodsLRDetails getOutWardReturnGoodsLRDetails() {
		return outWardReturnGoodsLRDetails;
	}

	public void setOutWardReturnGoodsLRDetails(
			OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails) {
		this.outWardReturnGoodsLRDetails = outWardReturnGoodsLRDetails;
	}
	
}
