package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="inwardcourierregistrationdocketfilepath")
public class InwardCourierRegistrationDocketFilePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer inwardCourierRegistrationDocketFilePathId ;
	
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="InwardCourierRegistrationId")
	private InwardCourierRegistration inwardCourierRegistration;

	public Integer getInwardCourierRegistrationDocketFilePathId() {
		return inwardCourierRegistrationDocketFilePathId;
	}

	public void setInwardCourierRegistrationDocketFilePathId(
			Integer inwardCourierRegistrationDocketFilePathId) {
		this.inwardCourierRegistrationDocketFilePathId = inwardCourierRegistrationDocketFilePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public InwardCourierRegistration getInwardCourierRegistration() {
		return inwardCourierRegistration;
	}

	public void setInwardCourierRegistration(
			InwardCourierRegistration inwardCourierRegistration) {
		this.inwardCourierRegistration = inwardCourierRegistration;
	}
	
	
}
