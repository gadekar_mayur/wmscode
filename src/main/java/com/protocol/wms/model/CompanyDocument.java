/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="companydocument")
public class CompanyDocument {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer companyDocumentId;
	
//	private String imagePath;
	
	private Integer status;
	
	private Date submitDate;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="documentListTypeId")
	private DocumentListType documentListType;

	@OneToMany(targetEntity=CompanyDocumentImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="companyDocumentId",referencedColumnName="companyDocumentId")
	private List<CompanyDocumentImagePath> companyDocumentImagePathList;
	
	/**
	 * @return the companyDocumentId
	 */
	public Integer getCompanyDocumentId() {
		return companyDocumentId;
	}

	/**
	 * @param companyDocumentId the companyDocumentId to set
	 */
	public void setCompanyDocumentId(Integer companyDocumentId) {
		this.companyDocumentId = companyDocumentId;
	}


	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the documentListType
	 */
	public DocumentListType getDocumentListType() {
		return documentListType;
	}

	/**
	 * @param documentListType the documentListType to set
	 */
	public void setDocumentListType(DocumentListType documentListType) {
		this.documentListType = documentListType;
	}

	public List<CompanyDocumentImagePath> getCompanyDocumentImagePathList() {
		return companyDocumentImagePathList;
	}

	public void setCompanyDocumentImagePathList(
			List<CompanyDocumentImagePath> companyDocumentImagePathList) {
		this.companyDocumentImagePathList = companyDocumentImagePathList;
	}
}
