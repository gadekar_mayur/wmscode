package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="organizationdocumentimagepath")
public class OrganizationDocumentImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer organizationDocumentImagePathId ;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="organizationDocumentId")
	private OrganizationDocument organizationDocument;


	public Integer getOrganizationDocumentImagePathId() {
		return organizationDocumentImagePathId;
	}

	public void setOrganizationDocumentImagePathId(
			Integer organizationDocumentImagePathId) {
		this.organizationDocumentImagePathId = organizationDocumentImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public OrganizationDocument getOrganizationDocument() {
		return organizationDocument;
	}

	public void setOrganizationDocument(OrganizationDocument organizationDocument) {
		this.organizationDocument = organizationDocument;
	}
}
