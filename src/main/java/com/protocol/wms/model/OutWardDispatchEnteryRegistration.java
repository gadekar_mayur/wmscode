/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="outwarddispatchenteryregistration")
public class OutWardDispatchEnteryRegistration {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer outWardDispatchEnteryRegistrationId;
	
	private Integer noOfCases;
	
	private Date submitDate;
	
	private Integer tripStatus;
	
	private Integer status;
	
	private Integer getPassStatus;
	
	private Integer printStickerStatus;
	
	@ManyToOne
	@JoinColumn(name="outWardTripId")
	private OutWardTrip outWardTrip;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;

/*	@ManyToOne
	@JoinColumn(name="outWardOrderInvoiceEnteryRegistrationId")
	private OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistration;*/
	
	@ManyToOne
	@JoinColumn(name="outWardGatePassId")
	private OutWardGatePass outWardGatePass;
	
	
	@OneToOne(mappedBy="outWardDispatchEnteryRegistration")
	private PrintSticker printSticker;
	
	public Integer getOutWardDispatchEnteryRegistrationId() {
		return outWardDispatchEnteryRegistrationId;
	}

	public void setOutWardDispatchEnteryRegistrationId(
			Integer outWardDispatchEnteryRegistrationId) {
		this.outWardDispatchEnteryRegistrationId = outWardDispatchEnteryRegistrationId;
	}

	public Integer getNoOfCases() {
		return noOfCases;
	}

	public void setNoOfCases(Integer noOfCases) {
		this.noOfCases = noOfCases;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public Integer getTripStatus() {
		return tripStatus;
	}

	public void setTripStatus(Integer tripStatus) {
		this.tripStatus = tripStatus;
	}

	public OutWardTrip getOutWardTrip() {
		return outWardTrip;
	}

	public void setOutWardTrip(OutWardTrip outWardTrip) {
		this.outWardTrip = outWardTrip;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Stockist getStockist() {
		return stockist;
	}

	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Integer getGetPassStatus() {
		return getPassStatus;
	}

	public void setGetPassStatus(Integer getPassStatus) {
		this.getPassStatus = getPassStatus;
	}

	public OutWardGatePass getOutWardGatePass() {
		return outWardGatePass;
	}

	public void setOutWardGatePass(OutWardGatePass outWardGatePass) {
		this.outWardGatePass = outWardGatePass;
	}

	public PrintSticker getPrintSticker() {
		return printSticker;
	}

	public void setPrintSticker(PrintSticker printSticker) {
		this.printSticker = printSticker;
	}

	public Integer getPrintStickerStatus() {
		return printStickerStatus;
	}

	public void setPrintStickerStatus(Integer printStickerStatus) {
		this.printStickerStatus = printStickerStatus;
	}
	
	
}
