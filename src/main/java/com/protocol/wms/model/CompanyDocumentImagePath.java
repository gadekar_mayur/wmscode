package com.protocol.wms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="companydocumentimagepath")
public class CompanyDocumentImagePath {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer companyDocumentImagePathId;
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="companyDocumentId")
	private CompanyDocument companyDocument;

	public Integer getCompanyDocumentImagePathId() {
		return companyDocumentImagePathId;
	}

	public void setCompanyDocumentImagePathId(Integer companyDocumentImagePathId) {
		this.companyDocumentImagePathId = companyDocumentImagePathId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public CompanyDocument getCompanyDocument() {
		return companyDocument;
	}

	public void setCompanyDocument(CompanyDocument companyDocument) {
		this.companyDocument = companyDocument;
	}
}
