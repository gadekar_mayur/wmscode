/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="inwardreturngoodsregistrationlrdetails")
public class InwardReturnGoodsRegistrationLRDetails {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer InwardReturnGoodsRegistrationLRDetailsId;
	
	private String LRNo;
	
	private Integer noOfCases;
	
	private String refNoOrClaimNo;
	
	private Float claimAmount;
	
	//private String LRImagePath;
	
	//private String claimCopyImagePath;
	
	private Integer status;
	
	@ManyToOne@JoinColumn(name="inwardReturnGoodRegistrationId")
	private InwardReturnGoodsRegistration inwardReturnGoodsRegistration;
	
	@OneToMany(targetEntity=InwardReturnProducts.class,cascade=CascadeType.ALL)
	@JoinColumn(name="InwardReturnGoodsRegistrationLRDetailsId",referencedColumnName="InwardReturnGoodsRegistrationLRDetailsId")
	private List<InwardReturnProducts> inwardReturnProductList;
	
	@OneToMany(targetEntity=InwardReturnGoodsRegistrationLRDetailsLrImgPath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="InwardReturnGoodsRegistrationLRDetailsId",referencedColumnName="InwardReturnGoodsRegistrationLRDetailsId")
	private List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> inwardReturnGoodsRegistrationLRDetailsLrImgPathList;
	
	@OneToMany(targetEntity=InwardReturnGoodsRegistrationLRDetailsClaimCopyPath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="InwardReturnGoodsRegistrationLRDetailsId",referencedColumnName="InwardReturnGoodsRegistrationLRDetailsId")
	private List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> inwardReturnGoodsRegistrationLRDetailsClaimCopyPathList;
	
	public Integer getInwardReturnGoodsRegistrationLRDetailsId() {
		return InwardReturnGoodsRegistrationLRDetailsId;
	}

	public void setInwardReturnGoodsRegistrationLRDetailsId(
			Integer inwardReturnGoodsRegistrationLRDetailsId) {
		InwardReturnGoodsRegistrationLRDetailsId = inwardReturnGoodsRegistrationLRDetailsId;
	}

	public String getLRNo() {
		return LRNo;
	}

	public void setLRNo(String lRNo) {
		LRNo = lRNo;
	}

	public Integer getNoOfCases() {
		return noOfCases;
	}

	public void setNoOfCases(Integer noOfCases) {
		this.noOfCases = noOfCases;
	}

	public String getRefNoOrClaimNo() {
		return refNoOrClaimNo;
	}

	public void setRefNoOrClaimNo(String refNoOrClaimNo) {
		this.refNoOrClaimNo = refNoOrClaimNo;
	}

	public Float getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(Float claimAmount) {
		this.claimAmount = claimAmount;
	}

	/*public String getLRImagePath() {
		return LRImagePath;
	}

	public void setLRImagePath(String lRImagePath) {
		LRImagePath = lRImagePath;
	}

	public String getClaimCopyImagePath() {
		return claimCopyImagePath;
	}

	public void setClaimCopyImagePath(String claimCopyImagePath) {
		this.claimCopyImagePath = claimCopyImagePath;
	}*/

	public InwardReturnGoodsRegistration getInwardReturnGoodsRegistration() {
		return inwardReturnGoodsRegistration;
	}

	public void setInwardReturnGoodsRegistration(
			InwardReturnGoodsRegistration inwardReturnGoodsRegistration) {
		this.inwardReturnGoodsRegistration = inwardReturnGoodsRegistration;
	}

	public List<InwardReturnProducts> getInwardReturnProductList() {
		return inwardReturnProductList;
	}

	public void setInwardReturnProductList(
			List<InwardReturnProducts> inwardReturnProductList) {
		this.inwardReturnProductList = inwardReturnProductList;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> getInwardReturnGoodsRegistrationLRDetailsLrImgPathList() {
		return inwardReturnGoodsRegistrationLRDetailsLrImgPathList;
	}

	public void setInwardReturnGoodsRegistrationLRDetailsLrImgPathList(
			List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> inwardReturnGoodsRegistrationLRDetailsLrImgPathList) {
		this.inwardReturnGoodsRegistrationLRDetailsLrImgPathList = inwardReturnGoodsRegistrationLRDetailsLrImgPathList;
	}

	public List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> getInwardReturnGoodsRegistrationLRDetailsClaimCopyPathList() {
		return inwardReturnGoodsRegistrationLRDetailsClaimCopyPathList;
	}

	public void setInwardReturnGoodsRegistrationLRDetailsClaimCopyPathList(
			List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> inwardReturnGoodsRegistrationLRDetailsClaimCopyPathList) {
		this.inwardReturnGoodsRegistrationLRDetailsClaimCopyPathList = inwardReturnGoodsRegistrationLRDetailsClaimCopyPathList;
	}
	
}
