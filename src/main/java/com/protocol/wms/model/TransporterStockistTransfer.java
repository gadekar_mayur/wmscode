package com.protocol.wms.model;


import java.util.List;
import java.sql.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "transporterstockisttransfer")

public class TransporterStockistTransfer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer transporterStockistTransferId;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "stockistId")
	private Stockist stockist;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "transporterStationDetailsId")
	private TransporterStationDetails transporterStationDetails;

	/*
	 * @OneToMany(targetEntity=TransporterStationDetails.class,cascade=
	 * CascadeType.ALL)
	 * 
	 * @JoinColumn(name="transporterStationDetailsId",referencedColumnName=
	 * "transporterStationDetailsId") private List<Stockist> stockistList;
	 * 
	 * @OneToMany(targetEntity=TransporterStationDetails.class,cascade=
	 * CascadeType.ALL)
	 * 
	 * @JoinColumn(name="transporterStationDetailsId",referencedColumnName=
	 * "transporterStationDetailsId") private List<TransporterStationDetails>
	 * transporterStationDetailsList;
	 */

	//@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	
	/*@JoinTable(name="transporterdetails"
	,joinColumns ={@JoinColumn(columnDefinition ="fromTransporterDetailsId", referencedColumnName="transporterDetailsId")})
	private TransporterDetails transporterDetails;*/

	@Column(name = "fromTransporterDetailsId")
	private Integer fromTransporterDetailsId;

	
	/*@JoinTable(name="transporterdetails"
	,joinColumns={@JoinColumn(columnDefinition ="toTransporterDetailsId", referencedColumnName="transporterDetailsId")})
	private TransporterDetails toTransporterDetails;*/
	
	@Column(name = "toTransporterDetailsId")
	private Integer toTransporterDetailsId;

	@Column(name = "adminApprovalStatus")
	private Integer adminApprovalStatus;
	
	@Column(name="transferSubmitDate")
	private java.util.Date transferSubmitDate;

	/**
	 * @return the drugLicenceList
	 */
	/*
	 * public List<Stockist> getStockistList() { return stockistList; }
	 */

	/**
	 * @param drugLicenceList
	 *            the drugLicenceList to set
	 */
	/*
	 * public void setStockistList(List<Stockist> stockistList) {
	 * this.stockistList = stockistList; }
	 */

	/**
	 * @return the drugLicenceList
	 */
	/*
	 * public List<TransporterStationDetails> getTransporterStationDetailsList()
	 * { return transporterStationDetailsList; }
	 */

	/**
	 * @param drugLicenceList
	 *            the drugLicenceList to set
	 */
	/*
	 * public void
	 * setTransporterStationDetailsList(List<TransporterStationDetails>
	 * transporterStationDetailsList) { this.transporterStationDetailsList =
	 * transporterStationDetailsList; }
	 */

	

	/**
	 * @return the adminApprovalStatus
	 */
	public Integer getAdminApprovalStatus() {
		return adminApprovalStatus;
	}

	public java.util.Date getTransferSubmitDate() {
		return transferSubmitDate;
	}

	public void setTransferSubmitDate(java.util.Date transferSubmitDate) {
		this.transferSubmitDate = transferSubmitDate;
	}

	/**
	 * @param adminApprovalStatus
	 *            the adminApprovalStatus to set
	 */
	public void setAdminApprovalStatus(Integer adminApprovalStatus) {
		this.adminApprovalStatus = adminApprovalStatus;
	}

	/**
	 * @return the toTransporterDetailsId
	 */

	public Integer getToTransporterDetailsId() {
		return toTransporterDetailsId;
	}

	/**
	 * @param toTransporterDetailsId
	 *            the toTransporterDetailsId to set
	 */
	public void setToTransporterDetailsId(Integer toTransporterDetailsId) {
		this.toTransporterDetailsId = toTransporterDetailsId;
	}

	/**
	 * @return the fromTransporterDetailsId
	 */

	public Integer getFromTransporterDetailsId() {
		return fromTransporterDetailsId;
	}

	/**
	 * @param fromTransporterDetailsId
	 *            the fromTransporterDetailsId to set
	 */
	public void setFromTransporterDetailsId(Integer fromTransporterDetailsId) {
		this.fromTransporterDetailsId = fromTransporterDetailsId;
	}

	/**
	 * @return the transporterStockistTransferId
	 */

	public Integer getTransporterStockistTransferId() {
		return transporterStockistTransferId;
	}

	/**
	 * @param transporterStockistTransferId
	 *            the transporterStockistTransferId to set
	 */
	public void setTransporterStockistTransferId(Integer transporterStockistTransferId) {
		this.transporterStockistTransferId = transporterStockistTransferId;
	}

	/**
	 * @return the stockist
	 */

	public Stockist getStockist() {
		return stockist;
	}

	/**
	 * @param stockist
	 *            the stockist to set
	 */
	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	/**
	 * @return the transporterStationDetails
	 */

	public TransporterStationDetails getTransporterStationDetails() {
		return transporterStationDetails;
	}

	/**
	 * @param transporterStationDetails
	 *            the transporterStationDetails to set
	 */
	public void setTransporterStationDetails(TransporterStationDetails transporterStationDetails) {
		this.transporterStationDetails = transporterStationDetails;
	}

}
