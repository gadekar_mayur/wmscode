/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="stockistdocument")
public class StockistDocument {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer stockistDocumentId;
	
//	private String imagePath;
	
	@ManyToOne
	@JoinColumn(name="companyId")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="stockistId")
	private Stockist stockist;
	
	@ManyToOne
	@JoinColumn(name="documentListTypeId")
	private DocumentListType documentListType;
	
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;
	
	private Integer status;
	
	private Date submitDate;

	@OneToMany(targetEntity=StockistDocumentImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="stockistDocumentId",referencedColumnName="stockistDocumentId")
	private List<StockistDocumentImagePath> stockistDocumentImagePathList;
	
	/**
	 * @return the stockistDocumentId
	 */
	public Integer getStockistDocumentId() {
		return stockistDocumentId;
	}

	/**
	 * @param stockistDocumentId the stockistDocumentId to set
	 */
	public void setStockistDocumentId(Integer stockistDocumentId) {
		this.stockistDocumentId = stockistDocumentId;
	}


	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the stockist
	 */
	public Stockist getStockist() {
		return stockist;
	}

	/**
	 * @param stockist the stockist to set
	 */
	public void setStockist(Stockist stockist) {
		this.stockist = stockist;
	}

	/**
	 * @return the documentListType
	 */
	public DocumentListType getDocumentListType() {
		return documentListType;
	}

	/**
	 * @param documentListType the documentListType to set
	 */
	public void setDocumentListType(DocumentListType documentListType) {
		this.documentListType = documentListType;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public List<StockistDocumentImagePath> getStockistDocumentImagePathList() {
		return stockistDocumentImagePathList;
	}

	public void setStockistDocumentImagePathList(
			List<StockistDocumentImagePath> stockistDocumentImagePathList) {
		this.stockistDocumentImagePathList = stockistDocumentImagePathList;
	}
}
