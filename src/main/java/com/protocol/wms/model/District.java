/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="district")
public class District {
	
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer districtId;
	
	private String districtName;
	
	private Integer status;
	
	@OneToMany(targetEntity=Stockist.class,cascade=CascadeType.ALL)
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private List<Stockist> stockistList;
	
	@OneToMany(targetEntity=TransporterDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private List<TransporterDetails> transporterDetailsList;

	@ManyToOne
	@JoinColumn(name="stateId")
	private State state;
	
	@OneToMany(targetEntity=InwardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private List<InwardCourierRegistration> inwardCourierRegistrationList;
	
	@OneToMany(targetEntity=OutWardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="districtId",referencedColumnName="districtId")
	private List<OutWardCourierRegistration> outWardCourierRegistrations;
	
	
	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}
	/**
	 * @return the districtId
	 */
	public Integer getDistrictId() {
		return districtId;
	}

	/**
	 * @param districtId the districtId to set
	 */
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	/**
	 * @return the districtName
	 */
	public String getDistrictName() {
		return districtName;
	}

	/**
	 * @param districtName the districtName to set
	 */
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the stockistList
	 */
	public List<Stockist> getStockistList() {
		return stockistList;
	}

	/**
	 * @param stockistList the stockistList to set
	 */
	public void setStockistList(List<Stockist> stockistList) {
		this.stockistList = stockistList;
	}

	public List<TransporterDetails> getTransporterDetailsList() {
		return transporterDetailsList;
	}

	public void setTransporterDetailsList(
			List<TransporterDetails> transporterDetailsList) {
		this.transporterDetailsList = transporterDetailsList;
	}

	public List<InwardCourierRegistration> getInwardCourierRegistrationList() {
		return inwardCourierRegistrationList;
	}

	public void setInwardCourierRegistrationList(
			List<InwardCourierRegistration> inwardCourierRegistrationList) {
		this.inwardCourierRegistrationList = inwardCourierRegistrationList;
	}
	

}
