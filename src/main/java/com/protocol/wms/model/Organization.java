/**
 * 
 */
package com.protocol.wms.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="organization")
public class Organization {
	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer organizationId;
	
	private String organizationName;
	
	private String organizationAddress;
	
	private String organizationLocation;
	
	private String organizationContactPerson;
	
	private Long organizationTelePhone;
	
	private Long organizationMobile;
	
	private String organizationEmailId;
	
	private String organizationWebSite;
	
	private String organizationStatus;
	

	private Date submitDate;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="userDetailsId")
	private UserDetails userDetails;
	
	@OneToOne(mappedBy="organization")
	private OrganizationTripCount organizationTripCount;
	
//	@OneToOne(mappedBy="organization")
//	private CartingAgentTripCount cartingAgentTripCount;
	
	@OneToMany(targetEntity=OrganizationBank.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<OrganizationBank> organizationBanksList;
	
	@OneToMany(targetEntity=OrganizationDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<OrganizationDocument> organizationDocumentList;
	
	@OneToMany(targetEntity=Company.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<Company> companyList;
	
	@OneToMany(targetEntity=CompanyBank.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<CompanyBank> companyBankList;
	
	@OneToMany(targetEntity=CompanyOrganizationBank.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<CompanyOrganizationBank> companyOrganizationBankList;
	
	@OneToMany(targetEntity=CompanyExecutive.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<CompanyExecutive> companyExecutiveList;
	
	@OneToMany(targetEntity=CompanyDepo.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<CompanyDepo> companyDepoList;
	
	@OneToMany(targetEntity=CompanyDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<CompanyDocument> companyDocumentList;
	
	@OneToMany(targetEntity=CompanyProduct.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<CompanyProduct> companyProductList;
	
	@OneToMany(targetEntity=CompanyCreditAndDiscount.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<CompanyCreditAndDiscount> companyCreditAndDiscountList;
	
	@OneToMany(targetEntity=EmployeeDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<EmployeeDetails> employeeDetailsList;
	
	@OneToMany(targetEntity=EmployeeDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<EmployeeDocument> employeeDocumentList;
	
	@OneToMany(targetEntity=TransporterDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<TransporterDetails> transporterDetailsList;
	
	@OneToMany(targetEntity=TransporterStationDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<TransporterStationDetails> transporterStationDetailsList;
	
	@OneToMany(targetEntity=TransporterDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<TransporterDocument> transporterDocumentList;
	
	@OneToMany(targetEntity=Stockist.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<Stockist> stockistList;
	
	@OneToMany(targetEntity=StockistBankDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<StockistBankDetails> stockistBankDetailsList;
	
	@OneToMany(targetEntity=StockistDocument.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<StockistDocument> stockistDocumentList;
	
	@OneToMany(targetEntity=StockistCompany.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<StockistCompany> stockistCompanyList;
	
	@OneToMany(targetEntity=InwardGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<InwardGoodsRegistration> inwardGoodsRegistrationList;
	
	@OneToMany(targetEntity=InwardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<InwardCourierRegistration> inwardCourierRegistrationList;
	
	@OneToMany(targetEntity=InwardReturnGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList;

	@OneToMany(targetEntity=CheckingDone.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<CheckingDone> checkingDoneList;
	
	@OneToMany(targetEntity=OutWardCourierRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<OutWardCourierRegistration> outWardCourierRegistrations;
	
	@OneToMany(targetEntity=OutWardReturnGoodsRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegistration;
	
	@OneToMany(targetEntity=OutWardOrderEntryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<OutWardOrderEntryRegistration> outWardOrderEnteryRegistrations;
	
	
	@OneToMany(targetEntity=OutWardTrip.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<OutWardTrip> outWardTripList;
	
	
	@OneToMany(targetEntity=OutWardOrderInvoiceEnteryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList;
	
	@OneToMany(targetEntity=OutWardDispatchEnteryRegistration.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationsList ;
	
	
	@OneToMany(targetEntity=OutWardGatePass.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<OutWardGatePass> outWardGatePasses  ;
	
	
	@OneToMany(targetEntity=AdvancedChequeInformation.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<AdvancedChequeInformation> advancedChequeInformations;   
	
	@OneToMany(targetEntity=StockistChequeDeposit.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<StockistChequeDeposit> stockistChequeDepositList;
	
	@OneToMany(targetEntity=StockistChequeReturn.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<StockistChequeReturn> stockistChequeReturnList;
	
	
	@OneToMany(targetEntity=CartingAgent.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<CartingAgent> cartingAgentList;
	
	
	@OneToMany(targetEntity=RemainingChequeAmount.class,cascade=CascadeType.ALL)
	@JoinColumn(name="organizationId",referencedColumnName="organizationId")
	private List<RemainingChequeAmount> remainingChequeAmountList;
	
	/**
	 * @return the organizationId
	 */
	public Integer getOrganizationId() {
		return organizationId;
	}

	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * @return the organizationName
	 */
	public String getOrganizationName() {
		return organizationName;
	}

	/**
	 * @param organizationName the organizationName to set
	 */
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	/**
	 * @return the organizationAddress
	 */
	public String getOrganizationAddress() {
		return organizationAddress;
	}

	/**
	 * @param organizationAddress the organizationAddress to set
	 */
	public void setOrganizationAddress(String organizationAddress) {
		this.organizationAddress = organizationAddress;
	}

	/**
	 * @return the organizationLocation
	 */
	public String getOrganizationLocation() {
		return organizationLocation;
	}

	/**
	 * @param organizationLocation the organizationLocation to set
	 */
	public void setOrganizationLocation(String organizationLocation) {
		this.organizationLocation = organizationLocation;
	}

	/**
	 * @return the organizationContactPerson
	 */
	public String getOrganizationContactPerson() {
		return organizationContactPerson;
	}

	/**
	 * @param organizationContactPerson the organizationContactPerson to set
	 */
	public void setOrganizationContactPerson(String organizationContactPerson) {
		this.organizationContactPerson = organizationContactPerson;
	}

	/**
	 * @return the organizationTelePhone
	 */
	public Long getOrganizationTelePhone() {
		return organizationTelePhone;
	}

	/**
	 * @param organizationTelePhone the organizationTelePhone to set
	 */
	public void setOrganizationTelePhone(Long organizationTelePhone) {
		this.organizationTelePhone = organizationTelePhone;
	}

	/**
	 * @return the organizationMobile
	 */
	public Long getOrganizationMobile() {
		return organizationMobile;
	}

	/**
	 * @param organizationMobile the organizationMobile to set
	 */
	public void setOrganizationMobile(Long organizationMobile) {
		this.organizationMobile = organizationMobile;
	}

	/**
	 * @return the organizationEmailId
	 */
	public String getOrganizationEmailId() {
		return organizationEmailId;
	}

	/**
	 * @param organizationEmailId the organizationEmailId to set
	 */
	public void setOrganizationEmailId(String organizationEmailId) {
		this.organizationEmailId = organizationEmailId;
	}

	/**
	 * @return the organizationWebSite
	 */
	public String getOrganizationWebSite() {
		return organizationWebSite;
	}

	/**
	 * @param organizationWebSite the organizationWebSite to set
	 */
	public void setOrganizationWebSite(String organizationWebSite) {
		this.organizationWebSite = organizationWebSite;
	}

	/**
	 * @return the organizationStatus
	 */
	public String getOrganizationStatus() {
		return organizationStatus;
	}

	/**
	 * @param organizationStatus the organizationStatus to set
	 */
	public void setOrganizationStatus(String organizationStatus) {
		this.organizationStatus = organizationStatus;
	}

	/**

	 * @return the submitDate
	 */
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the userDetails
	 */
	public UserDetails getUserDetails() {
		return userDetails;
	}

	/**
	 * @param userDetails the userDetails to set
	 */
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	/**
	 * @return the organizationBanksList
	 */
	public List<OrganizationBank> getOrganizationBanksList() {
		return organizationBanksList;
	}

	/**
	 * @param organizationBanksList the organizationBanksList to set
	 */
	public void setOrganizationBanksList(
			List<OrganizationBank> organizationBanksList) {
		this.organizationBanksList = organizationBanksList;
	}

	/**
	 * @return the organizationDocumentList
	 */
	public List<OrganizationDocument> getOrganizationDocumentList() {
		return organizationDocumentList;
	}

	/**
	 * @param organizationDocumentList the organizationDocumentList to set
	 */
	public void setOrganizationDocumentList(
			List<OrganizationDocument> organizationDocumentList) {
		this.organizationDocumentList = organizationDocumentList;
	}

	/**
	 * @return the companyList
	 */
	public List<Company> getCompanyList() {
		return companyList;
	}

	/**
	 * @param companyList the companyList to set
	 */
	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	/**
	 * @return the companyBankList
	 */
	public List<CompanyBank> getCompanyBankList() {
		return companyBankList;
	}

	/**
	 * @param companyBankList the companyBankList to set
	 */
	public void setCompanyBankList(List<CompanyBank> companyBankList) {
		this.companyBankList = companyBankList;
	}

	/**
	 * @return the companyOrganizationBankList
	 */
	public List<CompanyOrganizationBank> getCompanyOrganizationBankList() {
		return companyOrganizationBankList;
	}

	/**
	 * @param companyOrganizationBankList the companyOrganizationBankList to set
	 */
	public void setCompanyOrganizationBankList(
			List<CompanyOrganizationBank> companyOrganizationBankList) {
		this.companyOrganizationBankList = companyOrganizationBankList;
	}

	/**
	 * @return the companyExecutiveList
	 */
	public List<CompanyExecutive> getCompanyExecutiveList() {
		return companyExecutiveList;
	}

	/**
	 * @param companyExecutiveList the companyExecutiveList to set
	 */
	public void setCompanyExecutiveList(List<CompanyExecutive> companyExecutiveList) {
		this.companyExecutiveList = companyExecutiveList;
	}

	/**
	 * @return the companyDepoList
	 */
	public List<CompanyDepo> getCompanyDepoList() {
		return companyDepoList;
	}

	/**
	 * @param companyDepoList the companyDepoList to set
	 */
	public void setCompanyDepoList(List<CompanyDepo> companyDepoList) {
		this.companyDepoList = companyDepoList;
	}

	/**
	 * @return the companyDocumentList
	 */
	public List<CompanyDocument> getCompanyDocumentList() {
		return companyDocumentList;
	}

	/**
	 * @param companyDocumentList the companyDocumentList to set
	 */
	public void setCompanyDocumentList(List<CompanyDocument> companyDocumentList) {
		this.companyDocumentList = companyDocumentList;
	}

	/**
	 * @return the companyProductList
	 */
	public List<CompanyProduct> getCompanyProductList() {
		return companyProductList;
	}

	/**
	 * @param companyProductList the companyProductList to set
	 */
	public void setCompanyProductList(List<CompanyProduct> companyProductList) {
		this.companyProductList = companyProductList;
	}

	/**
	 * @return the companyCreditAndDiscountList
	 */
	public List<CompanyCreditAndDiscount> getCompanyCreditAndDiscountList() {
		return companyCreditAndDiscountList;
	}

	/**
	 * @param companyCreditAndDiscountList the companyCreditAndDiscountList to set
	 */
	public void setCompanyCreditAndDiscountList(
			List<CompanyCreditAndDiscount> companyCreditAndDiscountList) {
		this.companyCreditAndDiscountList = companyCreditAndDiscountList;
	}

	/**
	 * @return the employeeDetailsList
	 */
	public List<EmployeeDetails> getEmployeeDetailsList() {
		return employeeDetailsList;
	}

	/**
	 * @param employeeDetailsList the employeeDetailsList to set
	 */
	public void setEmployeeDetailsList(List<EmployeeDetails> employeeDetailsList) {
		this.employeeDetailsList = employeeDetailsList;
	}

	/**
	 * @return the employeeDocumentList
	 */
	public List<EmployeeDocument> getEmployeeDocumentList() {
		return employeeDocumentList;
	}

	/**
	 * @param employeeDocumentList the employeeDocumentList to set
	 */
	public void setEmployeeDocumentList(List<EmployeeDocument> employeeDocumentList) {
		this.employeeDocumentList = employeeDocumentList;
	}

	/**
	 * @return the transporterDetailsList
	 */
	public List<TransporterDetails> getTransporterDetailsList() {
		return transporterDetailsList;
	}

	/**
	 * @param transporterDetailsList the transporterDetailsList to set
	 */
	public void setTransporterDetailsList(
			List<TransporterDetails> transporterDetailsList) {
		this.transporterDetailsList = transporterDetailsList;
	}

	/**
	 * @return the transporterStationDetailsList
	 */
	public List<TransporterStationDetails> getTransporterStationDetailsList() {
		return transporterStationDetailsList;
	}

	/**
	 * @param transporterStationDetailsList the transporterStationDetailsList to set
	 */
	public void setTransporterStationDetailsList(
			List<TransporterStationDetails> transporterStationDetailsList) {
		this.transporterStationDetailsList = transporterStationDetailsList;
	}

	/**
	 * @return the transporterDocumentList
	 */
	public List<TransporterDocument> getTransporterDocumentList() {
		return transporterDocumentList;
	}

	/**
	 * @param transporterDocumentList the transporterDocumentList to set
	 */
	public void setTransporterDocumentList(
			List<TransporterDocument> transporterDocumentList) {
		this.transporterDocumentList = transporterDocumentList;
	}

	/**
	 * @return the stockistList
	 */
	public List<Stockist> getStockistList() {
		return stockistList;
	}

	/**
	 * @param stockistList the stockistList to set
	 */
	public void setStockistList(List<Stockist> stockistList) {
		this.stockistList = stockistList;
	}

	/**
	 * @return the stockistBankDetailsList
	 */
	public List<StockistBankDetails> getStockistBankDetailsList() {
		return stockistBankDetailsList;
	}

	/**
	 * @param stockistBankDetailsList the stockistBankDetailsList to set
	 */
	public void setStockistBankDetailsList(
			List<StockistBankDetails> stockistBankDetailsList) {
		this.stockistBankDetailsList = stockistBankDetailsList;
	}

	/**
	 * @return the stockistDocumentList
	 */
	public List<StockistDocument> getStockistDocumentList() {
		return stockistDocumentList;
	}

	/**
	 * @param stockistDocumentList the stockistDocumentList to set
	 */
	public void setStockistDocumentList(List<StockistDocument> stockistDocumentList) {
		this.stockistDocumentList = stockistDocumentList;
	}

	/**
	 * @return the stockistCompanyList
	 */
	public List<StockistCompany> getStockistCompanyList() {
		return stockistCompanyList;
	}

	/**
	 * @param stockistCompanyList the stockistCompanyList to set
	 */
	public void setStockistCompanyList(List<StockistCompany> stockistCompanyList) {
		this.stockistCompanyList = stockistCompanyList;
	}

	public List<InwardGoodsRegistration> getInwardGoodsRegistrationList() {
		return inwardGoodsRegistrationList;
	}

	public void setInwardGoodsRegistrationList(
			List<InwardGoodsRegistration> inwardGoodsRegistrationList) {
		this.inwardGoodsRegistrationList = inwardGoodsRegistrationList;
	}

	public List<InwardCourierRegistration> getInwardCourierRegistrationList() {
		return inwardCourierRegistrationList;
	}

	public void setInwardCourierRegistrationList(
			List<InwardCourierRegistration> inwardCourierRegistrationList) {
		this.inwardCourierRegistrationList = inwardCourierRegistrationList;
	}

	public List<InwardReturnGoodsRegistration> getInwardReturnGoodsRegistrationList() {
		return inwardReturnGoodsRegistrationList;
	}

	public void setInwardReturnGoodsRegistrationList(
			List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList) {
		this.inwardReturnGoodsRegistrationList = inwardReturnGoodsRegistrationList;
	}

	public List<CheckingDone> getCheckingDoneList() {
		return checkingDoneList;
	}

	public void setCheckingDoneList(List<CheckingDone> checkingDoneList) {
		this.checkingDoneList = checkingDoneList;
	}

	public List<OutWardCourierRegistration> getOutWardCourierRegistrations() {
		return outWardCourierRegistrations;
	}

	public void setOutWardCourierRegistrations(
			List<OutWardCourierRegistration> outWardCourierRegistrations) {
		this.outWardCourierRegistrations = outWardCourierRegistrations;
	}

	public List<OutWardReturnGoodsRegistration> getOutWardReturnGoodsRegistration() {
		return outWardReturnGoodsRegistration;
	}

	public void setOutWardReturnGoodsRegistration(
			List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegistration) {
		this.outWardReturnGoodsRegistration = outWardReturnGoodsRegistration;
	}

	public List<OutWardOrderEntryRegistration> getOutWardOrderEnteryRegistrations() {
		return outWardOrderEnteryRegistrations;
	}

	public void setOutWardOrderEnteryRegistrations(
			List<OutWardOrderEntryRegistration> outWardOrderEnteryRegistrations) {
		this.outWardOrderEnteryRegistrations = outWardOrderEnteryRegistrations;
	}

	public List<OutWardTrip> getOutWardTripList() {
		return outWardTripList;
	}

	public void setOutWardTripList(List<OutWardTrip> outWardTripList) {
		this.outWardTripList = outWardTripList;
	}

	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistrationList() {
		return outWardOrderInvoiceEnteryRegistrationList;
	}

	public void setOutWardOrderInvoiceEnteryRegistrationList(
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList) {
		this.outWardOrderInvoiceEnteryRegistrationList = outWardOrderInvoiceEnteryRegistrationList;
	}

	public List<OutWardDispatchEnteryRegistration> getOutWardDispatchEnteryRegistrationsList() {
		return outWardDispatchEnteryRegistrationsList;
	}

	public void setOutWardDispatchEnteryRegistrationsList(
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationsList) {
		this.outWardDispatchEnteryRegistrationsList = outWardDispatchEnteryRegistrationsList;
	}

	public List<OutWardGatePass> getOutWardGatePasses() {
		return outWardGatePasses;
	}

	public void setOutWardGatePasses(List<OutWardGatePass> outWardGatePasses) {
		this.outWardGatePasses = outWardGatePasses;
	}

	public List<AdvancedChequeInformation> getAdvancedChequeInformations() {
		return advancedChequeInformations;
	}

	public void setAdvancedChequeInformations(
			List<AdvancedChequeInformation> advancedChequeInformations) {
		this.advancedChequeInformations = advancedChequeInformations;
	}

	public List<StockistChequeDeposit> getStockistChequeDepositList() {
		return stockistChequeDepositList;
	}

	public void setStockistChequeDepositList(
			List<StockistChequeDeposit> stockistChequeDepositList) {
		this.stockistChequeDepositList = stockistChequeDepositList;
	}

	public List<StockistChequeReturn> getStockistChequeReturnList() {
		return stockistChequeReturnList;
	}

	public void setStockistChequeReturnList(
			List<StockistChequeReturn> stockistChequeReturnList) {
		this.stockistChequeReturnList = stockistChequeReturnList;
	}

	public List<CartingAgent> getCartingAgentList() {
		return cartingAgentList;
	}

	public void setCartingAgentList(List<CartingAgent> cartingAgentList) {
		this.cartingAgentList = cartingAgentList;
	}

	public List<RemainingChequeAmount> getRemainingChequeAmountList() {
		return remainingChequeAmountList;
	}

	public void setRemainingChequeAmountList(
			List<RemainingChequeAmount> remainingChequeAmountList) {
		this.remainingChequeAmountList = remainingChequeAmountList;
	}

	public OrganizationTripCount getOrganizationTripCount() {
		return organizationTripCount;
	}

	public void setOrganizationTripCount(OrganizationTripCount organizationTripCount) {
		this.organizationTripCount = organizationTripCount;
	}
	
	
	
}
