/**
 * 
 */
package com.protocol.wms.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Sudhakar
 *
 */
@Entity
@Table(name="inwardgoodsregistrationinvoicedetails")
public class InwardGoodsRegistrationInvoiceDetails {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer InwardGoodsRegistrationInvoiceDetailsId;
	
	private String stnNO;
	
	//private String stnNoImagePath;
	
	private Date stnDate;
	
	private Float grossAmount;
	
	private Float vatAmount;
	
	private Float discountAmount;

	private Float netAmount;
	
	@ManyToOne
	@JoinColumn(name="InwardGoodsRegistrationId")
	private InwardGoodsRegistration inwardGoodsRegistration;

	@OneToMany(targetEntity=InwardGoodsRegistrationInvoiceStnNoImagePath.class,cascade=CascadeType.ALL)
	@JoinColumn(name="InwardGoodsRegistrationInvoiceDetailsId",referencedColumnName="InwardGoodsRegistrationInvoiceDetailsId")
	private List<InwardGoodsRegistrationInvoiceStnNoImagePath> inwardGoodsRegistrationInvoiceStnNoImagePathList;
	
	public Integer getInwardGoodsRegistrationInvoiceDetailsId() {
		return InwardGoodsRegistrationInvoiceDetailsId;
	}

	public void setInwardGoodsRegistrationInvoiceDetailsId(
			Integer inwardGoodsRegistrationInvoiceDetailsId) {
		InwardGoodsRegistrationInvoiceDetailsId = inwardGoodsRegistrationInvoiceDetailsId;
	}

	public String getStnNO() {
		return stnNO;
	}

	public void setStnNO(String stnNO) {
		this.stnNO = stnNO;
	}

/*	public String getStnNoImagePath() {
		return stnNoImagePath;
	}

	public void setStnNoImagePath(String stnNoImagePath) {
		this.stnNoImagePath = stnNoImagePath;
	}*/

	public Date getStnDate() {
		return stnDate;
	}

	public void setStnDate(Date stnDate) {
		this.stnDate = stnDate;
	}

	public Float getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(Float grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Float getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(Float vatAmount) {
		this.vatAmount = vatAmount;
	}

	public Float getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Float discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Float getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(Float netAmount) {
		this.netAmount = netAmount;
	}

	public InwardGoodsRegistration getInwardGoodsRegistration() {
		return inwardGoodsRegistration;
	}

	public void setInwardGoodsRegistration(
			InwardGoodsRegistration inwardGoodsRegistration) {
		this.inwardGoodsRegistration = inwardGoodsRegistration;
	}

	/**
	 * @return the inwardGoodsRegistrationInvoiceStnNoImagePathList
	 */
	public List<InwardGoodsRegistrationInvoiceStnNoImagePath> getInwardGoodsRegistrationInvoiceStnNoImagePathList() {
		return inwardGoodsRegistrationInvoiceStnNoImagePathList;
	}

	/**
	 * @param inwardGoodsRegistrationInvoiceStnNoImagePathList the inwardGoodsRegistrationInvoiceStnNoImagePathList to set
	 */
	public void setInwardGoodsRegistrationInvoiceStnNoImagePathList(
			List<InwardGoodsRegistrationInvoiceStnNoImagePath> inwardGoodsRegistrationInvoiceStnNoImagePathList) {
		this.inwardGoodsRegistrationInvoiceStnNoImagePathList = inwardGoodsRegistrationInvoiceStnNoImagePathList;
	}
}
