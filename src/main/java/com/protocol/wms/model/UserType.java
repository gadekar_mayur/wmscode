/**
 * 
 */
package com.protocol.wms.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author admin
 *
 */
@Entity
@Table(name="usertype")
public class UserType {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	Integer  userTypeId;
	
	private String userTypeName;
	
	@OneToMany(targetEntity=UserDetails.class,cascade=CascadeType.ALL)
	@JoinColumn(name="userTypeId",referencedColumnName="userTypeId")
	private List<UserDetails> userDetailsList;
	
	@OneToMany(targetEntity=MenuAuthentication.class,cascade=CascadeType.ALL)
	@JoinColumn(name="userTypeId",referencedColumnName="userTypeId")
	private List<MenuAuthentication> menuAuthenticationList;

	/**
	 * @return the userTypeId
	 */
	public Integer getUserTypeId() {
		return userTypeId;
	}

	/**
	 * @param userTypeId the userTypeId to set
	 */
	public void setUserTypeId(Integer userTypeId) {
		this.userTypeId = userTypeId;
	}

	/**
	 * @return the userTypeName
	 */
	public String getUserTypeName() {
		return userTypeName;
	}

	/**
	 * @param userTypeName the userTypeName to set
	 */
	public void setUserTypeName(String userTypeName) {
		this.userTypeName = userTypeName;
	}

	/**
	 * @return the userDetailsList
	 */
	public List<UserDetails> getUserDetailsList() {
		return userDetailsList;
	}

	/**
	 * @param userDetailsList the userDetailsList to set
	 */
	public void setUserDetailsList(List<UserDetails> userDetailsList) {
		this.userDetailsList = userDetailsList;
	}

	/**
	 * @return the menuAuthenticationList
	 */
	public List<MenuAuthentication> getMenuAuthenticationList() {
		return menuAuthenticationList;
	}

	/**
	 * @param menuAuthenticationList the menuAuthenticationList to set
	 */
	public void setMenuAuthenticationList(
			List<MenuAuthentication> menuAuthenticationList) {
		this.menuAuthenticationList = menuAuthenticationList;
	}
	
}
