package com.protocol.wms.service;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.dao.DashboardDao;

@Service("DashboardServiceImpl")
public class DashboardServiceImpl implements DashboardService {
	
	@Autowired
	@Qualifier("DashboardDaoImpl")
	private DashboardDao dashboardDao;

	@Override
	public JSONArray loadDashboardTable(Integer[] organizations,
			Integer[] companies, Integer[] stockists, Integer year,Integer organizationId) {
		JSONArray jarray=new JSONArray();
		JSONObject jobject=null;
		Map<String, Object> tableData =null;
		/*if(Constant.ContantTableData !=null) {
			tableData=Constant.ContantTableData;
		}
		else{*/
			tableData=dashboardDao.loadDashboardTable(organizations, companies, stockists, year, organizationId);
			/*tableData=Constant.ContantTableData;
		}*/
		try {
			for(Integer i=4;i<14;i++){
				jobject=new JSONObject();
				jobject.put("outward_cases", tableData.get("outward_cases_"+i));
				jobject.put("inward_cases", tableData.get("inward_cases_"+i));
				jobject.put("inward_courier", tableData.get("inward_courier_"+i));
				jobject.put("outward_courier", tableData.get("outward_courier_"+i));
				jobject.put("orders_received",tableData.get("order_received_"+i));
				jobject.put("invoice_generated",tableData.get("invoice_generated_"+i));
				jobject.put("cheque_received", tableData.get("cheque_received_"+i));
				jobject.put("cheque_deposited", tableData.get("cheque_deposited_"+i));
				jobject.put("cheque_returned", tableData.get("cheque_returned_"+i));
				jobject.put("sales_amount", tableData.get("sales_amount_"+i));
				jobject.put("return_claim", tableData.get("return_claim_"+i));
				jobject.put("claim_amount", tableData.get("claim_amount_"+i));
				jobject.put("credit_notes", tableData.get("credit_notes_"+i));
				jobject.put("credit_amount", tableData.get("credit_amount_"+i));
				jobject.put("checking_pendings", tableData.get("checking_pendings_"+i));
				jobject.put("cn_pendings",tableData.get("cn_pendings_"+i));
				jobject.put("month", Constant.MONTHS[i]);
				
				jarray.put(jobject);
				
				if(i==12)
					i=0;
				if(i==3)
					i=12;
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return jarray;
	}

}
