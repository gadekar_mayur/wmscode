/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.OrganizationBankDao;
import com.protocol.wms.dao.OrganizationDao;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationBank;

/**
 * @author Sudhakar
 *
 */
@Service("OrganizationBankServiceImpl")
public class OrganizationBankServiceImpl implements OrganizationBankService{
	
	private OrganizationBankDao organizationBankDao ;
	
	private OrganizationDao organizationDao;

	
	@Autowired
	@Qualifier("OrganizationDaoImpl")
	public void setOrganizationDao(OrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	@Autowired
	@Qualifier("OrganizationBankDaoImpl")
	public void setOrganizationBankDao(OrganizationBankDao organizationBankDao) {
		this.organizationBankDao = organizationBankDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationBankService#saveOrganizationBank(com.protocol.wms.model.OrganizationBank)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOrganizationBank(OrganizationBank organizationBank) {
		return organizationBankDao.saveOrganizationBank(organizationBank);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationBankService#loadOrganizationBanksListUsingOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OrganizationBank> loadOrganizationBanksListUsingOrganizationId(
			Integer organizationId) {
		return organizationBankDao.loadOrganizationBanksListUsingOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationBankService#searchOrganiztionBankDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchOrganiztionBankDetails(
			String searchOption, String searchText,Integer from, Integer organizationId) {
		// TODO Auto-generated method stub
		return organizationBankDao.searchOrganiztionBankDetails(searchOption, searchText,from, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationBankService#deleteOrganizationBank(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOrganizationBank(Integer organizationBankId) {
		// TODO Auto-generated method stub
		return organizationBankDao.deleteOrganizationBank(organizationBankId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OrganizationBank loadOrganizationBankObjectUsingOrganizationBankId(
			Integer organizationBankId) {
		// TODO Auto-generated method stub
		return organizationBankDao.loadOrganizationBankObjectUsingOrganizationBankId(organizationBankId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OrganizationBank loadOrganizationankObjUsingOrganizationBankId(
			Integer organizationBankId) {
		return organizationBankDao.loadOrganizationBankObjectUsingOrganizationBankId(organizationBankId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Organization loadOrganizationObjectUsingOrganiztionId(
			Integer organizationId) {
		return organizationDao.loadOrganizationObjectUsingOrganiztionId(organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateOrganizationBankObj(OrganizationBank organizationBankObj) {
		return organizationBankDao.updateOrganizationBankObj(organizationBankObj);		
	}

	

	
}
