/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import com.protocol.wms.model.MenuAuthentication;

/**
 * @author admin
 *
 */
public interface MenuAuthenticationService {
	
	public List<MenuAuthentication> loadMainMenuUsingUserTypeId(Integer userTypeId);

}
