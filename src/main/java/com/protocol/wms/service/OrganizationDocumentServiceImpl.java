/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.hibernate.mapping.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.DocumentListTypeDao;
import com.protocol.wms.dao.OrganizationDocumentDao;
import com.protocol.wms.model.DocumentListType;
import com.protocol.wms.model.OrganizationDocument;
import com.protocol.wms.model.OrganizationDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
@Service("OrganizationDocumentServiceImpl")
public class OrganizationDocumentServiceImpl implements OrganizationDocumentService{
	
	private OrganizationDocumentDao organizationDocumentDao;

	@Autowired
	@Qualifier("DocumentListTypeDaoImpl")
	private DocumentListTypeDao documentListTypeDao;
	
	@Autowired
	@Qualifier("OrganizationDocumentDaoImpl")
	public void setOrganizationDocumentDao(
			OrganizationDocumentDao organizationDocumentDao) {
		this.organizationDocumentDao = organizationDocumentDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationDocumentService#saveOrganizationDocument(com.protocol.wms.model.OrganizationDocument)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOrganizationDocument(OrganizationDocument organizationDocumentObj) {
		return organizationDocumentDao.saveOrganizationDocument(organizationDocumentObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationDocumentService#loadOrganizationDocumentsUsingOrganiztionId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OrganizationDocument> loadOrganizationDocumentsListUsingOrganiztionId(
			Integer organizationId) {
		return organizationDocumentDao.loadOrganizationDocumentsListUsingOrganiztionId(organizationId);
	}

	/*@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray getOrganizationDocumentsListUsingOrganiztionId(Integer organizationId, Integer from) throws Exception {
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		{
			java.util.Map<String, Object> map = organizationDocumentDao.getOrganizationDocumentsListUsingOrganiztionId(organizationId, from);
			List<OrganizationDocument> organizationDocumentList = (List<OrganizationDocument>) map.get("orgDocList");
			Integer totalPage = (Integer) map.get("totalPages");
			if(organizationDocumentList.size()>0)
			{
				for(OrganizationDocument obj :organizationDocumentList)
				{
					if("1".equals(obj.getOrganization().getOrganizationStatus())){
					json=new JSONObject();
					json.put("ORG_DOCUMENT_ID", obj.getOrganizationDocumentId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("DOCUMENT_TYPE_ID", obj.getDocumentListType().getDocumentListTypeId());
					json.put("DOCUMENT_TYPE", obj.getDocumentListType().getDocumentName());
					json_ImageArray=new JSONArray();
					for(OrganizationDocumentImagePath tempObj : obj.getOrganizationDocumentImagePathList()){
						tempJson = new JSONObject();
						tempJson.put("IMAGE_PATH", tempObj.getImagePath());
						tempJson.put("IMAGE_ID", tempObj.getOrganizationDocumentImagePathId());
						json_ImageArray.put(tempJson);
					}
					json.put("FILE_NAME",json_ImageArray);
					json_data_array.put(json);
					}
				}
				json=new JSONObject();
				json.put("paginationCount", totalPage);
				//json.put("from", from);
				json_data_array.put(json);
			}
		}
		return json_data_array;
	}*/

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationDocumentService#searchOrgaiztionDocumentDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray searchOrgaiztionDocumentDetails(
			String searchOption, String searchText, Integer organizationId, Integer from) throws JSONException {
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		java.util.Map<String, Object>map = null;
		{
			map = organizationDocumentDao.searchOrgaiztionDocumentDetails(searchOption, searchText, organizationId,from);
			List<OrganizationDocument> organizationDocumentList  = (List<OrganizationDocument>) map.get("orgDocList");
			Integer totalPage = (Integer) map.get("totalPages");
			if(organizationDocumentList.size()>0)
			{
				for(OrganizationDocument obj :organizationDocumentList)
				{
					json=new JSONObject();
					json.put("ORG_DOCUMENT_ID", obj.getOrganizationDocumentId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("DOCUMENT_TYPE_ID", obj.getDocumentListType().getDocumentListTypeId());
					json.put("DOCUMENT_TYPE", obj.getDocumentListType().getDocumentName());
					json_ImageArray=new JSONArray();
					for(OrganizationDocumentImagePath tempObj : obj.getOrganizationDocumentImagePathList()){
						tempJson = new JSONObject();
						tempJson.put("IMAGE_PATH", tempObj.getImagePath());
						tempJson.put("IMAGE_ID", tempObj.getOrganizationDocumentImagePathId());
						json_ImageArray.put(tempJson);
					}
					json.put("FILE_NAME",json_ImageArray);
					json_data_array.put(json);
				}
			}
			json=new JSONObject();
			json.put("paginationCount", totalPage);
			json_data_array.put(json);
		} 
		return json_data_array;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateOrganizationUploadDocumentImage(Integer organizationDocumentId,List<OrganizationDocumentImagePath> documentList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		documentList = organizationDocumentDao.updateOrganizationUploadDocumentImage(organizationDocumentId, documentList);
		for(OrganizationDocumentImagePath tempObj : documentList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getOrganizationDocumentImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOrganizationDocumentFileImage(Integer orgDocumentPathId) {
		return organizationDocumentDao.deleteOrganizationDocumentFileImage(orgDocumentPathId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject updateOrganizationDocumentObj(Integer documentTypeId,
			Integer organiztionDocId) throws JSONException {
		JSONObject result= new JSONObject();
		OrganizationDocument organizationDocument = null;
		DocumentListType documentListType = null;
		{
			documentListType = documentListTypeDao.loadDocumentListTypeUsingDocumentListTypeId(documentTypeId);
			organizationDocument = organizationDocumentDao.loadOrganizationDocumentObjById(organiztionDocId);
			organizationDocument.setDocumentListType(documentListType);
			result.put("MSG", "Organization Uploaded Document Details Updation failed...!");
			if(organizationDocumentDao.updateOrganizationDocumentObj(organizationDocument)){
				result.put("RESULT", true);
				result.put("MSG", "Organization Uploaded Document Details Updated Successfully...!");
				result.put("DOCUMENT_TYPE_ID", organizationDocument.getDocumentListType().getDocumentListTypeId());
				result.put("DOCUMENT_TYPE", organizationDocument.getDocumentListType().getDocumentName());
			}
		}
		return result;
	}
	
}
