/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.OutWardReturnGoodsCreditNote;
import com.protocol.wms.model.OutWardReturnGoodsCreditNoteCreditNoteImagePath;

/**
 * @author admin
 *
 */
public interface OutWardReturnGoodsCreditNoteService {

	public Boolean saveOutwardReturnGoodsRegCreditNote(Integer organiztionId, OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote);
	
	public JSONArray getOutwardReturnGoodsRegCreditNoteList(Integer organizationId) throws Exception;
	
	public Boolean deleteOutwardReturnGoodsRegCreditNote(Integer creditNoteId);
	
	public OutWardReturnGoodsCreditNote loadOutWardReturnGoodsCreditNoteObjById(Integer outWardReturnGoodsCreditNoteId);
	
	public JSONObject editOutwardReturnGoodsRegCreditNote(Integer organiztionId,OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote) throws Exception;

	public JSONArray searchOutwardReturnGoodsRegCreditNote(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from) throws Exception;
	
	public JSONArray updateOutwardGoodsRegCreditNoteAttachImage(Integer creditNoteId, List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> creditNoteAttachList) throws Exception;
	
	public Boolean deleteOutwardGoodsRegCreditNoteAttachImage(Integer creditNoteAttachImageId);
}
