/**
 * 
 */
package com.protocol.wms.service;

import com.protocol.wms.model.CompanyProduct;

/**
 * @author admin
 *
 */
public interface CompanyProductService {

	public Boolean updateCompanyProductDetails(Boolean insertNewProductFlag,Integer companyProductId, CompanyProduct companyProduct);
}
