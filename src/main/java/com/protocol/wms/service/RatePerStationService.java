/**
 * 
 */
package com.protocol.wms.service;

import com.protocol.wms.model.RatePerStation;

/**
 * @author Sudhakar
 *
 */
public interface RatePerStationService {
	
	public Integer saveRatePerStationObject(RatePerStation ratePerStationObj);

}
