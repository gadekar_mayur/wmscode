/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.Organization;



/**
 * @author admin
 *
 */


public interface OrganizationService {
	
	
	public Integer saveOrganization(Organization organizationObj);
	
	public Boolean editOrganizationDetails(Organization organizationObj);
	
	public void updateOrganization(Organization organizationObj);
	
	public Organization loadOrganizationObjectUsingOrganiztionId(Integer organizationId);
	
	public List<Organization> loadListOfOrganization();

	public Map<String , Object> searchOrganizationDetails(String searchOption,String searchText, Integer from);
	
	public Organization loadOrganiztionObjectUsingUserDetailsId(Integer userDetailsId);
	
	public Boolean deleteOrganiztion(Integer organizationId);
	
	public Boolean deleteOrganizationDocument(Integer organizationDocumentId);
}
