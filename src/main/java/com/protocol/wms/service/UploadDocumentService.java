package com.protocol.wms.service;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;


public interface UploadDocumentService {
	public String uploadReport(String file, HttpServletRequest request, String reportName) throws IOException,InvalidFormatException,SQLException;
}
