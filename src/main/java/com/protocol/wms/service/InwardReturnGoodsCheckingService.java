/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.CreditNote;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnProducts;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.Stockist;

/**
 * @author Sudhakar
 *
 */
public interface InwardReturnGoodsCheckingService {

	public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationId(Integer organizationId);
	
	public Organization loadOrganizationObjectUsingOrganiztionId(Integer organizationId);
	
	public Stockist loadStockistObjUsigStokistId(Integer stokistId);
	
	public EmployeeDetails loadEmployeeObjectUsingEmployeeDetailsId(Integer empDetailsId);
	
	public Integer saveCheckingDone(CheckingDone checkingDoneObj);
	
	public InwardReturnGoodsRegistration loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(Integer InwardReturnGoodsRegistrationoId);
	
	public void updateInwardReturnGoodsRegistrationObj(InwardReturnGoodsRegistration inwardReturnGoodsRegistrationObj);
	
	public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOne(Integer organizationId);
	
	public Integer saveCreditNoteObj(CreditNote creditNoteObj);
	
	public CreditNote loadCreditNoteObjectUsingCreditNoteId(Integer creditNoteId);
	
	public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne(Integer organizationId);
	
	public Boolean addProductsToInwardReturnGoodsLRInCheckingPending(Integer lrId,List<InwardReturnProducts> inwardReturnProductsList);
}
