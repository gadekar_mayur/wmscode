
/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CompanyDepoDao;
import com.protocol.wms.model.CompanyDepo;

/**
 * @author admin
 *
 */
@Service("CompanyDepoServiceImpl")
public class CompanyDepoServiceImpl implements CompanyDepoService {

	private CompanyDepoDao companyDepoDao;
	
	@Autowired
	@Qualifier("CompanyDepoDaoImpl")
	public void setCompanyDepoDao(CompanyDepoDao companyDepoDao) {
		this.companyDepoDao = companyDepoDao;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyDepoService#saveCompanyDepoDetails(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyDepo)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveCompanyDepoDetails(Integer organizationId,
			Integer companyId, Integer stateId, Integer districtId,
			Integer cityId, CompanyDepo companyDepo) {
		// TODO Auto-generated method stub
		return companyDepoDao.saveCompanyDepoDetails(organizationId, companyId, stateId, districtId, cityId, companyDepo);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyDepoService#getCompanyDepoListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyDepo> getCompanyDepoListing(Integer organizationId) {
		// TODO Auto-generated method stub
		return companyDepoDao.getCompanyDepoListing(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyDepoService#searchCompanyDepotDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchCompanyDepotDetails(String searchOption,
			String searchText,Integer from, Integer organizationId) {
		// TODO Auto-generated method stub
		return companyDepoDao.searchCompanyDepotDetails(searchOption, searchText,from, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyDepoService#getCompanyDepoListByCompanyId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyDepo> getCompanyDepoListByCompanyId(Integer companyId) {
		// TODO Auto-generated method stub
		return companyDepoDao.getCompanyDepoListByCompanyId(companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyDepoService#deleteCompanyDepo(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCompanyDepo(Integer depoId) {
		// TODO Auto-generated method stub
		return companyDepoDao.deleteCompanyDepo(depoId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyDepoService#loadCompanyDepoObjectUsignCompanyDepoId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyDepo loadCompanyDepoObjectUsignCompanyDepoId(
			Integer companyDepoId) {
		// TODO Auto-generated method stub
		return companyDepoDao.loadCompanyDepoObjectUsignCompanyDepoId(companyDepoId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyDepoService#insertNewCompanyDepoDetailsAndDeleteExistingRecord(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyDepo)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer insertNewCompanyDepoDetailsAndDeleteExistingRecord(
			Integer stateId, Integer districtId, Integer cityId,
			Integer companyDepoId, CompanyDepo companyDepo) {
		// TODO Auto-generated method stub
		return companyDepoDao.insertNewCompanyDepoDetailsAndDeleteExistingRecord(stateId, districtId, cityId, companyDepoId, companyDepo);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyDepoService#updateCompanyDepoDetails(com.protocol.wms.model.CompanyDepo)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateCompanyDepoDetails(CompanyDepo companyDepo) {
		// TODO Auto-generated method stub
		return companyDepoDao.updateCompanyDepoDetails(companyDepo);
	}
	
}
