/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.RoleTypeDao;
import com.protocol.wms.model.RoleType;

/**
 * @author Sudhakar
 *
 */
@Service("RoleTypeerviceImpl")
public class RoleTypeServiceImpl implements RoleTypeService{

	private RoleTypeDao roleTypeDao;

	@Autowired
	@Qualifier("RoleTypeDaoImpl")
	public void setRoleTypeDao(RoleTypeDao roleTypeDao) {
		this.roleTypeDao = roleTypeDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.RoleTypeService#loadRoleTypeList()
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<RoleType> loadRoleTypeList() {
		return roleTypeDao.loadRoleTypeList();
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.RoleTypeService#loadRoleTypeUsingRoleTypeId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public RoleType loadRoleTypeUsingRoleTypeId(Integer roleTypeId) {
		return roleTypeDao.loadRoleTypeUsingRoleTypeId(roleTypeId);
	}
	
	
}
