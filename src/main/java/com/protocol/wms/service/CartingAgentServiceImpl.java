/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.dao.CartingAgentDao;
import com.protocol.wms.dao.CartingAgentUploadedDocumentDao;
import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentImagePath;
import com.protocol.wms.model.CartingAgentUploadedDocument;
import com.protocol.wms.model.CartingAgentUploadedDocumentImagePath;
import com.protocol.wms.model.CartingAgentVehiclePhotoImagePath;
import com.protocol.wms.model.UserDetails;

/**
 * @author admin
 *
 */
@Service("CartingAgentServiceImpl")
public class CartingAgentServiceImpl implements CartingAgentService {

	private Logger log = Logger.getLogger(CartingAgentServiceImpl.class.getClass());
	@Autowired
	@Qualifier("CartingAgentDaoImpl")
	private CartingAgentDao cartingAgentDao;
	
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	private UserDetailsService userDetailsService;
	
	@Autowired
	@Qualifier("CartingAgentUploadedDocumentDaoImpl")
	private CartingAgentUploadedDocumentDao cartingAgentUploadedDocumentDao;
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CartingAgentService#saveCartingAgentDetails(org.springframework.web.multipart.MultipartFile, org.springframework.web.multipart.MultipartFile, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CartingAgent)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveCartingAgentDetails(Integer organizationId,
		    CartingAgent cartingAgent,UserDetails userDetails) {
			return cartingAgentDao.saveCartingAgentDetails(organizationId, cartingAgent,userDetails);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CartingAgentService#getCartingAgentList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CartingAgent> getCartingAgentList(Integer organizationId) {
		// TODO Auto-generated method stub
		return cartingAgentDao.getCartingAgentList(organizationId);
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CartingAgentService#getCartingAgentDetailsById(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public String getCartingAgentDetailsById(Integer agentId) {
		JSONObject tempJson = null;
		CartingAgent cartingAgent = null;
		JSONObject tempJson1 = null;
		JSONArray json_ImageArray = null;
		try{
			cartingAgent = cartingAgentDao.getCartingAgentDetailsById(agentId);
			tempJson = new JSONObject();
			if(cartingAgent!=null){
				tempJson.put("ORG_ID", cartingAgent.getOrganization().getOrganizationId());
				tempJson.put("ORG_NAME", cartingAgent.getOrganization().getOrganizationName());
				//tempJson.put("TRAN_NAME", cartingAgent.getTransporterDetails().getTransporterName());
				tempJson.put("AGENT_NAME", cartingAgent.getName());
				tempJson.put("VEHICLE_NO", cartingAgent.getVehicleNumber());
				tempJson.put("VEHICLE_MODEL", cartingAgent.getModel());
				tempJson.put("MOBILE", cartingAgent.getMobileNo());
				tempJson.put("AGENT_ID", cartingAgent.getCartingAgentId());
				json_ImageArray=new JSONArray();
				for(CartingAgentImagePath tempObj : cartingAgent.getCartingAgentImagePathList()){
					tempJson1 = new JSONObject();
					tempJson1.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson1.put("IMAGE_ID", tempObj.getCartingAgentImagePathId());
					json_ImageArray.put(tempJson1);
				}
				tempJson.put("AGENT_PHOTO", json_ImageArray);
				json_ImageArray=new JSONArray();
				for(CartingAgentVehiclePhotoImagePath tempObj : cartingAgent.getCartingAgentVehiclePhotoImagePathList()){
					tempJson1 = new JSONObject();
					tempJson1.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson1.put("IMAGE_ID", tempObj.getCartingAgentVehiclePhotoImagePathId());
					json_ImageArray.put(tempJson1);
				}
				tempJson.put("VEHICLE_PHOTO", json_ImageArray);
				tempJson.put("ADDRESS", cartingAgent.getAddress());
				tempJson.put("USER_NAME", cartingAgent.getUserDetails().getUserName());
				tempJson.put("PASSWORD", cartingAgent.getUserDetails().getPassword());
				//tempJson.put("TRANSPORTER", cartingAgent.getTransporterDetails().getTransporterName());
			}
			else{
				tempJson.put("MSG", "Details Not Available ....!");
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return tempJson.toString();
		
		
//		return cartingAgentDao.getCartingAgentDetailsById(agentId);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CartingAgentService#deleteCartingAgentDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCartingAgentDetails(Integer agentId) {
		return cartingAgentDao.deleteCartingAgentDetails(agentId);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CartingAgentService#saveCartingAgentUploadDocument(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveCartingAgentUploadDocument(Integer organizationId,Integer cartingAgentId, Integer documentTypeId,CartingAgentUploadedDocument cartingAgentUploadedDocument) {
		
		return cartingAgentUploadedDocumentDao.saveCartingAgentUploadDocument(organizationId, cartingAgentId, documentTypeId,cartingAgentUploadedDocument);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CartingAgentService#getCartingAgentUploadedDocList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray getCartingAgentUploadedDocList(Integer organizationId) throws JSONException {
		JSONObject tempJson = null;
		JSONArray cartingAgentDocArray = null;
		List<CartingAgentUploadedDocument> docList =null;
		JSONObject tempJson1 = null;
		JSONArray json_ImageArray = null;
		{
			docList = cartingAgentUploadedDocumentDao.getCartingAgentUploadedDocList(organizationId);
			cartingAgentDocArray = new JSONArray();
			for(CartingAgentUploadedDocument tempObj:docList){
				tempJson = new JSONObject();
				tempJson.put("DOC_ID", tempObj.getCartingAgentUploadedDocumentId());
				tempJson.put("ORG_ID", tempObj.getOrganization().getOrganizationId());
				tempJson.put("ORG_NAME", tempObj.getOrganization().getOrganizationName());
				tempJson.put("AGENT_ID", tempObj.getCartingAgent().getCartingAgentId());
				tempJson.put("AGENT_NAME", tempObj.getCartingAgent().getName());
				tempJson.put("DOC_TYPE_ID", tempObj.getDocumentListType().getDocumentListTypeId());
				tempJson.put("DOC_TYPE", tempObj.getDocumentListType().getDocumentName());
				json_ImageArray=new JSONArray();
				for(CartingAgentUploadedDocumentImagePath tempObj1 : tempObj.getCartingAgentUploadedDocumentImagePathList()){
					tempJson1 = new JSONObject();
					tempJson1.put("IMAGE_PATH", tempObj1.getImagePath());
					tempJson1.put("IMAGE_ID", tempObj1.getCartingAgentUploadedDocumentImagePathId());
					json_ImageArray.put(tempJson1);
				}
				tempJson.put("DOCUMENT", json_ImageArray);
				cartingAgentDocArray.put(tempJson);
			}
		}
		return cartingAgentDocArray;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CartingAgentService#deleteCartingAgentDocumentDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCartingAgentDocumentDetails(Integer docId) {
		return cartingAgentUploadedDocumentDao.deleteCartingAgentDocumentDetails(docId);
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchCartingAgentsDetails(int searchOptioinINT,String searchText, Integer from, Integer organizationId) 
	{
		return cartingAgentDao.searchCartingAgentsDetails(searchOptioinINT,searchText,from,organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public String updateCartingAgentDetails(Integer organizationId,
			String agentName, Long mobile, MultipartFile agentPhoto,
			String address, String vehicleNo, String vehicleModel,
			MultipartFile vehiclePhoto, String userName, String password) {
//		JSONObject result = null;
//		CartingAgent cartingAgent =null;
//		UserDetails userDetails=null;
//		try{
////			userDetails = userDetailsService.checkUserNameAlreadyExitOrNot(userName);
////			if(userDetails!=null){
////				result = new JSONObject();
////				result.put("MSG","User Name already exists");
////				result.put("FLAG", true);
////				return result.toString();
////			}
//			cartingAgent = new CartingAgent();
//			cartingAgent.setAddress(address);
//			cartingAgent.setModel(vehicleModel);
//			cartingAgent.setName(agentName);
//			cartingAgent.setVehicleNumber(vehicleNo);
//			cartingAgent.setStatus(1);
//			cartingAgent.setMobileNo(String.valueOf(mobile));
//			UserDetails userDetailsObj = cartingAgent.getUserDetails();
//			userDetailsObj.setUserName(userName);
//			userDetailsObj.setPassword(password);
//			//set organization submit date
//			Date today = new Date();
//			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
//		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
//		    String IST = df.format(today);
//		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
//		    Date date = df2.parse(IST);
//		    cartingAgent.setSubmitDate(date);
//		    
//		    if(!"".equals(userName.trim())&&!"".equals(password.trim())&&userName!=null&&password!=null){
//				userDetails = new UserDetails();
//				userDetails.setUserName(userName);
//				userDetails.setPassword(password);
//			}
//		    result = new JSONObject();
//		    if(saveCartingAgentDetails(agentPhoto,vehiclePhoto,organizationId,cartingAgent,userDetails)){
//		    	result.put("result", true);
//		    	result.put("MSG","Add Catering Agent Registration Successful....!");
//		    }else{
//		    	result.put("result", false);
//		    	result.put("MSG","Add Catering Agent Registration Failed....!");
//		    }
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			log.error(e);
//			result = new JSONObject();
//			try {
//				result.put("MSG","Operation Failed....!");
//			} catch (JSONException e1) {
//				e1.printStackTrace();
//				log.error(e1);
//			}
//		}
		return null;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CartingAgent loadCartingAgentObjectUsingAgentId(Integer agentId) {
		return cartingAgentDao.loadCartingAgentObjectUsingAgentId(agentId);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateCartingAgentObject(CartingAgent cartingAgent) {
	
		return cartingAgentDao.updateCartingAgentObject(cartingAgent);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer insertCartingAgentDetilsAndDeleteOldCartingAgentDetails(Integer cartingAgentId, CartingAgent cartingAgent) {
		return cartingAgentDao.insertCartingAgentDetilsAndDeleteOldCartingAgentDetails(cartingAgentId, cartingAgent);
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateCartingAgentImagesObjectByNewCartingAgentId(Integer oldCartingAgentId, Integer newCartingAgentId) {
		cartingAgentDao.updateCartingAgentImagesObjectByNewCartingAgentId(oldCartingAgentId, newCartingAgentId);
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateCartingAgentPhotoImage(Integer cartingAgentId, 
			List<CartingAgentImagePath> cartingAgentPhotoImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		cartingAgentPhotoImgList = cartingAgentDao.updateCartingAgentPhotoImage(cartingAgentId, cartingAgentPhotoImgList);
		for(CartingAgentImagePath tempObj : cartingAgentPhotoImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getCartingAgentImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCartingAgentPhotoImage(Integer cartingAgentPhotoImageId) {
		return cartingAgentDao.deleteCartingAgentPhotoImage(cartingAgentPhotoImageId);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateCartingAgentVehiclePhotoImage(Integer cartingAgentId, List<CartingAgentVehiclePhotoImagePath> cartingAgentVehiclePhotoImgList) throws JSONException
			 {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		cartingAgentVehiclePhotoImgList = cartingAgentDao.updateCartingAgentVehiclePhotoImage(cartingAgentId, cartingAgentVehiclePhotoImgList);
		for(CartingAgentVehiclePhotoImagePath tempObj : cartingAgentVehiclePhotoImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getCartingAgentVehiclePhotoImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCartingAgentVehiclePhotoImage(Integer cartingAgentVehiclePhotoImageId) {
		return cartingAgentDao.deleteCartingAgentVehiclePhotoImage(cartingAgentVehiclePhotoImageId);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject updateCartingAgentDocumentInfo(Integer documentTypeId, Integer cartingAgentId, Integer cartingAgentDocId) throws Exception {
		JSONObject result= new JSONObject();
		CartingAgentUploadedDocument cartingAgentUploadedDocument = null;
		{
			cartingAgentUploadedDocument = cartingAgentUploadedDocumentDao.updateCartingAgentDocumentInfo(documentTypeId, cartingAgentId, cartingAgentDocId);
			
			result.put("AGENT_ID", cartingAgentUploadedDocument.getCartingAgent().getCartingAgentId());
			result.put("AGENT_NAME", cartingAgentUploadedDocument.getCartingAgent().getName());
			result.put("DOC_TYPE_ID", cartingAgentUploadedDocument.getDocumentListType().getDocumentListTypeId());
			result.put("DOC_TYPE", cartingAgentUploadedDocument.getDocumentListType().getDocumentName());
			result.put("RESULT", true);
			result.put("MSG", "Employee Uploaded Document Details Updated Successfully...!");
		}
		return result;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateCartingAgentUploadDocumentImage(Integer cartingAgentDocumentId, List<CartingAgentUploadedDocumentImagePath> documentList) throws JSONException
	{
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		documentList = cartingAgentUploadedDocumentDao.updateCartingAgentUploadDocumentImage(cartingAgentDocumentId, documentList);
		for(CartingAgentUploadedDocumentImagePath tempObj : documentList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getCartingAgentUploadedDocumentImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCartingAgentDocumentFileImage(Integer cartingAgentUploadDocumentPathId) {
		return cartingAgentUploadedDocumentDao.deleteCartingAgentDocumentFileImage(cartingAgentUploadDocumentPathId);
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray searchCartingAgentsUploadedDocumentDetails(
			String searchOptioinINT, String searchText, Integer organizationId, Integer from) throws JSONException {
		Map<Object, Object>map = null;
		JSONObject tempJson = null;
		JSONArray cartingAgentDocArray = null;
		List<CartingAgentUploadedDocument> docList =null;
		JSONObject tempJson1 = null;
		JSONArray json_ImageArray = null;
		{
			map=cartingAgentUploadedDocumentDao.searchCartingAgentsUploadedDocumentDetails(searchOptioinINT,  searchText,  organizationId,from);
			docList = (List<CartingAgentUploadedDocument>) map.get("cartingAgentUploadedDocument");
			Integer totalPage = (Integer) map.get("totalPages");
			//docList = cartingAgentUploadedDocumentDao.getCartingAgentUploadedDocList(organizationId);
			cartingAgentDocArray = new JSONArray();
			for(CartingAgentUploadedDocument tempObj:docList){
				tempJson = new JSONObject();
				tempJson.put("DOC_ID", tempObj.getCartingAgentUploadedDocumentId());
				tempJson.put("ORG_ID", tempObj.getOrganization().getOrganizationId());
				tempJson.put("ORG_NAME", tempObj.getOrganization().getOrganizationName());
				tempJson.put("AGENT_ID", tempObj.getCartingAgent().getCartingAgentId());
				tempJson.put("AGENT_NAME", tempObj.getCartingAgent().getName());
				tempJson.put("DOC_TYPE_ID", tempObj.getDocumentListType().getDocumentListTypeId());
				tempJson.put("DOC_TYPE", tempObj.getDocumentListType().getDocumentName());
				json_ImageArray=new JSONArray();
				for(CartingAgentUploadedDocumentImagePath tempObj1 : tempObj.getCartingAgentUploadedDocumentImagePathList()){
					tempJson1 = new JSONObject();
					tempJson1.put("IMAGE_PATH", tempObj1.getImagePath());
					tempJson1.put("IMAGE_ID", tempObj1.getCartingAgentUploadedDocumentImagePathId());
					json_ImageArray.put(tempJson1);
				}
				tempJson.put("DOCUMENT", json_ImageArray);
				cartingAgentDocArray.put(tempJson);
			}
			tempJson=new JSONObject();
			tempJson.put("paginationCount", totalPage);
			cartingAgentDocArray.put(tempJson);
		}
		return cartingAgentDocArray;
	}
	
}
