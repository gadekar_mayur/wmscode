/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import com.protocol.wms.model.OrderMode;

/**
 * @author Sudhakar
 *
 */
public interface OrderModeService {
	
	public List<OrderMode> loadAllOrderMode();
}
