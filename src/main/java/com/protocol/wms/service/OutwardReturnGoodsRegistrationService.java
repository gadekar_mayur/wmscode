/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.OutWardReturnGoodsLRDetails;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsClaimImagePath;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsLrImagePath;
import com.protocol.wms.model.OutWardReturnGoodsRegistration;

/**
 * @author admin
 *
 */
public interface OutwardReturnGoodsRegistrationService {

	public Boolean saveOutwardReturnGoodsRegistration(Integer orgId,Integer companyId,Integer stockistId,Integer transporterId,Integer employeeId,List<OutWardReturnGoodsLRDetails> lrDetailsList,OutWardReturnGoodsRegistration outWardReturnGoodsRegistration);
	
	public Boolean updateInwardReturnGoodsLrReg(List<OutWardReturnGoodsLRDetails> lrDetailsList);
	
	public List<OutWardReturnGoodsRegistration> outwardReturnGoodsRegListing(Integer organizationId);
	
	public Boolean deleteOutwardReturnGoodsRegDetails(Integer outwardRetGoodsRegId);
	
	public Map<String , Object> outwardReturnGoodsRegDetailsAndLrListing(Integer outWardReturnGoodsRegistrationId);
	
	public JSONObject outwardReturnGoodsRegDetailsViewAndLrListingView(Boolean editStatus, Integer outWardReturnGoodsRegistrationId) throws Exception;
	
	public Map<String,Object> viewOutwardLR_DetailedView_LrCreditNoteAndLrProductList(Integer outWardReturnGoodsLRDetailsId);
	
	public Boolean deleteOutwardReturnGoodsRegLrDetails(Integer LR_ID);
	
	//public Boolean deleteOutwardReturnGoodsRegLRProduct(Integer PRODUCT_ID);
	
	public OutWardReturnGoodsRegistration loadOutWardReturnGoodsRegistrationObjByOutwardReturnGoodsRegId(Integer outWardReturnGoodsRegistrationId);
	
	public JSONObject editOutwardReturnGoodsRegistration(Integer orgId,Integer companyId,Integer stockistId,Integer transporterId,Integer employeeId,OutWardReturnGoodsRegistration outwardReturnGoodsRegistration) throws Exception;
	
	public OutWardReturnGoodsLRDetails loadOutWardReturnGoodsLRDetailsByLrRegId(Integer outWardReturnGoodsLRDetailsId);
	
	public Boolean editOutwardReturnGoodsRegLrDetails(OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails);

	public Map<Object,Object> searchOutReturnGoodsRegListing(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from);
	
	public JSONArray updateOutwardReturnGoodsLrRegLrImage(Integer outwardReturnGoodsLrRegistrationId, List<OutWardReturnGoodsLRDetailsLrImagePath> lrImgList) throws Exception;
	
	public Boolean deleteOutwardReturnGoodsLrRegLrImage(Integer lrImageId);
	
	public JSONArray updateOutwardReturnGoodsLrRegClaimCopyImage(Integer outwardReturnGoodsLrRegistrationId, List<OutWardReturnGoodsLRDetailsClaimImagePath> claimCopyImgList) throws Exception;
	
	public Boolean deleteOutwardReturnGoodsLrRegClaimCopyImage(Integer claimCopyImageId);
}
