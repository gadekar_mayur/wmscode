/**
 * 
 */
package com.protocol.wms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.RatePerStationDao;
import com.protocol.wms.model.RatePerStation;

/**
 * @author Sudhakar
 *
 */
@Service("RatePerStrationServiceImpl")
public class RatePerStationServiceImpl implements RatePerStationService{

	
	private RatePerStationDao ratePerStationDao;

	@Autowired
	@Qualifier("RatePerStationDaoImpl")
	public void setRatePerStationDao(RatePerStationDao ratePerStationDao) {
		this.ratePerStationDao = ratePerStationDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.RatePerStationService#saveRatePerStationObject(com.protocol.wms.model.RatePerStation)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveRatePerStationObject(RatePerStation ratePerStationObj) {
		return ratePerStationDao.saveRatePerStationObject(ratePerStationObj);
	}
	
}
