/**
 * 
 */
package com.protocol.wms.service;

import com.protocol.wms.model.CompanyBank;

/**
 * @author admin
 *
 */
public interface CompanyBankService {

	public CompanyBank loadCompanyBankUsignCompanyBankId(Integer companyBankId);
	
	public Boolean updateCompanyBankDetails(CompanyBank companyBank);
}
