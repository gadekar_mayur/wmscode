/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.TransporterDocument;
import com.protocol.wms.model.TransporterDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
public interface TransporterDocumentService {
	
	public Integer saveTransporterDocument(TransporterDocument transporterDocumentObj);

	public JSONArray loadTransporterDocumentListUsignOrganizationId(Integer organizationId) throws Exception;
	
	public JSONArray searchTransporterUploadDocument(String searchOption,String searchText,Integer organizationId, Integer from) throws Exception;
	
	public JSONObject updateTransporterDocumentInfo(Integer documentTypeId, Integer transporterId, Integer transporterDocumentId)throws Exception;
	
	public JSONArray updateTransportUploadDocumentImage(Integer transportDocumentId, List<TransporterDocumentImagePath> documentList)throws Exception;
	
	public Boolean deleteTrnsporterDocumentFileImage(Integer transporterDocumentPathId);
}
