package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;








import com.protocol.wms.dao.TransporterDetailsDao;
import com.protocol.wms.model.TransporterDetails;

@Service("TransporterDetailsServiceImpl")
public class TransporterDetailsServiceImpl implements TransporterDetailsService {

	@Autowired
	@Qualifier("TransporterDetailsDaoImpl")
	private TransporterDetailsDao transporterDetailsDao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getTransporterList(Integer orgId) {
		// TODO Auto-generated method stub
		return transporterDetailsDao.getTransporterList(orgId);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDetailsService#saveTransporterDetails(com.protocol.wms.model.TransporterDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveTransporterDetails(TransporterDetails TranObj) {
		return transporterDetailsDao.saveTransporterDetails(TranObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDetailsService#loadTransporterDetailsUsingTransporterDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public TransporterDetails loadTransporterDetailsUsingTransporterDetailsId(
			Integer TransporterDetailsId) {
		return transporterDetailsDao.loadTransporterDetailsUsingTransporterDetailsId(TransporterDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDetailsService#updateTransporterDetails(com.protocol.wms.model.TransporterDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateTransporterDetails(
			TransporterDetails TransporterDetailsObj) {
		transporterDetailsDao.updateTransporterDetails(TransporterDetailsObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDetailsService#loadTransporterDetailsListUsingOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<TransporterDetails> loadTransporterDetailsListUsingOrganizationId(
			Integer organizationId) {
		// TODO Auto-generated method stub
		return transporterDetailsDao.loadTransporterDetailsListUsingOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDetailsService#searchTransporterDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchTransporterDetails(
			String searchOption, String searchText,Integer from, Integer organizationId) {
		return transporterDetailsDao.searchTransporterDetails(searchOption, searchText,from, organizationId);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDetailsService#deleteTransporterDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteTransporterDetails(Integer transporterDetailsId) {
		// TODO Auto-generated method stub
		return transporterDetailsDao.deleteTransporterDetails(transporterDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDetailsService#deleteStationPersonContactDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteStationPersonContactDetails(Integer stationPersonContactDetailsId) {
		return transporterDetailsDao.deleteStationPersonContactDetails(stationPersonContactDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDetailsService#deleteTransporterDocumentDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteTransporterDocumentDetails(Integer transporterDocumentId) {
		// TODO Auto-generated method stub
		return transporterDetailsDao.deleteTransporterDocumentDetails(transporterDocumentId);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDetailsService#insertNewTransporterAndDeleteOldTransporter(java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.TransporterDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject insertNewTransporterAndDeleteOldTransporter(Integer transporterDetailsId, Integer stateId,
			Integer districtId, Integer cityId,
			TransporterDetails transporterDetailsObj) throws JSONException {
		JSONObject result = new JSONObject();
		Integer id = transporterDetailsDao.insertNewTransporterAndDeleteOldTransporter(transporterDetailsId, stateId, districtId, cityId, transporterDetailsObj);
		result.put("MSG", "Transporter Details updation failed....!");
		if(id!=null&&id>0){
			 result.put("MSG", "Transporter Details updated successfully....!");
			 result.put("STATE_NAME", transporterDetailsObj.getState().getStateName());
			 result.put("DISTRICT_NAME", transporterDetailsObj.getDistrict().getDistrictName());
			 result.put("CITY_NAME", transporterDetailsObj.getCity().getCityName());
			 result.put("RESULT", true);
			 result.put("TRANSPORTER_ID", id);
		 }
		return result;
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String, Object> searchTransporterStockistDetails(Integer orgId, Integer companyId, Integer transporterId) {
		
		 return transporterDetailsDao.searchTransporterStockistDetails(orgId, companyId,transporterId);

	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getTransporter(Integer orgId, Integer companyId) {
		// TODO Auto-generated method stub
	  return transporterDetailsDao.getTransporterDetails(orgId, companyId);

	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveTranfereSelectedStockist(Integer[] stockistId, Integer[] transporterStationArrayId,
			Integer fromTransporterId, Integer toTransporterId) 
	{
		Integer count=0;
		
		try{
			
			 count=transporterDetailsDao.saveTranfereSelectedStockist(stockistId,transporterStationArrayId,fromTransporterId,toTransporterId);
	return count;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return count;
		}
		
		
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String, Object> loadPendingStockist(Integer rateId) {
		System.out.println("In service 186 transporter server impl");
	return transporterDetailsDao.loadPendingStockist(rateId);

	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String, Object> loadAdminApprovalStatus(Integer rateId) {
		// TODO Auto-generated method stub
		return transporterDetailsDao.loadAdminApprovalStatus(rateId);
		
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void saveApprovedData(String[] toTransporterArray, String[] stockistApprovalArray,
			String[] transporterStockistTransferArray) {
		 transporterDetailsDao.saveApprovedData(toTransporterArray,stockistApprovalArray,transporterStockistTransferArray);
		
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void saveRejectedData(String[] transporterStockistTransferArray) {
		// TODO Auto-generated method stub
		transporterDetailsDao.saveRejectedData(transporterStockistTransferArray);
		
	}
	
}
