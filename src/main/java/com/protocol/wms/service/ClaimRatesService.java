/**
 * 
 */
package com.protocol.wms.service;

import com.protocol.wms.model.ClaimRates;

/**
 * @author Sudhakar
 *
 */
public interface ClaimRatesService {
	public Integer saveClaimRate(ClaimRates claimRates);

}
