/**
 * 
 */
package com.protocol.wms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.UserTypeDao;
import com.protocol.wms.model.UserType;

/**
 * @author Sudhakar
 *
 */
@Service("UserTypeServiceImpl")
public class UserTypeServiceImpl implements UserTypeService{
	
	private UserTypeDao userTypeDao;

	@Autowired
	@Qualifier("UserTypeDaoImpl")
	public void setUserTypeDao(UserTypeDao userTypeDao) {
		this.userTypeDao = userTypeDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.UserTypeService#loadUserTypeUsingUserTypeId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public UserType loadUserTypeUsingUserTypeId(Integer userTypeId) {
		return userTypeDao.loadUserTypeUsingUserTypeId(userTypeId);
	}
	
}
