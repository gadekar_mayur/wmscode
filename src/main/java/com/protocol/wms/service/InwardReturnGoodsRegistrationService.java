/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.CheckingDoneCheckingSlipImagePath;
import com.protocol.wms.model.CreditNote;
import com.protocol.wms.model.CreditNoteCreditNoteImagePath;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsClaimCopyPath;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsLrImgPath;
import com.protocol.wms.model.InwardReturnProducts;

/**
 * @author admin
 *
 */
public interface InwardReturnGoodsRegistrationService {

	public Boolean saveInwardReturnGoodsRegistration(Integer organizationId,Integer companyId,Integer stockistId,Integer transporterId,Integer employeeId,List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList,Map<Integer, List<InwardReturnProducts>> inwardReturnProductsMap,InwardReturnGoodsRegistration inwardReturnGoodsRegistration);
	
	public Boolean updateInwardReturnGoodsLrReg(List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList);
	
	public List<InwardReturnGoodsRegistration> inwardReturnGoodsRegListing(Integer organizationId);
	
	public List<InwardReturnGoodsRegistrationLRDetails> inwardReturnGoodsRegLrListing(Integer inwardReturnGoodRegistrationId);
	
	public Map<String,Object> inwardReturnGoodsRegDetailsAndLrListing(Integer ID_NO);
	
	public Map<String,Object> inwardReturnGoodsRegDetailsAndLrListingForCheckingPending(Integer inwardReturnGoodRegistrationId);
	
	public List<InwardReturnProducts> inwardProductListByLR_NO(Integer LR_NO);
	
	public Map<String, Object> viewLR_DetailedViewsAndLrProductList(Integer LR_NO);
	
	public JSONObject viewLR_DetailedViewsAndLrProductListView(Integer LR_NO) throws Exception;
	
	public Boolean deleteInwardReturnGoodsRegDetails(Integer inwardReturnGoodRegistrationId);
	
	public Boolean deleteInwardReturnGoodsRegLrDetails(Integer LR_ID);
	
	public Boolean deleteInwardReturnGoodsRegLRProduct(Integer PRODUCT_ID);
	
	public InwardReturnGoodsRegistration loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(Integer InwardReturnGoodsRegistrationoId);
	
	public JSONObject inwardReturnGoodsRegDetailsAndLrListingCheckingDoneView(Integer InwardReturnGoodsRegistrationId) throws Exception;
	
	public JSONObject editInwardReturnGoodsRegistration(Integer orgId,Integer companyId,Integer stockistId,Integer transporterId,Integer employeeId,InwardReturnGoodsRegistration inwardReturnGoodsRegistration) throws Exception;
	
	public JSONObject editInwardReturnGoodsRegistration(Integer orgId,Integer companyId,Integer stockistId,Integer transporterId,Integer employeeId,InwardReturnGoodsRegistration inwardReturnGoodsRegistration,Integer checkingDoneEmployeeDetailsId,CheckingDone checkingDone) throws Exception;
	
	public InwardReturnGoodsRegistrationLRDetails loadInwardReturnGoodsRegistrationLRDetails(Integer InwardReturnGoodsRegistrationLRDetailsId);
	
	public Boolean editInwardReturnGoodsRegLrDetails(InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails);
	
	public InwardReturnProducts loadInwardReturnProductsObjById(Integer InwardReturnProductsId);
	
	public Boolean editInwardReturnGoodsRegLreditProductDetails(InwardReturnProducts inwardReturnProducts);
	
	public CheckingDone loadCheckingDoneObjByInwardReturnGoodsRegId(Integer inwardReturnGoodRegistrationId);
	
	public CheckingDone loadCheckingDoneObjByCheckingDoneId(Integer checkingDoneId);
	
	public JSONObject editInwardReturnGoodsRegCheckingDoneDetails(CheckingDone checkingDone,Integer checkingDoneEmployeeDetailsId) throws Exception;
	
	public CreditNote loadCreditNoteObjectUsingCreditNoteId(Integer creditNoteId);
	
	public Boolean editInwardReturnGoodsRegCreditNoteDetails(CreditNote creditNote);

	public Map<String , Object> searchInwardReturnGoodsRegListing(String selectedValue, String searchText, String fromSpecialData,String toSpecialData ,String searchOption,Integer from, Integer organizationId);
	
	public Boolean deleteInwardReturnGoodsLrRegLrImage(Integer lrImageId);
	
	public Boolean deleteInwardReturnGoodsLrRegClaimCopyImage(Integer lrImageId);
	
	public JSONArray updateInwardReturnGoodsLrRegLrImage(Integer inwardReturnGoodsLrRegistrationId, List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> lrImgList) throws Exception;
	
	public JSONArray updateInwardReturnGoodsLrRegClaimCopyImage(Integer inwardReturnGoodsLrRegistrationId, List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> claimCopyImgList) throws Exception;
	
	public JSONArray updateCheckingDoneRegCheckingSlipImage(Integer checkingDoneId, List<CheckingDoneCheckingSlipImagePath> checkingSlipImgList) throws Exception;
	
	public Boolean deleteCheckingDoneCheckingSlipImage(Integer checkingSlipImageId);
	
	public JSONArray updateCreditNoteRegCreditNoteAttachImage(Integer creditNoteId, List<CreditNoteCreditNoteImagePath> creditNoteImgList) throws Exception;
	
	public Boolean deleteCreditNoteRegCreditNoteAttachImage(Integer creditNoteAttachImageId);
	

	
}
