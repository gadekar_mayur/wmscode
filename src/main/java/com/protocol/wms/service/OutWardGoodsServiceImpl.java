/**
 * 
 */
package com.protocol.wms.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


import java.util.Map;

import org.bouncycastle.asn1.cmp.OOBCertHash;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.dao.CartingAgentDao;
import com.protocol.wms.dao.CartingAgentTripCountDao;
import com.protocol.wms.dao.ChequeNumberInfoDao;
import com.protocol.wms.dao.CompanyCreditAndDiscountDao;
import com.protocol.wms.dao.CompanyDao;
import com.protocol.wms.dao.CompanyProductDao;
import com.protocol.wms.dao.InvoicePaymentInformationDao;
import com.protocol.wms.dao.OrderModeDao;
import com.protocol.wms.dao.OrganizationDao;
import com.protocol.wms.dao.OrganizationTripCountDao;
import com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao;
import com.protocol.wms.dao.OutWardGatePassDao;
import com.protocol.wms.dao.OutWardOrderEntryRegistrationDao;
import com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao;
import com.protocol.wms.dao.OutWardProductOrderEnteryRegistrationDao;
import com.protocol.wms.dao.OutWardTripDao;
import com.protocol.wms.dao.OutWardTripTransporterDetailsDao;
import com.protocol.wms.dao.OutwardOrderEntryRegistrationImageCopyPathDao;
import com.protocol.wms.dao.OutwardTripReportDao;
import com.protocol.wms.dao.PrintStickerDao;
import com.protocol.wms.dao.RemainingChequeAmountDao;
import com.protocol.wms.dao.StockistBankDetailsDao;
import com.protocol.wms.dao.StockistDao;
import com.protocol.wms.dao.TransporterDetailsDao;
import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentTripCount;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.CompanyProduct;
import com.protocol.wms.model.InvoicePaymentInformation;
import com.protocol.wms.model.OrderMode;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationTripCount;
import com.protocol.wms.model.OutWardDispatchEnteryRegistration;
import com.protocol.wms.model.OutWardGatePass;
import com.protocol.wms.model.OutWardGatePassLrImagePath;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistrationOrderCopyPath;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.OutWardProductOrderEntryRegistration;
import com.protocol.wms.model.OutWardTrip;
import com.protocol.wms.model.OutWardTripTransporterDetails;
import com.protocol.wms.model.OutwardTripReport;
import com.protocol.wms.model.PrintSticker;
import com.protocol.wms.model.RemainingChequeAmount;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistBankDetails;
import com.protocol.wms.model.TransporterDetails;

/**
 * @author Sudhakar
 *
 */
@Service("OutWardGoodsServiceImpl")
public class OutWardGoodsServiceImpl implements OutWardGoodsService{

	private CompanyProductDao companyProductDao;
	
	private OrganizationDao organizationDao;
	
	private CompanyDao companyDao;
	
	private StockistDao stockistDao;
	
	private StockistBankDetailsDao stockistBankDetailsDao;
	
	private OrderModeDao orderModeDao;
	
	private OutWardOrderEntryRegistrationDao outWardOrderEntryRegistrationDao;
	
	private OutWardProductOrderEnteryRegistrationDao outWardProductOrderEnteryRegistrationDao;
	
	private CompanyCreditAndDiscountDao companyCreditAndDiscountDao;
	
	private OutWardOrderInvoiceEnteryRegistrationDao outWardOrderInvoiceEnteryRegistrationDao;
	
	private OutWardDispatchEnteryRegistrationDao OutWardDispatchEnteryRegistrationDao;
	
	private TransporterDetailsDao transporterDetailsDao;
	
	private OutWardTripDao outWardTripDao;
	
	private OutWardGatePassDao outWardGatePassDao;
	
	private RemainingChequeAmountDao remainingChequeAmountDao;
	
	private ChequeNumberInfoDao chequeNumberInfoDao;
	
	private InvoicePaymentInformationDao invoicePaymentInformationDao;
	
	private OrganizationTripCountDao organizationTripCountDao;
	
	private CartingAgentTripCountDao cartingAgentTripCountDao;
	
	private CartingAgentDao cartingAgentDao;
	
	private OutWardTripTransporterDetailsDao outWardTripTransporterDetailsDao;
	
	private OutwardTripReportDao outwardTripReportDao;
	
	private PrintStickerDao printStickerDao;
	
	
	private OutwardOrderEntryRegistrationImageCopyPathDao outwardOrderEntryRegistrationImageCopyPathDao;
	
	
	
	@Autowired
	@Qualifier("OutwardOrderEntryRegistrationImageCopyPathDaoImpl")
	public void setOutwardOrderEntryRegistrationImageCopyPathDao(
			OutwardOrderEntryRegistrationImageCopyPathDao outwardOrderEntryRegistrationImageCopyPathDao) {
		this.outwardOrderEntryRegistrationImageCopyPathDao = outwardOrderEntryRegistrationImageCopyPathDao;
	}

	@Autowired
	@Qualifier("StockistBankDetailsDaoImpl")
	public void setStockistBankDetailsDao(
			StockistBankDetailsDao stockistBankDetailsDao) {
		this.stockistBankDetailsDao = stockistBankDetailsDao;
	}

	@Autowired
	@Qualifier("PrintStickerDaoImpl")
	public void setPrintStickerDao(PrintStickerDao printStickerDao) {
		this.printStickerDao = printStickerDao;
	}

	@Autowired
	@Qualifier("OutwardTripReportDaoImpl")
	public void setOutwardTripReportDao(OutwardTripReportDao outwardTripReportDao) {
		this.outwardTripReportDao = outwardTripReportDao;
	}

	@Autowired
	@Qualifier("CartingAgentDaoImpl")
	public void setCartingAgentDao(CartingAgentDao cartingAgentDao) {
		this.cartingAgentDao = cartingAgentDao;
	}

	@Autowired
	@Qualifier("OutWardTripTransporterDetailsDaoImpl")
	public void setOutWardTripTransporterDetailsDao(
			OutWardTripTransporterDetailsDao outWardTripTransporterDetailsDao) {
		this.outWardTripTransporterDetailsDao = outWardTripTransporterDetailsDao;
	}

	@Autowired
	@Qualifier("CartingAgentTripCountDaoImpl")
	public void setCartingAgentTripCountDao(
			CartingAgentTripCountDao cartingAgentTripCountDao) {
		this.cartingAgentTripCountDao = cartingAgentTripCountDao;
	}

	@Autowired
	@Qualifier("OrganizationTripCountDaoImpl")
	public void setOrganizationTripCountDao(
			OrganizationTripCountDao organizationTripCountDao) {
		this.organizationTripCountDao = organizationTripCountDao;
	}

	@Autowired
	@Qualifier("InvoicePaymentInformationDaoImpl")
	public void setInvoicePaymentInformationDao(
			InvoicePaymentInformationDao invoicePaymentInformationDao) {
		this.invoicePaymentInformationDao = invoicePaymentInformationDao;
	}

	@Autowired
	@Qualifier("ChequeNumberInfoDaoImpl")
	public void setChequeNumberInfoDao(ChequeNumberInfoDao chequeNumberInfoDao) {
		this.chequeNumberInfoDao = chequeNumberInfoDao;
	}

	@Autowired
	@Qualifier("RemainingChequeAmountDaoImpl")
	public void setRemainingChequeAmountDao(
			RemainingChequeAmountDao remainingChequeAmountDao) {
		this.remainingChequeAmountDao = remainingChequeAmountDao;
	}

	@Autowired
	@Qualifier("OutWardGatePassDaoImpl")
	public void setOutWardGatePassDao(OutWardGatePassDao outWardGatePassDao) {
		this.outWardGatePassDao = outWardGatePassDao;
	}

	@Autowired
	@Qualifier("OutWardTripDaoImpl")
	public void setOutWardTripDao(OutWardTripDao outWardTripDao) {
		this.outWardTripDao = outWardTripDao;
	}

	@Autowired
	@Qualifier("TransporterDetailsDaoImpl")
	public void setTransporterDetailsDao(TransporterDetailsDao transporterDetailsDao) {
		this.transporterDetailsDao = transporterDetailsDao;
	}

	@Autowired
	@Qualifier("OutWardDispatchEnteryRegistrationDaoImpl")
	public void setOutWardDispatchEnteryRegistrationDao(
			OutWardDispatchEnteryRegistrationDao outWardDispatchEnteryRegistrationDao) {
		OutWardDispatchEnteryRegistrationDao = outWardDispatchEnteryRegistrationDao;
	}

	@Autowired
	@Qualifier("OutWardOrderInvoiceEnteryRegistrationDaoImpl")
	public void setOutWardOrderInvoiceEnteryRegistrationDao(
			OutWardOrderInvoiceEnteryRegistrationDao outWardOrderInvoiceEnteryRegistrationDao) {
		this.outWardOrderInvoiceEnteryRegistrationDao = outWardOrderInvoiceEnteryRegistrationDao;
	}

	@Autowired
	@Qualifier("CompanyCreditAndDiscountDaoImpl")
	public void setCompanyCreditAndDiscountDao(
			CompanyCreditAndDiscountDao companyCreditAndDiscountDao) {
		this.companyCreditAndDiscountDao = companyCreditAndDiscountDao;
	}

	@Autowired
	@Qualifier("OutWardProductOrderEnteryRegistrationDaoImpl")
	public void setOutWardProductOrderEnteryRegistrationDao(
			OutWardProductOrderEnteryRegistrationDao outWardProductOrderEnteryRegistrationDao) {
		this.outWardProductOrderEnteryRegistrationDao = outWardProductOrderEnteryRegistrationDao;
	}

	@Autowired
	@Qualifier("OutWardOrderEntryRegistrationDaoImpl")
	public void setOutWardOrderEntryRegistrationDao(
			OutWardOrderEntryRegistrationDao outWardOrderEntryRegistrationDao) {
		this.outWardOrderEntryRegistrationDao = outWardOrderEntryRegistrationDao;
	}

	@Autowired
	@Qualifier("CompanyProductDaoImpl")
	public void setCompanyProductDao(CompanyProductDao companyProductDao) {
		this.companyProductDao = companyProductDao;
	}
	
	@Autowired
	@Qualifier("OrganizationDaoImpl")
	public void setOrganizationDao(OrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	@Autowired
	@Qualifier("CompanyDaoImpl")
	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}
	
	@Autowired
	@Qualifier("StockistDaoImpl")
	public void setStockistDao(StockistDao stockistDao) {
		this.stockistDao = stockistDao;
	}

	@Autowired
	@Qualifier("OrderModeDaoImpl")
	public void setOrderModeDao(OrderModeDao orderModeDao) {
		this.orderModeDao = orderModeDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadCompanyProductListUsignCompanyId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyProduct> loadCompanyProductListUsignCompanyId(
			Integer companyId) {
		return companyProductDao.loadCompanyProductListUsignCompanyId(companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOrganizationObjectUsingOrganiztionId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Organization loadOrganizationObjectUsingOrganiztionId(
			Integer organizationId) {
		return organizationDao.loadOrganizationObjectUsingOrganiztionId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadCompanyObjectUsingCompanyId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Company loadCompanyObjectUsingCompanyId(Integer companyId) {
		return companyDao.loadCompanyObjectUsingCompanyId(companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadStockistObjectByStockistId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Stockist loadStockistObjectByStockistId(Integer stockistId) {
		return stockistDao.loadStockistObjectByStockistId(stockistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOrderModeObjectUsignOrderModeId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OrderMode loadOrderModeObjectUsignOrderModeId(Integer orderModeId) {
		return orderModeDao.loadOrderModeObjectUsignOrderModeId(orderModeId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveOutWardOrderEntryRegistration(com.protocol.wms.model.OutWardOrderEntryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOutWardOrderEntryRegistration(
			OutWardOrderEntryRegistration obj) {
		return outWardOrderEntryRegistrationDao.saveOutWardOrderEntryRegistration(obj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardOrderEntryRegistrationOfProductStatusIsZero()
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsZero(Integer organizationId) {
		return outWardOrderEntryRegistrationDao.loadOutWardOrderEntryRegistrationOfProductStatusIsZero(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardOrderEntryRegistration loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(
			Integer OutWardOrderEntryRegistrationId) {
		return outWardOrderEntryRegistrationDao.loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(OutWardOrderEntryRegistrationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray viewAllDetailsOfOutwardGoodsRegistration(
			Integer outWardOrderEntryRegistrationId) throws Exception {
		JSONArray json_data_array= new JSONArray();
		JSONArray json_cheque_array= new JSONArray();
		JSONArray json_invoice_array= new JSONArray();
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		JSONObject json=null;
		JSONObject jsonCheque = null;
		JSONObject jsonInvoice = null;
		{
			CommonMethods common = new CommonMethods();
			OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj=outWardOrderEntryRegistrationDao.loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(outWardOrderEntryRegistrationId);
			if(outWardOrderEntryRegistrationObj!=null)
			{
				json=new JSONObject();
				json.put("ORG_NAME", outWardOrderEntryRegistrationObj.getOrganization().getOrganizationName());
				json.put("ORG_ID", outWardOrderEntryRegistrationObj.getOrganization().getOrganizationId());
				json.put("COMP_NAME", outWardOrderEntryRegistrationObj.getCompany().getCompanyName());
				json.put("ORDER_NUM", outWardOrderEntryRegistrationObj.getOrderNo());
				
				String [] reg_date_arr = outWardOrderEntryRegistrationObj.getOrderDate().toString().split("-");
                json.put("DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
				
				json.put("MODE", outWardOrderEntryRegistrationObj.getOrderMode().getOrderModeName());
				json.put("ORDER_MODE_ID",outWardOrderEntryRegistrationObj.getOrderMode().getOrderModeId());
				json.put("REMARK", outWardOrderEntryRegistrationObj.getRemark());
				json_ImageArray=new JSONArray();
				for(OutWardOrderEntryRegistrationOrderCopyPath tempObj : outWardOrderEntryRegistrationObj.getOutWardOrderEntryRegistrationOrderCopyPathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getOutWardOrderEntryRegistrationOrderCopyPathId());
					json_ImageArray.put(tempJson);
				}
				json.put("ORDER_COPY", json_ImageArray);
				if(outWardOrderEntryRegistrationObj.getStockist()!= null)
					{
						json.put("STOCKIST", outWardOrderEntryRegistrationObj.getStockist().getStockistName());
						//For Percentages Edit Invoice ==>Add More Invoices
						String stockistLocation=outWardOrderEntryRegistrationObj.getStockist().getStockistLocation();
						CompanyCreditAndDiscount companyCreditAndDiscountObj=
								companyCreditAndDiscountDao.loadCompanyCreditAndDiscountObjectUsignComapnyId(outWardOrderEntryRegistrationObj.getCompany().getCompanyId());
						if(stockistLocation.equalsIgnoreCase("Local"))
						{
							json.put("PERCENTAGE", companyCreditAndDiscountObj.getLocalPercentage());
						}
						else
						{
							json.put("PERCENTAGE", companyCreditAndDiscountObj.getOutsideStationPercentage());
						}
						//
				}
				else{
					json.put("STOCKIST", "-");
				}
				json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(outWardOrderEntryRegistrationObj.getSubmitDate().toString()));
				
				if(outWardOrderEntryRegistrationObj.getProductStatus()==1)
				{
					//List<OutWardProductOrderEntryRegistration> outWardProductOrderEntryRegistrationList = outWardGoodsService.getProductOrderEnteryRegistrationDetailsUsingOutWardOrderEntryRegistrationId(OutWardOrderEntryRegistrationId);
					List<OutWardProductOrderEntryRegistration> outWardProductOrderEntryRegistrationList = outWardOrderEntryRegistrationObj.getOutWardProductOrderEnteryRegistrations();
					if(outWardProductOrderEntryRegistrationList.size()>0)
					{
						for(OutWardProductOrderEntryRegistration obj : outWardProductOrderEntryRegistrationList)
						{
							jsonCheque = new JSONObject();
							jsonCheque.put("PRODUCT_NAME", obj.getCompanyProduct().getCompanyProductName());
							jsonCheque.put("QUANTITY", obj.getQuantity());
							jsonCheque.put("OUTWARD_PRODUCT_ORDER_ENRTY_REG_ID", obj.getOutWardProductOrderEnteryRegistrationId());
							json_cheque_array.put(jsonCheque);
						}
					}
				}
				
				//List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService.getOutWardOrderInvoiceEnteryRegistrationUsignoutWardOrderEnteryRegistrationId(OutWardOrderEntryRegistrationId);
				List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardOrderEntryRegistrationObj.getOutWardOrderInvoiceEnteryRegistration();
				if(outWardOrderInvoiceEnteryRegistrationList.size() > 0)
				{
					for(OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEnteryRegistrationList)
					{
						jsonInvoice = new JSONObject();
						jsonInvoice.put("OUTWARD_ORDER_INVOICE_ENTRY_REG_ID", obj.getOutWardOrderInvoiceEnteryRegistrationId());
						jsonInvoice.put("INVOICE_NO", obj.getInvoiceNo());
						jsonInvoice.put("INVOICE_DATE", obj.getInvoiceDate());
						if(obj.getGrossAmount()!=null)
							jsonInvoice.put("GROSS_AMOUNT", obj.getGrossAmount());
						else
							jsonInvoice.put("GROSS_AMOUNT","-");
						if(obj.getTaxAmout()!=null)
							jsonInvoice.put("TAX_AMOUNT", obj.getTaxAmout());
						else
							jsonInvoice.put("TAX_AMOUNT", "-");
						if(obj.getVatAmount()!=null)
							jsonInvoice.put("VAT_AMOUNT", obj.getVatAmount());
						else
							jsonInvoice.put("VAT_AMOUNT", "-");
						jsonInvoice.put("DISCOUNT", obj.getDiscountIsTakenOrNot());
						jsonInvoice.put("NET_AMOUNT", obj.getNetAmount());
						if(obj.getOutWardDispatchEnteryRegistration()==null)
						{
							jsonInvoice.put("DISPATCH_STATUS", "No");	
						}
						else
						{
							jsonInvoice.put("DISPATCH_STATUS", "Yes");
						}
						json_invoice_array.put(jsonInvoice);
					}
				}
				json.put("PRODUCT_ARRAY", json_cheque_array);
				json.put("INVOICE_ARRAY", json_invoice_array);
				json_data_array.put(json);
			}
		}
		return json_data_array;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOutWardOrderEntryRegistration(com.protocol.wms.model.OutWardOrderEntryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateOutWardOrderEntryRegistration(
			OutWardOrderEntryRegistration obj) {
		outWardOrderEntryRegistrationDao.updateOutWardOrderEntryRegistration(obj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveOutWardProductOrderEnteryRegistration(com.protocol.wms.model.OutWardProductOrderEnteryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOutWardProductOrderEnteryRegistration(
			OutWardProductOrderEntryRegistration outWardProductOrderEnteryRegistrationObj) {
		return outWardProductOrderEnteryRegistrationDao.saveOutWardProductOrderEnteryRegistration(outWardProductOrderEnteryRegistrationObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadCompanyProductObjectUsignCompanyProductId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyProduct loadCompanyProductObjectUsignCompanyProductId(
			Integer companyProductId) {
		return companyProductDao.loadCompanyProductObjectUsignCompanyProductId(companyProductId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardOrderEntryRegistrationOfProductStatusIsOne(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsOne(
			Integer organizationId) {
		return outWardOrderEntryRegistrationDao.loadOutWardOrderEntryRegistrationOfProductStatusIsOne(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadCompanyCreditAndDiscountObjectUsignComapnyId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyCreditAndDiscount loadCompanyCreditAndDiscountObjectUsignComapnyId(
			Integer companyId) {
		return companyCreditAndDiscountDao.loadCompanyCreditAndDiscountObjectUsignComapnyId(companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveOutWardOrderInvoiceEnteryRegistrationObject(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOutWardOrderInvoiceEnteryRegistrationObject(
			OutWardOrderInvoiceEnteryRegistration obj) {
		return outWardOrderInvoiceEnteryRegistrationDao.saveOutWardOrderInvoiceEnteryRegistrationObject(obj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardOrderEntryRegistrationOfProductStatusIsOneAndInvoiceStatusIsOne(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsOneAndInvoiceStatusIsOne(
			Integer organizationId) {
		return outWardOrderEntryRegistrationDao.loadOutWardOrderEntryRegistrationOfProductStatusIsOneAndInvoiceStatusIsOne(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero(
			Integer organizationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.loadOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardOrderInvoiceEnteryRegistration loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(
			Integer OutWardOrderInvoiceEnteryRegistrationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(OutWardOrderInvoiceEnteryRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveOutWardDispatchEnteryRegistrationObj(com.protocol.wms.model.OutWardDispatchEnteryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOutWardDispatchEnteryRegistrationObj(
			OutWardDispatchEnteryRegistration obj) {
		// TODO Auto-generated method stub
		return OutWardDispatchEnteryRegistrationDao.saveOutWardDispatchEnteryRegistrationObj(obj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(
			Integer outWardDispatchEnteryRegistrationId) {
		return OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(outWardDispatchEnteryRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOutWardOrderInvoiceEnteryRegistrationObj(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateOutWardOrderInvoiceEnteryRegistrationObj(
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj) {
		outWardOrderInvoiceEnteryRegistrationDao.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardDispatchEnteryRegistrationListOfTripStatusZero(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfTripStatusZero(
			Integer organizationId) {
		return OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationListOfTripStatusZero(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardDispatchEnteryRegistrationListOfStatusOne(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOne(
			Integer organizationId) {
		return OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationListOfStatusOne(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadTransporterDetailsUsingTransporterDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public TransporterDetails loadTransporterDetailsUsingTransporterDetailsId(
			Integer TransporterDetailsId) {
		return transporterDetailsDao.loadTransporterDetailsUsingTransporterDetailsId(TransporterDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveOutWardTripObj(com.protocol.wms.model.OutWardTrip)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOutWardTripObj(OutWardTrip obj) {
		return outWardTripDao.saveOutWardTripObj(obj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardTripObjUsignOutWardTripiD(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardTrip loadOutWardTripObjUsignOutWardTripiD(
			Integer outWardTripId) {
		return outWardTripDao.loadOutWardTripObjUsignOutWardTripiD(outWardTripId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOutWardDispatchEnteryRegistrationObj(com.protocol.wms.model.OutWardDispatchEnteryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateOutWardDispatchEnteryRegistrationObj(
			OutWardDispatchEnteryRegistration OutWardDispatchEnteryRegistrationObj) {
		OutWardDispatchEnteryRegistrationDao.updateOutWardDispatchEnteryRegistrationObj(OutWardDispatchEnteryRegistrationObj);
		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardTripListOfStatusOneUsignOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardTrip> loadOutWardTripListOfStatusOneUsignOrganizationId(
			Integer organizationId) {
		return outWardTripDao.loadOutWardTripListOfStatusOneUsignOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardTripListOfGetPassStatusZeroAndStatusOneUsignOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardTrip> loadOutWardTripListOfGetPassStatusZeroAndStatusOneUsignOrganizationId(
			Integer organizationId) {
		return outWardTripDao.loadOutWardTripListOfGetPassStatusZeroAndStatusOneUsignOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveOutWardGatePass(com.protocol.wms.model.OutWardGatePass)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOutWardGatePass(OutWardGatePass OutWardGatePassObj) {
		return outWardGatePassDao.saveOutWardGatePass(OutWardGatePassObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardGatePassObjUsignOutWardGatePassId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardGatePass loadOutWardGatePassObjUsignOutWardGatePassId(
			Integer outWardGatePassId) {
		return outWardGatePassDao.loadOutWardGatePassObjUsignOutWardGatePassId(outWardGatePassId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOutWardTripObj(com.protocol.wms.model.OutWardTrip)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateOutWardTripObj(OutWardTrip obj) {
		outWardTripDao.updateOutWardTripObj(obj);		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadloadOutWardTripListOfGetPassStatusOneUsignOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardTrip> loadloadOutWardTripListOfGetPassStatusOneUsignOrganizationId(
			Integer organizationId) {
		return outWardTripDao.loadloadOutWardTripListOfGetPassStatusOneUsignOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardOrderEntryRegistrationOfStatusIsOne(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfStatusIsOne(
			Integer organizationId) {
		return outWardOrderEntryRegistrationDao.loadOutWardOrderEntryRegistrationOfStatusIsOne(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadremainingChequeAmountListUsignOrgIdAndCompIdAndStockistId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<RemainingChequeAmount> loadremainingChequeAmountListUsignOrgIdAndCompIdAndStockistId(
			Integer orgId, Integer companyId, Integer stockistId) {
		return remainingChequeAmountDao.loadremainingChequeAmountListUsignOrgIdAndCompIdAndStockistId(orgId, companyId, stockistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadChequeNumberInfoObjUsignChequeNumberInfoId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public ChequeNumberInfo loadChequeNumberInfoObjUsignChequeNumberInfoId(
			Integer ChequeNumberInfoId) {
		return chequeNumberInfoDao.loadChequeNumberInfoObjUsignChequeNumberInfoId(ChequeNumberInfoId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveInvoicePaymentInformationObj(com.protocol.wms.model.InvoicePaymentInformation)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveInvoicePaymentInformationObj(
			InvoicePaymentInformation InvoicePaymentInformationObj) {
		return invoicePaymentInformationDao.saveInvoicePaymentInformationObj(InvoicePaymentInformationObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadRemainingChequeAmountUsignRemainingChequeAmountId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public RemainingChequeAmount loadRemainingChequeAmountUsignRemainingChequeAmountId(
			Integer RemainingChequeAmountId) {
		return remainingChequeAmountDao.loadRemainingChequeAmountUsignRemainingChequeAmountId(RemainingChequeAmountId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateRemainingChequeAmountObj(com.protocol.wms.model.RemainingChequeAmount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateRemainingChequeAmountObj(
			RemainingChequeAmount remainingChequeAmountObj) {
		remainingChequeAmountDao.updateRemainingChequeAmountObj(remainingChequeAmountObj);		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardDispatchEnteryRegistrationObjUsigOrgIdAndStockistIdAndCompanyId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsigOrgIdAndStockistIdAndCompanyId(
			Integer orgId, Integer stokistId, Integer companyId) {
		return OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationObjUsigOrgIdAndStockistIdAndCompanyId(orgId, stokistId, companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOrganizationTripCountObjUsignOrgId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OrganizationTripCount loadOrganizationTripCountObjUsignOrgId(
			Integer orgId) {
		return organizationTripCountDao.loadOrganizationTripCountObjUsignOrgId(orgId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CartingAgentTripCount loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(
			Integer orgId, Integer cartingAgentId) {
		return cartingAgentTripCountDao.loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(orgId, cartingAgentId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveOutWardTripTransporterDetailsObj(com.protocol.wms.model.OutWardTripTransporterDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOutWardTripTransporterDetailsObj(
			OutWardTripTransporterDetails outWardTripTransporterDetailsObj) {
		return outWardTripTransporterDetailsDao.saveOutWardTripTransporterDetailsObj(outWardTripTransporterDetailsObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOrganizationTripCount(com.protocol.wms.model.OrganizationTripCount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateOrganizationTripCount(
			OrganizationTripCount organizationTripCountObj) {
		organizationTripCountDao.updateOrganizationTripCount(organizationTripCountObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateCartingAgentTripCountObj(com.protocol.wms.model.CartingAgentTripCount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateCartingAgentTripCountObj(
			CartingAgentTripCount cartingAgentTripCountObj) {
		cartingAgentTripCountDao.updateCartingAgentTripCountObj(cartingAgentTripCountObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveOrganizationTripCountObj(com.protocol.wms.model.OrganizationTripCount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOrganizationTripCountObj(
			OrganizationTripCount organizationTripCountObj) {
		return organizationTripCountDao.saveOrganizationTripCountObj(organizationTripCountObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveCartingAgentTripCountObj(com.protocol.wms.model.CartingAgentTripCount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveCartingAgentTripCountObj(
			CartingAgentTripCount cartingAgentTripCountObj) {
		return cartingAgentTripCountDao.saveCartingAgentTripCountObj(cartingAgentTripCountObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#getCartingAgentDetailsById(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CartingAgent getCartingAgentDetailsById(Integer agentId) {
		return cartingAgentDao.getCartingAgentDetailsById(agentId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardDispatchEnteryRegistrationListOfStatusOneAndTripStatusOne(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOneAndTripStatusOne(
			Integer organizationId) {
		return OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationListOfStatusOneAndTripStatusOne(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(
			Integer organizationId) {
		return OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray getOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(
			Integer organizationId) throws Exception {
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		{   
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistration=OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(organizationId);
			if(outWardDispatchEnteryRegistration.size()>0)
			{
				for(OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistration)
				{
					json=new JSONObject();
					json.put("OUTWARD_DISPATCH_ENTERY_REG_ID", obj.getOutWardDispatchEnteryRegistrationId());
					json.put("TRIP_NO", obj.getOutWardTrip().getTripNo());
//					json.put("LOADING_CHARGES",obj.getOutWardTrip().getLoadingCharges());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("ORG_ID", obj.getOrganization().getOrganizationId());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("TRAN", obj.getStockist().getTransporterDetails().getTransporterName());
//					json.put("DISPATCH_BY",obj.getOutWardTrip().getDispatchBy());
//					json.put("VEHICAL_NO",obj.getOutWardTrip().getVehicleNo());
//					json.put("DRIVER_NO", obj.getOutWardTrip().getDriverNo());
					json.put("NO_OF_CASES", obj.getNoOfCases());
//					json.put("LR_NO", obj.getOutWardGatePass().getLRNo());
					json_ImageArray=new JSONArray();
					for(OutWardGatePassLrImagePath tempObj : obj.getOutWardGatePass().getOutWardGatePassLrImagePathList()){
						tempJson = new JSONObject();
						tempJson.put("IMAGE_PATH", tempObj.getImagePath());
						tempJson.put("IMAGE_ID", tempObj.getOutWardGatePassLrImagePathId());
						json_ImageArray.put(tempJson);
					}
					json.put("LR_IMAGE_PATH", json_ImageArray);
					json.put("OUTWARD_GATEPASS_ID", obj.getOutWardGatePass().getOutWardGatePassId());
					json.put("LR_NO", obj.getOutWardGatePass().getLRNo());
					
					// load all invoice no
					String invoiceNo="";
					String x="";
					//outWardGoodsService.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId
					List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=outWardOrderInvoiceEnteryRegistrationDao.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(obj.getOutWardDispatchEnteryRegistrationId());
					if(outWardOrderInvoiceEnteryRegistrationList.size()>0)
					{
						for(OutWardOrderInvoiceEnteryRegistration ooierObj : outWardOrderInvoiceEnteryRegistrationList)
						{
							invoiceNo+=ooierObj.getInvoiceNo()+",";
							 //x=invoiceNo.substring(invoiceNo.length() - 4);
							System.out.println("string is="+invoiceNo);
						}
					}
					System.out.println("chekc="+invoiceNo);
					json.put("INVOICE_NO", x);
					json_data_array.put(json);
				}
			}
		}
		return json_data_array;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardProductOrderEntryRegistration> getProductOrderEnteryRegistrationDetailsUsingOutWardOrderEntryRegistrationId(
			Integer OutWardOrderEntryRegistrationId) {
		// TODO Auto-generated method stub
		return outWardProductOrderEnteryRegistrationDao.getProductOrderEnteryRegistrationDetailsUsingOutWardOrderEntryRegistrationId(OutWardOrderEntryRegistrationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistrationUsignoutWardOrderEnteryRegistrationId(
			Integer outWardOrderEnteryRegistrationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.getOutWardOrderInvoiceEnteryRegistrationUsignoutWardOrderEnteryRegistrationId(outWardOrderEnteryRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardTripTransporterDetailsUsignOutWardTripId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardTripTransporterDetails> loadOutWardTripTransporterDetailsListUsignOutWardTripId(
			Integer outWardTripId) {
		return outWardTripTransporterDetailsDao.loadOutWardTripTransporterDetailsListUsignOutWardTripId(outWardTripId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(
			Integer outWardDispatchEnteryRegistrationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(
				 outWardDispatchEnteryRegistrationId) ;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(
			Integer outwardTripId) {
		return OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(outwardTripId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveOutwardTripReport(com.protocol.wms.model.OutwardTripReport)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOutwardTripReport(OutwardTripReport outwardTripReportObj) {
		return outwardTripReportDao.saveOutwardTripReport(outwardTripReportObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutwardTripReportListUsignOutWardTripId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutwardTripReport> loadOutwardTripReportListUsignOutWardTripId(
			Integer outWardTripId) {
		return outwardTripReportDao.loadOutwardTripReportListUsignOutWardTripId(outWardTripId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#deletePrintedOutwardTripReportObj(java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void deletePrintedOutwardTripReportObj(
			List<OutwardTripReport> printedOutwardTripReportObjList) {
		outwardTripReportDao.deletePrintedOutwardTripReportObj(printedOutwardTripReportObjList);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#savePrintSticker(com.protocol.wms.model.PrintSticker)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer savePrintSticker(PrintSticker printStickerObj) {
		return printStickerDao.savePrintSticker(printStickerObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadPrintStickerObjUsingOutWardDispatchEnteryRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public PrintSticker loadPrintStickerObjUsingOutWardDispatchEnteryRegistrationId(
			Integer outWardDispatchEnteryRegistrationId) {
		return printStickerDao.loadPrintStickerObjUsingOutWardDispatchEnteryRegistrationId(outWardDispatchEnteryRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#deletePrintStickerObj(com.protocol.wms.model.PrintSticker)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void deletePrintStickerObj(PrintSticker printStickerObj) {
		printStickerDao.deletePrintStickerObj(printStickerObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOutWardOrderEntryRegistrationObj(com.protocol.wms.model.OutWardOrderEntryRegistration, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateOutWardOrderEntryRegistrationObj(
			OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj,
			Integer orderModeId) {
		return outWardOrderEntryRegistrationDao.updateOutWardOrderEntryRegistrationObj(outWardOrderEntryRegistrationObj, orderModeId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateOutwardGoodsRegOrderCopyImage(
			Integer outwardGoodsRegistrationId,
			List<OutWardOrderEntryRegistrationOrderCopyPath> orderCopyImgList)
			throws Exception {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		orderCopyImgList = outWardOrderEntryRegistrationDao.updateOutwardGoodsRegOrderCopyImage(outwardGoodsRegistrationId, orderCopyImgList);
		for(OutWardOrderEntryRegistrationOrderCopyPath tempObj : orderCopyImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getOutWardOrderEntryRegistration());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardGoodsRegistrationOrderCopyImage(Integer orderCopyImageId) {
		return outWardOrderEntryRegistrationDao.deleteOutwardGoodsRegistrationOrderCopyImage(orderCopyImageId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardProductOrderEntryRegistration loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(
			Integer outWardProductOrderEntryRegistrationId) {
		return outWardProductOrderEnteryRegistrationDao.loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(outWardProductOrderEntryRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOutWardProductOrderEntryRegistrationObj(com.protocol.wms.model.OutWardProductOrderEntryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateOutWardProductOrderEntryRegistrationObj(
			OutWardProductOrderEntryRegistration OutWardProductOrderEntryRegistrationObj) {
		return  outWardProductOrderEnteryRegistrationDao.updateOutWardProductOrderEntryRegistrationObj(OutWardProductOrderEntryRegistrationObj);
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateOutWardTripDetails(Integer selectcartingAgentId,
			OutWardTrip outWardTripObj) {
		// TODO Auto-generated method stub
		if(selectcartingAgentId==outWardTripObj.getCartingAgent().getCartingAgentId()){
			outWardTripDao.updateOutWardTripObj(outWardTripObj);
			return true;
		}
		else{
			CartingAgentTripCount oldCartingAgentTripCountObj=cartingAgentTripCountDao.loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(outWardTripObj.getOrganization().getOrganizationId(), outWardTripObj.getCartingAgent().getCartingAgentId());
			if(oldCartingAgentTripCountObj!=null)
			{
				Integer tripCount=oldCartingAgentTripCountObj.getTripCount();
				oldCartingAgentTripCountObj.setTripCount(tripCount-1);
				cartingAgentTripCountDao.updateCartingAgentTripCountObj(oldCartingAgentTripCountObj);
			}
			CartingAgentTripCount selectedCartingAgentTripCountObj=cartingAgentTripCountDao.loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(outWardTripObj.getOrganization().getOrganizationId(), selectcartingAgentId);
			if(selectedCartingAgentTripCountObj!=null)
	    	{
	    		Integer cartingAgentTripCount=selectedCartingAgentTripCountObj.getTripCount()+1;
	    		selectedCartingAgentTripCountObj.setTripCount(cartingAgentTripCount);
	    		// update cartingAgentTripCount
	    		cartingAgentTripCountDao.updateCartingAgentTripCountObj(selectedCartingAgentTripCountObj);
	    	}
	    	else
	    	{
	    		Integer cartingAgentTripCount=1;
	    		CartingAgentTripCount newCartingAgentTripCountObj=new CartingAgentTripCount();
	    		newCartingAgentTripCountObj.setTripCount(cartingAgentTripCount);
	    		newCartingAgentTripCountObj.setOrganization(outWardTripObj.getOrganization());
	    		newCartingAgentTripCountObj.setCartingAgent(cartingAgentDao.getCartingAgentDetailsById(selectcartingAgentId));
	    		cartingAgentTripCountDao.saveCartingAgentTripCountObj(newCartingAgentTripCountObj);
	    	}
			//outWardTripObj.setCartingAgent(cartingAgentDao.getCartingAgentDetailsById(selectcartingAgentId));
			
			// update outward trip
			//outWardTripDao.updateOutWardTripObj(outWardTripObj);
			if(outWardTripDao.updateOutWardTripDetails(selectcartingAgentId,outWardTripObj)){
				return true;
			}
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOutWardGatePassObj(com.protocol.wms.model.OutWardGatePass)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateOutWardGatePassObj(OutWardGatePass outWardGatePassObj) {
		if(outWardGatePassDao.updateOutWardGatePassObj(outWardGatePassObj))
		{
			return true;
		}
		return false;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateOutwardGoodsGatePassLrImage(Integer outwardGatePassId,
			List<OutWardGatePassLrImagePath> lrImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		lrImgList = outWardGatePassDao.updateOutwardGoodsGatePassLrImage(outwardGatePassId, lrImgList);
		for(OutWardGatePassLrImagePath tempObj : lrImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getOutWardGatePassLrImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardGoodsGatePassLrImage(Integer lrImageId) {
		return outWardGatePassDao.deleteOutwardGoodsGatePassLrImage(lrImageId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOutWardDispatchEnteryRegistrationObjDetails(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateOutWardDispatchEnteryRegistrationObjDetails(
			Integer outWardDispatchEnteryRegistrationId, Integer noOfCases) {
		return OutWardDispatchEnteryRegistrationDao.updateOutWardDispatchEnteryRegistrationObjDetails(outWardDispatchEnteryRegistrationId, noOfCases);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateInvoiceObj(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration, java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateInvoiceObj(
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj,
			String invoiceDate,String invoiceDiscountTakentOrNot,Double netAmount) {
		try 
		{
				DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
				Integer paymentStatus=outWardOrderInvoiceEnteryRegistrationObj.getPaymentStatus();
//				Date oldInvoiceDate=outWardOrderInvoiceEnteryRegistrationObj.getInvoiceDate();
				java.util.Date str_invoiceDate = dtf.parse(invoiceDate);
				java.sql.Date selectedInvoiceDate=new java.sql.Date(str_invoiceDate.getTime());
				outWardOrderInvoiceEnteryRegistrationObj.setInvoiceDate(selectedInvoiceDate);
				
//				set first date and last date of invoice
					if(invoiceDiscountTakentOrNot.equalsIgnoreCase("Yes"))
				    {
				    	outWardOrderInvoiceEnteryRegistrationObj.setNoOfDays(0);
				    	outWardOrderInvoiceEnteryRegistrationObj.setFirstDate(selectedInvoiceDate);
				    	outWardOrderInvoiceEnteryRegistrationObj.setLastDate(selectedInvoiceDate);
				    }
				    else
				    {
				    	// check stockist days
						String stockistLocation=outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getStockist().getStockistLocation();
				    	Integer noOfDays=0;
						if(stockistLocation.equalsIgnoreCase("Local"))
						{
							CompanyCreditAndDiscount companyCreditAndDiscountObj=companyCreditAndDiscountDao.loadCompanyCreditAndDiscountObjectUsignComapnyId(outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getCompany().getCompanyId());
							noOfDays=companyCreditAndDiscountObj.getLocalDays();
						}
						else
						{
							CompanyCreditAndDiscount companyCreditAndDiscountObj=companyCreditAndDiscountDao.loadCompanyCreditAndDiscountObjectUsignComapnyId(outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getCompany().getCompanyId());
							noOfDays=companyCreditAndDiscountObj.getOutsideStationDays();
						}
				    	outWardOrderInvoiceEnteryRegistrationObj.setNoOfDays(noOfDays);
				    	outWardOrderInvoiceEnteryRegistrationObj.setFirstDate(selectedInvoiceDate);
				    	// calculate  next date after some days  
				    	Calendar cal = Calendar.getInstance();
				        cal.setTime(str_invoiceDate);
				        cal.add(Calendar.DATE, noOfDays); //minus number would decrement the days
				        java.sql.Date afterDate=new java.sql.Date(cal.getTimeInMillis());
				        outWardOrderInvoiceEnteryRegistrationObj.setLastDate(afterDate);
				    }
					
			if(paymentStatus==0)
			{
				// set remaining amount 
				if(netAmount.equals(outWardOrderInvoiceEnteryRegistrationObj.getRemainingPayment()))
				{
					outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(netAmount);
					outWardOrderInvoiceEnteryRegistrationObj.setNetAmount(netAmount);
				}
				else
				{
					double newNetAmount=netAmount;
					Double diff_oldNetAmount_and_newNetAmount=outWardOrderInvoiceEnteryRegistrationObj.getNetAmount()-netAmount;
					outWardOrderInvoiceEnteryRegistrationObj.setNetAmount(netAmount);
					if(diff_oldNetAmount_and_newNetAmount>0)
					{
						// load all payment information of given invoice
						List<InvoicePaymentInformation> invoicePaymentInformationList=invoicePaymentInformationDao.loadinInvoicePaymentInformationsListUsingOutWardOrderInvoiceEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderInvoiceEnteryRegistrationId());
						if(invoicePaymentInformationList.size()>0)
						{
							System.out.println("invoice no==="+outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderInvoiceEnteryRegistrationId());
							int flag=0;
							for(InvoicePaymentInformation obj : invoicePaymentInformationList)
							{
							double	paidAmount=obj.getPaidAmount();
								// paid invoice
								if(newNetAmount>=paidAmount)
								{
									newNetAmount=newNetAmount-paidAmount;
								}
								else if(flag==0)
								{
									paidAmount=paidAmount-newNetAmount;
									if(paidAmount>0)
									{
										// update invoice payment and add remaining payment
										
										//1--update invoice payment
										// load invoice payment information object using invoice payment information id
										InvoicePaymentInformation invoicePaymentInformationObj=invoicePaymentInformationDao.loadInvoicePaymentInformationUsigInvoicePaymentInformationId(obj.getInvoicePaymentInformationId());
										if(invoicePaymentInformationObj!=null)
										{
											invoicePaymentInformationObj.setPaidAmount(newNetAmount);
											//update invoice payment information object
										  boolean status=invoicePaymentInformationDao.updateInvoicePaymentInformationObj(invoicePaymentInformationObj);
										  if(status==true)
										  {
											  outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(0.0);
											  outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(1);
										  }
										}
										//2--update remaining Payment
										//load remaining cheque amount object from chequeNumberInfoId 
										RemainingChequeAmount remainingChequeAmountObjAmount=remainingChequeAmountDao.loadRemainingChequeAmountObjUsignChequeNumberInfoId(obj.getChequeNumberInfo().getChequeNumberInfoId());
										if(remainingChequeAmountObjAmount!=null)
										{
											remainingChequeAmountObjAmount.setRemainingChequeAmount(remainingChequeAmountObjAmount.getRemainingChequeAmount()+paidAmount);
											remainingChequeAmountDao.updateRemainingChequeAmountObj(remainingChequeAmountObjAmount);
										}
										else
										{
											RemainingChequeAmount newremainingChequeAmountObj=new RemainingChequeAmount();
											newremainingChequeAmountObj.setStatus(1);
											newremainingChequeAmountObj.setChequeNumberInfo(chequeNumberInfoDao.loadChequeNumberInfoObjUsignChequeNumberInfoId(obj.getChequeNumberInfo().getChequeNumberInfoId()));
											newremainingChequeAmountObj.setCompany(companyDao.loadCompanyObjectUsingCompanyId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getCompany().getCompanyId()));
											newremainingChequeAmountObj.setOrganization(organizationDao.loadOrganizationObjectUsingOrganiztionId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getOrganization().getOrganizationId()));
											newremainingChequeAmountObj.setStockist(stockistDao.loadStockistObjUsigStokistId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getStockist().getStockistId()));
											newremainingChequeAmountObj.setStockistBankDetails(stockistBankDetailsDao.loadStockistBankDetailsUsignStockistBankDetailsId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getStockistBankDetails().getStockistBankDetailsId()));
											newremainingChequeAmountObj.setRemainingChequeAmount(paidAmount);
											remainingChequeAmountDao.saveRemainingChequeAmountObj(newremainingChequeAmountObj);
											
										}
										newNetAmount=0;
									}
									netAmount=(double) 0;
									flag=1;
								}
								else
								{
									//load remaining cheque amount object from chequeNumberInfoId 
									RemainingChequeAmount remainingChequeAmountObjAmount=remainingChequeAmountDao.loadRemainingChequeAmountObjUsignChequeNumberInfoId(obj.getChequeNumberInfo().getChequeNumberInfoId());
									if(remainingChequeAmountObjAmount!=null)
									{
										remainingChequeAmountObjAmount.setRemainingChequeAmount(remainingChequeAmountObjAmount.getRemainingChequeAmount()+paidAmount);
										remainingChequeAmountDao.updateRemainingChequeAmountObj(remainingChequeAmountObjAmount);
									}
									else
									{
										RemainingChequeAmount newremainingChequeAmountObj=new RemainingChequeAmount();
										newremainingChequeAmountObj.setStatus(1);
										newremainingChequeAmountObj.setChequeNumberInfo(chequeNumberInfoDao.loadChequeNumberInfoObjUsignChequeNumberInfoId(obj.getChequeNumberInfo().getChequeNumberInfoId()));
										newremainingChequeAmountObj.setCompany(companyDao.loadCompanyObjectUsingCompanyId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getCompany().getCompanyId()));
										newremainingChequeAmountObj.setOrganization(organizationDao.loadOrganizationObjectUsingOrganiztionId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getOrganization().getOrganizationId()));
										newremainingChequeAmountObj.setStockist(stockistDao.loadStockistObjUsigStokistId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getStockist().getStockistId()));
										newremainingChequeAmountObj.setStockistBankDetails(stockistBankDetailsDao.loadStockistBankDetailsUsignStockistBankDetailsId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getStockistBankDetails().getStockistBankDetailsId()));
										newremainingChequeAmountObj.setRemainingChequeAmount(paidAmount);
										remainingChequeAmountDao.saveRemainingChequeAmountObj(newremainingChequeAmountObj);
									}
									// delete paid invoice payment information
									invoicePaymentInformationDao.deleteInvoicePaymentInformationObj(obj);
								}
							}
						}
						else
						{
							outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(netAmount);
						}
						
						if(newNetAmount>0)
						{
							outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(newNetAmount);
							outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(0);
						}
						if(newNetAmount==0)
						{
							outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(0.0);
							outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(1);
						}
					}
					else
					{
						Double test=outWardOrderInvoiceEnteryRegistrationObj.getRemainingPayment()-diff_oldNetAmount_and_newNetAmount;
						outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(test);
					}
				}
				outWardOrderInvoiceEnteryRegistrationDao.updateInvoiceObj(outWardOrderInvoiceEnteryRegistrationObj, invoiceDate,invoiceDiscountTakentOrNot,netAmount);
			}
			else
			{
				// check new net amount  and old new amount are equals
				if(netAmount.equals(outWardOrderInvoiceEnteryRegistrationObj.getNetAmount()))
				{
					outWardOrderInvoiceEnteryRegistrationDao.updateInvoiceObj(outWardOrderInvoiceEnteryRegistrationObj, invoiceDate,invoiceDiscountTakentOrNot,netAmount);
				}
				else
				{
					Double diff_oldNetAmount_and_newNetAmount=netAmount-outWardOrderInvoiceEnteryRegistrationObj.getNetAmount();
					// check new net amount  and old new amount are less or greater
					Double oldnetAmount=outWardOrderInvoiceEnteryRegistrationObj.getNetAmount();
					outWardOrderInvoiceEnteryRegistrationObj.setNetAmount(netAmount);
					Integer netAmountStatus=netAmount.compareTo(oldnetAmount);
					if(netAmountStatus==-1)
					{
//						Double diff_oldNetAmount_and_newNetAmount=outWardOrderInvoiceEnteryRegistrationObj.getNetAmount()-netAmount;
						//less new amount
						System.out.println("in less");
						List<InvoicePaymentInformation> invoicePaymentInformationList=invoicePaymentInformationDao.loadinInvoicePaymentInformationsListUsingOutWardOrderInvoiceEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderInvoiceEnteryRegistrationId());
						if(invoicePaymentInformationList.size()>0)
						{
							System.out.println("invoice no==="+outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderInvoiceEnteryRegistrationId());
							double newNetAmount=netAmount;
							int flag=0;
							for(InvoicePaymentInformation obj : invoicePaymentInformationList)
							{
							double	paidAmount=obj.getPaidAmount();
								// paid invoice
								if(newNetAmount>=paidAmount)
								{
									newNetAmount=newNetAmount-paidAmount;
								}
								else if(flag==0)
								{
									paidAmount=paidAmount-newNetAmount;
									if(paidAmount>0)
									{
										// update invoice payment and add remaining payment
										
										//1--update invoice payment
										// load invoice payment information object using invoice payment information id
										InvoicePaymentInformation invoicePaymentInformationObj=invoicePaymentInformationDao.loadInvoicePaymentInformationUsigInvoicePaymentInformationId(obj.getInvoicePaymentInformationId());
										if(invoicePaymentInformationObj!=null)
										{
											invoicePaymentInformationObj.setPaidAmount(newNetAmount);
											//update invoice payment information object
										  boolean status=invoicePaymentInformationDao.updateInvoicePaymentInformationObj(invoicePaymentInformationObj);
										  if(status==true)
										  {
											  outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(0.0);
											  outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(1);
										  }
										}
										//2--update remaining Payment
										//load remaining cheque amount object from chequeNumberInfoId 
										RemainingChequeAmount remainingChequeAmountObjAmount=remainingChequeAmountDao.loadRemainingChequeAmountObjUsignChequeNumberInfoId(obj.getChequeNumberInfo().getChequeNumberInfoId());
										if(remainingChequeAmountObjAmount!=null)
										{
											remainingChequeAmountObjAmount.setRemainingChequeAmount(remainingChequeAmountObjAmount.getRemainingChequeAmount()+paidAmount);
											remainingChequeAmountDao.updateRemainingChequeAmountObj(remainingChequeAmountObjAmount);
										}
										else
										{
											RemainingChequeAmount newremainingChequeAmountObj=new RemainingChequeAmount();
											newremainingChequeAmountObj.setStatus(1);
											newremainingChequeAmountObj.setChequeNumberInfo(chequeNumberInfoDao.loadChequeNumberInfoObjUsignChequeNumberInfoId(obj.getChequeNumberInfo().getChequeNumberInfoId()));
											newremainingChequeAmountObj.setCompany(companyDao.loadCompanyObjectUsingCompanyId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getCompany().getCompanyId()));
											newremainingChequeAmountObj.setOrganization(organizationDao.loadOrganizationObjectUsingOrganiztionId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getOrganization().getOrganizationId()));
											newremainingChequeAmountObj.setStockist(stockistDao.loadStockistObjUsigStokistId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getStockist().getStockistId()));
											newremainingChequeAmountObj.setStockistBankDetails(stockistBankDetailsDao.loadStockistBankDetailsUsignStockistBankDetailsId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getStockistBankDetails().getStockistBankDetailsId()));
											newremainingChequeAmountObj.setRemainingChequeAmount(paidAmount);
											remainingChequeAmountDao.saveRemainingChequeAmountObj(newremainingChequeAmountObj);
											
										}
										newNetAmount=0;
									}
									netAmount=(double) 0;
									flag=1;
								}
								else
								{
									//load remaining cheque amount object from chequeNumberInfoId 
									RemainingChequeAmount remainingChequeAmountObjAmount=remainingChequeAmountDao.loadRemainingChequeAmountObjUsignChequeNumberInfoId(obj.getChequeNumberInfo().getChequeNumberInfoId());
									if(remainingChequeAmountObjAmount!=null)
									{
										remainingChequeAmountObjAmount.setRemainingChequeAmount(remainingChequeAmountObjAmount.getRemainingChequeAmount()+paidAmount);
										remainingChequeAmountDao.updateRemainingChequeAmountObj(remainingChequeAmountObjAmount);
									}
									else
									{
										RemainingChequeAmount newremainingChequeAmountObj=new RemainingChequeAmount();
										newremainingChequeAmountObj.setStatus(1);
										newremainingChequeAmountObj.setChequeNumberInfo(chequeNumberInfoDao.loadChequeNumberInfoObjUsignChequeNumberInfoId(obj.getChequeNumberInfo().getChequeNumberInfoId()));
										newremainingChequeAmountObj.setCompany(companyDao.loadCompanyObjectUsingCompanyId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getCompany().getCompanyId()));
										newremainingChequeAmountObj.setOrganization(organizationDao.loadOrganizationObjectUsingOrganiztionId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getOrganization().getOrganizationId()));
										newremainingChequeAmountObj.setStockist(stockistDao.loadStockistObjUsigStokistId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getStockist().getStockistId()));
										newremainingChequeAmountObj.setStockistBankDetails(stockistBankDetailsDao.loadStockistBankDetailsUsignStockistBankDetailsId(obj.getChequeNumberInfo().getAdvancedChequeInformation().getStockistBankDetails().getStockistBankDetailsId()));
										newremainingChequeAmountObj.setRemainingChequeAmount(paidAmount);
										remainingChequeAmountDao.saveRemainingChequeAmountObj(newremainingChequeAmountObj);
									}
									// delete paid invoice payment information
									invoicePaymentInformationDao.deleteInvoicePaymentInformationObj(obj);
								}
							}
						}
						
					}
					else
					{
						// change the invoice payment status and add remaining payment
						outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(diff_oldNetAmount_and_newNetAmount);
						outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(0);
						System.out.println("diff_oldNetAmount_and_newNetAmount====="+diff_oldNetAmount_and_newNetAmount);
					}
					outWardOrderInvoiceEnteryRegistrationDao.updateInvoiceObj(outWardOrderInvoiceEnteryRegistrationObj, invoiceDate, invoiceDiscountTakentOrNot, diff_oldNetAmount_and_newNetAmount);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return true;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object>searchOutwardOrderEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		// TODO Auto-generated method stub
		return outWardOrderEntryRegistrationDao.searchOutwardOrderEntryRegistration(selectedValue, searchText, fromSpecialData,
				 toSpecialData,from, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#deleteLrDetails(com.protocol.wms.model.OutWardGatePass)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateOutWardDispatchObjOfOutWardGetPassStatusZero(OutWardGatePass outWardGatePassObj) {
		try 
		{
			OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj=OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationObjUsignOutWardGatePassId(outWardGatePassObj.getOutWardGatePassId());
			if(outWardDispatchEnteryRegistrationObj!=null)
			{
				outWardDispatchEnteryRegistrationObj.setGetPassStatus(0);
				OutWardGatePass obj=null;
				outWardDispatchEnteryRegistrationObj.setOutWardGatePass(obj);
				if(OutWardDispatchEnteryRegistrationDao.updateOutWardDispatchEnteryRegistrationObjDetails(outWardDispatchEnteryRegistrationObj))
				{
						return true;
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#deleteLrDetailsObj(com.protocol.wms.model.OutWardGatePass)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean deleteLrDetailsObj(OutWardGatePass outWardGatePassObj) {
		return outWardGatePassDao.deleteLrDetails(outWardGatePassObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#updateOutWardDispatchObjOfOutWardTripStatusZero(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateOutWardDispatchObjOfOutWardTripStatusZero(OutWardTrip outWardTripObj) {
		try 
		{
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationsList=OutWardDispatchEnteryRegistrationDao.loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(outWardTripObj.getOutWardTripId());
			if(outWardDispatchEnteryRegistrationsList.size()>0)
			{
				for(OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistrationsList)
				{
					obj.setTripStatus(0);
					OutWardTrip newOutWardTripObj=null;
					obj.setOutWardTrip(newOutWardTripObj);
					OutWardDispatchEnteryRegistrationDao.updateOutWardDispatchEnteryRegistrationObjDetails(obj);
				}
				
				// update organization trip count and carting agent trip count -1
				
				//load organization trip count obj using organization id
				OrganizationTripCount organizationTripCountObj=organizationTripCountDao.loadOrganizationTripCountObjUsignOrgId(outWardTripObj.getOrganization().getOrganizationId());
				if(organizationTripCountObj!=null)
				{
					organizationTripCountObj.setTripCount(organizationTripCountObj.getTripCount()-1);
					organizationTripCountDao.updateOrganizationTripCount(organizationTripCountObj);
				}
				
				//load carting agent trip count obj using carting agent id
				CartingAgentTripCount cartingAgentTripCountObj=cartingAgentTripCountDao.loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(outWardTripObj.getOrganization().getOrganizationId(), outWardTripObj.getCartingAgent().getCartingAgentId());
				if(cartingAgentTripCountObj!=null)
				{
					cartingAgentTripCountObj.setTripCount(cartingAgentTripCountObj.getTripCount()-1);
					cartingAgentTripCountDao.updateCartingAgentTripCountObj(cartingAgentTripCountObj);
				}
				return true;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#deleteOutWardTripEntryObj(com.protocol.wms.model.OutWardTrip)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean deleteOutWardTripEntryObj(OutWardTrip outWardTripObj) {
		return outWardTripDao.deleteOutWardTripEntryObj(outWardTripObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#deleteOutWardProductOrderEntryRegistrationObj(com.protocol.wms.model.OutWardProductOrderEntryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateOutWardOrderEntryRegistrationObjOfProductStatusZero(
			OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj) {
		try 
		{
			// check more product are available or not 
			List<OutWardProductOrderEntryRegistration> outWardProductOrderEntryRegistrationsList=outWardProductOrderEnteryRegistrationDao.getProductOrderEnteryRegistrationDetailsUsingOutWardOrderEntryRegistrationId(outWardProductOrderEntryRegistrationObj.getOutWardOrderEntryRegistration().getOutWardOrderEnteryRegistrationId());
			if(outWardProductOrderEntryRegistrationsList.size()==1)
			{
				// change the product status of out ward order entry registration to zero
				//load outward order entry registration object using outward order entry id
				OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj=outWardOrderEntryRegistrationDao.loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(outWardProductOrderEntryRegistrationObj.getOutWardOrderEntryRegistration().getOutWardOrderEnteryRegistrationId());
				outWardOrderEntryRegistrationObj.setProductStatus(0);
				outWardOrderEntryRegistrationDao.updateOutWardOrderEntryRegistration(outWardOrderEntryRegistrationObj);
				return true;
			}
			else
			{
				return true;
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#deleteOutWardProductOrderEntryRegistrationObj(com.protocol.wms.model.OutWardProductOrderEntryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean deleteOutWardProductOrderEntryRegistrationObj(
			OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj) {
		return outWardProductOrderEnteryRegistrationDao.deleteOutWardProductOrderEntryRegistrationObj(outWardProductOrderEntryRegistrationObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadinInvoicePaymentInformationsListUsingOutWardOrderInvoiceEnteryRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InvoicePaymentInformation> loadinInvoicePaymentInformationsListUsingOutWardOrderInvoiceEnteryRegistrationId(
			Integer outWardOrderInvoiceEnteryRegistrationId) {
		return invoicePaymentInformationDao.loadinInvoicePaymentInformationsListUsingOutWardOrderInvoiceEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#deleteOutWardOrderInvoiceEnteryRegistrationObj(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean deleteOutWardOrderInvoiceEnteryRegistrationObj(
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj) {
		return outWardOrderInvoiceEnteryRegistrationDao.deleteOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadRemainingChequeAmountObjUsignChequeNumberInfoId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public RemainingChequeAmount loadRemainingChequeAmountObjUsignChequeNumberInfoId(
			Integer chequeNumberInfoId) {
		return remainingChequeAmountDao.loadRemainingChequeAmountObjUsignChequeNumberInfoId(chequeNumberInfoId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadStockistObjUsigStokistId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Stockist loadStockistObjUsigStokistId(Integer stokistId) {
		return stockistDao.loadStockistObjUsigStokistId(stokistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#loadStockistBankDetailsUsignStockistBankDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public StockistBankDetails loadStockistBankDetailsUsignStockistBankDetailsId(
			Integer stockistBankDetailsId) {
		return stockistBankDetailsDao.loadStockistBankDetailsUsignStockistBankDetailsId(stockistBankDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#saveRemainingChequeAmountObj(com.protocol.wms.model.RemainingChequeAmount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveRemainingChequeAmountObj(
			RemainingChequeAmount RemainingChequeAmountObj) {
		return remainingChequeAmountDao.saveRemainingChequeAmountObj(RemainingChequeAmountObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#changeDispatchEntryRegistrationStatusToZero(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean changeDispatchEntryRegistrationStatusToZero(
			Integer outWardDispatchEntryRegistarionId) {
		try 
		{
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=outWardOrderInvoiceEnteryRegistrationDao.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(outWardDispatchEntryRegistarionId);
			if(outWardOrderInvoiceEnteryRegistrationList.size()>0)
			{
				for(OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEnteryRegistrationList)
				{
					OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj=null;
					obj.setDispatchEnteryRegistrationStatus(0);
					obj.setOutWardDispatchEnteryRegistration(outWardDispatchEnteryRegistrationObj);
					outWardOrderInvoiceEnteryRegistrationDao.updateOutWardOrderInvoiceEnteryRegistrationObj(obj);
				}
				return true;
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardGoodsService#deleteOutWardDispatchEnteryRegistrationObjDetails(com.protocol.wms.model.OutWardDispatchEnteryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean deleteOutWardDispatchEnteryRegistrationObjDetails(
			OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj) {
		return OutWardDispatchEnteryRegistrationDao.deleteOutWardDispatchEnteryRegistrationObjDetails(outWardDispatchEnteryRegistrationObj);
	}
	
	

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object>searchOutwardViewOrderEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		// TODO Auto-generated method stub
		return outWardOrderEntryRegistrationDao.searchOutwardViewOrderEntryRegistration(selectedValue, searchText, fromSpecialData,
				 toSpecialData, from, organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object>searchOutwardinvoiceEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer from,Integer organizationId) {
		// TODO Auto-generated method stub
		return outWardOrderEntryRegistrationDao.searchOutwardinvoiceEntryRegistration(selectedValue, searchText, fromSpecialData,
				 toSpecialData,from,organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object>searchOutwardInvoiceEntryAddedRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		// TODO Auto-generated method stub
		return outWardOrderEntryRegistrationDao.searchOutwardInvoiceEntryAddedRegistrationList(selectedValue, searchText, fromSpecialData,
				 toSpecialData,from, organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchOutwardDispatchEntryRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.searchOutwardDispatchEntryRegistrationList(selectedValue, searchText, fromSpecialData,
				 toSpecialData,from, organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchOutwardAddTrip(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		
		return OutWardDispatchEnteryRegistrationDao.searchOutwardAddTrip(selectedValue, searchText, fromSpecialData,
				 toSpecialData, from,organizationId);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchOutwardViewDispatchEntryRegistrationList(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		return OutWardDispatchEnteryRegistrationDao.searchOutwardViewDispatchEntryRegistrationList(selectedValue, searchText, fromSpecialData,
				toSpecialData,from, organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchOutwardViewTripList(String selectedValue,String searchText, String fromSpecialData, String toSpecialData,
			Integer from,Integer organizationId) {

		return outWardTripDao.searchOutwardViewTripList(selectedValue, searchText, fromSpecialData,
				toSpecialData,from,organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object>searchOutwardGetPassList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from,Integer organizationId) {

		return OutWardDispatchEnteryRegistrationDao.searchOutwardGetPassList(selectedValue, searchText, fromSpecialData,
				toSpecialData,from,organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray searchViewGetPassLRList(String selectedValue,
			String searchText, String fromSpecialData, String toSpecialData,Integer from,
			Integer organizationId) {
		
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistration=null;
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		JSONObject jsonInvoiceLR = null;
		JSONObject jsonInvoiceADD = null;
		JSONArray json_array=null;
		{
			try {
				Map<String,Object> map=OutWardDispatchEnteryRegistrationDao.searchViewGetPassLRList(selectedValue, searchText, fromSpecialData,
						 toSpecialData,from, organizationId);
				//if(outWardDispatchEnteryRegistration.size()>0)
				//{
				outWardDispatchEnteryRegistration=(List<OutWardDispatchEnteryRegistration>) map.get("outWardDispatchEnteryRegistrationList");
				Integer totalPages = (Integer)map.get("totalPages");
				if(outWardDispatchEnteryRegistration!=null){
					
					for(OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistration)
					{
						json=new JSONObject();
						
						json.put("OUTWARD_DISPATCH_ENTERY_REG_ID", obj.getOutWardDispatchEnteryRegistrationId());
						json.put("TRIP_NO", obj.getOutWardTrip().getTripNo());
//					json.put("LOADING_CHARGES",obj.getOutWardTrip().getLoadingCharges());
						json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
						json.put("ORG_ID", obj.getOrganization().getOrganizationId());
						json.put("COMPANY", obj.getCompany().getCompanyName());
						json.put("STOCKIST", obj.getStockist().getStockistName());
						json.put("CARTING",obj.getOutWardTrip().getCartingAgent().getName());
						json.put("LOADING_CHARGES", obj.getOutWardTrip().getLoadingCharges());
						json.put("TRAN", obj.getStockist().getTransporterDetails().getTransporterName());
					json.put("DISPATCH_BY",obj.getOutWardTrip().getDispatchBy());
					json.put("VEHICAL_NO",obj.getOutWardTrip().getVehicleNo());
					json.put("DRIVER_NO", obj.getOutWardTrip().getDriverNo());
					json.put("TRIP_CASE",obj.getOutWardTrip().getNoOfCases());
					
						json.put("NO_OF_CASES", obj.getNoOfCases());
						String date="";
						date+=obj.getOutWardTrip().getSubmitDate();
						String DATE=obj.getOutWardTrip().getSubmitDate().toString().substring(0,10);
						
						json.put("DATE", DATE); 
						
					json.put("LR_NO", obj.getOutWardGatePass().getLRNo());
						json_ImageArray=new JSONArray();
						for(OutWardGatePassLrImagePath tempObj : obj.getOutWardGatePass().getOutWardGatePassLrImagePathList()){
							tempJson = new JSONObject();
							tempJson.put("IMAGE_PATH", tempObj.getImagePath());
							tempJson.put("IMAGE_ID", tempObj.getOutWardGatePassLrImagePathId());
							json_ImageArray.put(tempJson);
						}
						json.put("LR_IMAGE_PATH", json_ImageArray);
						json.put("OUTWARD_GATEPASS_ID", obj.getOutWardGatePass().getOutWardGatePassId());
						json.put("LR_NO", obj.getOutWardGatePass().getLRNo());
						// load all invoice no
						String invoiceNo="";
						String tempInvoiceNo="";
						//outWardGoodsService.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId
						List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=outWardOrderInvoiceEnteryRegistrationDao.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(obj.getOutWardDispatchEnteryRegistrationId());
						
						if(outWardOrderInvoiceEnteryRegistrationList.size()>0)												
						{
							 json_array= new JSONArray();
							for(OutWardOrderInvoiceEnteryRegistration ooierObj : outWardOrderInvoiceEnteryRegistrationList)
							{    
								if(ooierObj.getInvoiceNo().length()>=5)
								{
									tempInvoiceNo+=ooierObj.getInvoiceNo().substring(ooierObj.getInvoiceNo().length()-4)+",";
								}
								else
								{
									tempInvoiceNo+=ooierObj.getInvoiceNo()+",";
								
							}
				
			// for invoice info
								jsonInvoiceADD = new JSONObject();
								jsonInvoiceADD.put("INVOICE", ooierObj.getInvoiceNo());
								jsonInvoiceADD.put("GROSS_AMT", ooierObj.getGrossAmount());
//								System.out.println("AMOUNT IS"+ooierObj.getGrossAmount());
								jsonInvoiceADD.put("IN_DATE",ooierObj.getInvoiceDate());
								jsonInvoiceADD.put("TAX_AMT", ooierObj.getTaxAmout());
								jsonInvoiceADD.put("VAT_AMT", ooierObj.getVatAmount());
								jsonInvoiceADD.put("DISCOUNT", ooierObj.getDiscountIsTakenOrNot());
								jsonInvoiceADD.put("NET_AMT", ooierObj.getNetAmount());
								
								
						//-------- Organization		
								json.put("ORG_NAME", ooierObj.getOutWardOrderEnteryRegistration().getOrganization().getOrganizationName());
								json.put("COMPANY_NAME",ooierObj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
								json.put("ORDER_NAME",ooierObj.getOutWardOrderEnteryRegistration().getOrderMode().getOrderModeName());
								json.put("STOCKIST_NAME",ooierObj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
								json.put("ORDER_NO",ooierObj.getOutWardOrderEnteryRegistration().getOrderNo());
								json.put("ORG_ID",ooierObj.getOutWardOrderEnteryRegistration().getOrganization().getOrganizationId());
								json.put("ORDER_DATE",ooierObj.getOutWardOrderEnteryRegistration().getOrderDate());
									
								json_ImageArray=new JSONArray();
								for(OutWardOrderEntryRegistrationOrderCopyPath tempObj :  ooierObj.getOutWardOrderEnteryRegistration().getOutWardOrderEntryRegistrationOrderCopyPathList()){
									tempJson = new JSONObject();
									tempJson.put("IMAGE_PATH", tempObj.getImagePath());
//									System.out.println(" IMAGE IS="+tempObj.getImagePath());
									tempJson.put("IMAGE_ID", tempObj.getOutWardOrderEntryRegistrationOrderCopyPathId());
//									System.out.println("new file image is="+tempObj.getOutWardOrderEntryRegistrationOrderCopyPathId());
									json_ImageArray.put(tempJson);
								}
								json.put("ORDER_COPY", json_ImageArray);
//								System.out.println("VIEW IMAGE IS="+json_ImageArray);
								json_array.put(jsonInvoiceADD);
								//---------------------
								
							}
						
						}
						json.put("INVOICE_VIEW",json_array);
						json.put("INVOICE_NO", tempInvoiceNo);
						json_data_array.put(json);
					}
				}
				json=new JSONObject();
				json.put("paginationCount",totalPages);
				json_data_array.put(json);
					
			//	}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return json_data_array;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean deleteOutWardOrderEntry(
			OutWardOrderEntryRegistration outWardOrderEntryRegistration) {
		return outWardOrderEntryRegistrationDao.deleteOutWardOrderEntry(outWardOrderEntryRegistration);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String, Object> searchOutwardViewDispatchEntryPrintStickerList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer from, Integer organizationId) {
		return OutWardDispatchEnteryRegistrationDao.searchOutwardViewPrintStickerEntryRegistrationList(selectedValue, searchText, fromSpecialData, toSpecialData, from, organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean checkName(String orderNo, Integer companyId) {
		return outWardOrderEntryRegistrationDao.checkName(orderNo,companyId);
	//	}
	
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderEntryRegistrationOrderCopyPath> loadImages(Integer outWardOrderEnteryRegistrationId) {
		return outwardOrderEntryRegistrationImageCopyPathDao.loadImages(outWardOrderEnteryRegistrationId);
	}

}
