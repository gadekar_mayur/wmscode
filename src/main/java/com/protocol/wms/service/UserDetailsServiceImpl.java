/**
 * 
 */
package com.protocol.wms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.UserDetailsDao;
import com.protocol.wms.model.UserDetails;

/**
 * @author admin
 *
 */
@Service("UserDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private UserDetailsDao userDetailsDao;

	/**
	 * @param userDetailsDao the userDetailsDao to set
	 */
	@Autowired
	@Qualifier("UserDetailsDaoImpl")
	public void setUserDetailsDao(UserDetailsDao userDetailsDao) {
		this.userDetailsDao = userDetailsDao;
	}

	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public UserDetails loginValidate(String uname, String password) {
		return userDetailsDao.loginValidate(uname, password);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.UserDetailsService#checkUserNameAlreadyExitOrNot(java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public UserDetails checkUserNameAlreadyExitOrNot(String uname) {
		return userDetailsDao.checkUserNameAlreadyExitOrNot(uname);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.UserDetailsService#checkUserNameAlreadyExitOrNot(java.lang.Integer, java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean checkUserNameAlreadyExitOrNot(Integer userDetailsId,
			String uname) {
		return userDetailsDao.checkUserNameAlreadyExitOrNot(userDetailsId, uname);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.UserDetailsService#saveUserDetails(com.protocol.wms.model.UserDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveUserDetails(UserDetails userDetailsObj) {
		return userDetailsDao.saveUserDetails(userDetailsObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.UserDetailsService#loadUserDetailsUsingUserDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public UserDetails loadUserDetailsUsingUserDetailsId(Integer userDetailsId) {
		return userDetailsDao.loadUserDetailsUsingUserDetailsId(userDetailsId);
	}

}
