package com.protocol.wms.service;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.DistrictDao;
import com.protocol.wms.model.District;

@Service("DistrictServiceImpl")
public class DistrictServiceImpl implements DistrictService {
	
	@Autowired
	@Qualifier("DistrictDaoImpl")
	private DistrictDao districtDao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getDistrictList(Integer stateId) {
		// TODO Auto-generated method stub
		return districtDao.getDistrictList(stateId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.DistrictService#loadDistricObjectUsingDstrictId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public District loadDistricObjectUsingDstrictId(Integer districtId) {
		return districtDao.loadDistricObjectUsingDstrictId(districtId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateDistrictObj(District districtObj) {
		return districtDao.updateDistrictObj(districtObj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<District> loadDistrictList() {
		return districtDao.loadDistrictList();
	}
}
