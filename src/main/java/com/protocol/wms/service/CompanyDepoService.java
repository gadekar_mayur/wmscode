/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.CompanyDepo;

/**
 * @author admin
 *
 */
public interface CompanyDepoService {

	public Boolean saveCompanyDepoDetails(Integer organizationId,Integer companyId,Integer stateId,Integer districtId,Integer cityId,CompanyDepo companyDepo);
	
	public Integer insertNewCompanyDepoDetailsAndDeleteExistingRecord(Integer stateId,Integer districtId,Integer cityId, Integer companyDepoId, CompanyDepo companyDepo);
	
	public Boolean updateCompanyDepoDetails(CompanyDepo companyDepo);
	
	public List<CompanyDepo> getCompanyDepoListing(Integer organizationId);
	
	public Map<String,Object> searchCompanyDepotDetails(String searchOption,String searchText,Integer from, Integer organizationId);
	
	public List<CompanyDepo> getCompanyDepoListByCompanyId(Integer companyId);
	
	public Boolean deleteCompanyDepo(Integer depoId);
	
	public CompanyDepo loadCompanyDepoObjectUsignCompanyDepoId(Integer companyDepoId);
	
}
