package com.protocol.wms.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CityDao;
import com.protocol.wms.model.City;

@Service("CityServiceImpl")
public class CityServiceImpl implements CityService {

	@Autowired
	@Qualifier("CityDaoImpl")
	private CityDao cityDao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getCityList(Integer districtId) {
		// TODO Auto-generated method stub
		return cityDao.getCityList(districtId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CityService#loadCityObjectUsingCityId(java.lang.Integer)
	 */
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public City loadCityObjectUsingCityId(Integer cityId) {
		return cityDao.loadCityObjectUsingCityId(cityId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateCityObject(City cityObj)
	{
		return cityDao.updateCityObject(cityObj);		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public String getCityName(Integer cityId) {
		// TODO Auto-generated method stub
		return cityDao.getCityName(cityId);
	}

}
