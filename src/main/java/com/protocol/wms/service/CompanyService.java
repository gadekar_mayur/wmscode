package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyBank;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.CompanyDocument;
import com.protocol.wms.model.CompanyDocumentImagePath;
import com.protocol.wms.model.CompanyProduct;
import com.protocol.wms.model.OrganizationBank;
import com.protocol.wms.model.UserDetails;

public interface CompanyService {

	public JSONObject getCompanyList(Integer orgId);
	
	public JSONObject getCompanyDropDownForCredit_Discount(Integer organizationId);
	
	public Company loadCompanyObjectUsingCompanyId(Integer companyId);
	
	public JSONObject getCompanyListAccToStockist(Integer stockistId,Integer orgId);
	
	public Boolean checkUserNameAlreadyExitOrNot(String uname);
	
	public Boolean saveCompanyDetails(Integer orgId,Integer stateId,Integer districtId,Integer cityId,Integer roleTypeId,UserDetails userDetails ,Company company);
	
	public List<Company> companyDetailsList(Integer organizationId);
	
	public Map<String,Object> searchAssignedBankDetailsOfOrgToCompany(String searchOption,String searchText,Integer from, Integer organizationId);
	
	public Map<String , Object> searchCompanyDetails(String searchOption,String searchText, Integer from, Integer orgId);
	
	public boolean saveCompanyBankDetails(Integer orgId,Integer companyId,CompanyBank companyBank);
	
	public List<CompanyBank> companyBankDetailsListing(Integer organizationId);
	
	public Map<Object,Object> searchCompanyBankDetails(String searchOption,String searchText,Integer organizationId, Integer from);
	
	public List<CompanyBank> companyBankDropdown(Integer companyId,Integer organizationId);
	
	public Boolean saveCompanyProduct(Integer organizationId,Integer companyId,CompanyProduct companyProduct);
	
	public List<CompanyProduct> getCompanyProductListing(Integer organizationId);
	
	public Map<String , Object> searchCompanyProductDetails(String searchOption,String searchText,Double fromValue,Double toValue, Integer from, Integer organizationId);
	
	public List<OrganizationBank> loadOrganizationBanksListUsingOrganizationId(Integer organiztionNameId);
	
	public JSONArray getCompanyOragainzationBankList(Integer companyId);
	
	public Boolean saveOrganizationBankToCompany(Integer organizationId,Integer companyId,Integer [] orgBanks);
	
	public Boolean uploadCompanyDocs(Integer orgId,Integer companyId,Integer documentTypeId,CompanyDocument companyDocument);
	
	public List<CompanyDocument> getCompanyDocsListing(Integer organizationId);
	
	public JSONArray getCompanyDocsListingView(Integer organizationId) throws Exception;
	
	public JSONArray searchUploadedCompanyDocsDetails(String searchOption,String searchText,Integer organizationId,Integer from) throws Exception;
	
	public Integer saveCompanyCreditAndDiscount(Integer organizationId,Integer companyId,CompanyCreditAndDiscount companyCreditAndDiscount);
	
	public Boolean deleteCompanyDetails(Integer companyId);
	
	public Boolean deleteCompanyBankDetails(Integer companyBankId);
	
	
	public Boolean deleteCompanyProduct(Integer productId);
	
	public Boolean  deleteCompanyUploadedDocs(Integer companyDocId);
	
	public List<Integer> loadOrganizationBankIdListUsingCompanyId(Integer companyId);
	
	public OrganizationBank loadOrganizationBankObjectUsingOrganizationBankId(Integer organizationBankId);
	
	public CompanyProduct loadCompanyProductObjectUsignCompanyProductId(Integer companyProductId);
	
	public List<CompanyCreditAndDiscount> getCompanyCreditAndDiscountList(Integer organizationId);
	
	public Boolean deleteCompanyCreditAndDiscountDetails(Integer companyCreditAndDiscountId);

	public Boolean updateCompanyObj(Integer roleTypeId,Integer stateId,Integer districtId,Integer cityId,Company companyObj);
	
	public void updateUserDetailsObj(UserDetails userDetailsObj);
	
	public JSONObject updateCompanyDocumentInfo(Integer documentTypeId, Integer companyId, Integer companyDocumentId) throws Exception;
	
	public JSONArray updateCompanyUploadDocumentImage(Integer companyDocumentId, List<CompanyDocumentImagePath> documentList) throws Exception;
	
	public Boolean deleteCompanyDocumentFileImage(Integer companyDocumentPathId);
}
