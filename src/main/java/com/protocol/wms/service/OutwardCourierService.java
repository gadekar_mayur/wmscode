/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.protocol.wms.model.OutWardAdvancedChequeInformation;
import com.protocol.wms.model.OutWardChequeNumberInformation;
import com.protocol.wms.model.OutWardCourierRegistration;

/**
 * @author admin
 *
 */
public interface OutwardCourierService {

	public Boolean saveOutwardCourierRegistration(String companyStockistOption,Integer orgId,Integer companyId,Integer depoId,Integer particularId,Integer stockistId,Integer companyBankId,OutWardCourierRegistration outWardCourierRegistration,OutWardAdvancedChequeInformation advancedChequeInfo,List<OutWardChequeNumberInformation> chequeNoInfoList);//,Integer stateId,Integer districtId,Integer cityId
	
	public JSONObject editOutwardCourierRegistration(Integer orgId,Integer companyId,Integer depoId,Integer stateId,Integer districtId,Integer cityId,Integer particularId,Integer stockistId,Integer companyBankId,OutWardCourierRegistration outWardCourierRegistration,OutWardAdvancedChequeInformation advancedChequeInfo,List<OutWardChequeNumberInformation> chequeNoInfoList) throws Exception;
	
	public List<OutWardCourierRegistration> outwardCourierRegistrationListing(Integer organizationId);
	
	public Boolean deleteOutwardCourierRegistration(Integer outwardCourierRegId);
	
	public OutWardCourierRegistration loadOutwardCourierRegistrationObjectUsingoutwardCourierRegId(Integer outwardCourierRegId);
	
	public List<OutWardChequeNumberInformation> loadChequeInformationUsigngetOutWardAdvancedChequeInformationId(Integer outWardAdvancedChequeInformationId);

	public Map<Object,Object> searchoutwardCourierRegistration(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from);


}
