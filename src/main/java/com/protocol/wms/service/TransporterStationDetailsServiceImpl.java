/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.TransporterStationDetailsDao;
import com.protocol.wms.model.TransporterStationDetails;

/**
 * @author Sudhakar
 *
 */
@Service("TransporterStationDetailsServiceImpl")
public class TransporterStationDetailsServiceImpl implements TransporterStationDetailsService {

	
	private TransporterStationDetailsDao transporterStationDetailsDao;

	@Autowired
	@Qualifier("TransporterStationDetailsDaoImpl")
	public void setTransporterStationDetailsDao(
			TransporterStationDetailsDao transporterStationDetailsDao) {
		this.transporterStationDetailsDao = transporterStationDetailsDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterStationDetailsService#saveTransporterStationDetails(com.protocol.wms.model.TransporterStationDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveTransporterStationDetails(
			TransporterStationDetails tsdObj) {
		return transporterStationDetailsDao.saveTransporterStationDetails(tsdObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterStationDetailsService#laodTransporterStationDetailsUsignTransporterStationDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public TransporterStationDetails laodTransporterStationDetailsUsignTransporterStationDetailsId(
			Integer transporterStationDetailsId) {
		// TODO Auto-generated method stub
		return transporterStationDetailsDao.laodTransporterStationDetailsUsignTransporterStationDetailsId(transporterStationDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterStationDetailsService#loadTransporterStationDetailsListUsignOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<TransporterStationDetails> loadTransporterStationDetailsListUsignOrganizationId(
			Integer organizationId) {
		return transporterStationDetailsDao.loadTransporterStationDetailsListUsignOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterStationDetailsService#searchStationPersonContactDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object>searchStationPersonContactDetails(
			String searchOption, String searchText,Integer from,Integer organizationId) {
		return transporterStationDetailsDao.searchStationPersonContactDetails(searchOption, searchText,from, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterStationDetailsService#insertNewTransporterStationDetailsAndDeleteOldRecordDetails(java.lang.Integer, com.protocol.wms.model.TransporterStationDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer insertNewTransporterStationDetailsAndDeleteOldRecordDetails(
			Integer stationPersonContactDetailsId,
			TransporterStationDetails transporterStationDetails) {
		// TODO Auto-generated method stub
		return transporterStationDetailsDao.insertNewTransporterStationDetailsAndDeleteOldRecordDetails(stationPersonContactDetailsId, transporterStationDetails);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterStationDetailsService#updateTransporterStationDetails(com.protocol.wms.model.TransporterStationDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateTransporterStationDetails(TransporterStationDetails transporterStationDetails) {
		// TODO Auto-generated method stub
		return transporterStationDetailsDao.updateTransporterStationDetails(transporterStationDetails);
	}
}
