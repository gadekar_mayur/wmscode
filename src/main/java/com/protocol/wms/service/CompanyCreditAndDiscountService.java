/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.CompanyCreditAndDiscount;

/**
 * @author admin
 *
 */
public interface CompanyCreditAndDiscountService {
	
	public Integer updateCompanyCreditAndDiscount(Integer orgId, Integer companyId, Integer creditAndDiscountId,CompanyCreditAndDiscount companyCreditAndDiscount);
	
	public CompanyCreditAndDiscount getCompanyCreditAndDiscountDetailsById(Integer creditAndDiscountId);

	public Map<String,Object>searchCreditAndDiscount(
			String searchOption, String searchText, Integer from,Integer organizationId);
	
}
