/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.EmployeeDetailsAddressProofPath;
import com.protocol.wms.model.EmployeeDetailsImagePath;

/**
 * @author Sudhakar
 *
 */
public interface EmployeeDetailsService {

	public Integer saveEmployeeDetails(EmployeeDetails employeeDetailsObj);
	
	public EmployeeDetails loadEmployeeObjectUsingEmployeeDetailsId(Integer empDetailsId);
	
	public JSONArray viewEmployeeDetailsUsingEmployeeDetailsId(Integer empDetailsId, Boolean editStatus) throws Exception;
	
	public void updateEmployeeDetails(EmployeeDetails empObject);
	
	public Boolean updateEmployeeDetails(Integer roleTypeId, Integer bloodroupNameId,EmployeeDetails employeeDetails);
	
	public List<EmployeeDetails> loadEmployeeDetailsListUsingOrganizationId(Integer organizationId);
	
	public Map<Object, Object> searchEmployeeDetails(String searchOption,String searchText,Integer organizationId,Integer from);
	
	public Boolean deleteEmployeeDetails(Integer empDetailsId);
	
	public JSONArray updateEmployeeImages(Integer employeeDetailsId, List<EmployeeDetailsImagePath> employeeImgList) throws Exception;
	
	public Boolean deleteEmployeeImage(Integer employeeImageId);
	
	public JSONArray updateEmployeeAddressProofImage(Integer employeeDetailsId, List<EmployeeDetailsAddressProofPath> employeeAddressProofPathList) throws Exception;
	
	public Boolean deleteEmployeeAddressProofImage(Integer employeeAddressProofImageId);
}
