package com.protocol.wms.service;

import java.util.List;

import org.json.JSONObject;

import com.protocol.wms.model.StockistBankDetails;

public interface StockistBankDetailsService {

	public JSONObject displayStockistBankDetailsForm();
	
	public  StockistBankDetails loadStockistBankDetailsUsignStockistBankDetailsId(Integer stockistBankDetailsId);
	
	public Boolean updateStockistBankDetails(StockistBankDetails stockistBankDetails);
	
	public JSONObject saveStockistBankDetails(Integer orgId,Integer stockistId,Integer companyId,StockistBankDetails bankDetatils);
	
	public JSONObject stockistBankDetailsList(Integer organizationId);
	
	public JSONObject searchStokistBankDetails(String searchOption,String searchText,Integer organizationId,Integer from);
	
	public List<StockistBankDetails> stockistBankDropdownList(Integer organizationId,Integer companyId,Integer stockistId);
	
	public Boolean deleteStockistBankDetails(Integer stockistBankId);
}
