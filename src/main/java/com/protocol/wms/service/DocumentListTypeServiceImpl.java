/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.DocumentListTypeDao;
import com.protocol.wms.model.DocumentListType;

/**
 * @author Sudhakar
 *
 */
@Service("DocumentListTypeServiceImpl")
public class DocumentListTypeServiceImpl implements DocumentListTypeService{

	private DocumentListTypeDao documentListTypeDao;

	@Autowired
	@Qualifier("DocumentListTypeDaoImpl")
	public void setDocumentListTypeDao(DocumentListTypeDao documentListTypeDao) {
		this.documentListTypeDao = documentListTypeDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.DocumentListTypeService#loadDocumentListType()
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<DocumentListType> loadDocumentListType() {
		return documentListTypeDao.loadDocumentListType();
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.DocumentListTypeService#loadDocumentListTypeUsingDocumentListTypeId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public DocumentListType loadDocumentListTypeUsingDocumentListTypeId(
			Integer documentListTypeId) {
		return documentListTypeDao.loadDocumentListTypeUsingDocumentListTypeId(documentListTypeId);
	}
	
	
}
