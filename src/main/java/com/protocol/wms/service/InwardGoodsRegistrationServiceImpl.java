/**
 * 
 */
package com.protocol.wms.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CompanyDao;
import com.protocol.wms.dao.CompanyDepoDao;
import com.protocol.wms.dao.InwardGoodsRegistrationClaimLetterDao;
import com.protocol.wms.dao.InwardGoodsRegistrationDao;
import com.protocol.wms.dao.InwardGoodsRegistrationInvoiceDetailsDao;
import com.protocol.wms.dao.OrganizationDao;
import com.protocol.wms.dao.TransporterDetailsDao;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyDepo;
import com.protocol.wms.model.InwardGoodsRegistration;
import com.protocol.wms.model.InwardGoodsRegistrationClaimLetter;
import com.protocol.wms.model.InwardGoodsRegistrationClaimLetterImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationClaimPicturesImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails;
import com.protocol.wms.model.InwardGoodsRegistrationInvoiceStnNoImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationLRImgPath;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.TransporterDetails;

/**
 * @author Sudhakar
 *
 */
@Service("InwardGoodsRegistrationServiceImpl")
public class InwardGoodsRegistrationServiceImpl implements InwardGoodsRegistrationService{

	private CompanyDepoDao companyDepoDao;
	
	private InwardGoodsRegistrationDao inwardGoodsRegistrationDao;
	
	private OrganizationDao organizationDao;
	
	private CompanyDao companyDao;
	
	private TransporterDetailsDao transporterDetailsDao;
	
	private InwardGoodsRegistrationInvoiceDetailsDao inwardGoodsRegistrationInvoiceDetails;
	
	private InwardGoodsRegistrationClaimLetterDao inwardGoodsRegistrationClaimLetterDao;
	
	
	@Autowired
	@Qualifier("CompanyDepoDaoImpl")
	public void setCompanyDepoDao(CompanyDepoDao companyDepoDao) {
		this.companyDepoDao = companyDepoDao;
	}

	@Autowired
	@Qualifier("InwardGoodsRegistrationDaoImpl")
	public void setInwardGoodsRegistrationDao(
			InwardGoodsRegistrationDao inwardGoodsRegistrationDao) {
		this.inwardGoodsRegistrationDao = inwardGoodsRegistrationDao;
	}

	@Autowired
	@Qualifier("OrganizationDaoImpl")
	public void setOrganizationDao(OrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	@Autowired
	@Qualifier("CompanyDaoImpl")
	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}
	
	@Autowired
	@Qualifier("TransporterDetailsDaoImpl")
	public void setTransporterDetailsDao(TransporterDetailsDao transporterDetailsDao) {
		this.transporterDetailsDao = transporterDetailsDao;
	}

	
	@Autowired
	@Qualifier("InwardGoodsRegistrationInvoiceDetailsDaoImpl")
	public void setInwardGoodsRegistrationInvoiceDetails(
			InwardGoodsRegistrationInvoiceDetailsDao inwardGoodsRegistrationInvoiceDetails) {
		this.inwardGoodsRegistrationInvoiceDetails = inwardGoodsRegistrationInvoiceDetails;
	}

	@Autowired
	@Qualifier("InwardGoodsRegistrationClaimLetterDaoImpl")
	public void setInwardGoodsRegistrationClaimLetterDao(
			InwardGoodsRegistrationClaimLetterDao inwardGoodsRegistrationClaimLetterDao) {
		this.inwardGoodsRegistrationClaimLetterDao = inwardGoodsRegistrationClaimLetterDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#loadCompanyDepoListUsignCompanyIdAndOrgId(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyDepo> loadCompanyDepoListUsignCompanyIdAndOrgId(
			Integer orgId, Integer companyId) {
		return companyDepoDao.loadCompanyDepoListUsignCompanyIdAndOrgId(orgId, companyId);
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#saveInwardGoodsRegistration(com.protocol.wms.model.InwardGoodsRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveInwardGoodsRegistration(
			InwardGoodsRegistration inwardGoodsRegistrationObj) {
		return inwardGoodsRegistrationDao.saveInwardGoodsRegistration(inwardGoodsRegistrationObj);
	}

	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#editInwardGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardGoodsRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject editInwardGoodsRegistration(Integer organizationId,
			Integer comapnyId, Integer sendingDepo, Integer transporterId,
			InwardGoodsRegistration inwardGoodsRegistrationObj) throws JSONException {
		JSONObject json = new JSONObject();
			if(inwardGoodsRegistrationDao.editInwardGoodsRegistration(organizationId, comapnyId, sendingDepo, transporterId, inwardGoodsRegistrationObj)){
				json.put("RESULT", true);
				json.put("MSG", "Inward Goods Registration Updated successfully");
				json.put("ORG_NAME", inwardGoodsRegistrationObj.getOrganization().getOrganizationName());
				json.put("COMPANY", inwardGoodsRegistrationObj.getCompany().getCompanyName());
				json.put("TRANS", inwardGoodsRegistrationObj.getTransporterDetails().getTransporterName());
				json.put("DEPO", inwardGoodsRegistrationObj.getCompanyDepo().getDepoName());
				json.put("LR_NO", inwardGoodsRegistrationObj.getLRNo());
				json.put("DRIVER_NAME", inwardGoodsRegistrationObj.getDriverName());
				String str = inwardGoodsRegistrationObj.getInwardGoodsRegistrationDate().toString();
				json.put("REG_DATE", str.substring(8,10)+"/"+str.substring(5,7)+"/"+ str.substring(0,4));
				//json.put("LR_IMAGE_PATH",inwardGoodsRegistrationObj.getLRImagePath());
			}else{
				json.put("MSG", "Inward Goods Registration updation failed...!");
			}
		return json;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#loadOrganizationObjectUsingOrganiztionId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Organization loadOrganizationObjectUsingOrganiztionId(
			Integer organizationId) {
		return organizationDao.loadOrganizationObjectUsingOrganiztionId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#loadCompanyObjectUsingCompanyId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Company loadCompanyObjectUsingCompanyId(Integer companyId) {
		return companyDao.loadCompanyObjectUsingCompanyId(companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#loadTransporterDetailsUsingTransporterDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public TransporterDetails loadTransporterDetailsUsingTransporterDetailsId(
			Integer TransporterDetailsId) {
		return transporterDetailsDao.loadTransporterDetailsUsingTransporterDetailsId(TransporterDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#loadCompanyDepoObjectUsignCompanyDepoId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyDepo loadCompanyDepoObjectUsignCompanyDepoId(
			Integer companyDepoId) {
		return companyDepoDao.loadCompanyDepoObjectUsignCompanyDepoId(companyDepoId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public InwardGoodsRegistration loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(
			Integer inwardGoodsRegistrationId) {
		return inwardGoodsRegistrationDao.loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(inwardGoodsRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#inwardGoodsRegistration(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray inwardGoodsRegistrationView(Integer inwardGoodsRegistrationId) throws JSONException, ParseException {

		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		
		InwardGoodsRegistration obj=inwardGoodsRegistrationDao.loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(inwardGoodsRegistrationId);
		if(obj!=null)
		{
			json=new JSONObject();
			json.put("INWARD_REG_ID", obj.getInwardGoodsRegistrationId());
			json.put("ORG_ID", obj.getOrganization().getOrganizationId());
			json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
			json.put("DEPO_ID", obj.getCompanyDepo().getCompanyDepoId());
			json.put("DEPO", obj.getCompanyDepo().getDepoName());
			json.put("COMP_ID", obj.getCompany().getCompanyId());
			json.put("COMP", obj.getCompany().getCompanyName());
			json.put("TRAN_ID", obj.getTransporterDetails().getTransporterDetailsId());
			json.put("TRAN", obj.getTransporterDetails().getTransporterName());
			String [] reg_date_arr = obj.getInwardGoodsRegistrationDate().toString().split("-");
			json.put("REG_DATE",reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0] );
			json.put("NO_OF_CASES", obj.getNoOfCases());
			json.put("LR_NO", obj.getLRNo());
			//String s = obj.getLRImagePath();int i = s.lastIndexOf('/');s=s.substring(i+1);
			json_ImageArray=new JSONArray();
			for(InwardGoodsRegistrationLRImgPath tempObj : obj.getInwardGoodsRegistrationLRImgPathList()){
				tempJson = new JSONObject();
				tempJson.put("IMAGE_PATH", tempObj.getImagePath());
				tempJson.put("IMAGE_ID", tempObj.getInwardGoodsRegistrationLRImgPathId());
				json_ImageArray.put(tempJson);
			}
			json.put("LR_IMAGE_PATH", json_ImageArray);
			json.put("DRIVER_NAME", obj.getDriverName());
			if(obj.getUnloadingCharges()==null)
				json.put("UNLOADING_CHARGES","");
			else
				json.put("UNLOADING_CHARGES", obj.getUnloadingCharges());
			
			json.put("UNLOADER_NAME", obj.getUnloaderName());
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
	        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
	        java.util.Date _24HourDt = _24HourSDF.parse(obj.getTime().toString());
            //System.out.println(_24HourDt);
            //System.out.println(_12HourSDF.format(_24HourDt));
	        json.put("TIME", _12HourSDF.format(_24HourDt));
			//json.put("TIME",obj.getTime());
			json.put("RECEIVED_BY", obj.getReceivedBy());
			if(obj.getTranportationCharges()==null)
				json.put("TRAN_CHARGES","");
			else
				json.put("TRAN_CHARGES", obj.getTranportationCharges());
			if(obj.getHamaliCharges()==null)
				json.put("HAMALI_CHAR","");
			else
				json.put("HAMALI_CHAR", obj.getHamaliCharges());
			json.put("REMARK", obj.getRemark());
			json.put("CLAIM_LETTER_STATUS", obj.getClaimLetterStatus());
			if(obj.getClaimLetterStatus()==1){
				json.put("CLAIM_LETTER_ID",obj.getInwardGoodsRegistrationClaimLetter().getInwardGoodsRegistrationClaimLetterId());
				json.put("CLAIM_DESC",obj.getInwardGoodsRegistrationClaimLetter().getDescription());
				json_ImageArray=new JSONArray();
				for(InwardGoodsRegistrationClaimLetterImagePath tempObj : obj.getInwardGoodsRegistrationClaimLetter().getInwardGoodsRegistrationClaimLetterImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getInwardGoodsRegistrationClaimLetterImagePathId());
					json_ImageArray.put(tempJson);
				}
				json.put("CLAIM_LETTER", json_ImageArray);
				json_ImageArray=new JSONArray();
				for(InwardGoodsRegistrationClaimPicturesImagePath tempObj : obj.getInwardGoodsRegistrationClaimLetter().getInwardGoodsRegistrationClaimPicturesImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getInwardGoodsRegistrationClaimPicturesImagePathId());
					json_ImageArray.put(tempJson);
				}
				json.put("CLAIM_UPLOAD_PICTURE", json_ImageArray);
			}
			// load invoice details list
			List<InwardGoodsRegistrationInvoiceDetails> inwardGoodsRegistrationInvoiceDetailsList=inwardGoodsRegistrationInvoiceDetails.loadInwardGoodsRegistrationInvoiceDetailsListUsignInwardGoodsRegistrationId(inwardGoodsRegistrationId);
			if(inwardGoodsRegistrationInvoiceDetailsList.size()>0)
			{
				JSONArray json_subIvoiceArray=new JSONArray();
				JSONObject jsonSubInvoice=null;
				for(InwardGoodsRegistrationInvoiceDetails invoiceObj :  inwardGoodsRegistrationInvoiceDetailsList)
				{
					jsonSubInvoice=new JSONObject();
					jsonSubInvoice.put("INVOICE_ID", invoiceObj.getInwardGoodsRegistrationInvoiceDetailsId());
					jsonSubInvoice.put("STN_NO", invoiceObj.getStnNO());
					//String STN_Image = obj.getLRImagePath();int y = STN_Image.lastIndexOf('/');STN_Image=STN_Image.substring(y+1);
					json_ImageArray=new JSONArray();
					for(InwardGoodsRegistrationInvoiceStnNoImagePath tempObj : invoiceObj.getInwardGoodsRegistrationInvoiceStnNoImagePathList()){
						tempJson = new JSONObject();
						tempJson.put("IMAGE_PATH", tempObj.getImagePath());
						tempJson.put("IMAGE_ID", tempObj.getInwardGoodsRegistrationInvoiceStnNoImagePathId());
						json_ImageArray.put(tempJson);
					}
					jsonSubInvoice.put("STN_Image", json_ImageArray);
					String [] stn_date_arr = invoiceObj.getStnDate().toString().split("-");
					jsonSubInvoice.put("STN_Date",stn_date_arr[1]+"/"+stn_date_arr[2]+"/"+stn_date_arr[0]);
					//jsonSubInvoice.put("STN_Date", invoiceObj.getStnDate());
					jsonSubInvoice.put("Gross_Amount", invoiceObj.getGrossAmount());
					jsonSubInvoice.put("Vat_Amount", invoiceObj.getVatAmount());
					jsonSubInvoice.put("Discount_Amount", invoiceObj.getDiscountAmount());
					jsonSubInvoice.put("Net_Amount", invoiceObj.getNetAmount());
					json_subIvoiceArray.put(jsonSubInvoice);
				}
				json.put("INVOICE_DETAILS", json_subIvoiceArray);
			}
			json_data_array.put(json);
		}
	
		return json_data_array;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#saveInwardGoodsRegistrationInvoiceDetails(com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveInwardGoodsRegistrationInvoiceDetails(
			InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetailsObj) {
		return inwardGoodsRegistrationInvoiceDetails.saveInwardGoodsRegistrationInvoiceDetails(inwardGoodsRegistrationInvoiceDetailsObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#loadInwardGoodsRegistrationListUsignOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InwardGoodsRegistration> loadInwardGoodsRegistrationListUsignOrganizationId(
			Integer orgId) {
		return inwardGoodsRegistrationDao.loadInwardGoodsRegistrationListUsignOrganizationId(orgId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#searchInwardGoodsRegDetails(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object, Object> searchInwardGoodsRegDetails(
			String selectedValue,String searchText,String fromSpecialData,String toSpecialData, Integer organizationId,Integer from) {
		return inwardGoodsRegistrationDao.searchInwardGoodsRegDetails(selectedValue, searchText, fromSpecialData, toSpecialData, organizationId,from);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#loadInwardGoodsRegistrationInvoiceDetailsListUsignInwardGoodsRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InwardGoodsRegistrationInvoiceDetails> loadInwardGoodsRegistrationInvoiceDetailsListUsignInwardGoodsRegistrationId(
			Integer inwardGoodsRegistrationId) {
		return inwardGoodsRegistrationInvoiceDetails.loadInwardGoodsRegistrationInvoiceDetailsListUsignInwardGoodsRegistrationId(inwardGoodsRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#saveInwardGoodsRegistrationClaimLetter(com.protocol.wms.model.InwardGoodsRegistrationClaimLetter)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveInwardGoodsRegistrationClaimLetter(
			InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetterObj) {
		// TODO Auto-generated method stub
		return inwardGoodsRegistrationClaimLetterDao.saveInwardGoodsRegistrationClaimLetter(inwardGoodsRegistrationClaimLetterObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#updateInwardGoodsRegistrationObject(com.protocol.wms.model.InwardGoodsRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateInwardGoodsRegistrationObject(
			InwardGoodsRegistration inwardGoodsRegistrationObj) {
		inwardGoodsRegistrationDao.updateInwardGoodsRegistrationObject(inwardGoodsRegistrationObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#deleteInwardGoodsRegistrationDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardGoodsRegistrationDetails(Integer inwardGoodsRegistrationId) {
		// TODO Auto-generated method stub
		return inwardGoodsRegistrationDao.deleteInwardGoodsRegistrationDetails(inwardGoodsRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#deleteInwardGoodsRegistrationLrImage(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardGoodsRegistrationLrImage(Integer lrImageId) {
		// TODO Auto-generated method stub
		return inwardGoodsRegistrationDao.deleteInwardGoodsRegistrationLrImage(lrImageId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#deleteInwardGoodsRegistrationClaimLetterImage(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardGoodsRegistrationClaimLetterImage(Integer claimLetterImageId) {
		return inwardGoodsRegistrationClaimLetterDao.deleteInwardGoodsRegistrationClaimLetterImage(claimLetterImageId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#deleteInwardGoodsRegistrationClaimPictureImage(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardGoodsRegistrationClaimPictureImage(
			Integer claimPictureImageId) {
		return inwardGoodsRegistrationClaimLetterDao.deleteInwardGoodsRegistrationClaimPictureImage(claimPictureImageId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#deleteInwardGoodsRegInvoiceStnImage(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardGoodsRegInvoiceStnImage(Integer stnImageId) {
		// TODO Auto-generated method stub
		return inwardGoodsRegistrationInvoiceDetails.deleteInwardGoodsRegInvoiceStnImage(stnImageId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#getInwardGoodsRegistrationInvoiceDetailsObjById(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public InwardGoodsRegistrationInvoiceDetails getInwardGoodsRegistrationInvoiceDetailsObjById(
			Integer inwardGoodsRegistrationInvoiceDetailsId) {
		
		return inwardGoodsRegistrationInvoiceDetails.getInwardGoodsRegistrationInvoiceDetailsObjById(inwardGoodsRegistrationInvoiceDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#editInwardGoodsRegistrationInvoiceDetails(com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean editInwardGoodsRegistrationInvoiceDetails(
			InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetail) {
		// TODO Auto-generated method stub
		return inwardGoodsRegistrationInvoiceDetails.editInwardGoodsRegistrationInvoiceDetails(inwardGoodsRegistrationInvoiceDetail);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#getInwardGoodsRegistrationClaimLetterObj(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public InwardGoodsRegistrationClaimLetter getInwardGoodsRegistrationClaimLetterObj(
			Integer inwardGoodsRegistrationClaimLetterId) {
		// TODO Auto-generated method stub
		return inwardGoodsRegistrationClaimLetterDao.getInwardGoodsRegistrationClaimLetterObj(inwardGoodsRegistrationClaimLetterId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#editInwardGoodsRegistrationClaimLetter(com.protocol.wms.model.InwardGoodsRegistrationClaimLetter)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean editInwardGoodsRegistrationClaimLetter(InwardGoodsRegistrationClaimLetter claimObj) {
		// TODO Auto-generated method stub
		return inwardGoodsRegistrationClaimLetterDao.editInwardGoodsRegistrationClaimLetter(claimObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#updateInwardGoodsRegLrImages(java.lang.Integer, java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateInwardGoodsRegLrImages(
			Integer inwardGoodsRegistrationId,
			List<InwardGoodsRegistrationLRImgPath> lrImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		List<InwardGoodsRegistrationLRImgPath> tempLrImgList = null;
		tempLrImgList = inwardGoodsRegistrationDao.updateInwardGoodsRegLrImages(inwardGoodsRegistrationId, lrImgList);
		for(InwardGoodsRegistrationLRImgPath tempObj : tempLrImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getInwardGoodsRegistrationLRImgPathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#updateInwardGoodsRegInvoiceStnImageupdateInwardGoodsRegInvoiceStnImage(java.lang.Integer, java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateInwardGoodsRegInvoiceStnImage(
			Integer inwardGoodsRegistrationInvoiceId,
			List<InwardGoodsRegistrationInvoiceStnNoImagePath> stnImgList) throws JSONException {

		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		List<InwardGoodsRegistrationInvoiceStnNoImagePath> tempStnImgList = null;
		tempStnImgList =  inwardGoodsRegistrationInvoiceDetails.updateInwardGoodsRegInvoiceStnImage(inwardGoodsRegistrationInvoiceId, stnImgList);
		for(InwardGoodsRegistrationInvoiceStnNoImagePath tempObj : tempStnImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getInwardGoodsRegistrationInvoiceStnNoImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#updateInwardGoodsRegClaimLetterImage(java.lang.Integer, java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateInwardGoodsRegClaimLetterImage(
			Integer inwardGoodsRegistrationClaimLetterId,
			List<InwardGoodsRegistrationClaimLetterImagePath> claimLetterImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		//List<InwardGoodsRegistrationInvoiceStnNoImagePath> tempStnImgList = null;
		claimLetterImgList =  inwardGoodsRegistrationClaimLetterDao.updateInwardGoodsRegClaimLetterImage(inwardGoodsRegistrationClaimLetterId, claimLetterImgList);
		for(InwardGoodsRegistrationClaimLetterImagePath tempObj : claimLetterImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getInwardGoodsRegistrationClaimLetterImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardGoodsRegistrationService#updateInwardGoodsRegClaimPicturesImage(java.lang.Integer, java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateInwardGoodsRegClaimPicturesImage(
			Integer inwardGoodsRegistrationClaimLetterId,
			List<InwardGoodsRegistrationClaimPicturesImagePath> claimPicturesImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		//List<InwardGoodsRegistrationInvoiceStnNoImagePath> tempStnImgList = null;
		claimPicturesImgList =  inwardGoodsRegistrationClaimLetterDao.updateInwardGoodsRegClaimPicturesImage(inwardGoodsRegistrationClaimLetterId, claimPicturesImgList);
		for(InwardGoodsRegistrationClaimPicturesImagePath tempObj : claimPicturesImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getInwardGoodsRegistrationClaimPicturesImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}
	
}
