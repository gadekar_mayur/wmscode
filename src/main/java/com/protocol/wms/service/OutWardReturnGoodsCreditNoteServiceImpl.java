/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.OutWardReturnGoodsCreditNoteDao;
import com.protocol.wms.model.OutWardReturnGoodsCreditNote;
import com.protocol.wms.model.OutWardReturnGoodsCreditNoteCreditNoteImagePath;

/**
 * @author admin
 *
 */
@Service("OutWardReturnGoodsCreditNoteServiceImpl")
public class OutWardReturnGoodsCreditNoteServiceImpl implements OutWardReturnGoodsCreditNoteService {

	@Autowired
	@Qualifier("OutWardReturnGoodsCreditNoteDaoImpl")
	private OutWardReturnGoodsCreditNoteDao outWardReturnGoodsCreditNoteDao;
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardReturnGoodsCreditNoteService#saveOutwardReturnGoodsRegCreditNote(java.lang.Integer, org.springframework.web.multipart.MultipartFile, com.protocol.wms.model.OutWardReturnGoodsCreditNote)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveOutwardReturnGoodsRegCreditNote(Integer organiztionId, OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote) {
		return outWardReturnGoodsCreditNoteDao.saveOutwardReturnGoodsRegCreditNote(organiztionId,outWardReturnGoodsCreditNote);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardReturnGoodsCreditNoteService#getOutwardReturnGoodsRegCreditNoteList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray getOutwardReturnGoodsRegCreditNoteList(Integer organizationId) throws JSONException {
		JSONObject tempJsonObj = null;
		JSONArray creditNoteArray = null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		List<OutWardReturnGoodsCreditNote> creditNoteList=null;
		{
			creditNoteList = outWardReturnGoodsCreditNoteDao.getOutwardReturnGoodsRegCreditNoteList(organizationId);
			creditNoteArray = new JSONArray();
			for(OutWardReturnGoodsCreditNote tempObj:creditNoteList){
				tempJsonObj = new JSONObject();
				tempJsonObj.put("CREDIT_NOTE_ID",tempObj.getOutWardReturnGoodsCreditNoteId());
				tempJsonObj.put("ORG_ID",tempObj.getOrganization().getOrganizationId());
				tempJsonObj.put("ORG",tempObj.getOrganization().getOrganizationName());
				tempJsonObj.put("CREDIT_NOTE_NO",tempObj.getCreditNoteNo());
				tempJsonObj.put("CREDIT_NOTE_AMOUNT",tempObj.getCreditNoteAmount());
				tempJsonObj.put("REF_CLAIM_NO",tempObj.getRefNoOrClaimNo());
				tempJsonObj.put("REMARK",tempObj.getRemark());
				json_ImageArray=new JSONArray();
				for(OutWardReturnGoodsCreditNoteCreditNoteImagePath tempObj1 : tempObj.getOutWardReturnGoodsCreditNoteCreditNoteImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj1.getImagePath());
					tempJson.put("IMAGE_ID", tempObj1.getOutWardReturnGoodsCreditNoteCreditNoteImagePathId());
					json_ImageArray.put(tempJson);
				}
				tempJsonObj.put("ATTACH",json_ImageArray);
				creditNoteArray.put(tempJsonObj);
			}
		}
		return creditNoteArray;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardReturnGoodsCreditNoteService#deleteOutwardReturnGoodsRegCreditNote(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardReturnGoodsRegCreditNote(Integer creditNoteId) {
		// TODO Auto-generated method stub
		return outWardReturnGoodsCreditNoteDao.deleteOutwardReturnGoodsRegCreditNote(creditNoteId);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardReturnGoodsCreditNoteService#loadOutWardReturnGoodsCreditNoteObjById(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardReturnGoodsCreditNote loadOutWardReturnGoodsCreditNoteObjById(Integer outWardReturnGoodsCreditNoteId) {
		return outWardReturnGoodsCreditNoteDao.loadOutWardReturnGoodsCreditNoteObjById(outWardReturnGoodsCreditNoteId);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutWardReturnGoodsCreditNoteService#editOutwardReturnGoodsRegCreditNote(java.lang.Integer, com.protocol.wms.model.OutWardReturnGoodsCreditNote)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject editOutwardReturnGoodsRegCreditNote(Integer organiztionId,OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote) throws JSONException {
		JSONObject result = null;
		result = new JSONObject();
		result.put("MSG", "Credit Note Registraion Details Updation Failed....!");
		if(outWardReturnGoodsCreditNoteDao.editOutwardReturnGoodsRegCreditNote(organiztionId, outWardReturnGoodsCreditNote)){
			result.put("RESULT", true);
			//result.put("CREDIT_NOTE_ID",outWardReturnGoodsCreditNote.getOutWardReturnGoodsCreditNoteId());
			result.put("ORG_ID",outWardReturnGoodsCreditNote.getOrganization().getOrganizationId());
			result.put("ORG",outWardReturnGoodsCreditNote.getOrganization().getOrganizationName());
			result.put("CREDIT_NOTE_NO",outWardReturnGoodsCreditNote.getCreditNoteNo());
			result.put("CREDIT_NOTE_AMOUNT",outWardReturnGoodsCreditNote.getCreditNoteAmount());
			result.put("REF_CLAIM_NO",outWardReturnGoodsCreditNote.getRefNoOrClaimNo());
			result.put("REMARK",outWardReturnGoodsCreditNote.getRemark());
			//result.put("ATTACH",outWardReturnGoodsCreditNote.getCreditNoteImagePath());
			result.put("MSG", "Credit Note Registraion Details Updated Successfully....!");
		}
		return result;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray searchOutwardReturnGoodsRegCreditNote(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from) throws JSONException {
		JSONObject tempJsonObj = null;
		JSONArray creditNoteArray = null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		java.util.Map<Object, Object>map = null;
			map = outWardReturnGoodsCreditNoteDao.searchOutwardReturnGoodsRegCreditNote(selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
		
			List<OutWardReturnGoodsCreditNote> creditNoteList = (List<OutWardReturnGoodsCreditNote>) map.get("paidInvoice");
			Integer totalPage = (Integer) map.get("totalPages");
			creditNoteArray = new JSONArray();
			for(OutWardReturnGoodsCreditNote tempObj:creditNoteList){
				tempJsonObj = new JSONObject();
				tempJsonObj.put("CREDIT_NOTE_ID",tempObj.getOutWardReturnGoodsCreditNoteId());
				tempJsonObj.put("ORG_ID",tempObj.getOrganization().getOrganizationId());
				tempJsonObj.put("ORG",tempObj.getOrganization().getOrganizationName());
				tempJsonObj.put("CREDIT_NOTE_NO",tempObj.getCreditNoteNo());
				tempJsonObj.put("CREDIT_NOTE_AMOUNT",tempObj.getCreditNoteAmount());
				tempJsonObj.put("REF_CLAIM_NO",tempObj.getRefNoOrClaimNo());//
				tempJsonObj.put("REMARK",tempObj.getRemark());
				json_ImageArray=new JSONArray();
				for(OutWardReturnGoodsCreditNoteCreditNoteImagePath tempObj1 : tempObj.getOutWardReturnGoodsCreditNoteCreditNoteImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj1.getImagePath());
					tempJson.put("IMAGE_ID", tempObj1.getOutWardReturnGoodsCreditNoteCreditNoteImagePathId());
					json_ImageArray.put(tempJson);
				}
				tempJsonObj.put("ATTACH",json_ImageArray);
				creditNoteArray.put(tempJsonObj);
			}
			tempJsonObj=new JSONObject();
			tempJsonObj.put("paginationCount", totalPage);
			creditNoteArray.put(tempJsonObj);
		return creditNoteArray;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateOutwardGoodsRegCreditNoteAttachImage(Integer creditNoteId,
			List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> creditNoteAttachList) throws JSONException
			 {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		creditNoteAttachList = outWardReturnGoodsCreditNoteDao.updateOutwardGoodsRegCreditNoteAttachImage(creditNoteId, creditNoteAttachList);
		for(OutWardReturnGoodsCreditNoteCreditNoteImagePath tempObj : creditNoteAttachList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getOutWardReturnGoodsCreditNoteCreditNoteImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardGoodsRegCreditNoteAttachImage(
			Integer creditNoteAttachImageId) {
		// TODO Auto-generated method stub
		return outWardReturnGoodsCreditNoteDao.deleteOutwardGoodsRegCreditNoteAttachImage(creditNoteAttachImageId);
	}
}
