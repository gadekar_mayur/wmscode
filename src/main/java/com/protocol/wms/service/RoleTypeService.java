/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import com.protocol.wms.model.RoleType;

/**
 * @author Sudhakar
 *
 */
public interface RoleTypeService {
	
	public List<RoleType> loadRoleTypeList();
	
	public RoleType loadRoleTypeUsingRoleTypeId(Integer roleTypeId);

}
