/**
 * 
 */
package com.protocol.wms.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyBank;
import com.protocol.wms.model.InvoicePaymentInformation;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.RemainingChequeAmount;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistBankDetails;
import com.protocol.wms.model.StockistChequeDeposit;
import com.protocol.wms.model.StockistChequeReturn;

/**
 * @author Sudhakar
 *
 */
public interface BankService {
	
	public List<CompanyBank> companyBankDropdown(Integer companyId,Integer organizationId);
	
	public List<StockistBankDetails> stockistBankDropdownList(Integer organizationId, Integer companyId, Integer stockistId);

	public Company loadCompanyObjectUsingCompanyId(Integer companyId);
	
	public CompanyBank loadCompanyBankUsignCompanyBankId(Integer companyBankId);
	
	public Stockist loadStockistObjUsigStokistId(Integer stokistId);
	
	public StockistBankDetails loadStockistBankDetailsUsignStockistBankDetailsId(Integer stockistBankDetailsId);
	
	public Organization loadOrganizationObjectUsingOrganiztionId(Integer organizationId);
	
	public boolean saveAdvancedChequeInformationObj(AdvancedChequeInformation AdvancedChequeInformationObj,String[] checkNo);
	
	public List<AdvancedChequeInformation> loadAdvancedChequeInformationListUsignOrganizationId(Integer organizationId);
	
	public JSONArray loadChequeInformationUsignOrgIdAndCompanyIdAndCompanyBankId(Integer orgId,Integer companyId,Integer companyBankId);
	
	public JSONArray loadChequeInformationUsignOrgIdAndCompanyIdAndStockistAndStockistBankId(Integer orgId,Integer companyId,Integer stockistId,Integer stockistBankId);
	
	public Integer saveStockistChequeDepositObj(StockistChequeDeposit StockistChequeDepositObj);
	
	public boolean updateChequeNumberInfoObj(ChequeNumberInfo chequeNumberInfoObj);
	
	public List<StockistChequeDeposit> loadStockistChequeDepositListUsignOrgnizationId(Integer organizationId);
	
	public StockistChequeDeposit loadStockistChequeDepositObjUsignStockistChequeDepositId(Integer StockistChequeDepositId);
	
	public Integer saveStockistChequeReturnObj(StockistChequeReturn stockistChequeReturnObj);
	
	public void updateStockistChequeDepositObj(StockistChequeDeposit stockistChequeDepositObj);
	
	public List<StockistChequeReturn> loadStockistChequeReturnListUsignOrgnizationId(Integer organizationId);
	
	public boolean reDepositReturnCheque(String[] stockistReturnChequeIdArray,String[] stockistDepositChequeIdArray);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListEqualToCurrentDate(Date currentDate,Integer organizationId);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListLessThanToCurrentDate(Date currentDate,Integer organizationId);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(Date currentDate,Integer organizationId);
	
	public List<Integer> loadOutWardOrderEnteryRegistrationIdUsignOrgIdAndCompIdAndStockistId(Integer orgid,Integer companyId,Integer stockistId);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListUsignoutWardOrderEnteryRegistrationId(Integer outWardOrderEnteryRegistrationId);
	
	public OutWardOrderInvoiceEnteryRegistration loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(Integer OutWardOrderInvoiceEnteryRegistrationId);
	
	public ChequeNumberInfo loadChequeNumberInfoObjUsignChequeNumberInfoId(Integer ChequeNumberInfoId);
	
	public Integer saveInvoicePaymentInformationObj(InvoicePaymentInformation InvoicePaymentInformationObj);
	
	public void updateOutWardOrderInvoiceEnteryRegistrationObj(OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj);
	
	public Integer saveRemainingChequeAmountObj(RemainingChequeAmount RemainingChequeAmountObj);
	
	public RemainingChequeAmount loadRemainingChequeAmountUsignOrgIdAndCompnayIdAndStockistId(Integer organizationId,Integer companyId,Integer stockistId);
	
	public void deleteRemainingChequeAmountObj(RemainingChequeAmount remainingChequeAmountObj);
	
	public RemainingChequeAmount loadRemainingChequeAmountUsignRemainingChequeAmountId(Integer RemainingChequeAmountId);
	
	public List<InvoicePaymentInformation> loadInvoicePaymentInformationListUsignChequeNumberInfoId(Integer ChequeNumberInfoId);
	
	public void deleteInvoicePaymentInformationObj(InvoicePaymentInformation InvoicePaymentInformationObj);
	
	public InvoicePaymentInformation loadInvoicePaymentInformationUsigInvoicePaymentInformationId(Integer invoicePaymentInformationId);
	
	public RemainingChequeAmount loadRemainingChequeAmountObjUsignChequeNumberInfoId(Integer chequeNumberInfoId);
	
	public StockistChequeReturn loadStockistChequeReturnObjUsignStockistChequeReturnId(Integer stockistChequeReturnId);

	public void deleteStockistChequeReturnObj(StockistChequeReturn stockistChequeReturnObj);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListWithStatusOne(Integer orgnizationId);

	public AdvancedChequeInformation loadAdvancedChequeInformationObjUsignAdvanceChequeId(Integer advanceChequeid);

	public List<ChequeNumberInfo> loadChequeNumberInfoObjUsignAdvanceChequeId(Integer advanceChequeid);
	
	public boolean updateAdvancedChequeInformationObj(AdvancedChequeInformation advancedChequeInformationObj);
	
	public Integer returnNoOfChequeUsingAdvancedChequeInformationId(Integer AdvancedChequeInformationId);
	
	public boolean deleteChequeNumberInfoObj(ChequeNumberInfo chequeNumberInfoObj);
	
	public boolean deleteAdvancedChequeInformationObj(AdvancedChequeInformation advancedChequeInformationObj);

	public Map<Object, Object> searchAdvancedChequeInformation(String searchOption, String searchText, Integer organizationId, Integer from);

	public Map<Object, Object> searchStockistDepositedChequeEntries(String searchOption, String searchText, String fromSpecialData, String toSpecialData, Integer organizationId, Integer from);

	public Map<Object, Object> searchOutstandingTodaysDue(
			java.sql.Date ourJavaDateObject, String selectedValue,
			String searchText, String fromSpecialData, String toSpecialData,
			Integer organizationId, Integer from);

	public Map<Object, Object> searchOutstandingLapsed(
			java.sql.Date ourJavaDateObject, String selectedValue,
			String searchText, String fromSpecialData, String toSpecialData,
			Integer organizationId, Integer from);

	public Map<Object,Object> searchOutstandingUpcoming(
			java.sql.Date ourJavaDateObject, String selectedValue,
			String searchText, String fromSpecialData, String toSpecialData,
			Integer organizationId, Integer from);

	public Map<Object, Object> searchOutstandingPaidInvoice(
			java.sql.Date ourJavaDateObject, String selectedValue,
			String searchText, String fromSpecialData, String toSpecialData,
			Integer organizationId, Integer from);
}
