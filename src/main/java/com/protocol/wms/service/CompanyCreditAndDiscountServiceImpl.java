/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CompanyCreditAndDiscountDao;
import com.protocol.wms.model.CompanyCreditAndDiscount;

/**
 * @author admin
 *
 */
@Service("CompanyCreditAndDiscountServiceImpl")
public class CompanyCreditAndDiscountServiceImpl implements
		CompanyCreditAndDiscountService {

	@Autowired
	@Qualifier("CompanyCreditAndDiscountDaoImpl")
	private CompanyCreditAndDiscountDao companyCreditAndDiscountDao;
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyCreditAndDiscountService#updateCompanyCreditAndDiscount(java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyCreditAndDiscount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer updateCompanyCreditAndDiscount(Integer orgId,
			Integer companyId, Integer creditAndDiscountId,
			CompanyCreditAndDiscount companyCreditAndDiscount) {
		return companyCreditAndDiscountDao.updateCompanyCreditAndDiscount(orgId, companyId, creditAndDiscountId, companyCreditAndDiscount);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyCreditAndDiscountService#getCompanyCreditAndDiscountDetailsById(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyCreditAndDiscount getCompanyCreditAndDiscountDetailsById(Integer creditAndDiscountId) {
		return companyCreditAndDiscountDao.getCompanyCreditAndDiscountDetailsById(creditAndDiscountId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object>searchCreditAndDiscount(
			String searchOption, String searchText,Integer from, Integer organizationId) {
		// TODO Auto-generated method stub
		return companyCreditAndDiscountDao.searchCreditAndDiscount(searchOption,searchText,from,organizationId);
	}
}
