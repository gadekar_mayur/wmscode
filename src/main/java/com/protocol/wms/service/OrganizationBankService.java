/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationBank;

/**
 * @author Sudhakar
 *
 */
public interface OrganizationBankService {
	
	public Integer saveOrganizationBank(OrganizationBank organizationBank);
	
	public List<OrganizationBank> loadOrganizationBanksListUsingOrganizationId(Integer organizationId);
	
	public Map<String,Object> searchOrganiztionBankDetails(String searchOption,String searchText,Integer from,Integer organizationId);
	
	public Boolean deleteOrganizationBank(Integer organizationBankId);
	
	public OrganizationBank loadOrganizationBankObjectUsingOrganizationBankId(Integer organizationBankId);

	public OrganizationBank loadOrganizationankObjUsingOrganizationBankId(Integer organizationBankId);
	
	public Organization loadOrganizationObjectUsingOrganiztionId(Integer organizationId);
	
	public Boolean updateOrganizationBankObj(OrganizationBank organizationBankObj);
	

}
