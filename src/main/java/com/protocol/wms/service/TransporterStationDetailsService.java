/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.TransporterStationDetails;

/**
 * @author Sudhakar
 *
 */
public interface TransporterStationDetailsService {
	
	public Integer saveTransporterStationDetails(TransporterStationDetails tsdObj);
	
	public TransporterStationDetails laodTransporterStationDetailsUsignTransporterStationDetailsId(Integer transporterStationDetailsId);
	
	public List<TransporterStationDetails> loadTransporterStationDetailsListUsignOrganizationId(Integer organizationId);
	
	public Map<String,Object> searchStationPersonContactDetails(String searchOption,String searchText,Integer from,Integer organizationId);

	public Integer insertNewTransporterStationDetailsAndDeleteOldRecordDetails(Integer stationPersonContactDetailsId, TransporterStationDetails transporterStationDetails);
	
	public Boolean updateTransporterStationDetails(TransporterStationDetails transporterStationDetails);
}
