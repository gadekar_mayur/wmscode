/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CompanyExecutiveDao;
import com.protocol.wms.model.CompanyExecutive;

/**
 * @author Admin (Darshan)
 *
 */
@Service("CompanyExecutiveServiceImpl")
public class CompanyExecutiveServiceImpl implements CompanyExecutiveService {

	@Autowired
	@Qualifier("CompanyExecutiveDaoImpl")
	private CompanyExecutiveDao companyExecutiveDao;
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyExecutiveService#saveExecutivesToCompany(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyExecutive)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveExecutivesToCompany(Integer orgId, Integer companyId,
			CompanyExecutive companyExecutive) {
		// TODO Auto-generated method stub
		return companyExecutiveDao.saveExecutivesToCompany(orgId, companyId, companyExecutive);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyExecutiveService#getCompanyExecutiveObjById(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyExecutive getCompanyExecutiveObjById(
			Integer companyExecutiveId) {
		// TODO Auto-generated method stub
		return companyExecutiveDao.getCompanyExecutiveObjById(companyExecutiveId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyExecutiveService#updateCompanyExecutivesDetails(com.protocol.wms.model.CompanyExecutive)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateCompanyExecutivesDetails(
			CompanyExecutive companyExecutive) {
		// TODO Auto-generated method stub
		return companyExecutiveDao.updateCompanyExecutivesDetails(companyExecutive);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyExecutiveService#deleteCompanyExecutive(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCompanyExecutive(Integer companyExeId) {
		// TODO Auto-generated method stub
		return companyExecutiveDao.deleteCompanyExecutive(companyExeId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyExecutiveService#searchCompanyExecutiveDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchCompanyExecutiveDetails(
			String searchOption, String searchText,Integer from, Integer organizationId) {
		return companyExecutiveDao.searchCompanyExecutiveDetails(searchOption, searchText,from, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyExecutiveService#getCompanyExecutivesListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyExecutive> getCompanyExecutivesListing(
			Integer organizationId) {
		// TODO Auto-generated method stub
		return companyExecutiveDao.getCompanyExecutivesListing(organizationId);
	}

}
