/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import com.protocol.wms.model.DocumentListType;

/**
 * @author Sudhakar
 *
 */
public interface DocumentListTypeService {
	
	public List<DocumentListType> loadDocumentListType();
	
	public DocumentListType loadDocumentListTypeUsingDocumentListTypeId(Integer documentListTypeId);

}
