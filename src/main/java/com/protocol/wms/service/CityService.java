package com.protocol.wms.service;

import org.json.JSONObject;

import com.protocol.wms.model.City;

public interface CityService {
	
	public JSONObject getCityList(Integer districtId);
	
	public City loadCityObjectUsingCityId(Integer cityId);

	public boolean updateCityObject(City cityObj);

	public String getCityName(Integer cityId);
	
}
