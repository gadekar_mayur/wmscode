/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.EmployeeDocument;
import com.protocol.wms.model.EmployeeDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
public interface EmployeeDocumentService {
	
	public Integer saveEmployeeDocument(EmployeeDocument employeeDocumentObj);
	
	public List<EmployeeDocument> loadEmployeeDocumetListUsingOrganizationId(Integer organizationId);
	
	public JSONArray viewEmployeeDocumetListUsingOrganizationId(Integer organizationId) throws Exception;
	
	public JSONArray searchEmployeeDocumet(String searchOption,String searchText,Integer from,Integer organizationId)throws Exception;

	public Boolean deleteEmployeeDocumet(Integer empDocumentId);
	
	public JSONObject updateEmployeeDocumentInfo(Integer documentTypeId, Integer emplyeeDetailsId, Integer employeeDocumentId) throws Exception;
	
	public JSONArray updateEmployeeImages(Integer employeeDocumentId, List<EmployeeDocumentImagePath> documentList) throws Exception;
	
	public Boolean deleteEmployeeUploadDocumentFileImage(Integer employeeDocumentPathId);
}
