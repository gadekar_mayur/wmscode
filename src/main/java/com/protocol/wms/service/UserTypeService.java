/**
 * 
 */
package com.protocol.wms.service;

import com.protocol.wms.model.UserType;

/**
 * @author Sudhakar
 *
 */
public interface UserTypeService {
	
	public UserType loadUserTypeUsingUserTypeId(Integer userTypeId);

}
