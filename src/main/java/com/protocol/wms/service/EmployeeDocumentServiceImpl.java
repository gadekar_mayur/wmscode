/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.DocumentListTypeDao;
import com.protocol.wms.dao.EmployeeDetailsDao;
import com.protocol.wms.dao.EmployeeDocumentDao;
import com.protocol.wms.model.DocumentListType;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.EmployeeDocument;
import com.protocol.wms.model.EmployeeDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
@Service("EmployeeDocumentServiceImpl")
public class EmployeeDocumentServiceImpl implements EmployeeDocumentService{

	
	private EmployeeDocumentDao employeeDocumentDao;

	@Autowired
	@Qualifier("EmployeeDocumentDaoImpl")
	public void setEmployeeDocumentDao(EmployeeDocumentDao employeeDocumentDao) {
		this.employeeDocumentDao = employeeDocumentDao;
	}
	private EmployeeDetailsDao employeeDetailsDao;

	@Autowired
	@Qualifier("EmployeeDetailsDaoImpl")
	public void setEmployeeDetailsDao(EmployeeDetailsDao employeeDetailsDao) {
		this.employeeDetailsDao = employeeDetailsDao;
	}
	
	@Autowired
	@Qualifier("DocumentListTypeDaoImpl")
	private DocumentListTypeDao documentListTypeDao;
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDocumentService#saveEmployeeDocument(com.protocol.wms.model.EmployeeDocument)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveEmployeeDocument(EmployeeDocument employeeDocumentObj) {
		return employeeDocumentDao.saveEmployeeDocument(employeeDocumentObj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<EmployeeDocument> loadEmployeeDocumetListUsingOrganizationId (Integer organizationId) {
		return employeeDocumentDao.loadEmployeeDocumetListUsingOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDocumentService#loadEmployeeDocumetListUsingOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray viewEmployeeDocumetListUsingOrganizationId(
			Integer organizationId) throws JSONException {
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		{
			List<EmployeeDocument> employeeDocumentsList = employeeDocumentDao.loadEmployeeDocumetListUsingOrganizationId(organizationId);
			if(employeeDocumentsList.size()>0)
			{
				for(EmployeeDocument obj : employeeDocumentsList)
				{
					if("1".equals(obj.getOrganization().getOrganizationStatus())&&obj.getEmployeeDetails().getStatus()==1){
					json=new JSONObject();
					json.put("EMP_DOCUMENT_ID", obj.getEmployeeDocumentId());
					json.put("ORG_ID", obj.getOrganization().getOrganizationId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("EMP_ID", obj.getEmployeeDetails().getEmployeeDetailsId());
					json.put("EMP_NAME", obj.getEmployeeDetails().getName());
					json.put("DOCUMENT_TYPE_ID", obj.getDocumentListType().getDocumentListTypeId());
					json.put("DOCUMENT_TYPE", obj.getDocumentListType().getDocumentName());
					json_ImageArray=new JSONArray();
					for(EmployeeDocumentImagePath tempObj : obj.getEmployeeDocumentImagePathList()){
						tempJson = new JSONObject();
						tempJson.put("IMAGE_PATH", tempObj.getImagePath());
						tempJson.put("IMAGE_ID", tempObj.getEmployeeDocumentImagePathId());
						json_ImageArray.put(tempJson);
					}
					json.put("FILE_NAME", json_ImageArray);//.substring(obj.getImagePath().length() - 40));
					json_data_array.put(json);
					}
				}
			}
		}
		return json_data_array;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject updateEmployeeDocumentInfo(Integer documentTypeId, Integer emplyeeDetailsId, Integer employeeDocumentId)
			throws Exception {
		JSONObject result= new JSONObject();
		EmployeeDocument employeeDocument = null;
		EmployeeDetails employeeDetails = null;
		DocumentListType documentListType = null;
		int counter = 0;
		{
			employeeDocument = employeeDocumentDao.loadEmployeeDocumentObjById(employeeDocumentId);
			if(employeeDocument.getDocumentListType().getDocumentListTypeId()!=documentTypeId){
				documentListType = documentListTypeDao.loadDocumentListTypeUsingDocumentListTypeId(documentTypeId);
				employeeDocument.setDocumentListType(documentListType);counter++;
			}
			if(employeeDocument.getEmployeeDetails().getEmployeeDetailsId()!= emplyeeDetailsId){
				employeeDetails = employeeDetailsDao.loadEmployeeObjectUsingEmployeeDetailsId(emplyeeDetailsId);
				employeeDocument.setEmployeeDetails(employeeDetails);counter++;
			}
			result.put("MSG", "Employee Uploaded Document Details Updated Successfully...!");
			if(counter>0){
				result.put("MSG", "Employee Uploaded Document Details Updation failed...!");
				if(employeeDocumentDao.updateEmployeeDocumentObj(employeeDocument)){
					result.put("RESULT", true);
					result.put("MSG", "Employee Uploaded Document Details Updated Successfully...!");
					result.put("EMP_ID", employeeDocument.getEmployeeDetails().getEmployeeDetailsId());
					result.put("EMP_NAME", employeeDocument.getEmployeeDetails().getName());
					result.put("DOCUMENT_TYPE_ID", employeeDocument.getDocumentListType().getDocumentListTypeId());
					result.put("DOCUMENT_TYPE", employeeDocument.getDocumentListType().getDocumentName());
				}
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDocumentService#searchEmployeeDocumet(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray searchEmployeeDocumet(String searchOption,
			String searchText, Integer from, Integer organizationId) throws JSONException {
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		List<EmployeeDocument> employeeDocumentsList=null;
		{
			Map<String,Object>map = employeeDocumentDao.searchEmployeeDocumet(searchOption, searchText,from, organizationId);
			employeeDocumentsList =(List<EmployeeDocument>) map.get("employeeDocument");
			Integer totalPages = (Integer)map.get("totalPages");
			
			/*if(employeeDocumentsList.size()>0)
			{*/
				for(EmployeeDocument obj : employeeDocumentsList)
				{
					//if("1".equals(obj.getOrganization().getOrganizationStatus())&&obj.getEmployeeDetails().getStatus()==1){
					json=new JSONObject();
					json.put("EMP_DOCUMENT_ID", obj.getEmployeeDocumentId());
					json.put("ORG_ID", obj.getOrganization().getOrganizationId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("EMP_ID", obj.getEmployeeDetails().getEmployeeDetailsId());
					json.put("EMP_NAME", obj.getEmployeeDetails().getName());
					json.put("DOCUMENT_TYPE_ID", obj.getDocumentListType().getDocumentListTypeId());
					json.put("DOCUMENT_TYPE", obj.getDocumentListType().getDocumentName());
					json_ImageArray=new JSONArray();
					for(EmployeeDocumentImagePath tempObj : obj.getEmployeeDocumentImagePathList()){
						tempJson = new JSONObject();
						tempJson.put("IMAGE_PATH", tempObj.getImagePath());
						tempJson.put("IMAGE_ID", tempObj.getEmployeeDocumentImagePathId());
						json_ImageArray.put(tempJson);
					}
					json.put("FILE_NAME", json_ImageArray);
					json_data_array.put(json);
					//}
				}
				json=new JSONObject();
				json.put("paginationCount",totalPages);
				json_data_array.put(json);
			
		}
		return json_data_array;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDocumentService#deleteEmployeeDocumet(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteEmployeeDocumet(Integer empDocumentId) {
		return employeeDocumentDao.deleteEmployeeDocumet(empDocumentId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateEmployeeImages(Integer employeeDocumentId,
			List<EmployeeDocumentImagePath> documentList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		documentList = employeeDocumentDao.updateEmployeeImages(employeeDocumentId, documentList);
		for(EmployeeDocumentImagePath tempObj : documentList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getEmployeeDocumentImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteEmployeeUploadDocumentFileImage(Integer employeeDocumentPathId) {
		return employeeDocumentDao.deleteEmployeeUploadDocumentFileImage(employeeDocumentPathId);
	}
}
