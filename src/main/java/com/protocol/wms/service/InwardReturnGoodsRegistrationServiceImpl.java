/**
 * 
 */
package com.protocol.wms.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CheckingDoneDao;
import com.protocol.wms.dao.CreditNoteDao;
import com.protocol.wms.dao.InwardReturnGoodsRegistrationDao;
import com.protocol.wms.dao.InwardReturnGoodsRegistrationLRDetailsDao;
import com.protocol.wms.dao.InwardReturnProductsDao;
import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.CheckingDoneCheckingSlipImagePath;
import com.protocol.wms.model.CreditNote;
import com.protocol.wms.model.CreditNoteCreditNoteImagePath;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsClaimCopyPath;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsLrImgPath;
import com.protocol.wms.model.InwardReturnProducts;

/**
 * @author admin
 *
 */
@Service("InwardReturnGoodsRegistrationServiceImpl")
public class InwardReturnGoodsRegistrationServiceImpl implements InwardReturnGoodsRegistrationService {

	@Autowired
	@Qualifier("InwardReturnGoodsRegistrationDaoImpl")
	private InwardReturnGoodsRegistrationDao inwardReturnGoodsRegistrationDao;

	@Autowired
	@Qualifier("InwardReturnGoodsRegistrationLRDetailsDaoImpl")
	private InwardReturnGoodsRegistrationLRDetailsDao inwardReturnGoodsRegistrationLRDetailsDao;
	
	@Autowired
	@Qualifier("InwardReturnProductsDaoImpl")
	private InwardReturnProductsDao inwardReturnProductsDao;
	
	@Autowired
	@Qualifier("CheckingDoneDaoImpl")
	private CheckingDoneDao checkingDoneDao;
	
	@Autowired
	@Qualifier("CreditNoteDaoImpl")
	private CreditNoteDao creditNoteDao ;
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#inwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.util.List, java.util.Map)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveInwardReturnGoodsRegistration(Integer organizationId,
			Integer companyId, Integer stockistId, Integer transporterId,
			Integer employeeId,
			List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList,
			Map<Integer, List<InwardReturnProducts>> inwardReturnProductsMap,
			InwardReturnGoodsRegistration inwardReturnGoodsRegistration) {
		return inwardReturnGoodsRegistrationDao.saveInwardReturnGoodsRegistration(organizationId, companyId, stockistId, transporterId, employeeId, lrDetailsList, inwardReturnProductsMap,inwardReturnGoodsRegistration);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#updateInwardReturnGoodsLrReg(java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateInwardReturnGoodsLrReg(List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList) {
		// TODO Auto-generated method stub
		return inwardReturnGoodsRegistrationDao.updateInwardReturnGoodsLrReg(lrDetailsList);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#inwardReturnGoodsRegListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InwardReturnGoodsRegistration> inwardReturnGoodsRegListing(Integer organizationId) {
		// TODO Auto-generated method stub
		return inwardReturnGoodsRegistrationDao.inwardReturnGoodsRegListing(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#inwardReturnGoodsRegLrListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InwardReturnGoodsRegistrationLRDetails> inwardReturnGoodsRegLrListing(Integer inwardReturnGoodRegistrationId) {
		// TODO Auto-generated method stub
		return inwardReturnGoodsRegistrationDao.inwardReturnGoodsRegLrListing(inwardReturnGoodRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#inwardReturnGoodsRegDetailsAndLrListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String, Object> inwardReturnGoodsRegDetailsAndLrListing(Integer ID_NO) {
		// TODO Auto-generated method stub
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration = inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(ID_NO);
		List<InwardReturnGoodsRegistrationLRDetails> lrList = inwardReturnGoodsRegistrationDao.inwardReturnGoodsRegLrListing(ID_NO);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("InwardReturnGoodsRegistration", inwardReturnGoodsRegistration);
		map.put("lrList", lrList);
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#inwardReturnGoodsRegDetailsAndLrListingForCheckingPending(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String, Object> inwardReturnGoodsRegDetailsAndLrListingForCheckingPending(Integer inwardReturnGoodRegistrationId) {
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration = inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(inwardReturnGoodRegistrationId);
		List<InwardReturnGoodsRegistrationLRDetails> lrList = inwardReturnGoodsRegistrationDao.inwardReturnGoodsRegLrListing(inwardReturnGoodRegistrationId);
		Map<String,Object> map = new HashMap<String, Object>();
		List<Integer> lrIdList = inwardReturnGoodsRegistrationDao.inwardReturnGoodsRegLrIdDoNotHaveProduct(inwardReturnGoodRegistrationId);
		map.put("InwardReturnGoodsRegistration", inwardReturnGoodsRegistration);
		map.put("lrList", lrList);
		map.put("lrIdList", lrIdList);
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#inwardProductListByLR_NO(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InwardReturnProducts> inwardProductListByLR_NO(Integer LR_NO) {
		return inwardReturnGoodsRegistrationDao.inwardProductListByLR_NO(LR_NO);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#viewLR_DetailedViewsAndLrProductList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String, Object> viewLR_DetailedViewsAndLrProductList(Integer LR_NO) {
		InwardReturnGoodsRegistrationLRDetails LR_Details = inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationLRDetails(LR_NO);
		List<InwardReturnProducts> productList = inwardReturnGoodsRegistrationDao.inwardProductListByLR_NO(LR_NO);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("InwardReturnGoodsRegistrationLRDetails", LR_Details);
		map.put("productList", productList);
		return map;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject viewLR_DetailedViewsAndLrProductListView(Integer LR_NO) throws JSONException {

		JSONObject result = null;
		JSONObject tempJsonObj = null;
		JSONArray tempJsonArr = null;
		InwardReturnGoodsRegistrationLRDetails LR_Details = null;
		List<InwardReturnProducts>inwardReturnProductsList = null;
		InwardReturnProducts tempInwardReturnProducts = null;
		Iterator<InwardReturnProducts> inwardReturnProductsItr = null;
		{
			
			LR_Details = inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationLRDetails(LR_NO);
			//inwardReturnProductsList = LR_Details.getInwardReturnProductList();
			inwardReturnProductsList = inwardReturnGoodsRegistrationDao.inwardProductListByLR_NO(LR_NO);
			result = new JSONObject();
			result.put("LR_ID", LR_Details.getInwardReturnGoodsRegistrationLRDetailsId());
			result.put("LR_NO", LR_Details.getLRNo());
			result.put("NO_OF_CASES", LR_Details.getNoOfCases());
			result.put("REF_CLAIM_NO", LR_Details.getRefNoOrClaimNo());
			result.put("CLAIM_AMOUNT", LR_Details.getClaimAmount());
			//result.put("ATTACH_LR_PATH",LR_Details.getLRImagePath());
			//result.put("ATTACH_CLAIM_COPY_PATH", LR_Details.getClaimCopyImagePath());
			tempJsonArr=new JSONArray();
			for(InwardReturnGoodsRegistrationLRDetailsLrImgPath tempObj : LR_Details.getInwardReturnGoodsRegistrationLRDetailsLrImgPathList()){
				tempJsonObj = new JSONObject();
				tempJsonObj.put("IMAGE_PATH", tempObj.getImagePath());
				tempJsonObj.put("IMAGE_ID", tempObj.getInwardReturnGoodsRegistrationLRDetailsLrImgPathId());
				tempJsonArr.put(tempJsonObj);
			}
			result.put("ATTACH_LR", tempJsonArr);
			tempJsonArr=new JSONArray();
			for(InwardReturnGoodsRegistrationLRDetailsClaimCopyPath tempObj : LR_Details.getInwardReturnGoodsRegistrationLRDetailsClaimCopyPathList()){
				tempJsonObj = new JSONObject();
				tempJsonObj.put("IMAGE_PATH", tempObj.getImagePath());
				tempJsonObj.put("IMAGE_ID", tempObj.getInwardReturnGoodsRegistrationLRDetailsClaimCopyPathId());
				tempJsonArr.put(tempJsonObj);
			}
			result.put("ATTACH_CLAIM_COPY", tempJsonArr);
			tempJsonArr = new JSONArray();
			inwardReturnProductsItr = inwardReturnProductsList.iterator();
			while(inwardReturnProductsItr.hasNext()){
				tempInwardReturnProducts = inwardReturnProductsItr.next();
				tempJsonObj = new JSONObject();
				tempJsonObj.put("PRODUCT_ID", tempInwardReturnProducts.getInwardReturnProductsId());
				tempJsonObj.put("PRODUCT_NAME", tempInwardReturnProducts.getProductName());
				tempJsonObj.put("CLAIM_QUANTITY", tempInwardReturnProducts.getClaimQuatity());
				tempJsonObj.put("RECEIVED_QUANTITY", tempInwardReturnProducts.getReceivedQuantity());
				tempJsonObj.put("BATCH", tempInwardReturnProducts.getBatch());
				tempJsonObj.put("MFG_DATE", tempInwardReturnProducts.getMfgDate());
				tempJsonObj.put("EXPIRY_DATE",tempInwardReturnProducts.getExpiryDate() );
				//for edit product form
				String [] date_arr = tempInwardReturnProducts.getMfgDate().toString().split("-");
				tempJsonObj.put("EDIT_MFG_DATE",date_arr[1]+"/"+date_arr[2]+"/"+date_arr[0] );
				date_arr = tempInwardReturnProducts.getExpiryDate().toString().split("-");
				tempJsonObj.put("EDIT_EXPIRY_DATE",date_arr[1]+"/"+date_arr[2]+"/"+date_arr[0]);
				
				tempJsonObj.put("MFG_COMPANY", tempInwardReturnProducts.getMfgCompany());
				tempJsonObj.put("QUANTITY_VARIANCE", tempInwardReturnProducts.getQuantityVariance());
				tempJsonObj.put("REASON", tempInwardReturnProducts.getReason());
				tempJsonArr.put(tempJsonObj);
			}
			result.put("productsArr", tempJsonArr);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#deleteInwardReturnGoodsRegDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardReturnGoodsRegDetails(Integer inwardReturnGoodRegistrationId) {
		// TODO Auto-generated method stub
		return inwardReturnGoodsRegistrationDao.deleteInwardReturnGoodsRegDetails(inwardReturnGoodRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#deleteInwardReturnGoodsRegLrDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardReturnGoodsRegLrDetails(Integer LR_ID) {
		// TODO Auto-generated method stub
		return inwardReturnGoodsRegistrationDao.deleteInwardReturnGoodsRegLrDetails(LR_ID);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#deleteInwardReturnGoodsRegLRProduct(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardReturnGoodsRegLRProduct(Integer PRODUCT_ID) {
		// TODO Auto-generated method stub
		return inwardReturnGoodsRegistrationDao.deleteInwardReturnGoodsRegLRProduct(PRODUCT_ID);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public InwardReturnGoodsRegistration loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(
			Integer InwardReturnGoodsRegistrationoId) {
		// TODO Auto-generated method stub
		return inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(InwardReturnGoodsRegistrationoId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject inwardReturnGoodsRegDetailsAndLrListingCheckingDoneView(Integer InwardReturnGoodsRegistrationId) throws JSONException, ParseException {

		JSONObject result = null;
		JSONArray irgrLRArr = null;
		JSONArray json_ImageArray = null;
		JSONObject tempJson = null;
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration = null;
		List<InwardReturnGoodsRegistrationLRDetails>inwardReturnGoodsRegistrationLRList = null;
		InwardReturnGoodsRegistrationLRDetails tempInwardReturnGoodsRegLR = null;
		Iterator<InwardReturnGoodsRegistrationLRDetails> irgrLRItr = null;
		//block
		{
			irgrLRArr = new JSONArray();
			inwardReturnGoodsRegistration = inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(InwardReturnGoodsRegistrationId);
			inwardReturnGoodsRegistrationLRList = inwardReturnGoodsRegistration.getInwardReturnGoodsRegistrationLRDetailList();
			irgrLRItr = inwardReturnGoodsRegistrationLRList.iterator();
			while(irgrLRItr.hasNext()){
				tempInwardReturnGoodsRegLR = irgrLRItr.next();
				result = new JSONObject();
				result.put("LR_ID", tempInwardReturnGoodsRegLR.getInwardReturnGoodsRegistrationLRDetailsId());
				result.put("LR_NO", tempInwardReturnGoodsRegLR.getLRNo());
				result.put("NO_OF_CASES", tempInwardReturnGoodsRegLR.getNoOfCases());
				result.put("REF_CLAIM_NO", tempInwardReturnGoodsRegLR.getRefNoOrClaimNo());
				result.put("CLAIM_AMOUNT", tempInwardReturnGoodsRegLR.getClaimAmount());
//				String s = tempInwardReturnGoodsRegLR.getLRImagePath();int i = s.lastIndexOf('/');s=s.substring(i+1);
				json_ImageArray=new JSONArray();
				for(InwardReturnGoodsRegistrationLRDetailsLrImgPath tempObj : tempInwardReturnGoodsRegLR.getInwardReturnGoodsRegistrationLRDetailsLrImgPathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getInwardReturnGoodsRegistrationLRDetailsLrImgPathId());
					json_ImageArray.put(tempJson);
				}
				result.put("ATTACH_LR",json_ImageArray);
//				s = tempInwardReturnGoodsRegLR.getClaimCopyImagePath(); i = s.lastIndexOf('/');s=s.substring(i+1);
				json_ImageArray=new JSONArray();
				for(InwardReturnGoodsRegistrationLRDetailsClaimCopyPath tempObj : tempInwardReturnGoodsRegLR.getInwardReturnGoodsRegistrationLRDetailsClaimCopyPathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getInwardReturnGoodsRegistrationLRDetailsClaimCopyPathId());
					json_ImageArray.put(tempJson);
				}
				result.put("ATTACH_CLAIM_COPY", json_ImageArray);
				irgrLRArr.put(result);
			}
			result = new JSONObject();
			//for Edit
			result.put("ORG_ID", inwardReturnGoodsRegistration.getOrganization().getOrganizationId());
			result.put("COMPANY_ID", inwardReturnGoodsRegistration.getCompany().getCompanyId());
			result.put("STOCKIST_ID", inwardReturnGoodsRegistration.getStockist().getStockistId());
			result.put("TRANSPORTER_ID", inwardReturnGoodsRegistration.getTransporterDetails().getTransporterDetailsId());
			result.put("EMPLOYEE_ID", inwardReturnGoodsRegistration.getEmployeeDetails().getEmployeeDetailsId());
			
			result.put("ID_NO", inwardReturnGoodsRegistration.getInwardReturnGoodRegistrationId());
			result.put("ORGANIZATION", inwardReturnGoodsRegistration.getOrganization().getOrganizationName());
			result.put("COMPANY", inwardReturnGoodsRegistration.getCompany().getCompanyName());
			result.put("STOCKIST", inwardReturnGoodsRegistration.getStockist().getStockistName());
			result.put("TRANSPORTER", inwardReturnGoodsRegistration.getTransporterDetails().getTransporterName());
			String [] reg_date_arr = inwardReturnGoodsRegistration.getReturnGoodsRegistrationDate().toString().split("-");
			result.put("REG_DATE",reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0] );
			//result.put("REG_DATE", inwardReturnGoodsRegistration.getReturnGoodsRegistrationDate());
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
	        java.util.Date _24HourDt = _24HourSDF.parse(inwardReturnGoodsRegistration.getTime().toString());
			result.put("TIME", _12HourSDF.format(_24HourDt));
			if(inwardReturnGoodsRegistration.getDriverName()==null || "".equals(inwardReturnGoodsRegistration.getDriverName()))
			{
				result.put("DRIVER", "");
			}else{
				result.put("DRIVER", inwardReturnGoodsRegistration.getDriverName());
			}
			if(inwardReturnGoodsRegistration.getTransportationCharges()==null || "".equals(inwardReturnGoodsRegistration.getTransportationCharges()))
			{
				result.put("TRANSPORTATION_CHARGES", "");
			}else{
				result.put("TRANSPORTATION_CHARGES", inwardReturnGoodsRegistration.getTransportationCharges());
			}
			
			if("".equals(inwardReturnGoodsRegistration.getDriverNo()) || inwardReturnGoodsRegistration.getDriverNo()==null)
			{
				result.put("DRIVER_NO", "");
			}else{
				result.put("DRIVER_NO", inwardReturnGoodsRegistration.getDriverNo());
			}
			
			result.put("SUBMIT_DATE", inwardReturnGoodsRegistration.getSubmitDate());
			result.put("EMPLOYEE_NAME", inwardReturnGoodsRegistration.getEmployeeDetails().getName());
			if(inwardReturnGoodsRegistration.getCreditNote()!=null){
				result.put("CREDIT_NOTE_NO", inwardReturnGoodsRegistration.getCreditNote().getCreditNoteNo());
				result.put("CREDIT_NOTE_AMOUNT", inwardReturnGoodsRegistration.getCreditNote().getCreditNoteAmount());
				result.put("CREDIT_NOTE_REMARK", inwardReturnGoodsRegistration.getCreditNote().getRemark());
				json_ImageArray = new JSONArray();
				for(CreditNoteCreditNoteImagePath tempObj : inwardReturnGoodsRegistration.getCreditNote().getCreditNoteCreditNoteImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getCreditNoteCreditNoteImagePathId());
					json_ImageArray.put(tempJson);
				}
				result.put("CREDIT_NOTE_ATTACH", json_ImageArray);
				//for Edit
				result.put("CREDIT_NOTE_ID", inwardReturnGoodsRegistration.getCreditNote().getCreditNoteId());
			}
			if(inwardReturnGoodsRegistration.getCheckingDoneStatus()==1){
				//CheckingDone checkingDone = inwardReturnGoodsRegistrationService.loadCheckingDoneObjByInwardReturnGoodsRegId(ID_NO);
				CheckingDone checkingDone = inwardReturnGoodsRegistration.getCheckingDoneList().get(0);
				result.put("CHECKING_DONE_EMP_NAME", checkingDone.getEmployeeDetails().getName());
				reg_date_arr = checkingDone.getCheckingDoneDate().toString().split("-");
				result.put("CHECKING_DONE_DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
				_24HourDt = _24HourSDF.parse(checkingDone.getTime().toString());
				result.put("CHECKING_DONE_TIME", _12HourSDF.format(_24HourDt));
				result.put("CHECKING_DONE_REMARK", checkingDone.getRemark());
				json_ImageArray = new JSONArray();
				for(CheckingDoneCheckingSlipImagePath tempObj : checkingDone.getCheckingDoneCheckingSlipImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getCheckingDoneCheckingSlipImagePathId());
					json_ImageArray.put(tempJson);
				}
				result.put("CHECKING_DONE_SLIP_IMG_PATH", json_ImageArray);
				//for Edit Checking Done
				result.put("CHECKING_DONE_EMP_ID", checkingDone.getEmployeeDetails().getEmployeeDetailsId());
				result.put("CHECKING_DONE_ID", checkingDone.getCheckingDoneId());
			}
			result.put("irgrLRArr", irgrLRArr);
		}//end block
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#editInwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardReturnGoodsRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject editInwardReturnGoodsRegistration(Integer orgId,
			Integer companyId, Integer stockistId, Integer transporterId,
			Integer employeeId,
			InwardReturnGoodsRegistration inwardReturnGoodsRegistration) throws JSONException {
		JSONObject result = new JSONObject();
		result.put("MSG", "Inward Return Goods Registration Updation Failed....!");
		if(inwardReturnGoodsRegistrationDao.editInwardReturnGoodsRegistration(orgId, companyId, stockistId, transporterId, employeeId, inwardReturnGoodsRegistration)){
			result.put("ID_NO", inwardReturnGoodsRegistration.getInwardReturnGoodRegistrationId());
			result.put("ORGANIZATION", inwardReturnGoodsRegistration.getOrganization().getOrganizationName());
			result.put("COMPANY", inwardReturnGoodsRegistration.getCompany().getCompanyName());
			result.put("STOCKIST", inwardReturnGoodsRegistration.getStockist().getStockistName());
			result.put("TRANSPORTER", inwardReturnGoodsRegistration.getTransporterDetails().getTransporterName());
			result.put("RESULT",true);
			result.put("MSG", "Inward Return Goods Registration Updated Successfully....!");
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#editInwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardReturnGoodsRegistration, java.lang.Integer, com.protocol.wms.model.CheckingDone)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject editInwardReturnGoodsRegistration(Integer orgId,
			Integer companyId, Integer stockistId, Integer transporterId,
			Integer employeeId,
			InwardReturnGoodsRegistration inwardReturnGoodsRegistration,
			Integer checkingDoneEmployeeDetailsId, CheckingDone checkingDone)
			throws Exception {
		JSONObject result = new JSONObject();
		result.put("MSG", "Inward Return Goods Registration Updation Failed....!");
		if(inwardReturnGoodsRegistrationDao.editInwardReturnGoodsRegistration(orgId, companyId, stockistId, transporterId, employeeId, inwardReturnGoodsRegistration,checkingDoneEmployeeDetailsId,checkingDone)){
			result.put("ID_NO", inwardReturnGoodsRegistration.getInwardReturnGoodRegistrationId());
			result.put("ORG_ID", inwardReturnGoodsRegistration.getOrganization().getOrganizationId());
			result.put("ORGANIZATION", inwardReturnGoodsRegistration.getOrganization().getOrganizationName());
			result.put("COMPANY", inwardReturnGoodsRegistration.getCompany().getCompanyName());
			result.put("STOCKIST", inwardReturnGoodsRegistration.getStockist().getStockistName());
			result.put("TRANSPORTER", inwardReturnGoodsRegistration.getTransporterDetails().getTransporterName());
			result.put("RESULT",true);

			result.put("CHECKING_DONE_ID", inwardReturnGoodsRegistration.getCheckingDoneList().get(0).getCheckingDoneId());
			result.put("CHECKING_DONE_EMP_NAME", inwardReturnGoodsRegistration.getCheckingDoneList().get(0).getEmployeeDetails().getName());
			String [] reg_date_arr = inwardReturnGoodsRegistration.getCheckingDoneList().get(0).getCheckingDoneDate().toString().split("-");
			result.put("CHECKING_DONE_DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
	        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
	        java.util.Date _24HourDt = _24HourSDF.parse(inwardReturnGoodsRegistration.getCheckingDoneList().get(0).getTime().toString());
			result.put("CHECKING_DONE_TIME", _12HourSDF.format(_24HourDt));
			result.put("CHECKING_DONE_REMARK", inwardReturnGoodsRegistration.getCheckingDoneList().get(0).getRemark());
			result.put("CHECKING_DONE_SLIP_IMG_PATH", inwardReturnGoodsRegistration.getCheckingDoneList().get(0).getCheckingSlipImagePath());
			//for Edit Checking Done
			result.put("CHECKING_DONE_EMP_ID", inwardReturnGoodsRegistration.getCheckingDoneList().get(0).getEmployeeDetails().getEmployeeDetailsId());
			
			result.put("MSG", "Inward Return Goods Registration Updated Successfully....!");
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#loadInwardReturnGoodsRegistrationLRDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public InwardReturnGoodsRegistrationLRDetails loadInwardReturnGoodsRegistrationLRDetails(
			Integer InwardReturnGoodsRegistrationLRDetailsId) {
		return inwardReturnGoodsRegistrationLRDetailsDao.loadInwardReturnGoodsRegistrationLRDetails(InwardReturnGoodsRegistrationLRDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#editInwardReturnGoodsRegLrDetails(com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean editInwardReturnGoodsRegLrDetails(
			InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails) {
		return inwardReturnGoodsRegistrationLRDetailsDao.editInwardReturnGoodsRegLrDetails(inwardReturnGoodsRegistrationLRDetails);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#loadInwardReturnProductsObjById(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public InwardReturnProducts loadInwardReturnProductsObjById(Integer InwardReturnProductsId) {
		return inwardReturnProductsDao.loadInwardReturnProductsObjById(InwardReturnProductsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#editInwardReturnGoodsRegLreditProductDetails(com.protocol.wms.model.InwardReturnProducts)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean editInwardReturnGoodsRegLreditProductDetails(
			InwardReturnProducts inwardReturnProducts) {
		return inwardReturnProductsDao.editInwardReturnGoodsRegLreditProductDetails(inwardReturnProducts);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#loadCheckingDoneObjByInwardReturnGoodsRegId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CheckingDone loadCheckingDoneObjByInwardReturnGoodsRegId(
			Integer inwardReturnGoodRegistrationId) {
		return checkingDoneDao.loadCheckingDoneObjByInwardReturnGoodsRegId(inwardReturnGoodRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#loadCheckingDoneObjByCheckingDoneId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CheckingDone loadCheckingDoneObjByCheckingDoneId(Integer checkingDoneId) {
		return checkingDoneDao.loadCheckingDoneObjByCheckingDoneId(checkingDoneId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#editInwardReturnGoodsRegCheckingDoneDetails(com.protocol.wms.model.CheckingDone, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject editInwardReturnGoodsRegCheckingDoneDetails(CheckingDone checkingDone, Integer checkingDoneEmployeeDetailsId) throws JSONException, ParseException {
		JSONObject result = null;
		result = new JSONObject();
		result.put("MSG", "Checking Done Details Updation Failed....!");
		if(checkingDoneDao.editInwardReturnGoodsRegCheckingDoneDetails(checkingDone, checkingDoneEmployeeDetailsId)){
			//result.put("CHECKING_DONE_ID", checkingDone.getCheckingDoneId());
			result.put("CHECKING_DONE_EMP_NAME", checkingDone.getEmployeeDetails().getName());
			String [] reg_date_arr = checkingDone.getCheckingDoneDate().toString().split("-");
			result.put("CHECKING_DONE_DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
	        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
	        java.util.Date _24HourDt = _24HourSDF.parse(checkingDone.getTime().toString());
			result.put("CHECKING_DONE_TIME", _12HourSDF.format(_24HourDt));
			result.put("CHECKING_DONE_REMARK", checkingDone.getRemark());
			result.put("CHECKING_DONE_SLIP_IMG_PATH", checkingDone.getCheckingSlipImagePath());
			//for Edit Checking Done
			result.put("CHECKING_DONE_EMP_ID", checkingDone.getEmployeeDetails().getEmployeeDetailsId());
			result.put("RESULT",true);
			result.put("MSG", "Checking Done Details Updated Successfully....!");
		}
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateCheckingDoneRegCheckingSlipImage(
			Integer checkingDoneId,
			List<CheckingDoneCheckingSlipImagePath> checkingSlipImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		checkingSlipImgList = checkingDoneDao.updateCheckingDoneRegCheckingSlipImage(checkingDoneId, checkingSlipImgList);
		for(CheckingDoneCheckingSlipImagePath tempObj : checkingSlipImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getCheckingDoneCheckingSlipImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#loadCreditNoteObjectUsingCreditNoteId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CreditNote loadCreditNoteObjectUsingCreditNoteId(Integer creditNoteId) {
		return creditNoteDao.loadCreditNoteObjectUsingCreditNoteId(creditNoteId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsRegistrationService#editInwardReturnGoodsRegCreditNoteDetails(com.protocol.wms.model.CreditNote)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean editInwardReturnGoodsRegCreditNoteDetails(CreditNote creditNote) {
		// TODO Auto-generated method stub
		return creditNoteDao.editInwardReturnGoodsRegCreditNoteDetails(creditNote);
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String , Object> searchInwardReturnGoodsRegListing(
			String selectedValue, String searchText,String fromSpecialData,String toSpecialData,String searchOption, Integer from, Integer organizationId) {
		return inwardReturnGoodsRegistrationLRDetailsDao.searchInwardReturnGoodsRegListing(selectedValue,searchText,fromSpecialData,toSpecialData,searchOption, from, organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardReturnGoodsLrRegLrImage(Integer lrImageId) {
		return inwardReturnGoodsRegistrationLRDetailsDao.deleteInwardReturnGoodsLrRegLrImage(lrImageId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardReturnGoodsLrRegClaimCopyImage(Integer lrImageId) {
		return inwardReturnGoodsRegistrationLRDetailsDao.deleteInwardReturnGoodsLrRegClaimCopyImage(lrImageId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateInwardReturnGoodsLrRegLrImage(Integer inwardReturnGoodsLrRegistrationId,List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> lrImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		lrImgList = inwardReturnGoodsRegistrationLRDetailsDao.updateInwardReturnGoodsLrRegLrImage(inwardReturnGoodsLrRegistrationId, lrImgList);
		for(InwardReturnGoodsRegistrationLRDetailsLrImgPath tempObj : lrImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getInwardReturnGoodsRegistrationLRDetailsLrImgPathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateInwardReturnGoodsLrRegClaimCopyImage(
			Integer inwardReturnGoodsLrRegistrationId,
			List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> claimCopyImgList) throws JSONException
	 {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		claimCopyImgList = inwardReturnGoodsRegistrationLRDetailsDao.updateInwardReturnGoodsLrRegClaimCopyImage(inwardReturnGoodsLrRegistrationId, claimCopyImgList);
		for(InwardReturnGoodsRegistrationLRDetailsClaimCopyPath tempObj : claimCopyImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getInwardReturnGoodsRegistrationLRDetailsClaimCopyPathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCheckingDoneCheckingSlipImage(
			Integer checkingSlipImageId) {
		// TODO Auto-generated method stub
		return checkingDoneDao.deleteCheckingDoneCheckingSlipImage(checkingSlipImageId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateCreditNoteRegCreditNoteAttachImage(Integer creditNoteId,List<CreditNoteCreditNoteImagePath> creditNoteImgList)
			throws Exception {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		creditNoteImgList = creditNoteDao.updateCreditNoteRegCreditNoteAttachImage(creditNoteId, creditNoteImgList);
		for(CreditNoteCreditNoteImagePath tempObj : creditNoteImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getCreditNoteCreditNoteImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCreditNoteRegCreditNoteAttachImage(Integer creditNoteAttachImageId) {
		return creditNoteDao.deleteCreditNoteRegCreditNoteAttachImage(creditNoteAttachImageId);
	}
}
