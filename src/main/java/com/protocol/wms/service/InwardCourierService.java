/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.InwardCourierRegistration;
import com.protocol.wms.model.InwardCourierRegistrationDocketFilePath;
import com.protocol.wms.model.Particulars;

/**
 * @author admin
 *
 */
public interface InwardCourierService {
	
	public List<Particulars> getParticularsDropdownList();
	
	public Boolean addInwardCourierRegistration(String companyStockistOption,Integer orgId,Integer companyId ,Integer state, Integer district, Integer city,Integer stockistId ,Integer particular ,Integer companyBankId,InwardCourierRegistration inwardCourierRegistration, AdvancedChequeInformation advancedChequeInformation,List<ChequeNumberInfo>chequeNumberInfoList);
	
	public JSONObject editInwardCourierRegistration(String companyStockistOption,Integer orgId,Integer companyId ,Integer state ,Integer district ,Integer city ,Integer stockistId ,Integer particular ,Integer companyBankId,InwardCourierRegistration inwardCourierRegistration, AdvancedChequeInformation advancedChequeInformation,List<ChequeNumberInfo>chequeNumberInfoList) throws Exception;
	
	public List<InwardCourierRegistration> inwardCourierRegistrationListing(Integer organizationId);
	
	public Map<Object,Object> searchInwardCourierRegistrationDetails(String searchOption,String searchText,String fromSpecialData,String toSpecialData,Integer organizationId, Integer from);
	
	public Boolean deleteInwardCourierRegDetails(Integer inwardCourierRegId);
	
	public InwardCourierRegistration loadInwardCourierRegistrationObjectUsignInwardCourierRegId(Integer inwardCourierRegId);
	
	public JSONArray loadInwardCourierRegistrationViewUsignInwardCourierRegId(Boolean editStatus, Integer inwardCourierRegId) throws Exception;
	
	public JSONArray updateInwardCourierRegDocketFileImage(Integer inwardCourierRegId, List<InwardCourierRegistrationDocketFilePath> docketFileImgList) throws Exception;
	
	public Boolean deleteInwardCourierRegistrationDocketFileImage(Integer docketFileImageId);
}
