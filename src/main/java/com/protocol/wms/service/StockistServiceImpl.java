/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CompanyDao;
import com.protocol.wms.dao.DocumentListTypeDao;
import com.protocol.wms.dao.DrugLicenseDao;
import com.protocol.wms.dao.StockistDao;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.DocumentListType;
import com.protocol.wms.model.DrugLicence;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistDocument;
import com.protocol.wms.model.StockistDocumentImagePath;
import com.protocol.wms.model.UserDetails;

/**
 * @author admin
 *
 */
@Service("StockistServiceImpl")
public class StockistServiceImpl implements StockistService{
	@Autowired
	@Qualifier("StockistDaoImpl")
	private StockistDao stockistDao;
	
	@Autowired
	@Qualifier("DrugLicenseDaoImpl")
	private DrugLicenseDao drugStockistDao;
	
	@Autowired
	@Qualifier("CompanyDaoImpl")
	private CompanyDao companyDao;
	
	@Autowired
	@Qualifier("DocumentListTypeDaoImpl")
	private DocumentListTypeDao documentListTypeDao;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject displayStockistDetailsForm(Integer organizationId) {
		return stockistDao.displayStockistDetailsForm(organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject saveStockist(Stockist stockist, Integer orgId,Integer stateId, Integer cityId, Integer companyId,
			Integer districtId, Integer transporterDetailsId,UserDetails userDetails) {
		return stockistDao.saveStockist(stockist,orgId,stateId,cityId,companyId,districtId,transporterDetailsId,userDetails);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getStockistByOrgAndCompany(Integer orgId,Integer companyId) {
		return stockistDao.getStockistByOrgAndCompany(orgId,companyId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getStokistList(Integer organizationId) {
		return stockistDao.getStokistList(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#searchStockistDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject searchStockistDetails(String searchOption,String searchText, Integer organizationId,Integer from) {
		return stockistDao.searchStockistDetails(searchOption, searchText, organizationId,from);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#getStockistByOrganization(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getStockistByOrganization(Integer organizationId) {
		return stockistDao.getStockistByOrganization(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#addCompanyToStockist(java.lang.Integer[], java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject addCompanyToStockist(Integer[] companyId,Integer stockistId, Integer orgId) {
		return stockistDao.addCompanyToStockist(companyId,stockistId,orgId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#stockistListingAccToOrg(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject stockistListingAccToOrg(Integer orgnizationId) {
		return stockistDao.stockistListingAccToOrg(orgnizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#searchOrganizationStockist(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject searchOrganizationStockist(String searchOption,String searchText, Integer orgnizationId,Integer from) {
		// TODO Auto-generated method stub
		return stockistDao.searchOrganizationStockist(searchOption, searchText, orgnizationId,from);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#uploadStockistDocument(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.StockistDocument)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject uploadStockistDocument(Integer orgId,Integer stockistId,Integer companyId,Integer documentTypeId,StockistDocument stockistDocument){
		return stockistDao.uploadStockistDocument(orgId,stockistId,companyId,documentTypeId,stockistDocument);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updatesStockistUploadDocumentImage(Integer stockistDocumentId,
			List<StockistDocumentImagePath> documentList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		documentList = stockistDao.updateCompanyUploadDocumentImage(stockistDocumentId, documentList);
		for(StockistDocumentImagePath tempObj : documentList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getStockistDocumentImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#uploadDocumentListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray uploadDocumentListing(Integer organizationId) {
		return stockistDao.uploadDocumentListing(organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteStockistDocumentFileImage(Integer stockistDocumentPathId) {
		// TODO Auto-generated method stub
		return stockistDao.deleteStockistDocumentFileImage(stockistDocumentPathId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject updateStockistDocumentInfo(Integer documentTypeId,
			Integer companyId, Integer stockistId, Integer stockistDocumentId)
			throws Exception {
		JSONObject result= new JSONObject();
		StockistDocument stockistDocument = null;
		Company company = null;
		Stockist stockist= null;
		DocumentListType documentListType = null;
		int counter = 0;
		{
			stockistDocument = stockistDao.loadStockistDocumentObjById(stockistDocumentId);
			if(stockistDocument.getDocumentListType().getDocumentListTypeId()!=documentTypeId){
				documentListType = documentListTypeDao.loadDocumentListTypeUsingDocumentListTypeId(documentTypeId);
				stockistDocument.setDocumentListType(documentListType);counter++;
			}
			if(stockistDocument.getCompany().getCompanyId()!=companyId){
				company = companyDao.loadCompanyObjectUsingCompanyId(companyId);
				stockistDocument.setCompany(company);counter++;
			}
			if(stockistDocument.getStockist().getStockistId()!=stockistId){
				stockist = stockistDao.loadStockistObjectByStockistId(stockistId);
				stockistDocument.setStockist(stockist);counter++;
			}
			result.put("MSG", "Stockist Uploaded Document Details Updated Successfully...!");
			if(counter>0){
				result.put("MSG", "Stockist Uploaded Document Details Updation failed...!");
				if(stockistDao.updateStockistDocumentObj(stockistDocument)){
					result.put("RESULT", true);
					result.put("MSG", "Organization Uploaded Document Details Updated Successfully...!");
					result.put("DOCUMENT_TYPE_ID", stockistDocument.getDocumentListType().getDocumentListTypeId());
					result.put("DOCUMENT_TYPE", stockistDocument.getDocumentListType().getDocumentName());
					result.put("COMPANY_ID", stockistDocument.getCompany().getCompanyId());
					result.put("COMPANY_NAME", stockistDocument.getCompany().getCompanyName());
					result.put("STOCKIST_ID", stockistDocument.getStockist().getStockistId());
					result.put("STOCKIST_NAME", stockistDocument.getStockist().getStockistName());
				}
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#searchUploadedDocumentsDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray searchUploadedDocumentsDetails(String searchOption,
			String searchText, Integer organizationId, Integer from) {
		return stockistDao.searchUploadedDocumentsDetails(searchOption, searchText, organizationId,from);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#organizationListDropdown(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject organizationListDropdown(Integer organizationId) {
		return stockistDao.organizationListDropdown(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#deleteStockistDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteStockistDetails(Integer stockistId) {
		return stockistDao.deleteStockistDetails(stockistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#deleteStockistDocumentDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteStockistDocumentDetails(Integer stockistDocId) {
		// TODO Auto-generated method stub
		return stockistDao.deleteStockistDocumentDetails(stockistDocId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Stockist loadStockistObjectByStockistId(Integer stockistId) {
		return stockistDao.loadStockistObjectByStockistId(stockistId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<DrugLicence> loadDrugLicenseListUsingStockistId(Integer stockistId) {
		// TODO Auto-generated method stub
		return drugStockistDao.loadDrugLicenseListUsingStockistId(stockistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#getStockistCompanyList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray getStockistCompanyList(Integer stockistId) {
		// TODO Auto-generated method stub
		return stockistDao.getStockistCompanyList(stockistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#insertStockistDetilsAndDeleteOldStockistDetails(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.Stockist)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer insertStockistDetilsAndDeleteOldStockistDetails(Integer stockistId, Integer transporterDetailsId, Integer stateId, Integer districtId,Integer cityId, Stockist stockist) {
		return stockistDao.insertStockistDetilsAndDeleteOldStockistDetails(stockistId,transporterDetailsId, stateId, districtId, cityId, stockist);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#updateStockistDetails(com.protocol.wms.model.Stockist)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateStockistDetails(Stockist stockist) {
		return stockistDao.updateStockistDetails(stockist);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#getDrugLicenceObjectByDrugLicenceId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public DrugLicence getDrugLicenceObjectByDrugLicenceId(Integer drugLicenceId) {
		// TODO Auto-generated method stub
		return drugStockistDao.getDrugLicenceObjectByDrugLicenceId(drugLicenceId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#updateDrugLicenceDetails(com.protocol.wms.model.DrugLicence)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateDrugLicenceDetails(DrugLicence drugLicence) {
		// TODO Auto-generated method stub
		return drugStockistDao.updateDrugLicenceDetails(drugLicence);
	}
}
