/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.json.JSONObject;
import com.protocol.wms.model.City;
import com.protocol.wms.model.District;
import com.protocol.wms.model.State;

/**
 * @author admin
 *
 */
public interface MaintenanceService {
	public JSONObject saveStateForOrganization(Integer orgId,String stateName);
	
	public Boolean stateAlreadyExistsOrNot(Integer orgId, String stateName);
	
	public JSONObject saveDistrict(Integer stateId,String districtName);
	
	public JSONObject saveCity(Integer districtId,String cityName);
	
	public List<State> getStateList(Integer organizationId);
	
	public Boolean deleteStateDetails(Integer stateId);
	
	public JSONObject getDistrictList(Integer stateId);
	
	public Boolean deleteDistrictDetails(Integer districtId);
	
	public JSONObject getCityList(Integer districtId);
	
	public Boolean deleteCityDetails(Integer cityId);

	public boolean districtAlreadyExistsOrNot(Integer stateId,
			String districtName);

	public boolean cityAlreadyExistsOrNot(Integer districtId, String cityName);
	

	public List<State> searchOptionForState(String searchOption, String searchText);

	public List<District> searchOptionForDistrict(String searchOption, String searchText,Integer organizationId);

	public List<City> searchOptionForCity(String searchOption, String searchText,Integer organizationId);

}
