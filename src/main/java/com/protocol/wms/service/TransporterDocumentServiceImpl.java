/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.DocumentListTypeDao;
import com.protocol.wms.dao.TransporterDetailsDao;
import com.protocol.wms.dao.TransporterDocumentDao;
import com.protocol.wms.model.DocumentListType;
import com.protocol.wms.model.TransporterDetails;
import com.protocol.wms.model.TransporterDocument;
import com.protocol.wms.model.TransporterDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
@Service("TransporterDocumentServiceImpl")
public class TransporterDocumentServiceImpl  implements TransporterDocumentService{

	
	private TransporterDocumentDao transporterDocumentDao;

	@Autowired
	@Qualifier("TransporterDocumentDaoImpl")
	public void setTransporterDocumentDao(
			TransporterDocumentDao transporterDocumentDao) {
		this.transporterDocumentDao = transporterDocumentDao;
	}
	@Autowired
	@Qualifier("TransporterDetailsDaoImpl")
	private TransporterDetailsDao transporterDetailsDao;
	
	@Autowired
	@Qualifier("DocumentListTypeDaoImpl")
	private DocumentListTypeDao documentListTypeDao;
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDocumentService#saveTransporterDocument(com.protocol.wms.model.TransporterDocument)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveTransporterDocument(
			TransporterDocument transporterDocumentObj) {
		return transporterDocumentDao.saveTransporterDocument(transporterDocumentObj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject updateTransporterDocumentInfo(Integer documentTypeId, Integer transporterId, Integer transporterDocumentId) throws JSONException {
		JSONObject result= new JSONObject();
		TransporterDocument transporterDocument = null;
		TransporterDetails transporterDetails = null;
		DocumentListType documentListType = null;
		int counter = 0;
		{
			transporterDocument = transporterDocumentDao.loadTransporterDocumentObjById(transporterDocumentId);
			if(transporterDocument.getDocumentListType().getDocumentListTypeId()!=documentTypeId){
				documentListType = documentListTypeDao.loadDocumentListTypeUsingDocumentListTypeId(documentTypeId);
				transporterDocument.setDocumentListType(documentListType);counter++;
			}
			if(transporterDocument.getTransporterDetails().getTransporterDetailsId()!=transporterId){
				transporterDetails = transporterDetailsDao.loadTransporterDetailsUsingTransporterDetailsId(transporterId);
				transporterDocument.setTransporterDetails(transporterDetails);counter++;
			}
			result.put("MSG", "Transporter Uploaded Document Details Updated Successfully...!");
			if(counter>0){
				result.put("MSG", "Transporter Uploaded Document Details Updation failed...!");
				if(transporterDocumentDao.updateTransporterDocumentObj(transporterDocument)){
					result.put("RESULT", true);
					result.put("MSG", "Transporter Uploaded Document Details Updated Successfully...!");
					result.put("TRAN_ID",transporterDocument.getTransporterDetails().getTransporterDetailsId());
					result.put("TRAN_NAME",transporterDocument.getTransporterDetails().getTransporterName());
					result.put("DOC_TYPE_ID", transporterDocument.getDocumentListType().getDocumentListTypeId());
					result.put("DOC_TYPE", transporterDocument.getDocumentListType().getDocumentName());
				}
			}
			
		}
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateTransportUploadDocumentImage(Integer transportDocumentId,List<TransporterDocumentImagePath> documentList) throws Exception {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		documentList = transporterDocumentDao.updateTransportUploadDocumentImage(transportDocumentId, documentList);
		for(TransporterDocumentImagePath tempObj : documentList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getTransporterDocumentImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteTrnsporterDocumentFileImage(Integer transporterDocumentPathId) {
		return transporterDocumentDao.deleteTrnsporterDocumentFileImage(transporterDocumentPathId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDocumentService#loadTransporterDocumentListUsignOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray loadTransporterDocumentListUsignOrganizationId(
			Integer organizationId) throws JSONException {
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		{
			List<TransporterDocument> transporterDocumentList=transporterDocumentDao.loadTransporterDocumentListUsignOrganizationId(organizationId);
			if(transporterDocumentList.size()>0)
			{
				for(TransporterDocument obj : transporterDocumentList)
				{
					if("1".equals(obj.getOrganization().getOrganizationStatus())&&obj.getTransporterDetails().getStatus()==1){
					json=new JSONObject();
					json.put("TRAN_DOCUMENT_ID", obj.getTransporterDocumentId());
					json.put("ORG_ID", obj.getOrganization().getOrganizationId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("TRAN_ID",obj.getTransporterDetails().getTransporterDetailsId());
					json.put("TRAN_NAME",obj.getTransporterDetails().getTransporterName());
					json.put("DOC_TYPE_ID", obj.getDocumentListType().getDocumentListTypeId());
					json.put("DOC_TYPE", obj.getDocumentListType().getDocumentName());
					json_ImageArray=new JSONArray();
					for(TransporterDocumentImagePath tempObj : obj.getTransporterDocumentImagePathList()){
						tempJson = new JSONObject();
						tempJson.put("IMAGE_PATH", tempObj.getImagePath());
						tempJson.put("IMAGE_ID", tempObj.getTransporterDocumentImagePathId());
						json_ImageArray.put(tempJson);
					}
					json.put("FILE_NAME",json_ImageArray);
					json_data_array.put(json);
					}
				}
			}
		} 
		return json_data_array;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.TransporterDocumentService#searchTransporterUploadDocument(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray searchTransporterUploadDocument(
			String searchOption, String searchText, Integer organizationId, Integer from) throws JSONException {
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		java.util.Map<Object, Object>map = null;
		{
			map=transporterDocumentDao.searchTransporterUploadDocument(searchOption, searchText, organizationId,from);
			List<TransporterDocument> transporterDocumentList = (List<TransporterDocument>) map.get("transporterDocList");
			Integer totalPage = (Integer) map.get("totalPages");
			if(transporterDocumentList.size()>0)
			{
				for(TransporterDocument obj : transporterDocumentList)
				{
//					if("1".equals(obj.getOrganization().getOrganizationStatus())&&obj.getTransporterDetails().getStatus()==1){
					json=new JSONObject();
					json.put("TRAN_DOCUMENT_ID", obj.getTransporterDocumentId());
					json.put("ORG_ID", obj.getOrganization().getOrganizationId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("TRAN_ID",obj.getTransporterDetails().getTransporterDetailsId());
					json.put("TRAN_NAME",obj.getTransporterDetails().getTransporterName());
					json.put("DOC_TYPE_ID", obj.getDocumentListType().getDocumentListTypeId());
					json.put("DOC_TYPE", obj.getDocumentListType().getDocumentName());
					json_ImageArray=new JSONArray();
					for(TransporterDocumentImagePath tempObj : obj.getTransporterDocumentImagePathList()){
						tempJson = new JSONObject();
						tempJson.put("IMAGE_PATH", tempObj.getImagePath());
						tempJson.put("IMAGE_ID", tempObj.getTransporterDocumentImagePathId());
						json_ImageArray.put(tempJson);
					}
					json.put("FILE_NAME",json_ImageArray);
					json_data_array.put(json);
//					}
				}
			}
			json=new JSONObject();
			json.put("paginationCount", totalPage);
			json_data_array.put(json);

		} 
		return json_data_array;
	}
}
