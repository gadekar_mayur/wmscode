/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;


import com.protocol.wms.model.SubMenuAuthentication;

/**
 * @author admin
 *
 */
public interface SubMenuAuthenticationService {
	
	public List<SubMenuAuthentication> loadSubMenuUsingMenuAuthenticationId(Integer menuAuthenticationId);

}
