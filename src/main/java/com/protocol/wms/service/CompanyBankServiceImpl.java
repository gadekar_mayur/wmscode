/**
 * 
 */
package com.protocol.wms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CompanyBankDao;
import com.protocol.wms.model.CompanyBank;

/**
 * @author admin
 *
 */
@Service("CompanyBankServiceImpl")
public class CompanyBankServiceImpl implements CompanyBankService {

	@Autowired
	@Qualifier("CompanyBankDaoImpl")
	private CompanyBankDao companyBankDao;
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyBankService#loadCompanyBankUsignCompanyBankId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyBank loadCompanyBankUsignCompanyBankId(Integer companyBankId) {
		// TODO Auto-generated method stub
		return companyBankDao.loadCompanyBankUsignCompanyBankId(companyBankId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyBankService#updateCompanyBankDetails(com.protocol.wms.model.CompanyBank)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateCompanyBankDetails(CompanyBank companyBank) {
		// TODO Auto-generated method stub
		return companyBankDao.updateCompanyBankDetails(companyBank);
	}

}
