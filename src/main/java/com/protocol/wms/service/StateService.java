/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import com.protocol.wms.model.State;

/**
 * @author Sudhakar
 *
 */
public interface StateService {
	
	public List<State> loadStateList(Integer orgID);
	
	public State loadSateObjectUsingStateId(Integer stateId);

	public Boolean updateStateObj(State stateObj);

	

}
