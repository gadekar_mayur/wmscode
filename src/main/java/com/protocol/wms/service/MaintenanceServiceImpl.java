/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CityDao;
import com.protocol.wms.dao.DistrictDao;
import com.protocol.wms.dao.StateDao;
import com.protocol.wms.model.City;
import com.protocol.wms.model.District;
import com.protocol.wms.model.State;

/**
 * @author admin
 *
 */
@Service("MaintenanceServiceImpl")
public class MaintenanceServiceImpl implements MaintenanceService {

	/*@Autowired
	@Qualifier("MaintenanceDaoImpl")
	private MaintenanceDao maintenanceDao;*/
	
	@Autowired
	@Qualifier("StateDaoImpl")
	private StateDao stateDao;
	
	@Autowired
	@Qualifier("DistrictDaoImpl")
	private DistrictDao districtDao;
	
	@Autowired
	@Qualifier("CityDaoImpl")
	private CityDao cityDao;
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MaintenanceService#saveStateForOrganization(java.lang.Integer, java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject saveStateForOrganization(Integer orgId, String stateName) {
		return stateDao.saveStateForOrganization(orgId, stateName);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MaintenanceService#saveDistrict(java.lang.Integer, java.lang.Integer, java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject saveDistrict(Integer stateId,String districtName) {
		return districtDao.saveDistrict(stateId, districtName);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MaintenanceService#saveCity(java.lang.Integer, java.lang.Integer, java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject saveCity( Integer districtId,String cityName) {
		return cityDao.saveCity(districtId, cityName);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MaintenanceService#getStateList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<State> getStateList(Integer organizationId) {
		// TODO Auto-generated method stub
		return stateDao.getStateListByOrg(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MaintenanceService#deleteStateDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteStateDetails(Integer stateId) {
		return stateDao.deleteStateDetails(stateId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MaintenanceService#getDistrictList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getDistrictList(Integer stateId) {
		return districtDao.getDistrictList(stateId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MaintenanceService#deleteDistrictDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteDistrictDetails(Integer districtId) {
		return districtDao.deleteDistrictDetails(districtId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MaintenanceService#getCityList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getCityList(Integer districtId) {
		return cityDao.getCityList(districtId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MaintenanceService#deleteCityDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCityDetails(Integer cityId) {
		return cityDao.deleteCityDetails(cityId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean stateAlreadyExistsOrNot(Integer orgId, String stateName) {
		return stateDao.stateAlreadyExistsOrNot(orgId, stateName);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean districtAlreadyExistsOrNot(Integer stateId,
			String districtName) {
		return districtDao.districtAlreadyExistsOrNot(stateId,districtName);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean cityAlreadyExistsOrNot(Integer districtId, String cityName) {
		return cityDao.cityAlreadyExistsOrNot(districtId,cityName);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<State> searchOptionForState(String searchOption,
			String searchText) {
		// TODO Auto-generated method stub
		return stateDao.searchOptionForState(searchOption, searchText);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<District> searchOptionForDistrict(String searchOption,
			String searchText,Integer organizationId) {
		// TODO Auto-generated method stub
		return districtDao.searchOptionForDistrict(searchOption, searchText, organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<City> searchOptionForCity(String searchOption,
			String searchText, Integer organizationId) {
		// TODO Auto-generated method stub
		return cityDao.searchOptionForCity(searchOption, searchText, organizationId);
	}
	
	
	
}
