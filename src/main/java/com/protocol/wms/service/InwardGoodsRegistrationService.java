/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyDepo;
import com.protocol.wms.model.InwardGoodsRegistration;
import com.protocol.wms.model.InwardGoodsRegistrationClaimLetter;
import com.protocol.wms.model.InwardGoodsRegistrationClaimLetterImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationClaimPicturesImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails;
import com.protocol.wms.model.InwardGoodsRegistrationInvoiceStnNoImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationLRImgPath;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.TransporterDetails;

/**
 * @author Sudhakar
 *
 */
public interface InwardGoodsRegistrationService {

	public List<CompanyDepo> loadCompanyDepoListUsignCompanyIdAndOrgId(Integer orgId,Integer companyId);
	
	public Integer saveInwardGoodsRegistration(InwardGoodsRegistration inwardGoodsRegistrationObj);
	
	public JSONObject editInwardGoodsRegistration(Integer organizationId,Integer comapnyId,Integer sendingDepo,Integer transporterId,InwardGoodsRegistration inwardGoodsRegistrationObj) throws Exception;
	
	public Organization loadOrganizationObjectUsingOrganiztionId(Integer organizationId);
	
	public Company loadCompanyObjectUsingCompanyId(Integer companyId);
	
	public TransporterDetails loadTransporterDetailsUsingTransporterDetailsId(Integer TransporterDetailsId);
	
	public CompanyDepo loadCompanyDepoObjectUsignCompanyDepoId(Integer companyDepoId);
	
	public InwardGoodsRegistration loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(Integer inwardGoodsRegistrationId);
	
	public Integer saveInwardGoodsRegistrationInvoiceDetails(InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetailsObj);
	
	public List<InwardGoodsRegistration> loadInwardGoodsRegistrationListUsignOrganizationId(Integer orgId);
	
	public Map<Object, Object> searchInwardGoodsRegDetails(String selectedValue,String searchText,String fromSpecialData,String toSpecialData,Integer organizationId, Integer from);
	
	public List<InwardGoodsRegistrationInvoiceDetails> loadInwardGoodsRegistrationInvoiceDetailsListUsignInwardGoodsRegistrationId(Integer inwardGoodsRegistrationId);
	
	public Integer saveInwardGoodsRegistrationClaimLetter(InwardGoodsRegistrationClaimLetter  inwardGoodsRegistrationClaimLetterObj);
	
	public void updateInwardGoodsRegistrationObject(InwardGoodsRegistration inwardGoodsRegistrationObj);
	
	public Boolean deleteInwardGoodsRegistrationDetails(Integer inwardGoodsRegistrationId);
	
	public Boolean deleteInwardGoodsRegistrationLrImage(Integer lrImageId);
	
	public Boolean deleteInwardGoodsRegistrationClaimLetterImage(Integer claimLetterImageId);
	
	public Boolean deleteInwardGoodsRegistrationClaimPictureImage(Integer claimPictureImageId);
	
	public Boolean deleteInwardGoodsRegInvoiceStnImage(Integer stnImageId);
	
	public InwardGoodsRegistrationInvoiceDetails getInwardGoodsRegistrationInvoiceDetailsObjById(Integer inwardGoodsRegistrationInvoiceDetailsId);
	
	public Boolean editInwardGoodsRegistrationInvoiceDetails(InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetails);
	
	public InwardGoodsRegistrationClaimLetter getInwardGoodsRegistrationClaimLetterObj(Integer inwardGoodsRegistrationClaimLetterId);
	
	public Boolean editInwardGoodsRegistrationClaimLetter(InwardGoodsRegistrationClaimLetter claimObj);
	
	public JSONArray inwardGoodsRegistrationView(Integer inwardGoodsRegistrationId) throws Exception;
	
	public JSONArray updateInwardGoodsRegLrImages(Integer inwardGoodsRegistrationId,List<InwardGoodsRegistrationLRImgPath> lrImgList) throws Exception;
	
	public JSONArray updateInwardGoodsRegInvoiceStnImage(Integer inwardGoodsRegistrationInvoiceId, List<InwardGoodsRegistrationInvoiceStnNoImagePath> stnImgList) throws Exception;
	
	public JSONArray updateInwardGoodsRegClaimLetterImage(Integer inwardGoodsRegistrationClaimLetterId, List<InwardGoodsRegistrationClaimLetterImagePath> claimLetterImgList) throws Exception;
	
	public JSONArray updateInwardGoodsRegClaimPicturesImage(Integer inwardGoodsRegistrationClaimLetterId,List<InwardGoodsRegistrationClaimPicturesImagePath> claimPicturesImgList) throws Exception;
}
