/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.dao.EmployeeDetailsDao;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.EmployeeDetailsAddressProofPath;
import com.protocol.wms.model.EmployeeDetailsImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationLRImgPath;

/**
 * @author Sudhakar
 *
 */
@Service("EmployeeDetailsServiceImpl")
public class EmployeeDetailsServiceImpl implements EmployeeDetailsService {
	
	private EmployeeDetailsDao employeeDetailsDao;

	@Autowired
	@Qualifier("EmployeeDetailsDaoImpl")
	public void setEmployeeDetailsDao(EmployeeDetailsDao employeeDetailsDao) {
		this.employeeDetailsDao = employeeDetailsDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDetailsService#saveEmployeeDetails(com.protocol.wms.model.EmployeeDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveEmployeeDetails(EmployeeDetails employeeDetailsObj) {
		return employeeDetailsDao.saveEmployeeDetails(employeeDetailsObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDetailsService#loadEmployeeObjectUsingEmployeeDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public EmployeeDetails loadEmployeeObjectUsingEmployeeDetailsId(
			Integer empDetailsId) {
		return employeeDetailsDao.loadEmployeeObjectUsingEmployeeDetailsId(empDetailsId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray viewEmployeeDetailsUsingEmployeeDetailsId(Integer empDetailsId, Boolean editStatus) throws JSONException {
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		{
			EmployeeDetails empObj = employeeDetailsDao.loadEmployeeObjectUsingEmployeeDetailsId(empDetailsId);
						json=new JSONObject();
						json.put("ORG_NAME", empObj.getOrganization().getOrganizationName());
						json.put("COMP_NAME", empObj.getCompany().getCompanyName());
						json.put("EMP_NAME", empObj.getName());
						json.put("MARRITAL_STATUS", empObj.getMaritalStats());
						json.put("MOBILE", empObj.getMobileNo());
						json.put("CURR_ADDR",empObj.getCurrentAddress());
						json.put("PER_ADDR", empObj.getPermanetAddress());
						CommonMethods common = new CommonMethods();
						json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(empObj.getSubmitDate().toString()));
						String [] reg_date_arr;
						reg_date_arr =  empObj.getDOB().toString().split("-");
						json.put("DOB", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
						//json.put("DOB", empObj.getDOB());
						json_ImageArray=new JSONArray();
						for(EmployeeDetailsImagePath tempObj : empObj.getEmployeeDetailsImagePathList()){
							tempJson = new JSONObject();
							tempJson.put("IMAGE_PATH", tempObj.getImagePath());
							tempJson.put("IMAGE_ID", tempObj.getEmployeeDetailsImagePathId());
							json_ImageArray.put(tempJson);
						}
						json.put("IMAGE", json_ImageArray);
						json.put("USER_NAME", empObj.getUserDetails().getUserName());
						json.put("GENDER", empObj.getGender());
						reg_date_arr =  empObj.getDOJ().toString().split("-");
						json.put("DOJ", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
						//json.put("DOJ", empObj.getDOJ());
						json.put("QUALIFICATION", empObj.getQualification());
						if(empObj.getTelephone()==null)
						{
						json.put("TELEPHONE", "");
						}else{
							json.put("TELEPHONE", empObj.getTelephone());
						}
						System.out.println("Email : "+empObj.getEmailId());
						if("".equals(empObj.getEmailId())||null==empObj.getEmailId())
						{
							json.put("EMAIL", "");
						}else{
							json.put("EMAIL", empObj.getEmailId());
						}
						
						json.put("PIN_CODE", empObj.getPinCode());
						System.out.println("PAN : "+empObj.getPanNo());
						if(empObj.getPanNo()==null || ("".equals(empObj.getPanNo())))
						{
						json.put("PAN_NUM", "");
						}else{
							json.put("PAN_NUM", empObj.getPanNo());
						}
						if(empObj.getBasicSalary()==null)
						{
							json.put("BASIC_SALARY", "");
						}else{
							json.put("BASIC_SALARY", empObj.getBasicSalary());
						}
						json_ImageArray=new JSONArray();
						for(EmployeeDetailsAddressProofPath tempObj : empObj.getEmployeeDetailsAddressProofPathList()){
							tempJson = new JSONObject();
							tempJson.put("IMAGE_PATH", tempObj.getImagePath());
							tempJson.put("IMAGE_ID", tempObj.getEmployeeDetailsAddressProofPathId());
							json_ImageArray.put(tempJson);
						}
						json.put("ADDR_PROOF", json_ImageArray);
						if(editStatus!=null&&editStatus==true){
							json.put("PASSWORD", empObj.getUserDetails().getPassword());
							json.put("ROLE_ID", empObj.getRoleType().getRoleTypeId());
							json.put("BLOOD_GROUP_ID", empObj.getBloodGroup().getBloodGroupId());
						}else{
							json.put("BLOOD_GROUP", empObj.getBloodGroup().getBloodGroup());
							json.put("ROLE", empObj.getRoleType().getRoleName());
						}
						json_data_array.put(json);
					
		}
		return json_data_array;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDetailsService#updateEmployeeDetails(com.protocol.wms.model.EmployeeDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateEmployeeDetails(EmployeeDetails empObject) {
		employeeDetailsDao.updateEmployeeDetails(empObject);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDetailsService#updateEmployeeDetails(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.EmployeeDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateEmployeeDetails(Integer roleTypeId,Integer bloodroupNameId, EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		return employeeDetailsDao.updateEmployeeDetails(roleTypeId, bloodroupNameId, employeeDetails);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateEmployeeImages(Integer employeeDetailsId,List<EmployeeDetailsImagePath> employeeImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		employeeImgList = employeeDetailsDao.updateEmployeeImages(employeeDetailsId, employeeImgList);
		for(EmployeeDetailsImagePath tempObj : employeeImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getEmployeeDetailsImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteEmployeeImage(Integer employeeImageId) {
		return employeeDetailsDao.deleteEmployeeImage(employeeImageId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateEmployeeAddressProofImage(Integer employeeDetailsId, List<EmployeeDetailsAddressProofPath> employeeAddressProofPathList)
			throws Exception {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		employeeAddressProofPathList = employeeDetailsDao.updateEmployeeAddressProofImage(employeeDetailsId, employeeAddressProofPathList);
		for(EmployeeDetailsAddressProofPath tempObj : employeeAddressProofPathList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getEmployeeDetailsAddressProofPathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteEmployeeAddressProofImage(Integer employeeAddressProofImageId) {
		return employeeDetailsDao.deleteEmployeeAddressProofImage(employeeAddressProofImageId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDetailsService#loadEmployeeDetailsListUsingOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<EmployeeDetails> loadEmployeeDetailsListUsingOrganizationId(
			Integer organizationId) {
		return employeeDetailsDao.loadEmployeeDetailsListUsingOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDetailsService#searchEmployeeDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object, Object> searchEmployeeDetails(String searchOption,
			String searchText, Integer organizationId,Integer from) {
		return employeeDetailsDao.searchEmployeeDetails(searchOption, searchText, organizationId,from);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.EmployeeDetailsService#deleteEmployeeDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteEmployeeDetails(Integer empDetailsId) {
		// TODO Auto-generated method stub
		return employeeDetailsDao.deleteEmployeeDetails(empDetailsId);
	}

}
