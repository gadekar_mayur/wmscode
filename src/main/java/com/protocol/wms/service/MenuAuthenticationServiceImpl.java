/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.MenuAuthenticationDao;
import com.protocol.wms.model.MenuAuthentication;

/**
 * @author admin
 *
 */
@Service("MenuAuthenticationServiceImpl")
public class MenuAuthenticationServiceImpl implements MenuAuthenticationService{
	
	private MenuAuthenticationDao menuAuthenticationDao;

	/**
	 * @param menuAuthenticationDao the menuAuthenticationDao to set
	 */
	@Autowired
	@Qualifier("MenuAuthenticationDaoImpl")
	public void setMenuAuthenticationDao(MenuAuthenticationDao menuAuthenticationDao) {
		this.menuAuthenticationDao = menuAuthenticationDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.MenuAuthenticationService#loadMainMenuUsingUserTypeId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<MenuAuthentication> loadMainMenuUsingUserTypeId(
			Integer userTypeId) {
		// TODO Auto-generated method stub
		return menuAuthenticationDao.loadMainMenuUsingUserTypeId(userTypeId);
	}

}
