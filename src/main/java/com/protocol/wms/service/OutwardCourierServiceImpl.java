/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.OutWardChequeNumberInformationDao;
import com.protocol.wms.dao.OutwardCourierDao;
import com.protocol.wms.model.OutWardAdvancedChequeInformation;
import com.protocol.wms.model.OutWardChequeNumberInformation;
import com.protocol.wms.model.OutWardCourierRegistration;

/**
 * @author admin
 *
 */
@Service("OutwardCourierServiceImpl")
public class OutwardCourierServiceImpl implements OutwardCourierService {

	@Autowired
	@Qualifier("OutwardCourierDaoImpl")
	private OutwardCourierDao outwardCourierDao;
	
	
	@Autowired
	@Qualifier("OutWardChequeNumberInformationDaoImpl")
	private OutWardChequeNumberInformationDao outWardChequeNumberInformationDao;
	
//	@Autowired
//	@Qualifier("AdvancedChequeInformationDaoImpl")
//	private AdvancedChequeInformationDao advancedChequeInformationDao;
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardCourierService#saveOutwardCourierRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.OutWardCourierRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveOutwardCourierRegistration(String companyStockistOption,Integer orgId,Integer companyId, Integer depoId, Integer particularId,Integer stockistId,Integer companyBankId,OutWardCourierRegistration outWardCourierRegistration,OutWardAdvancedChequeInformation advancedChequeInfo,List<OutWardChequeNumberInformation> chequeNoInfoList) {
		// TODO Auto-generated method stub
		return outwardCourierDao.saveOutwardCourierRegistration(companyStockistOption, orgId, companyId, depoId,  particularId,stockistId,companyBankId, outWardCourierRegistration,advancedChequeInfo,chequeNoInfoList);
	}

	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardCourierService#editOutwardCourierRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.OutWardCourierRegistration, com.protocol.wms.model.OutWardAdvancedChequeInformation, java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject editOutwardCourierRegistration(Integer orgId,
			Integer companyId, Integer depoId, Integer stateId,
			Integer districtId, Integer cityId, Integer particularId,
			Integer stockistId, Integer companyBankId,
			OutWardCourierRegistration outWardCourierRegistration,
			OutWardAdvancedChequeInformation advancedChequeInfo,
			List<OutWardChequeNumberInformation> chequeNoInfoList) throws JSONException {
		JSONObject result = null;
		result = new JSONObject();
		result.put("MSG", "Outward Courier Registration Details Updation Failed....!");
		if(outwardCourierDao.editOutwardCourierRegistration(orgId, companyId, depoId, stateId, districtId, cityId, particularId, stockistId, companyBankId, outWardCourierRegistration, advancedChequeInfo, chequeNoInfoList)){
			result.put("COURIER_NAME",outWardCourierRegistration.getCourierName());
			result.put("REG_DATE",outWardCourierRegistration.getCourierRegistrationDate());
			result.put("COMPANY",outWardCourierRegistration.getCompany().getCompanyName());
			String stocistName="-";
			if(outWardCourierRegistration.getStockist()!=null)
				stocistName=outWardCourierRegistration.getStockist().getStockistName();
			result.put("STOCKIST_NAME",stocistName);
			if(outWardCourierRegistration.getCity()!=null)
			result.put("CITY",outWardCourierRegistration.getCity().getCityName());
			else
				result.put("CITY","");
			result.put("DOCKET_NO",outWardCourierRegistration.getDocketNo());
			result.put("MSG", "Outward Courier Registration Details Updated Successfully....!");
			result.put("RESULT", true);
		}
		return result;
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardCourierService#outwardCourierRegistrationListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardCourierRegistration> outwardCourierRegistrationListing(Integer organizationId) {
		// TODO Auto-generated method stub
		return outwardCourierDao.outwardCourierRegistrationListing(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardCourierService#deleteOutwardCourierRegistration(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardCourierRegistration(Integer outwardCourierRegId) {
		// TODO Auto-generated method stub
		return outwardCourierDao.deleteOutwardCourierRegistration(outwardCourierRegId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardCourierRegistration loadOutwardCourierRegistrationObjectUsingoutwardCourierRegId(
			Integer outwardCourierRegId) {
		return outwardCourierDao.loadOutwardCourierRegistrationObjectUsingoutwardCourierRegId(outwardCourierRegId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardChequeNumberInformation> loadChequeInformationUsigngetOutWardAdvancedChequeInformationId(
			Integer outWardAdvancedChequeInformationId) {
			return outWardChequeNumberInformationDao.loadChequeInformationUsigngetOutWardAdvancedChequeInformationId(outWardAdvancedChequeInformationId);
		}


	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object,Object> searchoutwardCourierRegistration(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from)
 {
		// TODO Auto-generated method stub
		return outwardCourierDao.searchoutwardCourierRegistration(selectedValue, searchText, fromSpecialData,
				 toSpecialData,  organizationId,from);
	}


}
