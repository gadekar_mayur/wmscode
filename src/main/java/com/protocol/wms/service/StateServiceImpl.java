/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.StateDao;
import com.protocol.wms.model.State;

/**
 * @author Sudhakar
 *
 */
@Service("StateServiceImpl")
public class StateServiceImpl implements StateService{

	
	private StateDao stateDao;

	@Autowired
	@Qualifier("StateDaoImpl")
	public void setStateDao(StateDao stateDao) {
		this.stateDao = stateDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StateService#loadStateList()
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<State> loadStateList(Integer orgId) {
		return stateDao.loadStateList(orgId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StateService#loadSateObjectUsingStateId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public State loadSateObjectUsingStateId(Integer stateId) {
		return stateDao.loadSateObjectUsingStateId(stateId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateStateObj(State stateObj) {
		
		return stateDao.updateStateObj(stateObj);
		
	}
	
	
}
