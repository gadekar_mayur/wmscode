/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.SubMenuAuthenticationDao;
import com.protocol.wms.model.SubMenuAuthentication;

/**
 * @author admin
 *
 */
@Service("SubMenuAuthenticationServiceImpl")
public  class SubMenuAuthenticationServiceImpl implements SubMenuAuthenticationService{

	private SubMenuAuthenticationDao subMenuAuthenticationDao;

	/**
	 * @param subMenuAuthenticationDao the subMenuAuthenticationDao to set
	 */
	@Autowired
	@Qualifier("SubMenuAuthenticationDaoImpl")
	public void setSubMenuAuthenticationDao(
			SubMenuAuthenticationDao subMenuAuthenticationDao) {
		this.subMenuAuthenticationDao = subMenuAuthenticationDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.SubMenuAuthenticationService#loadSubMenuUsingMenuAuthenticationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<SubMenuAuthentication> loadSubMenuUsingMenuAuthenticationId(
			Integer menuAuthenticationId) {
		return subMenuAuthenticationDao.loadSubMenuUsingMenuAuthenticationId(menuAuthenticationId);
	}
	
	
}
