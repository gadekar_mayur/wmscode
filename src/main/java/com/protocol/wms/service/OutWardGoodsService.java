/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentTripCount;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.CompanyProduct;
import com.protocol.wms.model.InvoicePaymentInformation;
import com.protocol.wms.model.OrderMode;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationTripCount;
import com.protocol.wms.model.OutWardDispatchEnteryRegistration;
import com.protocol.wms.model.OutWardGatePass;
import com.protocol.wms.model.OutWardGatePassLrImagePath;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistrationOrderCopyPath;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.OutWardProductOrderEntryRegistration;
import com.protocol.wms.model.OutWardTrip;
import com.protocol.wms.model.OutWardTripTransporterDetails;
import com.protocol.wms.model.OutwardTripReport;
import com.protocol.wms.model.PrintSticker;
import com.protocol.wms.model.RemainingChequeAmount;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistBankDetails;
import com.protocol.wms.model.TransporterDetails;

/**
 * @author Sudhakar
 *
 */
public interface OutWardGoodsService {
	public Boolean checkName(String orderNo,Integer companyId) throws Exception;
	
	
	public List<CompanyProduct> loadCompanyProductListUsignCompanyId(Integer companyId);

	public Organization loadOrganizationObjectUsingOrganiztionId(Integer organizationId);
	
	public Company loadCompanyObjectUsingCompanyId(Integer companyId);
	
	public Stockist loadStockistObjectByStockistId(Integer stockistId);
	
	public OrderMode loadOrderModeObjectUsignOrderModeId(Integer orderModeId);
	
	public Integer saveOutWardOrderEntryRegistration(OutWardOrderEntryRegistration obj);
	
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsZero(Integer organizationId);
	
	public OutWardOrderEntryRegistration loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(Integer OutWardOrderEntryRegistrationId);
	
	public JSONArray viewAllDetailsOfOutwardGoodsRegistration(Integer outWardOrderEntryRegistrationId ) throws Exception;
	
	public void updateOutWardOrderEntryRegistration(OutWardOrderEntryRegistration obj);
	
	public Integer saveOutWardProductOrderEnteryRegistration(OutWardProductOrderEntryRegistration outWardProductOrderEnteryRegistrationObj);
	
	public CompanyProduct loadCompanyProductObjectUsignCompanyProductId(Integer companyProductId);
	
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsOne(Integer organizationId);
	
	public CompanyCreditAndDiscount loadCompanyCreditAndDiscountObjectUsignComapnyId(Integer companyId);
	
	public Integer saveOutWardOrderInvoiceEnteryRegistrationObject(OutWardOrderInvoiceEnteryRegistration obj);
	
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsOneAndInvoiceStatusIsOne(Integer organizationId);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero(Integer organizationId);
	
	public OutWardOrderInvoiceEnteryRegistration loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(Integer OutWardOrderInvoiceEnteryRegistrationId);
	
	public Integer saveOutWardDispatchEnteryRegistrationObj(OutWardDispatchEnteryRegistration obj);
	
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(Integer outWardDispatchEnteryRegistrationId);
	
	public void updateOutWardOrderInvoiceEnteryRegistrationObj(OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj);
	
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfTripStatusZero(Integer organizationId);
	
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOne(Integer organizationId);
	
	public TransporterDetails loadTransporterDetailsUsingTransporterDetailsId(Integer TransporterDetailsId);
	
	public Integer saveOutWardTripObj(OutWardTrip obj);
	
	public OutWardTrip loadOutWardTripObjUsignOutWardTripiD(Integer outWardTripId);
	
	public void updateOutWardDispatchEnteryRegistrationObj(OutWardDispatchEnteryRegistration OutWardDispatchEnteryRegistrationObj);
	
	public List<OutWardTrip> loadOutWardTripListOfStatusOneUsignOrganizationId(Integer organizationId);
	
	public List<OutWardTrip> loadOutWardTripListOfGetPassStatusZeroAndStatusOneUsignOrganizationId(Integer organizationId);
	
	public Integer saveOutWardGatePass(OutWardGatePass OutWardGatePassObj);
	
	public OutWardGatePass loadOutWardGatePassObjUsignOutWardGatePassId(Integer outWardGatePassId);
	
	public void updateOutWardTripObj(OutWardTrip obj);
	
	public List<OutWardTrip> loadloadOutWardTripListOfGetPassStatusOneUsignOrganizationId(Integer organizationId);
	
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfStatusIsOne(Integer organizationId);
	
	public List<RemainingChequeAmount> loadremainingChequeAmountListUsignOrgIdAndCompIdAndStockistId(Integer orgId,Integer companyId,Integer stockistId);
	
	public ChequeNumberInfo loadChequeNumberInfoObjUsignChequeNumberInfoId(Integer ChequeNumberInfoId);
	
	public Integer saveInvoicePaymentInformationObj(InvoicePaymentInformation InvoicePaymentInformationObj);
	
	public RemainingChequeAmount loadRemainingChequeAmountUsignRemainingChequeAmountId(Integer RemainingChequeAmountId);
	
	public void updateRemainingChequeAmountObj(RemainingChequeAmount remainingChequeAmountObj);
	
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsigOrgIdAndStockistIdAndCompanyId(Integer orgId,Integer stokistId,Integer companyId);
	
	public OrganizationTripCount loadOrganizationTripCountObjUsignOrgId(Integer orgId);
	
	public CartingAgentTripCount loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(Integer orgId,Integer cartingAgentId);
	
	public Integer saveOutWardTripTransporterDetailsObj(OutWardTripTransporterDetails outWardTripTransporterDetailsObj);
	
	public void updateOrganizationTripCount(OrganizationTripCount organizationTripCountObj);
	
	public void updateCartingAgentTripCountObj(CartingAgentTripCount cartingAgentTripCountObj);
	
	public Integer saveOrganizationTripCountObj(OrganizationTripCount organizationTripCountObj);
	
	public Integer saveCartingAgentTripCountObj(CartingAgentTripCount cartingAgentTripCountObj);
	
	public CartingAgent getCartingAgentDetailsById(Integer agentId);
	
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOneAndTripStatusOne(Integer organizationId);
	
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(Integer organizationId);
	
	public JSONArray getOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(Integer organizationId) throws Exception;
	
	public List<OutWardProductOrderEntryRegistration> getProductOrderEnteryRegistrationDetailsUsingOutWardOrderEntryRegistrationId(Integer OutWardOrderEntryRegistrationId);

	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistrationUsignoutWardOrderEnteryRegistrationId(Integer outWardOrderEnteryRegistrationId);
	
	public List<OutWardTripTransporterDetails> loadOutWardTripTransporterDetailsListUsignOutWardTripId(Integer outWardTripId);

	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(Integer outWardDispatchEnteryRegistrationId);

	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(Integer outwardTripId);
	
	public Integer saveOutwardTripReport(OutwardTripReport outwardTripReportObj);
	
	public List<OutwardTripReport> loadOutwardTripReportListUsignOutWardTripId(Integer outWardTripId);
	
	public void deletePrintedOutwardTripReportObj(List<OutwardTripReport> printedOutwardTripReportObjList);
	
	public Integer savePrintSticker(PrintSticker printStickerObj);
	
	public PrintSticker loadPrintStickerObjUsingOutWardDispatchEnteryRegistrationId(Integer outWardDispatchEnteryRegistrationId);
	
	public void deletePrintStickerObj(PrintSticker printStickerObj);
	
	public boolean updateOutWardOrderEntryRegistrationObj(OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj,Integer orderModeId);
	
	public OutWardProductOrderEntryRegistration loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(Integer outWardProductOrderEntryRegistrationId);
	
	public boolean updateOutWardProductOrderEntryRegistrationObj(OutWardProductOrderEntryRegistration OutWardProductOrderEntryRegistrationObj);
	
	public Boolean updateOutWardTripDetails(Integer selectcartingAgentId, OutWardTrip outWardTripObj) throws Exception;
	
	public boolean updateOutWardGatePassObj(OutWardGatePass outWardGatePassObj);
	
	public boolean updateOutWardDispatchEnteryRegistrationObjDetails(Integer outWardDispatchEnteryRegistrationId,Integer noOfCases);
	
	public boolean updateInvoiceObj(OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj,String invoiceDate,String invoiceDiscountTakentOrNot,Double netAmount);

	public Map<String,Object>searchOutwardOrderEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);
	
	public boolean updateOutWardDispatchObjOfOutWardGetPassStatusZero(OutWardGatePass outWardGatePassObj);
	
	public boolean deleteLrDetailsObj(OutWardGatePass outWardGatePassObj);
	
	public boolean updateOutWardDispatchObjOfOutWardTripStatusZero(OutWardTrip outWardTripObj);
	
	public boolean deleteOutWardTripEntryObj(OutWardTrip outWardTripObj);
	
	public boolean updateOutWardOrderEntryRegistrationObjOfProductStatusZero(OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj);
	
	public boolean deleteOutWardProductOrderEntryRegistrationObj(OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj);
	
	public List<InvoicePaymentInformation> loadinInvoicePaymentInformationsListUsingOutWardOrderInvoiceEnteryRegistrationId(Integer outWardOrderInvoiceEnteryRegistrationId);
	
	public boolean deleteOutWardOrderInvoiceEnteryRegistrationObj(OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj);
	
	public RemainingChequeAmount loadRemainingChequeAmountObjUsignChequeNumberInfoId(Integer chequeNumberInfoId);
	
	public Stockist loadStockistObjUsigStokistId(Integer stokistId);
	
	public StockistBankDetails loadStockistBankDetailsUsignStockistBankDetailsId(Integer stockistBankDetailsId);
	
	public Integer saveRemainingChequeAmountObj(RemainingChequeAmount RemainingChequeAmountObj);
	
	public boolean changeDispatchEntryRegistrationStatusToZero(Integer outWardDispatchEntryRegistarionId);
	
	public boolean deleteOutWardDispatchEnteryRegistrationObjDetails(OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj);
	
	public JSONArray updateOutwardGoodsRegOrderCopyImage(Integer outwardGoodsRegistrationId, List<OutWardOrderEntryRegistrationOrderCopyPath> orderCopyImgList) throws Exception;
	
	public Boolean deleteOutwardGoodsRegistrationOrderCopyImage(Integer orderCopyImageId);

	public JSONArray updateOutwardGoodsGatePassLrImage(Integer outwardGatePassId, List<OutWardGatePassLrImagePath> lrImgList) throws Exception;
	
	public Boolean deleteOutwardGoodsGatePassLrImage(Integer lrImageId);

	public Map<String,Object>searchOutwardViewOrderEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer from,Integer organizationId);

	public Map<String,Object>searchOutwardinvoiceEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);

	public Map<String,Object>searchOutwardInvoiceEntryAddedRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);

	public Map<String, Object> searchOutwardDispatchEntryRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);

	public Map<String,Object>searchOutwardAddTrip(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);

	public Map<String, Object> searchOutwardViewDispatchEntryRegistrationList(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);
	
	public Map<String, Object> searchOutwardViewDispatchEntryPrintStickerList(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);

	public Map<String,Object> searchOutwardViewTripList(String selectedValue,
			String searchText, String fromSpecialData, String toSpecialData,Integer from,
			Integer organizationId);

	public Map<String,Object>searchOutwardGetPassList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);

	public JSONArray searchViewGetPassLRList(String selectedValue,
			String searchText, String fromSpecialData, String toSpecialData,Integer from,
			Integer organizationId);
	
	public boolean deleteOutWardOrderEntry(OutWardOrderEntryRegistration outWardOrderEntryRegistration);
	
	public List<OutWardOrderEntryRegistrationOrderCopyPath> loadImages(Integer outWardOrderEnteryRegistrationId);
}
