package com.protocol.wms.service;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.StockistBankDetailsDao;
import com.protocol.wms.model.StockistBankDetails;

@Service("StockistBankDetailsServiceImpl")
public class StockistBankDetailsServiceImpl implements	StockistBankDetailsService {

	@Autowired
	@Qualifier("StockistBankDetailsDaoImpl")
	private StockistBankDetailsDao stockistBankDetailDao;
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject displayStockistBankDetailsForm() {
		// TODO Auto-generated method stub
		return stockistBankDetailDao.displayStockistBankDetailsForm();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject saveStockistBankDetails(Integer orgId,Integer stockistId, Integer companyId,StockistBankDetails bankDetatils) {
		// TODO Auto-generated method stub
		return stockistBankDetailDao.saveStockistBankDetails(orgId,stockistId,companyId,bankDetatils);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistBankDetailsService#stockistBankDetailsList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject stockistBankDetailsList(Integer organizationId) {
		// TODO Auto-generated method stub
		return stockistBankDetailDao.stockistBankDetailsList(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistBankDetailsService#searchStokistBankDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject searchStokistBankDetails(String searchOption,String searchText, Integer organizationId,Integer from) {
		// TODO Auto-generated method stub
		return stockistBankDetailDao.searchStokistBankDetails(searchOption, searchText, organizationId,from);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistBankDetailsService#stockistBankDropdownList(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<StockistBankDetails> stockistBankDropdownList(Integer organizationId, Integer companyId, Integer stockistId) {
		// TODO Auto-generated method stub
		return stockistBankDetailDao.stockistBankDropdownList(organizationId,companyId,stockistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistService#deleteStockistBankDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteStockistBankDetails(Integer stockistBankId) {
		return stockistBankDetailDao.deleteStockistBankDetails(stockistBankId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistBankDetailsService#loadStockistBankDetailsUsignStockistBankId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public StockistBankDetails loadStockistBankDetailsUsignStockistBankDetailsId(Integer stockistBankDetailsId) {
		return stockistBankDetailDao.loadStockistBankDetailsUsignStockistBankDetailsId(stockistBankDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.StockistBankDetailsService#updateStockistBankDetails(com.protocol.wms.model.StockistBankDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateStockistBankDetails(StockistBankDetails stockistBankDetails) {
		// TODO Auto-generated method stub
		return stockistBankDetailDao.updateStockistBankDetails(stockistBankDetails);
	}
	
}
