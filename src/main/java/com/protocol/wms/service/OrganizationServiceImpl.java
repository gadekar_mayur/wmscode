/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;




import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.OrganizationDao;
import com.protocol.wms.model.Organization;



/**
 * @author admin
 *
 */
@Service("OrganizationServiceImpl")
public class OrganizationServiceImpl implements OrganizationService{

	
	private OrganizationDao organizationDao;
	
	
	@Autowired
	@Qualifier("OrganizationDaoImpl")
	public void setOrganizationDao(OrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationService#saveOrganization(com.protocol.wms.model.Organization)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveOrganization(Organization organizationObj) {
		return organizationDao.saveOrganization(organizationObj);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationService#editOrganizationDetails(com.protocol.wms.model.Organization)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean editOrganizationDetails(Organization organizationObj) {
		// TODO Auto-generated method stub
		return organizationDao.editOrganizationDetails(organizationObj);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationService#updateOrganization(com.protocol.wms.model.Organization)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateOrganization(Organization organizationObj) {
		organizationDao.updateOrganization(organizationObj);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationService#loadOrganizationObjectUsingOrganiztionId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Organization loadOrganizationObjectUsingOrganiztionId(
			Integer organizationId) {
		return organizationDao.loadOrganizationObjectUsingOrganiztionId(organizationId);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationService#loadListOfOrganization()
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Organization> loadListOfOrganization() {
		return organizationDao.loadListOfOrganization();
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationService#searchOrganizationDetails(java.lang.String, java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String , Object> searchOrganizationDetails(String searchOption,
			String searchText, Integer from) {
		return organizationDao.searchOrganizationDetails(searchOption, searchText, from);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationService#loasOrganiztionObjectUsingUserDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Organization loadOrganiztionObjectUsingUserDetailsId(
			Integer userDetailsId) {
		return organizationDao.loadOrganiztionObjectUsingUserDetailsId(userDetailsId);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationService#deleteOrganiztion(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOrganiztion(Integer organizationId) {
		return organizationDao.deleteOrganiztion(organizationId);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrganizationService#deleteOrganizationDocument(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOrganizationDocument(Integer organizationDocumentId) {
		return organizationDao.deleteOrganizationDocument(organizationDocumentId);
	}

}
