/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.BloodGroupDao;
import com.protocol.wms.model.BloodGroup;

/**
 * @author Sudhakar
 *
 */
@Service("BloodGroupServiceImpl")
public class BloodGroupServiceImpl implements BloodGroupService {

	
	private BloodGroupDao bloodGroupDao;

	@Autowired
	@Qualifier("BloodGroupDaoImpl")
	public void setBloodGroupDao(BloodGroupDao bloodGroupDao) {
		this.bloodGroupDao = bloodGroupDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BloodGroupService#loadBloodGroupList()
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<BloodGroup> loadBloodGroupList() {
		return bloodGroupDao.loadBloodGroupList();
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BloodGroupService#loadBloodGroupObjectUsingBloodGroupId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public BloodGroup loadBloodGroupObjectUsingBloodGroupId(Integer bloodGroupId) {
		// TODO Auto-generated method stub
		return bloodGroupDao.loadBloodGroupObjectUsingBloodGroupId(bloodGroupId);
	}
	
	
}
