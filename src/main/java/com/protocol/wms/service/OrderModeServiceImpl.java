/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.OrderModeDao;
import com.protocol.wms.model.OrderMode;

/**
 * @author Sudhakar
 *
 */
@Service("OrderModeServiceImpl")
public class OrderModeServiceImpl implements OrderModeService{

	private OrderModeDao orderModeDao;
	
	@Autowired
	@Qualifier("OrderModeDaoImpl")
	public void setOrderModeDao(OrderModeDao orderModeDao) {
		this.orderModeDao = orderModeDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OrderModeService#loadAllOrderMode()
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OrderMode> loadAllOrderMode() {
		return orderModeDao.loadAllOrderMode();
	}
	
	
}
