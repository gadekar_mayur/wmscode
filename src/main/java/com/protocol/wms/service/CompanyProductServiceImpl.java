/**
 * 
 */
package com.protocol.wms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CompanyProductDao;
import com.protocol.wms.model.CompanyProduct;

/**
 * @author admin
 *
 */
@Service("CompanyProductServiceImpl")
public class CompanyProductServiceImpl implements CompanyProductService{

	private CompanyProductDao companyProductDao;
	
	@Autowired
	@Qualifier("CompanyProductDaoImpl") 
	public void setCompanyProductDao(CompanyProductDao companyProductDao) {
		this.companyProductDao = companyProductDao;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyProductService#updateCompanyProductDetails(java.lang.Boolean, com.protocol.wms.model.CompanyProduct)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateCompanyProductDetails(Boolean insertNewProductFlag,Integer companyProductId,CompanyProduct companyProduct) {
		if(insertNewProductFlag)
		{
			Integer id = companyProductDao.saveNewCompanyProductDetailsAndDeleteOldOne(companyProductId,companyProduct);
			if(id!=null)
				return true;
		}else if(companyProductDao.updateCompanyProductDetails(companyProduct))
		{
			return true;
		}
		return false;
	}

}
