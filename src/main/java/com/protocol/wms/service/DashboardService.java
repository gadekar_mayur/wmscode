package com.protocol.wms.service;

import org.json.JSONArray;

public interface DashboardService {
	
	public JSONArray loadDashboardTable(Integer[] organizations, Integer[] companies, Integer[] stockists, Integer year, Integer organizationId);

}
