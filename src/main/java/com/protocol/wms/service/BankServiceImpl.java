/**
 * 
 */
package com.protocol.wms.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.AdvancedChequeInformationDao;
import com.protocol.wms.dao.ChequeNumberInfoDao;
import com.protocol.wms.dao.CompanyBankDao;
import com.protocol.wms.dao.CompanyDao;
import com.protocol.wms.dao.InvoicePaymentInformationDao;
import com.protocol.wms.dao.OrganizationDao;
import com.protocol.wms.dao.OutWardOrderEntryRegistrationDao;
import com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao;
import com.protocol.wms.dao.RemainingChequeAmountDao;
import com.protocol.wms.dao.StockistBankDetailsDao;
import com.protocol.wms.dao.StockistChequeDepositDao;
import com.protocol.wms.dao.StockistChequeReturnDao;
import com.protocol.wms.dao.StockistDao;
import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyBank;
import com.protocol.wms.model.InvoicePaymentInformation;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.RemainingChequeAmount;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistBankDetails;
import com.protocol.wms.model.StockistChequeDeposit;
import com.protocol.wms.model.StockistChequeReturn;

/**
 * @author Sudhakar
 *
 */
@Service("BankServiceImpl")
public class BankServiceImpl implements BankService{
	
	private CompanyBankDao companyBankDao;
	
	private StockistBankDetailsDao StockistBankDetailsDao;
	
	private CompanyDao companyDao;
	
	private StockistDao stockistDao;
	
	private OrganizationDao organizationDao;
	
	private AdvancedChequeInformationDao advancedChequeInformationDao;
	
	private StockistChequeDepositDao stockistChequeDepositDao;
	
	private ChequeNumberInfoDao chequeNumberInfoDao;
	
	private StockistChequeReturnDao stockistChequeReturnDao;
	
	private OutWardOrderInvoiceEnteryRegistrationDao outWardOrderInvoiceEnteryRegistrationDao;
	
	private OutWardOrderEntryRegistrationDao outWardOrderEntryRegistrationDao;
	
	private InvoicePaymentInformationDao invoicePaymentInformationDao;
	
	private RemainingChequeAmountDao remainingChequeAmountDao;
	
	
	
	
	@Autowired
	@Qualifier("RemainingChequeAmountDaoImpl")
	public void setRemainingChequeAmountDao(
			RemainingChequeAmountDao remainingChequeAmountDao) {
		this.remainingChequeAmountDao = remainingChequeAmountDao;
	}

	@Autowired
	@Qualifier("InvoicePaymentInformationDaoImpl")
	public void setInvoicePaymentInformationDao(
			InvoicePaymentInformationDao invoicePaymentInformationDao) {
		this.invoicePaymentInformationDao = invoicePaymentInformationDao;
	}

	@Autowired
	@Qualifier("OutWardOrderEntryRegistrationDaoImpl")
	public void setOutWardOrderEntryRegistrationDao(
			OutWardOrderEntryRegistrationDao outWardOrderEntryRegistrationDao) {
		this.outWardOrderEntryRegistrationDao = outWardOrderEntryRegistrationDao;
	}

	@Autowired
	@Qualifier("OutWardOrderInvoiceEnteryRegistrationDaoImpl")
	public void setOutWardOrderInvoiceEnteryRegistrationDao(
			OutWardOrderInvoiceEnteryRegistrationDao outWardOrderInvoiceEnteryRegistrationDao) {
		this.outWardOrderInvoiceEnteryRegistrationDao = outWardOrderInvoiceEnteryRegistrationDao;
	}

	@Autowired
	@Qualifier("StockistChequeReturnDaoImpl")
	public void setStockistChequeReturnDao(
			StockistChequeReturnDao stockistChequeReturnDao) {
		this.stockistChequeReturnDao = stockistChequeReturnDao;
	}

	@Autowired
	@Qualifier("ChequeNumberInfoDaoImpl")
	public void setChequeNumberInfoDao(ChequeNumberInfoDao chequeNumberInfoDao) {
		this.chequeNumberInfoDao = chequeNumberInfoDao;
	}

	@Autowired
	@Qualifier("StockistChequeDepositDaoImpl")
	public void setStockistChequeDepositDao(
			StockistChequeDepositDao stockistChequeDepositDao) {
		this.stockistChequeDepositDao = stockistChequeDepositDao;
	}

	@Autowired
	@Qualifier("AdvancedChequeInformationDaoImpl")
	public void setAdvancedChequeInformationDao(
			AdvancedChequeInformationDao advancedChequeInformationDao) {
		this.advancedChequeInformationDao = advancedChequeInformationDao;
	}

	@Autowired
	@Qualifier("OrganizationDaoImpl")
	public void setOrganizationDao(OrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	@Autowired
	@Qualifier("StockistDaoImpl")
	public void setStockistDao(StockistDao stockistDao) {
		this.stockistDao = stockistDao;
	}

	@Autowired
	@Qualifier("CompanyDaoImpl")
	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	@Autowired
	@Qualifier("StockistBankDetailsDaoImpl")
	public void setStockistBankDetailsDao(
			StockistBankDetailsDao stockistBankDetailsDao) {
		StockistBankDetailsDao = stockistBankDetailsDao;
	}

	@Autowired
	@Qualifier("CompanyBankDaoImpl")
	public void setCompanyBankDao(CompanyBankDao companyBankDao) {
		this.companyBankDao = companyBankDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#companyBankDropdown(java.lang.Integer, java.lang.Integer)
	 */
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyBank> companyBankDropdown(Integer companyId,
			Integer organizationId) {
		return companyBankDao.companyBankDropdown(companyId, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#stockistBankDropdownList(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<StockistBankDetails> stockistBankDropdownList(
			Integer organizationId, Integer companyId, Integer stockistId) {
		return StockistBankDetailsDao.stockistBankDropdownList(organizationId, companyId, stockistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadCompanyObjectUsingCompanyId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Company loadCompanyObjectUsingCompanyId(Integer companyId) {
		return companyDao.loadCompanyObjectUsingCompanyId(companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadCompanyBankUsignCompanyBankId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyBank loadCompanyBankUsignCompanyBankId(Integer companyBankId) {
		return companyBankDao.loadCompanyBankUsignCompanyBankId(companyBankId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadStockistObjUsigStokistId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Stockist loadStockistObjUsigStokistId(Integer stokistId) {
		return stockistDao.loadStockistObjUsigStokistId(stokistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadStockistBankDetailsUsignStockistBankDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public StockistBankDetails loadStockistBankDetailsUsignStockistBankDetailsId(
			Integer stockistBankDetailsId) {
		return StockistBankDetailsDao.loadStockistBankDetailsUsignStockistBankDetailsId(stockistBankDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadOrganizationObjectUsingOrganiztionId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Organization loadOrganizationObjectUsingOrganiztionId(
			Integer organizationId) {
		return organizationDao.loadOrganizationObjectUsingOrganiztionId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#saveAdvancedChequeInformationObj(com.protocol.wms.model.AdvancedChequeInformation, java.lang.String[])
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean saveAdvancedChequeInformationObj(
			AdvancedChequeInformation AdvancedChequeInformationObj,
			String[] checkNo) {
		return advancedChequeInformationDao.saveAdvancedChequeInformationObj(AdvancedChequeInformationObj, checkNo);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadAdvancedChequeInformationUsignOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<AdvancedChequeInformation> loadAdvancedChequeInformationListUsignOrganizationId(
			Integer organizationId) {
		return advancedChequeInformationDao.loadAdvancedChequeInformationListUsignOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadChequeInformationUsignOrgIdAndCompanyIdAndCompanyBankId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray loadChequeInformationUsignOrgIdAndCompanyIdAndCompanyBankId(
			Integer orgId, Integer companyId, Integer companyBankId) {
		return advancedChequeInformationDao.loadChequeInformationUsignOrgIdAndCompanyIdAndCompanyBankId(orgId, companyId, companyBankId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadChequeInformationUsignOrgIdAndCompanyIdAndStockistAndStockistBankId(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray loadChequeInformationUsignOrgIdAndCompanyIdAndStockistAndStockistBankId(
			Integer orgId, Integer companyId, Integer stockistId,
			Integer stockistBankId) {
		return advancedChequeInformationDao.loadChequeInformationUsignOrgIdAndCompanyIdAndStockistAndStockistBankId(orgId, companyId, stockistId, stockistBankId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#saveStockistChequeDepositObj(com.protocol.wms.model.StockistChequeDeposit)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveStockistChequeDepositObj(
			StockistChequeDeposit StockistChequeDepositObj) {
		return stockistChequeDepositDao.saveStockistChequeDepositObj(StockistChequeDepositObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadChequeNumberInfoObjUsignChequeNumberInfoId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public ChequeNumberInfo loadChequeNumberInfoObjUsignChequeNumberInfoId(
			Integer ChequeNumberInfoId) {
		return chequeNumberInfoDao.loadChequeNumberInfoObjUsignChequeNumberInfoId(ChequeNumberInfoId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#updateChequeNumberInfoObj(com.protocol.wms.model.ChequeNumberInfo)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateChequeNumberInfoObj(ChequeNumberInfo chequeNumberInfoObj) {
		return chequeNumberInfoDao.updateChequeNumberInfoObj(chequeNumberInfoObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadStockistChequeDepositListUsignOrgnizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<StockistChequeDeposit> loadStockistChequeDepositListUsignOrgnizationId(
			Integer organizationId) {
		return stockistChequeDepositDao.loadStockistChequeDepositListUsignOrgnizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadStockistChequeDepositObjUsignStockistChequeDepositId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public StockistChequeDeposit loadStockistChequeDepositObjUsignStockistChequeDepositId(
			Integer StockistChequeDepositId) {
		return stockistChequeDepositDao.loadStockistChequeDepositObjUsignStockistChequeDepositId(StockistChequeDepositId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#saveStockistChequeReturnObj(com.protocol.wms.model.StockistChequeReturn)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveStockistChequeReturnObj(
			StockistChequeReturn stockistChequeReturnObj) {
		return stockistChequeReturnDao.saveStockistChequeReturnObj(stockistChequeReturnObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#updateStockistChequeDepositObj(com.protocol.wms.model.StockistChequeDeposit)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateStockistChequeDepositObj(
			StockistChequeDeposit stockistChequeDepositObj) {
		stockistChequeDepositDao.updateStockistChequeDepositObj(stockistChequeDepositObj);		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadStockistChequeReturnListUsignOrgnizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<StockistChequeReturn> loadStockistChequeReturnListUsignOrgnizationId(
			Integer organizationId) {
		return stockistChequeReturnDao.loadStockistChequeReturnListUsignOrgnizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#reDepositReturnCheque(java.lang.String[], java.lang.String[])
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean reDepositReturnCheque(String[] stockistReturnChequeIdArray,
			String[] stockistDepositChequeIdArray) {
		return stockistChequeDepositDao.reDepositReturnCheque(stockistReturnChequeIdArray, stockistDepositChequeIdArray);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadOutWardOrderInvoiceEnteryRegistrationListEqualToCurrentDate(java.sql.Date, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListEqualToCurrentDate(
			Date currentDate, Integer organizationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.loadOutWardOrderInvoiceEnteryRegistrationListEqualToCurrentDate(currentDate, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(java.sql.Date, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListLessThanToCurrentDate(
			Date currentDate, Integer organizationId) {
		// TODO Auto-generated method stub
		return outWardOrderInvoiceEnteryRegistrationDao.loadOutWardOrderInvoiceEnteryRegistrationListLessThanToCurrentDate(currentDate, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(java.sql.Date, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(
			Date currentDate, Integer organizationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(currentDate, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadOutWardOrderEnteryRegistrationIdUsignOrgIdAndCompIdAndStockistId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Integer> loadOutWardOrderEnteryRegistrationIdUsignOrgIdAndCompIdAndStockistId(
			Integer orgid, Integer companyId, Integer stockistId) {
		return outWardOrderEntryRegistrationDao.loadOutWardOrderEnteryRegistrationIdUsignOrgIdAndCompIdAndStockistId(orgid, companyId, stockistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadOutWardOrderInvoiceEnteryRegistrationListUsignoutWardOrderEnteryRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListUsignoutWardOrderEnteryRegistrationId(
			Integer outWardOrderEnteryRegistrationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.loadOutWardOrderInvoiceEnteryRegistrationListUsignoutWardOrderEnteryRegistrationId(outWardOrderEnteryRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardOrderInvoiceEnteryRegistration loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(
			Integer OutWardOrderInvoiceEnteryRegistrationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(OutWardOrderInvoiceEnteryRegistrationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#saveInvoicePaymentInformationObj(com.protocol.wms.model.InvoicePaymentInformation)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveInvoicePaymentInformationObj(
			InvoicePaymentInformation InvoicePaymentInformationObj) {
		return invoicePaymentInformationDao.saveInvoicePaymentInformationObj(InvoicePaymentInformationObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#updateOutWardOrderInvoiceEnteryRegistrationObj(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateOutWardOrderInvoiceEnteryRegistrationObj(
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj) {
		outWardOrderInvoiceEnteryRegistrationDao.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#saveRemainingChequeAmountObj(com.protocol.wms.model.RemainingChequeAmount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveRemainingChequeAmountObj(
			RemainingChequeAmount RemainingChequeAmountObj) {
		return remainingChequeAmountDao.saveRemainingChequeAmountObj(RemainingChequeAmountObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadRemainingChequeAmountUsignOrgIdAndCompnayIdAndStockistId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public RemainingChequeAmount loadRemainingChequeAmountUsignOrgIdAndCompnayIdAndStockistId(
			Integer organizationId, Integer companyId, Integer stockistId) {
		return remainingChequeAmountDao.loadRemainingChequeAmountUsignOrgIdAndCompnayIdAndStockistId(organizationId, companyId, stockistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#deleteRemainingChequeAmountObj(com.protocol.wms.model.RemainingChequeAmount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void deleteRemainingChequeAmountObj(
			RemainingChequeAmount remainingChequeAmountObj) {
		remainingChequeAmountDao.deleteRemainingChequeAmountObj(remainingChequeAmountObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#laodRemainingChequeAmountUsignRemainingChequeAmountId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public RemainingChequeAmount loadRemainingChequeAmountUsignRemainingChequeAmountId(
			Integer RemainingChequeAmountId) {
		return remainingChequeAmountDao.loadRemainingChequeAmountUsignRemainingChequeAmountId(RemainingChequeAmountId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadInvoicePaymentInformationObjUsignChequeNumberInfoId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InvoicePaymentInformation> loadInvoicePaymentInformationListUsignChequeNumberInfoId(
			Integer ChequeNumberInfoId) {
		return invoicePaymentInformationDao.loadInvoicePaymentInformationListUsignChequeNumberInfoId(ChequeNumberInfoId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#deleteInvoicePaymentInformationObj(com.protocol.wms.model.InvoicePaymentInformation)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void deleteInvoicePaymentInformationObj(
			InvoicePaymentInformation InvoicePaymentInformationObj) {
		invoicePaymentInformationDao.deleteInvoicePaymentInformationObj(InvoicePaymentInformationObj);		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadInvoicePaymentInformationUsigInvoicePaymentInformationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public InvoicePaymentInformation loadInvoicePaymentInformationUsigInvoicePaymentInformationId(
			Integer invoicePaymentInformationId) {
		return invoicePaymentInformationDao.loadInvoicePaymentInformationUsigInvoicePaymentInformationId(invoicePaymentInformationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadRemainingChequeAmountObjUsignChequeNumberInfoId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public RemainingChequeAmount loadRemainingChequeAmountObjUsignChequeNumberInfoId(
			Integer chequeNumberInfoId) {
		return remainingChequeAmountDao.loadRemainingChequeAmountObjUsignChequeNumberInfoId(chequeNumberInfoId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#loadStockistChequeReturnObjUsignStockistChequeReturnId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public StockistChequeReturn loadStockistChequeReturnObjUsignStockistChequeReturnId(
			Integer stockistChequeReturnId) {
		return stockistChequeReturnDao.loadStockistChequeReturnObjUsignStockistChequeReturnId(stockistChequeReturnId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#deleteStockistChequeReturnObj(com.protocol.wms.model.StockistChequeReturn)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void deleteStockistChequeReturnObj(
			StockistChequeReturn stockistChequeReturnObj) {
		stockistChequeReturnDao.deleteStockistChequeReturnObj(stockistChequeReturnObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#outWardOrderInvoiceEnteryRegistrationListWithStatusOne(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListWithStatusOne(
			Integer orgnizationId) {
		return outWardOrderInvoiceEnteryRegistrationDao.loadOutWardOrderInvoiceEnteryRegistrationListWithStatusOne(orgnizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public AdvancedChequeInformation loadAdvancedChequeInformationObjUsignAdvanceChequeId(
			Integer advanceChequeid) {
		return advancedChequeInformationDao.loadAdvancedChequeInformationObjUsignAdvanceChequeId(advanceChequeid);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<ChequeNumberInfo> loadChequeNumberInfoObjUsignAdvanceChequeId(
			Integer advanceChequeid) {
		return advancedChequeInformationDao.loadChequeNumberInfoObjUsignAdvanceChequeId(advanceChequeid);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.BankService#updateAdvancedChequeInformationObj(com.protocol.wms.model.AdvancedChequeInformation)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean updateAdvancedChequeInformationObj(
			AdvancedChequeInformation advancedChequeInformationObj) {
		if(advancedChequeInformationDao.updateAdvancedChequeInformationObj(advancedChequeInformationObj))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer returnNoOfChequeUsingAdvancedChequeInformationId(
			Integer AdvancedChequeInformationId) {
		return chequeNumberInfoDao.returnNoOfChequeUsingAdvancedChequeInformationId(AdvancedChequeInformationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean deleteChequeNumberInfoObj(
			ChequeNumberInfo chequeNumberInfoObj) {
		return chequeNumberInfoDao.deleteChequeNumberInfoObj(chequeNumberInfoObj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean deleteAdvancedChequeInformationObj(
			AdvancedChequeInformation advancedChequeInformationObj) {
		return advancedChequeInformationDao.deleteAdvancedChequeInformationObj(advancedChequeInformationObj);
	}
	
	

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object, Object> searchAdvancedChequeInformation(
			String searchOption, String searchText, Integer organizationId,Integer from) {
		return advancedChequeInformationDao.searchAdvancedChequeInformation(searchOption, searchText,organizationId,from);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object, Object> searchStockistDepositedChequeEntries(String searchOption, String searchText, String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {
		return stockistChequeDepositDao.searchStockistDepositedChequeEntries( searchOption,searchText,fromSpecialData,toSpecialData,organizationId,from);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object, Object> searchOutstandingTodaysDue(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {
		return outWardOrderInvoiceEnteryRegistrationDao.searchOutstandingTodaysDue(
				 ourJavaDateObject,  selectedValue,  searchText,
				 fromSpecialData,  toSpecialData,  organizationId,from);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object, Object> searchOutstandingLapsed(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {
		return outWardOrderInvoiceEnteryRegistrationDao.searchOutstandingLapsed(
				 ourJavaDateObject,  selectedValue,  searchText,
				 fromSpecialData,  toSpecialData,  organizationId,from);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object,Object> searchOutstandingUpcoming(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {
		return outWardOrderInvoiceEnteryRegistrationDao.searchOutstandingUpcoming(
				 ourJavaDateObject,  selectedValue,  searchText,
				 fromSpecialData,  toSpecialData,  organizationId,from);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object, Object> searchOutstandingPaidInvoice(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {
		return outWardOrderInvoiceEnteryRegistrationDao.searchOutstandingPaidInvoice(
				 ourJavaDateObject,  selectedValue,  searchText,
				 fromSpecialData,  toSpecialData,  organizationId,from);
	}
}
