/**
 * 
 */
package com.protocol.wms.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.OutWardReturnGoodsCreditNoteDao;
import com.protocol.wms.dao.OutWardReturnGoodsProductRegistrationDao;
import com.protocol.wms.dao.OutwardReturnGoodsLRDao;
import com.protocol.wms.dao.OutwardReturnGoodsRegistrationDao;
import com.protocol.wms.model.OutWardReturnGoodsLRDetails;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsClaimImagePath;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsLrImagePath;
import com.protocol.wms.model.OutWardReturnGoodsRegistration;

/**
 * @author admin
 *
 */
@Service("OutwardReturnGoodsRegistrationServiceImpl")
public class OutwardReturnGoodsRegistrationServiceImpl implements OutwardReturnGoodsRegistrationService {

	@Autowired
	@Qualifier("OutwardReturnGoodsRegistrationDaoImpl")
	private OutwardReturnGoodsRegistrationDao outwardReturnGoodsRegistrationDao;
	
	@Autowired
	@Qualifier("OutwardReturnGoodsLRDaoImpl")
	private OutwardReturnGoodsLRDao outwardReturnGoodsLRDao;
	
	@Autowired
	@Qualifier("OutWardReturnGoodsCreditNoteDaoImpl")
	private OutWardReturnGoodsCreditNoteDao outWardReturnGoodsCreditNoteDao;
	
	@Autowired
	@Qualifier("OutWardReturnGoodsProductRegistrationDaoImpl")
	private OutWardReturnGoodsProductRegistrationDao outWardReturnGoodsProductRegistrationDao;
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#saveOutwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.util.List, java.util.List, java.util.Map, com.protocol.wms.model.OutWardReturnGoodsRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveOutwardReturnGoodsRegistration(
			Integer orgId,
			Integer companyId,
			Integer stockistId,
			Integer transporterId,
			Integer employeeId,
			List<OutWardReturnGoodsLRDetails> lrDetailsList,
			OutWardReturnGoodsRegistration outWardReturnGoodsRegistration) {
		return outwardReturnGoodsRegistrationDao.saveOutwardReturnGoodsRegistration(orgId,companyId,stockistId,transporterId,employeeId,lrDetailsList,outWardReturnGoodsRegistration);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#updateInwardReturnGoodsLrReg(java.util.List, java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateInwardReturnGoodsLrReg(
			List<OutWardReturnGoodsLRDetails> lrDetailsList) {
		return outwardReturnGoodsRegistrationDao.updateInwardReturnGoodsLrReg(lrDetailsList);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#outwardReturnGoodsRegListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OutWardReturnGoodsRegistration> outwardReturnGoodsRegListing(Integer organizationId) {
		return outwardReturnGoodsRegistrationDao.outwardReturnGoodsRegListing(organizationId);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#deleteOutwardReturnGoodsRegDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardReturnGoodsRegDetails(Integer outwardRetGoodsRegId) {
		return outwardReturnGoodsRegistrationDao.deleteOutwardReturnGoodsRegDetails(outwardRetGoodsRegId);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#outwardReturnGoodsRegDetailsAndLrListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String, Object> outwardReturnGoodsRegDetailsAndLrListing(Integer outWardReturnGoodsRegistrationId) {
		OutWardReturnGoodsRegistration outWardReturnGoodsRegistration = outwardReturnGoodsRegistrationDao.getOutWardReturnGoodsRegistrationDetails(outWardReturnGoodsRegistrationId);
		List<OutWardReturnGoodsLRDetails> lrList = outwardReturnGoodsLRDao.outWardReturnGoodsLRDetailsList(outWardReturnGoodsRegistrationId);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("OutWardReturnGoodsRegistration", outWardReturnGoodsRegistration);
		map.put("lrList", lrList);
		return map;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject outwardReturnGoodsRegDetailsViewAndLrListingView(Boolean editStatus,
			Integer outWardReturnGoodsRegistrationId) throws JSONException, ParseException {
		JSONObject result = null;
		JSONArray orgrLRArr = null;
		JSONArray json_ImageArray = null;
		JSONObject tempJson = null;
		OutWardReturnGoodsRegistration outWardReturnGoodsRegistration = null;
		List<OutWardReturnGoodsLRDetails>outWardReturnGoodsLRDetailsList = null;
		OutWardReturnGoodsLRDetails tempOutWardReturnGoodsLRDetails = null;
		Iterator<OutWardReturnGoodsLRDetails> orgrLRItr = null;
		SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
        java.util.Date _24HourDt = null;
		{
			orgrLRArr = new JSONArray();
			outWardReturnGoodsRegistration = outwardReturnGoodsRegistrationDao.getOutWardReturnGoodsRegistrationDetails(outWardReturnGoodsRegistrationId);
			//outWardReturnGoodsLRDetailsList = outWardReturnGoodsRegistration.getOutWardReturnGoodsLRDetails();
			outWardReturnGoodsLRDetailsList = outwardReturnGoodsRegistrationDao.getOutWardReturnGoodsLRDetailsHavingStatusOneUsingOutwardReturnGoodsRegId(outWardReturnGoodsRegistrationId);
			orgrLRItr = outWardReturnGoodsLRDetailsList.iterator();
			while(orgrLRItr.hasNext()){
				tempOutWardReturnGoodsLRDetails = orgrLRItr.next();
				result = new JSONObject();
				result.put("LR_ID", tempOutWardReturnGoodsLRDetails.getOutWardReturnGoodsLRDetailsId());
				result.put("LR_NO", tempOutWardReturnGoodsLRDetails.getLrNo());
				result.put("NO_OF_CASES", tempOutWardReturnGoodsLRDetails.getNoOfCases());
				result.put("REF_CLAIM_NO", tempOutWardReturnGoodsLRDetails.getClaimNo());
				result.put("CLAIM_AMOUNT", tempOutWardReturnGoodsLRDetails.getClaimAmount());
				//String s = tempOutWardReturnGoodsLRDetails.getLrImagePath();int i = s.lastIndexOf('/');s=s.substring(i+1);
				json_ImageArray=new JSONArray();
				for(OutWardReturnGoodsLRDetailsLrImagePath tempObj : tempOutWardReturnGoodsLRDetails.getOutWardReturnGoodsLRDetailsLrImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getOutWardReturnGoodsLRDetailsLrImagePathId());
					json_ImageArray.put(tempJson);
				}
				result.put("ATTACH_LR",json_ImageArray);
				//s = tempOutWardReturnGoodsLRDetails.getClaimImagePath(); i = s.lastIndexOf('/');s=s.substring(i+1);
				json_ImageArray=new JSONArray();
				for(OutWardReturnGoodsLRDetailsClaimImagePath tempObj : tempOutWardReturnGoodsLRDetails.getOutWardReturnGoodsLRDetailsClaimImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getOutWardReturnGoodsLRDetailsClaimImagePathId());
					json_ImageArray.put(tempJson);
				}
				result.put("ATTACH_CLAIM_COPY", json_ImageArray);
				orgrLRArr.put(result);
			}
			
			result = new JSONObject();
			result.put("ID_NO", outWardReturnGoodsRegistration.getOutWardReturnGoodsRegistrationId());
			result.put("ORGANIZATION", outWardReturnGoodsRegistration.getOrganization().getOrganizationName());
			result.put("COMPANY", outWardReturnGoodsRegistration.getCompany().getCompanyName());
			if(outWardReturnGoodsRegistration.getStockist()!=null){
				result.put("STOCKIST", outWardReturnGoodsRegistration.getStockist().getStockistName());
				result.put("STOCKIST_ID", outWardReturnGoodsRegistration.getStockist().getStockistId());
			}else
				result.put("STOCKIST", "");
			result.put("TRANSPORTER", outWardReturnGoodsRegistration.getTransporterDetails().getTransporterName());
			String [] reg_date_arr = outWardReturnGoodsRegistration.getRegistrationDate().toString().split("-");
			result.put("REG_DATE",reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0] );
	        _24HourDt = _24HourSDF.parse(outWardReturnGoodsRegistration.getTime().toString());
			result.put("TIME", _12HourSDF.format(_24HourDt));
			result.put("DRIVER", outWardReturnGoodsRegistration.getDriverName());
			result.put("REASON", outWardReturnGoodsRegistration.getReason());
			if(outWardReturnGoodsRegistration.getTransportationCharges()!=null)
				result.put("TRANSPORTATION_CHARGES", outWardReturnGoodsRegistration.getTransportationCharges());
			else
				result.put("TRANSPORTATION_CHARGES", "");
			result.put("DRIVER_NO", outWardReturnGoodsRegistration.getDriverNo());
			result.put("SUBMIT_DATE", outWardReturnGoodsRegistration.getSubmitDate());
			if(outWardReturnGoodsRegistration.getEmployeeDetails()!=null){
				result.put("EMPLOYEE_NAME", outWardReturnGoodsRegistration.getEmployeeDetails().getName());
				result.put("EMPLOYEE_ID", outWardReturnGoodsRegistration.getEmployeeDetails().getEmployeeDetailsId());
			}else
				result.put("EMPLOYEE_NAME", "");
			if(editStatus!=null&&editStatus==true){
				result.put("ORG_ID", outWardReturnGoodsRegistration.getOrganization().getOrganizationId());
				result.put("COMPANY_ID", outWardReturnGoodsRegistration.getCompany().getCompanyId());
				result.put("TRANSPORTER_ID", outWardReturnGoodsRegistration.getTransporterDetails().getTransporterDetailsId());
			}
			result.put("orgrLRArr", orgrLRArr);
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#viewOutwardLR_DetailedView_LrCreditNoteAndLrProductList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String, Object> viewOutwardLR_DetailedView_LrCreditNoteAndLrProductList(
			Integer outWardReturnGoodsLRDetailsId) {
		OutWardReturnGoodsLRDetails LR_Details = outwardReturnGoodsLRDao.getOutWardReturnGoodsLRDetailsByLrId(outWardReturnGoodsLRDetailsId);
		//OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote = outWardReturnGoodsCreditNoteDao.getOutWardReturnGoodsCreditNoteDetailsByLR_ID(outWardReturnGoodsLRDetailsId);
		//List<OutWardReturnGoodsProductRegistration> outwardReturnProductsList = outWardReturnGoodsProductRegistrationDao.outWardReturnGoodsProductRegistrationList(outWardReturnGoodsLRDetailsId);
		Map<String,Object> map = new HashMap<String, Object>();
		
		map.put("OutWardReturnGoodsLRDetails", LR_Details);
		//map.put("OutWardReturnGoodsCreditNote", outWardReturnGoodsCreditNote);
		//map.put("productList", outwardReturnProductsList);
		return map;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#deleteOutwardReturnGoodsRegLrDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardReturnGoodsRegLrDetails(Integer LR_ID) {
		// TODO Auto-generated method stub
		return outwardReturnGoodsLRDao.deleteOutwardReturnGoodsRegLrDetails(LR_ID);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#deleteOutwardReturnGoodsRegLRProduct(java.lang.Integer)
	 
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardReturnGoodsRegLRProduct(Integer PRODUCT_ID) {
		// TODO Auto-generated method stub
		return outWardReturnGoodsProductRegistrationDao.deleteOutwardReturnGoodsRegLRProduct(PRODUCT_ID);
	}*/
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#loadOutWardReturnGoodsRegistrationObjByOutwardReturnGoodsRegId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardReturnGoodsRegistration loadOutWardReturnGoodsRegistrationObjByOutwardReturnGoodsRegId(Integer outWardReturnGoodsRegistrationId) {
		// TODO Auto-generated method stub
		return outwardReturnGoodsRegistrationDao.loadOutWardReturnGoodsRegistrationObjByOutwardReturnGoodsRegId(outWardReturnGoodsRegistrationId);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#editInwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.OutWardReturnGoodsRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject editOutwardReturnGoodsRegistration(Integer orgId,
			Integer companyId, Integer stockistId, Integer transporterId,
			Integer employeeId,
			OutWardReturnGoodsRegistration outwardReturnGoodsRegistration) throws JSONException {
		JSONObject result = null;
		result =  new JSONObject();
		result.put("MSG", "Inward Return Goods Registration Updation Failed....!");
		if(outwardReturnGoodsRegistrationDao.editOtwardReturnGoodsRegistration(orgId, companyId, stockistId, transporterId, employeeId, outwardReturnGoodsRegistration)){
			result.put("ID_NO", outwardReturnGoodsRegistration.getOutWardReturnGoodsRegistrationId());
			result.put("ORGANIZATION", outwardReturnGoodsRegistration.getOrganization().getOrganizationName());
			result.put("COMPANY", outwardReturnGoodsRegistration.getCompany().getCompanyName());
			if(outwardReturnGoodsRegistration.getStockist()!=null)
				result.put("STOCKIST", outwardReturnGoodsRegistration.getStockist().getStockistName());
			else
				result.put("STOCKIST","-");
			result.put("TRANSPORTER", outwardReturnGoodsRegistration.getTransporterDetails().getTransporterName());
			result.put("RESULT",true);
			result.put("MSG", "Inward Return Goods Registration Updated Successfully....!");
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#loadOutWardReturnGoodsLRDetailsByLrRegId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OutWardReturnGoodsLRDetails loadOutWardReturnGoodsLRDetailsByLrRegId(Integer outWardReturnGoodsLRDetailsId) {
		// TODO Auto-generated method stub
		return outwardReturnGoodsLRDao.loadOutWardReturnGoodsLRDetailsByLrRegId(outWardReturnGoodsLRDetailsId);
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.OutwardReturnGoodsRegistrationService#editOutwardReturnGoodsRegLrDetails(com.protocol.wms.model.OutWardReturnGoodsLRDetails)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean editOutwardReturnGoodsRegLrDetails(OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails) {
		// TODO Auto-generated method stub
		return outwardReturnGoodsLRDao.editOutwardReturnGoodsRegLrDetails(outWardReturnGoodsLRDetails);
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object,Object> searchOutReturnGoodsRegListing(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from) {
		// TODO Auto-generated method stub
		return outwardReturnGoodsRegistrationDao.searchOutReturnGoodsRegListing(selectedValue, searchText, fromSpecialData,
				 toSpecialData, organizationId,from);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateOutwardReturnGoodsLrRegLrImage(
			Integer outwardReturnGoodsLrRegistrationId,
			List<OutWardReturnGoodsLRDetailsLrImagePath> lrImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		lrImgList = outwardReturnGoodsLRDao.updateOutwardReturnGoodsLrRegLrImage(outwardReturnGoodsLrRegistrationId, lrImgList);
		for(OutWardReturnGoodsLRDetailsLrImagePath tempObj : lrImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getOutWardReturnGoodsLRDetailsLrImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardReturnGoodsLrRegLrImage(Integer lrImageId) {
		return outwardReturnGoodsLRDao.deleteOutwardReturnGoodsLrRegLrImage(lrImageId);
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateOutwardReturnGoodsLrRegClaimCopyImage(
			Integer outwardReturnGoodsLrRegistrationId,
			List<OutWardReturnGoodsLRDetailsClaimImagePath> claimCopyImgList)
			throws Exception {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		claimCopyImgList = outwardReturnGoodsLRDao.updateOutwardReturnGoodsLrRegClaimCopyImage(outwardReturnGoodsLrRegistrationId, claimCopyImgList);
		for(OutWardReturnGoodsLRDetailsClaimImagePath tempObj : claimCopyImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getOutWardReturnGoodsLRDetailsClaimImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteOutwardReturnGoodsLrRegClaimCopyImage(
			Integer claimCopyImageId) {
		return outwardReturnGoodsLRDao.deleteOutwardReturnGoodsLrRegClaimCopyImage(claimCopyImageId);
	}
}
