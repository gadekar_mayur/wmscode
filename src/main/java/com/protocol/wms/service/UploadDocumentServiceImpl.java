package com.protocol.wms.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.protocol.wms.dao.UploadDocumentDao;




@Service("UploadDocumentServiceImpl")
public class UploadDocumentServiceImpl implements UploadDocumentService{
	@Autowired
	@Qualifier("UploadDocumentDaoImpl")
	private UploadDocumentDao uploadDocumentDao;
	@Override
	public String uploadReport(String file, HttpServletRequest request, String reportName)
			throws IOException,InvalidFormatException, SQLException {
		List<String>Order_No=null;
		List<String>Order_Date=null;
		List<String>Inv_No=null;
		List<String>Inv_Date=null;
		List<String>Inv_Amt=null;
		List<String>Stockist_Name=null;
		List<String>Stockist_City=null;
		List<String>Company_Name=null;
		Properties code=new Properties();
		code.load(uploadDocumentDao.getClass().getClassLoader().getResourceAsStream("code.properties"));
		String stockistCodeValue="";
		try
		{
			if(reportName.equalsIgnoreCase("invoice_report"))
				stockistCodeValue=code.getProperty("invoice");
			if(stockistCodeValue!=null && stockistCodeValue.length()>0)
			{
				String[] codeDetails=stockistCodeValue.split(",");
				 InputStream input = new FileInputStream(file);
					Workbook wb = WorkbookFactory.create(input);
					Sheet sheet = wb.getSheetAt(0);
					int columnIndex = 0;
				    int rowNumber = 0;    
			
			if(true){                               
				Order_No=new ArrayList<String>(); 
				Order_Date=new ArrayList<String>(); 
				Inv_No=new ArrayList<String>();  
				 Inv_Date=new ArrayList<String>();
				 Inv_Amt=new ArrayList<String>();
				 Stockist_Name=new ArrayList<String>();
				 Stockist_City=new ArrayList<String>();
				 Company_Name=new ArrayList<String>();
				 
			 }
			for(Row rowise:sheet)
			{
				for(Cell cellwise:rowise)
				{
					 columnIndex = cellwise.getColumnIndex();
			          rowNumber = cellwise.getRow().getRowNum();
			 		  Row row = null;
			 		  Cell cell = null;
			 		  boolean isNull = false;
			 		  if(codeDetails[0].trim().equalsIgnoreCase(cellwise.toString().trim()))
			 		  {
			 			  rowNumber+=1;
			 			 do
				 		  { 
				 		    try {
				 		   row = sheet.getRow(rowNumber);
				 		   cell = row.getCell(columnIndex);
				 		 
				 		   if(cell==null){
				 			  Order_No.add("");
				 		   }else{
				 			  cell.setCellType(Cell.CELL_TYPE_STRING);
				 			 Order_No.add(cell.toString());
				 		   }
				 			  
				 		   rowNumber++;
				 		   }
				 		   catch(Exception e) {
				 			  try{
				 				  row = sheet.getRow(rowNumber+1);
						 		   cell = row.getCell(columnIndex);
						 		   rowNumber+=1;
				 			   }catch(Exception ex){
				 				  isNull = true;
				 			   }
				 		    
				 		   }
				 		  }while(isNull!=true);
			 		  }
			 		  if(codeDetails[1].trim().equalsIgnoreCase(cellwise.toString().trim()))
			 		  {
			 			 rowNumber+=1;
			 			 do
				 		  { 
				 		    try {
				 		   row = sheet.getRow(rowNumber);
				 		   cell = row.getCell(columnIndex);
				 		 
				 		   if(cell==null){
				 			  Order_Date.add("");
				 		   }else{
				 			  cell.setCellType(Cell.CELL_TYPE_STRING);
				 			 Order_Date.add(cell.toString());
				 		   }
				 			  
				 		   rowNumber++;
				 		   }
				 		   catch(Exception e) {
				 			  try{
				 				  row = sheet.getRow(rowNumber+1);
						 		   cell = row.getCell(columnIndex);
						 		   rowNumber+=1;
				 			   }catch(Exception ex){
				 				  isNull = true;
				 			   }
				 		    
				 		   }
				 		  }while(isNull!=true);
			 		  }
			 		  if(codeDetails[2].trim().equalsIgnoreCase(cellwise.toString().trim()))
			 		  {
			 			 rowNumber+=1;
			 			 do
				 		  { 
				 		    try {
				 		   row = sheet.getRow(rowNumber);
				 		   cell = row.getCell(columnIndex);
				 		 
				 		   if(cell==null){
				 			  Inv_No.add("");
				 		   }else{
				 			  cell.setCellType(Cell.CELL_TYPE_STRING);
				 			 Inv_No.add(cell.toString());
				 		   }
				 			  
				 		   rowNumber++;
				 		   }
				 		   catch(Exception e) {
				 			  try{
				 				  row = sheet.getRow(rowNumber+1);
						 		   cell = row.getCell(columnIndex);
						 		   rowNumber+=1;
				 			   }catch(Exception ex){
				 				  isNull = true;
				 			   }
				 		    
				 		   }
				 		  }while(isNull!=true);
			 		  }
			 		  if((codeDetails[3].trim().equalsIgnoreCase(cellwise.toString().trim())))
			 		  {
			 			 rowNumber+=1;
			 			 do
				 		  { 
				 		    try {
				 		   row = sheet.getRow(rowNumber);
				 		   cell = row.getCell(columnIndex);
				 		 
				 		   if(cell==null){
				 			  Inv_Date.add("");
				 		   }else{
				 			  cell.setCellType(Cell.CELL_TYPE_STRING);
				 			 Inv_Date.add(cell.toString());
				 		   }
				 			  
				 		   rowNumber++;
				 		   }
				 		   catch(Exception e) {
				 			  try{
				 				  row = sheet.getRow(rowNumber+1);
						 		   cell = row.getCell(columnIndex);
						 		   rowNumber+=1;
				 			   }catch(Exception ex){
				 				  isNull = true;
				 			   }
				 		    
				 		   }
				 		  }while(isNull!=true);
			 		  }
			 		 if((codeDetails[4].trim().equalsIgnoreCase(cellwise.toString().trim())))
			 		  {
			 			 rowNumber+=1;
			 			 do
				 		  { 
				 		    try {
				 		   row = sheet.getRow(rowNumber);
				 		   cell = row.getCell(columnIndex);
				 		 
				 		   if(cell==null){
				 			  Inv_Amt.add("");
				 		   }else{
				 			  cell.setCellType(Cell.CELL_TYPE_STRING);
				 			 Inv_Amt.add(cell.toString());
				 		   }
				 			  
				 		   rowNumber++;
				 		   }
				 		   catch(Exception e) {
				 			  try{
				 				  row = sheet.getRow(rowNumber+1);
						 		   cell = row.getCell(columnIndex);
						 		   rowNumber+=1;
				 			   }catch(Exception ex){
				 				  isNull = true;
				 			   }
				 		    
				 		   }
				 		  }while(isNull!=true);
			 		  }
			 		if((codeDetails[5].trim().equalsIgnoreCase(cellwise.toString().trim())))
			 		  {
			 			 rowNumber+=1;
			 			 do
				 		  { 
				 		    try {
				 		   row = sheet.getRow(rowNumber);
				 		   cell = row.getCell(columnIndex);
				 		 
				 		   if(cell==null){
				 			  Stockist_Name.add("");
				 		   }else{
				 			  cell.setCellType(Cell.CELL_TYPE_STRING);
				 			 Stockist_Name.add(cell.toString());
				 		   }
				 			  
				 		   rowNumber++;
				 		   }
				 		   catch(Exception e) {
				 			  try{
				 				  row = sheet.getRow(rowNumber+1);
						 		   cell = row.getCell(columnIndex);
						 		   rowNumber+=1;
				 			   }catch(Exception ex){
				 				  isNull = true;
				 			   }
				 		    
				 		   }
				 		  }while(isNull!=true);
			 		  }
			 		if((codeDetails[6].trim().equalsIgnoreCase(cellwise.toString().trim())))
			 		  {
			 			 rowNumber+=1;
			 			 do
				 		  { 
				 		    try {
				 		   row = sheet.getRow(rowNumber);
				 		   cell = row.getCell(columnIndex);
				 		 
				 		   if(cell==null){
								 Stockist_City.add("");
				 		   }else{
				 			  cell.setCellType(Cell.CELL_TYPE_STRING);
								 Stockist_City.add(cell.toString());
				 		   }
				 			  
				 		   rowNumber++;
				 		   }
				 		   catch(Exception e) {
				 			  try{
				 				  row = sheet.getRow(rowNumber+1);
						 		   cell = row.getCell(columnIndex);
						 		   rowNumber+=1;
				 			   }catch(Exception ex){
				 				  isNull = true;
				 			   }
				 		    
				 		   }
				 		  }while(isNull!=true);
			 		  }
			 		if((codeDetails[7].trim().equalsIgnoreCase(cellwise.toString().trim())))
			 		  {
			 			 rowNumber+=1;
			 			 do
				 		  { 
				 		    try {
				 		   row = sheet.getRow(rowNumber);
				 		   cell = row.getCell(columnIndex);
				 		 
				 		   if(cell==null){
				 			  Company_Name.add("");
				 		   }else{
				 			  cell.setCellType(Cell.CELL_TYPE_STRING);
				 			 Company_Name.add(cell.toString());
				 		   }
				 			  
				 		   rowNumber++;
				 		   }
				 		   catch(Exception e) {
				 			  try{
				 				  row = sheet.getRow(rowNumber+1);
						 		   cell = row.getCell(columnIndex);
						 		   rowNumber+=1;
				 			   }catch(Exception ex){
				 				  isNull = true;
				 			   }
				 		    
				 		   }
				 		  }while(isNull!=true);
			 		  }
				}
			}
			if(reportName.equalsIgnoreCase("invoice_report")){
				 Integer count=uploadDocumentDao.saveInvReport(Order_No,Order_Date,Inv_No,Inv_Date,Inv_Amt,Stockist_Name,Stockist_City,Company_Name);
				 
				 if(count>0)
				 return "Invoice  Report Upload Successfully";
				 else
				 return "Invoice  Report Upload Failed";
				 }
			
			}
			else
			{
				return "Select Correct Format Of Uploading File And Document Name...";
			}
		}
		catch(Exception e){
			
			e.printStackTrace();
		}
		
		return null;
	}

}
