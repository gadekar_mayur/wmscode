package com.protocol.wms.service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CompanyBankDetailsDao;
import com.protocol.wms.dao.CompanyCreditAndDiscountDao;
import com.protocol.wms.dao.CompanyDao;
//import com.protocol.wms.dao.CompanyDepoDao;
import com.protocol.wms.dao.CompanyDocumentDao;
import com.protocol.wms.dao.CompanyProductDao;
import com.protocol.wms.dao.DocumentListTypeDao;
import com.protocol.wms.dao.OrganizationBankCompanyJoinDAO;
import com.protocol.wms.dao.OrganizationBankDao;
import com.protocol.wms.dao.UserDetailsDao;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyBank;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.CompanyDocument;
import com.protocol.wms.model.CompanyDocumentImagePath;
import com.protocol.wms.model.CompanyProduct;
import com.protocol.wms.model.DocumentListType;
import com.protocol.wms.model.OrganizationBank;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.UserDetails;
@Service("CompanyServiceImpl")
public class CompanyServiceImpl implements CompanyService {

//	private Logger log = Logger.getLogger(CompanyServiceImpl.class.getClass());
	
	private OrganizationBankCompanyJoinDAO organizationBankCompanyJoinDAO;
	
	private OrganizationBankDao organizationBankDao;
	
	@Autowired
	@Qualifier("CompanyDaoImpl")
	private CompanyDao companyDao;
	
	@Autowired
	@Qualifier("UserDetailsDaoImpl")
	private UserDetailsDao userDetailsDao;
	@Autowired
	@Qualifier("CompanyCreditAndDiscountDaoImpl")
	private CompanyCreditAndDiscountDao companyCreditAndDiscountDao;
	
	@Autowired
	@Qualifier("CompanyDocumentDaoImpl")
	private CompanyDocumentDao companyDocumentDao;
	@Autowired
	@Qualifier("CompanyBankDetailsDaoImpl")
	private CompanyBankDetailsDao companyBankDao;
	
	@Autowired
	@Qualifier("DocumentListTypeDaoImpl")
	private DocumentListTypeDao documentListTypeDao;
	
//	private CompanyDepoDao companyDepoDao;
	
	private CompanyProductDao companyProductDao;

	@Autowired
	@Qualifier("UserDetailsServiceImpl") 
	private UserDetailsService userDetailsService; 
	
	@Autowired
	@Qualifier("CompanyProductDaoImpl") 
	public void setCompanyProductDao(CompanyProductDao companyProductDao) {
		this.companyProductDao = companyProductDao;
	}

	@Autowired
	@Qualifier("OrganizationBankServiceImpl") 
	private OrganizationBankService organizationBankService;
	
	@Autowired
	@Qualifier("OrganizationBankCompanyJoinDAOImpl")
	public void setOrganizationBankCompanyJoinDAO(
			OrganizationBankCompanyJoinDAO organizationBankCompanyJoinDAO) {
		this.organizationBankCompanyJoinDAO = organizationBankCompanyJoinDAO;
	}
	
	@Autowired
	@Qualifier("OrganizationBankDaoImpl")
	public void setOrganizationBankDao(OrganizationBankDao organizationBankDao) {
		this.organizationBankDao = organizationBankDao;
	}
	
//	@Autowired
//	@Qualifier("CompanyDepoDaoImpl")
//	public void setCompanyDepoDao(CompanyDepoDao companyDepoDao) {
//		this.companyDepoDao = companyDepoDao;
//	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getCompanyList(Integer orgId) {
		return companyDao.getCompanyList(orgId);
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#getCompanyDropDownForCredit_Discount(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getCompanyDropDownForCredit_Discount(Integer organizationId) {
		return companyCreditAndDiscountDao.getCompanyDropDownForCredit_Discount(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#loadCompanyObjectUsingCompanyId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Company loadCompanyObjectUsingCompanyId(Integer companyId) {
		return companyDao.loadCompanyObjectUsingCompanyId(companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#getCompanyListAccToStockist(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject getCompanyListAccToStockist(Integer stockistId,	Integer orgId) {
		return companyDao.getCompanyListAccToStockist(stockistId, orgId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#saveCompanyDetails(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.Company)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveCompanyDetails(Integer orgId,Integer stateId,Integer districtId,Integer cityId,Integer roleTypeId,UserDetails userDetails ,Company company){
		return companyDao.saveCompanyDetails(orgId,stateId,districtId,cityId,roleTypeId,userDetails,company);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#checkUserNameAlreadyExitOrNot(java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean checkUserNameAlreadyExitOrNot(String uname) {
		UserDetails userdetails = null;
		Boolean result = false;
		try{
			userdetails = userDetailsService.checkUserNameAlreadyExitOrNot(uname);
			if(userdetails!=null)
				result=true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#companyDetailsList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Company> companyDetailsList(Integer organizationId) {
		return companyDao.companyDetailsList(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#searchAssignedBankDetailsOfOrgToCompany(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Object> searchAssignedBankDetailsOfOrgToCompany(String searchOption, String searchText,Integer from, Integer organizationId) {
		// TODO Auto-generated method stub
		return companyDao.searchAssignedBankDetailsOfOrgToCompany(searchOption, searchText,from, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#saveCompanyBankDetails(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyBank)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public boolean saveCompanyBankDetails(Integer orgId, Integer companyId,
			CompanyBank companyBank) {
		return companyDao.saveCompanyBankDetails(orgId,companyId,companyBank);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#companyBankDetailsListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyBank> companyBankDetailsListing(Integer organizationId) {
		return companyDao.companyBankDetailsListing(organizationId);
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#searchCompanyDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String , Object> searchCompanyDetails(String searchOption,String searchText, Integer from, Integer orgId) {
		return companyDao.searchCompanyDetails(searchOption, searchText, from, orgId);
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#companyBankDropdown(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyBank> companyBankDropdown(Integer companyId,Integer organizationId) {
		// TODO Auto-generated method stub
		return companyDao.companyBankDropdown(companyId,organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#searchCompanyBankDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object,Object> searchCompanyBankDetails(String searchOption,String searchText,Integer organizationId, Integer from) {
		return companyBankDao.searchCompanyBankDetails(searchOption, searchText, organizationId,from);
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#saveCompanyDepoDetails(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyDepo)
	 
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveCompanyDepoDetails(Integer organizationId,Integer companyId,Integer stateId,Integer districtId,Integer cityId,CompanyDepo companyDepo) {
		return companyDao.saveCompanyDepoDetails(organizationId,companyId,stateId,districtId,cityId,companyDepo);
	}

	 (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#getCompanyDepoListing(java.lang.Integer)
	 
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyDepo> getCompanyDepoListing(Integer organizationId) {
		return companyDao.getCompanyDepoListing(organizationId);
	}

	 (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#searchCompanyDepotDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyDepo> searchCompanyDepotDetails(String searchOption,
			String searchText, Integer organizationId) {
		// TODO Auto-generated method stub
		return companyDepoDao.searchCompanyDepotDetails(searchOption, searchText, organizationId);
	}

	 (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#getCompanyDepoListByCompanyId(java.lang.Integer)
	 
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyDepo> getCompanyDepoListByCompanyId(Integer companyId) {
		// TODO Auto-generated method stub
		return companyDao.getCompanyDepoListByCompanyId(companyId);
	}*/

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#saveCompanyProduct(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyProduct)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveCompanyProduct(Integer organizationId, Integer companyId,
			CompanyProduct companyProduct) {
		return companyDao.saveCompanyProduct(organizationId,companyId,companyProduct);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#getCompanyProductListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyProduct> getCompanyProductListing(Integer organizationId) {
		// TODO Auto-generated method stub
		return companyDao.getCompanyProductListing(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#searchCompanyProductDetails(java.lang.String, java.lang.String, java.lang.Double, java.lang.Double, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String , Object> searchCompanyProductDetails(String searchOption, String searchText, Double fromValue,Double toValue, Integer from, Integer organizationId) {
		// TODO Auto-generated method stub
		return companyProductDao.searchCompanyProductDetails(searchOption, searchText, fromValue, toValue, from, organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#loadOrganizationBanksListUsingOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<OrganizationBank> loadOrganizationBanksListUsingOrganizationId(Integer organizationId) {
		return organizationBankService.loadOrganizationBanksListUsingOrganizationId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#getCompanyOragainzationBankList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray getCompanyOragainzationBankList(Integer companyId) {
		return companyDao.getCompanyOragainzationBankList(companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#saveOrganizationBankToCompany(java.lang.Integer, java.lang.Integer, java.lang.Integer[])
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean saveOrganizationBankToCompany(Integer organizationId,Integer companyId,Integer [] orgBanks) {
		return companyDao.saveOrganizationBankToCompany(organizationId,companyId,orgBanks);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#uploadCompanyDocs(java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyDocument)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean uploadCompanyDocs(Integer orgId,Integer companyId,Integer documentTypeId,CompanyDocument companyDocument) {
		return companyDao.uploadCompanyDocs(orgId,companyId,documentTypeId,companyDocument);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#getCompanyDocsListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyDocument> getCompanyDocsListing(Integer organizationId) {
		return companyDao.getCompanyDocsListing(organizationId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray getCompanyDocsListingView(Integer organizationId)
			throws Exception {
		JSONObject result = null;
		JSONArray companyDocArr = null;
		List<CompanyDocument> companyDocList = null;
		CompanyDocument companyDoc = null;
		Iterator<CompanyDocument> companyDocItr = null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		{
			companyDocArr = new JSONArray();
			companyDocList = companyDao.getCompanyDocsListing(organizationId);
			companyDocItr = companyDocList.iterator();
			while(companyDocItr.hasNext()){
				companyDoc = companyDocItr.next();
				if("1".equals(companyDoc.getOrganization().getOrganizationStatus())&&companyDoc.getCompany().getStatus()==1){
				result = new JSONObject();
				result.put("ORG_NAME", companyDoc.getOrganization().getOrganizationName());
				result.put("ORG_ID", companyDoc.getOrganization().getOrganizationId());
				result.put("DOCUMENT_ID", companyDoc.getCompanyDocumentId());
				result.put("COMPANY_ID", companyDoc.getCompany().getCompanyId());
				result.put("COMPANY_NAME", companyDoc.getCompany().getCompanyName());
				result.put("DOCUMENT_TYPE_ID", companyDoc.getDocumentListType().getDocumentListTypeId());
				result.put("DOCUMENT_TYPE", companyDoc.getDocumentListType().getDocumentName());
				json_ImageArray=new JSONArray();
				for(CompanyDocumentImagePath tempObj : companyDoc.getCompanyDocumentImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getCompanyDocumentImagePathId());
					json_ImageArray.put(tempJson);
				}
				result.put("FILE_NAME",json_ImageArray);
				companyDocArr.put(result);
				}
			}
		}
			return companyDocArr;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#searchUploadedCompanyDocsDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray searchUploadedCompanyDocsDetails(String searchOption, String searchText, Integer organizationId,Integer from) throws JSONException {
		JSONObject result = null;
		JSONArray companyDocArr = null;
//		List<CompanyDocument> companyDocList = null;
		CompanyDocument companyDoc = null;
		Iterator<CompanyDocument> companyDocItr = null;
		JSONObject tempJson = null;
		java.util.Map<Object, Object>map = null;
		JSONArray json_ImageArray = null;
		{
			companyDocArr = new JSONArray();
			map = companyDocumentDao.searchUploadedCompanyDocsDetails(searchOption, searchText, organizationId,from);
			List<CompanyDocument> companyDocList = (List<CompanyDocument>) map.get("companyDoc");
			Integer totalPage = (Integer) map.get("totalPages");
			
			companyDocItr = companyDocList.iterator();
			while(companyDocItr.hasNext()){
				companyDoc = companyDocItr.next();
				//if("1".equals(companyDoc.getOrganization().getOrganizationStatus())&&companyDoc.getCompany().getStatus()==1){
				result = new JSONObject();
				result.put("ORG_NAME", companyDoc.getOrganization().getOrganizationName());
				result.put("ORG_ID", companyDoc.getOrganization().getOrganizationId());
				result.put("DOCUMENT_ID", companyDoc.getCompanyDocumentId());
				result.put("COMPANY_ID", companyDoc.getCompany().getCompanyId());
				result.put("COMPANY_NAME", companyDoc.getCompany().getCompanyName());
				result.put("DOCUMENT_TYPE_ID", companyDoc.getDocumentListType().getDocumentListTypeId());
				result.put("DOCUMENT_TYPE", companyDoc.getDocumentListType().getDocumentName());
				json_ImageArray=new JSONArray();
				for(CompanyDocumentImagePath tempObj : companyDoc.getCompanyDocumentImagePathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getCompanyDocumentImagePathId());
					json_ImageArray.put(tempJson);
				}
				result.put("FILE_NAME",json_ImageArray);
				companyDocArr.put(result);
				//}
			}
			result=new JSONObject();
			result.put("paginationCount", totalPage);
			companyDocArr.put(result);
			
		}
			return companyDocArr;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateCompanyUploadDocumentImage(Integer companyDocumentId,List<CompanyDocumentImagePath> documentList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		documentList = companyDocumentDao.updateCompanyUploadDocumentImage(companyDocumentId, documentList);
		for(CompanyDocumentImagePath tempObj : documentList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getCompanyDocumentImagePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject updateCompanyDocumentInfo(Integer documentTypeId,
			Integer companyId, Integer companyDocumentId) throws JSONException {
		JSONObject result= new JSONObject();
		CompanyDocument companyDocument = null;
		Company company = null;
		DocumentListType documentListType = null;
		int counter = 0;
		{
			companyDocument = companyDocumentDao.loadCompanyDocumentObjById(companyDocumentId);
			if(companyDocument.getDocumentListType().getDocumentListTypeId()!=documentTypeId){
				documentListType = documentListTypeDao.loadDocumentListTypeUsingDocumentListTypeId(documentTypeId);
				companyDocument.setDocumentListType(documentListType);counter++;
			}
			if(companyDocument.getCompany().getCompanyId()!=companyId){
				company = companyDao.loadCompanyObjectUsingCompanyId(companyId);
				companyDocument.setCompany(company);counter++;
			}
			result.put("MSG", "Company Uploaded Document Details Updated Successfully...!");
			if(counter>0){
				result.put("MSG", "Company Uploaded Document Details Updation failed...!");
				if(companyDocumentDao.updateCompanyDocumentObj(companyDocument)){
					result.put("RESULT", true);
					result.put("MSG", "Company Uploaded Document Details Updated Successfully...!");
					result.put("COMPANY_ID", companyDocument.getCompany().getCompanyId());
					result.put("COMPANY_NAME", companyDocument.getCompany().getCompanyName());
					result.put("DOCUMENT_TYPE_ID", companyDocument.getDocumentListType().getDocumentListTypeId());
					result.put("DOCUMENT_TYPE", companyDocument.getDocumentListType().getDocumentName());
				}
			}
			
		}
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCompanyDocumentFileImage(Integer companyDocumentPathId) {
		return companyDocumentDao.deleteCompanyDocumentFileImage(companyDocumentPathId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#saveCompanyCreditAndDiscount(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyCreditAndDiscount)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveCompanyCreditAndDiscount(Integer organizationId,Integer companyId, CompanyCreditAndDiscount companyCreditAndDiscount) {
		return companyDao.saveCompanyCreditAndDiscount(organizationId,companyId,companyCreditAndDiscount);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#deleteCompanyDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCompanyDetails(Integer companyId) {
		return companyDao.deleteCompanyDetails(companyId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#deleteCompanyBankDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCompanyBankDetails(Integer companyBankId) {
		// TODO Auto-generated method stub
		return companyDao.deleteCompanyBankDetails(companyBankId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#deleteCompanyDepo(java.lang.Integer)
	 
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCompanyDepo(Integer depoId) {
		// TODO Auto-generated method stub
		return companyDao.deleteCompanyDepo(depoId);
	}*/

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#deleteCompanyProduct(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCompanyProduct(Integer productId) {
		// TODO Auto-generated method stub
		return companyDao.deleteCompanyProduct(productId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#deleteCompanyUploadedDocs(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCompanyUploadedDocs(Integer companyDocId) {
		// TODO Auto-generated method stub
		return companyDao.deleteCompanyUploadedDocs(companyDocId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Integer> loadOrganizationBankIdListUsingCompanyId(
			Integer companyId) {
		return organizationBankCompanyJoinDAO.loadOrganizationBankIdListUsingCompanyId(companyId);
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public OrganizationBank loadOrganizationBankObjectUsingOrganizationBankId(
			Integer organizationBankId) {
		return organizationBankDao.loadOrganizationBankObjectUsingOrganizationBankId(organizationBankId);
	}

/*	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyDepo loadCompanyDepoObjectUsignCompanyDepoId(
			Integer companyDepoId) {
		return companyDepoDao.loadCompanyDepoObjectUsignCompanyDepoId(companyDepoId);
	}*/

	//Add by nutan
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CompanyProduct loadCompanyProductObjectUsignCompanyProductId(
			Integer companyProductId) {
		// TODO Auto-generated method stub
		return companyProductDao.loadCompanyProductObjectUsignCompanyProductId(companyProductId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#getCompanyCreditAndDiscountList(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<CompanyCreditAndDiscount> getCompanyCreditAndDiscountList(
			Integer organizationId) {
		return companyCreditAndDiscountDao.getCompanyCreditAndDiscountList(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.CompanyService#deleteCompanyCreditAndDiscountDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteCompanyCreditAndDiscountDetails(Integer companyCreditAndDiscountId) {
		return companyCreditAndDiscountDao.deleteCompanyCreditAndDiscountDetails(companyCreditAndDiscountId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean updateCompanyObj(Integer roleTypeId,Integer stateId,Integer districtId,Integer cityId,Company companyObj) {
		return companyDao.updateCompanyObj(roleTypeId,stateId,districtId,cityId,companyObj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateUserDetailsObj(UserDetails userDetailsObj) {
		userDetailsDao.updateUserDetailsObj(userDetailsObj);
	}

	/*@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Company> searchCreditAndDiscount(String searchOption,
			String searchText, Integer orgId) {
		// TODO Auto-generated method stub
		return companyDao.searchCreditAndDiscount(searchOption, searchText, orgId);}*/
	
}
