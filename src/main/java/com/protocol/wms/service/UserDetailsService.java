/**
 * 
 */
package com.protocol.wms.service;

import com.protocol.wms.model.UserDetails;

/**
 * @author admin
 *
 */
public interface UserDetailsService {

	public UserDetails loginValidate(String uname,String password);
	
	public UserDetails checkUserNameAlreadyExitOrNot(String uname);//Exists
	
	public Boolean checkUserNameAlreadyExitOrNot(Integer userDetailsId,String uname);//Exists
	
	public Integer saveUserDetails(UserDetails userDetailsObj);
	
	public UserDetails loadUserDetailsUsingUserDetailsId(Integer userDetailsId);

}
