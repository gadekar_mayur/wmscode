package com.protocol.wms.service;

import java.util.List;

import org.json.JSONObject;

import com.protocol.wms.model.District;

public interface DistrictService {

	public JSONObject getDistrictList(Integer stateId);
	
	public District loadDistricObjectUsingDstrictId(Integer districtId);

	public boolean updateDistrictObj(District districtObj);

	public List<District> loadDistrictList();
}
