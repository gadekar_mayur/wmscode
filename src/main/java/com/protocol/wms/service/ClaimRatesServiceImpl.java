/**
 * 
 */
package com.protocol.wms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.ClaimRatesDao;
import com.protocol.wms.model.ClaimRates;

/**
 * @author Sudhakar
 *
 */
@Service("ClaimRatesServiceImpl")
public class ClaimRatesServiceImpl implements ClaimRatesService{

	private ClaimRatesDao claimRatesDao;

	
	@Autowired
	@Qualifier("ClaimRatesDaoImpl")
	public void setClaimRatesDao(ClaimRatesDao claimRatesDao) {
		this.claimRatesDao = claimRatesDao;
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.service.ClaimRatesService#saveClaimRate(com.protocol.wms.model.ClaimRates)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveClaimRate(ClaimRates claimRates) {
		// TODO Auto-generated method stub
		return claimRatesDao.saveClaimRate(claimRates);
	}
	
	
}
