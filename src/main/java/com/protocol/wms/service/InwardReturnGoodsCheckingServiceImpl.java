/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.dao.CheckingDoneDao;
import com.protocol.wms.dao.CreditNoteDao;
import com.protocol.wms.dao.EmployeeDetailsDao;
import com.protocol.wms.dao.InwardReturnGoodsRegistrationDao;
import com.protocol.wms.dao.OrganizationDao;
import com.protocol.wms.dao.StockistDao;
import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.CreditNote;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnProducts;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.Stockist;

/**
 * @author Sudhakar
 *
 */
@Service("InwardReturnGoodsCheckingServiceImpl")
public class InwardReturnGoodsCheckingServiceImpl implements InwardReturnGoodsCheckingService{

	private InwardReturnGoodsRegistrationDao inwardReturnGoodsRegistrationDao;
	
	private OrganizationDao organizationDao;
	
	private StockistDao stockistDao;
	
	private EmployeeDetailsDao employeeDetailsDao;
	
	private CheckingDoneDao checkingDoneDao;
	
	private CreditNoteDao creditNoteDao;
	
	@Autowired
	@Qualifier("CreditNoteDaoImpl")
	public void setCreditNoteDao(CreditNoteDao creditNoteDao) {
		this.creditNoteDao = creditNoteDao;
	}

	@Autowired
	@Qualifier("InwardReturnGoodsRegistrationDaoImpl")
	public void setInwardReturnGoodsRegistrationDao(
			InwardReturnGoodsRegistrationDao inwardReturnGoodsRegistrationDao) {
		this.inwardReturnGoodsRegistrationDao = inwardReturnGoodsRegistrationDao;
	}

	@Autowired
	@Qualifier("OrganizationDaoImpl")
	public void setOrganizationDao(OrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	@Autowired
	@Qualifier("StockistDaoImpl")
	public void setStockistDao(StockistDao stockistDao) {
		this.stockistDao = stockistDao;
	}
	@Autowired
	@Qualifier("EmployeeDetailsDaoImpl")
	public void setEmployeeDetailsDao(EmployeeDetailsDao employeeDetailsDao) {
		this.employeeDetailsDao = employeeDetailsDao;
	}
	@Autowired
	@Qualifier("CheckingDoneDaoImpl")
	public void setCheckingDoneDao(CheckingDoneDao checkingDoneDao) {
		this.checkingDoneDao = checkingDoneDao;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#loadInwardReturnGoodsRegistrationListUsingOrganizationId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationId(
			Integer organizationId) {
		return inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationListUsingOrganizationId(organizationId);
	}



	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#loadOrganizationObjectUsingOrganiztionId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Organization loadOrganizationObjectUsingOrganiztionId(
			Integer organizationId) {
		return organizationDao.loadOrganizationObjectUsingOrganiztionId(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#loadStockistObjUsigStokistId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Stockist loadStockistObjUsigStokistId(Integer stokistId) {
		return stockistDao.loadStockistObjUsigStokistId(stokistId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#loadEmployeeObjectUsingEmployeeDetailsId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public EmployeeDetails loadEmployeeObjectUsingEmployeeDetailsId(
			Integer empDetailsId) {
		return employeeDetailsDao.loadEmployeeObjectUsingEmployeeDetailsId(empDetailsId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#saveCheckingDone(com.protocol.wms.model.CheckingDone)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveCheckingDone(CheckingDone checkingDoneObj) {
		return checkingDoneDao.saveCheckingDone(checkingDoneObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public InwardReturnGoodsRegistration loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(
			Integer InwardReturnGoodsRegistrationoId) {
		// TODO Auto-generated method stub
		return inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(InwardReturnGoodsRegistrationoId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#updateInwardReturnGoodsRegistrationObj(com.protocol.wms.model.InwardReturnGoodsRegistration)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateInwardReturnGoodsRegistrationObj(
			InwardReturnGoodsRegistration inwardReturnGoodsRegistrationObj) {
		inwardReturnGoodsRegistrationDao.updateInwardReturnGoodsRegistrationObj(inwardReturnGoodsRegistrationObj);		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOne(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOne(
			Integer organizationId) {
		return inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOne(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#saveCreditNoteObj(com.protocol.wms.model.CreditNote)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer saveCreditNoteObj(CreditNote creditNoteObj) {
		// TODO Auto-generated method stub
		return creditNoteDao.saveCreditNoteObj(creditNoteObj);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#loadCreditNoteObjectUsingCreditNoteId(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public CreditNote loadCreditNoteObjectUsingCreditNoteId(Integer creditNoteId) {
		return creditNoteDao.loadCreditNoteObjectUsingCreditNoteId(creditNoteId);
	}

	@Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
    public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne(
            Integer organizationId) {
         return inwardReturnGoodsRegistrationDao.loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne(organizationId);
    }

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardReturnGoodsCheckingService#addProductsToInwardReturnGoodsLRInCheckingPending(java.lang.Integer, java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean addProductsToInwardReturnGoodsLRInCheckingPending(
			Integer lrId, List<InwardReturnProducts> inwardReturnProductsList) {
		return inwardReturnGoodsRegistrationDao.addProductsToInwardReturnGoodsLRInCheckingPending(lrId, inwardReturnProductsList);
	}
	
}
