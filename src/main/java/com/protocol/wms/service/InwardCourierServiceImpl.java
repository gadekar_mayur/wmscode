/**
 * 
 */
package com.protocol.wms.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.dao.InwardCourierDao;
import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.InwardCourierRegistration;
import com.protocol.wms.model.InwardCourierRegistrationDocketFilePath;
import com.protocol.wms.model.Particulars;

/**
 * @author admin
 *
 */
@Service("InwardCourierServiceImpl")
public class InwardCourierServiceImpl implements InwardCourierService {

	@Autowired
	@Qualifier("InwardCourierDaoImpl")
	private InwardCourierDao inwardCourierDao;
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardCourierService#getParticularsDropdownList()
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Particulars> getParticularsDropdownList() {
		// TODO Auto-generated method stub
		return inwardCourierDao.getParticularsDropdownList();
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardCourierService#addInwardCourierRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardCourierRegistration, com.protocol.wms.model.AdvancedChequeInformation, java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean addInwardCourierRegistration(String companyStockistOption,Integer orgId,
			Integer companyId, Integer state, Integer district, Integer city,
			Integer stockistId, Integer particular, Integer companyBankId,
			InwardCourierRegistration inwardCourierRegistration,
			AdvancedChequeInformation advancedChequeInformation,
			List<ChequeNumberInfo> chequeNumberInfoList) {
		return inwardCourierDao.addInwardCourierRegistration(companyStockistOption,orgId,companyId ,state ,district ,city ,stockistId ,particular ,companyBankId,inwardCourierRegistration,advancedChequeInformation,chequeNumberInfoList);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardCourierService#editInwardCourierRegistration(java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardCourierRegistration, com.protocol.wms.model.AdvancedChequeInformation, java.util.List)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONObject editInwardCourierRegistration(String companyStockistOption,
			Integer orgId, Integer companyId, Integer state, Integer district,
			Integer city, Integer stockistId, Integer particular,
			Integer companyBankId,
			InwardCourierRegistration inwardCourierRegistration,
			AdvancedChequeInformation advancedChequeInformation,
			List<ChequeNumberInfo> chequeNumberInfoList) throws JSONException {
		JSONObject result = null;
		result = new JSONObject();
		result.put("MSG", "Inward Courier Details updation failed....!");
		if(inwardCourierDao.editInwardCourierRegistration(companyStockistOption,orgId,companyId ,state ,district ,city ,stockistId ,particular ,companyBankId,inwardCourierRegistration,advancedChequeInformation,chequeNumberInfoList)){
			result.put("COURIER_NAME",inwardCourierRegistration.getCourierName());
			result.put("REG_DATE",inwardCourierRegistration.getCourierRegistrationDate());
			if(inwardCourierRegistration.getCompany()==null)
				result.put("COMPANY","-");
			else
				result.put("COMPANY",inwardCourierRegistration.getCompany().getCompanyName());
			String stocistName="-";
			if(inwardCourierRegistration.getStockist()!=null)
				stocistName=inwardCourierRegistration.getStockist().getStockistName();
			result.put("STOCKIST_NAME",stocistName);
			result.put("CITY",inwardCourierRegistration.getCity().getCityName());
			result.put("DOCKET_NO",inwardCourierRegistration.getDocketNo());
			result.put("result", true);
			result.put("MSG", "Inward Courier Details updated successfully....!");
		}
		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray updateInwardCourierRegDocketFileImage(
			Integer inwardCourierRegId,
			List<InwardCourierRegistrationDocketFilePath> docketFileImgList) throws JSONException {
		JSONArray json_ImageArray= new JSONArray();
		JSONObject tempJson=null;
		docketFileImgList = inwardCourierDao.updateInwardCourierRegDocketFileImage(inwardCourierRegId, docketFileImgList);
		for(InwardCourierRegistrationDocketFilePath tempObj : docketFileImgList){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getInwardCourierRegistrationDocketFilePathId());
			json_ImageArray.put(tempJson);
		}
		return json_ImageArray;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardCourierService#inwardCourierRegistrationListing(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<InwardCourierRegistration> inwardCourierRegistrationListing(Integer organizationId) {
		return inwardCourierDao.inwardCourierRegistrationListing(organizationId);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardCourierService#searchInwardCourierRegistrationDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<Object,Object> searchInwardCourierRegistrationDetails(String searchOption,String searchText,String fromSpecialData,String toSpecialData,Integer organizationId,Integer from) {
		return inwardCourierDao.searchInwardCourierRegistrationDetails(searchOption, searchText, fromSpecialData, toSpecialData, organizationId,from);
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.service.InwardCourierService#deleteInwardCourierRegDetails(java.lang.Integer)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardCourierRegDetails(Integer inwardCourierRegId) {
		return inwardCourierDao.deleteInwardCourierRegDetails(inwardCourierRegId);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public Boolean deleteInwardCourierRegistrationDocketFileImage(
			Integer docketFileImageId) {
		return inwardCourierDao.deleteInwardCourierRegistrationDocketFileImage(docketFileImageId);
	}

	@Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
    public InwardCourierRegistration loadInwardCourierRegistrationObjectUsignInwardCourierRegId(
            Integer inwardCourierRegId) {
        return inwardCourierDao.loadInwardCourierRegistrationObjectUsignInwardCourierRegId(inwardCourierRegId);
    }

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor=Exception.class)
	public JSONArray loadInwardCourierRegistrationViewUsignInwardCourierRegId(Boolean editStatus,
			Integer inwardCourierRegId) throws JSONException, ParseException {
	  JSONArray json_data_array= new JSONArray();
      JSONObject json=null;
      JSONObject tempJson=null;
      JSONArray json_ImageArray = null;
      SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
      SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
      java.util.Date _24HourDt = null;
      Boolean companyStat=false,stockistStat=false;
	      
		InwardCourierRegistration inwardCourierRegistrationObj=inwardCourierDao.loadInwardCourierRegistrationObjectUsignInwardCourierRegId(inwardCourierRegId);
        json=new JSONObject();
        json.put("ORG_NAME", inwardCourierRegistrationObj.getOrganization().getOrganizationName());
        String [] reg_date_arr = inwardCourierRegistrationObj.getCourierRegistrationDate().toString().split("-");
        json.put("DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
        if(inwardCourierRegistrationObj.getCompany()==null)
        json.put("COMPANY_NAME", "");
        else{
        json.put("COMPANY_NAME", inwardCourierRegistrationObj.getCompany().getCompanyName());companyStat=true;
        }
        if(inwardCourierRegistrationObj.getStockist() != null){
        json.put("STOCKIST_NAME", inwardCourierRegistrationObj.getStockist().getStockistName());stockistStat=true;
        }
        else
        json.put("STOCKIST_NAME", "");
        CommonMethods common = new CommonMethods();
        json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(inwardCourierRegistrationObj.getSubmitDate().toString()));
        json.put("STATE", inwardCourierRegistrationObj.getState().getStateName());
        json.put("DISTRICT",inwardCourierRegistrationObj.getDistrict().getDistrictName());
        json.put("CITY", inwardCourierRegistrationObj.getCity().getCityName());
        json.put("COURIER_NAME", inwardCourierRegistrationObj.getCourierName());
        json.put("DELIVERY_PERSON", inwardCourierRegistrationObj.getDeliveryPerson());
        _24HourDt = _24HourSDF.parse(inwardCourierRegistrationObj.getTime().toString());
        json.put("TIME", _12HourSDF.format(_24HourDt));
        json.put("DOCKET_NUMBER",inwardCourierRegistrationObj.getDocketNo());
        json_ImageArray=new JSONArray();
		  for(InwardCourierRegistrationDocketFilePath tempObj : inwardCourierRegistrationObj.getInwardCourierRegistrationDocketFilePathList()){
			tempJson = new JSONObject();
			tempJson.put("IMAGE_PATH", tempObj.getImagePath());
			tempJson.put("IMAGE_ID", tempObj.getInwardCourierRegistrationDocketFilePathId());
			json_ImageArray.put(tempJson);
		  }
        json.put("DOCKET_FILE", json_ImageArray);
        json.put("REMARK", inwardCourierRegistrationObj.getRemark());
        json.put("PARTICULAR", inwardCourierRegistrationObj.getParticulars().getParticularsName());
        if(companyStat==false&&stockistStat==false){
      	  json.put("OTHER", inwardCourierRegistrationObj.getOther());
        }
        if(editStatus!=null&&editStatus==true){
      	  json.put("STATE_ID", inwardCourierRegistrationObj.getState().getStateId());
            json.put("DISTRICT_ID",inwardCourierRegistrationObj.getDistrict().getDistrictId());
            json.put("CITY_ID", inwardCourierRegistrationObj.getCity().getCityId());
      	  Integer particularId=inwardCourierRegistrationObj.getParticulars().getParticularsId();
      	  json.put("ORG_ID", inwardCourierRegistrationObj.getOrganization().getOrganizationId());
      	  json.put("PARTICULAR_ID",particularId );
      	  if(inwardCourierRegistrationObj.getStockist() != null){ stockistStat=true;
      		  json.put("STOCKIST_ID",inwardCourierRegistrationObj.getStockist().getStockistId());}
      	  if(inwardCourierRegistrationObj.getCompany()!=null){ companyStat=true;
      		  json.put("COMPANY_ID", inwardCourierRegistrationObj.getCompany().getCompanyId());}
      	  if(companyStat==true&&stockistStat==false){
      		  if(particularId==3)
      			  json.put("OPTION", "CompanyHavingCheque");
      		  else
      			  json.put("OPTION", "CompanyDontHaveCheque");
      	  }
      	  else if(companyStat==true&&stockistStat==true){
      		  if(particularId==3)
      			  json.put("OPTION", "CompanyStockistHavingCheque");
      		  else
      			  json.put("OPTION", "CompanyStockistDontHaveCheque");
      	  }
      	  else
      		  json.put("OPTION", "OTHER");
        }
        json_data_array.put(json);
        
		return json_data_array;
	}
	
}
