/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import com.protocol.wms.model.BloodGroup;

/**
 * @author Sudhakar
 *
 */
public interface BloodGroupService {
	
	public List<BloodGroup> loadBloodGroupList();
	
	public BloodGroup loadBloodGroupObjectUsingBloodGroupId(Integer bloodGroupId);
	
}
