/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentImagePath;
import com.protocol.wms.model.CartingAgentUploadedDocument;
import com.protocol.wms.model.CartingAgentUploadedDocumentImagePath;
import com.protocol.wms.model.CartingAgentVehiclePhotoImagePath;
import com.protocol.wms.model.UserDetails;

/**
 * @author admin
 *
 */

public interface CartingAgentService 
{

	public Boolean saveCartingAgentDetails(Integer organizationId,CartingAgent cartingAgent,UserDetails userDetails);
	
	public List<CartingAgent> getCartingAgentList(Integer organizationId);
	
	public String getCartingAgentDetailsById(Integer agentId);
	
	public Boolean saveCartingAgentUploadDocument(Integer organizationId,Integer cartingAgentId,Integer documentTypeId,CartingAgentUploadedDocument cartingAgentUploadedDocument);
	
	public JSONArray getCartingAgentUploadedDocList(Integer organizationId) throws Exception;
	
	public Boolean deleteCartingAgentDetails(Integer agentId);
	
	public Boolean deleteCartingAgentDocumentDetails(Integer docId);

	public Map<String,Object>searchCartingAgentsDetails(int searchOptioinINT,String searchText, Integer from, Integer organizationId);

	public String updateCartingAgentDetails(Integer organizationId,
			String agentName, Long mobile, MultipartFile agentPhoto,
			String address, String vehicleNo, String vehicleModel,
			MultipartFile vehiclePhoto, String userName, String password);
	
	public CartingAgent loadCartingAgentObjectUsingAgentId(Integer agentId);

	public Boolean updateCartingAgentObject(CartingAgent cartingAgent);
	
	public Integer insertCartingAgentDetilsAndDeleteOldCartingAgentDetails(Integer cartingAgentId, CartingAgent cartingAgent);
	
	public void updateCartingAgentImagesObjectByNewCartingAgentId(Integer oldCartingAgentId, Integer newCartingAgentId);
	
	public JSONArray updateCartingAgentPhotoImage(Integer cartingAgentId, List<CartingAgentImagePath> cartingAgentPhotoImgList) throws Exception;
	
	public Boolean deleteCartingAgentPhotoImage(Integer cartingAgentPhotoImageId);
	
	public JSONArray updateCartingAgentVehiclePhotoImage(Integer cartingAgentId, List<CartingAgentVehiclePhotoImagePath> cartingAgentVehiclePhotoImgList)throws Exception;
	
	public Boolean deleteCartingAgentVehiclePhotoImage(Integer cartingAgentVehiclePhotoImageId);
	
	public JSONObject updateCartingAgentDocumentInfo(Integer documentTypeId, Integer cartingAgentId, Integer cartingAgentDocId) throws Exception;
	
	public JSONArray updateCartingAgentUploadDocumentImage(Integer cartingAgentDocumentId, List<CartingAgentUploadedDocumentImagePath> documentList) throws Exception;
	
	public Boolean deleteCartingAgentDocumentFileImage(Integer cartingAgentUploadDocumentPathId);

	public JSONArray searchCartingAgentsUploadedDocumentDetails(
			String searchOptioin, String searchText, Integer attribute, Integer from) throws Exception;

}
 