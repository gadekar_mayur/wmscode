/**
 * 
 */
package com.protocol.wms.service;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.OrganizationDocument;
import com.protocol.wms.model.OrganizationDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
public interface OrganizationDocumentService {
	
	public Integer saveOrganizationDocument(OrganizationDocument organizationDocumentObj);
	
	public List<OrganizationDocument> loadOrganizationDocumentsListUsingOrganiztionId(Integer organizationId);
	
	//public JSONArray getOrganizationDocumentsListUsingOrganiztionId(Integer organizationId, Integer from) throws Exception;
	
	public JSONArray searchOrgaiztionDocumentDetails(String searchOption, String searchText, Integer organizationId, Integer from) throws Exception;

	public JSONObject updateOrganizationDocumentObj(Integer documentTypeId, Integer organiztionDocId) throws Exception;
	
	public JSONArray updateOrganizationUploadDocumentImage(Integer organizationDocumentId, List<OrganizationDocumentImagePath> documentList) throws Exception;
	
	public Boolean deleteOrganizationDocumentFileImage(Integer orgDocumentPathId);
}
