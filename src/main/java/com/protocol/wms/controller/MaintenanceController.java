/**
 * 
 */
package com.protocol.wms.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protocol.wms.model.City;
import com.protocol.wms.model.District;
import com.protocol.wms.model.State;
import com.protocol.wms.service.CityService;
import com.protocol.wms.service.DistrictService;
import com.protocol.wms.service.MaintenanceService;
import com.protocol.wms.service.OrganizationService;
import com.protocol.wms.service.StateService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/MaintenanceController")
public class MaintenanceController {

	private Logger log = Logger.getLogger(MaintenanceController.class.getClass());
	@Autowired
	private HttpSession session;
	@Autowired
	@Qualifier("MaintenanceServiceImpl")
	private MaintenanceService maintenanceService;
	
	@Autowired
	@Qualifier("OrganizationServiceImpl")
	private OrganizationService organizationService;
	
	@Autowired
	@Qualifier("StateServiceImpl")
	private StateService stateService;
	
	@Autowired
	@Qualifier("DistrictServiceImpl")
	private DistrictService districtService;
	
	@Autowired
	@Qualifier("CityServiceImpl")
	private CityService cityService;
	
	@RequestMapping(value="/addState",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String addStateToOrganization(@RequestParam(value="orgName",required=false) Integer orgId,
			@RequestParam(value="stateName",required=false) String stateName ){
		JSONObject result =  maintenanceService.saveStateForOrganization(orgId,stateName);
		return result.toString();
	} 
	
	@RequestMapping(value="/addDistrict",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String addDistrict(
			@RequestParam(value="state",required=false) Integer stateId,@RequestParam(value="districtName",required=false) String districtName ){
		return maintenanceService.saveDistrict(stateId,districtName).toString();
	} 
	
	@RequestMapping(value="/addCity",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String addCity(
			@RequestParam(value="district",required=false) Integer districtId,@RequestParam(value="cityName",required=false) String cityName ){
		return maintenanceService.saveCity(districtId,cityName).toString();
	} 
	
	@RequestMapping(value="/getStateList",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String getStateList(){
		
		JSONObject tempJson = null;
		JSONArray jsonArray = null;
	  List<State> stateList = null;
		
	  try{
			Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
			stateList = maintenanceService.getStateList(orgId);
			jsonArray = new JSONArray();
			for(State tempState:stateList)
			{
				tempJson = new JSONObject();
				tempJson.put("STATE_NAME", tempState.getStateName());
				tempJson.put("STATE_ID", tempState.getStateId());
				tempJson.put("ORG_ID", tempState.getOrganization().getOrganizationName());
				jsonArray.put(tempJson);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return jsonArray.toString();
	}
	//Delete State Details
	@RequestMapping(value="/deleteStateDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteStateDetails(@RequestParam(value="stateId",required=false) Integer stateId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(maintenanceService.deleteStateDetails(stateId)){
				result.put("MSG","State Details Deleted Successfully");
			}else{
				result.put("MSG","State Details Deletion Failed");
			}
		}catch(Exception e){
			e.printStackTrace();log.error(e);
			try{
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){e1.printStackTrace();log.error(e1);}
		}
		
		return result.toString();
	}
	@RequestMapping(value="/getDistrictList",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String getDistrictList(@RequestParam(value="stateId",required=false) Integer stateId){
		JSONObject tempJson = null;
		try{
			tempJson = maintenanceService.getDistrictList(stateId);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return tempJson.toString();
	}
	//Delete District Details
	@RequestMapping(value="/deleteDistrictDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteDistrictDetails(@RequestParam(value="districtId",required=false) Integer districtId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(maintenanceService.deleteDistrictDetails(districtId)){
				result.put("MSG","District Details Deleted Successfully");
			}else{
				result.put("MSG","District Details Deletion Failed");
			}
		}catch(Exception e){
			e.printStackTrace();log.error(e);
			try{
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){e1.printStackTrace();log.error(e1);}
		}
		
		return result.toString();
	}
	@RequestMapping(value="/getCityList",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String getCityList(@RequestParam(value="districtId",required=false) Integer districtId){
		JSONObject tempJson = null;
		try{
			tempJson = maintenanceService.getCityList(districtId);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return tempJson.toString();
	}
	//Delete City Details
	@RequestMapping(value="/deleteCityDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteCityDetails(@RequestParam(value="cityId",required=false) Integer cityId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(maintenanceService.deleteCityDetails(cityId)){
				result.put("MSG","City Details Deleted Successfully");
			}else{
				result.put("MSG","City Details Deletion Failed");
			}
		}catch(Exception e){
			e.printStackTrace();log.error(e);
			try{
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){e1.printStackTrace();log.error(e1);}
		}
		
		return result.toString();
	}
	
	@RequestMapping(value="/EditStateRegistrationDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editStateRegistrationDetails(@RequestParam(value="stateId",required=true) Integer stateId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 	
		{
			State stateObj=stateService.loadSateObjectUsingStateId(stateId);
			json=new JSONObject();
			json.put("ORG_ID", stateObj.getOrganization().getOrganizationId());
			json.put("ORG_NAME", stateObj.getOrganization().getOrganizationName());
			json.put("STATE_NAME", stateObj.getStateName());
			
			json_data_array.put(json);
}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/updateState",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String updateState(@RequestParam(value="organizationId",required=false) Integer organizationId,  
			@RequestParam(value="stateId",required=true) Integer stateId,@RequestParam(value="stateName",required=false) String stateName ){
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			json=new JSONObject();
			State stateObj=stateService.loadSateObjectUsingStateId(stateId);
			if(stateObj!=null)
			{
				stateObj.setStateName(stateName.toUpperCase());
				if(maintenanceService.stateAlreadyExistsOrNot(organizationId, stateName))
				{
					json.put("MSG", "State already Exist....! Please Enter another State");
				}
				else if(stateService.updateStateObj(stateObj))
				{
					json.put("MSG", "State Details Updated successfully..!");
					json.put("RESULT",true);
				}
				else
				{
					json.put("MSG", "State Details Updation Failed..!");
				}
			}
			json_data_array.put(json);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/EditDitrictRegistrationDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editDistrictRegistrationDetails(@RequestParam(value="districtId",required=true) Integer districtId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			District districtObj = districtService.loadDistricObjectUsingDstrictId(districtId);
			json=new JSONObject();
			json.put("STATE_ID", districtObj.getState().getStateId());
			json.put("ORG_ID", districtObj.getState().getOrganization().getOrganizationId());
			json.put("ORG_NAME", districtObj.getState().getOrganization().getOrganizationName());
			json.put("STATE_NAME", districtObj.getState().getStateName());
			json.put("DISTRICT_NAME", districtObj.getDistrictName());
			json_data_array.put(json);
}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/updateDistrict",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String updateDistrict(
			@RequestParam(value="districtId",required=false) Integer districtId,
			@RequestParam(value="stateId",required=false) Integer stateId,
			@RequestParam(value="districtName",required=false) String districtName ){
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			District districtObj = districtService.loadDistricObjectUsingDstrictId(districtId);
			json=new JSONObject();
			if(districtObj!=null)
			{
				districtObj.setDistrictName(districtName.toUpperCase());
				if(maintenanceService.districtAlreadyExistsOrNot(stateId, districtName)){
					json.put("MSG", "District already Exist....! Please Enter Other District..");
				}
				else if(districtService.updateDistrictObj(districtObj)){
					json.put("MSG", "District Details Update successfully..!");
					json.put("RESULT",true);
				}
				else
				{
					json.put("MSG", "District Details Updation Failed..!");
				}
			}
			json_data_array.put(json);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	
	@RequestMapping(value="/EditCityRegistrationDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editCityRegistrationDetails(@RequestParam(value="cityId",required=true) Integer cityId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			City cityObj = cityService.loadCityObjectUsingCityId(cityId);
			json=new JSONObject();
			json.put("STATE_ID", cityObj.getDistrict().getState().getStateId());
			json.put("ORG_ID", cityObj.getDistrict().getState().getOrganization().getOrganizationId());
			json.put("ORG", cityObj.getDistrict().getState().getOrganization().getOrganizationName());
			json.put("STATE_NAME", cityObj.getDistrict().getState().getStateName());
			json.put("DISTRICT_ID", cityObj.getDistrict().getDistrictId());
			json.put("DISTRICT_NAME", cityObj.getDistrict().getDistrictName());
			json.put("CITY_NAME", cityObj.getCityName());
			
			json_data_array.put(json);
}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/updateCity",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String updateCity(
			@RequestParam(value="districtId",required=false) Integer districtId,
			@RequestParam(value="cityId",required=false) Integer cityId,
			@RequestParam(value="cityName",required=false) String cityName ){
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			json=new JSONObject();
			City cityObj = cityService.loadCityObjectUsingCityId(cityId);
			if(cityObj != null)
			{
			cityObj.setCityName(cityName.toUpperCase());
			if(maintenanceService.cityAlreadyExistsOrNot(districtId, cityName)){
				json.put("MSG", "City already Exist....! Please enter other City..");
			}
			else if(cityService.updateCityObject(cityObj)){
				json.put("MSG", "City Details Update successfully..!");
				json.put("RESULT",true);
			}
			else
			{
				json.put("MSG", "City Details Updation Failed..!");
			}
			json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
			
	}
	
	///////////////////////////// searchOptionForState BY Vijay
	
	@RequestMapping(value="/searchOptionForState",method=RequestMethod.POST)
	private  @ResponseBody String searchOptionForState(
			@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText){
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		
		try 
		{
			List<State> stateList = maintenanceService.searchOptionForState(searchOption.trim(),searchText.trim());
			
			Iterator<State> stateIt = stateList.iterator();
			while(stateIt.hasNext()){
				State temp = stateIt.next();
				if("1".equals(temp.getOrganization().getOrganizationStatus())){
					json = new JSONObject();
				//organization = (Organization) session.get(Organization.class, temp.getOrganization().getOrganizationId());
				
				json.put("ORG_ID", temp.getOrganization().getOrganizationName());
				json.put("STATE_ID", temp.getStateId());
				json.put("STATE_NAME", temp.getStateName());
				json_data_array.put(json);
				}
			}
			
		}
		
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	
	///////////////////////// Search List for District/////
	
	@RequestMapping(value="/searchDistrictDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchDistrictDetails(
			@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText)	{
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		
		try 
		{
			List<District> districtList = maintenanceService.searchOptionForDistrict(searchOption.trim(),searchText.trim(),(Integer)session.getAttribute("ORGANIZATION_ID"));
			
			Iterator<District> districtIt = districtList.iterator();
			while(districtIt.hasNext()){
				District temp = districtIt.next();
				json = new JSONObject();
				json.put("ORG", temp.getState().getOrganization().getOrganizationName());
				json.put("STATE_NAME", temp.getState().getStateName());
				json.put("STATE_ID", temp.getState().getStateId());
				json.put("distId", temp.getDistrictId());
				json.put("distName", temp.getDistrictName());
				json_data_array.put(json);
			}
		}
		
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	
	
	
	@RequestMapping(value="/searchCityDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchCityDetails(
			@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText)	{
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		
		try 
		{
			List<City> cityList = maintenanceService.searchOptionForCity(searchOption.trim(),searchText.trim(),(Integer)session.getAttribute("ORGANIZATION_ID"));
			
			Iterator<City> cityIt = cityList.iterator();
			while(cityIt.hasNext()){
				City temp = cityIt.next();
				json = new JSONObject();
				json.put("ORG", temp.getDistrict().getState().getOrganization().getOrganizationName());
				json.put("STATE_NAME", temp.getDistrict().getState().getStateName());
				json.put("STATE_ID", temp.getDistrict().getState().getStateId());
				json.put("distId", temp.getDistrict().getDistrictId());
				json.put("distName", temp.getDistrict().getDistrictName());
				json.put("cityId", temp.getCityId());
				json.put("cityName", temp.getCityName());
				json_data_array.put(json);
			}
		}
		
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
		
	
}
