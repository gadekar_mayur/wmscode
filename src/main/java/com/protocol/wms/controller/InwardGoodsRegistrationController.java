/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.model.CompanyDepo;
import com.protocol.wms.model.InwardGoodsRegistration;
import com.protocol.wms.model.InwardGoodsRegistrationClaimLetter;
import com.protocol.wms.model.InwardGoodsRegistrationClaimLetterImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationClaimPicturesImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails;
import com.protocol.wms.model.InwardGoodsRegistrationInvoiceStnNoImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationLRImgPath;
import com.protocol.wms.service.InwardGoodsRegistrationService;

/**
 * @author Sudhakar
 *
 */
@Controller
@RequestMapping(value="/InwardGoodsRegistrationController")
public class InwardGoodsRegistrationController 
{
	
	private Logger log = Logger.getLogger(InwardGoodsRegistrationController.class.getClass());
	
	private InwardGoodsRegistrationService inwardGoodsRegistrationService;

	@Autowired
	@Qualifier("InwardGoodsRegistrationServiceImpl")
	public void setInwardGoodsRegistrationService(
			InwardGoodsRegistrationService inwardGoodsRegistrationService) {
		this.inwardGoodsRegistrationService = inwardGoodsRegistrationService;
	}
	
	@RequestMapping(value="/loadCompanyDepo",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadCompanyDepo(@RequestParam(value="orgId",required=true) Integer orgId,
			@RequestParam(value="companyId",required=true) Integer companyId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<CompanyDepo> companyDepoList=inwardGoodsRegistrationService.loadCompanyDepoListUsignCompanyIdAndOrgId(orgId, companyId);
			if(companyDepoList.size()>0)
			{
				for(CompanyDepo obj : companyDepoList)
				{
					json=new JSONObject();
					json.put("COMP_DEPO_ID", obj.getCompanyDepoId());
					json.put("COMP_DEPO_NAME",obj.getDepoName());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(log);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/saveInwardGoodsRegistration",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveInwardGoodsRegistration(
			@RequestParam(value="selectedOrganiztionNameId",required=true) Integer orgId,
			@RequestParam(value="comapnyId",required=true) Integer comapnyId,
			@RequestParam(value="sendingDepo",required=true) Integer sendingDepo,
			@RequestParam(value="transporterId",required=true) Integer transporterId,
			@RequestParam(value="date",required=true) String date,
			@RequestParam(value="NoofCases",required=true) Integer NoofCases,
			@RequestParam(value="LRNo",required=true) String LRNo,
			@RequestParam(value="LRNoImage",required=true) MultipartFile LRNoImage[],
			@RequestParam(value="driverName",required=true) String driverName,
			@RequestParam(value="unloadingCharges",required=true) Float unloadingCharges,
			@RequestParam(value="unloaderName",required=true) String unloaderName,
			@RequestParam(value="time",required=true) String time,
			@RequestParam(value="receivedBy",required=true) String receivedBy,
			@RequestParam(value="transportationCharges",required=true) Float transportationCharges,
			@RequestParam(value="hamaliCharges",required=true) Float hamaliCharges,
			@RequestParam(value="remark",required=true) String remark,
			@RequestParam(value="stnNo",required=true) String stnNo[],
			@RequestParam(value="stnImagePath",required=true) MultipartFile stnImagePath[],
			@RequestParam(value="stnDate",required=true) String stnDate[],
			@RequestParam(value="amount",required=true) Float amount[],
			@RequestParam(value="vatAmount",required=true) Float vatAmount[],
			@RequestParam(value="discountAmount",required=true) Float discountAmount[],
			@RequestParam(value="netAmount",required=true) Float netAmount[],
			@RequestParam(value="invoiceFileAttachedCount",required=true) Integer invoiceFileAttachedCount[],
			HttpServletRequest request) throws IOException
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		List<InwardGoodsRegistrationLRImgPath> lrImgList = null;
		InwardGoodsRegistrationLRImgPath tempLrImgPath = null;
		List<InwardGoodsRegistrationInvoiceStnNoImagePath> stnImgList = null;
		InwardGoodsRegistrationInvoiceStnNoImagePath stnNoImagePathObj = null;
		try 
		{
			//invoiceDetailsIndex---->for invoice form reference(which invoice form) and  invoiceAndFileDetailsIndex----->file reference for invoice form(which invoice form file it is)

			// save Inward Good Registration
			DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
			InwardGoodsRegistration inwardGoodsRegistrationObj=new InwardGoodsRegistration();
			// set registration date
			java.util.Date d1 = dtf.parse(date);
			inwardGoodsRegistrationObj.setInwardGoodsRegistrationDate(new java.sql.Date(d1.getTime()));
			inwardGoodsRegistrationObj.setNoOfCases(NoofCases);
			inwardGoodsRegistrationObj.setLRNo(LRNo);
			lrImgList = new ArrayList<InwardGoodsRegistrationLRImgPath>();
			// write lr image
			for(MultipartFile file : LRNoImage){
				if(file!=null){
					tempLrImgPath = new InwardGoodsRegistrationLRImgPath();
					tempLrImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					lrImgList.add(tempLrImgPath);
				}
			}
			inwardGoodsRegistrationObj.setInwardGoodsRegistrationLRImgPathList(lrImgList);
			inwardGoodsRegistrationObj.setDriverName(driverName);
			inwardGoodsRegistrationObj.setUnloadingCharges(unloadingCharges);
			inwardGoodsRegistrationObj.setUnloaderName(unloaderName);
			inwardGoodsRegistrationObj.setReceivedBy(receivedBy);
			inwardGoodsRegistrationObj.setTranportationCharges(transportationCharges);
			inwardGoodsRegistrationObj.setHamaliCharges(hamaliCharges);
			inwardGoodsRegistrationObj.setRemark(remark);
			inwardGoodsRegistrationObj.setStatus(1);
			inwardGoodsRegistrationObj.setClaimLetterStatus(0);
			// set time
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a"); // 12 hour format
			java.util.Date time1 =format.parse(time);
			java.sql.Time ppstime = new java.sql.Time(time1.getTime());
			inwardGoodsRegistrationObj.setTime(ppstime);
			// set submit date
		    inwardGoodsRegistrationObj.setSubmitDate(CommonMethods.setSubmitedDate());
			
		    // set organization object
		    inwardGoodsRegistrationObj.setOrganization(inwardGoodsRegistrationService.loadOrganizationObjectUsingOrganiztionId(orgId));
		    // set company object
		    inwardGoodsRegistrationObj.setCompany(inwardGoodsRegistrationService.loadCompanyObjectUsingCompanyId(comapnyId));
		    // set company depo object
		    inwardGoodsRegistrationObj.setCompanyDepo(inwardGoodsRegistrationService.loadCompanyDepoObjectUsignCompanyDepoId(sendingDepo));
		    // set transporter
		    inwardGoodsRegistrationObj.setTransporterDetails(inwardGoodsRegistrationService.loadTransporterDetailsUsingTransporterDetailsId(transporterId));
			// save Inward Good Registration
			Integer inwardGoodsRegistrationId=inwardGoodsRegistrationService.saveInwardGoodsRegistration(inwardGoodsRegistrationObj);
			if(inwardGoodsRegistrationId!=null)
			{
				// save Inward Goods Registration Invoice Details
				int fileIndexCounter = 0;
				for(int i=0;i<stnNo.length-1;i++)
				{
					InwardGoodsRegistrationInvoiceDetails invoiceDetailsObj=new InwardGoodsRegistrationInvoiceDetails();
					invoiceDetailsObj.setStnNO(stnNo[i]);
					invoiceDetailsObj.setGrossAmount(amount[i]);
					invoiceDetailsObj.setVatAmount(vatAmount[i]);
					invoiceDetailsObj.setDiscountAmount(discountAmount[i]);
					invoiceDetailsObj.setNetAmount(netAmount[i]);
					fileIndexCounter = fileIndexCounter + invoiceFileAttachedCount[i];
					int j = fileIndexCounter-invoiceFileAttachedCount[i];
					//Write Stn Images
					stnImgList = new ArrayList<InwardGoodsRegistrationInvoiceStnNoImagePath>();
					for(;j<fileIndexCounter;j++){
					stnNoImagePathObj = new InwardGoodsRegistrationInvoiceStnNoImagePath();
					stnNoImagePathObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(stnImagePath[j]));
					stnImgList.add(stnNoImagePathObj);
					}
					invoiceDetailsObj.setInwardGoodsRegistrationInvoiceStnNoImagePathList(stnImgList);
					// set date invoice date
					java.util.Date stnSubDate = dtf.parse(stnDate[i]);
					invoiceDetailsObj.setStnDate(new java.sql.Date(stnSubDate.getTime()));
					// set Inward Goods Registration object
					invoiceDetailsObj.setInwardGoodsRegistration(inwardGoodsRegistrationService.loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(inwardGoodsRegistrationId));
					
					// save Inward Goods Registration Invoice Details
					Integer inwardGoodsRegistrationInvoiceDetailsId=inwardGoodsRegistrationService.saveInwardGoodsRegistrationInvoiceDetails(invoiceDetailsObj);
					if(inwardGoodsRegistrationInvoiceDetailsId!=null)
					{
						System.out.println("inwardGoodsRegistrationInvoiceDetailsId=="+inwardGoodsRegistrationInvoiceDetailsId);
					}
				}
			}
			json=new JSONObject();
			json.put("MSG", "Inward Goods Registration added successfully");
			json_data_array.put(json);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//
	@RequestMapping(value="/editInwardGoodsRegistrationInvoiceDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editInwardGoodsRegistrationInvoiceDetails(
			@RequestParam(value="invoiceId",required=true) Integer invoiceId,
			@RequestParam(value="stnNo",required=true) String stnNo,
			//@RequestParam(value="stnImagePath",required=false) MultipartFile stnImagePath,
			@RequestParam(value="stnDate",required=true) String stnDate,
			@RequestParam(value="grossAmount",required=true) Float grossAmount,
			@RequestParam(value="vatAmount",required=true) Float vatAmount,
			@RequestParam(value="discountAmount",required=true) Float discountAmount,
			@RequestParam(value="netAmount",required=true) Float netAmount,
			HttpServletRequest request) throws IOException
	{
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		InwardGoodsRegistrationInvoiceDetails invoiceDetailsObj = null;
		JSONObject json=new JSONObject();
		try{
		invoiceDetailsObj=inwardGoodsRegistrationService.getInwardGoodsRegistrationInvoiceDetailsObjById(invoiceId);
		invoiceDetailsObj.setStnNO(stnNo);
		invoiceDetailsObj.setGrossAmount(grossAmount);
		invoiceDetailsObj.setVatAmount(vatAmount);
		invoiceDetailsObj.setDiscountAmount(discountAmount);
		invoiceDetailsObj.setNetAmount(netAmount);
		// set date invoice date
		java.util.Date stnSubDate = dtf.parse(stnDate);
		invoiceDetailsObj.setStnDate(new java.sql.Date(stnSubDate.getTime()));
		
		json.put("MSG", "Record Updation Failed....!");
		if(inwardGoodsRegistrationService.editInwardGoodsRegistrationInvoiceDetails(invoiceDetailsObj)){
			json.put("STN_NO", invoiceDetailsObj.getStnNO());
			//String STN_Image = obj.getLRImagePath();int y = STN_Image.lastIndexOf('/');STN_Image=STN_Image.substring(y+1);
			//json.put("STN_Image", invoiceDetailsObj.getStnNoImagePath());
			String [] stn_date_arr = invoiceDetailsObj.getStnDate().toString().split("-");
			json.put("STN_Date",stn_date_arr[1]+"/"+stn_date_arr[2]+"/"+stn_date_arr[0]);
			//jsonSubInvoice.put("STN_Date", invoiceObj.getStnDate());
			json.put("Gross_Amount", invoiceDetailsObj.getGrossAmount());
			json.put("Vat_Amount", invoiceDetailsObj.getVatAmount());
			json.put("Discount_Amount", invoiceDetailsObj.getDiscountAmount());
			json.put("Net_Amount", invoiceDetailsObj.getNetAmount());
			json.put("MSG", "Record Updated Successfully....!");
			json.put("RESULT",true);
		}
		}catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json.toString();
	}
	//
	@RequestMapping(value="/editInwardGoodsRegistration",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editInwardGoodsRegistration(
			@RequestParam(value="orgName",required=true) Integer orgId,
			@RequestParam(value="comapnyId",required=true) Integer comapnyId,
			@RequestParam(value="sendingDepo",required=true) Integer sendingDepo,
			@RequestParam(value="transporterId",required=true) Integer transporterId,
			
			@RequestParam(value="regDate",required=true) String date,
			@RequestParam(value="noOfCases",required=true) Integer NoofCases,
			@RequestParam(value="LR_NO",required=true) String LRNo,
			//@RequestParam(value="LR_File",required=false) MultipartFile LR_File[],
			@RequestParam(value="driverName",required=true) String driverName,
			@RequestParam(value="unloadingCharges",required=true) Float unloadingCharges,
			@RequestParam(value="unloaderName",required=true) String unloaderName,
			@RequestParam(value="time",required=true) String time,
			@RequestParam(value="receivedBy",required=true) String receivedBy,
			@RequestParam(value="transPorterCharges",required=true) Float transportationCharges,
			@RequestParam(value="hamaliCharges",required=true) Float hamaliCharges,
			@RequestParam(value="id",required=true) Integer inwardGoodsRegistrationId ,
			@RequestParam(value="remark",required=true) String remark,
			HttpServletRequest request) throws IOException
	{//sendingDepo transporterId noOfCases regDate LR_NO LR_File driverName unloadingCharges unloaderName time receivedBy transPorterCharges hamaliCharges remark
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
//		List<InwardGoodsRegistrationLRImgPath> lrImgList = null;
//		InwardGoodsRegistrationLRImgPath tempLrImgPath = null;
		try 
		{
			// save Inward Good Registration
			DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
			InwardGoodsRegistration inwardGoodsRegistrationObj=inwardGoodsRegistrationService.loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(inwardGoodsRegistrationId);
			// set registration date
			java.util.Date d1 = dtf.parse(date);
			inwardGoodsRegistrationObj.setInwardGoodsRegistrationDate(new java.sql.Date(d1.getTime()));
			inwardGoodsRegistrationObj.setNoOfCases(NoofCases);
			inwardGoodsRegistrationObj.setLRNo(LRNo);
			/*// write lr image
			for(MultipartFile file : LR_File){
				if(file!=null){
					tempLrImgPath = new InwardGoodsRegistrationLRImgPath();
					tempLrImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					lrImgList.add(tempLrImgPath);
				}
			}*/
			//inwardGoodsRegistrationObj.setInwardGoodsRegistrationLRImgPathList(lrImgList);
			inwardGoodsRegistrationObj.setDriverName(driverName);
			inwardGoodsRegistrationObj.setUnloadingCharges(unloadingCharges);
			inwardGoodsRegistrationObj.setUnloaderName(unloaderName);
			inwardGoodsRegistrationObj.setReceivedBy(receivedBy);
			inwardGoodsRegistrationObj.setTranportationCharges(transportationCharges);
			inwardGoodsRegistrationObj.setHamaliCharges(hamaliCharges);
			inwardGoodsRegistrationObj.setRemark(remark);

			// set time
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a"); // 12 hour format
			java.util.Date time1 =format.parse(time);
			java.sql.Time ppstime = new java.sql.Time(time1.getTime());
			inwardGoodsRegistrationObj.setTime(ppstime);
		   
			json=inwardGoodsRegistrationService.editInwardGoodsRegistration(orgId,comapnyId,sendingDepo,transporterId,inwardGoodsRegistrationObj);
			json_data_array.put(json);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//
	@RequestMapping(value="/updateInwardGoodsRegLrImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateInwardGoodsRegLrImage(
			@RequestParam(value="LR_File",required=true) MultipartFile LR_File[],
			@RequestParam(value="registrationId",required=true) Integer inwardGoodsRegistrationId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<InwardGoodsRegistrationLRImgPath> lrImgList = null;
		InwardGoodsRegistrationLRImgPath tempLrImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(LR_File!=null&&!(LR_File.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			lrImgList  = new ArrayList<InwardGoodsRegistrationLRImgPath>();
			for(MultipartFile file : LR_File){
				if(file!=null){
					tempLrImgPath = new InwardGoodsRegistrationLRImgPath();
					tempLrImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					lrImgList.add(tempLrImgPath);
				}
			}
			json_ImageArray = inwardGoodsRegistrationService.updateInwardGoodsRegLrImages(inwardGoodsRegistrationId, lrImgList);
			result.put("MSG", "File Upload successful...!");
			result.put("LR_IMAGE_PATH", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/updateInwardGoodsRegInvoiceStnImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateInwardGoodsRegInvoiceStnImage(
			@RequestParam(value="stnImagePath",required=true) MultipartFile stnImagePath[],
			@RequestParam(value="registrationId",required=true) Integer inwardGoodsRegistrationInvoiceId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<InwardGoodsRegistrationInvoiceStnNoImagePath> stnImgList = null;
		InwardGoodsRegistrationInvoiceStnNoImagePath tempStnImgPath = null;
		try 
		{
			result.put("MSG", "STN File Upload failed...!");
			if(stnImagePath!=null&&!(stnImagePath.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write STN image
			stnImgList  = new ArrayList<InwardGoodsRegistrationInvoiceStnNoImagePath>();
			for(MultipartFile file : stnImagePath){
				if(file!=null){
					tempStnImgPath = new InwardGoodsRegistrationInvoiceStnNoImagePath();
					tempStnImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					stnImgList.add(tempStnImgPath);
				}
			}
			json_ImageArray = inwardGoodsRegistrationService.updateInwardGoodsRegInvoiceStnImage(inwardGoodsRegistrationInvoiceId, stnImgList);
			result.put("MSG", "STN File Upload successful.....!");
			result.put("STN_Image", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/updateInwardGoodsRegClaimLetterImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateInwardGoodsRegClaimLetterImage(
			@RequestParam(value="claimLetter",required=true) MultipartFile claimLetter[],
			@RequestParam(value="registrationId",required=true) Integer inwardGoodsRegistrationClaimLetterId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<InwardGoodsRegistrationClaimLetterImagePath> claimLetterImgList = null;
		InwardGoodsRegistrationClaimLetterImagePath tempClaimLetterImgPath = null;
		try 
		{
			result.put("MSG", "Claim Letter File Upload failed...!");
			if(claimLetter!=null&&!(claimLetter.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write STN image
			claimLetterImgList  = new ArrayList<InwardGoodsRegistrationClaimLetterImagePath>();
			for(MultipartFile file : claimLetter){
				if(file!=null){
					tempClaimLetterImgPath = new InwardGoodsRegistrationClaimLetterImagePath();
					tempClaimLetterImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					claimLetterImgList.add(tempClaimLetterImgPath);
				}
			}
			json_ImageArray = inwardGoodsRegistrationService.updateInwardGoodsRegClaimLetterImage(inwardGoodsRegistrationClaimLetterId, claimLetterImgList);
			
			result.put("MSG", "Claim Letter File Upload successful.....!");
			result.put("CLAIM_LETTER", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/updateInwardGoodsRegClaimPicturesImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateInwardGoodsRegClaimPicturesImage(
			@RequestParam(value="uploadPictures",required=true) MultipartFile uploadPictures[],
			@RequestParam(value="registrationId",required=true) Integer inwardGoodsRegistrationClaimLetterId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<InwardGoodsRegistrationClaimPicturesImagePath> claimPicturesImgList = null;
		InwardGoodsRegistrationClaimPicturesImagePath tempClaimPicturesImgPath = null;
		try 
		{
			result.put("MSG", "Claim Pictures File Upload failed...!");
			if(uploadPictures!=null&&!(uploadPictures.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write STN image
			claimPicturesImgList  = new ArrayList<InwardGoodsRegistrationClaimPicturesImagePath>();
			for(MultipartFile file : uploadPictures){
				if(file!=null){
					tempClaimPicturesImgPath = new InwardGoodsRegistrationClaimPicturesImagePath();
					tempClaimPicturesImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					claimPicturesImgList.add(tempClaimPicturesImgPath);
				}
			}
			json_ImageArray = inwardGoodsRegistrationService.updateInwardGoodsRegClaimPicturesImage(inwardGoodsRegistrationClaimLetterId, claimPicturesImgList);
			
			result.put("MSG", "Claim Pictures File Upload successful.....!");
			result.put("CLAIM_UPLOAD_PICTURE", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	@RequestMapping(value="/loadInwardGoodsRegistrationList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadInwardGoodsRegistrationList(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<InwardGoodsRegistration> inwardGoodsRegistrationList=inwardGoodsRegistrationService.loadInwardGoodsRegistrationListUsignOrganizationId((Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
			if(inwardGoodsRegistrationList.size()>0)
			{
				for(InwardGoodsRegistration obj : inwardGoodsRegistrationList)
				{
					json=new JSONObject();
					json.put("GOODS_REGISTRATION_ID", obj.getInwardGoodsRegistrationId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("TRANS", obj.getTransporterDetails().getTransporterName());
					json.put("DEPO", obj.getCompanyDepo().getDepoName());
					json.put("LR_NO", obj.getLRNo());
					json.put("DRIVER_NAME", obj.getDriverName());
					json.put("CLAIM_LETTER_STATUS", obj.getClaimLetterStatus());
					String str = obj.getInwardGoodsRegistrationDate().toString();
					json.put("REG_DATE", str.substring(8,10)+"/"+str.substring(5,7)+"/"+ str.substring(0,4));
					json_data_array.put(json);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	//
	@RequestMapping(value="/searchInwardGoodsRegDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchInwardGoodsRegDetails(
			@RequestParam(value="selectedValue",required=false) String selectedValue,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="fromSpecialData",required=false) String fromSpecialData,
			@RequestParam(value="toSpecialData",required=false) String toSpecialData,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		java.util.Map<Object, Object>map = null;
		try 
		{
			map=inwardGoodsRegistrationService.searchInwardGoodsRegDetails(selectedValue,searchText,fromSpecialData,toSpecialData,(Integer)request.getSession().getAttribute("ORGANIZATION_ID"),from);
			List<InwardGoodsRegistration> inwardGoodsRegistrationList = (List<InwardGoodsRegistration>) map.get("inwardGoodsList");
			Integer totalPage = (Integer) map.get("totalPages");
			if(inwardGoodsRegistrationList.size()>0)
			{
				for(InwardGoodsRegistration obj : inwardGoodsRegistrationList)
				{
					json=new JSONObject();
					json.put("GOODS_REGISTRATION_ID", obj.getInwardGoodsRegistrationId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("TRANS", obj.getTransporterDetails().getTransporterName());
					json.put("DEPO", obj.getCompanyDepo().getDepoName());
					json.put("LR_NO", obj.getLRNo());
					json.put("DRIVER_NAME", obj.getDriverName());
					json.put("CLAIM_LETTER_STATUS", obj.getClaimLetterStatus());
					String str = obj.getInwardGoodsRegistrationDate().toString();
					json.put("REG_DATE", str.substring(8,10)+"/"+str.substring(5,7)+"/"+ str.substring(0,4));
					json_data_array.put(json);
				}
			}
			json=new JSONObject();
			json.put("paginationCount", totalPage);
			json_data_array.put(json);

		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	@RequestMapping(value="loadInwardGoodsRegistrationUsignId",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadInwardGoodsRegistrationUsignId(
	@RequestParam(value="inwardGoodsRegistrationId",required=true) Integer inwardGoodsRegistrationId)
	{
	
		JSONArray json_data_array = null;
		try 
		{
			json_data_array = inwardGoodsRegistrationService.inwardGoodsRegistrationView(inwardGoodsRegistrationId);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	
	@RequestMapping(value="saveClaimLetter",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveClaimLetter(
	@RequestParam(value="inwardGoodsRegistrationId",required=true) Integer inwardGoodsRegistrationId
	,@RequestParam(value="claimLetter",required=false) MultipartFile claimLetter[]
	,@RequestParam(value="uploadPictures",required=false) MultipartFile uploadPictures[]
	,@RequestParam(value="description",required=true) String description)
	{
		List<InwardGoodsRegistrationClaimLetterImagePath> claimLetterImageList = null;
		InwardGoodsRegistrationClaimLetterImagePath tempClaimLetterImgObj =null;
		List<InwardGoodsRegistrationClaimPicturesImagePath> claimPicturesImageList = null;
		InwardGoodsRegistrationClaimPicturesImagePath tempClaimPicturesImgObj = null;
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			// save add claim letter
			InwardGoodsRegistrationClaimLetter claimObj=new InwardGoodsRegistrationClaimLetter();
			claimLetterImageList = new ArrayList<InwardGoodsRegistrationClaimLetterImagePath>();
			for(MultipartFile file : claimLetter){
				if(file!=null){
					tempClaimLetterImgObj = new InwardGoodsRegistrationClaimLetterImagePath();
					tempClaimLetterImgObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					claimLetterImageList.add(tempClaimLetterImgObj);
				}
			}
			claimObj.setInwardGoodsRegistrationClaimLetterImagePathList(claimLetterImageList);
			claimPicturesImageList = new ArrayList<InwardGoodsRegistrationClaimPicturesImagePath>();
			for(MultipartFile file : uploadPictures){
				if(file!=null){
					tempClaimPicturesImgObj = new InwardGoodsRegistrationClaimPicturesImagePath();
					tempClaimPicturesImgObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					claimPicturesImageList.add(tempClaimPicturesImgObj);
				}
			}
			claimObj.setInwardGoodsRegistrationClaimPicturesImagePathList(claimPicturesImageList);
			claimObj.setDescription(description);
		    claimObj.setSubmitDate(CommonMethods.setSubmitedDate());
			
			//set Inward Goods Registration object
			claimObj.setInwardGoodsRegistration(inwardGoodsRegistrationService.loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(inwardGoodsRegistrationId));
			
			// save Inward Goods Registration Claim Letter object
			Integer inwardGoodsRegistrationClaimLetterId=inwardGoodsRegistrationService.saveInwardGoodsRegistrationClaimLetter(claimObj);
			if(inwardGoodsRegistrationClaimLetterId!=null)
			{
				//update inward Goods Registration object
				InwardGoodsRegistration inwardGoodsRegistrationObj=inwardGoodsRegistrationService.loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(inwardGoodsRegistrationId);
				inwardGoodsRegistrationObj.setClaimLetterStatus(1);
				// update object
				inwardGoodsRegistrationService.updateInwardGoodsRegistrationObject(inwardGoodsRegistrationObj);
			}
			json=new JSONObject();
			json.put("MSG", "Claim Letter upload successfuly");
			json_data_array.put(json);
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//
	@RequestMapping(value="editClaimLetter",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editClaimLetter(
	@RequestParam(value="claimLetterId",required=true) Integer claimLetterId
	//,@RequestParam(value="claimLetter",required=false) MultipartFile claimLetter
	//,@RequestParam(value="uploadPictures",required=false) MultipartFile uploadPictures
	,@RequestParam(value="description",required=true) String description)
	{
		JSONObject json=null;
		try 
		{
			// save add claim letter
			InwardGoodsRegistrationClaimLetter claimObj=inwardGoodsRegistrationService.getInwardGoodsRegistrationClaimLetterObj(claimLetterId);
			claimObj.setDescription(description);
			
			/*if(claimLetter!=null){
				// write claim image path
				byte[] bytes = claimLetter.getBytes();
				String orgNameFile = claimLetter.getOriginalFilename();
				int index = orgNameFile.indexOf(".");
				String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
				File serverFile = new File(Constant.IMAGE_PATH+ fileName);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				claimObj.setClaimLetterImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
				stream.write(bytes);
				stream.close();
			}
			if(uploadPictures!=null){
				// write picture image
				byte[] bytes1 = uploadPictures.getBytes();
				String orgNameFile1 = uploadPictures.getOriginalFilename();
				int index1 = orgNameFile1.indexOf(".");
				String fileName1=UUID.randomUUID().toString()+orgNameFile1.substring(index1);
				File serverFile1 = new File(Constant.IMAGE_PATH+ fileName1);
				BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
				claimObj.setMobileImagePath(Constant.DATABASE_IMAGE_PATH+ fileName1);
				stream1.write(bytes1);
				stream1.close();
			}*/
			// edit Inward Goods Registration Claim Letter object
			json=new JSONObject();
			json.put("MSG", "Claim Letter Edit Failed...!");
			if(inwardGoodsRegistrationService.editInwardGoodsRegistrationClaimLetter(claimObj))
			{
				json.put("MSG", "Claim Letter Edit successfuly....!");
				//json.put("CLAIM_DESC",claimObj.getDescription());
				//json.put("CLAIM_LETTER", claimObj.getClaimLetterImagePath());
				//json.put("CLAIM_UPLOAD_PICTURE", claimObj.getMobileImagePath());
				json.put("RESULT", true);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json.toString();
	}
	//Delete Inward Goods Registration  Details
	@RequestMapping(value="/deleteInwardGoodsRegistration",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardGoodsRegistration(@RequestParam(value="inwardGoodsRegistrationId",required=false) Integer inwardGoodsRegistrationId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(inwardGoodsRegistrationService.deleteInwardGoodsRegistrationDetails(inwardGoodsRegistrationId)){
				result.put("MSG","Inward Goods Registration Details  Deleted Successfully");
			}else{
				result.put("MSG","Inward Goods Registration Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//Delete Inward Goods Registration Lr Images Details
	@RequestMapping(value="/deleteInwardGoodsRegistrationLrImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardGoodsRegistrationLrImage(@RequestParam(value="lrImageId",required=false) Integer lrImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(inwardGoodsRegistrationService.deleteInwardGoodsRegistrationLrImage(lrImageId)){
				result.put("RESULT",true);
				result.put("MSG","Inward Goods Registration LR Image Deleted Successfully");
			}else{
				result.put("MSG","Inward Goods Registration LR Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/deleteInwardGoodsRegistrationClaimLetterImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardGoodsRegistrationClaimLetterImage(@RequestParam(value="claimLetterImageId",required=false) Integer claimLetterImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(inwardGoodsRegistrationService.deleteInwardGoodsRegistrationClaimLetterImage(claimLetterImageId)){
				result.put("RESULT",true);
				result.put("MSG","Inward Goods Registration claim Letter Image Deleted Successfully");
			}else{
				result.put("MSG","Inward Goods Registration claim Letter Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}//
	@RequestMapping(value="/deleteInwardGoodsRegistrationClaimPictureImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardGoodsRegistrationClaimPictureImage(@RequestParam(value="claimPictureImageId",required=false) Integer claimPictureImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(inwardGoodsRegistrationService.deleteInwardGoodsRegistrationClaimPictureImage(claimPictureImageId)){
				result.put("RESULT",true);
				result.put("MSG","Inward Goods Registration claim Picture Image Deleted Successfully");
			}else{
				result.put("MSG","Inward Goods Registration claim Picture Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//Delete Inward Goods Registration Invoice Stn Images Details
	@RequestMapping(value="/deleteInwardGoodsRegInvoiceStnImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardGoodsRegInvoiceStnImage(@RequestParam(value="stnImageId",required=false) Integer stnImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(inwardGoodsRegistrationService.deleteInwardGoodsRegInvoiceStnImage(stnImageId)){
				result.put("RESULT",true);
				result.put("MSG","Inward Goods Registration Invoice Details STN Image Deleted Successfully");
			}else{
				result.put("MSG","Inward Goods Registration Invoice Details STN Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
}
