
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyBank;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.CompanyDepo;
import com.protocol.wms.model.CompanyDocument;
import com.protocol.wms.model.CompanyDocumentImagePath;
import com.protocol.wms.model.CompanyExecutive;
import com.protocol.wms.model.CompanyProduct;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationBank;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.service.CompanyBankService;
import com.protocol.wms.service.CompanyCreditAndDiscountService;
import com.protocol.wms.service.CompanyDepoService;
import com.protocol.wms.service.CompanyExecutiveService;
import com.protocol.wms.service.CompanyProductService;
import com.protocol.wms.service.CompanyService;
import com.protocol.wms.service.OrganizationService;
import com.protocol.wms.service.RoleTypeService;
import com.protocol.wms.service.UserDetailsService;

@Controller
@RequestMapping(value="/CompanyController")

public class CompanyController {
	private Logger log = Logger.getLogger(CompanyController.class.getClass());
	
	@Autowired
	@Qualifier("CompanyExecutiveServiceImpl")
	private CompanyExecutiveService companyExecutiveService;
	
	@Autowired
	@Qualifier("CompanyBankServiceImpl")
	private CompanyBankService companyBankService;
	
	@Autowired
	@Qualifier("CompanyDepoServiceImpl")
	private CompanyDepoService companyDepoService;
	@Autowired
	@Qualifier("CompanyServiceImpl")
	private CompanyService companyService;
	
//	@Autowired
//	@Qualifier("CartingAgentTripCountServiceImpl")
//	private CartingAgentTripCountService cartingAgentTripCountService;
	
	@Autowired
	@Qualifier("CompanyCreditAndDiscountServiceImpl")
	private CompanyCreditAndDiscountService companyCreditAndDiscountService;
	
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	private UserDetailsService userDetailsService;
	
	@Autowired
	@Qualifier("OrganizationServiceImpl")
	private OrganizationService organizationservices;
	
	@Autowired
	@Qualifier("CompanyProductServiceImpl")
	private CompanyProductService companyProductService;
	
	@Autowired
	@Qualifier("RoleTypeerviceImpl")
	private RoleTypeService roleTypeService;
	@Autowired
	private HttpSession session;
	
	@RequestMapping(value="/getCompanies",method=RequestMethod.POST)
	private  @ResponseBody String getCompanyList(@RequestParam(value="orgId",required=false) Integer orgId){
		return companyService.getCompanyList(orgId).toString();
	}
	
	@RequestMapping(value="/getCompanyDropDownForCredit_Discount",method=RequestMethod.POST)
	private  @ResponseBody String getCompanyDropDownForCredit_Discount(@RequestParam(value="orgId",required=false) Integer orgId){
		return companyService.getCompanyDropDownForCredit_Discount(orgId).toString();
	}

	@RequestMapping(value="/getCompanyList",method=RequestMethod.POST)
	private  @ResponseBody String getCompanyListAccToStockist(@RequestParam(value="stockistId",required=false) Integer stockistId,
			@RequestParam(value="orgId",required=false) Integer orgId){
		return companyService.getCompanyListAccToStockist(stockistId,orgId).toString();
	}
	
	@RequestMapping(value="/addCompanyDetails",method=RequestMethod.POST)
	private  @ResponseBody String saveCompanyDetails(@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=true) String companyName,
			@RequestParam(value="state",required=false) Integer stateId,@RequestParam(value="district",required=false) Integer districtId ,
			@RequestParam(value="city",required=false) Integer cityId,@RequestParam(value="contactPerson",required=false) String contactPerson ,
			@RequestParam(value="mobile",required=false) Long mobile, 
			@RequestParam(value="website",required=false) String website,@RequestParam(value="fax",required=false) String fax,
			@RequestParam(value="service_tax_no",required=false) String service_tax_no,@RequestParam(value="roleType",required=false) Integer roleTypeId,
			@RequestParam(value="address",required=true) String address,@RequestParam(value="telephone",required=true) Long telephone,
			@RequestParam(value="email",required=true) String email,@RequestParam(value="vat",required=true) String vat,
			@RequestParam(value="cst",required=false) String cst,@RequestParam(value="pan_no",required=true) String pan_no,
			@RequestParam(value="uname",required=true) String userName,@RequestParam(value="password",required=true) String password){
		Company company = null;
		UserDetails userDetails=null;
		JSONObject result = new JSONObject();
		try{
			//userDetails = companyService.checkUserNameAlreadyExitOrNot(userName);
			if(companyService.checkUserNameAlreadyExitOrNot(userName)){
				result.put("MSG","User Name already exists");
				result.put("FLAG", true);
				return result.toString();
			}
			if(!"".equals(userName)&&!"".equals(password)&&userName!=null&&password!=null){
				userDetails = new UserDetails();
				userDetails.setUserName(userName);
				userDetails.setPassword(password);
			}
			
			company = new Company();
			company.setCompanyName(companyName);
			company.setCompanyPerson(contactPerson);
			company.setCompanyMobileNo(mobile);
			company.setCompanyWebSite(website);
			company.setCompanyFaxNumber(fax);
			company.setCompanyServiceTaxNo(service_tax_no);
			company.setCompanyAddress(address);
			company.setCompanyTelephone(telephone);
			company.setCompanyEmailId(email);
			company.setCompanyVat(vat);
			company.setCompanyCST(cst);
			company.setCompanyPanNo(pan_no);
			company.setStatus(1);
			company.setCompanyCreditAndDiscountStatus(0);
			
			//set company  reg date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
		    company.setSubmitDate(date);
		    //
		    
		    if(companyService.saveCompanyDetails(orgId,stateId,districtId,cityId,roleTypeId,userDetails,company)){
		    	result.put("result", true);
		    	result.put("MSG", "Company Details Added Succefully....!");
		    }else{
		    	result.put("result", false);
		    	result.put("MSG", "Company Details Added Failed....!");
		    }
		}catch(JSONException e){
			try {
				result.put("result", false);
				result.put("MSG", "Operation Failed....!");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
	    	
		}catch(Exception e){
			try {
				result.put("result", false);
				result.put("MSG", "Operation Failed....!");
				e.printStackTrace();
				log.error(e);
			} catch (JSONException e1) {
				e1.printStackTrace();
				log.error(e);
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/companyDetailsListing",method=RequestMethod.POST)
	private  @ResponseBody String companyDetailsList(){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		JSONObject result = null;
		JSONArray companyArr = null;
		List<Company>companyList = null;
		Company tempCompany = null;
		Iterator<Company> cmpanyItr = null;
		try{
			companyArr = new JSONArray();
			companyList = companyService.companyDetailsList(orgId);
			cmpanyItr = companyList.iterator();
			while(cmpanyItr.hasNext()){
				tempCompany = cmpanyItr.next();
				if("1".equals(tempCompany.getOrganization().getOrganizationStatus())){
				result = new JSONObject();
				result.put("ORG_NAME", tempCompany.getOrganization().getOrganizationName());
				result.put("COMPANY_ID", tempCompany.getCompanyId());
				result.put("COMPANY_NAME", tempCompany.getCompanyName());
				result.put("CONTACT_PERSON", tempCompany.getCompanyPerson());
				if(tempCompany.getCompanyMobileNo() == null)
				{
				result.put("MOBILE" , "");
				}else{
					result.put("MOBILE" , tempCompany.getCompanyMobileNo());
				}
				result.put("EMAIL", tempCompany.getCompanyEmailId());
				companyArr.put(result);
				}
			}
			result = new JSONObject();
			result.put("companyArray", companyArr);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/searchCompanyDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchCompanyDetails(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=true) Integer from){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		JSONObject result = null;
		JSONArray companyArr = null;
		List<Company>companyList = null;
		Company tempCompany = null;
		Iterator<Company> cmpanyItr = null;
		try{
			companyArr = new JSONArray();
			Map<String , Object> map = companyService.searchCompanyDetails(searchOption,searchText, from, orgId);
			companyList = (List<Company>) map.get("companyList");
			Integer totalPages = (Integer)map.get("totalPages");
			cmpanyItr = companyList.iterator();
			while(cmpanyItr.hasNext()){
				tempCompany = cmpanyItr.next();
				//if("1".equals(tempCompany.getOrganization().getOrganizationStatus())){
				result = new JSONObject();
				result.put("ORG_NAME", tempCompany.getOrganization().getOrganizationName());
				result.put("COMPANY_ID", tempCompany.getCompanyId());
				result.put("COMPANY_NAME", tempCompany.getCompanyName());
				result.put("CONTACT_PERSON", tempCompany.getCompanyPerson());
				if(tempCompany.getCompanyMobileNo()==null)
					result.put("MOBILE", "");
				else
					result.put("MOBILE" , tempCompany.getCompanyMobileNo());
				result.put("EMAIL", tempCompany.getCompanyEmailId());
				companyArr.put(result);
				//}
			}
			result = new JSONObject();
			result.put("paginationCount", totalPages);
			result.put("companyArray", companyArr);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	
	@RequestMapping(value="/addCompanyBankDetails",method=RequestMethod.POST)
	private  @ResponseBody String saveCompanyBankDetails(@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="bankName",required=false) String bankName ,@RequestParam(value="accNo",required=false) String accNo, 
			@RequestParam(value="branch",required=false) String branch,@RequestParam(value="ifsc",required=false) String ifsc){
		CompanyBank companyBank =null;
		JSONObject result = new JSONObject();
		try{
			companyBank = new CompanyBank();
			companyBank.setCompanyBankName(bankName);
			companyBank.setCompanyBankAccountNo(accNo);
			companyBank.setCompanyBankBranch(branch);
			companyBank.setCompanyBankIfscCode(ifsc);
			companyBank.setStatus(1);
			
			//set company  reg date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
		    companyBank.setSubmitDate(date);
		    //
		    
			result.put("result",false);
			if(companyService.saveCompanyBankDetails(orgId,companyId,companyBank)){
				result.put("result",true);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	
	@RequestMapping(value="/updateCompanyBankDetails",method=RequestMethod.POST)
	private  @ResponseBody String updateCompanyBankDetails(@RequestParam(value="companyBankId",required=true) Integer companyBankId,
			@RequestParam(value="bankName",required=true) String bankName ,@RequestParam(value="accountNo",required=true) String accNo, 
			@RequestParam(value="branchName",required=true) String branch,@RequestParam(value="ifscCode",required=true) String ifsc){
		CompanyBank companyBank =null;
		JSONObject result =null;
		try{
			companyBank = companyBankService.loadCompanyBankUsignCompanyBankId(companyBankId);
			companyBank.setCompanyBankName(bankName);
			companyBank.setCompanyBankAccountNo(accNo);
			companyBank.setCompanyBankBranch(branch);
			companyBank.setCompanyBankIfscCode(ifsc);
			result = new JSONObject();
			result.put("MSG","Company Bank Details updation failed....!");
			if(companyBankService.updateCompanyBankDetails(companyBank)){
				result.put("MSG","Company Bank Details updated successfully....!");
				result.put("RESULT",true);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	
	@RequestMapping(value="/companyBankDetailsListing",method=RequestMethod.POST)
	private  @ResponseBody String companyBankDetailsListing(){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		JSONObject result = null;
		JSONArray companyBankArr = null;
		List<CompanyBank>companyBankList = null;
		CompanyBank tempCompany = null;
		Iterator<CompanyBank> cmpanyBankItr = null;
		try{
			companyBankArr = new JSONArray();
			companyBankList = companyService.companyBankDetailsListing(orgId);
			cmpanyBankItr = companyBankList.iterator();
			while(cmpanyBankItr.hasNext()){
				tempCompany = cmpanyBankItr.next();
				if("1".equals(tempCompany.getOrganization().getOrganizationStatus())&&tempCompany.getCompany().getStatus()==1){
					result = new JSONObject();
					result.put("ORG_NAME", tempCompany.getOrganization().getOrganizationName());
					result.put("COMPANY_BANK_ID", tempCompany.getCompanyBankId());
					result.put("COMPANY_NAME", tempCompany.getCompany().getCompanyName());
					result.put("BANK_NAME", tempCompany.getCompanyBankName());
					result.put("BRANCH" , tempCompany.getCompanyBankBranch());
					result.put("ACCOUNT_NO", tempCompany.getCompanyBankAccountNo());
					result.put("IFSC", tempCompany.getCompanyBankIfscCode());
					companyBankArr.put(result);
				}
			}
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return companyBankArr.toString();
	}
	
	@RequestMapping(value="/searchCompanyBankDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchCompanyBankDetails(@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		JSONObject result = null;
		JSONArray companyBankArr = null;
//		List<CompanyBank>companyBankList = null;
		java.util.Map<Object, Object>map = null;
		CompanyBank tempCompany = null;
		Iterator<CompanyBank> cmpanyBankItr = null;
		try{
			companyBankArr = new JSONArray();
			map = companyService.searchCompanyBankDetails(searchOption,searchText,orgId,from);
			List<CompanyBank>companyBankList = (List<CompanyBank>) map.get("compBank");
			cmpanyBankItr = companyBankList.iterator();
			Integer totalPage = (Integer) map.get("totalPages");
			while(cmpanyBankItr.hasNext()){
				tempCompany = cmpanyBankItr.next();
				//if("1".equals(tempCompany.getOrganization().getOrganizationStatus())&&tempCompany.getCompany().getStatus()==1){
					result = new JSONObject();
					result.put("ORG_NAME", tempCompany.getOrganization().getOrganizationName());
					result.put("COMPANY_BANK_ID", tempCompany.getCompanyBankId());
					result.put("COMPANY_NAME", tempCompany.getCompany().getCompanyName());
					result.put("BANK_NAME", tempCompany.getCompanyBankName());
					result.put("BRANCH" , tempCompany.getCompanyBankBranch());
					result.put("ACCOUNT_NO", tempCompany.getCompanyBankAccountNo());
					result.put("IFSC", tempCompany.getCompanyBankIfscCode());
					companyBankArr.put(result);
				//}
			}
			result=new JSONObject();
			result.put("paginationCount", totalPage);
			companyBankArr.put(result);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return companyBankArr.toString();
	}
	//company bank dropdown list 
	@RequestMapping(value="/companyBankDropdown",method=RequestMethod.POST)
	private  @ResponseBody String companyBankDropdown(@RequestParam(value="companyId",required=false) Integer companyId,
			@RequestParam(value="orgId",required=false) Integer organizationId){
		JSONObject result = null;
		JSONArray companyBankArr = null;
		List<CompanyBank>companyBankList = null;
		CompanyBank tempCompany = null;
		Iterator<CompanyBank> cmpanyBankItr = null;
		try{
			companyBankArr = new JSONArray();
			companyBankList = companyService.companyBankDropdown(companyId,organizationId);
			cmpanyBankItr = companyBankList.iterator();
			while(cmpanyBankItr.hasNext()){
				tempCompany = cmpanyBankItr.next();
				//if("1".equals(tempCompany.getOrganization().getOrganizationStatus())&&tempCompany.getCompany().getStatus()==1){
					result = new JSONObject();
					result.put("COMPANY_BANK_ID", tempCompany.getCompanyBankId());
					result.put("BANK_NAME", tempCompany.getCompanyBankName());
					companyBankArr.put(result);
				//}
			}
			result = new JSONObject();
			result.put("companyBankArray", companyBankArr);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	//add Executives For Company
	@RequestMapping(value="/addExecutivesToCompany",method=RequestMethod.POST)
	private  @ResponseBody String addExecutivesToCompany(@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="executiveName",required=false) String executiveName ,@RequestParam(value="mobile",required=false) Long mobile, 
			@RequestParam(value="designation",required=false) String designation,@RequestParam(value="email",required=false) String email){
		CompanyExecutive companyExecutive =null;
		JSONObject result = new JSONObject();
		try{
			result.put("result",false);
			companyExecutive = new CompanyExecutive();
			companyExecutive.setExecutiveName(executiveName);
			companyExecutive.setMobileNo(mobile);
			companyExecutive.setDesignation(designation);
			companyExecutive.setEmailId(email);
			companyExecutive.setStatus(1);
			//set company  reg date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
		    companyExecutive.setSubmitDate(date);
		    
			if(companyExecutiveService.saveExecutivesToCompany(orgId,companyId,companyExecutive)){
				result.put("result",true);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	//update company executive details
	@RequestMapping(value="/updateCompanyExecutiveDetails",method=RequestMethod.POST)
	private  @ResponseBody String updateCompanyExecutiveDetails(@RequestParam(value="companyExecutiveId",required=false) Integer companyExecutiveId,
			@RequestParam(value="executiveName",required=false) String executiveName ,@RequestParam(value="mobile",required=false) Long mobile, 
			@RequestParam(value="designation",required=false) String designation,@RequestParam(value="email",required=false) String email){
		CompanyExecutive companyExecutive =null;
		JSONObject result = new JSONObject();
		try{
			companyExecutive =	companyExecutiveService.getCompanyExecutiveObjById(companyExecutiveId);
			companyExecutive.setExecutiveName(executiveName);
			companyExecutive.setMobileNo(mobile);
			companyExecutive.setDesignation(designation);
			companyExecutive.setEmailId(email);
			
		    result.put("MSG","Company Executive Details updation failed....!");
			if(companyExecutiveService.updateCompanyExecutivesDetails(companyExecutive)){
				result.put("MSG","Company Executive Details updated successfully....!");
				result.put("RESULT",true);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG","Operation failed....!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/companyExecutivesListing",method=RequestMethod.POST)
	private  @ResponseBody String companyExecutivesListing(){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		JSONObject result = null;
		JSONArray companyExecutiveArr = null;
		List<CompanyExecutive>companyExecutiveList = null;
		CompanyExecutive tempcompanyExecutive = null;
		Iterator<CompanyExecutive> cmpanyExecutiveItr = null;
		try{
			companyExecutiveArr = new JSONArray();
			companyExecutiveList = companyExecutiveService.getCompanyExecutivesListing(orgId);
			cmpanyExecutiveItr = companyExecutiveList.iterator();
			while(cmpanyExecutiveItr.hasNext()){
				tempcompanyExecutive = cmpanyExecutiveItr.next();
				if("1".equals(tempcompanyExecutive.getOrganization().getOrganizationStatus())&&tempcompanyExecutive.getCompany().getStatus()==1){
					result = new JSONObject();
					result.put("ORG_NAME", tempcompanyExecutive.getOrganization().getOrganizationName());
					result.put("EXECUTIVE_ID", tempcompanyExecutive.getCompanyExecutiveId());
					result.put("COMPANY_NAME", tempcompanyExecutive.getCompany().getCompanyName());
					result.put("EXECUTIVE_NAME", tempcompanyExecutive.getExecutiveName());
					result.put("DESIGNATION" , tempcompanyExecutive.getDesignation());
					result.put("MOBILE", tempcompanyExecutive.getMobileNo());
					result.put("EMAIL", tempcompanyExecutive.getEmailId());
					companyExecutiveArr.put(result);
				}
			}
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return companyExecutiveArr.toString();
	}
	//
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/searchCompanyExecutiveDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchCompanyExecutiveDetails(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=true) Integer from){
		
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		JSONObject result = null;
		JSONArray companyExecutiveArr = null;
		List<CompanyExecutive>companyExecutiveList = null;
		CompanyExecutive tempcompanyExecutive = null;
		Iterator<CompanyExecutive> cmpanyExecutiveItr = null;
		
		try{
			companyExecutiveArr = new JSONArray();
			Map<String,Object> map = companyExecutiveService.searchCompanyExecutiveDetails(searchOption,searchText,from,orgId);
			companyExecutiveList =(List<CompanyExecutive>) map.get("companyExecutive");
			Integer totalPages = (Integer)map.get("totalPages");
			
			cmpanyExecutiveItr = companyExecutiveList.iterator();
			while(cmpanyExecutiveItr.hasNext()){
				tempcompanyExecutive = cmpanyExecutiveItr.next();
			//	if("1".equals(tempcompanyExecutive.getOrganization().getOrganizationStatus())&&tempcompanyExecutive.getCompany().getStatus()==1){
					result = new JSONObject();
					result.put("ORG_NAME", tempcompanyExecutive.getOrganization().getOrganizationName());
					result.put("EXECUTIVE_ID", tempcompanyExecutive.getCompanyExecutiveId());
					result.put("COMPANY_NAME", tempcompanyExecutive.getCompany().getCompanyName());
					result.put("EXECUTIVE_NAME", tempcompanyExecutive.getExecutiveName());
					result.put("DESIGNATION" , tempcompanyExecutive.getDesignation());
					result.put("MOBILE", tempcompanyExecutive.getMobileNo());
					result.put("EMAIL", tempcompanyExecutive.getEmailId());
					companyExecutiveArr.put(result);
				}
			result = new JSONObject();
			result.put("paginationCount",totalPages);
			//result.put("companyArray", companyExecutiveArr);
			companyExecutiveArr.put(result);
			
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return companyExecutiveArr.toString();
	}
	//add Depo For Company
	@RequestMapping(value="/addCompanyDepo",method=RequestMethod.POST)
	private  @ResponseBody String addCompanyDepo(@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="company",required=false) Integer companyId,
			@RequestParam(value="depoName",required=false) String depoName ,@RequestParam(value="mobile",required=false) Long mobile, 
			@RequestParam(value="designation",required=false) String designation,@RequestParam(value="city",required=false) Integer cityId,
			@RequestParam(value="telephone",required=false) Long telephone,@RequestParam(value="email",required=false) String email,
			@RequestParam(value="contactPerson",required=false) String contactPerson,@RequestParam(value="district",required=false) Integer districtId,
			@RequestParam(value="address",required=false) String address,@RequestParam(value="website",required=false) String website,
			@RequestParam(value="state",required=false) Integer stateId){
		CompanyDepo companyDepo =null;
		JSONObject result = new JSONObject();
		try{       
			result.put("result",false);
			companyDepo = new CompanyDepo();
			companyDepo.setDepoName(depoName);
			companyDepo.setMobileNo(mobile);
			companyDepo.setDesignation(designation);
			companyDepo.setTelePhoneNo(telephone);
			companyDepo.setEmailId(email);
			companyDepo.setContactPerson(contactPerson);
			companyDepo.setWebSite(website);
			companyDepo.setAddress(address);
			companyDepo.setStatus(1);
			//set company  reg date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
		    companyDepo.setSubmitDate(date);
		    
			if(companyDepoService.saveCompanyDepoDetails(orgId,companyId,stateId,districtId,cityId,companyDepo)){
				result.put("result",true);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/updateCompanyDepoDetails",method=RequestMethod.POST)
	private  @ResponseBody String updateCompanyDepoDetails(@RequestParam(value="companyDepoId",required=false) Integer companyDepoId,
			@RequestParam(value="depoName",required=false) String depoName ,@RequestParam(value="mobile",required=false) Long mobile, 
			@RequestParam(value="designation",required=false) String designation,@RequestParam(value="city",required=false) Integer cityId,
			@RequestParam(value="telephone",required=false) Long telephone,@RequestParam(value="email",required=false) String email,
			@RequestParam(value="contactPerson",required=false) String contactPerson,@RequestParam(value="district",required=false) Integer districtId,
			@RequestParam(value="address",required=false) String address,@RequestParam(value="website",required=false) String website,
			@RequestParam(value="state",required=false) Integer stateId){
		CompanyDepo companyDepo =null;
		JSONObject result = new JSONObject();
		try{
			Boolean insertNewRecordFlag = true;
			companyDepo = companyDepoService.loadCompanyDepoObjectUsignCompanyDepoId(companyDepoId);
			if(stateId==companyDepo.getState().getStateId() && districtId==companyDepo.getDistrict().getDistrictId() && companyDepo.getCity().getCityId()==cityId){
				insertNewRecordFlag = false;
			}
			if(insertNewRecordFlag){
				java.util.Date submitDate = companyDepo.getSubmitDate();
				Organization organization = companyDepo.getOrganization();
				Company company = companyDepo.getCompany();
				//re instantiation/initialization
				companyDepo = new CompanyDepo();
				companyDepo.setOrganization(organization);
				companyDepo.setCompany(company);
				companyDepo.setStatus(1);
				/*//set company  reg date
				java.util.Date today = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			    String IST = df.format(today);
			    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    java.util.Date date = df2.parse(IST);*/
			    companyDepo.setSubmitDate(submitDate);
			}
			companyDepo.setDepoName(depoName);
			companyDepo.setMobileNo(mobile);
			companyDepo.setDesignation(designation);
			companyDepo.setTelePhoneNo(telephone);
			companyDepo.setEmailId(email);
			companyDepo.setContactPerson(contactPerson);
			companyDepo.setWebSite(website);
			companyDepo.setAddress(address);
			
			result.put("MSG","Company Depo Details Updation Failed.....!");
			if(insertNewRecordFlag){
				Integer id = companyDepoService.insertNewCompanyDepoDetailsAndDeleteExistingRecord(stateId, districtId, cityId, companyDepoId, companyDepo);
				if(id!=null&&id>0){
					result.put("MSG","Company Depo Details Updated Successfully.....!");
					result.put("InsertNewRecordFlag",true);
					result.put("DEPO_ID", id);
					result.put("RESULT",true);
				}
			}
			else if(companyDepoService.updateCompanyDepoDetails(companyDepo)){
					result.put("MSG","Company Depo Details Updated Successfully.....!");
					result.put("InsertNewRecordFlag",false);
					result.put("RESULT",true);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG","Operation Failed.....!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/companyDepoListing",method=RequestMethod.POST)
	private  @ResponseBody String companyDepoListing(){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		JSONObject result = null;
		JSONArray companyDepoArr = null;
		List<CompanyDepo>companyDepoList = null;
		CompanyDepo tempcompanyDepo = null;
		Iterator<CompanyDepo> companyDepoItr = null;
		try{
			companyDepoArr = new JSONArray();
			companyDepoList = companyDepoService.getCompanyDepoListing(orgId);
			companyDepoItr = companyDepoList.iterator();
			while(companyDepoItr.hasNext()){
				tempcompanyDepo = companyDepoItr.next();
				if("1".equals(tempcompanyDepo.getOrganization().getOrganizationStatus())&&tempcompanyDepo.getCompany().getStatus()==1){
					result = new JSONObject();
					result.put("ORG_NAME", tempcompanyDepo.getOrganization().getOrganizationName());
					result.put("DEPO_ID", tempcompanyDepo.getCompanyDepoId());
					result.put("COMPANY_NAME", tempcompanyDepo.getCompany().getCompanyName());
					result.put("DEPO_NAME", tempcompanyDepo.getDepoName());
					result.put("CONTACT_PERSON" , tempcompanyDepo.getContactPerson());
					if(tempcompanyDepo.getMobileNo() ==null)
					{
					result.put("MOBILE", "-");
					}else{
						result.put("MOBILE", tempcompanyDepo.getMobileNo());
					}
					result.put("EMAIL", tempcompanyDepo.getEmailId());
					companyDepoArr.put(result);
				}
			}
			result = new JSONObject();
			result.put("companyDepoArr", companyDepoArr);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/searchCompanyDepotDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchCompanyDepotDetails(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=true) Integer from){
		
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		JSONObject result = null;
		JSONArray companyDepoArr = null;
		List<CompanyDepo>companyDepoList = null;
		CompanyDepo tempcompanyDepo = null;
		Iterator<CompanyDepo> companyDepoItr = null;
		try{
			companyDepoArr = new JSONArray();
			Map<String,Object>map = companyDepoService.searchCompanyDepotDetails(searchOption,searchText,from,orgId);
			companyDepoList =(List<CompanyDepo>) map.get("companyDepo");
			Integer totalPages = (Integer)map.get("totalPages");
			
			companyDepoItr = companyDepoList.iterator();
			while(companyDepoItr.hasNext()){
				tempcompanyDepo = companyDepoItr.next();
				//if("1".equals(tempcompanyDepo.getOrganization().getOrganizationStatus())&&tempcompanyDepo.getCompany().getStatus()==1){
					result = new JSONObject();
					result.put("ORG_NAME", tempcompanyDepo.getOrganization().getOrganizationName());
					result.put("DEPO_ID", tempcompanyDepo.getCompanyDepoId());
					result.put("COMPANY_NAME", tempcompanyDepo.getCompany().getCompanyName());
					result.put("DEPO_NAME", tempcompanyDepo.getDepoName());
					result.put("CONTACT_PERSON" , tempcompanyDepo.getContactPerson());
					if(tempcompanyDepo.getMobileNo()!=null)
						result.put("MOBILE", tempcompanyDepo.getMobileNo());
					else
						result.put("MOBILE", "");
					result.put("EMAIL", tempcompanyDepo.getEmailId());
					companyDepoArr.put(result);
				}
			
			 result=new JSONObject();
			 result.put("paginationCount",totalPages);
			 result.put("companyDepoArr", companyDepoArr);
			// companyDepoArr.put(result);
			 
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/companyDepoDropdown",method=RequestMethod.POST)
	private  @ResponseBody String companyDepoDropdown(@RequestParam(value="companyId",required=false) Integer companyId){
		JSONObject result = null;
		JSONArray companyDepoArr = null;
		List<CompanyDepo>companyDepoList = null;
		CompanyDepo tempcompanyDepo = null;
		Iterator<CompanyDepo> companyDepoItr = null;
		try{
			companyDepoArr = new JSONArray();
			companyDepoList = companyDepoService.getCompanyDepoListByCompanyId(companyId);
			companyDepoItr = companyDepoList.iterator();
			while(companyDepoItr.hasNext()){
				tempcompanyDepo = companyDepoItr.next();
					result = new JSONObject();
					result.put("DEPO_ID", tempcompanyDepo.getCompanyDepoId());
					result.put("DEPO_NAME", tempcompanyDepo.getDepoName());
					companyDepoArr.put(result);
			}
			result = new JSONObject();
			result.put("companyDepoArr", companyDepoArr);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}	
//add Product For Company
	@RequestMapping(value="/addCompanyProduct",method=RequestMethod.POST)
	private  @ResponseBody String addCompanyProduct(@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="company",required=false) Integer companyId,
			@RequestParam(value="productName",required=false) String productName ,@RequestParam(value="productCode",required=false) String productCode, 
			@RequestParam(value="productType",required=false) String productType,@RequestParam(value="mrp",required=false) Double mrp,
			@RequestParam(value="ptd",required=false) Double ptd,@RequestParam(value="boxSize",required=false) Integer boxSize,
			@RequestParam(value="unitSize",required=false) Integer unitSize){
		CompanyProduct companyProduct =null;
		JSONObject result = new JSONObject();
		try{       
			result.put("result",false);
			companyProduct = new CompanyProduct();
			companyProduct.setCompanyProductName(productName);
			companyProduct.setCompanyProductCode(productCode);
			companyProduct.setCompanyProductType(productType);
			companyProduct.setCompanyProductMRP(mrp);
			companyProduct.setCompanyProductPTD(ptd);
			companyProduct.setCompanyProductBoxSize(boxSize);
			companyProduct.setCompanyProductUnitSize(unitSize);
			companyProduct.setStatus(1);
			//set company  reg date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
		    companyProduct.setSubmitDate(date);
		    
			if(companyService.saveCompanyProduct(orgId,companyId,companyProduct)){
				result.put("result",true);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	@RequestMapping(value="/updateCompanyProductDetails",method=RequestMethod.POST)
	private  @ResponseBody String updateCompanyProductDetails(@RequestParam(value="companyProductId",required=false) Integer companyProductId,
			@RequestParam(value="productName",required=true) String productName ,@RequestParam(value="productCode",required=true) String productCode, 
			@RequestParam(value="productType",required=true) String productType,@RequestParam(value="mrp",required=true) Double mrp,
			@RequestParam(value="ptd",required=true) Double ptd,@RequestParam(value="boxSize",required=true) Integer boxSize,
			@RequestParam(value="unitSize",required=true) Integer unitSize){
		CompanyProduct companyProduct =null;
		JSONObject result = new JSONObject();
		Boolean insertNewProductFlag = false;
		try{ 
			companyProduct=companyService.loadCompanyProductObjectUsignCompanyProductId(companyProductId);
			if(Double.compare(companyProduct.getCompanyProductMRP(),mrp)!=0){
				insertNewProductFlag = true;
			}
			else if(Double.compare(companyProduct.getCompanyProductPTD(),ptd)!=0){
				insertNewProductFlag = true;
			}
			else if(!companyProduct.getCompanyProductCode().equals(productCode)){
				insertNewProductFlag = true;
			}
			else if(companyProduct.getCompanyProductBoxSize().compareTo(boxSize)!=0){
				insertNewProductFlag = true;
			}
			else if(companyProduct.getCompanyProductUnitSize().compareTo(unitSize)!=0){
				insertNewProductFlag = true;
			}
			else if(!companyProduct.getCompanyProductType().equals(productType)){
				insertNewProductFlag = true;
			}
			if(insertNewProductFlag){
				java.util.Date submitDate = companyProduct.getSubmitDate();
				Organization organization = companyProduct.getOrganization();
				Company company = companyProduct.getCompany();
				companyProduct = new CompanyProduct();
				companyProduct.setOrganization(organization);
				companyProduct.setCompany(company);
				companyProduct.setStatus(1);
				companyProduct.setSubmitDate(submitDate);
			}
			companyProduct.setCompanyProductName(productName);
			companyProduct.setCompanyProductCode(productCode);
			companyProduct.setCompanyProductType(productType);
			companyProduct.setCompanyProductMRP(mrp);
			companyProduct.setCompanyProductPTD(ptd);
			companyProduct.setCompanyProductBoxSize(boxSize);
			companyProduct.setCompanyProductUnitSize(unitSize);
			result.put("MSG","Company Product Details Updation failed.....!");
			if(companyProductService.updateCompanyProductDetails(insertNewProductFlag,companyProductId,companyProduct)){
				result.put("MSG","Company Product Details Updated Successfully.....!");
				result.put("RESULT",true);
				if(insertNewProductFlag){
					result.put("PRODUCT_ID",companyProduct.getCompanyProductId());
					result.put("InsertNewProductFlag",insertNewProductFlag);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG","Operation failed.....!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
		//Company Product Listing
		@RequestMapping(value="/companyProductListing",method=RequestMethod.POST)
		private  @ResponseBody String companyProductListing(){
			Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
			JSONObject result = null;
			JSONArray companyProductArr = null;
			List<CompanyProduct>companyProductList = null;
			CompanyProduct tempCompanyProduct = null;
			Iterator<CompanyProduct> companyProductItr = null;
			try{
				companyProductArr = new JSONArray();
				companyProductList = companyService.getCompanyProductListing(orgId);
				companyProductItr = companyProductList.iterator();
				while(companyProductItr.hasNext()){
					tempCompanyProduct = companyProductItr.next();
					if("1".equals(tempCompanyProduct.getOrganization().getOrganizationStatus())&&tempCompanyProduct.getCompany().getStatus()==1){
					result = new JSONObject();
					result.put("ORG_NAME", tempCompanyProduct.getOrganization().getOrganizationName());
					result.put("PRODUCT_ID", tempCompanyProduct.getCompanyProductId());
					result.put("COMPANY_NAME", tempCompanyProduct.getCompany().getCompanyName());
					result.put("PRODUCT_NAME", tempCompanyProduct.getCompanyProductName());
					result.put("PTD" , tempCompanyProduct.getCompanyProductPTD());
					result.put("MRP", tempCompanyProduct.getCompanyProductMRP());
					if(tempCompanyProduct.getCompanyProductType().equalsIgnoreCase(""))
					{
					result.put("TYPE", "-");
					}else{
						result.put("TYPE", tempCompanyProduct.getCompanyProductType());
					}
					companyProductArr.put(result);
					}
				}
				result = new JSONObject();
				result.put("companyProductArr", companyProductArr);
			}catch(JSONException e){
				log.error(e);e.printStackTrace();
			}
			catch(Exception e){
				log.error(e);e.printStackTrace();
			}
			return result.toString();
		}
		//Company Product Listing
		@RequestMapping(value="/searchCompanyProductDetails",method=RequestMethod.POST)
		private  @ResponseBody String searchCompanyProductDetails(@RequestParam(value="searchOption",required=false) String searchOption,
				@RequestParam(value="searchText",required=false) String searchText,
				@RequestParam(value="from",required=true) Integer from){
			Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
			JSONObject result = null;
			JSONArray companyProductArr = null;
			List<CompanyProduct>companyProductList = null;
			CompanyProduct tempCompanyProduct = null;
			Iterator<CompanyProduct> companyProductItr = null;
			Double fromValue=0.0;
			Double toValue=0.0;
			try{
				companyProductArr = new JSONArray();
				if("PTD".equals(searchOption)||"MRP".equals(searchOption)){
					try{
						String []temp=searchText.split(",");
						fromValue = Double.parseDouble(temp[0]);
						toValue = Double.parseDouble(temp[1]);
					}catch(Exception e){
						e.printStackTrace();
						result = new JSONObject();
						result.put("result", false);
						return result.toString();
					}
				}
				Map<String , Object> map = companyService.searchCompanyProductDetails(searchOption,searchText,fromValue,toValue,from,orgId);
				companyProductList = (List<CompanyProduct>) map.get("companyProductList");
				Integer totalPages = (Integer) map.get("totalPages");
				companyProductItr = companyProductList.iterator();
				while(companyProductItr.hasNext()){
					tempCompanyProduct = companyProductItr.next();
					//if("1".equals(tempCompanyProduct.getOrganization().getOrganizationStatus())&&tempCompanyProduct.getCompany().getStatus()==1){
					result = new JSONObject();
					result.put("ORG_NAME", tempCompanyProduct.getOrganization().getOrganizationName());
					result.put("PRODUCT_ID", tempCompanyProduct.getCompanyProductId());
					result.put("COMPANY_NAME", tempCompanyProduct.getCompany().getCompanyName());
					result.put("PRODUCT_NAME", tempCompanyProduct.getCompanyProductName());
					result.put("PTD" , tempCompanyProduct.getCompanyProductPTD());
					result.put("MRP", tempCompanyProduct.getCompanyProductMRP());
					result.put("TYPE", tempCompanyProduct.getCompanyProductType());
					companyProductArr.put(result);
					//}
				}
				result = new JSONObject();
				result.put("paginationCount", totalPages);
				result.put("companyProductArr", companyProductArr);
			}catch(JSONException e){
				log.error(e);e.printStackTrace();
			}
			catch(Exception e){
				log.error(e);e.printStackTrace();
			}
			return result.toString();
		}
		//Organization bank dropdown value list
		@RequestMapping(value="/organiztionBankDropdown",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody String organiztionBankDropdown(@RequestParam(value="orgId",required=true) Integer organiztionNameId,
				@RequestParam(value="companyId",required=true) Integer companyId)
		{
			List<OrganizationBank> organizationBankList = null;
			JSONArray json_data_array= new JSONArray();
			JSONObject tempJson=null;
			JSONObject result=null;
			try 
			{
				if(organiztionNameId==null)
					organiztionNameId = (Integer) session.getAttribute("ORGANIZATION_ID");
				
				organizationBankList=companyService.loadOrganizationBanksListUsingOrganizationId(organiztionNameId);
				for(OrganizationBank obj : organizationBankList)
				{
					tempJson=new JSONObject();
					tempJson.put("ORG_BANK_ID",	obj.getOrganizationBankId());
					//tempJson.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					tempJson.put("BANK_NAME", obj.getOrganizationBankName()+","+obj.getOrganizationBankBranch());
					json_data_array.put(tempJson);
				}
				result=new JSONObject();
				result.put("orgBankArray", json_data_array);
				
				json_data_array= null;
				json_data_array = companyService.getCompanyOragainzationBankList(companyId);
				result.put("companyOrgBankArray", json_data_array);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return result.toString();
		}
		
		//add Depo For Company
		@RequestMapping(value="/addOrganizationBankToCompany",method=RequestMethod.POST)
		private  @ResponseBody String addOrganizationBankToCompany(
				@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="company",required=false) Integer companyId,
				@RequestParam(value="orgBanks",required=false) Integer[] orgBanks){
			JSONObject result = new JSONObject();
			try{       
				result.put("result",false);
				if(companyService.saveOrganizationBankToCompany(orgId,companyId,orgBanks)){
					result.put("result",true);
				}
			}catch(Exception e){
				e.printStackTrace();
				log.error(e);
			}
			return result.toString();
		}
		
		//
		@RequestMapping(value="/getOrgBankToCompanyList",method=RequestMethod.POST)
		private  @ResponseBody String getOrgBankToCompanyList(){
			Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
			JSONObject result = null;
			JSONArray companyArr = null;
			List<Company>companyList = null;
			Company tempCompany = null;
			Iterator<Company> cmpanyItr = null;
			try{
				companyArr = new JSONArray();
				companyList = companyService.companyDetailsList(orgId);
				cmpanyItr = companyList.iterator();
				while(cmpanyItr.hasNext()){
					tempCompany = cmpanyItr.next();
					if("1".equals(tempCompany.getOrganization().getOrganizationStatus())){
					result = new JSONObject();
					result.put("ORG_NAME", tempCompany.getOrganization().getOrganizationName());
					result.put("COMPANY_ID", tempCompany.getCompanyId());
					result.put("COMPANY_NAME", tempCompany.getCompanyName());
					companyArr.put(result);
					}
				}
				result = new JSONObject();
				result.put("companyArray", companyArr);
			}catch(JSONException e){
				log.error(e);e.printStackTrace();
			}
			catch(Exception e){
				log.error(e);e.printStackTrace();
			}
			return result.toString();
		}
		//
		@SuppressWarnings("unchecked")
		@RequestMapping(value="/searchAssignedBankDetailsOfOrgToCompany",method=RequestMethod.POST)
		private  @ResponseBody String searchAssignedBankDetailsOfOrgToCompany(@RequestParam(value="searchOption",required=false) String searchOption,
				@RequestParam(value="searchText",required=false) String searchText,
				@RequestParam(value="from",required=true) Integer from){
			
			Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
			JSONObject result = null;
			JSONArray companyArr = null;
			List<Company>companyList = null;
			Company tempCompany = null;
			Iterator<Company> cmpanyItr = null;
			try{
				companyArr = new JSONArray();
				Map<String,Object> map = companyService.searchAssignedBankDetailsOfOrgToCompany(searchOption,searchText,from, orgId);
				companyList =(List<Company>) map.get("company");
				Integer totalPages = (Integer)map.get("totalPages");
				
				cmpanyItr = companyList.iterator();
				while(cmpanyItr.hasNext()){
					tempCompany = cmpanyItr.next();
					//if("1".equals(tempCompany.getOrganization().getOrganizationStatus())){
					result = new JSONObject();
					result.put("ORG_NAME", tempCompany.getOrganization().getOrganizationName());
					result.put("COMPANY_ID", tempCompany.getCompanyId());
					result.put("COMPANY_NAME", tempCompany.getCompanyName());
					companyArr.put(result);
					}
				
				result = new JSONObject();
				result.put("paginationCount",totalPages);
				result.put("companyArray", companyArr);
				//companyArr.put(result);
				
			}catch(JSONException e){
				log.error(e);e.printStackTrace();
			}
			catch(Exception e){
				log.error(e);e.printStackTrace();
			}
			return result.toString();
		}
		//add Company Credit And Discount
		@RequestMapping(value="/addCompanyCreditAndDiscount",method=RequestMethod.POST)
		private  @ResponseBody String addCompanyCreditAndDiscount(
				@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="company",required=false) Integer companyId,
				@RequestParam(value="localDays",required=false) Integer localDays,@RequestParam(value="localPercentage",required=false) Float localPercentage,
				@RequestParam(value="outsideDays",required=false) Integer outsideDays,@RequestParam(value="outsidePercentage",required=false) Float outsidePercentage){
			JSONObject result = new JSONObject();
			CompanyCreditAndDiscount companyCreditAndDiscount =null;
			try{   
				companyCreditAndDiscount = new CompanyCreditAndDiscount();
				companyCreditAndDiscount.setLocalDays(localDays);
				companyCreditAndDiscount.setLocalPercentage(localPercentage);
				companyCreditAndDiscount.setOutsideStationDays(outsideDays);
				companyCreditAndDiscount.setOutsideStationPercentage(outsidePercentage);
				
				result.put("result",false);
				//set organization submit date
				java.util.Date today = new java.util.Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			    String IST = df.format(today);
			    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    java.util.Date date = df2.parse(IST);
			    companyCreditAndDiscount.setSubmitDate(date);
			    companyCreditAndDiscount.setStatus(1);
			    Integer id = companyService.saveCompanyCreditAndDiscount(orgId,companyId,companyCreditAndDiscount);
				if(id!=null){
					result.put("result",true);
				}
			}catch(Exception e){
				e.printStackTrace();
				log.error(e);
			}
			return result.toString();
		}
		//
		@RequestMapping(value="/updateCompanyCreditAndDiscountDetails",method=RequestMethod.POST)
		private  @ResponseBody String updateCompanyCreditAndDiscountDetails(@RequestParam(value="creditAndDiscountId",required=true) Integer creditAndDiscountId,
				@RequestParam(value="orgId",required=true) Integer orgId,@RequestParam(value="companyId",required=true) Integer companyId,
				@RequestParam(value="localDays",required=true) Integer localDays,@RequestParam(value="localPercentage",required=true) Float localPercentage,
				@RequestParam(value="outsideDays",required=true) Integer outsideDays,@RequestParam(value="outsidePercentage",required=true) Float outsidePercentage){
			JSONObject result = new JSONObject();
			CompanyCreditAndDiscount companyCreditAndDiscount =null;
			Boolean insertNewRecordFlag = false;
			try{
				companyCreditAndDiscount = companyCreditAndDiscountService.getCompanyCreditAndDiscountDetailsById(creditAndDiscountId);
				if(companyCreditAndDiscount.getLocalDays().compareTo(localDays)!=0 || companyCreditAndDiscount.getLocalPercentage().compareTo(localPercentage)!=0){
					insertNewRecordFlag=true;
				}
				else if(companyCreditAndDiscount.getOutsideStationDays().compareTo(outsideDays)!=0 || companyCreditAndDiscount.getOutsideStationPercentage().compareTo(outsidePercentage)!=0){
					insertNewRecordFlag=true;
				}
				if(insertNewRecordFlag==false){
					result.put("MSG","Company Credit And Discount Details updated successfully.....!");
					result.put("RESULT",true);
				}else if(insertNewRecordFlag){
					java.util.Date submitDate = companyCreditAndDiscount.getSubmitDate();
					companyCreditAndDiscount = new CompanyCreditAndDiscount();
				    companyCreditAndDiscount.setLocalDays(localDays);
					companyCreditAndDiscount.setLocalPercentage(localPercentage);
					companyCreditAndDiscount.setOutsideStationDays(outsideDays);
					companyCreditAndDiscount.setOutsideStationPercentage(outsidePercentage);
					companyCreditAndDiscount.setSubmitDate(submitDate);
				    companyCreditAndDiscount.setStatus(1);
				    result.put("MSG","Company Credit And Discount Details updation failed.....!");
				    Integer id = companyCreditAndDiscountService.updateCompanyCreditAndDiscount(orgId,companyId,creditAndDiscountId,companyCreditAndDiscount);
				    if(id!=null){
				    	result.put("CREDIT_DISCOUNT_ID",id);
				    	result.put("MSG","Company Credit And Discount Details updated successfully.....!");
						result.put("RESULT",true);
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
				log.error(e);
				try {
					result.put("MSG","Operation Failed.....!");
				} catch (JSONException e1) {
					log.error(e1);
					e1.printStackTrace();
				}
			}
			return result.toString();
		}
		//add Depo For Company
		@RequestMapping(value="/uploadCompanyDocs",method=RequestMethod.POST)
		private  @ResponseBody String uploadCompanyDocs(
				@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="company",required=false) Integer companyId,
				@RequestParam(value="documentTypeId",required=false) Integer documentTypeId,@RequestParam(value="documentImage",required=false) MultipartFile documentFile[]){
			JSONObject result = new JSONObject();
			CompanyDocument companyDocument =null;
			List<CompanyDocumentImagePath> documentList = null;
			CompanyDocumentImagePath tempDocumentObj = null;
			
			try{   
				companyDocument = new CompanyDocument();
				result.put("result",false);
				// write image
				documentList = new  ArrayList<CompanyDocumentImagePath>();
				 for(MultipartFile file : documentFile){
						if(file!=null){
							tempDocumentObj = new CompanyDocumentImagePath();
							tempDocumentObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
							documentList.add(tempDocumentObj);
						}
					}
				companyDocument.setCompanyDocumentImagePathList(documentList);
				//set organization submit date
				java.util.Date today = new java.util.Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			    String IST = df.format(today);
			    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    java.util.Date date = df2.parse(IST);
			    companyDocument.setSubmitDate(date);
			    companyDocument.setStatus(1);
				if(companyService.uploadCompanyDocs(orgId,companyId,documentTypeId,companyDocument)){
					result.put("result",true);
				}
			}catch(Exception e){
				e.printStackTrace();
				log.error(e);
			}
			return result.toString();
		}
		//company Docs Listing
		@RequestMapping(value="/companyDocsListing",method=RequestMethod.POST)
		private  @ResponseBody String companyDocsListing(){
			Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
			JSONArray companyDocArr = null;
			try{
				companyDocArr = companyService.getCompanyDocsListingView(orgId);
			}catch(JSONException e){
				log.error(e);e.printStackTrace();
			}
			catch(Exception e){
				log.error(e);e.printStackTrace();
			}
			return companyDocArr.toString();
		}
		//company Docs Listing
		@RequestMapping(value="/searchUploadedCompanyDocsDetails",method=RequestMethod.POST)
		private  @ResponseBody String searchUploadedCompanyDocsDetails(@RequestParam(value="searchOption",required=false) String searchOption,
				@RequestParam(value="searchText",required=false) String searchText,
				@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
			JSONArray companyDocArr = null;
			try{
				Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
				companyDocArr = companyService.searchUploadedCompanyDocsDetails(searchOption,searchText,orgId,from);
				
			}catch(JSONException e){
				log.error(e);e.printStackTrace();
			}
			catch(Exception e){
				log.error(e);e.printStackTrace();
			}
			return companyDocArr.toString();
		}
		//
		@RequestMapping(value="/updateCompanyDocumentInfo",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody String updateCompanyDocumentInfo(@RequestParam(value="companyDocId",required=true) Integer companyDocumentId,
				@RequestParam(value="company",required=true) Integer companyId,
				@RequestParam(value="documentTypeId",required=true) Integer documentTypeId)
		{
			JSONObject result= null;
			try
			{
				result = companyService.updateCompanyDocumentInfo(documentTypeId,companyId,companyDocumentId);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return result.toString();
		}
		//
		@RequestMapping(value="/updateCompanyUploadDocumentImage",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody String updateCompanyUploadDocumentImage(
				@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
				@RequestParam(value="registrationId",required=true) Integer companyDocumentId) throws IOException
		{
			JSONArray json_ImageArray= null;
			JSONObject result =  new JSONObject();
			List<CompanyDocumentImagePath> documentList = null;
			CompanyDocumentImagePath tempDocumentPath = null;
			try 
			{
				result.put("MSG", "File Upload failed...!");
				if(documentImage!=null&&!(documentImage.length>0)){
					result.put("MSG", "Please Select at least one file...!");
					return result.toString();
				}
				// write image
				documentList  = new ArrayList<CompanyDocumentImagePath>();
				for(MultipartFile file : documentImage){
					if(file!=null){
						tempDocumentPath = new CompanyDocumentImagePath();
						tempDocumentPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
						documentList.add(tempDocumentPath);
					}
				}
				json_ImageArray = companyService.updateCompanyUploadDocumentImage(companyDocumentId, documentList);
				result.put("MSG", "Company Document File Upload successful...!");
				result.put("FILE_NAME", json_ImageArray);
				result.put("RESULT", true);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
				try {
					result.put("MSG", "File Upload failed...!");
				} catch (JSONException e1) {
					log.error(e1);
					e1.printStackTrace();
				}
			}
			return result.toString();
		}
		//
		@RequestMapping(value="/deleteCompanyDocumentFileImage",method=RequestMethod.POST)
		private  @ResponseBody String deleteCompanyDocumentFileImage(@RequestParam(value="companyDocumentPathId",required=false) Integer companyDocumentPathId){
			JSONObject result = null;
			try{
				result = new JSONObject();
				if(companyService.deleteCompanyDocumentFileImage(companyDocumentPathId)){
					result.put("RESULT",true);
					result.put("MSG","Company Upload Document Image Deleted Successfully");
				}else{
					result.put("MSG","Company Upload Document Image Deletion Failed");
				}
			}catch(Exception e){
				try{
					e.printStackTrace();log.error(e);
					result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){}
			}
			return result.toString();
		}
		
		//Delete Company Details
		@RequestMapping(value="/deleteCompanyDetails",method=RequestMethod.POST)
		private  @ResponseBody String deleteCompanyDetails(@RequestParam(value="companyId",required=false) Integer companyId){
			JSONObject result = null;
			try{
				result = new JSONObject();
				//result.put("result",false);
				if(companyService.deleteCompanyDetails(companyId)){
					result.put("MSG","Company Details Deleted Successfully");
				}else{
					result.put("MSG","Company Details Deletion Failed");
				}
			}catch(Exception e){
				e.printStackTrace();log.error(e);
				try{
					result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){e1.printStackTrace();log.error(e1);}
			}
			return result.toString();
		}
		
		//Delete Company Bank Details
		@RequestMapping(value="/deleteCompanyBankDetails",method=RequestMethod.POST)
		private  @ResponseBody String deleteCompanyBankDetails(@RequestParam(value="companyBankId",required=false) Integer companyBankId){
			JSONObject result = null;
			try{
				result = new JSONObject();
				//result.put("result",false);
				if(companyService.deleteCompanyBankDetails(companyBankId)){
					result.put("MSG","Company Bank Details Deleted Successfully");
				}else{
					result.put("MSG","Company Bank Details Deletion Failed");
				}
			}catch(Exception e){
				try{
					e.printStackTrace();log.error(e);
					result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){}
			}
			
			return result.toString();
		}
		//Delete Company Executive Details
		@RequestMapping(value="/deleteCompanyExecutive",method=RequestMethod.POST)
		private  @ResponseBody String deleteCompanyExecutive(@RequestParam(value="companyExeId",required=false) Integer companyExeId){
			JSONObject result = null;
			try{
				result = new JSONObject();
				//result.put("result",false);
				if(companyExecutiveService.deleteCompanyExecutive(companyExeId)){
					result.put("MSG","Company Executive Details Deleted Successfully");
				}else{
					result.put("MSG","Company Executive Details Deletion Failed");
				}
			}catch(Exception e){
				try{
					e.printStackTrace();log.error(e);
					result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){}
			}
			
			return result.toString();
		}
		//Delete Company Depo Details
		@RequestMapping(value="/deleteCompanyDepo",method=RequestMethod.POST)
		private  @ResponseBody String deleteCompanyDepo(@RequestParam(value="depoId",required=false) Integer depoId){
			JSONObject result = null;
			try{
				result = new JSONObject();
				//result.put("result",false);
				if(companyDepoService.deleteCompanyDepo(depoId)){
					result.put("MSG","Company Depo Details Deleted Successfully");
				}else{
					result.put("MSG","Company Depo Details Deletion Failed");
				}
			}catch(Exception e){
				try{
					e.printStackTrace();log.error(e);
					result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){}
			}
			
			return result.toString();
		}
		
		//Delete Company Product Details
		@RequestMapping(value="/deleteCompanyProduct",method=RequestMethod.POST)
		private  @ResponseBody String deleteCompanyProduct(@RequestParam(value="productId",required=false) Integer productId){
			JSONObject result = null;
			try{
				result = new JSONObject();
				//result.put("result",false);
				if(companyService.deleteCompanyProduct(productId)){
					result.put("MSG","Company Product Details Deleted Successfully");
				}else{
					result.put("MSG","Company Product Details Deletion Failed");
				}
			}catch(Exception e){
				try{
					e.printStackTrace();log.error(e);
					result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){}
			}
			
			return result.toString();
		}
		//Delete Company Document Details
		@RequestMapping(value="/deleteCompanyDocs",method=RequestMethod.POST)
		private  @ResponseBody String deleteCompanyDocs(@RequestParam(value="companyDocId",required=false) Integer companyDocId){
			JSONObject result = null;
			try{
				result = new JSONObject();
				//result.put("result",false);
				if(companyService.deleteCompanyUploadedDocs(companyDocId)){
					result.put("MSG","Company Document  Deleted Successfully");
				}else{
					result.put("MSG","Company Document Deletion Failed");
				}
			}catch(Exception e){
				try{
					e.printStackTrace();log.error(e);
					result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){}
			}
			
			return result.toString();
		}
		
		//Add by nutan
		@RequestMapping(value="/ViewCompanyDetails",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody String ViewCompanyDetails(@RequestParam(value="companyId",required=true) Integer companyId,
				HttpServletRequest request)
		{
			JSONArray json_data_array= new JSONArray();
			JSONObject json=null;
			try 
			{
							Company companyObj=companyService.loadCompanyObjectUsingCompanyId(companyId);
							json=new JSONObject();
							json.put("ORG_NAME", companyObj.getOrganization().getOrganizationName());
							json.put("COMPANY_NAME", companyObj.getCompanyName());
							json.put("CONTACT_PERSON", companyObj.getCompanyPerson());
							if(companyObj.getCompanyMobileNo() == null)
							{
								json.put("MOBILE", "-");
							}else
							{
							json.put("MOBILE", companyObj.getCompanyMobileNo());
							}
							CommonMethods common = new CommonMethods();
							json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(companyObj.getSubmitDate().toString()));
							String a = companyObj.getCompanyWebSite();
							System.out.println("dsf : "+a);
							if(companyObj.getCompanyWebSite().equalsIgnoreCase(""))
							{
							json.put("WEBSITE", "-");
							}else{
								json.put("WEBSITE", companyObj.getCompanyWebSite());
							}
							if(companyObj.getCompanyFaxNumber().equalsIgnoreCase(""))
							{
							json.put("FAX_NO","-");
							}else{
								json.put("FAX_NO",companyObj.getCompanyFaxNumber());
							}
							if( companyObj.getCompanyServiceTaxNo().equalsIgnoreCase(""))
							{
							json.put("TAX_NO", "-");  
							}else{
								json.put("TAX_NO", companyObj.getCompanyServiceTaxNo()); 
							}
							json.put("ROLE_TYPE", companyObj.getRoleType().getRoleName());
							json.put("ADDRESS", companyObj.getCompanyAddress());
							json.put("TELEPHONE", companyObj.getCompanyTelephone());
							json.put("EMAIL_ID", companyObj.getCompanyEmailId());
							json.put("VAT", companyObj.getCompanyVat());
							if(companyObj.getCompanyCST().equalsIgnoreCase(""))
							{
							json.put("CTS", "-");  
							}else{
								json.put("CTS", companyObj.getCompanyCST());  
							}
							json.put("USER_NAME", companyObj.getUserDetails().getUserName());  
							json.put("PAN_NO", companyObj.getCompanyPanNo());
							if(companyObj.getState()!=null)
								json.put("STATE", companyObj.getState().getStateName());
							else
								json.put("STATE", "");
							if(companyObj.getDistrict()!=null)
								json.put("DISTRICT", companyObj.getDistrict().getDistrictName());
							else
								json.put("DISTRICT", "");
							if(companyObj.getCity()!=null)
								json.put("CITY", companyObj.getCity().getCityName());
							else
								json.put("CITY", "");
							json_data_array.put(json);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return json_data_array.toString();
		}
		
		//Add by nutan
		//ViewCompanyOrganizationBankDetails
		@RequestMapping(value = "/ViewCompanyOrganizationBankDetails",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody String ViewCompanyOrganizationBankDetails(@RequestParam(value="companyId",required=true) Integer companyId,
				HttpServletRequest request)
		{
			JSONArray json_data_array= new JSONArray();
			JSONArray json_bankName_array= new JSONArray();
			JSONObject json=null,jsonBank = null;
			try 
			{
							json=new JSONObject();
							Company companyObj=companyService.loadCompanyObjectUsingCompanyId(companyId);
							json.put("ORG_NAME", companyObj.getOrganization().getOrganizationName());
							json.put("COMPANY_NAME", companyObj.getCompanyName());
							List<Integer> CompanyOrgBankList = companyService.loadOrganizationBankIdListUsingCompanyId(companyId);
							Iterator<Integer> itr = CompanyOrgBankList.iterator();
							while (itr.hasNext()) 
							{
								jsonBank = new JSONObject();
								OrganizationBank organizationBank=companyService.loadOrganizationBankObjectUsingOrganizationBankId(itr.next());	
								jsonBank.put("BANK_NAME", organizationBank.getOrganizationBankName());
								jsonBank.put("BRANCH_NAME", organizationBank.getOrganizationBankBranch());
								jsonBank.put("ACCOUNT_NO", organizationBank.getOrganizationBankAccountNo());
								jsonBank.put("IFSC_CODE", organizationBank.getOrganizationBankIfscCode());
								json_bankName_array.put(jsonBank);
							}
							CommonMethods common = new CommonMethods();
							json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(companyObj.getSubmitDate().toString()));
							json.put("BANK_ARRAY", json_bankName_array);
							json_data_array.put(json);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return json_data_array.toString();
		}
		
		//Add by nutan
		@SuppressWarnings("null")
		@RequestMapping(value = "/getCompanyOrganizationBankListByCompanyId",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody JSONObject getCompanyOrganizationBankListByCompanyId(List<Integer> CompanyOrgBankList)
		{
			JSONObject json=null;
			Integer companyId = 0;
			Iterator<Integer> itr = CompanyOrgBankList.iterator();
			CompanyOrgBankList = companyService.loadOrganizationBankIdListUsingCompanyId(companyId);
			
			try {
			while (itr.hasNext()) {
					json.put("Bank", itr.next());
				    System.out.println(itr.next());
			}
			
			} catch (JSONException e) {
				e.printStackTrace();
				log.error(e);
			}
			return json;
		
		}
		
//		Add by nutan
		@RequestMapping(value="/viewCompanyDepot",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody String viewCompanyDepot(@RequestParam(value="companyDepoId",required=true) Integer companyDepoId,
				@RequestParam(value="editStatus",required=false) Boolean editStatus,
				HttpServletRequest request)
		{
			JSONArray json_data_array= new JSONArray();
			JSONObject json=null;
			try 
			{
							CompanyDepo companyDepoObj=companyDepoService.loadCompanyDepoObjectUsignCompanyDepoId(companyDepoId);
							json=new JSONObject();
							json.put("ORG_NAME", companyDepoObj.getOrganization().getOrganizationName());
							json.put("COMPANY_NAME", companyDepoObj.getCompany().getCompanyName());
							json.put("DEPOT_NAME", companyDepoObj.getDepoName());
							json.put("CONTACT_PERSON", companyDepoObj.getContactPerson());
							if(companyDepoObj.getDesignation().equalsIgnoreCase(""))
							{
							json.put("DESIGNATION", "-");
							}else{
								json.put("DESIGNATION", companyDepoObj.getDesignation());
							}
							//json.put("MOBILE", companyDepoObj.getMobileNo());
							if(companyDepoObj.getMobileNo() == null)
							{
								json.put("MOBILE", "");
							}else
							{
							json.put("MOBILE", companyDepoObj.getMobileNo());
							}
							CommonMethods common = new CommonMethods();
							json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(companyDepoObj.getSubmitDate().toString()));
							json.put("STATE", companyDepoObj.getState().getStateName());
							json.put("CITY",companyDepoObj.getCity().getCityName());
							json.put("DISTRICT", companyDepoObj.getDistrict().getDistrictName());  
							json.put("TELEPHONE", companyDepoObj.getTelePhoneNo());
							json.put("ADDRESS", companyDepoObj.getAddress());
							json.put("EMAIL_ID", companyDepoObj.getEmailId());
							if(companyDepoObj.getWebSite().equalsIgnoreCase(""))
							{
							json.put("WEB_SITE", "-");
							}else{
								json.put("WEB_SITE", companyDepoObj.getWebSite());
							}
							if(editStatus!=null&&editStatus==true){
								json.put("STATE_ID", companyDepoObj.getState().getStateId());
								json.put("CITY_ID",companyDepoObj.getCity().getCityId());
								json.put("DISTRICT_ID", companyDepoObj.getDistrict().getDistrictId());
							}
							json_data_array.put(json);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return json_data_array.toString();
		}
		
		
//		Add by nutan
		@RequestMapping(value="/viewCompanyProduct",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody String viewCompanyProduct(@RequestParam(value="companyProductId",required=true) Integer companyProductId,
				HttpServletRequest request)
		{
			JSONArray json_data_array= new JSONArray();
			JSONObject json=null;
			try 
			{
							CompanyProduct companyProductObj=companyService.loadCompanyProductObjectUsignCompanyProductId(companyProductId);
							json=new JSONObject();
							json.put("ORG_NAME", companyProductObj.getOrganization().getOrganizationName());
							json.put("COMPANY_NAME", companyProductObj.getCompany().getCompanyName());
							json.put("PRODUCT_NAME", companyProductObj.getCompanyProductName());
							if(companyProductObj.getCompanyProductCode().equalsIgnoreCase(""))
							{
							json.put("PRODUCT_CODE","");
							}else{
								json.put("PRODUCT_CODE", companyProductObj.getCompanyProductCode());
							}
							CommonMethods common = new CommonMethods();
							json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(companyProductObj.getSubmitDate().toString()));
							if(companyProductObj.getCompanyProductType().equalsIgnoreCase(""))
							{
							json.put("TYPE", "");
							}else{
								json.put("TYPE", companyProductObj.getCompanyProductType());
							}
							json.put("MRP",companyProductObj.getCompanyProductMRP());
							json.put("PTD", companyProductObj.getCompanyProductPTD());
							if(companyProductObj.getCompanyProductBoxSize() == null)
							{
								json.put("BOX_SIZE", "");
							}else
							{
							json.put("BOX_SIZE", companyProductObj.getCompanyProductBoxSize());
							}
							if(companyProductObj.getCompanyProductUnitSize() == null)
							{
							json.put("UNIT_SIZE", "");
							}else
							{
								json.put("UNIT_SIZE", companyProductObj.getCompanyProductUnitSize());
							}
							json_data_array.put(json);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return json_data_array.toString();
		}
		
		@RequestMapping(value="/companyCreditAndDiscountListing",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody String companyCreditAndDiscountListing(
				HttpServletRequest request)
		{
			JSONArray json_data_array= new JSONArray();
			JSONObject json=null;
			try 
			{
				Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
				List<CompanyCreditAndDiscount> creditAndDiscountList=companyService.getCompanyCreditAndDiscountList(organizationId);
				
					for(CompanyCreditAndDiscount obj : creditAndDiscountList)
					{
						json=new JSONObject();
						json.put("CREDIT_DISCOUNT_ID", obj.getCompanyCreditAndDiscountId());
						json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
						json.put("COMPANY", obj.getCompany().getCompanyName());
						json.put("ORG_ID", obj.getOrganization().getOrganizationId());
						json.put("COMPANY_ID", obj.getCompany().getCompanyId());
						json.put("LOCAL_DAYS",obj.getLocalDays());
						json.put("OUTSTATION_DAYS", obj.getOutsideStationDays());
						json.put("LOCAL_PER", obj.getLocalPercentage());
						json.put("OUTSTATION_PER", obj.getOutsideStationPercentage());
						json_data_array.put(json);
					}
				
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return json_data_array.toString();
		}
		//Delete Company Credit And Discount Details
		@RequestMapping(value="/deleteCompanyCreditAndDiscountDetails",method=RequestMethod.POST)
		private  @ResponseBody String deleteCompanyCreditAndDiscountDetails(@RequestParam(value="ID",required=false) Integer companyCreditAndDiscountId){
			JSONObject result = null;
			try{
				result = new JSONObject();
				//result.put("result",false);
				if(companyService.deleteCompanyCreditAndDiscountDetails(companyCreditAndDiscountId)){
					result.put("MSG","Company Credit And Discount Details Deleted Successfully");
				}else{
					result.put("MSG","Company Credit And Discount Details Deletion Failed");
				}
			}catch(Exception e){
				try{
					e.printStackTrace();log.error(e);
					result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){}
			}
			
			return result.toString();
		}
		
		
		//Add by nutan
			@RequestMapping(value="/EditCompanyDetails",method=RequestMethod.POST,produces="application/json")
			public @ResponseBody String editCompanyDetails(@RequestParam(value="companyId",required=true) Integer companyId,
					HttpServletRequest request)
			{
				JSONArray json_data_array= new JSONArray();
				JSONObject json=null;
				try 
				{
					Company companyObj=companyService.loadCompanyObjectUsingCompanyId(companyId);
					json=new JSONObject();
					json.put("ORG_ID", companyObj.getOrganization().getOrganizationId());
					json.put("USER_DETAILS_ID", companyObj.getUserDetails().getUserDetailsId());
					json.put("ROLE_TYPE_ID", companyObj.getRoleType().getRoleTypeId());
					json.put("ORG_NAME", companyObj.getOrganization().getOrganizationName());
					//json.put("USER_DETAILS_ID", companyObj.getOrganization().getOrganizationId());
					if(companyObj.getRoleType()!=null)
						json.put("ROLE_TYPE_ID", companyObj.getRoleType().getRoleTypeId());
					else
						json.put("ROLE_TYPE_ID", 0);
					json.put("ORG_NAME", companyObj.getOrganization().getOrganizationName());
					json.put("COMPANY_NAME", companyObj.getCompanyName());
					if(companyObj.getState()!=null)
					json.put("STATE_ID", companyObj.getState().getStateId());
					if(companyObj.getDistrict()!=null)
					json.put("DISTRICT_ID",companyObj.getDistrict().getDistrictId());
					if(companyObj.getCity()!=null)
					json.put("CITY_ID", companyObj.getCity().getCityId());
					json.put("CONTACT_PERSON", companyObj.getCompanyPerson());
					if(companyObj.getCompanyMobileNo()==null)
						json.put("MOBILE","");
					else
						json.put("MOBILE", companyObj.getCompanyMobileNo());
					/*CommonMethods common = new CommonMethods();
					json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(companyObj.getSubmitDate().toString()));*/
					json.put("WEBSITE", companyObj.getCompanyWebSite());
					json.put("FAX_NO",companyObj.getCompanyFaxNumber());
					json.put("TAX_NO", companyObj.getCompanyServiceTaxNo());  
					//json.put("ROLE_TYPE", companyObj.getRoleType().getRoleName());
					json.put("ADDRESS", companyObj.getCompanyAddress());
					json.put("TELEPHONE", companyObj.getCompanyTelephone());
					json.put("EMAIL_ID", companyObj.getCompanyEmailId());
					json.put("VAT", companyObj.getCompanyVat());
					json.put("CTS", companyObj.getCompanyCST());  
					json.put("PAN_NO", companyObj.getCompanyPanNo());
					if(companyObj.getUserDetails()!=null){
					json.put("USER_NAME", companyObj.getUserDetails().getUserName());
					json.put("PWD", companyObj.getUserDetails().getPassword());
					}else{
						json.put("USER_NAME","");
						json.put("PWD", "");
					}
					json_data_array.put(json);
	}
				catch (Exception e) 
				{
					e.printStackTrace();
					log.error(e);
				}
				return json_data_array.toString();
			}
			
			//Add by nutan
			@RequestMapping(value="/UpdateCompanyDetails",method=RequestMethod.POST,produces="application/json")
			public @ResponseBody String UpdateCompanyDetails(@RequestParam(value="companyId",required=true) Integer companyId,
					@RequestParam(value="companyName",required=true) String companyName,
					@RequestParam(value="city",required=false) Integer cityId,@RequestParam(value="state",required=false) Integer stateId,
					@RequestParam(value="district",required=false) Integer districtId,
					@RequestParam(value="contactPerson",required=false) String contactPerson,
					@RequestParam(value="mobile",required=false) Long mobile,
					@RequestParam(value="website",required=false) String website,
					@RequestParam(value="fax",required=false) String fax,
					@RequestParam(value="service_tax_no",required=false) String service_tax_no,
					@RequestParam(value="pan_no",required=true) String pan_no,
					@RequestParam(value="email",required=true) String email,
					@RequestParam(value="address",required=true) String address,
					@RequestParam(value="telephone",required=true) Long telephone,
					@RequestParam(value="roleTypeId",required=false) Integer roleTypeId,
					@RequestParam(value="vat",required=true) String vat,
					@RequestParam(value="cst",required=false) String cst,
					@RequestParam(value="uname",required=true) String uname,  //
					@RequestParam(value="password",required=true) String password,
					HttpServletRequest request)
			{
				JSONObject json=null;
				try 
				{
					json=new JSONObject();
					Company companyObj=companyService.loadCompanyObjectUsingCompanyId(companyId);
					
					if(userDetailsService.checkUserNameAlreadyExitOrNot(companyObj.getUserDetails().getUserDetailsId(),uname)){
						companyObj.setCompanyName(companyName);
						companyObj.setCompanyPerson(contactPerson);
						companyObj.setCompanyMobileNo(mobile);
						companyObj.setCompanyWebSite(website);
						//companyObj.setOrganization(organizationservices.loadOrganizationObjectUsingOrganiztionId(organiztionNameId));
						companyObj.setCompanyFaxNumber(fax);
						companyObj.setCompanyServiceTaxNo(service_tax_no);
						companyObj.setCompanyPanNo(pan_no);
						companyObj.setCompanyEmailId(email);
						companyObj.setCompanyAddress(address);
						companyObj.setCompanyTelephone(telephone);
						companyObj.setCompanyVat(vat);
						companyObj.setCompanyCST(cst);
						UserDetails userDetailsObj = companyObj.getUserDetails();
						userDetailsObj.setUserName(uname);
						userDetailsObj.setPassword(password);
						//companyObj.setRoleType(roleTypeService.loadRoleTypeUsingRoleTypeId(roleTypeId));
						json.put("MSG", "Company Details Updation failed.....!");
						if(companyService.updateCompanyObj(roleTypeId,stateId,districtId,cityId,companyObj)){
							json.put("MSG", "Company Details Updated successfully.....!");
							json.put("RESULT", true);
						}
					}
					else{
						json.put("RESULT", false);
						json.put("MSG", "User Name already exists.....!");
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					log.error(e);
				}
				return json.toString();
			}
			
			
			
////////////////Vijay//////
			
			@SuppressWarnings("unchecked")
			@RequestMapping(value="/searchCreditAndDiscount",method=RequestMethod.POST)
			private  @ResponseBody String searchCreditAndDiscount(@RequestParam(value="searchOption",required=false) String searchOption,
					@RequestParam(value="searchText",required=false) String searchText,
				     @RequestParam(value="from",required=true)Integer from,HttpServletRequest request){
				Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
				JSONObject json = null;
				JSONArray companyArr = null;
				List<CompanyCreditAndDiscount> creditAndDiscountList = null;
				CompanyCreditAndDiscount companyCreditAndDiscount = null;
				Iterator<CompanyCreditAndDiscount> cmpanyItr = null;
				
				try{
					companyArr = new JSONArray();
					Map<String, Object> map = companyCreditAndDiscountService.searchCreditAndDiscount(searchOption,searchText,from,organizationId);
					creditAndDiscountList = (List<CompanyCreditAndDiscount>) map.get("companyCreditAndDiscountList");
					Integer totalPages = (Integer)map.get("totalPages");
					cmpanyItr = creditAndDiscountList.iterator();
					
						while (cmpanyItr.hasNext())
						{
							companyCreditAndDiscount = cmpanyItr.next();
							//if("1".equals(companyCreditAndDiscount.getOrganization().getOrganizationStatus())&&companyCreditAndDiscount.getCompany().getStatus()==1){
							json=new JSONObject();
							json.put("CREDIT_DISCOUNT_ID", companyCreditAndDiscount.getCompanyCreditAndDiscountId());
							json.put("ORG_NAME", companyCreditAndDiscount.getOrganization().getOrganizationName());
							json.put("COMPANY", companyCreditAndDiscount.getCompany().getCompanyName());
							json.put("ORG_ID", companyCreditAndDiscount.getOrganization().getOrganizationId());
							json.put("COMPANY_ID", companyCreditAndDiscount.getCompany().getCompanyId());
							json.put("LOCAL_DAYS",companyCreditAndDiscount.getLocalDays());
							json.put("OUTSTATION_DAYS", companyCreditAndDiscount.getOutsideStationDays());
							json.put("LOCAL_PER", companyCreditAndDiscount.getLocalPercentage());
							json.put("OUTSTATION_PER", companyCreditAndDiscount.getOutsideStationPercentage());
							companyArr.put(json);
							//}
						}
						json=new JSONObject();
						json.put("paginationCount",totalPages);
						companyArr.put(json);
						/*json = new JSONObject();
						json.put("companyArr", companyArr);*/
										
				}catch(JSONException e){
					log.error(e);e.printStackTrace();
				}
				catch(Exception e){
					log.error(e);e.printStackTrace();
				}
				return companyArr.toString();
			}
			
}
