package com.protocol.wms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protocol.wms.service.CityService;
/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/CityController")
public class CityController {
	
	@Autowired
	@Qualifier("CityServiceImpl")
	private CityService cityService;
	
	@RequestMapping(value="/getCities",method=RequestMethod.POST)
	private  @ResponseBody String getCityList(@RequestParam(value="distId",required=false) Integer distId){
		return cityService.getCityList(distId).toString();
	}
	@RequestMapping(value="/getCityName",method=RequestMethod.POST)
	private  @ResponseBody String getCityName(@RequestParam(value="cityId",required=false) Integer cityId){
		return cityService.getCityName(cityId).toString();
	}
}
