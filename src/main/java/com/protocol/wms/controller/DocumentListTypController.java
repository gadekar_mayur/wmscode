/**
 * 
 */
package com.protocol.wms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protocol.wms.model.DocumentListType;
import com.protocol.wms.service.DocumentListTypeService;

/**
 * @author Sudhakar
 *
 */
@Controller
@RequestMapping(value="/DocumentListTypController")
public class DocumentListTypController {
	
	private Logger log = Logger.getLogger(DocumentListTypController.class.getClass());
	
	private DocumentListTypeService documentListTypeService;

	@Autowired
	@Qualifier("DocumentListTypeServiceImpl")
	public void setDocumentListTypeService(
			DocumentListTypeService documentListTypeService) {
		this.documentListTypeService = documentListTypeService;
	}
	
	@RequestMapping(value="/documentList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String documentList(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<DocumentListType> documentListTypeList=documentListTypeService.loadDocumentListType();
			if(documentListTypeList.size()>0)
			{
				for(DocumentListType obj :documentListTypeList)
				{
					json=new JSONObject();
					json.put("DOCUMENT_ID", obj.getDocumentListTypeId());
					json.put("DOCUMENT_NAME", obj.getDocumentName());
					json_data_array.put(json);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
		
	}

}
