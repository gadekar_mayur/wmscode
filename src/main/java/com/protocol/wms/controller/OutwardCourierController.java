
/**
 * 
 */
package com.protocol.wms.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.model.OutWardAdvancedChequeInformation;
import com.protocol.wms.model.OutWardChequeNumberInformation;
import com.protocol.wms.model.OutWardCourierRegistration;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.service.OutwardCourierService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/OutwardCourierController")
public class OutwardCourierController {
	
	private Logger log = Logger.getLogger(OutwardCourierController.class.getClass());
	@Autowired
	private HttpSession session;
	@Autowired
	@Qualifier("OutwardCourierServiceImpl")
	private	OutwardCourierService outwardCourierService;
	
	
	//To add Outward Courier Registration
	@RequestMapping(value="/saveOutwardCourierRegistration",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String saveOutwardCourierRegistration(@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="courierRegDate",required=false) String courierRegistrationDate ,@RequestParam(value="depoId",required=false) String depoId,
			//@RequestParam(value="state",required=false) Integer stateId, 
			@RequestParam(value="weight",required=false) String weight,
			//@RequestParam(value="district",required=false) Integer districtId ,@RequestParam(value="city",required=false) Integer cityId,
			@RequestParam(value="courierName",required=false) String courierName ,@RequestParam(value="deliveryPerson",required=false) String deliveryPerson,
			@RequestParam(value="time",required=false) String time ,@RequestParam(value="docketNo",required=false) String docketNo,
			@RequestParam(value="remark",required=false) String remark,@RequestParam(value="particular",required=false) Integer particularId,
			@RequestParam(value="quantity",required=false) Integer quantity, @RequestParam(value="courierPerson",required=false) String courierPerson,
			@RequestParam(value="stockistId",required=false) Integer stockistId,@RequestParam(value="companyBank",required=false) Integer companyBankId,
			@RequestParam(value="receivedDate",required=false) String courierReceivedDate,@RequestParam(value="chequeNo",required=false) String chequeNo[],
			@RequestParam(value="companyStockistOption",required=false) String companyStockistOption){
		JSONObject result = null;
		OutWardCourierRegistration outWardCourierRegistration = null;
		OutWardAdvancedChequeInformation advancedChequeInfo = null;
		List<OutWardChequeNumberInformation> chequeNoInfoList = null;
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		try{
			result = new JSONObject();
			result.put("result", false);
			result.put("MSG","Add Outward Courier Registration Details Failed...!");
			//
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
			java.util.Date d5 = format.parse(time);
			java.sql.Time	ppstime = new java.sql.Time(d5.getTime());
			//
			//set organization submit date
			java.util.Date today = new java.util.Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date submitDate = df2.parse(IST);
		    //
		    
			if(companyBankId!=null){
				advancedChequeInfo = new OutWardAdvancedChequeInformation();
				advancedChequeInfo.setStatus(1);
				advancedChequeInfo.setSubmitDate(submitDate);
				d1 = dtf.parse(courierReceivedDate);
				advancedChequeInfo.setCourierReceivedDate(new java.sql.Date(d1.getTime()));
				
				chequeNoInfoList = new ArrayList<OutWardChequeNumberInformation>();
				for(String temp : chequeNo){
					if(!"".equals(temp)&&!temp.equals(null)){
						OutWardChequeNumberInformation tempObj  = null;
						tempObj = new OutWardChequeNumberInformation();
						tempObj.setChequeNumber(temp);
						tempObj.setStatus(1);
						chequeNoInfoList.add(tempObj);
					}
				}
				advancedChequeInfo.setNoOfCheque(chequeNoInfoList.size());
			}
			
			outWardCourierRegistration = new OutWardCourierRegistration();
			d1 = dtf.parse(courierRegistrationDate);
			outWardCourierRegistration.setCourierRegistrationDate(new java.sql.Date(d1.getTime()));
			outWardCourierRegistration.setWeight(weight);
			outWardCourierRegistration.setCourierName(courierName);
			outWardCourierRegistration.setDeliveryPerson(deliveryPerson);
			outWardCourierRegistration.setTime(ppstime);
			outWardCourierRegistration.setDocketNo(docketNo);
			outWardCourierRegistration.setRemark(remark);
			outWardCourierRegistration.setQuantity(quantity);
			outWardCourierRegistration.setCourierPerson(courierPerson);
			outWardCourierRegistration.setStatus(1);
			Integer depoId1 = null;
			try{depoId1 = Integer.parseInt(depoId);}catch(Exception e){}
		    outWardCourierRegistration.setSubmitDate(submitDate);
		    if(outwardCourierService.saveOutwardCourierRegistration(companyStockistOption,orgId,companyId,depoId1,particularId,stockistId,companyBankId,outWardCourierRegistration,advancedChequeInfo,chequeNoInfoList)){
		    	result.put("result", true);
		    	result.put("MSG","Outward Courier Registration Details Added Successful...!");
		    }
		}catch(Exception e){
			log.error(e);
			try {
				result = new JSONObject();
				result.put("result", false);
				result.put("MSG","Operation Failed...!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}
			
			e.printStackTrace();
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/editOutwardCourierRegistration",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String editOutwardCourierRegistration(@RequestParam(value="courierId",required=false) Integer outwardCourierRegId,
			@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="courierRegDate",required=false) String courierRegistrationDate ,@RequestParam(value="depoId",required=false) String depoId,
			@RequestParam(value="state",required=false) Integer stateId, 
			@RequestParam(value="weight",required=false) String weight,
			@RequestParam(value="district",required=false) Integer districtId ,@RequestParam(value="city",required=false) Integer cityId,
			@RequestParam(value="courierName",required=false) String courierName ,@RequestParam(value="deliveryPerson",required=false) String deliveryPerson,
			@RequestParam(value="time",required=false) String time ,@RequestParam(value="docketNo",required=false) String docketNo,
			@RequestParam(value="remark",required=false) String remark,@RequestParam(value="particular",required=false) Integer particularId,
			@RequestParam(value="quantity",required=false) Integer quantity, @RequestParam(value="courierPerson",required=false) String courierPerson,
			@RequestParam(value="stockistId",required=false) Integer stockistId,@RequestParam(value="companyBank",required=false) Integer companyBankId,
			@RequestParam(value="receivedDate",required=false) String courierReceivedDate,@RequestParam(value="chequeNo",required=false) String chequeNo[]){
		JSONObject result = null;
		OutWardCourierRegistration outWardCourierRegistration = null;
		OutWardAdvancedChequeInformation advancedChequeInfo = null;
		List<OutWardChequeNumberInformation> chequeNoInfoList = null;
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		try{
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
			java.util.Date d5 = format.parse(time);
			java.sql.Time	ppstime = new java.sql.Time(d5.getTime());
			//
			//set organization submit date
			java.util.Date today = new java.util.Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date submitDate = df2.parse(IST);
		    //
		    
			if(companyBankId!=null){
				advancedChequeInfo = new OutWardAdvancedChequeInformation();
				advancedChequeInfo.setStatus(1);
				advancedChequeInfo.setSubmitDate(submitDate);
				d1 = dtf.parse(courierReceivedDate);
				advancedChequeInfo.setCourierReceivedDate(new java.sql.Date(d1.getTime()));
				
				chequeNoInfoList = new ArrayList<OutWardChequeNumberInformation>();
				for(String temp : chequeNo){
					if(!"".equals(temp)&&!temp.equals(null)){
						OutWardChequeNumberInformation tempObj  = null;
						tempObj = new OutWardChequeNumberInformation();
						tempObj.setChequeNumber(temp);
						tempObj.setStatus(1);
						chequeNoInfoList.add(tempObj);
					}
				}
				advancedChequeInfo.setNoOfCheque(chequeNoInfoList.size());
			}
			
			outWardCourierRegistration = outwardCourierService.loadOutwardCourierRegistrationObjectUsingoutwardCourierRegId(outwardCourierRegId);
			d1 = dtf.parse(courierRegistrationDate);
			outWardCourierRegistration.setCourierRegistrationDate(new java.sql.Date(d1.getTime()));
			outWardCourierRegistration.setWeight(weight);
			outWardCourierRegistration.setCourierName(courierName);
			outWardCourierRegistration.setDeliveryPerson(deliveryPerson);
			outWardCourierRegistration.setTime(ppstime);
			outWardCourierRegistration.setDocketNo(docketNo);
			outWardCourierRegistration.setRemark(remark);
			outWardCourierRegistration.setQuantity(quantity);
			outWardCourierRegistration.setCourierPerson(courierPerson);
			outWardCourierRegistration.setStatus(1);
			Integer depoId1 = null;
			try{depoId1 = Integer.parseInt(depoId);}catch(Exception e){}
		    result = outwardCourierService.editOutwardCourierRegistration(orgId,companyId,depoId1,stateId,districtId,cityId,particularId,stockistId,companyBankId,outWardCourierRegistration,advancedChequeInfo,chequeNoInfoList);
		}catch(Exception e){
			log.error(e);
			try {
				result = new JSONObject();
				result.put("result", false);
				result.put("MSG","Operation Failed...!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}
			
			e.printStackTrace();
		}
		return result.toString();
	}
	
	@RequestMapping(value="/outwardCourierRegistrationListing",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String outwardCourierRegistrationListing(){
		JSONObject result = null;
		JSONArray outwardCourierRegArr = null;
		List<OutWardCourierRegistration> outWardCourierRegistration = null;
		try{
			outwardCourierRegArr = new JSONArray();
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			
			outWardCourierRegistration = outwardCourierService.outwardCourierRegistrationListing(organizationId);
			for(OutWardCourierRegistration tempCourierReg :outWardCourierRegistration){
				Boolean stat = true;
				Stockist stockist = tempCourierReg.getStockist();
				if(stockist!=null){
					if(!(stockist.getStatus()==1))
						stat=false;
				}
				if(stat){
					result = new JSONObject();
					result.put("COURIER_ID",tempCourierReg.getOutWardCourierRegistrationId());
					result.put("COURIER_NAME",tempCourierReg.getCourierName());
					result.put("REG_DATE",tempCourierReg.getCourierRegistrationDate());
					result.put("COMPANY",tempCourierReg.getCompany().getCompanyName());
					String stocistName="-";
					if(stockist!=null)
						stocistName=stockist.getStockistName();
					result.put("STOCKIST_NAME",stocistName);
					result.put("CITY",tempCourierReg.getCity().getCityName());
					result.put("DOCKET_NO",tempCourierReg.getDocketNo());
					outwardCourierRegArr.put(result);
				}
			}
		}catch(Exception e){
			log.error(e);
		}
		return outwardCourierRegArr.toString();
	}
	@RequestMapping(value="/deleteOutwardCourierRegistration",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String deleteOutwardCourierRegistration(@RequestParam(value="outwardCourierRegId",required=true) Integer outwardCourierRegId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(outwardCourierService.deleteOutwardCourierRegistration(outwardCourierRegId)){
				result.put("MSG","Outward Courier Registration Details Deleted Successfully");
			}else{
				result.put("MSG","Outward Courier Registration Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){}
			}
			
			return result.toString();
	}
	
	@RequestMapping(value="/ViewAllDetailsOfOutwardCorierRegistration",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String ViewAllDetailsOfOutwardCorierRegistration(@RequestParam(value="outwardCourierRegId",required=true) Integer outwardCourierRegId,
			@RequestParam(value="editStatus",required=false) Boolean editStatus)
	{
		JSONArray json_data_array= new JSONArray();
		//JSONArray json_cheque_array= new JSONArray();
		JSONObject json=null;
		//JSONObject jsonCheque = null;
		try 
		{
			CommonMethods common = new CommonMethods();
			OutWardCourierRegistration outwardCourierObj=outwardCourierService.loadOutwardCourierRegistrationObjectUsingoutwardCourierRegId(outwardCourierRegId);
			if(outwardCourierObj!=null)
			{
				json=new JSONObject();
				json.put("ORG_NAME", outwardCourierObj.getOrganization().getOrganizationName());
				json.put("COMP_NAME", outwardCourierObj.getCompany().getCompanyName());
				json.put("COURIER_NAME", outwardCourierObj.getCourierName());
				String [] reg_date_arr = outwardCourierObj.getCourierRegistrationDate().toString().split("-");
                json.put("DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
				//json.put("DATE", outwardCourierObj.getCourierRegistrationDate());
				json.put("DOCKET_NUM", outwardCourierObj.getDocketNo());
				json.put("WEIGHT", outwardCourierObj.getWeight());
				json.put("QUANTITY",outwardCourierObj.getQuantity());
				if(outwardCourierObj.getStockist()!= null)
				{
				json.put("STOCKIST", outwardCourierObj.getStockist().getStockistName());
				}
				json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(outwardCourierObj.getSubmitDate().toString()));
				json.put("DELIVERY_PERSON", outwardCourierObj.getDeliveryPerson());
				if(outwardCourierObj.getCompanyDepo()!=null)
					json.put("DEPOT", outwardCourierObj.getCompanyDepo().getDepoName());
				else
					json.put("DEPOT", "");
				json.put("CORIER_PERSON",outwardCourierObj.getCourierPerson());
				if(outwardCourierObj.getState()!=null)
					json.put("STATE", outwardCourierObj.getState().getStateName());
				else
					json.put("STATE", "");
				if(outwardCourierObj.getDistrict()!=null)
					json.put("DISTRICT", outwardCourierObj.getDistrict().getDistrictName());
				else
					json.put("DISTRICT", "");
				json.put("TIME", CommonMethods.getTimeIn12HrFormat(outwardCourierObj.getTime().toString()));
				if(outwardCourierObj.getCity()!=null)
					json.put("CITY", outwardCourierObj.getCity().getCityName());
				else
					json.put("CITY", "");
				json.put("REMARK", outwardCourierObj.getRemark());
				
				json.put("PRTICULARS", outwardCourierObj.getParticulars().getParticularsName());
				/*if(editStatus==null&&outwardCourierObj.getParticulars().getParticularsId()==3)//PARTICULARS ID IS CONSTANT FOR CHEQUE==3
				{
					List<OutWardChequeNumberInformation> ChequeNumberInfoList = outwardCourierService.loadChequeInformationUsigngetOutWardAdvancedChequeInformationId(outwardCourierObj.getOutWardAdvancedChequeInformation().getOutWardAdvancedChequeInformationId());
					if(ChequeNumberInfoList.size()>0)
					{
						for(OutWardChequeNumberInformation obj : ChequeNumberInfoList)
						{
							jsonCheque = new JSONObject();
							jsonCheque.put("BANK_NAME", obj.getOutWardAdvancedChequeInformation().getOrganizationBank().getOrganizationBankName());
							jsonCheque.put("CHEQUE_NO", obj.getChequeNumber());
							json_cheque_array.put(jsonCheque);
						}
					}
				}
				json.put("CHEQUE_ARRAY", json_cheque_array);*/
				
				//For Edit Form View Details(This details is used to show edit form)
				if(editStatus!=null&&editStatus==true){
					json.put("ORG_ID", outwardCourierObj.getOrganization().getOrganizationId());
					json.put("COMPANY_ID", outwardCourierObj.getCompany().getCompanyId());
					if(outwardCourierObj.getStockist()!= null)
					{
						json.put("STOCKIST_ID", outwardCourierObj.getStockist().getStockistId());
						/*if(outwardCourierObj.getParticulars().getParticularsId()==3)
							json.put("OPTION","SockistHavingCheque");
						else
							json.put("OPTION","NoCheque");*/
					}
					else{
						/*if(outwardCourierObj.getParticulars().getParticularsId()==3)
							json.put("OPTION","CompanyHavingCheque");
						else
							json.put("OPTION","NoCheque");*/
					}
					if(outwardCourierObj.getCompanyDepo()!=null)
						json.put("DEPOT_ID", outwardCourierObj.getCompanyDepo().getCompanyDepoId());
					else
						json.put("DEPOT_ID", "");
					json.put("STATE_ID", outwardCourierObj.getState().getStateId());
					json.put("DISTRICT_ID", outwardCourierObj.getDistrict().getDistrictId());
					json.put("CITY_ID", outwardCourierObj.getCity().getCityId());
					json.put("PRTICULARS_ID", outwardCourierObj.getParticulars().getParticularsId());
				}
				json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/searchoutwardCourierRegistration",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String searchoutwardCourierRegistration(@RequestParam(value="searchOption",required=true) String selectedValue ,
			@RequestParam(value="searchText",required=true) String searchText ,
			@RequestParam(value="fromSpecialData",required=true) String fromSpecialData ,
			@RequestParam(value="toSpecialData",required=true) String toSpecialData,	
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		JSONObject result = null;
		JSONArray outwardCourierRegArr = null;
		java.util.Map<Object, Object>map = null;
//		List<OutWardCourierRegistration> outWardCourierRegistration = null;
		try{
			outwardCourierRegArr = new JSONArray();
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			
			map = outwardCourierService.searchoutwardCourierRegistration(selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
			List<OutWardCourierRegistration> outWardCourierRegistration = (List<OutWardCourierRegistration>) map.get("outwardCourier");
			Integer totalPage = (Integer) map.get("totalPages");
			for(OutWardCourierRegistration tempCourierReg :outWardCourierRegistration){
				Boolean stat = true;
				Stockist stockist = tempCourierReg.getStockist();
				/*if(stockist!=null){
					if(!(stockist.getStatus()==1))
						stat=false;
				}
				if(stat){*/
					result = new JSONObject();
					result.put("ORG",tempCourierReg.getOrganization().getOrganizationName());
					result.put("COURIER_ID",tempCourierReg.getOutWardCourierRegistrationId());
					result.put("COURIER_NAME",tempCourierReg.getCourierName());
					result.put("REG_DATE",tempCourierReg.getCourierRegistrationDate());
					result.put("COMPANY",tempCourierReg.getCompany().getCompanyName());
					String stocistName="-";
					if(stockist!=null)
						stocistName=stockist.getStockistName();
					result.put("STOCKIST_NAME",stocistName);
					result.put("CITY",tempCourierReg.getCity().getCityName());
					result.put("DOCKET_NO",tempCourierReg.getDocketNo());
					outwardCourierRegArr.put(result);
				//}
			}
			result=new JSONObject();
			result.put("paginationCount", totalPage);
			outwardCourierRegArr.put(result);
		}catch(Exception e){
			//log.error(e);
		e.printStackTrace();
		}
		return outwardCourierRegArr.toString();
	}
	
}
