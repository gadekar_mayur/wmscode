/**
 * 
 */
package com.protocol.wms.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.protocol.wms.model.UserDetails;
import com.protocol.wms.service.UserDetailsService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/LoginController")
public class LoginController {
	
	private UserDetailsService userDetailsService;
	
	
	/**
	 * @param userDetailsService the userDetailsService to set
	 */
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}


	@RequestMapping(value="/login",method=RequestMethod.POST)
	private  String login(@RequestParam(value="uname",required=true) String uname,
			@RequestParam(value="password",required=true) String pwd,
			HttpServletRequest request)
			{
				UserDetails userDetails = userDetailsService.loginValidate(uname, pwd);
				if(userDetails!=null)
				{
					request.getSession().setAttribute("USER_TYPE", userDetails.getUserType().getUserTypeId());
					request.getSession().setAttribute("USER_DETAILS_ID", userDetails.getUserDetailsId());
					return "dashboard";	
				}
				else
				{
					return "home";
				}
			}


}
