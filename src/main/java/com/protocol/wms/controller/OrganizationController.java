/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationBank;
import com.protocol.wms.model.OrganizationDocument;
import com.protocol.wms.model.OrganizationDocumentImagePath;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.model.UserType;
import com.protocol.wms.service.DocumentListTypeService;
import com.protocol.wms.service.OrganizationBankService;
import com.protocol.wms.service.OrganizationDocumentService;
import com.protocol.wms.service.OrganizationService;
import com.protocol.wms.service.UserDetailsService;
import com.protocol.wms.service.UserTypeService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/OrganizationController")
public class OrganizationController {
	
	private Logger log = Logger.getLogger(OrganizationController.class.getClass());
	
	private OrganizationService organizationservices;
	
	private UserDetailsService userDetailsService;
	
	private UserTypeService userTypeService;
	
	private OrganizationBankService organizationBankService;
	
	private DocumentListTypeService documentListTypeService;
	
	private OrganizationDocumentService organizationDocumentService;
	

	@Autowired
	@Qualifier("OrganizationServiceImpl")
	public void setOrganizationservices(OrganizationService organizationservices) {
		this.organizationservices = organizationservices;
	}
	
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@Autowired
	@Qualifier("UserTypeServiceImpl")
	public void setUserTypeService(UserTypeService userTypeService) {
		this.userTypeService = userTypeService;
	}
	
	@Autowired
	@Qualifier("OrganizationBankServiceImpl")
	public void setOrganizationBankService(
			OrganizationBankService organizationBankService) {
		this.organizationBankService = organizationBankService;
	}

	@Autowired
	@Qualifier("DocumentListTypeServiceImpl")
	public void setDocumentListTypeService(
			DocumentListTypeService documentListTypeService) {
		this.documentListTypeService = documentListTypeService;
	}
	
	
	@Autowired
	@Qualifier("OrganizationDocumentServiceImpl")
	public void setOrganizationDocumentService(
			OrganizationDocumentService organizationDocumentService) {
		this.organizationDocumentService = organizationDocumentService;
	}

	@RequestMapping(value="/saveOrganization",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveOrganization(@RequestParam(value="organizationName",required=true) String orgName,
			@RequestParam(value="location",required=true) String location,
			@RequestParam(value="address",required=true) String address,    
			@RequestParam(value="emailId",required=true) String emailId,  
			@RequestParam(value="telephone",required=true) Long Telephone,       
			@RequestParam(value="userName",required=true) String userName,  
			@RequestParam(value="contactPerson",required=true) String contactPerson,   
			@RequestParam(value="mobileNo",required=true) Long mobileNo, 
			@RequestParam(value="website",required=true) String website,   
			@RequestParam(value="password",required=true) String password,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			// check user name already register
			UserDetails userDetailsObj=userDetailsService.checkUserNameAlreadyExitOrNot(userName);
			if(userDetailsObj==null)
			{
				Organization organizationObj=new Organization();
				organizationObj.setOrganizationName(orgName);
				organizationObj.setOrganizationLocation(location);
				organizationObj.setOrganizationAddress(address);
				organizationObj.setOrganizationContactPerson(contactPerson);
				organizationObj.setOrganizationTelePhone(Telephone);
				organizationObj.setOrganizationMobile(mobileNo);
				organizationObj.setOrganizationEmailId(emailId);
				organizationObj.setOrganizationWebSite(website);
				//set organization submit date
				Date today = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			    String IST = df.format(today);
			    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    Date date = df2.parse(IST);
				organizationObj.setSubmitDate(date);
				
				organizationObj.setOrganizationStatus("1");
				Integer organizationId=organizationservices.saveOrganization(organizationObj);
				if(organizationId!=null)
				{
					// Save organization user details
					UserDetails userDetailsNewObj=new UserDetails();
					userDetailsNewObj.setUserName(userName);
					userDetailsNewObj.setPassword(password);
					//load login user type object
					userDetailsNewObj.setUserType(userTypeService.loadUserTypeUsingUserTypeId(Constant.ADMIN_OR_ORGANIZATION));

					//save user details object
					Integer userDetailsId=userDetailsService.saveUserDetails(userDetailsNewObj);
					
					
					if(userDetailsId!=null)
					{
						//load organization object
						Organization oldOrganizationObj=organizationservices.loadOrganizationObjectUsingOrganiztionId(organizationId);
						UserDetails obj= userDetailsService.loadUserDetailsUsingUserDetailsId(userDetailsId);
						oldOrganizationObj.setUserDetails(obj);
						// update organization object
						organizationservices.updateOrganization(oldOrganizationObj);
					}
					
					
					json=new JSONObject();
					json.put("MSG","Organization added successfully");
					json_data_array.put(json);
				}
			}
			else
			{
				json=new JSONObject();
				json.put("MSG","User Name already exists");
				json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/editOrganization",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editOrganization(
			@RequestParam(value="organizationId",required=true) Integer organizationId,
			@RequestParam(value="organizationName",required=true) String orgName,
			@RequestParam(value="location",required=true) String location,
			@RequestParam(value="address",required=true) String address,    
			@RequestParam(value="emailId",required=true) String emailId,  
			@RequestParam(value="telephone",required=true) Long Telephone,       
			@RequestParam(value="userName",required=true) String userName,  
			@RequestParam(value="contactPerson",required=true) String contactPerson,   
			@RequestParam(value="mobileNo",required=true) Long mobileNo, 
			@RequestParam(value="website",required=true) String website,   
			@RequestParam(value="password",required=true) String password,
			HttpServletRequest request)
	{
		JSONObject result=null;
		try 
		{
			result=new JSONObject();
			// check user name already register
			Organization organizationObj = organizationservices.loadOrganizationObjectUsingOrganiztionId(organizationId);
			if(userDetailsService.checkUserNameAlreadyExitOrNot(organizationObj.getUserDetails().getUserDetailsId(),userName))
			{
				organizationObj.setOrganizationName(orgName);
				organizationObj.setOrganizationLocation(location);
				organizationObj.setOrganizationAddress(address);
				organizationObj.setOrganizationContactPerson(contactPerson);
				organizationObj.setOrganizationTelePhone(Telephone);
				organizationObj.setOrganizationMobile(mobileNo);
				organizationObj.setOrganizationEmailId(emailId);
				organizationObj.setOrganizationWebSite(website);
				
				organizationObj.getUserDetails().setUserName(userName);
				organizationObj.getUserDetails().setPassword(password);
				/*userDetailsNewObj.setUserName(userName);
				userDetailsNewObj.setPassword(password);
				organizationObj.setUserDetails(userDetailsNewObj);*/
				result.put("MSG","Organization Details Upadation Failed.....!");
				if(organizationservices.editOrganizationDetails(organizationObj))
				{
					result.put("ORG_NAME", organizationObj.getOrganizationName());
					result.put("CONTACT_PERSON",organizationObj.getOrganizationContactPerson());
					result.put("MOBILE_NO", organizationObj.getOrganizationMobile());
					result.put("EMAIL_ID", organizationObj.getOrganizationEmailId());
					result.put("RESULT",true);
					result.put("MSG","Organization Details Upadation Successfully.....!");
				}
			}
			else
			{
				result.put("RESULT",false);
				result.put("MSG","User Name already exists....!");
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			result=new JSONObject();
			try {
				result.put("MSG","Operation Failed.....!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}
		}
		return result.toString();
	}
	@RequestMapping(value="/listOrganization",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listOrganization(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<Organization> organizationList=organizationservices.loadListOfOrganization();
			if(organizationList.size()>0)
			{
				for(Organization obj : organizationList)
				{
					json=new JSONObject();
					json.put("ORG_ID", obj.getOrganizationId());
					json.put("ORG_NAME", obj.getOrganizationName());
					json.put("CONTACT_PERSON",obj.getOrganizationContactPerson());
					json.put("MOBILE_NO", obj.getOrganizationMobile());
					json.put("EMAIL_ID", obj.getOrganizationEmailId());
					json_data_array.put(json);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	//Add by nutan
	@RequestMapping(value="/ViewAllDetailsOfOrganization",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String ViewAllDetailsOfOrganization(@RequestParam(value="organizationId",required=true) Integer organizationId,
			@RequestParam(value="editStatus",required=false) Boolean editStatus,HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			Organization orgObj= organizationservices.loadOrganizationObjectUsingOrganiztionId(organizationId);
					json=new JSONObject();
					json.put("ORG_ID", orgObj.getOrganizationId());
					json.put("ORG_NAME", orgObj.getOrganizationName());
					json.put("LOCATION_NAME", orgObj.getOrganizationLocation());
					json.put("TELEPHONE", orgObj.getOrganizationTelePhone());
					json.put("CONTACT_PERSON",orgObj.getOrganizationContactPerson());
					json.put("MOBILE_NO", orgObj.getOrganizationMobile());
					CommonMethods common = new CommonMethods();
					json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(orgObj.getSubmitDate().toString()));
					json.put("EMAIL_ID", orgObj.getOrganizationEmailId());
					json.put("ADDRESS", orgObj.getOrganizationAddress());
					json.put("WEB_SITE", orgObj.getOrganizationWebSite());
					json.put("USER_NAME", orgObj.getUserDetails().getUserName());
					if(editStatus!=null&&editStatus==true)
						json.put("PWD", orgObj.getUserDetails().getPassword());
					json_data_array.put(json);
				
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	
	@RequestMapping(value="/organizationDropDownList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String organizationDropDownList(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			// check current login user type
			UserType userTypeObj=userTypeService.loadUserTypeUsingUserTypeId((Integer) request.getSession().getAttribute("USER_TYPE"));
			if(userTypeObj.getUserTypeId().equals(1))
			{
				// load all the organization
				List<Organization> organizationList=organizationservices.loadListOfOrganization();
				if(organizationList.size()>0)
				{
					for(Organization obj :organizationList)
					{
						json=new JSONObject();
						json.put("USER_TYPE", "SuperAdmin");
						json.put("ORG_NAME", obj.getOrganizationName());
						json.put("ORG_ID",obj.getOrganizationId());
						json_data_array.put(json);
					}
				}
				
			}
			else
			{
				// load current login user organization
				//load organization object using user details id
				Organization organizationobj=organizationservices.loadOrganizationObjectUsingOrganiztionId((Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
				if(organizationobj!=null)
				{
					json=new JSONObject();
					json.put("USER_TYPE", "other");
					json.put("ORG_NAME", organizationobj.getOrganizationName());
					json.put("ORG_ID", organizationobj.getOrganizationId());
					json_data_array.put(json);
					
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/saveOrganizationBank",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveOrganizationBank(@RequestParam(value="organiztionNameId",required=true) Integer organiztionNameId,
			@RequestParam(value="branchName",required=true) String branchName,
			@RequestParam(value="ifscCode",required=true) String ifscCode,
			@RequestParam(value="bankName",required=true) String bankName,
			@RequestParam(value="accountNo",required=true) String accountNo,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			OrganizationBank organizationBankObj=new OrganizationBank();
			organizationBankObj.setOrganizationBankName(bankName);
			organizationBankObj.setOrganizationBankBranch(branchName);
			organizationBankObj.setOrganizationBankAccountNo(accountNo);
			organizationBankObj.setOrganizationBankIfscCode(ifscCode);
			organizationBankObj.setOrganization(organizationservices.loadOrganizationObjectUsingOrganiztionId(organiztionNameId));
			organizationBankObj.setStatus(1);
			//set organization submit date
			Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    Date date = df2.parse(IST);
			organizationBankObj.setSubmitDate(date);
			// save organization bank
			Integer organizationBankId=organizationBankService.saveOrganizationBank(organizationBankObj);
			if(organizationBankId!=null)
			{
				json=new JSONObject();
				json.put("MSG", "Organization bank added successfully");
				json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/saveOrganizationDocument",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveOrganizationDocument(@RequestParam(value="organiztionNameId",required=true) Integer organiztionNameId,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId,
			@RequestParam(value="documentImage",required=true) MultipartFile orgDocFile[])
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		List<OrganizationDocumentImagePath> orgDocPathList = null;
		OrganizationDocumentImagePath tempOrgDocPath = null;
		try
		{
			OrganizationDocument organizationDocumentObj=new OrganizationDocument();
			organizationDocumentObj.setOrganization(organizationservices.loadOrganizationObjectUsingOrganiztionId(organiztionNameId));
			organizationDocumentObj.setDocumentListType(documentListTypeService.loadDocumentListTypeUsingDocumentListTypeId(documentTypeId));
			//set organization submit date
			Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    Date date = df2.parse(IST);
		    organizationDocumentObj.setSubmitDate(date);
		    organizationDocumentObj.setStatus(1);
		    // write image
		    orgDocPathList = new ArrayList<OrganizationDocumentImagePath>();
		    for(MultipartFile file : orgDocFile){
				if(file!=null){
					tempOrgDocPath = new OrganizationDocumentImagePath();
					tempOrgDocPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					orgDocPathList.add(tempOrgDocPath);
				}
			}
		    organizationDocumentObj.setOrganizationDocumentImagePathList(orgDocPathList);
			Integer organizationDocumentId=organizationDocumentService.saveOrganizationDocument(organizationDocumentObj);
			if(organizationDocumentId!=null)
			{
				json=new JSONObject();
				json.put("MSG", "Organization document added successfully");
				json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//
	@RequestMapping(value="/updateOrganizationDocumentInfo",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateOrganizationDocumentInfo(@RequestParam(value="organiztionDocId",required=true) Integer organiztionDocumentId,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId)
	{
		JSONObject result= null;
		try
		{
			result = organizationDocumentService.updateOrganizationDocumentObj(documentTypeId,organiztionDocumentId);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	
	/*@RequestMapping(value="/organizationDocumentList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String organizationDocumentList(
			@RequestParam(value="from",required=true) Integer from,
			HttpServletRequest request)
	{
		JSONArray json_data_array= null;
		try 
		{
			json_data_array = organizationDocumentService.getOrganizationDocumentsListUsingOrganiztionId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"), from);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}*/
	
	@RequestMapping(value="/searchOrgaiztionDocumentDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchOrgaiztionDocumentDetails(@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request)
	{
		JSONArray json_data_array= null;
		try 
		{
			json_data_array = organizationDocumentService.searchOrgaiztionDocumentDetails(searchOption,searchText,(Integer) request.getSession().getAttribute("ORGANIZATION_ID"),from);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//
	@RequestMapping(value="/updateOrganizationUploadDocumentImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateOrganizationUploadDocumentImage(
			@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
			@RequestParam(value="registrationId",required=true) Integer organizationDocumentId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<OrganizationDocumentImagePath> documentList = null;
		OrganizationDocumentImagePath tempDocumentPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(documentImage!=null&&!(documentImage.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			documentList  = new ArrayList<OrganizationDocumentImagePath>();
			for(MultipartFile file : documentImage){
				if(file!=null){
					tempDocumentPath = new OrganizationDocumentImagePath();
					tempDocumentPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					documentList.add(tempDocumentPath);
				}
			}
			json_ImageArray = organizationDocumentService.updateOrganizationUploadDocumentImage(organizationDocumentId, documentList);
			result.put("MSG", "Organization Document File Upload successful...!");
			result.put("FILE_NAME", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/deleteOrganizationDocumentFileImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteOrganizationDocumentFileImage(@RequestParam(value="orgDocumentPathId",required=false) Integer orgDocumentPathId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(organizationDocumentService.deleteOrganizationDocumentFileImage(orgDocumentPathId)){
				result.put("RESULT",true);
				result.put("MSG","Organization Upload Document Image Deleted Successfully");
			}else{
				result.put("MSG","Organization Upload Document Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/organiztionBankList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String organiztionBankList(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<OrganizationBank> organizationBankList=organizationBankService.loadOrganizationBanksListUsingOrganizationId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(organizationBankList.size()>0)
			{
				for(OrganizationBank obj : organizationBankList)
				{
					if("1".equals(obj.getOrganization().getOrganizationStatus())){
					json=new JSONObject();
					json.put("ORG_BANK_ID",	obj.getOrganizationBankId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("ORG_ID", obj.getOrganization().getOrganizationId());
					json.put("BANK_NAME", obj.getOrganizationBankName());
					json.put("BRANCH_NAME", obj.getOrganizationBankBranch());
					json.put("ACCOUNT_NO",obj.getOrganizationBankAccountNo());
					json.put("IFSC_CODE", obj.getOrganizationBankIfscCode());
					json_data_array.put(json);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//
	@RequestMapping(value="/searchOrganiztionBankDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchOrganiztionBankDetails(@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		List<OrganizationBank> organizationBankList = null;
		try 
		{
			Map<String,Object> map = organizationBankService.searchOrganiztionBankDetails(searchOption,searchText,from,(Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			organizationBankList = (List<OrganizationBank>) map.get("organizationBank");
			Integer totalPages = (Integer)map.get("totalPages");
			
			/*if(organizationBankList.size()>0)
			{*/
				for(OrganizationBank obj : organizationBankList)
				{
					//if("1".equals(obj.getOrganization().getOrganizationStatus())){
					json=new JSONObject();
					json.put("ORG_BANK_ID",	obj.getOrganizationBankId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("BANK_NAME", obj.getOrganizationBankName());
					json.put("BRANCH_NAME", obj.getOrganizationBankBranch());
					json.put("ACCOUNT_NO",obj.getOrganizationBankAccountNo());
					json.put("IFSC_CODE", obj.getOrganizationBankIfscCode());
					json_data_array.put(json);
				}
				//}
				json=new JSONObject();
				json.put("paginationCount",totalPages);
				json_data_array.put(json);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//Organization bank dropdown value list
	@RequestMapping(value="/organiztionBankDropdown",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String organiztionBankDropdown(@RequestParam(value="orgId",required=true) Integer orgId,HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<OrganizationBank> organizationBankList=organizationBankService.loadOrganizationBanksListUsingOrganizationId(orgId);
			if(organizationBankList.size()>0)
			{
				for(OrganizationBank obj : organizationBankList)
				{
					json=new JSONObject();
					json.put("ORG_BANK_ID",	obj.getOrganizationBankId());
					json.put("BANK_NAME", obj.getOrganizationBankName()+","+obj.getOrganizationBankBranch());
					json_data_array.put(json);
				}
				json=new JSONObject();
				json.put("orgBankArray", json_data_array);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json.toString();
	}
	
	@RequestMapping(value="/checkLoginUser",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String checkLoginUser(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			String type=null;
			Integer organizationId=(Integer) request.getSession().getAttribute("ORGANIZATION_ID");
			if(organizationId==null)
			{
				type="SuperAdmin";
			}
			else
			{
				type="Other";
			}
			
			json=new JSONObject();
			json.put("LOGIN_USER",type);
			json_data_array.put(json);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	//Delete ORGANIZATION  Details
	@RequestMapping(value="/deleteOrganiztion",method=RequestMethod.POST)
	private  @ResponseBody String deleteOrganiztion(@RequestParam(value="organizationId",required=false) Integer organizationId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(organizationservices.deleteOrganiztion(organizationId)){
				result.put("MSG","Organiztion  Deleted Successfully");
			}else{
				result.put("MSG","Organiztion Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	//Delete ORGANIZATION  Bank
	@RequestMapping(value="/deleteOrganizationBank",method=RequestMethod.POST)
	private  @ResponseBody String deleteOrganizationBank(@RequestParam(value="organizationBankId",required=false) Integer organizationBankId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(organizationBankService.deleteOrganizationBank(organizationBankId)){
				result.put("MSG","Organiztion Bank Deleted Successfully");
			}else{
				result.put("MSG","Organiztion Bank Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	//Delete ORGANIZATION  Uploaded Document
	@RequestMapping(value="/deleteOrganizationDoc",method=RequestMethod.POST)
	private  @ResponseBody String deleteOrganizationDoc(@RequestParam(value="organizationDocumentId",required=false) Integer organizationDocumentId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(organizationservices.deleteOrganizationDocument(organizationDocumentId)){
				result.put("MSG","Organiztion Document Deleted Successfully");
			}else{
				result.put("MSG","Organiztion Document Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/searchOrganizationDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchOrganizationDetails(@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText, @RequestParam(value="from",required=true) Integer from){
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			Map<String, Object> map = organizationservices.searchOrganizationDetails(searchOption.trim(),searchText.trim(), from);
			List<Organization> organizationList = (List<Organization>) map.get("organizationsList");
			Integer totalPages = (Integer) map.get("totalPages");
			if(organizationList.size()>0)
			{
				for(Organization obj : organizationList)
				{
					json=new JSONObject();
					json.put("ORG_ID", obj.getOrganizationId());
					json.put("ORG_NAME", obj.getOrganizationName());
					json.put("CONTACT_PERSON",obj.getOrganizationContactPerson());
					json.put("MOBILE_NO", obj.getOrganizationMobile());
					json.put("EMAIL_ID", obj.getOrganizationEmailId());
					json_data_array.put(json);
				}
			}
			json=new JSONObject();
			json.put("paginationCount", totalPages);
			json_data_array.put(json);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	//Add by nutan
		@RequestMapping(value="/EditOranizationBankDetails",method=RequestMethod.POST,produces="application/json")
		public @ResponseBody String EditOranizationBankDetails(@RequestParam(value="organizationBankId",required=true) Integer organizationBankId,
				HttpServletRequest request)
		{
			JSONArray json_data_array= new JSONArray();
			JSONObject json=null;
			try 
			{
				OrganizationBank orgBankObj= organizationBankService.loadOrganizationankObjUsingOrganizationBankId(organizationBankId);
						json=new JSONObject();
						json.put("ORG_ID", orgBankObj.getOrganization().getOrganizationId());
						json.put("ORG_NAME", orgBankObj.getOrganization().getOrganizationName());
						json.put("BANK_NAME", orgBankObj.getOrganizationBankName());
						json.put("BRANCH", orgBankObj.getOrganizationBankBranch());
						json.put("ACC_NO",orgBankObj.getOrganizationBankAccountNo());
						json.put("IFSC_CODE", orgBankObj.getOrganizationBankIfscCode());
						CommonMethods common = new CommonMethods();
						json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(orgBankObj.getSubmitDate().toString()));
						json_data_array.put(json);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return json_data_array.toString();
		}
		
		//Add by nutan
				@RequestMapping(value="/UpdateOranizationBankDetails",method=RequestMethod.POST,produces="application/json")
				public @ResponseBody String UpdateOranizationBankDetails(@RequestParam(value="organizationBankId",required=true) Integer organizationBankId,
						@RequestParam(value="branchName",required=true) String branchName,
						@RequestParam(value="ifscCode",required=true) String ifscCode,
						@RequestParam(value="bankName",required=true) String bankName,
						@RequestParam(value="accountNo",required=true) String accountNo,
						//@RequestParam(value="organiztionNameId",required=true) Integer organiztionNameId,
						HttpServletRequest request)
				{
					JSONObject json=null;
					try 
					{
						OrganizationBank orgBankObj= organizationBankService.loadOrganizationankObjUsingOrganizationBankId(organizationBankId);
						orgBankObj.setOrganizationBankName(bankName);
						orgBankObj.setOrganizationBankBranch(branchName);
						orgBankObj.setOrganizationBankAccountNo(accountNo);
						orgBankObj.setOrganizationBankIfscCode(ifscCode);
						//orgBankObj.setOrganization(organizationservices.loadOrganizationObjectUsingOrganiztionId(organiztionNameId));
						json=new JSONObject();
						json.put("MSG", "Organization Bank Updation failed...!");
						if(organizationBankService.updateOrganizationBankObj(orgBankObj)){
							json.put("MSG", "Organization Bank Updated Successfully...!");
							json.put("RESULT", true);
						}
					}
					catch (Exception e) 
					{
						e.printStackTrace();
						log.error(e);
						try {
							json=new JSONObject();
							json.put("MSG", "Operation failed...!");
						} catch (JSONException e1) {
							log.error(e1);
							e1.printStackTrace();
						}
					}
					return json.toString();
				}
}
