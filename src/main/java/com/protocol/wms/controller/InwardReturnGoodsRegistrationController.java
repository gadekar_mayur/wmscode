/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.CheckingDoneCheckingSlipImagePath;
import com.protocol.wms.model.CreditNote;
import com.protocol.wms.model.CreditNoteCreditNoteImagePath;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsClaimCopyPath;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsLrImgPath;
import com.protocol.wms.model.InwardReturnProducts;
import com.protocol.wms.service.InwardReturnGoodsRegistrationService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/InwardReturnGoodsRegistrationController")
public class InwardReturnGoodsRegistrationController {

	private Logger log = Logger.getLogger(InwardReturnGoodsRegistrationController.class.getClass());
	@Autowired
	private HttpSession session;
	
	@Autowired
	@Qualifier("InwardReturnGoodsRegistrationServiceImpl")
	private InwardReturnGoodsRegistrationService inwardReturnGoodsRegistrationService;
	
	@RequestMapping(value="/saveInwardReturnGoodsRegistration",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveInwardReturnGoodsRegistration(
			@RequestParam(value="orgName",required=true) Integer orgId,@RequestParam(value="companyName",required=true) Integer companyId,
			@RequestParam(value="stockistName",required=false) Integer stockistId,@RequestParam(value="transporter",required=false) Integer transporterId,
			@RequestParam(value="employeeName",required=false) Integer employeeId,@RequestParam(value="regDate",required=false) String regDate,		
			@RequestParam(value="time",required=false) String time,@RequestParam(value="driverName",required=false) String driverName,
			@RequestParam(value="transpCharges",required=false) Float transpCharges,@RequestParam(value="driverNo",required=false) String driverNo,
			
			@RequestParam(value="lrNo",required=false) String[] lrNo,@RequestParam(value="noOfCases",required=false) Integer[] noOfCases,
			@RequestParam(value="refOrClaimNo",required=false) String[] refOrClaimNo,@RequestParam(value="claimAmount",required=false) Float[] claimAmount,
			@RequestParam(value="attachLr",required=false) MultipartFile[] attachLr,@RequestParam(value="attachClaimCopy",required=false) MultipartFile[] attachClaimCopy,
			@RequestParam(value="lrFileAttachedCounter",required=false) Integer[] lrFileAttachedCounter,@RequestParam(value="claimCopyFileAttachedCounter",required=false) Integer[] claimCopyFileAttachedCounter,
			
			@RequestParam(value="productName",required=false) String[] productName,@RequestParam(value="claimQuantity",required=false) Integer[] claimQuantity,
			@RequestParam(value="receivedQuantity",required=false) Integer[] receivedQuantity,@RequestParam(value="batch",required=false) String[] batch,
			@RequestParam(value="mfgDate",required=false) String[] mfgDate,@RequestParam(value="expiryDate",required=false) String[] expiryDate,
			@RequestParam(value="mfgCompany",required=false) String[] mfgCompany,@RequestParam(value="reason",required=false) String[] reason,
			@RequestParam(value="quantityVariance",required=false) Integer[] quantityVariance,@RequestParam(value="lrFormIndex",required=false) Integer[] lrFormIndex) throws IOException
	{
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration = null;
		InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails = null;
		List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList = null;
		Map<Integer, List<InwardReturnProducts>> inwardReturnProductsMap = null;
		InwardReturnProducts inwardReturnProducts = null;
		List<InwardReturnProducts> inwardReturnProductsList = null;
		//Format Date
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		JSONObject result = null;
		//
		List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> lrImgPathList =null;
		InwardReturnGoodsRegistrationLRDetailsLrImgPath lrImagePathObj = null;
		List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> claimCopyPathList = null;
		InwardReturnGoodsRegistrationLRDetailsClaimCopyPath claimCopyImgPathObj = null;
		//
		try{
			result = new JSONObject();
			result.put("result", false);
			result.put("MSG", "Inward Return Goods Registration Failed....!");
			
			inwardReturnGoodsRegistration = new InwardReturnGoodsRegistration();
			
			d1 = dtf.parse(regDate);
			inwardReturnGoodsRegistration.setReturnGoodsRegistrationDate(new java.sql.Date(d1.getTime()));
			//
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
			java.util.Date d5 = format.parse(time);
			java.sql.Time	ppstime = new java.sql.Time(d5.getTime());
			inwardReturnGoodsRegistration.setTime(ppstime);
			//set organization submit date
			java.util.Date today = new java.util.Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
	        //
			inwardReturnGoodsRegistration.setDriverName(driverName);
			inwardReturnGoodsRegistration.setTransportationCharges(transpCharges);
			inwardReturnGoodsRegistration.setDriverNo(driverNo);
			inwardReturnGoodsRegistration.setSubmitDate(date);
			inwardReturnGoodsRegistration.setStatus(1);
			inwardReturnGoodsRegistration.setCheckingDoneStatus(0);
			inwardReturnGoodsRegistration.setCreditNoteStatus(0);
			
			lrDetailsList = new ArrayList<InwardReturnGoodsRegistrationLRDetails>();
			inwardReturnProductsMap = new TreeMap<Integer, List<InwardReturnProducts>>();
			List<Integer> lrFormIndexList = null;
			if(lrFormIndex!=null)
				lrFormIndexList = Arrays.asList(lrFormIndex);
			int lrFileIndexCounter = 0;
			int claimCopyIndexCounter = 0;
			for(int i=0;i<(lrNo.length-1);i++){
				// 
				//if(attachLr[i]!=null&&attachClaimCopy[i]!=null){
					inwardReturnGoodsRegistrationLRDetails = new InwardReturnGoodsRegistrationLRDetails();
					inwardReturnGoodsRegistrationLRDetails.setLRNo(lrNo[i]);
					inwardReturnGoodsRegistrationLRDetails.setNoOfCases(noOfCases[i]);
					inwardReturnGoodsRegistrationLRDetails.setRefNoOrClaimNo(refOrClaimNo[i]);
					inwardReturnGoodsRegistrationLRDetails.setClaimAmount(claimAmount[i]);
					inwardReturnGoodsRegistrationLRDetails.setStatus(1);
					lrDetailsList.add(inwardReturnGoodsRegistrationLRDetails);
					//
					lrFileIndexCounter = lrFileIndexCounter + lrFileAttachedCounter[i];
					int k = lrFileIndexCounter-lrFileAttachedCounter[i];
					//Write Images
					lrImgPathList = new ArrayList<InwardReturnGoodsRegistrationLRDetailsLrImgPath>();
					for(;k<lrFileIndexCounter;k++){
						lrImagePathObj = new InwardReturnGoodsRegistrationLRDetailsLrImgPath();
						lrImagePathObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(attachLr[k]));
						lrImgPathList.add(lrImagePathObj);
					}
					inwardReturnGoodsRegistrationLRDetails.setInwardReturnGoodsRegistrationLRDetailsLrImgPathList(lrImgPathList);
					
					claimCopyIndexCounter = claimCopyIndexCounter + claimCopyFileAttachedCounter[i];
					k = claimCopyIndexCounter-claimCopyFileAttachedCounter[i];
					claimCopyPathList = new ArrayList<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath>();
					for(;k<claimCopyIndexCounter;k++){
						claimCopyImgPathObj = new InwardReturnGoodsRegistrationLRDetailsClaimCopyPath();
						claimCopyImgPathObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(attachClaimCopy[k]));
						claimCopyPathList.add(claimCopyImgPathObj);
					}
					inwardReturnGoodsRegistrationLRDetails.setInwardReturnGoodsRegistrationLRDetailsClaimCopyPathList(claimCopyPathList);
					//
					if(lrFormIndex!=null){
						int firstIndex = lrFormIndexList.indexOf(i);
						int lastIndex = lrFormIndexList.lastIndexOf(i);
						inwardReturnProductsList = new ArrayList<InwardReturnProducts>();
						
						for(int j=firstIndex;j<lastIndex;j++){
							inwardReturnProducts = new InwardReturnProducts();
							inwardReturnProducts.setProductName(productName[j]);
							inwardReturnProducts.setClaimQuatity(claimQuantity[j]);
							inwardReturnProducts.setReceivedQuantity(receivedQuantity[j]);
							inwardReturnProducts.setBatch(batch[j]);
							d1 = dtf.parse(mfgDate[j]);
							inwardReturnProducts.setMfgDate(new java.sql.Date(d1.getTime()));
							d1 = dtf.parse(expiryDate[j]);
							inwardReturnProducts.setExpiryDate(new java.sql.Date(d1.getTime()));
							inwardReturnProducts.setMfgCompany(mfgCompany[j]);
							inwardReturnProducts.setReason(reason[j]);
							inwardReturnProducts.setQuantityVariance(quantityVariance[j]);
							inwardReturnProducts.setStatus(1);
							inwardReturnProductsList.add(inwardReturnProducts);
						}
						inwardReturnProductsMap.put(i,  inwardReturnProductsList);
					}
				//}//---
			}
			
			if(inwardReturnGoodsRegistrationService.saveInwardReturnGoodsRegistration(orgId,companyId,stockistId,transporterId,employeeId,lrDetailsList,inwardReturnProductsMap,inwardReturnGoodsRegistration)){
				/*for(int i=0;i<lrDetailsList.size();i++){
					// write image
				    // set the unique name
					   // if(attachLr[i]!=null)
					{
						    byte[] bytes = attachLr[i].getBytes();
						    String orgNameFile = attachLr[i].getOriginalFilename();
							int index = orgNameFile.indexOf(".");
							String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
							File serverFile = new File(Constant.IMAGE_PATH+ fileName);
							BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
							lrDetailsList.get(i).setLRImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
							stream.write(bytes);
							stream.close();
					 }
					   // if(attachClaimCopy[i]!=null)
					{
						    byte[] bytes = attachClaimCopy[i].getBytes();
						    String orgNameFile = attachClaimCopy[i].getOriginalFilename();
							int index = orgNameFile.indexOf(".");
							String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
							File serverFile = new File(Constant.IMAGE_PATH+ fileName);
							BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
							lrDetailsList.get(i).setClaimCopyImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
							stream.write(bytes);
							stream.close();
					}
				}*/
				//to update image path in LR Reg Form
				//if(inwardReturnGoodsRegistrationService.updateInwardReturnGoodsLrReg(lrDetailsList)){
					result.put("result", true);
					result.put("MSG", "Inward Return Goods Registration Successful....! Your Registration Id is "+inwardReturnGoodsRegistration.getInwardReturnGoodRegistrationId());
				//}
			}
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("result", false);
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}
	//
	@RequestMapping(value="/editInwardReturnGoodsRegLreditProductDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editInwardReturnGoodsRegLreditProductDetails(@RequestParam(value="productRegId",required=false) Integer productRegId,
			@RequestParam(value="productName",required=false) String productName,@RequestParam(value="claimQuantity",required=false) Integer claimQuantity,
			@RequestParam(value="receivedQuantity",required=false) Integer receivedQuantity,@RequestParam(value="batch",required=false) String batch,
			@RequestParam(value="mfgDate",required=false) String mfgDate,@RequestParam(value="expiryDate",required=false) String expiryDate,
			@RequestParam(value="mfgCompany",required=false) String mfgCompany,@RequestParam(value="reason",required=false) String reason,
			@RequestParam(value="quantityVariance",required=false) Integer quantityVariance
			) throws IOException 
	{
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		InwardReturnProducts inwardReturnProducts = null;
		JSONObject result = null;
		try{
			inwardReturnProducts = inwardReturnGoodsRegistrationService.loadInwardReturnProductsObjById(productRegId);
			inwardReturnProducts.setProductName(productName);
			inwardReturnProducts.setClaimQuatity(claimQuantity);
			inwardReturnProducts.setReceivedQuantity(receivedQuantity);
			inwardReturnProducts.setBatch(batch);
			d1 = dtf.parse(mfgDate);
			inwardReturnProducts.setMfgDate(new java.sql.Date(d1.getTime()));
			d1 = dtf.parse(expiryDate);
			inwardReturnProducts.setExpiryDate(new java.sql.Date(d1.getTime()));
			inwardReturnProducts.setMfgCompany(mfgCompany);
			inwardReturnProducts.setReason(reason);
			inwardReturnProducts.setQuantityVariance(quantityVariance);
			
			result = new JSONObject();
			result.put("MSG", "Inward Return Goods LR Product Registration Updation Failed....!");
			if(inwardReturnGoodsRegistrationService.editInwardReturnGoodsRegLreditProductDetails(inwardReturnProducts)){
				result.put("PRODUCT_NAME", inwardReturnProducts.getProductName());
				result.put("CLAIM_QUANTITY", inwardReturnProducts.getClaimQuatity());
				result.put("RECEIVED_QUANTITY", inwardReturnProducts.getReceivedQuantity());
				result.put("BATCH", inwardReturnProducts.getBatch());
				String [] date_arr = inwardReturnProducts.getMfgDate().toString().split("-");
				result.put("MFG_DATE", inwardReturnProducts.getMfgDate());
				result.put("EXPIRY_DATE",inwardReturnProducts.getExpiryDate() );
				result.put("EDIT_MFG_DATE",date_arr[1]+"/"+date_arr[2]+"/"+date_arr[0] );
				date_arr = inwardReturnProducts.getExpiryDate().toString().split("-");
				result.put("EDIT_EXPIRY_DATE",date_arr[1]+"/"+date_arr[2]+"/"+date_arr[0]);
				result.put("MFG_COMPANY", inwardReturnProducts.getMfgCompany());
				result.put("QUANTITY_VARIANCE", inwardReturnProducts.getQuantityVariance());
				result.put("REASON", inwardReturnProducts.getReason());
				result.put("MSG", "Inward Return Goods LR Product Registration Updated Successfully....!");
				result.put("RESULT", true);
			}
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}
	//
	@RequestMapping(value="/editInwardReturnGoodsRegLrDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editInwardReturnGoodsRegLrDetails(@RequestParam(value="LR_REG_ID",required=false) Integer LR_REG_ID,
			@RequestParam(value="lrNo",required=false) String lrNo,@RequestParam(value="noOfCases",required=false) Integer noOfCases,
			@RequestParam(value="refOrClaimNo",required=false) String refOrClaimNo,@RequestParam(value="claimAmount",required=false) Float claimAmount
			//,@RequestParam(value="attachLr",required=false) MultipartFile attachLr,@RequestParam(value="attachClaimCopy",required=false) MultipartFile attachClaimCopy
			) throws IOException 
	{
		InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails = null;
		JSONObject result = null;
		try{
			inwardReturnGoodsRegistrationLRDetails =inwardReturnGoodsRegistrationService.loadInwardReturnGoodsRegistrationLRDetails(LR_REG_ID);
			inwardReturnGoodsRegistrationLRDetails.setLRNo(lrNo);
			inwardReturnGoodsRegistrationLRDetails.setNoOfCases(noOfCases);
			inwardReturnGoodsRegistrationLRDetails.setRefNoOrClaimNo(refOrClaimNo);
			inwardReturnGoodsRegistrationLRDetails.setClaimAmount(claimAmount);
			/*if(attachLr!=null)
			{
				    byte[] bytes = attachLr.getBytes();
				    String orgNameFile = attachLr.getOriginalFilename();
					int index = orgNameFile.indexOf(".");
					String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
					File serverFile = new File(Constant.IMAGE_PATH+ fileName);
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					inwardReturnGoodsRegistrationLRDetails.setLRImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
					stream.write(bytes);
					stream.close();
			 }
			if(attachClaimCopy!=null)
			{
				    byte[] bytes = attachClaimCopy.getBytes();
				    String orgNameFile = attachClaimCopy.getOriginalFilename();
					int index = orgNameFile.indexOf(".");
					String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
					File serverFile = new File(Constant.IMAGE_PATH+ fileName);
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					inwardReturnGoodsRegistrationLRDetails.setClaimCopyImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
					stream.write(bytes);
					stream.close();
			}*/
			result = new JSONObject();
			result.put("MSG", "Inward Return Goods LR Registration Updation Failed....!");
			if(inwardReturnGoodsRegistrationService.editInwardReturnGoodsRegLrDetails(inwardReturnGoodsRegistrationLRDetails)){
				result.put("LR_ID", inwardReturnGoodsRegistrationLRDetails.getInwardReturnGoodsRegistrationLRDetailsId());
				result.put("LR_NO", inwardReturnGoodsRegistrationLRDetails.getLRNo());
				result.put("NO_OF_CASES", inwardReturnGoodsRegistrationLRDetails.getNoOfCases());
				result.put("REF_CLAIM_NO", inwardReturnGoodsRegistrationLRDetails.getRefNoOrClaimNo());
				result.put("CLAIM_AMOUNT", inwardReturnGoodsRegistrationLRDetails.getClaimAmount());
				//result.put("ATTACH_LR",inwardReturnGoodsRegistrationLRDetails.getLRImagePath());
				//result.put("ATTACH_CLAIM_COPY", inwardReturnGoodsRegistrationLRDetails.getClaimCopyImagePath());
				result.put("MSG", "Inward Return Goods LR Registration Updated Successfully....!");
				result.put("RESULT", true);
			}
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}
	//
	@RequestMapping(value="/editInwardReturnGoodsRegistration",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editInwardReturnGoodsRegistration(@RequestParam(value="regId",required=false) Integer inwardReturnGoodsRegistrationoId,
			@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="stockistName",required=false) Integer stockistId,@RequestParam(value="transporter",required=false) Integer transporterId,
			@RequestParam(value="employeeName",required=false) Integer employeeId,@RequestParam(value="regDate",required=false) String regDate,		
			@RequestParam(value="time",required=false) String time,@RequestParam(value="driverName",required=false) String driverName,
			@RequestParam(value="transpCharges",required=false) Float transpCharges,@RequestParam(value="driverNo",required=false) String driverNo) throws IOException
	{
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration = null;
		//Format Date
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		JSONObject result = null;
		try{
			inwardReturnGoodsRegistration =inwardReturnGoodsRegistrationService.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(inwardReturnGoodsRegistrationoId);
			
			d1 = dtf.parse(regDate);
			inwardReturnGoodsRegistration.setReturnGoodsRegistrationDate(new java.sql.Date(d1.getTime()));
			//
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
			java.util.Date d5 = format.parse(time);
			java.sql.Time	ppstime = new java.sql.Time(d5.getTime());
			inwardReturnGoodsRegistration.setTime(ppstime);
			
	        //
			inwardReturnGoodsRegistration.setDriverName(driverName);
			inwardReturnGoodsRegistration.setTransportationCharges(transpCharges);
			inwardReturnGoodsRegistration.setDriverNo(driverNo);
			
			result = inwardReturnGoodsRegistrationService.editInwardReturnGoodsRegistration(orgId,companyId,stockistId,transporterId,employeeId,inwardReturnGoodsRegistration);
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}
	//
	@RequestMapping(value="/editInwardReturnGoodsRegistrationAndCheckingDone",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editInwardReturnGoodsRegistration(@RequestParam(value="regId",required=false) Integer inwardReturnGoodsRegistrationoId,
			@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="stockistName",required=false) Integer stockistId,@RequestParam(value="transporter",required=false) Integer transporterId,
			@RequestParam(value="employeeName",required=false) Integer employeeId,@RequestParam(value="regDate",required=false) String regDate,		
			@RequestParam(value="time",required=false) String time,@RequestParam(value="driverName",required=false) String driverName,
			@RequestParam(value="transpCharges",required=false) Float transpCharges,@RequestParam(value="driverNo",required=false) String driverNo,
			//checkingDoneEmployeeDetailsId checkingDoneTime checkingDoneDate checkingDoneRemark CheckingSlip
			@RequestParam(value="CheckingSlip",required=false) MultipartFile checkingDoneFile,
			@RequestParam(value="checkingDoneDate",required=false) String checkingDoneDate,
			@RequestParam(value="checkingDoneTime",required=false) String checkingDoneTime,
			@RequestParam(value="checkingDoneRemark",required=false) String checkingDoneRemark,
			@RequestParam(value="checkingDoneEmployeeDetailsId",required=false) Integer checkingDoneEmployeeDetailsId) throws IOException
	{
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration = null;
		CheckingDone checkingDone = null;
		//Format Date
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		JSONObject result = null;
		try{
			inwardReturnGoodsRegistration =inwardReturnGoodsRegistrationService.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(inwardReturnGoodsRegistrationoId);
			
			d1 = dtf.parse(regDate);
			inwardReturnGoodsRegistration.setReturnGoodsRegistrationDate(new java.sql.Date(d1.getTime()));
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
			java.util.Date d5 = format.parse(time);
			java.sql.Time	ppstime = new java.sql.Time(d5.getTime());
			inwardReturnGoodsRegistration.setTime(ppstime);
			inwardReturnGoodsRegistration.setDriverName(driverName);
			inwardReturnGoodsRegistration.setTransportationCharges(transpCharges);
			inwardReturnGoodsRegistration.setDriverNo(driverNo);
			if(checkingDoneEmployeeDetailsId!=null){
				//checkingDoneFile checkingDoneDate  checkingDoneTime  checkingDoneRemark checkingDoneEmployeeDetailsId
				checkingDone = inwardReturnGoodsRegistrationService.loadCheckingDoneObjByInwardReturnGoodsRegId(inwardReturnGoodsRegistrationoId);
				d1 = dtf.parse(checkingDoneDate);
				checkingDone.setCheckingDoneDate(new java.sql.Date(d1.getTime()));
				checkingDone.setRemark(checkingDoneRemark);
				d5 = format.parse(checkingDoneTime);
				ppstime = new java.sql.Time(d5.getTime());
				checkingDone.setTime(ppstime);
				if(checkingDoneFile!=null){
					checkingDone.setCheckingSlipImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(checkingDoneFile));
				}
			}
			result = inwardReturnGoodsRegistrationService.editInwardReturnGoodsRegistration(orgId,companyId,stockistId,transporterId,employeeId,inwardReturnGoodsRegistration,checkingDoneEmployeeDetailsId,checkingDone);
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}
	//
	@RequestMapping(value="/editInwardReturnGoodsRegCheckingDoneDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editInwardReturnGoodsRegCheckingDoneDetails(@RequestParam(value="regId",required=false) Integer inwardReturnGoodsRegistrationoId,
			//@RequestParam(value="CheckingSlip",required=false) MultipartFile checkingDoneFile,
			@RequestParam(value="checkingDoneDate",required=false) String checkingDoneDate,
			@RequestParam(value="checkingDoneTime",required=false) String checkingDoneTime,
			@RequestParam(value="checkingDoneRemark",required=false) String checkingDoneRemark,
			@RequestParam(value="checkingDoneEmployeeDetailsId",required=false) Integer checkingDoneEmployeeDetailsId) throws IOException
	{
		CheckingDone checkingDone = null;
		JSONObject result = null;
		try{
			DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date d1= null;
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
			checkingDone = inwardReturnGoodsRegistrationService.loadCheckingDoneObjByInwardReturnGoodsRegId(inwardReturnGoodsRegistrationoId);
			d1 = dtf.parse(checkingDoneDate);
			checkingDone.setCheckingDoneDate(new java.sql.Date(d1.getTime()));
			checkingDone.setRemark(checkingDoneRemark);
			java.util.Date d5 = format.parse(checkingDoneTime);
			java.sql.Time	ppstime = new java.sql.Time(d5.getTime());
			checkingDone.setTime(ppstime);
			/*if(checkingDoneFile!=null){
				checkingDone.setCheckingSlipImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(checkingDoneFile));
			}*/
			result = new JSONObject();
			result=inwardReturnGoodsRegistrationService.editInwardReturnGoodsRegCheckingDoneDetails(checkingDone,checkingDoneEmployeeDetailsId);
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}
	//
	@RequestMapping(value="/updateCheckingDoneRegCheckingSlipImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateCheckingDoneRegCheckingSlipImage(
			@RequestParam(value="CheckingSlip",required=true) MultipartFile CheckingSlip[],
			@RequestParam(value="registrationId",required=true) Integer checkingDoneId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<CheckingDoneCheckingSlipImagePath> checkingSlipImgList = null;
		CheckingDoneCheckingSlipImagePath tempCheckingSlipImgPath = null;
		try 
		{
			result.put("MSG", "Checking Done Checking Slip File Upload failed...!");
			if(CheckingSlip!=null&&!(CheckingSlip.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			checkingSlipImgList  = new ArrayList<CheckingDoneCheckingSlipImagePath>();
			for(MultipartFile file : CheckingSlip){
				if(file!=null){
					tempCheckingSlipImgPath = new CheckingDoneCheckingSlipImagePath();
					tempCheckingSlipImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					checkingSlipImgList.add(tempCheckingSlipImgPath);
				}
			}
			json_ImageArray = inwardReturnGoodsRegistrationService.updateCheckingDoneRegCheckingSlipImage(checkingDoneId, checkingSlipImgList);
			result.put("MSG", "Checking Done Checking Slip File Upload successful...!");
			result.put("CHECKING_DONE_SLIP_IMG_PATH", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/deleteCheckingDoneCheckingSlipImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteCheckingDoneCheckingSlipImage(@RequestParam(value="checkingSlipImageId",required=false) Integer checkingSlipImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(inwardReturnGoodsRegistrationService.deleteCheckingDoneCheckingSlipImage(checkingSlipImageId)){
				result.put("RESULT",true);
				result.put("MSG","Checking Done Checking Slip File  Deleted Successfully.....!");
			}else{
				result.put("MSG","Checking Done Checking Slip File Deletion Failed.....!");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/editInwardReturnGoodsRegCreditNoteDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editInwardReturnGoodsRegCreditNoteDetails(@RequestParam(value="creditNoteId",required=false) Integer creditNoteId,
			@RequestParam(value="creditNoteAttach",required=false) MultipartFile creditNoteAttach,
			@RequestParam(value="creditNoteNo",required=false) String creditNoteNo,
			@RequestParam(value="creditNoteAmount",required=false) Float creditNoteAmount,
			@RequestParam(value="creditNoteRemark",required=false) String creditNoteRemark) throws IOException
	{
		CreditNote creditNote = null;
		JSONObject result = null;
		try{
			creditNote = inwardReturnGoodsRegistrationService.loadCreditNoteObjectUsingCreditNoteId(creditNoteId);
			if(creditNoteAttach!=null){
				creditNote.setCreditNoteImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(creditNoteAttach));
			}
			creditNote.setCreditNoteAmount(creditNoteAmount);
			creditNote.setCreditNoteNo(creditNoteNo);
			creditNote.setRemark(creditNoteRemark);
			result = new JSONObject();
			result.put("MSG", "Credit Note Details Updation Failed....!");
			if(inwardReturnGoodsRegistrationService.editInwardReturnGoodsRegCreditNoteDetails(creditNote)){
				result.put("MSG", "Credit Note Details Updated Successfully....!");
				result.put("CREDIT_NOTE_ATTACH", creditNote.getCreditNoteImagePath());
				result.put("RESULT", true);
			}
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}
	//
	@RequestMapping(value="/updateCreditNoteRegCreditNoteAttachImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateCreditNoteRegCreditNoteAttachImage(
			@RequestParam(value="creditNoteAttach",required=true) MultipartFile creditNoteAttach[],
			@RequestParam(value="registrationId",required=true) Integer creditNoteId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<CreditNoteCreditNoteImagePath> creditNoteImgList = null;
		CreditNoteCreditNoteImagePath tempcreditNoteImgPath = null;
		try 
		{
			result.put("MSG", "Credit Note Attach File Upload failed...!");
			if(creditNoteAttach!=null&&!(creditNoteAttach.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			creditNoteImgList  = new ArrayList<CreditNoteCreditNoteImagePath>();
			for(MultipartFile file : creditNoteAttach){
				if(file!=null){
					tempcreditNoteImgPath = new CreditNoteCreditNoteImagePath();
					tempcreditNoteImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					creditNoteImgList.add(tempcreditNoteImgPath);
				}
			}
			json_ImageArray = inwardReturnGoodsRegistrationService.updateCreditNoteRegCreditNoteAttachImage(creditNoteId, creditNoteImgList);
			result.put("MSG", "Credit Note Attach File Upload successful...!");
			result.put("CREDIT_NOTE_ATTACH", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//deleteCreditNoteRegCreditNoteAttachImage
	@RequestMapping(value="/deleteCreditNoteRegCreditNoteAttachImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteCreditNoteRegCreditNoteAttachImage(@RequestParam(value="creditNoteAttachImageId",required=false) Integer creditNoteAttachImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(inwardReturnGoodsRegistrationService.deleteCreditNoteRegCreditNoteAttachImage(creditNoteAttachImageId)){
				result.put("RESULT",true);
				result.put("MSG","Credit Note Attach File  Deleted Successfully.....!");
			}else{
				result.put("MSG","Credit Note Attach File Deletion Failed.....!");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//Inw
	@RequestMapping(value="/inwardReturnGoodsRegListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewInwardReturnGoodsRegListing(){
		JSONObject result = null;
		JSONArray irgrArr = null;
		List<InwardReturnGoodsRegistration>InwardReturnGoodsRegistrationList = null;
		InwardReturnGoodsRegistration tempInwardReturnGoodsReg = null;
		Iterator<InwardReturnGoodsRegistration> irgrItr = null;
		try{
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			irgrArr = new JSONArray();
			InwardReturnGoodsRegistrationList = inwardReturnGoodsRegistrationService.inwardReturnGoodsRegListing(organizationId);
			irgrItr = InwardReturnGoodsRegistrationList.iterator();
			while(irgrItr.hasNext()){
				tempInwardReturnGoodsReg = irgrItr.next();
				result = new JSONObject();
				result.put("ID_NO", tempInwardReturnGoodsReg.getInwardReturnGoodRegistrationId());
				result.put("ORG", tempInwardReturnGoodsReg.getOrganization().getOrganizationName());
				result.put("COMPANY", tempInwardReturnGoodsReg.getCompany().getCompanyName());
				result.put("STOCKIST", tempInwardReturnGoodsReg.getStockist().getStockistName());
				result.put("TRAN", tempInwardReturnGoodsReg.getTransporterDetails().getTransporterName());
				irgrArr.put(result);
			}
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return irgrArr.toString();
	}
	//Inward Return GoodsRegistration Details along with its all LR Registration listing
	@RequestMapping(value="/inwardReturnGoodsRegDetailsAndLrListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewinwardReturnGoodsRegDetailsAndLrListing(@RequestParam(value="ID_NO",required=false) Integer ID_NO){
		JSONObject result = null;
		JSONArray irgrLRArr = null;
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration = null;
		List<InwardReturnGoodsRegistrationLRDetails>inwardReturnGoodsRegistrationLRList = null;
		Map<String,Object> map = null;
		InwardReturnGoodsRegistrationLRDetails tempInwardReturnGoodsRegLR = null;
		Iterator<InwardReturnGoodsRegistrationLRDetails> irgrLRItr = null;
		
		try{
			irgrLRArr = new JSONArray();
			map = inwardReturnGoodsRegistrationService.inwardReturnGoodsRegDetailsAndLrListing(ID_NO);
			inwardReturnGoodsRegistration = (InwardReturnGoodsRegistration) map.get("InwardReturnGoodsRegistration");
			inwardReturnGoodsRegistrationLRList = (List<InwardReturnGoodsRegistrationLRDetails>) map.get("lrList");
			irgrLRItr = inwardReturnGoodsRegistrationLRList.iterator();
			while(irgrLRItr.hasNext()){
				tempInwardReturnGoodsRegLR = irgrLRItr.next();
				result = new JSONObject();
				result.put("LR_ID", tempInwardReturnGoodsRegLR.getInwardReturnGoodsRegistrationLRDetailsId());
				result.put("LR_NO", tempInwardReturnGoodsRegLR.getLRNo());
				result.put("NO_OF_CASES", tempInwardReturnGoodsRegLR.getNoOfCases());
				result.put("REF_CLAIM_NO", tempInwardReturnGoodsRegLR.getRefNoOrClaimNo());
				result.put("CLAIM_AMOUNT", tempInwardReturnGoodsRegLR.getClaimAmount());
				//String s = tempInwardReturnGoodsRegLR.getLRImagePath();int i = s.lastIndexOf('/');s=s.substring(i+1);
				//result.put("ATTACH_LR",s );
				//s = tempInwardReturnGoodsRegLR.getClaimCopyImagePath(); i = s.lastIndexOf('/');s=s.substring(i+1);
				//result.put("ATTACH_CLAIM_COPY", s);
				irgrLRArr.put(result);
			}
			
			result = new JSONObject();
			result.put("ID_NO", inwardReturnGoodsRegistration.getInwardReturnGoodRegistrationId());
			result.put("ORG_ID", inwardReturnGoodsRegistration.getOrganization().getOrganizationId());
			result.put("ORGANIZATION", inwardReturnGoodsRegistration.getOrganization().getOrganizationName());
			result.put("COMPANY_ID", inwardReturnGoodsRegistration.getCompany().getCompanyId());
			result.put("COMPANY", inwardReturnGoodsRegistration.getCompany().getCompanyName());
			result.put("STOCKIST_ID", inwardReturnGoodsRegistration.getStockist().getStockistId());
			result.put("STOCKIST", inwardReturnGoodsRegistration.getStockist().getStockistName());
			result.put("TRANSPORTER_ID", inwardReturnGoodsRegistration.getTransporterDetails().getTransporterDetailsId());
			result.put("TRANSPORTER", inwardReturnGoodsRegistration.getTransporterDetails().getTransporterName());
			String [] reg_date_arr = inwardReturnGoodsRegistration.getReturnGoodsRegistrationDate().toString().split("-");
			result.put("REG_DATE",reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0] );
			//result.put("REG_DATE", inwardReturnGoodsRegistration.getReturnGoodsRegistrationDate());
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
	        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
	        java.util.Date _24HourDt = _24HourSDF.parse(inwardReturnGoodsRegistration.getTime().toString());
            //System.out.println(_24HourDt);
            //System.out.println(_12HourSDF.format(_24HourDt));
			result.put("TIME", _12HourSDF.format(_24HourDt));
			result.put("DRIVER", inwardReturnGoodsRegistration.getDriverName());
			if(inwardReturnGoodsRegistration.getTransportationCharges()!=null)
				result.put("TRANSPORTATION_CHARGES", inwardReturnGoodsRegistration.getTransportationCharges());
			else
				result.put("TRANSPORTATION_CHARGES", "");
			result.put("DRIVER_NO", inwardReturnGoodsRegistration.getDriverNo());
			result.put("SUBMIT_DATE", inwardReturnGoodsRegistration.getSubmitDate());
			result.put("EMPLOYEE_ID", inwardReturnGoodsRegistration.getEmployeeDetails().getEmployeeDetailsId());
			result.put("EMPLOYEE_NAME", inwardReturnGoodsRegistration.getEmployeeDetails().getName());
			result.put("irgrLRArr", irgrLRArr);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	//Inward Return GoodsRegistration Details along with its all LR Registration listing and its Checking Done Details
	@RequestMapping(value="/viewInwardReturnGoodsRegDetailsCheckingDoneDetailsView",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewInwardReturnGoodsRegDetailsCheckingDoneDetailsView(@RequestParam(value="ID_NO",required=false) Integer ID_NO){
		JSONObject result = null;
		JSONArray irgrLRArr = null;
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration = null;
		List<InwardReturnGoodsRegistrationLRDetails>inwardReturnGoodsRegistrationLRList = null;
		Map<String,Object> map = null;
		InwardReturnGoodsRegistrationLRDetails tempInwardReturnGoodsRegLR = null;
		Iterator<InwardReturnGoodsRegistrationLRDetails> irgrLRItr = null;
		
		try{
			irgrLRArr = new JSONArray();
			map = inwardReturnGoodsRegistrationService.inwardReturnGoodsRegDetailsAndLrListing(ID_NO);
			inwardReturnGoodsRegistration = (InwardReturnGoodsRegistration) map.get("InwardReturnGoodsRegistration");
			inwardReturnGoodsRegistrationLRList = (List<InwardReturnGoodsRegistrationLRDetails>) map.get("lrList");
			irgrLRItr = inwardReturnGoodsRegistrationLRList.iterator();
			while(irgrLRItr.hasNext()){
				tempInwardReturnGoodsRegLR = irgrLRItr.next();
				result = new JSONObject();
				result.put("LR_ID", tempInwardReturnGoodsRegLR.getInwardReturnGoodsRegistrationLRDetailsId());
				result.put("LR_NO", tempInwardReturnGoodsRegLR.getLRNo());
				result.put("NO_OF_CASES", tempInwardReturnGoodsRegLR.getNoOfCases());
				result.put("REF_CLAIM_NO", tempInwardReturnGoodsRegLR.getRefNoOrClaimNo());
				result.put("CLAIM_AMOUNT", tempInwardReturnGoodsRegLR.getClaimAmount());
				//String s = tempInwardReturnGoodsRegLR.getLRImagePath();int i = s.lastIndexOf('/');s=s.substring(i+1);
				//result.put("ATTACH_LR",s );
				//s = tempInwardReturnGoodsRegLR.getClaimCopyImagePath(); i = s.lastIndexOf('/');s=s.substring(i+1);
				//result.put("ATTACH_CLAIM_COPY", s);
				irgrLRArr.put(result);
			}
			
			result = new JSONObject();
			//for Edit
			result.put("ORG_ID", inwardReturnGoodsRegistration.getOrganization().getOrganizationId());
			result.put("COMPANY_ID", inwardReturnGoodsRegistration.getCompany().getCompanyId());
			result.put("STOCKIST_ID", inwardReturnGoodsRegistration.getStockist().getStockistId());
			result.put("TRANSPORTER_ID", inwardReturnGoodsRegistration.getTransporterDetails().getTransporterDetailsId());
			result.put("EMPLOYEE_ID", inwardReturnGoodsRegistration.getEmployeeDetails().getEmployeeDetailsId());
			//
			result.put("ID_NO", inwardReturnGoodsRegistration.getInwardReturnGoodRegistrationId());
			result.put("ORGANIZATION", inwardReturnGoodsRegistration.getOrganization().getOrganizationName());
			result.put("COMPANY", inwardReturnGoodsRegistration.getCompany().getCompanyName());
			result.put("STOCKIST", inwardReturnGoodsRegistration.getStockist().getStockistName());
			result.put("TRANSPORTER", inwardReturnGoodsRegistration.getTransporterDetails().getTransporterName());
			String [] reg_date_arr = inwardReturnGoodsRegistration.getReturnGoodsRegistrationDate().toString().split("-");
			result.put("REG_DATE",reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0] );
			//result.put("REG_DATE", inwardReturnGoodsRegistration.getReturnGoodsRegistrationDate());
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
	        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
	        java.util.Date _24HourDt = _24HourSDF.parse(inwardReturnGoodsRegistration.getTime().toString());
			result.put("TIME", _12HourSDF.format(_24HourDt));
			result.put("DRIVER", inwardReturnGoodsRegistration.getDriverName());
			result.put("TRANSPORTATION_CHARGES", inwardReturnGoodsRegistration.getTransportationCharges());
			result.put("DRIVER_NO", inwardReturnGoodsRegistration.getDriverNo());
			result.put("SUBMIT_DATE", inwardReturnGoodsRegistration.getSubmitDate());
			result.put("EMPLOYEE_NAME", inwardReturnGoodsRegistration.getEmployeeDetails().getName());
			//if(inwardReturnGoodsRegistration.getCheckingDoneStatus()==1){
			CheckingDone checkingDone = inwardReturnGoodsRegistrationService.loadCheckingDoneObjByInwardReturnGoodsRegId(ID_NO);
			result.put("CHECKING_DONE_EMP_NAME", checkingDone.getEmployeeDetails().getName());
			reg_date_arr = checkingDone.getCheckingDoneDate().toString().split("-");
			result.put("CHECKING_DONE_DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
			_24HourDt = _24HourSDF.parse(checkingDone.getTime().toString());
			result.put("CHECKING_DONE_TIME", _12HourSDF.format(_24HourDt));
			result.put("CHECKING_DONE_REMARK", checkingDone.getRemark());
			result.put("CHECKING_DONE_SLIP_IMG_PATH", checkingDone.getCheckingSlipImagePath());
			//for Edit Checking Done
			result.put("CHECKING_DONE_EMP_ID", checkingDone.getEmployeeDetails().getEmployeeDetailsId());
			//}
			result.put("irgrLRArr", irgrLRArr);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	//Inward Return GoodsRegistration Details along with its all LR Registration listing and its Credit Note Details
	@RequestMapping(value="/inwardReturnGoodsRegDetailsAndLrListingCheckingDoneView",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String inwardReturnGoodsRegDetailsAndLrListingCheckingDoneView(@RequestParam(value="ID_NO",required=false) Integer ID_NO){
		JSONObject result = null;
		
		try{
			result = inwardReturnGoodsRegistrationService.inwardReturnGoodsRegDetailsAndLrListingCheckingDoneView(ID_NO);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	//Inward Return GoodsRegistration Details along with its all LR Registration listing For Checking Pending
	@RequestMapping(value="/inwardReturnGoodsRegDetailsAndLrListingForCheckingPending",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String inwardReturnGoodsRegDetailsAndLrListingForCheckingPending(@RequestParam(value="ID_NO",required=false) Integer ID_NO){
		JSONObject result = null;
		JSONArray irgrLRArr = null;
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration = null;
		List<InwardReturnGoodsRegistrationLRDetails>inwardReturnGoodsRegistrationLRList = null;
		List<Integer> lrIdList = null;
		Map<String,Object> map = null;
		InwardReturnGoodsRegistrationLRDetails tempInwardReturnGoodsRegLR = null;
		Iterator<InwardReturnGoodsRegistrationLRDetails> irgrLRItr = null;
		
		try{
			irgrLRArr = new JSONArray();
			map = inwardReturnGoodsRegistrationService.inwardReturnGoodsRegDetailsAndLrListingForCheckingPending(ID_NO);
			inwardReturnGoodsRegistration = (InwardReturnGoodsRegistration) map.get("InwardReturnGoodsRegistration");
			inwardReturnGoodsRegistrationLRList = (List<InwardReturnGoodsRegistrationLRDetails>) map.get("lrList");
			lrIdList = (List<Integer>) map.get("lrIdList");
			irgrLRItr = inwardReturnGoodsRegistrationLRList.iterator();
			while(irgrLRItr.hasNext()){
				tempInwardReturnGoodsRegLR = irgrLRItr.next();
				result = new JSONObject();
				result.put("LR_ID", tempInwardReturnGoodsRegLR.getInwardReturnGoodsRegistrationLRDetailsId());
				result.put("LR_NO", tempInwardReturnGoodsRegLR.getLRNo());
				result.put("NO_OF_CASES", tempInwardReturnGoodsRegLR.getNoOfCases());
				result.put("REF_CLAIM_NO", tempInwardReturnGoodsRegLR.getRefNoOrClaimNo());
				result.put("CLAIM_AMOUNT", tempInwardReturnGoodsRegLR.getClaimAmount());
				//String s = tempInwardReturnGoodsRegLR.getLRImagePath();int i = s.lastIndexOf('/');s=s.substring(i+1);
				//result.put("ATTACH_LR",s );
				//s = tempInwardReturnGoodsRegLR.getClaimCopyImagePath(); i = s.lastIndexOf('/');s=s.substring(i+1);
				//result.put("ATTACH_CLAIM_COPY", s);
				if(lrIdList!=null&&lrIdList.contains(tempInwardReturnGoodsRegLR.getInwardReturnGoodsRegistrationLRDetailsId()))
					result.put("HAS_PRODUCT",true);
				else
					result.put("HAS_PRODUCT",false);
				irgrLRArr.put(result);
			}
			
			result = new JSONObject();
			result.put("ID_NO", inwardReturnGoodsRegistration.getInwardReturnGoodRegistrationId());
			result.put("ORGANIZATION", inwardReturnGoodsRegistration.getOrganization().getOrganizationName());
			result.put("COMPANY", inwardReturnGoodsRegistration.getCompany().getCompanyName());
			result.put("STOCKIST", inwardReturnGoodsRegistration.getStockist().getStockistName());
			result.put("TRANSPORTER", inwardReturnGoodsRegistration.getTransporterDetails().getTransporterName());
			result.put("REG_DATE", inwardReturnGoodsRegistration.getReturnGoodsRegistrationDate());
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
	        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm:ss a");
	        java.util.Date _24HourDt = _24HourSDF.parse(inwardReturnGoodsRegistration.getTime().toString());
            //System.out.println(_24HourDt);
            //System.out.println(_12HourSDF.format(_24HourDt));
			result.put("TIME", _12HourSDF.format(_24HourDt));
			if(inwardReturnGoodsRegistration.getDriverName()==null || "".equalsIgnoreCase(inwardReturnGoodsRegistration.getDriverName()))
			{
			result.put("DRIVER", "-");
			}else{
				result.put("DRIVER", inwardReturnGoodsRegistration.getDriverName());
			}
			if(inwardReturnGoodsRegistration.getTransportationCharges()==null || "".equalsIgnoreCase(""+inwardReturnGoodsRegistration.getTransportationCharges()))
			{
			result.put("TRANSPORTATION_CHARGES", "-");
			}else{
				result.put("TRANSPORTATION_CHARGES", inwardReturnGoodsRegistration.getTransportationCharges());
			}
			if(inwardReturnGoodsRegistration.getDriverNo()==null || "".equalsIgnoreCase(inwardReturnGoodsRegistration.getDriverNo()))
			{
			result.put("DRIVER_NO", "-");
			}else{
				result.put("DRIVER_NO", inwardReturnGoodsRegistration.getDriverNo());
			}
			result.put("SUBMIT_DATE", inwardReturnGoodsRegistration.getSubmitDate());
			result.put("EMPLOYEE_NAME", inwardReturnGoodsRegistration.getEmployeeDetails().getName());
			result.put("irgrLRArr", irgrLRArr);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	/*//Inw
	@RequestMapping(value="/inwardReturnGoodsRegLrListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewInwardReturnGoodsRegLrListing(@RequestParam(value="ID_NO",required=false) Integer ID_NO){
		JSONObject result = null;
		JSONArray irgrLRArr = null;
		List<InwardReturnGoodsRegistrationLRDetails>InwardReturnGoodsRegistrationLRList = null;
		InwardReturnGoodsRegistrationLRDetails tempInwardReturnGoodsRegLR = null;
		Iterator<InwardReturnGoodsRegistrationLRDetails> irgrLRItr = null;
		try{
			irgrLRArr = new JSONArray();
			InwardReturnGoodsRegistrationLRList = inwardReturnGoodsRegistrationService.inwardReturnGoodsRegLrListing(ID_NO);
			irgrLRItr = InwardReturnGoodsRegistrationLRList.iterator();
			while(irgrLRItr.hasNext()){
				tempInwardReturnGoodsRegLR = irgrLRItr.next();
				result = new JSONObject();
				result.put("LR_ID", tempInwardReturnGoodsRegLR.getInwardReturnGoodsRegistrationLRDetailsId());
				result.put("LR_NO", tempInwardReturnGoodsRegLR.getLRNo());
				result.put("NO_OF_CASES", tempInwardReturnGoodsRegLR.getNoOfCases());
				result.put("REF_CLAIM_NO", tempInwardReturnGoodsRegLR.getRefNoOrClaimNo());
				result.put("CLAIM_AMOUNT", tempInwardReturnGoodsRegLR.getClaimAmount());
				String s = tempInwardReturnGoodsRegLR.getLRImagePath();int i = s.lastIndexOf('/');s=s.substring(i+1);
				result.put("ATTACH_LR",s );
				s = tempInwardReturnGoodsRegLR.getClaimCopyImagePath(); i = s.lastIndexOf('/');s=s.substring(i+1);
				result.put("ATTACH_CLAIM_COPY", s);
				irgrLRArr.put(result);
			}
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return irgrLRArr.toString();
	}*/
		
	//Inw
	@RequestMapping(value="/LR_DetailedViewsAndLrProductList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewLR_DetailedViewsAndLrProductList(@RequestParam(value="LR_ID",required=false) Integer LR_ID){
		JSONObject result = null;
		try{
			result = inwardReturnGoodsRegistrationService.viewLR_DetailedViewsAndLrProductListView(LR_ID);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	/*@RequestMapping(value="/viewProductListByLR_NO",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewProductListByLR_NO(@RequestParam(value="LR_ID",required=false) Integer LR_ID){
		JSONObject result = null;
		JSONArray productsArr = null;
		List<InwardReturnProducts>inwardReturnProductsList = null;
		InwardReturnProducts tempInwardReturnProducts = null;
		Iterator<InwardReturnProducts> inwardReturnProductsItr = null;
		try{
			productsArr = new JSONArray();
			inwardReturnProductsList = inwardReturnGoodsRegistrationService.inwardProductListByLR_NO(LR_ID);
			inwardReturnProductsItr = inwardReturnProductsList.iterator();
			while(inwardReturnProductsItr.hasNext()){
				tempInwardReturnProducts = inwardReturnProductsItr.next();
				result = new JSONObject();
				result.put("PRODUCT_ID", tempInwardReturnProducts.getInwardReturnProductsId());
				result.put("PRODUCT_NAME", tempInwardReturnProducts.getProductName());
				result.put("CLAIM_QUANTITY", tempInwardReturnProducts.getClaimQuatity());
				result.put("RECEIVED_QUANTITY", tempInwardReturnProducts.getReceivedQuantity());
				result.put("BATCH", tempInwardReturnProducts.getBatch());
				result.put("MFG_DATE", tempInwardReturnProducts.getMfgDate());
				result.put("EXPIRY_DATE",tempInwardReturnProducts.getExpiryDate() );
				result.put("MFG_COMPANY", tempInwardReturnProducts.getMfgCompany());
				result.put("QUANTITY_VARIANCE", tempInwardReturnProducts.getQuantityVariance());
				result.put("REASON", tempInwardReturnProducts.getReason());
				productsArr.put(result);
			}
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return productsArr.toString();
	}*/
	//Delete Inward Return Goods Registration  Details
	@RequestMapping(value="/deleteInwardReturnGoodsReg",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardReturnGoodsReg(@RequestParam(value="ID_NO",required=false) Integer inwardReturnGoodRegistrationId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(inwardReturnGoodsRegistrationService.deleteInwardReturnGoodsRegDetails(inwardReturnGoodRegistrationId)){
				result.put("MSG","Inward Return Goods Registration Details  Deleted Successfully");
			}else{
				result.put("MSG","Inward Return Goods Registration Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//Delete Inward Return Goods Registration LR Details
	@RequestMapping(value="/deleteInwardReturnGoodsRegLrDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardReturnGoodsRegLrDetails(@RequestParam(value="LR_ID",required=false) Integer LR_ID){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(inwardReturnGoodsRegistrationService.deleteInwardReturnGoodsRegLrDetails(LR_ID)){
				result.put("MSG","Inward Return Goods Registration LR Details  Deleted Successfully");
			}else{
				result.put("MSG","Inward Return Goods Registration LR Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//Delete Inward Return Goods Registration LR Product Details
	@RequestMapping(value="/deleteInwardReturnGoodsRegLRProduct",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardReturnGoodsRegLRProduct(@RequestParam(value="PRODUCT_ID",required=false) Integer PRODUCT_ID){
		JSONObject result = null;
		try{                           
			result = new JSONObject();
			//result.put("result",false);
			if(inwardReturnGoodsRegistrationService.deleteInwardReturnGoodsRegLRProduct(PRODUCT_ID)){
				result.put("MSG","Inward Return Goods Registration LR Product Details  Deleted Successfully");
			}else{
				result.put("MSG","Inward Return Goods Registration LR Product Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/searchInwardReturnGoodsRegListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchInwardReturnGoodsRegListing(@RequestParam(value="selectedValue",required=true) String selectedValue ,
			@RequestParam(value="searchText",required=true) String searchText ,
			@RequestParam(value="fromSpecialData",required=true) String fromSpecialData ,
			@RequestParam(value="toSpecialData",required=true) String toSpecialData,
			@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="from",required=true) Integer from)
			{
		JSONObject result = null;
		JSONArray irgrArr = null;
		List<InwardReturnGoodsRegistration>InwardReturnGoodsRegistrationList = null;
		InwardReturnGoodsRegistration tempInwardReturnGoodsReg = null;
		Iterator<InwardReturnGoodsRegistration> irgrItr = null;
		try{
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			irgrArr = new JSONArray();
			Map<String , Object> map = inwardReturnGoodsRegistrationService.searchInwardReturnGoodsRegListing(selectedValue,searchText,fromSpecialData,toSpecialData,searchOption,from,organizationId);
			InwardReturnGoodsRegistrationList = (List<InwardReturnGoodsRegistration>) map.get("InwardReturnGoodsRegistration");
			Integer totalPages = (Integer) map.get("totalPages");
			irgrItr = InwardReturnGoodsRegistrationList.iterator();
			while(irgrItr.hasNext()){
				tempInwardReturnGoodsReg = irgrItr.next();
				result = new JSONObject();
				result.put("ID_NO", tempInwardReturnGoodsReg.getInwardReturnGoodRegistrationId());
				result.put("ORG", tempInwardReturnGoodsReg.getOrganization().getOrganizationName());
				result.put("ORG_ID", tempInwardReturnGoodsReg.getOrganization().getOrganizationId());
				result.put("COMPANY", tempInwardReturnGoodsReg.getCompany().getCompanyName());
				result.put("STOCKIST", tempInwardReturnGoodsReg.getStockist().getStockistName());
				result.put("STOCKIST_ID", tempInwardReturnGoodsReg.getStockist().getStockistId());
				result.put("TRAN", tempInwardReturnGoodsReg.getTransporterDetails().getTransporterName());
				result.put("Inward_Return_Goods_Registration_id", tempInwardReturnGoodsReg.getInwardReturnGoodRegistrationId());
				irgrArr.put(result);
			}
			result = new JSONObject();
			result.put("paginationCount", totalPages);
			irgrArr.put(result);
			
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return irgrArr.toString();
	}
	//
	@RequestMapping(value="/updateInwardReturnGoodsLrRegLrImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateInwardReturnGoodsLrRegLrImage(
			@RequestParam(value="attachLr",required=true) MultipartFile LR_File[],
			@RequestParam(value="registrationId",required=true) Integer inwardReturnGoodsLrRegistrationId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> lrImgList = null;
		InwardReturnGoodsRegistrationLRDetailsLrImgPath tempLrImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(LR_File!=null&&!(LR_File.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			lrImgList  = new ArrayList<InwardReturnGoodsRegistrationLRDetailsLrImgPath>();
			for(MultipartFile file : LR_File){
				if(file!=null){
					tempLrImgPath = new InwardReturnGoodsRegistrationLRDetailsLrImgPath();
					tempLrImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					lrImgList.add(tempLrImgPath);
				}
			}
			json_ImageArray = inwardReturnGoodsRegistrationService.updateInwardReturnGoodsLrRegLrImage(inwardReturnGoodsLrRegistrationId, lrImgList);
			result.put("MSG", "Inward Return Goods Registration Lr Details Lr File Upload successful...!");
			result.put("ATTACH_LR", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//Delete Inward Return Goods LR Registration Lr Images Details
	@RequestMapping(value="/deleteInwardReturnGoodsLrRegLrImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardReturnGoodsLrRegLrImage(@RequestParam(value="lrImageId",required=false) Integer lrImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(inwardReturnGoodsRegistrationService.deleteInwardReturnGoodsLrRegLrImage(lrImageId)){
				result.put("RESULT",true);
				result.put("MSG","Inward Return Goods LR Registration LR Image Deleted Successfully");
			}else{
				result.put("MSG","Inward Return Goods LR Registration LR Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/updateInwardReturnGoodsLrRegClaimCopyImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateInwardReturnGoodsLrRegClaimCopyImage(
			@RequestParam(value="attachClaimCopy",required=true) MultipartFile ClaimCopyFile[],
			@RequestParam(value="registrationId",required=true) Integer inwardReturnGoodsLrRegistrationId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> claimCopyImgList = null;
		InwardReturnGoodsRegistrationLRDetailsClaimCopyPath tempClaimCopyImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(ClaimCopyFile!=null&&!(ClaimCopyFile.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			claimCopyImgList  = new ArrayList<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath>();
			for(MultipartFile file : ClaimCopyFile){
				if(file!=null){
					tempClaimCopyImgPath = new InwardReturnGoodsRegistrationLRDetailsClaimCopyPath();
					tempClaimCopyImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					claimCopyImgList.add(tempClaimCopyImgPath);
				}
			}
			json_ImageArray = inwardReturnGoodsRegistrationService.updateInwardReturnGoodsLrRegClaimCopyImage(inwardReturnGoodsLrRegistrationId, claimCopyImgList);
			result.put("MSG", "Inward Return Goods Registration Lr Details Claim Copy File Upload successful...!");
			result.put("ATTACH_CLAIM_COPY", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//Delete Inward Return Goods LR Registration Claim Copy Images Details
	@RequestMapping(value="/deleteInwardReturnGoodsLrRegClaimCopyImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardReturnGoodsLrRegClaimCopyImage(@RequestParam(value="lrImageId",required=false) Integer lrImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(inwardReturnGoodsRegistrationService.deleteInwardReturnGoodsLrRegClaimCopyImage(lrImageId)){
				result.put("RESULT",true);
				result.put("MSG","Inward Return Goods LR Registration Claim Copy Image Deleted Successfully");
			}else{
				result.put("MSG","Inward Return Goods LR Registration Claim Copy Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed....!");
			}catch(Exception e1){}
		}
		return result.toString();
	}
}
