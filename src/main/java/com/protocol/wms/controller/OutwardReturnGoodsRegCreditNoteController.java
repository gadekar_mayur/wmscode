/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.model.OutWardReturnGoodsCreditNote;
import com.protocol.wms.model.OutWardReturnGoodsCreditNoteCreditNoteImagePath;
import com.protocol.wms.service.OutWardReturnGoodsCreditNoteService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/OutwardReturnGoodsRegCreditNoteController")
public class OutwardReturnGoodsRegCreditNoteController {

	private Logger log = Logger.getLogger(OutwardReturnGoodsRegCreditNoteController.class.getClass());
	
	@Autowired
	@Qualifier("OutWardReturnGoodsCreditNoteServiceImpl")
	private OutWardReturnGoodsCreditNoteService outWardReturnGoodsCreditNoteService;
	
	@Autowired
	private HttpSession session;
	
	@RequestMapping(value="/addOutwardReturnGoodsRegCreditNote",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveOutwardReturnGoodsRegCreditNote(
			@RequestParam(value="organiztionNameId",required=true) Integer organiztionId,@RequestParam(value="creditNoteNo",required=false) String creditNoteNo,
			@RequestParam(value="creditNoteAmount",required=false) Double creditNoteAmount,@RequestParam(value="attachCreditNote",required=false) MultipartFile attachCreditNote[],
			@RequestParam(value="refNoOrClaimNo",required=false) String refNoOrClaimNo,@RequestParam(value="remark",required=false) String remark){
		OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote = null;
		OutWardReturnGoodsCreditNoteCreditNoteImagePath temoCreditNoteFilePath = null;
		JSONObject result = null;
		List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> creditNoteImageList;
		try{
			result = new JSONObject();
			outWardReturnGoodsCreditNote = new OutWardReturnGoodsCreditNote();
			outWardReturnGoodsCreditNote.setCreditNoteAmount(creditNoteAmount);
			outWardReturnGoodsCreditNote.setCreditNoteNo(creditNoteNo);
			outWardReturnGoodsCreditNote.setRefNoOrClaimNo(refNoOrClaimNo);
			outWardReturnGoodsCreditNote.setRemark(remark);
			outWardReturnGoodsCreditNote.setStatus(1);
			creditNoteImageList = new ArrayList<OutWardReturnGoodsCreditNoteCreditNoteImagePath>();
			// write lr image
			for(MultipartFile file : attachCreditNote){
				if(file!=null){
					temoCreditNoteFilePath = new OutWardReturnGoodsCreditNoteCreditNoteImagePath();
					temoCreditNoteFilePath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					creditNoteImageList.add(temoCreditNoteFilePath);
				}
			}
			outWardReturnGoodsCreditNote.setOutWardReturnGoodsCreditNoteCreditNoteImagePathList(creditNoteImageList);
			//set submit date
		    outWardReturnGoodsCreditNote.setSubmitDate(CommonMethods.setSubmitedDate());
			if(outWardReturnGoodsCreditNoteService.saveOutwardReturnGoodsRegCreditNote(organiztionId,outWardReturnGoodsCreditNote)){
				result.put("result", true);
				result.put("MSG", "Add Credit Note Registraion Details Successful....!");
			}
			else
			{
				result.put("MSG", "Add Credit Note Details Failed....!");
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	@RequestMapping(value="/editOutwardReturnGoodsRegCreditNote",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editOutwardReturnGoodsRegCreditNote(@RequestParam(value="creditNoteId",required=true) Integer creditNoteId,
			@RequestParam(value="orgName",required=true) Integer organiztionId,@RequestParam(value="creditNoteNo",required=false) String creditNoteNo,
			@RequestParam(value="creditNoteAmount",required=false) Double creditNoteAmount,//@RequestParam(value="attachCreditNote",required=false) MultipartFile attachCreditNote,
			@RequestParam(value="refNoOrClaimNo",required=false) String refNoOrClaimNo,@RequestParam(value="remark",required=false) String remark){
		OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote = null;
		JSONObject result = null;
		try{
			outWardReturnGoodsCreditNote =outWardReturnGoodsCreditNoteService.loadOutWardReturnGoodsCreditNoteObjById(creditNoteId);
			outWardReturnGoodsCreditNote.setCreditNoteAmount(creditNoteAmount);
			outWardReturnGoodsCreditNote.setCreditNoteNo(creditNoteNo);
			outWardReturnGoodsCreditNote.setRefNoOrClaimNo(refNoOrClaimNo);
			outWardReturnGoodsCreditNote.setRemark(remark);
			/*if(attachCreditNote!=null){
				outWardReturnGoodsCreditNote.setCreditNoteImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(attachCreditNote));
			}*/
		    result = outWardReturnGoodsCreditNoteService.editOutwardReturnGoodsRegCreditNote(organiztionId, outWardReturnGoodsCreditNote);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	@RequestMapping(value="/outwardReturnGoodsRegCreditNoteListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String outwardReturnGoodsRegCreditNoteListing(){
		JSONArray creditNoteArray = null;
		Integer organizationId = null;
		try{
			organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			creditNoteArray = outWardReturnGoodsCreditNoteService.getOutwardReturnGoodsRegCreditNoteList(organizationId);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return creditNoteArray.toString();
	}
	//Delete Outward Return Goods Registration Credit Note Details
	@RequestMapping(value="/deleteOutwardReturnGoodsRegCreditNote",method=RequestMethod.POST)
	private  @ResponseBody String deleteOutwardReturnGoodsRegCreditNote(@RequestParam(value="ID",required=false) Integer creditNoteId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(outWardReturnGoodsCreditNoteService.deleteOutwardReturnGoodsRegCreditNote(creditNoteId)){
				result.put("MSG","Outward Return Goods Registration Credit Note Details  Deleted Successfully");
			}else{
				result.put("MSG","Outward Return Goods Registration Credit Note Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/searchOutwardReturnGoodsRegCreditNote",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchOutwardReturnGoodsRegCreditNote(@RequestParam(value="searchOption",required=true) String selectedValue ,
			@RequestParam(value="searchText",required=true) String searchText ,
			@RequestParam(value="fromSpecialData",required=true) String fromSpecialData ,
			@RequestParam(value="toSpecialData",required=true) String toSpecialData,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		//OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote = null;
		JSONObject tempJsonObj = null;
		JSONArray creditNoteArray = null;
//		List<OutWardReturnGoodsCreditNote> creditNoteList=null;
		java.util.Map<Object, Object>map = null;
		Integer organizationId = null;
		try{
			
			
			organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			creditNoteArray = outWardReturnGoodsCreditNoteService.searchOutwardReturnGoodsRegCreditNote(selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
			/*map = outWardReturnGoodsCreditNoteService.searchOutwardReturnGoodsRegCreditNote(selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
		
			List<OutWardReturnGoodsCreditNote> creditNoteList = (List<OutWardReturnGoodsCreditNote>) map.get("paidInvoice");
			Integer totalPage = (Integer) map.get("totalPages");
			creditNoteArray = new JSONArray();
			for(OutWardReturnGoodsCreditNote tempObj:creditNoteList){
				tempJsonObj = new JSONObject();
				tempJsonObj.put("CREDIT_NOTE_ID",tempObj.getOutWardReturnGoodsCreditNoteId());
				tempJsonObj.put("ORG_ID",tempObj.getOrganization().getOrganizationId());
				tempJsonObj.put("ORG",tempObj.getOrganization().getOrganizationName());
				tempJsonObj.put("CREDIT_NOTE_NO",tempObj.getCreditNoteNo());
				tempJsonObj.put("CREDIT_NOTE_AMOUNT",tempObj.getCreditNoteAmount());
				tempJsonObj.put("REF_CLAIM_NO",tempObj.getRefNoOrClaimNo());//
				tempJsonObj.put("REMARK",tempObj.getRemark());
				creditNoteArray.put(tempJsonObj);
			}
			tempJsonObj=new JSONObject();
			tempJsonObj.put("paginationCount", totalPage);
			creditNoteArray.put(tempJsonObj);*/
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return creditNoteArray.toString();
	}
	//
	@RequestMapping(value="/updateOutwardGoodsRegCreditNoteAttachImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateOutwardGoodsRegCreditNoteAttachImage(
			@RequestParam(value="attachCreditNote",required=true) MultipartFile attachCreditNote[],
			@RequestParam(value="registrationId",required=true) Integer creditNoteId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> creditNoteAttachList = null;
		OutWardReturnGoodsCreditNoteCreditNoteImagePath creditNoteAttachImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(attachCreditNote!=null&&!(attachCreditNote.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			creditNoteAttachList  = new ArrayList<OutWardReturnGoodsCreditNoteCreditNoteImagePath>();
			for(MultipartFile file : attachCreditNote){
				if(file!=null){
					creditNoteAttachImgPath = new OutWardReturnGoodsCreditNoteCreditNoteImagePath();
					creditNoteAttachImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					creditNoteAttachList.add(creditNoteAttachImgPath);
				}
			}
			json_ImageArray = outWardReturnGoodsCreditNoteService.updateOutwardGoodsRegCreditNoteAttachImage(creditNoteId, creditNoteAttachList);
			result.put("MSG", "File Upload successful...!");
			result.put("ATTACH", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/deleteOutwardGoodsRegCreditNoteAttachImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteOutwardGoodsRegCreditNoteAttachImage(@RequestParam(value="creditNoteAttachImageId",required=false) Integer creditNoteAttachImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(outWardReturnGoodsCreditNoteService.deleteOutwardGoodsRegCreditNoteAttachImage(creditNoteAttachImageId)){
				result.put("RESULT",true);
				result.put("MSG","Outward Return Goods Credit Note Attach Image Deleted Successfully");
			}else{
				result.put("MSG","Outward Return Goods Credit Note Attach Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
}
