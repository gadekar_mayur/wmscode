/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.model.OutWardReturnGoodsLRDetails;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsClaimImagePath;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsLrImagePath;
import com.protocol.wms.model.OutWardReturnGoodsRegistration;
import com.protocol.wms.service.OutwardReturnGoodsRegistrationService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/OutwardReturnGoodsRegistrationController")
public class OutwardReturnGoodsRegistrationController {

	private Logger log = Logger.getLogger(OutwardReturnGoodsRegistrationController.class.getClass());
	@Autowired
	private HttpServletRequest session;
	@Autowired
	@Qualifier("OutwardReturnGoodsRegistrationServiceImpl")
	private OutwardReturnGoodsRegistrationService outwardReturnGoodsRegistrationService;
	
	@RequestMapping(value="/saveOutwardReturnGoodsRegistration",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveOutwardReturnGoodsRegistration(
			@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="stockistName",required=false) String stockistId,@RequestParam(value="transporter",required=false) Integer transporterId,
			@RequestParam(value="employeeName",required=false) String employeeId,@RequestParam(value="regDate",required=false) String regDate,		
			@RequestParam(value="time",required=false) String time,@RequestParam(value="driverName",required=false) String driverName,
			@RequestParam(value="transpCharges",required=false) Float transpCharges,@RequestParam(value="driverNo",required=false) String driverNo,
			@RequestParam(value="outwardReturnReason",required=false) String outwardReturnReason,
			
			@RequestParam(value="lrNo",required=false) String[] lrNo,@RequestParam(value="noOfCases",required=false) Integer[] noOfCases,
			@RequestParam(value="refOrClaimNo",required=false) String[] refOrClaimNo,@RequestParam(value="claimAmount",required=false) Float[] claimAmount,
			@RequestParam(value="attachLr",required=false) MultipartFile[] attachLr,@RequestParam(value="attachClaimCopy",required=false) MultipartFile[] attachClaimCopy,
			@RequestParam(value="lrFileAttachedCounter",required=false) Integer[] lrFileAttachedCounter,@RequestParam(value="claimCopyFileAttachedCounter",required=false) Integer[] claimCopyFileAttachedCounter
			) throws IOException
	{
		OutWardReturnGoodsRegistration outWardReturnGoodsRegistration = null;
		OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails = null;
		List<OutWardReturnGoodsLRDetails> lrDetailsList = null;
		//
		List<OutWardReturnGoodsLRDetailsLrImagePath> lrImgPathList =null;
		OutWardReturnGoodsLRDetailsLrImagePath lrImagePathObj = null;
		List<OutWardReturnGoodsLRDetailsClaimImagePath> claimCopyPathList = null;
		OutWardReturnGoodsLRDetailsClaimImagePath claimCopyImgPathObj = null;
		//
		//Format Date
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		JSONObject result = null;
		try{

			result = new JSONObject();
			result.put("result", false);
			result.put("MSG", "Inward Return Goods Registration Failed....!");
			
			outWardReturnGoodsRegistration = new OutWardReturnGoodsRegistration();
			
			d1 = dtf.parse(regDate);
			outWardReturnGoodsRegistration.setRegistrationDate(new java.sql.Date(d1.getTime()));
			//
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
			java.util.Date d5 = format.parse(time);
			java.sql.Time	ppstime = new java.sql.Time(d5.getTime());
			outWardReturnGoodsRegistration.setTime(ppstime);
			//set organization submit date
			java.util.Date today = new java.util.Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
	        //
		    outWardReturnGoodsRegistration.setDriverName(driverName);
		    outWardReturnGoodsRegistration.setTransportationCharges(transpCharges);
		    outWardReturnGoodsRegistration.setDriverNo(driverNo);
		    outWardReturnGoodsRegistration.setReason(outwardReturnReason);
		    outWardReturnGoodsRegistration.setSubmitDate(date);
		    outWardReturnGoodsRegistration.setStatus(1);
			
			lrDetailsList = new ArrayList<OutWardReturnGoodsLRDetails>();
			//
			int lrFileIndexCounter = 0;
			int claimCopyIndexCounter = 0;
			for(int i=0;i<(lrNo.length-1);i++){
				outWardReturnGoodsLRDetails = new OutWardReturnGoodsLRDetails();
				outWardReturnGoodsLRDetails.setLrNo(lrNo[i]);
				outWardReturnGoodsLRDetails.setNoOfCases(noOfCases[i]);
				outWardReturnGoodsLRDetails.setClaimNo(refOrClaimNo[i]);
				outWardReturnGoodsLRDetails.setClaimAmount(claimAmount[i]);
				outWardReturnGoodsLRDetails.setStatus(1);
				outWardReturnGoodsLRDetails.setSubmitDate(date);
				lrFileIndexCounter = lrFileIndexCounter + lrFileAttachedCounter[i];
				int k = lrFileIndexCounter-lrFileAttachedCounter[i];
				lrImgPathList = new ArrayList<OutWardReturnGoodsLRDetailsLrImagePath>();
				for(;k<lrFileIndexCounter;k++){
					lrImagePathObj = new OutWardReturnGoodsLRDetailsLrImagePath();
					lrImagePathObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(attachLr[k]));
					lrImgPathList.add(lrImagePathObj);
				}
				outWardReturnGoodsLRDetails.setOutWardReturnGoodsLRDetailsLrImagePathList(lrImgPathList);
				claimCopyIndexCounter = claimCopyIndexCounter + claimCopyFileAttachedCounter[i];
				k = claimCopyIndexCounter-claimCopyFileAttachedCounter[i];
				claimCopyPathList = new ArrayList<OutWardReturnGoodsLRDetailsClaimImagePath>();
				for(;k<claimCopyIndexCounter;k++){
					claimCopyImgPathObj = new OutWardReturnGoodsLRDetailsClaimImagePath();
					claimCopyImgPathObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(attachClaimCopy[k]));
					claimCopyPathList.add(claimCopyImgPathObj);
				}
				outWardReturnGoodsLRDetails.setOutWardReturnGoodsLRDetailsClaimImagePathList(claimCopyPathList);
				lrDetailsList.add(outWardReturnGoodsLRDetails);
			}
			Integer stockistId1 = null;
			Integer employeeId1 = null;
			try{
				stockistId1 = Integer.parseInt(stockistId);
			}catch(Exception e){}
			try{
				employeeId1 = Integer.parseInt(employeeId);
			}catch(Exception e){}
			if(outwardReturnGoodsRegistrationService.saveOutwardReturnGoodsRegistration(orgId,companyId,stockistId1,transporterId,employeeId1,lrDetailsList,outWardReturnGoodsRegistration)){
				/*for(int i=0;i<lrDetailsList.size();i++){
					// write image
				    // set the unique name
					{
						    byte[] bytes = attachLr[i].getBytes();
						    String orgNameFile = attachLr[i].getOriginalFilename();
							int index = orgNameFile.indexOf(".");
							String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
							File serverFile = new File(Constant.IMAGE_PATH+ fileName);
							BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
							lrDetailsList.get(i).setLrImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
							stream.write(bytes);
							stream.close();
					 }
					  
					{
						    byte[] bytes = attachClaimCopy[i].getBytes();
						    String orgNameFile = attachClaimCopy[i].getOriginalFilename();
							int index = orgNameFile.indexOf(".");
							String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
							File serverFile = new File(Constant.IMAGE_PATH+ fileName);
							BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
							lrDetailsList.get(i).setClaimImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
							stream.write(bytes);
							stream.close();
					}
					//for credit note 
					{
					    byte[] bytes = attachCreditNote[i].getBytes();
					    String orgNameFile = attachCreditNote[i].getOriginalFilename();
						int index = orgNameFile.indexOf(".");
						String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
						File serverFile = new File(Constant.IMAGE_PATH+ fileName);
						BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
						lrCreditNoteList.get(i).setCreditNoteImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
						stream.write(bytes);
						stream.close();
					}
				}
				//to update image path in LR Reg Form
				if(outwardReturnGoodsRegistrationService.updateInwardReturnGoodsLrReg(lrDetailsList)){*/
					result.put("result", true);
					result.put("MSG", "Outward Return Goods Registration Successful....! Your Registration Id is "+outWardReturnGoodsRegistration.getOutWardReturnGoodsRegistrationId());
				/*}*/
			}
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("result", false);
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	
	@RequestMapping(value="/editOutwardReturnGoodsRegLrDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editOutwardReturnGoodsRegLrDetails(@RequestParam(value="LR_REG_ID",required=false) Integer outWardReturnGoodsLRDetailsId,
			@RequestParam(value="lrNo",required=false) String lrNo,@RequestParam(value="noOfCases",required=false) Integer noOfCases,
			@RequestParam(value="refOrClaimNo",required=false) String refOrClaimNo,@RequestParam(value="claimAmount",required=false) Float claimAmount
			//,@RequestParam(value="attachLr",required=false) MultipartFile attachLr,@RequestParam(value="attachClaimCopy",required=false) MultipartFile attachClaimCopy
			) throws IOException 
	{
		OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails = null;
		JSONObject result = null;
		try{
			outWardReturnGoodsLRDetails =outwardReturnGoodsRegistrationService.loadOutWardReturnGoodsLRDetailsByLrRegId(outWardReturnGoodsLRDetailsId);
			outWardReturnGoodsLRDetails.setLrNo(lrNo);
			outWardReturnGoodsLRDetails.setNoOfCases(noOfCases);
			outWardReturnGoodsLRDetails.setClaimNo(refOrClaimNo);
			outWardReturnGoodsLRDetails.setClaimAmount(claimAmount);
			/*if(attachLr!=null)
			{
				    byte[] bytes = attachLr.getBytes();
				    String orgNameFile = attachLr.getOriginalFilename();
					int index = orgNameFile.indexOf(".");
					String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
					File serverFile = new File(Constant.IMAGE_PATH+ fileName);
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					outWardReturnGoodsLRDetails.setLrImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
					stream.write(bytes);
					stream.close();
			 }
			if(attachClaimCopy!=null)
			{
				    byte[] bytes = attachClaimCopy.getBytes();
				    String orgNameFile = attachClaimCopy.getOriginalFilename();
					int index = orgNameFile.indexOf(".");
					String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
					File serverFile = new File(Constant.IMAGE_PATH+ fileName);
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					outWardReturnGoodsLRDetails.setClaimImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
					stream.write(bytes);
					stream.close();
			}*/
			result = new JSONObject();
			result.put("MSG", "Outward Return Goods LR Registration Updation Failed....!");
			if(outwardReturnGoodsRegistrationService.editOutwardReturnGoodsRegLrDetails(outWardReturnGoodsLRDetails)){
				//result.put("LR_ID", outWardReturnGoodsLRDetails.getInwardReturnGoodsRegistrationLRDetailsId());
				result.put("LR_NO", outWardReturnGoodsLRDetails.getLrNo());
				result.put("NO_OF_CASES", outWardReturnGoodsLRDetails.getNoOfCases());
				result.put("REF_CLAIM_NO", outWardReturnGoodsLRDetails.getClaimNo());
				result.put("CLAIM_AMOUNT", outWardReturnGoodsLRDetails.getClaimAmount());
				//result.put("ATTACH_LR",outWardReturnGoodsLRDetails.getLrImagePath());
				//result.put("ATTACH_CLAIM_COPY", outWardReturnGoodsLRDetails.getClaimImagePath());
				result.put("MSG", "Outward Return Goods LR Registration Updated Successfully....!");
				result.put("RESULT", true);
			}
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}
	
	@RequestMapping(value="/editOutwardReturnGoodsRegistration",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editOutwardReturnGoodsRegistration(@RequestParam(value="regId",required=false) Integer outWardReturnGoodsRegistrationId,
			@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="stockistName",required=false) String stockistId,@RequestParam(value="transporter",required=false) Integer transporterId,
			@RequestParam(value="employeeName",required=false) String employeeId,@RequestParam(value="regDate",required=false) String regDate,		
			@RequestParam(value="time",required=false) String time,@RequestParam(value="driverName",required=false) String driverName,
			@RequestParam(value="transpCharges",required=false) Float transpCharges,@RequestParam(value="reason",required=false) String reason,
			@RequestParam(value="driverNo",required=false) String driverNo) throws IOException
	{
		OutWardReturnGoodsRegistration outWardReturnGoodsRegistration = null;
		//Format Date
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		JSONObject result = null;
		SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
		java.sql.Time	ppstime =null;
		try{
			outWardReturnGoodsRegistration =outwardReturnGoodsRegistrationService.loadOutWardReturnGoodsRegistrationObjByOutwardReturnGoodsRegId(outWardReturnGoodsRegistrationId);
			d1 = dtf.parse(regDate);
			outWardReturnGoodsRegistration.setRegistrationDate(new java.sql.Date(d1.getTime()));
			java.util.Date d5 = format.parse(time);
			ppstime = new java.sql.Time(d5.getTime());
			outWardReturnGoodsRegistration.setTime(ppstime);
			outWardReturnGoodsRegistration.setDriverName(driverName);
			outWardReturnGoodsRegistration.setTransportationCharges(transpCharges);
			outWardReturnGoodsRegistration.setDriverNo(driverNo);
			outWardReturnGoodsRegistration.setReason(reason);
			Integer stockistId1 = null;
			Integer employeeId1 = null;
			try{stockistId1 = Integer.parseInt(stockistId);}catch(Exception e){}
			try{employeeId1 = Integer.parseInt(employeeId);}catch(Exception e){}
			result =  outwardReturnGoodsRegistrationService.editOutwardReturnGoodsRegistration(orgId, companyId, stockistId1, transporterId, employeeId1, outWardReturnGoodsRegistration);
		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}
	//Outward Return Goods Registration Listing
	@RequestMapping(value="/outwardReturnGoodsRegListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewOutwardReturnGoodsRegListing(){
		Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
		JSONObject result = null;
		JSONArray irgrArr = null;
		List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegList = null;
		OutWardReturnGoodsRegistration tempInwardReturnGoodsReg = null;
		Iterator<OutWardReturnGoodsRegistration> irgrItr = null;
		try{
			irgrArr = new JSONArray();
			outWardReturnGoodsRegList = outwardReturnGoodsRegistrationService.outwardReturnGoodsRegListing(organizationId);
			irgrItr = outWardReturnGoodsRegList.iterator();
			while(irgrItr.hasNext()){
				tempInwardReturnGoodsReg = irgrItr.next();
				result = new JSONObject();
				result.put("ID_NO", tempInwardReturnGoodsReg.getOutWardReturnGoodsRegistrationId());
				result.put("ORGANIZATION", tempInwardReturnGoodsReg.getOrganization().getOrganizationName());
				result.put("COMPANY", tempInwardReturnGoodsReg.getCompany().getCompanyName());
				result.put("STOCKIST", tempInwardReturnGoodsReg.getStockist().getStockistName());
				result.put("TRAN", tempInwardReturnGoodsReg.getTransporterDetails().getTransporterName());
				irgrArr.put(result);
			}
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return irgrArr.toString();
	}
	//Delete Outward Return Goods Registration Details
	@RequestMapping(value="/deleteOutwardReturnGoodsReg",method=RequestMethod.POST)
	private  @ResponseBody String deleteOutwardReturnGoodsReg(@RequestParam(value="outwardRetGoodsRegId",required=false) Integer outwardRetGoodsRegId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(outwardReturnGoodsRegistrationService.deleteOutwardReturnGoodsRegDetails(outwardRetGoodsRegId)){
				result.put("MSG","Outward Return Goods Registration Details  Deleted Successfully");
			}else{
				result.put("MSG","Outward Return Goods Registration Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//Inward Return GoodsRegistration Details along with its all LR Registration listing
	@RequestMapping(value="/outwardReturnGoodsRegDetailsAndLrListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewOutwardReturnGoodsRegDetailsAndLrListing(@RequestParam(value="ID_NO",required=true) Integer ID_NO,
			@RequestParam(value="editStatus",required=false) Boolean editStatus){
		JSONObject result = null;
		/*JSONArray orgrLRArr = null;
		OutWardReturnGoodsRegistration outWardReturnGoodsRegistration = null;
		List<OutWardReturnGoodsLRDetails>outWardReturnGoodsLRDetailsList = null;
		Map<String,Object> map = null;
		OutWardReturnGoodsLRDetails tempOutWardReturnGoodsLRDetails = null;
		Iterator<OutWardReturnGoodsLRDetails> orgrLRItr = null;
		SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
        java.util.Date _24HourDt = null;*/
		try{
			result = outwardReturnGoodsRegistrationService.outwardReturnGoodsRegDetailsViewAndLrListingView(editStatus, ID_NO);
			//orgrLRArr = new JSONArray();
			//map = outwardReturnGoodsRegistrationService.outwardReturnGoodsRegDetailsAndLrListing(ID_NO);
			/*outWardReturnGoodsRegistration = (OutWardReturnGoodsRegistration) map.get("OutWardReturnGoodsRegistration");
			outWardReturnGoodsLRDetailsList = (List<OutWardReturnGoodsLRDetails>) map.get("lrList");
			orgrLRItr = outWardReturnGoodsLRDetailsList.iterator();
			while(orgrLRItr.hasNext()){
				tempOutWardReturnGoodsLRDetails = orgrLRItr.next();
				result = new JSONObject();
				result.put("LR_ID", tempOutWardReturnGoodsLRDetails.getOutWardReturnGoodsLRDetailsId());
				result.put("LR_NO", tempOutWardReturnGoodsLRDetails.getLrNo());
				result.put("NO_OF_CASES", tempOutWardReturnGoodsLRDetails.getNoOfCases());
				result.put("REF_CLAIM_NO", tempOutWardReturnGoodsLRDetails.getClaimNo());
				result.put("CLAIM_AMOUNT", tempOutWardReturnGoodsLRDetails.getClaimAmount());
				//String s = tempOutWardReturnGoodsLRDetails.getLrImagePath();int i = s.lastIndexOf('/');s=s.substring(i+1);
				result.put("ATTACH_LR",tempOutWardReturnGoodsLRDetails.getLrImagePath() );
				//s = tempOutWardReturnGoodsLRDetails.getClaimImagePath(); i = s.lastIndexOf('/');s=s.substring(i+1);
				result.put("ATTACH_CLAIM_COPY", tempOutWardReturnGoodsLRDetails.getClaimImagePath());
				orgrLRArr.put(result);
			}
			
			result = new JSONObject();
			result.put("ID_NO", outWardReturnGoodsRegistration.getOutWardReturnGoodsRegistrationId());
			result.put("ORGANIZATION", outWardReturnGoodsRegistration.getOrganization().getOrganizationName());
			result.put("COMPANY", outWardReturnGoodsRegistration.getCompany().getCompanyName());
			result.put("STOCKIST", outWardReturnGoodsRegistration.getStockist().getStockistName());
			result.put("TRANSPORTER", outWardReturnGoodsRegistration.getTransporterDetails().getTransporterName());
			String [] reg_date_arr = outWardReturnGoodsRegistration.getRegistrationDate().toString().split("-");
			result.put("REG_DATE",reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0] );
	        _24HourDt = _24HourSDF.parse(outWardReturnGoodsRegistration.getTime().toString());
			result.put("TIME", _12HourSDF.format(_24HourDt));
			result.put("DRIVER", outWardReturnGoodsRegistration.getDriverName());
			result.put("TRANSPORTATION_CHARGES", outWardReturnGoodsRegistration.getTransportationCharges());
			result.put("DRIVER_NO", outWardReturnGoodsRegistration.getDriverNo());
			result.put("SUBMIT_DATE", outWardReturnGoodsRegistration.getSubmitDate());
			result.put("EMPLOYEE_NAME", outWardReturnGoodsRegistration.getEmployeeDetails().getName());
			if(editStatus!=null&&editStatus==true){
				result.put("ORG_ID", outWardReturnGoodsRegistration.getOrganization().getOrganizationId());
				result.put("COMPANY_ID", outWardReturnGoodsRegistration.getCompany().getCompanyId());
				result.put("STOCKIST_ID", outWardReturnGoodsRegistration.getStockist().getStockistId());
				result.put("TRANSPORTER_ID", outWardReturnGoodsRegistration.getTransporterDetails().getTransporterDetailsId());
				result.put("EMPLOYEE_ID", outWardReturnGoodsRegistration.getEmployeeDetails().getEmployeeDetailsId());
			}
			result.put("orgrLRArr", orgrLRArr);*/
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	@RequestMapping(value="/OutwardLR_DetailedViewsAndLrProductList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewOutwardLR_DetailedViewsAndLrProductList(@RequestParam(value="LR_ID",required=false) Integer LR_ID){
		JSONObject result = null;
		//JSONArray productsArr = null;
		OutWardReturnGoodsLRDetails LR_Details = null;
		//OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote = null;
		//List<OutWardReturnGoodsProductRegistration>outwardReturnProductsList = null;
		//OutWardReturnGoodsProductRegistration tempOutwardReturnProducts = null;
		//Iterator<OutWardReturnGoodsProductRegistration> outwardReturnProductsItr = null;
		Map<String,Object> map = null;
		try{
			//productsArr = new JSONArray();
			map = outwardReturnGoodsRegistrationService.viewOutwardLR_DetailedView_LrCreditNoteAndLrProductList(LR_ID);
			LR_Details = (OutWardReturnGoodsLRDetails) map.get("OutWardReturnGoodsLRDetails");
			/*outWardReturnGoodsCreditNote = (OutWardReturnGoodsCreditNote) map.get("OutWardReturnGoodsCreditNote");
			//outwardReturnProductsList = (List<OutWardReturnGoodsProductRegistration>) map.get("productList");
			outwardReturnProductsItr = outwardReturnProductsList.iterator();
			while(outwardReturnProductsItr.hasNext()){
				tempOutwardReturnProducts = outwardReturnProductsItr.next();
				result = new JSONObject();
				result.put("PRODUCT_ID", tempOutwardReturnProducts.getOutWardReturnGoodsProductRegistrationId());
				result.put("PRODUCT_NAME", tempOutwardReturnProducts.getProductName());
				result.put("CLAIM_QUANTITY", tempOutwardReturnProducts.getClaimQuantity());
				result.put("RECEIVED_QUANTITY", tempOutwardReturnProducts.getReceivedQuantity());
				result.put("BATCH", tempOutwardReturnProducts.getBatch());
				result.put("MFG_DATE", tempOutwardReturnProducts.getMfgDate());
				result.put("EXPIRY_DATE",tempOutwardReturnProducts.getExpiryDate() );
				result.put("MFG_COMPANY", tempOutwardReturnProducts.getMfgCompany());
				result.put("QUANTITY_VARIANCE", tempOutwardReturnProducts.getQuantityVariance());
				result.put("REASON", tempOutwardReturnProducts.getReason());
				productsArr.put(result);
			}*/
			
			result = new JSONObject();
			result.put("LR_ID", LR_Details.getOutWardReturnGoodsLRDetailsId());
			result.put("LR_NO", LR_Details.getLrNo());
			result.put("NO_OF_CASES", LR_Details.getNoOfCases());
			result.put("REF_CLAIM_NO", LR_Details.getClaimNo());
			result.put("CLAIM_AMOUNT", LR_Details.getClaimAmount());
			//result.put("ATTACH_LR_PATH",LR_Details.getLrImagePath());
			//result.put("ATTACH_CLAIM_COPY_PATH", LR_Details.getClaimImagePath());
			//String s = LR_Details.getLrImagePath();int i = s.lastIndexOf('/');s=s.substring(i+1);
			//result.put("ATTACH_LR", s);
			//s = LR_Details.getClaimImagePath(); i = s.lastIndexOf('/');s=s.substring(i+1);
			//result.put("ATTACH_CLAIM_COPY", s);
			
			/*result.put("CREDIT_NOTE_NO", outWardReturnGoodsCreditNote.getCreditNoteNo());
			result.put("CREDIT_NOTE_AMOUNT", outWardReturnGoodsCreditNote.getCreditNoteAmount());
			 s = outWardReturnGoodsCreditNote.getCreditNoteImagePath();i = s.lastIndexOf('/');s=s.substring(i+1);
			result.put("CREDIT_NOTE_IMAGE", s);
			result.put("CREDIT_NOTE_ID", outWardReturnGoodsCreditNote.getOutWardReturnGoodsCreditNoteId());*/
			
			//result.put("productsArr", productsArr);
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return result.toString();
	}
	//Delete Outward Return Goods Registration LR Details
	@RequestMapping(value="/deleteOutwardReturnGoodsRegLrDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteOutwardReturnGoodsRegLrDetails(@RequestParam(value="LR_ID",required=false) Integer LR_ID){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(outwardReturnGoodsRegistrationService.deleteOutwardReturnGoodsRegLrDetails(LR_ID)){
				result.put("MSG","Outward Return Goods Registration LR Details  Deleted Successfully");
			}else{
				result.put("MSG","Outward Return Goods Registration LR Details Deletion Failed");
			}
		}catch(Exception e){
			e.printStackTrace();log.error(e);
			try{
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){e1.printStackTrace();log.error(e1);}
		}
		return result.toString();
	}
	//Delete Outward Return Goods Registration LR Product Details
	/*@RequestMapping(value="/deleteOutwardReturnGoodsRegLRProduct",method=RequestMethod.POST)
	private  @ResponseBody String deleteOutwardReturnGoodsRegLRProduct(@RequestParam(value="PRODUCT_ID",required=false) Integer PRODUCT_ID){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(outwardReturnGoodsRegistrationService.deleteOutwardReturnGoodsRegLRProduct(PRODUCT_ID)){
				result.put("MSG","Outward Return Goods Registration LR Product Details  Deleted Successfully");
			}else{
				result.put("MSG","Outward Return Goods Registration LR Product Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}*/
	
	@RequestMapping(value="/searchOutReturnGoodsRegListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchOutReturnGoodsRegListing(@RequestParam(value="searchOption",required=true) String selectedValue ,
			@RequestParam(value="searchText",required=true) String searchText ,
			@RequestParam(value="fromSpecialData",required=true) String fromSpecialData ,
			@RequestParam(value="toSpecialData",required=true) String toSpecialData,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		JSONObject result = null;
		JSONArray irgrArr = null;
//		List<OutWardReturnGoodsRegistration>outReturnGoodsRegistrationList = null;
		OutWardReturnGoodsRegistration tempoutReturnGoodsReg = null;
		Iterator<OutWardReturnGoodsRegistration> irgrItr = null;
		java.util.Map<Object, Object>map = null;
		try{
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			irgrArr = new JSONArray();
			map = outwardReturnGoodsRegistrationService.searchOutReturnGoodsRegListing(selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
			List<OutWardReturnGoodsRegistration> outReturnGoodsRegistrationList = (List<OutWardReturnGoodsRegistration>) map.get("paidInvoice");
			Integer totalPage = (Integer) map.get("totalPages");
			irgrItr = outReturnGoodsRegistrationList.iterator();
			while(irgrItr.hasNext()){
				tempoutReturnGoodsReg = irgrItr.next();
				result = new JSONObject();
				result.put("ID_NO", tempoutReturnGoodsReg.getOutWardReturnGoodsRegistrationId());
				result.put("ORGANIZATION", tempoutReturnGoodsReg.getOrganization().getOrganizationName());
				result.put("COMPANY", tempoutReturnGoodsReg.getCompany().getCompanyName());
				if(tempoutReturnGoodsReg.getStockist()!=null)
					result.put("STOCKIST", tempoutReturnGoodsReg.getStockist().getStockistName());
				else
					result.put("STOCKIST", "");
				result.put("TRAN", tempoutReturnGoodsReg.getTransporterDetails().getTransporterName());
				irgrArr.put(result);
			}
			
			result=new JSONObject();
			result.put("paginationCount", totalPage);
			irgrArr.put(result);
			
		}catch(JSONException e){
			log.error(e);e.printStackTrace();
		}
		catch(Exception e){
			log.error(e);e.printStackTrace();
		}
		return irgrArr.toString();
	}
	//
	@RequestMapping(value="/updateOutwardReturnGoodsLrRegLrImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateOutwardReturnGoodsLrRegLrImage(
			@RequestParam(value="attachLr",required=true) MultipartFile LR_File[],
			@RequestParam(value="registrationId",required=true) Integer outwardReturnGoodsLrRegistrationId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<OutWardReturnGoodsLRDetailsLrImagePath> lrImgList = null;
		OutWardReturnGoodsLRDetailsLrImagePath tempLrImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(LR_File!=null&&!(LR_File.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			lrImgList  = new ArrayList<OutWardReturnGoodsLRDetailsLrImagePath>();
			for(MultipartFile file : LR_File){
				if(file!=null){
					tempLrImgPath = new OutWardReturnGoodsLRDetailsLrImagePath();
					tempLrImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					lrImgList.add(tempLrImgPath);
				}
			}
			json_ImageArray = outwardReturnGoodsRegistrationService.updateOutwardReturnGoodsLrRegLrImage(outwardReturnGoodsLrRegistrationId, lrImgList);
			result.put("MSG", "Outwart Return Goods Registration Lr Details Lr File Upload successful...!");
			result.put("ATTACH_LR", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//Delete Outward Return Goods LR Registration Lr Images Details
	@RequestMapping(value="/deleteOutwardReturnGoodsLrRegLrImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteOutwardReturnGoodsLrRegLrImage(@RequestParam(value="lrImageId",required=false) Integer lrImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(outwardReturnGoodsRegistrationService.deleteOutwardReturnGoodsLrRegLrImage(lrImageId)){
				result.put("RESULT",true);
				result.put("MSG","Outward Return Goods LR Registration LR Image Deleted Successfully");
			}else{
				result.put("MSG","Outward Return Goods LR Registration LR Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/updateOutwardReturnGoodsLrRegClaimCopyImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateOutwardReturnGoodsLrRegClaimCopyImage(
			@RequestParam(value="attachClaimCopy",required=true) MultipartFile attachClaimCopy[],
			@RequestParam(value="registrationId",required=true) Integer outwardReturnGoodsLrRegistrationId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<OutWardReturnGoodsLRDetailsClaimImagePath> claimCopyImgList = null;
		OutWardReturnGoodsLRDetailsClaimImagePath tempClaimCopyImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(attachClaimCopy!=null&&!(attachClaimCopy.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			claimCopyImgList  = new ArrayList<OutWardReturnGoodsLRDetailsClaimImagePath>();
			for(MultipartFile file : attachClaimCopy){
				if(file!=null){
					tempClaimCopyImgPath = new OutWardReturnGoodsLRDetailsClaimImagePath();
					tempClaimCopyImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					claimCopyImgList.add(tempClaimCopyImgPath);
				}
			}
			json_ImageArray = outwardReturnGoodsRegistrationService.updateOutwardReturnGoodsLrRegClaimCopyImage(outwardReturnGoodsLrRegistrationId, claimCopyImgList);
			result.put("MSG", "Outwart Return Goods Registration Lr Details Claim Copy File Upload successful...!");
			result.put("ATTACH_CLAIM_COPY", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/deleteOutwardReturnGoodsLrRegClaimCopyImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteOutwardReturnGoodsLrRegClaimCopyImage(@RequestParam(value="claimCopyImageId",required=false) Integer claimCopyImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(outwardReturnGoodsRegistrationService.deleteOutwardReturnGoodsLrRegClaimCopyImage(claimCopyImageId)){
				result.put("RESULT",true);
				result.put("MSG","Outward Return Goods LR Registration Claim Copy Image Deleted Successfully");
			}else{
				result.put("MSG","Outward Return Goods LR Registration Claim Copy Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
}
