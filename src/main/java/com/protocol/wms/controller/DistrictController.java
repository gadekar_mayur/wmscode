package com.protocol.wms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protocol.wms.model.District;
import com.protocol.wms.service.DistrictService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/DistrictController")
public class DistrictController {
	private Logger log = Logger.getLogger(DistrictController.class.getClass());
	@Autowired
	@Qualifier("DistrictServiceImpl")
	private DistrictService districtService;
	
	@RequestMapping(value="/getDistrict",method=RequestMethod.POST)
	private  @ResponseBody String getDistrictList(@RequestParam(value="stateId",required=false) Integer stateId){
		return districtService.getDistrictList(stateId).toString();
		
	}
	
	@RequestMapping(value="/districtDropDownList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String districtDropDownList(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
				List<District> districtList=districtService.loadDistrictList();
				if(districtList!=null)
				{
					for(District Obj : districtList)
					{
					json=new JSONObject();
					json.put("DISTRICT_NAME", Obj.getDistrictName());
					json.put("DISTRICT_ID", Obj.getDistrictId());
					json_data_array.put(json);
					}
				}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
}
