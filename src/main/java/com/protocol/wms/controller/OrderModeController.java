/**
 * 
 */
package com.protocol.wms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protocol.wms.model.OrderMode;
import com.protocol.wms.service.OrderModeService;

/**
 * @author Sudhakar
 *
 */
@Controller
@RequestMapping(value="/OrderModeController")
public class OrderModeController {

	private Logger log = Logger.getLogger(OrderModeController.class.getClass());
	
	private OrderModeService orderModeService;
	
	@Autowired
	@Qualifier("OrderModeServiceImpl")
	public void setOrderModeService(OrderModeService orderModeService) {
		this.orderModeService = orderModeService;
	}
	
	@RequestMapping(value="/getOrderMode",method=RequestMethod.POST)
	private  @ResponseBody String getOrderMode()
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<OrderMode> orderModeList=orderModeService.loadAllOrderMode();
			if(orderModeList.size()>0)
			{
				for(OrderMode obj : orderModeList)
				{
					json=new JSONObject();
					json.put("ORDER_MODE_ID",obj.getOrderModeId());
					json.put("ORDER_MODE_NAME", obj.getOrderModeName());
					json_data_array.put(json);
				}
			}

		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
}
