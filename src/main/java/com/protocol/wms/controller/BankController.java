/**
 * 
 */
package com.protocol.wms.controller;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;






























import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.dao.OutWardOrderEntryRegistrationDao;
import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyBank;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.InvoicePaymentInformation;
import com.protocol.wms.model.InwardGoodsRegistration;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistrationOrderCopyPath;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.RemainingChequeAmount;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistBankDetails;
import com.protocol.wms.model.StockistChequeDeposit;
import com.protocol.wms.model.StockistChequeReturn;
import com.protocol.wms.service.BankService;

/**
 * @author Sudhakar
 *
 */
@Controller
@RequestMapping(value="/BankController")
public class BankController
{
	
	private Logger log = Logger.getLogger(BankController.class.getClass());
	
	@Autowired
	@Qualifier("BankServiceImpl")
	private BankService bankService;
	
	@Autowired
	private HttpSession session;
	
	
	@RequestMapping(value="/getCompanyBankUsignCompanyIdAndOrgId",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String getCompanyBankUsignCompanyIdAndOrgId(@RequestParam(value="organizationId",required=true) Integer orgnizationId,
			@RequestParam(value="companyId",required=true) Integer companyId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<CompanyBank> companyBanksList=bankService.companyBankDropdown(companyId, orgnizationId);
			if(companyBanksList.size()>0)
			{
				for(CompanyBank obj : companyBanksList)
				{
					json=new JSONObject();
					json.put("COMPANY_BANK_ID",obj.getCompanyBankId());
					json.put("BANK_NAME", obj.getCompanyBankName());
					json_data_array.put(json);
				}
			}
			
		} catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/getStockistBankUsignCompanyIdAndOrgIdAndStockistId",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String getStockistBankUsignCompanyIdAndOrgIdAndStockistId(@RequestParam(value="organizationId",required=true) Integer orgnizationId,
			@RequestParam(value="companyId",required=true) Integer companyId,
			@RequestParam(value="stockistId",required=true) Integer stockistId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<StockistBankDetails> stockistBankDetails=bankService.stockistBankDropdownList(orgnizationId, companyId, stockistId);
			if(stockistBankDetails!=null)
			{
				for(StockistBankDetails obj : stockistBankDetails)
				{
					json=new JSONObject();
					json.put("STOCKIST_BANK_DETAILS_ID", obj.getStockistBankDetailsId());
					json.put("BANK_NAME", obj.getStockistBankName());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/saveStockistAdvancedCheque ",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveStockistAdvancedCheque(@RequestParam(value="selectedOrganiztionNameId",required=true) Integer orgnizationId,
			@RequestParam(value="comapnyId",required=true) Integer comapnyId,
			@RequestParam(value="stockistId",required=false) Integer stockistId,
			@RequestParam(value="bankID",required=true) Integer bankID,
			@RequestParam(value="chequeNo",required=true) String chequeNo[],
			@RequestParam(value="company_stokist_crit",required=true) String company_stokist_crit,
			@RequestParam(value="date",required=true) String advanceCheckDate,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
			AdvancedChequeInformation advancedChequeInformationObj=new AdvancedChequeInformation();
			advancedChequeInformationObj.setStatus(1);
			// set submit date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
		    advancedChequeInformationObj.setSubmitDate(date);
		    // set advance courier date
		    java.util.Date d1 = dtf.parse(advanceCheckDate);
		    advancedChequeInformationObj.setCourierReceivedDate(new java.sql.Date(d1.getTime()));
		    // set no of check
		    advancedChequeInformationObj.setNoOfCheque(chequeNo.length);
		    // set company
		    advancedChequeInformationObj.setCompany(bankService.loadCompanyObjectUsingCompanyId(comapnyId));
		    if(company_stokist_crit.equalsIgnoreCase("Company"))
		    {
		    	// set company id and company bank id
		    	
		    	advancedChequeInformationObj.setCompanyBank(bankService.loadCompanyBankUsignCompanyBankId(bankID));
		    }
		    else
		    {
		    	advancedChequeInformationObj.setStockist(bankService.loadStockistObjUsigStokistId(stockistId));
		    	advancedChequeInformationObj.setStockistBankDetails(bankService.loadStockistBankDetailsUsignStockistBankDetailsId(bankID));
		    }
		    
		    // set  organization
		    advancedChequeInformationObj.setOrganization(bankService.loadOrganizationObjectUsingOrganiztionId(orgnizationId));
		    //save advanced Cheque  Information Obj
		   boolean status= bankService.saveAdvancedChequeInformationObj(advancedChequeInformationObj, chequeNo);
		   if(status==true)
		   {
			   json=new JSONObject();
			   json.put("MSG", "Stockist Advanced Cheque Information Added Successfuly");
			   json_data_array.put(json);
		   }
		   else
		   {
			   json=new JSONObject();
			   json.put("MSG", "Stockist Advanced Cheque Information Not Added ");
			   json_data_array.put(json);
		   }
		    
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/listAdvancedChequeInformation",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listAdvancedChequeInformation(HttpServletRequest request) //@RequestParam(value="AdvanceChequeid",required=true) Integer AdvanceChequeid HttpServletRequest request
//	public @ResponseBody String listAdvancedChequeInformation(@RequestParam(value="AdvanceChequeid",required=true) Integer AdvanceChequeid, HttpServletRequest request) 
	{
		JSONArray json_checque_array= new JSONArray();
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		JSONObject jsonCheque = null;
		try 
		{
			List<AdvancedChequeInformation> advancedChequeInformationList=bankService.loadAdvancedChequeInformationListUsignOrganizationId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(advancedChequeInformationList.size()>0)
			{
				for(AdvancedChequeInformation obj : advancedChequeInformationList)
				{
					json=new JSONObject();
					json.put("ADV_CHEQUE_INFORMATION_ID", obj.getAdvancedChequeInformationId());
					json.put("ORG",obj.getOrganization().getOrganizationName());
					
					if(obj.getCompany()!=null)
					{
						json.put("COMPNAY", obj.getCompany().getCompanyName());
					}
					else
					{
						json.put("COMPNAY", "-");
					}
						
					
					if(obj.getCompanyBank()!=null)
					{
						json.put("COMPANY_BANK",obj.getCompanyBank().getCompanyBankName());
					}
					else
					{
						json.put("COMPANY_BANK","-");
					}
					if(obj.getStockist()!=null)
					{
						json.put("STOCKIST", obj.getStockist().getStockistName());
						json.put("STOCKIST_BANK",obj.getStockistBankDetails().getStockistBankName());
					}
					else
					{
						json.put("STOCKIST", "-");
						json.put("STOCKIST_BANK","-");
					}
					json.put("NO_OF_CHEQUE", obj.getNoOfCheque());
					
					List<ChequeNumberInfo> chequeNumberInfoList = bankService.loadChequeNumberInfoObjUsignAdvanceChequeId((Integer) request.getSession().getAttribute("AdvanceChequeid"));
					if(chequeNumberInfoList.size() > 0)
					{
						for(ChequeNumberInfo objCheque : chequeNumberInfoList)
						{
							jsonCheque = new JSONObject();
							jsonCheque.put("CHEQUE_NO", objCheque.getChequeNumber());
							jsonCheque.put("CHEQUE_NO_INFO_ID",objCheque.getChequeNumberInfoId());
							json_checque_array.put(jsonCheque);
						}
					}
					json.put("CHEQUE_ARRAY", json_checque_array);
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/getCompanyChequeList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String getCompanyChequeList(@RequestParam(value="orgId",required=true) Integer orgId,
			@RequestParam(value="companyId",required=true) Integer companyId,
			@RequestParam(value="companyBankId",required=true) Integer companyBankId,
			HttpServletRequest request)
	{
		return bankService.loadChequeInformationUsignOrgIdAndCompanyIdAndCompanyBankId(orgId, companyId, companyBankId).toString();
	}
	
	@RequestMapping(value="/getStockistChequeList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String getStockistChequeList(@RequestParam(value="orgId",required=true) Integer orgId,
			@RequestParam(value="companyId",required=true) Integer companyId,
			@RequestParam(value="stockistId",required=true) Integer stockistId,
			@RequestParam(value="stockistBankId",required=true) Integer stockistBankId,
			HttpServletRequest request)
	{
		return bankService.loadChequeInformationUsignOrgIdAndCompanyIdAndStockistAndStockistBankId(orgId, companyId, stockistId, stockistBankId).toString();
	}
	
	
	@RequestMapping(value="/saveStockistChequeDepositEntry",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveStockistChequeDepositEntry(@RequestParam(value="selectedOrganiztionNameId",required=true) Integer selectedOrganiztionNameId,
			@RequestParam(value="comapnyId",required=true) Integer comapnyId,
			@RequestParam(value="bankID",required=true) Integer bankID,
			@RequestParam(value="stockistId",required=false) Integer stockistId,
			@RequestParam(value="ChequeNumberInfoId",required=true) Integer ChequeNumberInfoId,
			@RequestParam(value="remark",required=true) String remark,
			@RequestParam(value="depositDate",required=true) String depositDate,
			@RequestParam(value="company_stokist_crit",required=true) String company_stokist_crit,
			@RequestParam(value="ChequeAmount",required=true) Double ChequeAmount,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			System.out.println("org=="+selectedOrganiztionNameId+"**company==="+comapnyId+"**Stockist======"+stockistId+"**stockist bank id=="+bankID);
			Stockist stockistObj=null;
			StockistBankDetails stockistBankDetailsObj=null;
			DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
			StockistChequeDeposit stockistChequeDepositObj=new StockistChequeDeposit();
			stockistChequeDepositObj.setChequeAmount(ChequeAmount);
			stockistChequeDepositObj.setRemark(remark);
			stockistChequeDepositObj.setStatus(1);
			stockistChequeDepositObj.setReturnStatus(0);
			
			// set submit date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
		    stockistChequeDepositObj.setSubmitDate(date);
		    // set deposit date
		    java.util.Date d1 = dtf.parse(depositDate);
		    stockistChequeDepositObj.setDepositDate(new java.sql.Date(d1.getTime()));
		    //set Organization 
		    Organization organizationObj=bankService.loadOrganizationObjectUsingOrganiztionId(selectedOrganiztionNameId);
		    stockistChequeDepositObj.setOrganization(organizationObj);
		    // set company
		    Company companyObj=bankService.loadCompanyObjectUsingCompanyId(comapnyId);
		    stockistChequeDepositObj.setCompany(companyObj);
		    // set ChequeNumberInfo object
		    ChequeNumberInfo chequeNumberInfoObj=bankService.loadChequeNumberInfoObjUsignChequeNumberInfoId(ChequeNumberInfoId);
		    stockistChequeDepositObj.setChequeNumberInfo(chequeNumberInfoObj);
		    if(company_stokist_crit.equalsIgnoreCase("Company"))
		    {
		    	// set company id and company bank id
		    	
		    	stockistChequeDepositObj.setCompanyBank(bankService.loadCompanyBankUsignCompanyBankId(bankID));
		    }
		    else
		    {
		    	stockistObj=bankService.loadStockistObjUsigStokistId(stockistId);
		    	stockistChequeDepositObj.setStockist(stockistObj);
		    	stockistBankDetailsObj=bankService.loadStockistBankDetailsUsignStockistBankDetailsId(bankID);
		    	stockistChequeDepositObj.setStockistBankDetails(stockistBankDetailsObj);
		    }
		    
		    // save stockist Cheque Deposit Obj
		    Integer stockistChequeDepositId=bankService.saveStockistChequeDepositObj(stockistChequeDepositObj);
		    if(stockistChequeDepositId!=null)
		    {
		    	// update cheque information object
		    	chequeNumberInfoObj.setDepositStatus(1);
		    	bankService.updateChequeNumberInfoObj(chequeNumberInfoObj);
		    	
		    	// pay invoice amount
		    	if(company_stokist_crit.equalsIgnoreCase("Stockist"))
		    	{
		    		List<OutWardOrderInvoiceEnteryRegistration> allOutWardOrderInvoiceEnteryRegistrationList=new ArrayList<OutWardOrderInvoiceEnteryRegistration>();
		    		//load out Ward Order Entery Registration Id usign org Id,company Id,stockist Id
		    		System.out.println("org id==="+selectedOrganiztionNameId+"compnay id=="+comapnyId+"stockist id==="+stockistId);
		    		List<Integer> outWardOrderEnteryRegistrationIdList=bankService.loadOutWardOrderEnteryRegistrationIdUsignOrgIdAndCompIdAndStockistId(selectedOrganiztionNameId, comapnyId, stockistId);
		    		if(outWardOrderEnteryRegistrationIdList.size()>0)
		    		{
		    			// load all invoice of outWardOrderEnteryRegistrationId
		    			for(int i=0;i<outWardOrderEnteryRegistrationIdList.size();i++)
		    			{
		    				List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=bankService.loadOutWardOrderInvoiceEnteryRegistrationListUsignoutWardOrderEnteryRegistrationId(outWardOrderEnteryRegistrationIdList.get(i));
		    				if(outWardOrderInvoiceEnteryRegistrationList.size()>0)
		    				{
		    					for(int j=0;j<outWardOrderInvoiceEnteryRegistrationList.size();j++)
								{
		    						allOutWardOrderInvoiceEnteryRegistrationList.add(outWardOrderInvoiceEnteryRegistrationList.get(j));
								}
		    				}
		    			}
		    			// sort allOutWardOrderInvoiceEnteryRegistrationList based on invoice last Date by ascending order
						 if (allOutWardOrderInvoiceEnteryRegistrationList.size() > 0) 
						 {
							    Collections.sort(allOutWardOrderInvoiceEnteryRegistrationList, new Comparator<OutWardOrderInvoiceEnteryRegistration>() {
							        @Override
							        public int compare(final OutWardOrderInvoiceEnteryRegistration object1, final OutWardOrderInvoiceEnteryRegistration object2) 
							        {
							            return object1.getLastDate().compareTo(object2.getLastDate());// for ascending order
							        	//return object2.getLastDate().compareTo(object1.getLastDate()); //for descending order
							        }
							       });
						}
						 
						 // iterate one by one invoice and pay invoice
						 if(allOutWardOrderInvoiceEnteryRegistrationList.size()>0)
						 {
//							// get old remaining payment
//				    			RemainingChequeAmount remainingChequeAmountObjOld=bankService.loadRemainingChequeAmountUsignOrgIdAndCompnayIdAndStockistId(selectedOrganiztionNameId, comapnyId, stockistId);
//				    			if(remainingChequeAmountObjOld!=null)
//				    			{
//				    				ChequeAmount=ChequeAmount+remainingChequeAmountObjOld.getRemainingChequeAmount();
//				    				// delete old remaining payment
//				    				bankService.deleteRemainingChequeAmountObj(bankService.loadRemainingChequeAmountUsignRemainingChequeAmountId(remainingChequeAmountObjOld.getRemainingChequeAmountId()));
//				    			}
							 
							 for(OutWardOrderInvoiceEnteryRegistration obj :allOutWardOrderInvoiceEnteryRegistrationList)
							 {
								 Double invoiceAmount=obj.getRemainingPayment();
								 if(invoiceAmount<=ChequeAmount)
								 {
									 ChequeAmount=ChequeAmount-invoiceAmount;
									 // paid invoice
									 InvoicePaymentInformation invoicePaymentInformationObj=new InvoicePaymentInformation();
									 //set submit date
									invoicePaymentInformationObj.setSubmitDate(date);
									invoicePaymentInformationObj.setPaidAmount(invoiceAmount);
									//setOutWardOrderInvoiceEnteryRegistration
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj=bankService.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(obj.getOutWardOrderInvoiceEnteryRegistrationId());
									invoicePaymentInformationObj.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObj);
									// set chequeNumberInfoObj
									invoicePaymentInformationObj.setChequeNumberInfo(chequeNumberInfoObj);
									//save invoicePaymentInformationObj
									Integer invoicePaymentInformationId=bankService.saveInvoicePaymentInformationObj(invoicePaymentInformationObj);
									if(invoicePaymentInformationId!=null)
									{
										// update OutWardOrderInvoiceEnteryRegistration
										outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(1);
										outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(0.0);
										bankService.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
									}
								 }
								 else
								 {
									Double remainingInvoiceAmount=invoiceAmount-ChequeAmount;
									// paid invoice
									 InvoicePaymentInformation invoicePaymentInformationObj=new InvoicePaymentInformation();
									 //set submit date
									invoicePaymentInformationObj.setSubmitDate(date);
									invoicePaymentInformationObj.setPaidAmount(ChequeAmount);
									//setOutWardOrderInvoiceEnteryRegistration
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj=bankService.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(obj.getOutWardOrderInvoiceEnteryRegistrationId());
									invoicePaymentInformationObj.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObj);
									// set chequeNumberInfoObj
									invoicePaymentInformationObj.setChequeNumberInfo(chequeNumberInfoObj);
									//save invoicePaymentInformationObj
									Integer invoicePaymentInformationId=bankService.saveInvoicePaymentInformationObj(invoicePaymentInformationObj);
									if(invoicePaymentInformationId!=null)
									{
										outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(remainingInvoiceAmount);
										bankService.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
									}
									ChequeAmount=0.0;
									break;
								 }
							 }	 
						 }
		    		}
		    		if(ChequeAmount>0.0)
					 {
//		    			// get old remaining payment
//		    			RemainingChequeAmount remainingChequeAmountObjOld=bankService.loadRemainingChequeAmountUsignOrgIdAndCompnayIdAndStockistId(selectedOrganiztionNameId, comapnyId, stockistId);
//		    			if(remainingChequeAmountObjOld!=null)
//		    			{
//		    				ChequeAmount=ChequeAmount+remainingChequeAmountObjOld.getRemainingChequeAmount();
//		    				// delete old remaining payment
//		    				bankService.deleteRemainingChequeAmountObj(bankService.loadRemainingChequeAmountUsignRemainingChequeAmountId(remainingChequeAmountObjOld.getRemainingChequeAmountId()));
//		    			}
		    			
		    			// save remaining cheque deposit amount
		    			RemainingChequeAmount remainingChequeAmountObj=new RemainingChequeAmount();
		    			remainingChequeAmountObj.setRemainingChequeAmount(ChequeAmount);
		    			remainingChequeAmountObj.setOrganization(organizationObj);
		    			remainingChequeAmountObj.setStockist(stockistObj);
		    			remainingChequeAmountObj.setStockistBankDetails(stockistBankDetailsObj);
		    			remainingChequeAmountObj.setCompany(companyObj);
		    			remainingChequeAmountObj.setStatus(1);
		    			remainingChequeAmountObj.setChequeNumberInfo(chequeNumberInfoObj);
		    			Integer remainingChequeAmountId=bankService.saveRemainingChequeAmountObj(remainingChequeAmountObj);
		    			if(remainingChequeAmountId!=null)
		    			{
		    				System.out.println("remaining amount=="+ChequeAmount);	
		    			}
					 }
		    	}
		    	json=new JSONObject();
		    	json.put("MSG", "Stockist Cheque Deposit Successfully");
		    	json_data_array.put(json);
		    }
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	
	@RequestMapping(value="/listStockistChequeDepositEntry",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listStockistChequeDepositEntry(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<StockistChequeDeposit> stockistChequeDepositList=bankService.loadStockistChequeDepositListUsignOrgnizationId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(stockistChequeDepositList.size()>0)
			{
				for(StockistChequeDeposit obj : stockistChequeDepositList)
				{
					json=new JSONObject();
					json.put("STOCKIST_CHEQUE_DEPOSIT_ID", obj.getStockistChequeDepositId());
					json.put("ORG",obj.getOrganization().getOrganizationName());
					json.put("ORG_ID", obj.getOrganization().getOrganizationId());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("CHEQUE_NUMBER_INFO_ID", obj.getChequeNumberInfo().getChequeNumberInfoId());
					json.put("CHEQUE_NUMBER", obj.getChequeNumberInfo().getChequeNumber());
					json.put("CHEQUE_AMOUNT", obj.getChequeAmount());
					if(obj.getCompanyBank()!=null)
					{
						json.put("COMPANY_BANK",obj.getCompanyBank().getCompanyBankName());
					}
					else
					{
						json.put("COMPANY_BANK","-");
					}
					if(obj.getStockist()!=null)
					{
						json.put("STOCKIST", obj.getStockist().getStockistName());
						json.put("STOCKIST_BANK",obj.getStockistBankDetails().getStockistBankName());
					}
					else
					{
						json.put("STOCKIST", "-");
						json.put("STOCKIST_BANK","-");
					}
					json_data_array.put(json);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/saveStockistReturnChequeDepositEntry",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveStockistReturnChequeDepositEntry(
			@RequestParam(value="stockistChequeDepositId",required=true) Integer stockistChequeDepositId,
			@RequestParam(value="reason",required=true) String reason,
			@RequestParam(value="orgnizationId",required=true) Integer orgnizationId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			StockistChequeReturn stockistChequeReturnObjNew=new StockistChequeReturn();
			stockistChequeReturnObjNew.setReturnReason(reason);
			
			//set submit date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
		    stockistChequeReturnObjNew.setSubmitDate(date);
			//set stockist cheque deposit object
			StockistChequeDeposit stockistChequeDepositObj=bankService.loadStockistChequeDepositObjUsignStockistChequeDepositId(stockistChequeDepositId);
			stockistChequeReturnObjNew.setStockistChequeDeposit(stockistChequeDepositObj);
			// set organization
			stockistChequeReturnObjNew.setOrganization(bankService.loadOrganizationObjectUsingOrganiztionId(orgnizationId));
			
			//save stockist Cheque Return Obj
			Integer stockistChequeReturnId=bankService.saveStockistChequeReturnObj(stockistChequeReturnObjNew);
			if(stockistChequeReturnId!=null)
			{
				// update stockist Cheque deposit Obj
				stockistChequeDepositObj.setReturnStatus(1);
				bankService.updateStockistChequeDepositObj(stockistChequeDepositObj);
				
				// reverse invoice payment
				Integer chequeNumberInfoId=stockistChequeDepositObj.getChequeNumberInfo().getChequeNumberInfoId();
				// load invoice payment information list usign cheque number info id
				List<InvoicePaymentInformation> invoicePaymentInformationList=bankService.loadInvoicePaymentInformationListUsignChequeNumberInfoId(chequeNumberInfoId);
				if(invoicePaymentInformationList.size()>0)
				{
					for(InvoicePaymentInformation obj : invoicePaymentInformationList)
					{
						//load OutWardOrderInvoiceEnteryRegistration
						OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj=bankService.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(obj.getOutWardOrderInvoiceEnteryRegistration().getOutWardOrderInvoiceEnteryRegistrationId());
						Double remainingPayment=outWardOrderInvoiceEnteryRegistrationObj.getRemainingPayment();
						Double paidAmount=obj.getPaidAmount();
						System.out.println("paidAmount=="+paidAmount);
						remainingPayment=remainingPayment+obj.getPaidAmount();
						outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(remainingPayment);
						outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(0);
						// update OutWardOrderInvoiceEnteryRegistration
						bankService.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);

						// load InvoicePaymentInformationObj
						InvoicePaymentInformation invoicePaymentInformationObj=bankService.loadInvoicePaymentInformationUsigInvoicePaymentInformationId(obj.getInvoicePaymentInformationId());
						//delete invoice payment information object
						bankService.deleteInvoicePaymentInformationObj(invoicePaymentInformationObj);
					}
				}
				
					// check RemainingChequeAmount available
					RemainingChequeAmount remainingChequeAmountObj=bankService.loadRemainingChequeAmountObjUsignChequeNumberInfoId(chequeNumberInfoId);
					if(remainingChequeAmountObj!=null)
					{
						bankService.deleteRemainingChequeAmountObj(remainingChequeAmountObj);
					}
					
				
				json=new JSONObject();
				json.put("MSG","Cheque Return Successfuly");
				json_data_array.put(json);
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/listStockistReturnChequeEntry",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listStockistReturnChequeEntry(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<StockistChequeReturn> stockistChequeReturnList=bankService.loadStockistChequeReturnListUsignOrgnizationId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(stockistChequeReturnList.size()>0)
			{
				for(StockistChequeReturn obj :stockistChequeReturnList)
				{
					json=new JSONObject();
					json.put("STOCKIST_CHEQUE_RETURN_ID", obj.getStockistChequeReturnId());
					json.put("ORG",obj.getOrganization().getOrganizationName());
					json.put("ORG_ID", obj.getOrganization().getOrganizationId());
					json.put("COMPANY", obj.getStockistChequeDeposit().getCompany().getCompanyName());
					json.put("STOCKIST_CHEQUE_DEPOSIT_ID", obj.getStockistChequeDeposit().getStockistChequeDepositId());
					json.put("CHEQUE_NUMBER", obj.getStockistChequeDeposit().getChequeNumberInfo().getChequeNumber());
					json.put("CHEQUE_AMOUNT", obj.getStockistChequeDeposit().getChequeAmount());
					json.put("REASON", obj.getReturnReason());
					if(obj.getStockistChequeDeposit().getCompanyBank()!=null)
					{
						json.put("COMPANY_BANK",obj.getStockistChequeDeposit().getCompanyBank().getCompanyBankName());
					}
					else
					{
						json.put("COMPANY_BANK","-");
					}
					if(obj.getStockistChequeDeposit().getStockist()!=null)
					{
						json.put("STOCKIST", obj.getStockistChequeDeposit().getStockist().getStockistName());
						json.put("STOCKIST_BANK",obj.getStockistChequeDeposit().getStockistBankDetails().getStockistBankName());
					}
					else
					{
						json.put("STOCKIST", "-");
						json.put("STOCKIST_BANK","-");
					}
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/reDepositCheque",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String reDepositCheque(
			@RequestParam(value="stockistReturnChequeId",required=true) String stockistReturnChequeId,
			@RequestParam(value="orgValues",required=true) String orgValues,
			@RequestParam(value="stockistDepositChequeId",required=true) String stockistDepositChequeId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
//			String orgIdArray[]=orgValues.split(",");
			String stockistChequeReturnIdArray[]=stockistReturnChequeId.split(",");
			String stockistChequeDepositIdArray[]=stockistDepositChequeId.split(",");
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
			for(int y=0;y<stockistChequeDepositIdArray.length;y++)
			{
				// load stockist Deposit Cheque object
				StockistChequeDeposit stockistChequeDepositObj=bankService.loadStockistChequeDepositObjUsignStockistChequeDepositId(Integer.parseInt(stockistChequeDepositIdArray[y]));
				stockistChequeDepositObj.setReturnStatus(0);
				//update stockist Deposit Cheque object
				bankService.updateStockistChequeDepositObj(stockistChequeDepositObj);
				//delete stockist Return Cheque object
				StockistChequeReturn stockistChequeReturnobj=bankService.loadStockistChequeReturnObjUsignStockistChequeReturnId(Integer.parseInt(stockistChequeReturnIdArray[y]));
				bankService.deleteStockistChequeReturnObj(stockistChequeReturnobj);
				
				
				// pay invoice start logic 
				// check stockist in not null
				if(stockistChequeDepositObj.getStockist()!=null)
				{
					ChequeNumberInfo chequeNumberInfoObj=bankService.loadChequeNumberInfoObjUsignChequeNumberInfoId(stockistChequeDepositObj.getChequeNumberInfo().getChequeNumberInfoId());
					Organization organizationObj=bankService.loadOrganizationObjectUsingOrganiztionId(stockistChequeDepositObj.getOrganization().getOrganizationId());
					Company companyObj=bankService.loadCompanyObjectUsingCompanyId(stockistChequeDepositObj.getCompany().getCompanyId());
					Stockist stockistObj=bankService.loadStockistObjUsigStokistId(stockistChequeDepositObj.getStockist().getStockistId());
					StockistBankDetails stockistBankDetailsObj=bankService.loadStockistBankDetailsUsignStockistBankDetailsId(stockistChequeDepositObj.getStockistBankDetails().getStockistBankDetailsId());
					
					Double ChequeAmount=stockistChequeDepositObj.getChequeAmount();
					List<OutWardOrderInvoiceEnteryRegistration> allOutWardOrderInvoiceEnteryRegistrationList=new ArrayList<OutWardOrderInvoiceEnteryRegistration>();
					//load out Ward Order Entery Registration Id usign org Id,company Id,stockist Id
		    		List<Integer> outWardOrderEnteryRegistrationIdList=bankService.loadOutWardOrderEnteryRegistrationIdUsignOrgIdAndCompIdAndStockistId(stockistChequeDepositObj.getOrganization().getOrganizationId(), stockistChequeDepositObj.getCompany().getCompanyId(), stockistChequeDepositObj.getStockist().getStockistId());
		    		if(outWardOrderEnteryRegistrationIdList.size()>0)
		    		{
		    			// load all invoice of outWardOrderEnteryRegistrationId
		    			for(int i=0;i<outWardOrderEnteryRegistrationIdList.size();i++)
		    			{
		    				List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=bankService.loadOutWardOrderInvoiceEnteryRegistrationListUsignoutWardOrderEnteryRegistrationId(outWardOrderEnteryRegistrationIdList.get(i));
		    				if(outWardOrderInvoiceEnteryRegistrationList.size()>0)
		    				{
		    					for(int j=0;j<outWardOrderInvoiceEnteryRegistrationList.size();j++)
								{
		    						allOutWardOrderInvoiceEnteryRegistrationList.add(outWardOrderInvoiceEnteryRegistrationList.get(j));
								}
		    				}
		    			}
		    			// sort allOutWardOrderInvoiceEnteryRegistrationList based on invoice last Date by ascending order
						 if (allOutWardOrderInvoiceEnteryRegistrationList.size() > 0) 
						 {
							    Collections.sort(allOutWardOrderInvoiceEnteryRegistrationList, new Comparator<OutWardOrderInvoiceEnteryRegistration>() {
							        @Override
							        public int compare(final OutWardOrderInvoiceEnteryRegistration object1, final OutWardOrderInvoiceEnteryRegistration object2) 
							        {
							            return object1.getLastDate().compareTo(object2.getLastDate());// for ascending order
							        	//return object2.getLastDate().compareTo(object1.getLastDate()); //for descending order
							        }
							       });
						}
						 
						// iterate one by one invoice and pay invoice
						 if(allOutWardOrderInvoiceEnteryRegistrationList.size()>0)
						 {
							 for(OutWardOrderInvoiceEnteryRegistration obj :allOutWardOrderInvoiceEnteryRegistrationList)
							 {
								 Double invoiceAmount=obj.getRemainingPayment();
								 if(invoiceAmount<=ChequeAmount)
								 {
									 ChequeAmount=ChequeAmount-invoiceAmount;
									 // paid invoice
									 InvoicePaymentInformation invoicePaymentInformationObj=new InvoicePaymentInformation();
									 //set submit date
									invoicePaymentInformationObj.setSubmitDate(date);
									invoicePaymentInformationObj.setPaidAmount(invoiceAmount);
									//setOutWardOrderInvoiceEnteryRegistration
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj=bankService.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(obj.getOutWardOrderInvoiceEnteryRegistrationId());
									invoicePaymentInformationObj.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObj);
									// set chequeNumberInfoObj
									invoicePaymentInformationObj.setChequeNumberInfo(chequeNumberInfoObj);
									//save invoicePaymentInformationObj
									Integer invoicePaymentInformationId=bankService.saveInvoicePaymentInformationObj(invoicePaymentInformationObj);
									if(invoicePaymentInformationId!=null)
									{
										// update OutWardOrderInvoiceEnteryRegistration
										outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(1);
										outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(0.0);
										bankService.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
									}
								 }
								 else
								 {
									Double remainingInvoiceAmount=invoiceAmount-ChequeAmount;
									// paid invoice
									 InvoicePaymentInformation invoicePaymentInformationObj=new InvoicePaymentInformation();
									 //set submit date
									invoicePaymentInformationObj.setSubmitDate(date);
									invoicePaymentInformationObj.setPaidAmount(ChequeAmount);
									//setOutWardOrderInvoiceEnteryRegistration
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj=bankService.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(obj.getOutWardOrderInvoiceEnteryRegistrationId());
									invoicePaymentInformationObj.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObj);
									// set chequeNumberInfoObj
									invoicePaymentInformationObj.setChequeNumberInfo(chequeNumberInfoObj);
									//save invoicePaymentInformationObj
									Integer invoicePaymentInformationId=bankService.saveInvoicePaymentInformationObj(invoicePaymentInformationObj);
									if(invoicePaymentInformationId!=null)
									{
										outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(remainingInvoiceAmount);
										bankService.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
									}
									ChequeAmount=0.0;
									break;
								 }
							 }	 
						 }
		    		}
		    		if(ChequeAmount>0.0)
					 {
		    			// save remaining cheque deposit amount
		    			RemainingChequeAmount remainingChequeAmountObj=new RemainingChequeAmount();
		    			remainingChequeAmountObj.setRemainingChequeAmount(ChequeAmount);
		    			remainingChequeAmountObj.setOrganization(organizationObj);
		    			remainingChequeAmountObj.setStockist(stockistObj);
		    			remainingChequeAmountObj.setStockistBankDetails(stockistBankDetailsObj);
		    			remainingChequeAmountObj.setCompany(companyObj);
		    			remainingChequeAmountObj.setStatus(1);
		    			remainingChequeAmountObj.setChequeNumberInfo(chequeNumberInfoObj);
		    			Integer remainingChequeAmountId=bankService.saveRemainingChequeAmountObj(remainingChequeAmountObj);
		    			if(remainingChequeAmountId!=null)
		    			{
		    				System.out.println("rmaining amount=="+ChequeAmount);	
		    			}
					 }
				}
				else
				{
					System.out.println("in company ");
				}
			}
				json=new JSONObject();
				json.put("MSG", "Cheque Deposit Successfuly");
				json_data_array.put(json);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
				return json_data_array.toString();
	}
	
	@RequestMapping(value="/listOutstandingChequeDepositEntryDue",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listOutstandingChequeDepositEntryDue(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			java.sql.Date ourJavaDateObject = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=bankService.loadOutWardOrderInvoiceEnteryRegistrationListEqualToCurrentDate(ourJavaDateObject, (Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(outWardOrderInvoiceEnteryRegistrationList.size()>0)
			{
				for(OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEnteryRegistrationList)
				{
					json=new JSONObject();
					json.put("ORDER_ID", obj.getOutWardOrderEnteryRegistration().getOrderId());
					json.put("ORDER_NO", obj.getOutWardOrderEnteryRegistration().getOrderNo());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
					json.put("STOCKIST", obj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
					json.put("INVOICE_NO", obj.getInvoiceNo());
					json.put("INVOICE_AMOUNT", obj.getNetAmount());
					DecimalFormat df = new DecimalFormat("#.##");      
					json.put("REMAINING_AMOUNT",Double.valueOf(df.format(obj.getRemainingPayment())));
//					json.put("NO_OF_DAYS", obj.getNoOfDays());
					json.put("OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID",obj.getOutWardOrderInvoiceEnteryRegistrationId());
					json_data_array.put(json);
				}
			}	
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/listOutstandingChequeDepositEntryLapsed",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listOutstandingChequeDepositEntryLapsed(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			java.sql.Date ourJavaDateObject = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=bankService.loadOutWardOrderInvoiceEnteryRegistrationListLessThanToCurrentDate(ourJavaDateObject, (Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(outWardOrderInvoiceEnteryRegistrationList.size()>0)
			{
				for(OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEnteryRegistrationList)
				{
					json=new JSONObject();
					json.put("ORDER_ID", obj.getOutWardOrderEnteryRegistration().getOrderId());
					json.put("ORDER_NO", obj.getOutWardOrderEnteryRegistration().getOrderNo());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
					json.put("STOCKIST", obj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
					json.put("INVOICE_NO", obj.getInvoiceNo());
					json.put("INVOICE_AMOUNT", obj.getNetAmount());
					DecimalFormat df = new DecimalFormat("#.##");      
					json.put("REMAINING_AMOUNT",Double.valueOf(df.format(obj.getRemainingPayment())));
					
					// calculate no of days
					long MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;
					int day=(int)((ourJavaDateObject.getTime() - obj.getLastDate().getTime()) / MILLISECONDS_IN_DAY);
					json.put("NO_OF_DAYS", day);
					json.put("OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID",obj.getOutWardOrderInvoiceEnteryRegistrationId());
					json_data_array.put(json);
				}
			}	
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	
	@RequestMapping(value="/listOutstandingChequeDepositEntryUpcoming",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listOutstandingChequeDepositEntryUpcoming(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			java.sql.Date ourJavaDateObject = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=bankService.loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(ourJavaDateObject, (Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(outWardOrderInvoiceEnteryRegistrationList.size()>0)
			{
				for(OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEnteryRegistrationList)
				{
					json=new JSONObject();
					json.put("ORDER_ID", obj.getOutWardOrderEnteryRegistration().getOrderId());
					json.put("ORDER_NO", obj.getOutWardOrderEnteryRegistration().getOrderNo());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
					json.put("STOCKIST", obj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
					json.put("INVOICE_NO", obj.getInvoiceNo());
					json.put("INVOICE_AMOUNT", obj.getNetAmount());
					DecimalFormat df = new DecimalFormat("#.##");      
					json.put("REMAINING_AMOUNT",Double.valueOf(df.format(obj.getRemainingPayment())));
					
					//calculate no of days
					long MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;
					int day=(int)((obj.getLastDate().getTime()-ourJavaDateObject.getTime()) / MILLISECONDS_IN_DAY);
					json.put("NO_OF_DAYS", day);
					json.put("OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID",obj.getOutWardOrderInvoiceEnteryRegistrationId());
					json_data_array.put(json);
				}
			}	
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/listOutstandingChequeDepositEntryPaidInvoice",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listOutstandingChequeDepositEntryPaidInvoice(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=bankService.loadOutWardOrderInvoiceEnteryRegistrationListWithStatusOne( (Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(outWardOrderInvoiceEnteryRegistrationList.size()>0)
			{
				for(OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEnteryRegistrationList)
				{
					Double netAmount=obj.getNetAmount();
					Double remainingAmount=obj.getRemainingPayment();
					
					if(!netAmount.equals(remainingAmount))
					{
						json=new JSONObject();
						json.put("ORDER_ID", obj.getOutWardOrderEnteryRegistration().getOrderId());
						json.put("ORDER_NO", obj.getOutWardOrderEnteryRegistration().getOrderNo());
						json.put("ORG", obj.getOrganization().getOrganizationName());
						json.put("COMPANY", obj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
						json.put("STOCKIST", obj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
						json.put("INVOICE_NO", obj.getInvoiceNo());
						json.put("INVOICE_AMOUNT", obj.getNetAmount());
						DecimalFormat df = new DecimalFormat("#.##");
						json.put("PAID_AMOUNT",Double.valueOf(df.format(netAmount-remainingAmount)));
						json.put("REMAINING_AMOUNT",Double.valueOf(df.format(obj.getRemainingPayment())));
						json.put("OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID",obj.getOutWardOrderInvoiceEnteryRegistrationId());
						json_data_array.put(json);
					}
				}
			}	
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	
	@RequestMapping(value="/ViewStockistAdvanceCheck",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String ViewStockistAdvanceCheck(@RequestParam(value="AdvanceChequeid",required=true) Integer AdvanceChequeid,
			HttpServletRequest request)
	{
		System.out.println(AdvanceChequeid);
		JSONArray json_data_array= new JSONArray();
		JSONArray json_checque_array= new JSONArray();
		JSONObject json=null;
		JSONObject jsonCheque = null;
		try 
		{
			CommonMethods common = new CommonMethods();
			AdvancedChequeInformation advancedChequeInformationObj=bankService.loadAdvancedChequeInformationObjUsignAdvanceChequeId(AdvanceChequeid);
			if(advancedChequeInformationObj!=null)
			{
				json=new JSONObject();
				json.put("ORG_NAME", advancedChequeInformationObj.getOrganization().getOrganizationName());
				if(advancedChequeInformationObj.getCompany()!=null)
				{
				json.put("COMP_NAME", advancedChequeInformationObj.getCompany().getCompanyName());
				}else
				{
					json.put("COMP_NAME","-");
				}
				if(advancedChequeInformationObj.getStockist() !=null)
				{
				json.put("STOCKIST", advancedChequeInformationObj.getStockist().getStockistName());
				}
				else
				{
					json.put("STOCKIST","-");
				}
				if(advancedChequeInformationObj.getStockistBankDetails()!=null)
				{
				json.put("BANK", advancedChequeInformationObj.getStockistBankDetails().getStockistBankName());
				}
				else{
					json.put("BANK", "-");
				}
				if(advancedChequeInformationObj.getCompanyBank()!=null)
				{
				json.put("COMPANY_BANK", advancedChequeInformationObj.getCompanyBank().getCompanyBankName());
				}
				else{
					json.put("COMPANY_BANK", "-");
				}
				json.put("NO_OF_CHEQUE", advancedChequeInformationObj.getNoOfCheque());
				json.put("COURIER_RECEVIE_DATE", advancedChequeInformationObj.getCourierReceivedDate());
				json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(advancedChequeInformationObj.getSubmitDate().toString()));
				List<ChequeNumberInfo> chequeNumberInfoList = bankService.loadChequeNumberInfoObjUsignAdvanceChequeId(AdvanceChequeid);
				if(chequeNumberInfoList.size() > 0)
				{
					for(ChequeNumberInfo obj : chequeNumberInfoList)
					{
						jsonCheque = new JSONObject();
						jsonCheque.put("CHEQUE_NO", obj.getChequeNumber());
						jsonCheque.put("CHEQUE_NO_INFO_ID",obj.getChequeNumberInfoId());
						json_checque_array.put(jsonCheque);
					}
				}
				json.put("CHEQUE_ARRAY", json_checque_array);
				json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	private OutWardOrderEntryRegistrationDao outWardOrderEntryRegistrationDao;
	@RequestMapping(value="/listOutstandingChequeDepositEntryTodaysDue",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listOutstandingChequeDepositEntryTodaysDue(@RequestParam(value="OutWardOrderInvoiceEnteryRegistrationId",required=true) Integer OutWardOrderInvoiceEnteryRegistrationId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		CommonMethods common = new CommonMethods();
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
	
		try 
		{
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj=bankService.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(OutWardOrderInvoiceEnteryRegistrationId);
			
		if(outWardOrderInvoiceEnteryRegistrationObj!=null)
			{  
			
				json=new JSONObject();
				Double netAmount=outWardOrderInvoiceEnteryRegistrationObj.getNetAmount();
				Double remainingAmount=outWardOrderInvoiceEnteryRegistrationObj.getRemainingPayment();
				
				
				json.put("INVOICE_NO", outWardOrderInvoiceEnteryRegistrationObj.getInvoiceNo());
				json.put("NET_AMOUNT", outWardOrderInvoiceEnteryRegistrationObj.getNetAmount());
				json.put("ORDER_NO", outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getOrderNo());
				json.put("ORDER_ID", outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getOrderId());
				json.put("FROM_DATE", outWardOrderInvoiceEnteryRegistrationObj.getFirstDate());
				json.put("TO_DATE", outWardOrderInvoiceEnteryRegistrationObj.getLastDate());
				json.put("INVOICE_AMOUNT", outWardOrderInvoiceEnteryRegistrationObj.getNetAmount());
				json.put("REMARK", outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getRemark());
				json.put("ORDER_MODE", outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getOrderMode().getOrderModeName());
			
				json_ImageArray=new JSONArray();
				for(OutWardOrderEntryRegistrationOrderCopyPath tempObj :  outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getOutWardOrderEntryRegistrationOrderCopyPathList()){
					tempJson = new JSONObject();
					tempJson.put("IMAGE_PATH", tempObj.getImagePath());
					tempJson.put("IMAGE_ID", tempObj.getOutWardOrderEntryRegistrationOrderCopyPathId());
					json_ImageArray.put(tempJson);
				}
				
				json.put("ORDER_COPY", json_ImageArray);
				
				json.put("GROSS_AMOUNT", outWardOrderInvoiceEnteryRegistrationObj.getGrossAmount());
				json.put("DISCOUNT", outWardOrderInvoiceEnteryRegistrationObj.getDiscountIsTakenOrNot());
				json.put("INVOICE_DATE", outWardOrderInvoiceEnteryRegistrationObj.getInvoiceDate());
				json.put("NO_OF_DAYS", outWardOrderInvoiceEnteryRegistrationObj.getNoOfDays());
				json.put("TAX_AMOUNT", outWardOrderInvoiceEnteryRegistrationObj.getTaxAmout());
				json.put("VAT_AMOUNT", outWardOrderInvoiceEnteryRegistrationObj.getVatAmount());
				
				
				DecimalFormat df = new DecimalFormat("#.##");
				json.put("PAID_AMOUNT",Double.valueOf(df.format(netAmount-remainingAmount)));
				json.put("REMAINING_AMOUNT",Double.valueOf(df.format(outWardOrderInvoiceEnteryRegistrationObj.getRemainingPayment())));
				json.put("COMP_NAME", outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
				json.put("ORG_NAME", outWardOrderInvoiceEnteryRegistrationObj.getOrganization().getOrganizationName());
				json.put("STOCKIST", outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
				json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(outWardOrderInvoiceEnteryRegistrationObj.getSubmitDate().toString()));
				json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/updatetChequeNumberInfo",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updatetChequeNumberInfo(@RequestParam(value="ChequeNumberInfoId",required=true) Integer ChequeNumberInfoId,
			@RequestParam(value="ChequeNumber",required=true) String ChequeNumber,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			// load ChequeNumberInfo obj using ChequeNumberInfoId
			ChequeNumberInfo chequeNumberInfoObj=bankService.loadChequeNumberInfoObjUsignChequeNumberInfoId(ChequeNumberInfoId);
			if(chequeNumberInfoObj!=null)
			{
				chequeNumberInfoObj.setChequeNumber(ChequeNumber);
				if(bankService.updateChequeNumberInfoObj(chequeNumberInfoObj))
				{
					json=new JSONObject();
					json.put("CHEQUE_NO_INFO_ID", ChequeNumberInfoId);
					json.put("MSG","Cheque Number Updated Successfully");
					json.put("CHEQUE_NO", ChequeNumber);
					json_data_array.put(json);
				}
				else
				{
					json=new JSONObject();
					json.put("CHEQUE_NO_INFO_ID", ChequeNumberInfoId);
					json.put("CHEQUE_NO", ChequeNumber);
					json.put("MSG","Operation Failed ");
					json_data_array.put(json);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/editStockistAdvanceCheck",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String editStockistAdvanceCheck(@RequestParam(value="AdvanceChequeid",required=true) Integer AdvanceChequeid,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONArray json_checque_array= new JSONArray();
		JSONObject json=null;
		JSONObject jsonCheque = null;
		try 
		{
			AdvancedChequeInformation advancedChequeInformationObj=bankService.loadAdvancedChequeInformationObjUsignAdvanceChequeId(AdvanceChequeid);
			if(advancedChequeInformationObj!=null)
			{
				json=new JSONObject();
				json.put("ADVANCE_CHEQUE_INFO_ID", advancedChequeInformationObj.getAdvancedChequeInformationId());
				json.put("ORG_NAME", advancedChequeInformationObj.getOrganization().getOrganizationName());
				json.put("COMP_NAME", advancedChequeInformationObj.getCompany().getCompanyName());
				json.put("ORG_ID", advancedChequeInformationObj.getOrganization().getOrganizationId());
				json.put("COMPANY_ID",advancedChequeInformationObj.getCompany().getCompanyId());
				
				if(advancedChequeInformationObj.getStockist() !=null)
				{
				json.put("STOCKIST_STATUS", "Yes");
				json.put("STOCKIST", advancedChequeInformationObj.getStockist().getStockistName());
				json.put("STOCKIST_ID", advancedChequeInformationObj.getStockist().getStockistId());
				json.put("STOCKIST_BANK", advancedChequeInformationObj.getStockistBankDetails().getStockistBankName());
				json.put("STOCKIST_BANK_ID", advancedChequeInformationObj.getStockistBankDetails().getStockistBankDetailsId());
				}
				else
				{
					json.put("STOCKIST_STATUS", "No");
					json.put("STOCKIST","-");
				}
				
				if(advancedChequeInformationObj.getCompanyBank()!=null)
				{
				json.put("COMPANY_BANK_STATUS", "Yes");
				json.put("COMPANY_BANK_ID", advancedChequeInformationObj.getCompanyBank().getCompanyBankId());
				json.put("COMPANY_BANK", advancedChequeInformationObj.getCompanyBank().getCompanyBankName());
				}
				else
				{
					json.put("COMPANY_BANK", "-");
					json.put("COMPANY_BANK_STATUS", "No");
				}
				
				String [] reg_date_arr = advancedChequeInformationObj.getCourierReceivedDate().toString().split("-");
                json.put("DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
				
				List<ChequeNumberInfo> chequeNumberInfoList = bankService.loadChequeNumberInfoObjUsignAdvanceChequeId(AdvanceChequeid);
				if(chequeNumberInfoList.size() > 0)
				{
					for(ChequeNumberInfo obj : chequeNumberInfoList)
					{
						jsonCheque = new JSONObject();
						jsonCheque.put("CHEQUE_NO", obj.getChequeNumber());
						jsonCheque.put("CHEQUE_NO_INFO_ID",obj.getChequeNumberInfoId());
						jsonCheque.put("DEPOSIT_STATUS", obj.getDepositStatus());
						json_checque_array.put(jsonCheque);
					}
				}
				json.put("CHEQUE_ARRAY", json_checque_array);
				json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/updateStockistAdvanceCheck",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateStockistAdvanceCheck(@RequestParam(value="advancedChequeInformationId",required=true) Integer advancedChequeInformationId,
			@RequestParam(value="advanceChequeDate",required=true) String advanceChequeDate)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			// load advancedChequeInformation obj usign advancedChequeInformationId
			AdvancedChequeInformation advancedChequeInformationObj=bankService.loadAdvancedChequeInformationObjUsignAdvanceChequeId(advancedChequeInformationId);
			DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date d1 = dtf.parse(advanceChequeDate);
			advancedChequeInformationObj.setCourierReceivedDate(new java.sql.Date(d1.getTime()));
			
			//update advancedChequeInformation obj
			if(bankService.updateAdvancedChequeInformationObj(advancedChequeInformationObj))
			{
				json=new JSONObject();
				json.put("MSG", "Advance Cheque Information updated successfully.......!!");
				json_data_array.put(json);
			}
			else
			{
				json=new JSONObject();
				json.put("MSG", "Operation failed........!!");
				json_data_array.put(json);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	
	@RequestMapping(value="/searchStockistAdvancedCheque",method=RequestMethod.POST)
	private  @ResponseBody String searchStockistAdvancedCheque(
			@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request)
	
	{
		JSONObject json = null;
		java.util.Map<Object, Object>map = null;
		JSONArray advanceCheckArr = new JSONArray();
		Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
		try{
			
			
			map= bankService.searchAdvancedChequeInformation(searchOption,searchText,organizationId,from);
			
			List<AdvancedChequeInformation> advancedChequeInformationList = (List<AdvancedChequeInformation>) map.get("advancedChequeInformationList");
			AdvancedChequeInformation advancedChequeInformationObj=null;
			Iterator<AdvancedChequeInformation> advanceCheckItr  = advancedChequeInformationList.iterator();
			Integer totalPage = (Integer) map.get("totalPages");
			while(advanceCheckItr.hasNext())
			{
				advancedChequeInformationObj=advanceCheckItr.next();
				json=new JSONObject();
				json.put("ADV_CHEQUE_INFORMATION_ID", advancedChequeInformationObj.getAdvancedChequeInformationId());
				json.put("ORG", advancedChequeInformationObj.getOrganization().getOrganizationName());
				json.put("COMPNAY", advancedChequeInformationObj.getCompany().getCompanyName());
				json.put("ORG_ID", advancedChequeInformationObj.getOrganization().getOrganizationId());
				json.put("COMPANY_ID",advancedChequeInformationObj.getCompany().getCompanyId());
				json.put("NO_OF_CHEQUE",advancedChequeInformationObj.getNoOfCheque());
				if(advancedChequeInformationObj.getStockist() !=null)
				{
				json.put("STOCKIST", advancedChequeInformationObj.getStockist().getStockistName());
				json.put("STOCKIST_BANK", advancedChequeInformationObj.getStockistBankDetails().getStockistBankName());
				}
				else
				{
					json.put("STOCKIST","-");
					json.put("STOCKIST_BANK", "-");
				}
				if(advancedChequeInformationObj.getCompanyBank()!=null)
				{
				json.put("COMPANY_BANK", advancedChequeInformationObj.getCompanyBank().getCompanyBankName());
				}
				else
				{
					json.put("COMPANY_BANK", "-");
				}
                advanceCheckArr.put(json);
		}
			json=new JSONObject();
			json.put("paginationCount", totalPage);
            advanceCheckArr.put(json);
	}catch(JSONException e){
		log.error(e);e.printStackTrace();
	}
	catch(Exception e){
		log.error(e);e.printStackTrace();
	}
	return advanceCheckArr.toString();
}
	
	
	@RequestMapping(value="/deleteCheque",method=RequestMethod.POST)
	private  @ResponseBody String deleteCheque(@RequestParam(value="CHEQUE_NO_INFO_ID",required=true) Integer CHEQUE_NO_INFO_ID)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=new JSONObject();
		try 
		{
			json.put("MSG", "Operation failed........!!");
			// load cheque Number info obj using CHEQUE_NO_INFO_ID
			ChequeNumberInfo chequeNumberInfoObj=bankService.loadChequeNumberInfoObjUsignChequeNumberInfoId(CHEQUE_NO_INFO_ID);
			if(chequeNumberInfoObj!=null)
			{
				// load No Of Cheque Using Advanced Cheque Information Id
				Integer noOfCheque=bankService.returnNoOfChequeUsingAdvancedChequeInformationId(chequeNumberInfoObj.getAdvancedChequeInformation().getAdvancedChequeInformationId());
				if(noOfCheque>1)
				{
					// delete only cheque no entry
					System.out.println("delete only cheque no entry");
					if(bankService.deleteChequeNumberInfoObj(chequeNumberInfoObj))
					{
						AdvancedChequeInformation advancedChequeInformationObj=chequeNumberInfoObj.getAdvancedChequeInformation();
						Integer no_Of_Cheque=advancedChequeInformationObj.getNoOfCheque()-1;
						advancedChequeInformationObj.setNoOfCheque(no_Of_Cheque);
						if(bankService.updateAdvancedChequeInformationObj(advancedChequeInformationObj))
						{
							json.put("MSG", "Advance cheque deleted successfully");
							json.put("CHEQUE_NO_INFO_ID", CHEQUE_NO_INFO_ID);
							json.put("AdvancedChequeInformationId",chequeNumberInfoObj.getAdvancedChequeInformation().getAdvancedChequeInformationId());
							json.put("NO_OF_CHEQUE", no_Of_Cheque);
							json_data_array.put(json);
						}
					}
				}
				else
				{
					// delete cheque no entry and advanced cheque information entry
					System.out.println("delete cheque no entry and advanced cheque information entry");
					if(bankService.deleteChequeNumberInfoObj(chequeNumberInfoObj))
					{
						//delete advance cheque information object
						if(bankService.deleteAdvancedChequeInformationObj(chequeNumberInfoObj.getAdvancedChequeInformation()))
							{
							json.put("CRITERIA","DELETE_STOCKIST_ADV_CHEQUE_ENTRY");
							json.put("MSG", "Advance cheque deleted successfully");
							json.put("CHEQUE_NO_INFO_ID", CHEQUE_NO_INFO_ID);
							json.put("AdvancedChequeInformationId",chequeNumberInfoObj.getAdvancedChequeInformation().getAdvancedChequeInformationId());
							json_data_array.put(json);
							}
					}
				}
			}	
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/searchStockistDepositedChequeEntries",method=RequestMethod.POST)
	private  @ResponseBody String searchStockistDepositedChequeEntries(
			        @RequestParam(value="selectedValue",required=false) String selectedValue ,
					@RequestParam(value="searchText",required=false) String searchText ,
					@RequestParam(value="fromSpecialData",required=false) String fromSpecialData ,
					@RequestParam(value="toSpecialData",required=false) String toSpecialData,
					@RequestParam(value="searchOption",required=false) String searchOption,
					@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		JSONObject json = null;
		JSONArray stockistChequeDepositArr = new JSONArray();
		java.util.Map<Object, Object>map = null;
		Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
		try{
			
			map = bankService.searchStockistDepositedChequeEntries(selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
			List<StockistChequeDeposit> stockistChequeDepositList = (List<StockistChequeDeposit>) map.get("empList");
			StockistChequeDeposit stockistChequeDepositObj=null;
			Integer totalPage = (Integer) map.get("totalPages");
			Iterator<StockistChequeDeposit> stockistChequeDepositItr  = stockistChequeDepositList.iterator();
			while(stockistChequeDepositItr.hasNext())
			{
				stockistChequeDepositObj=stockistChequeDepositItr.next();
				json=new JSONObject();
				json.put("STOCKIST_CHEQUE_DEPOSIT_ID", stockistChequeDepositObj.getStockistChequeDepositId());
				json.put("ORG", stockistChequeDepositObj.getOrganization().getOrganizationName());
				json.put("COMPANY", stockistChequeDepositObj.getCompany().getCompanyName());
				json.put("ORG_ID", stockistChequeDepositObj.getOrganization().getOrganizationId());
				json.put("CHEQUE_AMOUNT",stockistChequeDepositObj.getChequeAmount());
				json.put("COMPANY_ID",stockistChequeDepositObj.getCompany().getCompanyId());
				json.put("CHEQUE_NUMBER",stockistChequeDepositObj.getChequeNumberInfo().getChequeNumber());
				if(stockistChequeDepositObj.getStockist() !=null)
				{
				json.put("STOCKIST", stockistChequeDepositObj.getStockist().getStockistName());
				json.put("STOCKIST_BANK", stockistChequeDepositObj.getStockistBankDetails().getStockistBankName());
				}
				else
				{
					json.put("STOCKIST","-");
					json.put("STOCKIST_BANK", "-");
				}
				if(stockistChequeDepositObj.getCompanyBank()!=null)
				{
				json.put("COMPANY_BANK", stockistChequeDepositObj.getCompanyBank().getCompanyBankName());
				}
				else
				{
					json.put("COMPANY_BANK", "-");
				}
				stockistChequeDepositArr.put(json);
		}
			json=new JSONObject();
			json.put("paginationCount", totalPage);
			stockistChequeDepositArr.put(json);
	}catch(JSONException e){
		log.error(e);e.printStackTrace();
	}
	catch(Exception e){
		log.error(e);e.printStackTrace();
	}
	return stockistChequeDepositArr.toString();
}
	
	//
	@RequestMapping(value="/searchOutstandingChequeDepositEntry",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String searchOutstandingChequeDepositEntry(@RequestParam(value="searchOption",required=true) String selectedValue ,
			@RequestParam(value="searchText",required=true) String searchText ,
			@RequestParam(value="fromSpecialData",required=true) String fromSpecialData ,
			@RequestParam(value="toSpecialData",required=true) String toSpecialData,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		
		JSONObject json = null;
		JSONArray outwardOrdInvEntryArr = null;
		java.util.Map<Object, Object>map = null;
		
//		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEntryRegistration = null;
		try{
			outwardOrdInvEntryArr = new JSONArray();
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			java.sql.Date ourJavaDateObject = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			
			map = bankService.searchOutstandingTodaysDue(ourJavaDateObject,selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEntryRegistration = (List<OutWardOrderInvoiceEnteryRegistration>) map.get("OutWardOrderInvoiceEntery");
			Integer totalPage = (Integer) map.get("totalPages");
			for(OutWardOrderInvoiceEnteryRegistration obj :outWardOrderInvoiceEntryRegistration)
			{

				json=new JSONObject();
				json.put("ORDER_ID", obj.getOutWardOrderEnteryRegistration().getOrderId());
				json.put("ORDER_NO", obj.getOutWardOrderEnteryRegistration().getOrderNo());
				json.put("ORG", obj.getOrganization().getOrganizationName());
				json.put("COMPANY", obj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
				json.put("STOCKIST", obj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
				json.put("INVOICE_NO", obj.getInvoiceNo());
				json.put("INVOICE_AMOUNT", obj.getNetAmount());
				DecimalFormat df = new DecimalFormat("#.##");      
				json.put("REMAINING_AMOUNT",Double.valueOf(df.format(obj.getRemainingPayment())));
				json.put("OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID",obj.getOutWardOrderInvoiceEnteryRegistrationId());
			
					outwardOrdInvEntryArr.put(json);
				}
			json=new JSONObject();
			json.put("paginationCount", totalPage);
			outwardOrdInvEntryArr.put(json);
		}catch(Exception e){
			log.error(e);
		}
		return outwardOrdInvEntryArr.toString();
	}
	
	@RequestMapping(value="/searchOutstandingChequeDepositEntryLaps",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String searchOutstandingChequeDepositEntryLaps(@RequestParam(value="searchOption",required=true) String selectedValue ,
			@RequestParam(value="searchText",required=true) String searchText ,
			@RequestParam(value="fromSpecialData",required=true) String fromSpecialData ,
			@RequestParam(value="toSpecialData",required=true) String toSpecialData,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		
		JSONObject json = null;
		java.util.Map<Object, Object>map = null;
		JSONArray outwardOrdInvEntryArr = null;
		
//		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEntryRegistration = null;
		try{
			outwardOrdInvEntryArr = new JSONArray();
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			java.sql.Date ourJavaDateObject = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			
			map = bankService.searchOutstandingLapsed(ourJavaDateObject,selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEntryRegistration = (List<OutWardOrderInvoiceEnteryRegistration>) map.get("laps");
			Integer totalPage = (Integer) map.get("totalPages");
			for(OutWardOrderInvoiceEnteryRegistration obj :outWardOrderInvoiceEntryRegistration)
			{

				json=new JSONObject();
				json.put("ORDER_ID", obj.getOutWardOrderEnteryRegistration().getOrderId());
				json.put("ORDER_NO", obj.getOutWardOrderEnteryRegistration().getOrderNo());
				json.put("ORG", obj.getOrganization().getOrganizationName());
				json.put("COMPANY", obj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
				json.put("STOCKIST", obj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
				json.put("INVOICE_NO", obj.getInvoiceNo());
				json.put("INVOICE_AMOUNT", obj.getNetAmount());
				DecimalFormat df = new DecimalFormat("#.##");      
				json.put("REMAINING_AMOUNT",Double.valueOf(df.format(obj.getRemainingPayment())));
				json.put("OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID",obj.getOutWardOrderInvoiceEnteryRegistrationId());
			
					outwardOrdInvEntryArr.put(json);
				}
			json=new JSONObject();
			json.put("paginationCount", totalPage);
			outwardOrdInvEntryArr.put(json);
		}catch(Exception e){
			log.error(e);
		}
		return outwardOrdInvEntryArr.toString();
	}
	
	

	@RequestMapping(value="/searchOutstandingChequeDepositEntryUpcoming",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String searchOutstandingChequeDepositEntryUpcoming(@RequestParam(value="searchOption",required=true) String selectedValue ,
			@RequestParam(value="searchText",required=true) String searchText ,
			@RequestParam(value="fromSpecialData",required=true) String fromSpecialData ,
			@RequestParam(value="toSpecialData",required=true) String toSpecialData,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		
		JSONObject json = null;
		JSONArray outwardOrdInvEntryArr = null;
		java.util.Map<Object, Object>map = null;
//		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEntryRegistration = null;
		try{
			outwardOrdInvEntryArr = new JSONArray();
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			java.sql.Date ourJavaDateObject = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			
			map =  bankService.searchOutstandingUpcoming(ourJavaDateObject,selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEntryRegistration = (List<OutWardOrderInvoiceEnteryRegistration>) map.get("upcoming");
			Integer totalPage = (Integer) map.get("totalPages");
			for(OutWardOrderInvoiceEnteryRegistration obj :outWardOrderInvoiceEntryRegistration)
			{

				json=new JSONObject();
				json.put("ORDER_ID", obj.getOutWardOrderEnteryRegistration().getOrderId());
				json.put("ORDER_NO", obj.getOutWardOrderEnteryRegistration().getOrderNo());
				json.put("ORG", obj.getOrganization().getOrganizationName());
				json.put("COMPANY", obj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
				json.put("STOCKIST", obj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
				json.put("INVOICE_NO", obj.getInvoiceNo());
				json.put("INVOICE_AMOUNT", obj.getNetAmount());
				DecimalFormat df = new DecimalFormat("#.##");      
				json.put("REMAINING_AMOUNT",Double.valueOf(df.format(obj.getRemainingPayment())));
				json.put("OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID",obj.getOutWardOrderInvoiceEnteryRegistrationId());
			
					outwardOrdInvEntryArr.put(json);
				}
			json=new JSONObject();
			json.put("paginationCount", totalPage);
			outwardOrdInvEntryArr.put(json);

			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return outwardOrdInvEntryArr.toString();
	}

	//
	@RequestMapping(value="/searchOutstandingChequeDepositEntryPaidInvoice",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String searchOutstandingChequeDepositEntryPaidInvoice(@RequestParam(value="searchOption",required=true) String selectedValue ,
			@RequestParam(value="searchText",required=true) String searchText ,
			@RequestParam(value="fromSpecialData",required=true) String fromSpecialData ,
			@RequestParam(value="toSpecialData",required=true) String toSpecialData,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		
		JSONObject json = null;
		JSONArray outwardOrdInvEntryArr = null;
		java.util.Map<Object, Object>map = null;
//		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEntryRegistrationList = null;
		try{
			outwardOrdInvEntryArr = new JSONArray();
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			java.sql.Date ourJavaDateObject = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			
			map = bankService.searchOutstandingPaidInvoice(ourJavaDateObject,selectedValue,searchText,fromSpecialData,toSpecialData,organizationId,from);
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEntryRegistrationList = (List<OutWardOrderInvoiceEnteryRegistration>) map.get("paidInvoice");
			Integer totalPage = (Integer) map.get("totalPages");
			if(outWardOrderInvoiceEntryRegistrationList.size()>0)
			{
				for(OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEntryRegistrationList)
				{
					Double netAmount=obj.getNetAmount();
					Double remainingAmount=obj.getRemainingPayment();
					
//					if(!netAmount.equals(remainingAmount))
//					{
						json=new JSONObject();
						json.put("ORDER_ID", obj.getOutWardOrderEnteryRegistration().getOrderId());
						json.put("ORDER_NO", obj.getOutWardOrderEnteryRegistration().getOrderNo());
						json.put("ORG", obj.getOrganization().getOrganizationName());
						json.put("COMPANY", obj.getOutWardOrderEnteryRegistration().getCompany().getCompanyName());
						json.put("STOCKIST", obj.getOutWardOrderEnteryRegistration().getStockist().getStockistName());
						json.put("INVOICE_NO", obj.getInvoiceNo());
						json.put("INVOICE_AMOUNT", obj.getNetAmount());
						DecimalFormat df = new DecimalFormat("#.##");
						json.put("PAID_AMOUNT",Double.valueOf(df.format(netAmount-remainingAmount)));
						json.put("REMAINING_AMOUNT",Double.valueOf(df.format(obj.getRemainingPayment())));
						json.put("OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID",obj.getOutWardOrderInvoiceEnteryRegistrationId());
						outwardOrdInvEntryArr.put(json);
//					}
				}
			}
			
			json=new JSONObject();
			json.put("paginationCount", totalPage);
			outwardOrdInvEntryArr.put(json);
		}catch(Exception e){
			log.error(e);
		}
		return outwardOrdInvEntryArr.toString();
	}

}

//


