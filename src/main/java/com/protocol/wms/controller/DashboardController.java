package com.protocol.wms.controller;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.protocol.wms.service.DashboardService;

@Controller
@RequestMapping(value="/DashboardController")
public class DashboardController {
	
	@Autowired
	private HttpSession session;
	
	@Autowired
	@Qualifier("DashboardServiceImpl")
	private DashboardService dashboardService;
	
	@RequestMapping(value="/loadDashboardTable",method=RequestMethod.POST)
	private  @ResponseBody String loadDashboardTable(
			@RequestParam(value="organizations",required=false) Integer[] organizations,
			@RequestParam(value="companies",required=false) Integer[] companies,
			@RequestParam(value="stockists",required=false) Integer[] stockists,
			@RequestParam(value="year",required=false) Integer year ){
		
		Integer organizationId = (Integer) session.getAttribute("ORGANIZATION_ID");
		return  dashboardService.loadDashboardTable(organizations, companies, stockists, year,organizationId).toString();
	}

}
