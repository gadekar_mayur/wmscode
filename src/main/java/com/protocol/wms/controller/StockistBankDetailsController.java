package com.protocol.wms.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protocol.wms.model.StockistBankDetails;
import com.protocol.wms.service.StockistBankDetailsService;

@Controller
@RequestMapping(value="/StockistBankDetailsController")
public class StockistBankDetailsController {

	private Logger log = Logger.getLogger(StockistBankDetailsController.class.getClass());
	@Autowired
	private HttpSession session;
	@Autowired
	@Qualifier("StockistBankDetailsServiceImpl")
	private StockistBankDetailsService stockistBankDetailsService;
	
	@RequestMapping(value="/addStockistBankDetailsForm",method=RequestMethod.POST)
	private  @ResponseBody String displayStockistBankDetailsForm(){
		
		return stockistBankDetailsService.displayStockistBankDetailsForm().toString();
	}
	
	//Add Stockist Bank Details
	@RequestMapping(value="/addStockistBankDetails",method=RequestMethod.POST)
	private  @ResponseBody String saveStockistBankDetails(
			@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="stockistName",required=false) Integer stockistId,
			@RequestParam(value="branch",required=false) String branch,@RequestParam(value="ifsc",required=false) String ifsc,
			@RequestParam(value="companyName",required=false) Integer companyId,@RequestParam(value="bankName",required=false) String bankName,
			@RequestParam(value="accNo",required=false) String accNo){
		StockistBankDetails bankDetatils = null;
		try{
			bankDetatils = new StockistBankDetails();
			bankDetatils.setStatus(1);
			bankDetatils.setStockistBankBranchName(branch);
			bankDetatils.setStockistBankIfscCode(ifsc);
			bankDetatils.setStockistBankName(bankName);
			bankDetatils.setStockistBankaccountNo(accNo);
		}catch(Exception e){
			
		}
		return stockistBankDetailsService.saveStockistBankDetails(orgId,stockistId,companyId,bankDetatils).toString();
	}
	
	@RequestMapping(value="/updateStockistBankDetails",method=RequestMethod.POST)
	private  @ResponseBody String updateStockistBankDetails(@RequestParam(value="stockistBankId",required=true) Integer stockistBankId,
			@RequestParam(value="bankName",required=true) String bankName ,@RequestParam(value="accountNo",required=true) String accNo, 
			@RequestParam(value="branchName",required=true) String branch,@RequestParam(value="ifscCode",required=true) String ifsc){
		StockistBankDetails stockistBankDetatils = null;
		JSONObject result =null;
		try{
			stockistBankDetatils = stockistBankDetailsService.loadStockistBankDetailsUsignStockistBankDetailsId(stockistBankId);
			stockistBankDetatils.setStockistBankName(bankName);;
			stockistBankDetatils.setStockistBankaccountNo(accNo);
			stockistBankDetatils.setStockistBankBranchName(branch);
			stockistBankDetatils.setStockistBankIfscCode(ifsc);
			result = new JSONObject();
			result.put("MSG","Company Bank Details updation failed....!");
			if(stockistBankDetailsService.updateStockistBankDetails(stockistBankDetatils)){
				result.put("MSG","Stockist Bank Details updated successfully....!");
				result.put("RESULT",true);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	@RequestMapping(value="/stockistBankDetailsList",method=RequestMethod.POST)
	private  @ResponseBody String stockistBankDetailsList(){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		return stockistBankDetailsService.stockistBankDetailsList(orgId).toString();
	}

	@RequestMapping(value="/searchStokistBankDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchStokistBankDetails(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=false) Integer from){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		return stockistBankDetailsService.searchStokistBankDetails(searchOption,searchText,orgId,from).toString();
	}
	@RequestMapping(value="/stockistBankDropdownList",method=RequestMethod.POST)
	private  @ResponseBody String stockistBankDropdownList(@RequestParam(value="orgId",required=false) Integer orgId,
			@RequestParam(value="companyId",required=false) Integer companyId,@RequestParam(value="stockistId",required=false) Integer stockistId){
		List<StockistBankDetails> stockistBankList = null;
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try{
			stockistBankList = stockistBankDetailsService.stockistBankDropdownList(orgId,companyId,stockistId);
			for(StockistBankDetails  obj : stockistBankList)
			{
				json=new JSONObject();
				json.put("BANK_ID", obj.getStockistBankDetailsId());
				json.put("BANK_NAME", obj.getStockistBankName());
				json_data_array.put(json);
			}
			json = new JSONObject();
			json.put("bankArray", json_data_array);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return json.toString();
	}
	//Delete Stockist Bank Details
	@RequestMapping(value="/deleteStockistBankDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteStockistBankDetails(@RequestParam(value="stockistBankId",required=false) Integer stockistBankId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(stockistBankDetailsService.deleteStockistBankDetails(stockistBankId)){
				result.put("MSG","Stockist Bank Details Deleted Successfully");
			}else{
				result.put("MSG","Stockist Bank Details Deletion Failed");
			}
		}
		catch(Exception e)
		{
			try
			{

				e.printStackTrace();
				log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}
			catch(Exception e1)
			{
				e.printStackTrace();
				log.error(e1);
			}
		}
		
		return result.toString();
	}
}
