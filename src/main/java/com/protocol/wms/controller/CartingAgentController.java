/**
 * 
 */
package com.protocol.wms.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentImagePath;
import com.protocol.wms.model.CartingAgentTripCount;
import com.protocol.wms.model.CartingAgentUploadedDocument;
import com.protocol.wms.model.CartingAgentUploadedDocumentImagePath;
import com.protocol.wms.model.CartingAgentVehiclePhotoImagePath;
import com.protocol.wms.model.EmployeeDetailsImagePath;
import com.protocol.wms.model.EmployeeDocumentImagePath;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OutWardCourierRegistration;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.service.CartingAgentService;
import com.protocol.wms.service.UserDetailsService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/CartingAgentController")
public class CartingAgentController {
	private Logger log = Logger.getLogger(CartingAgentController.class.getClass());
	@Autowired
	private HttpSession session;
	
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	private UserDetailsService userDetailsService;
	
	//OutWardGoodsService
	
//	@Autowired
//	@Qualifier("OutWardGoodsServiceImpl")
//	private OutWardGoodsService outWardGoodsService;
		
	@Autowired
	@Qualifier("CartingAgentServiceImpl")
	private CartingAgentService cartingAgentService;
	
	@RequestMapping(value="/saveCartingAgentDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveCartingAgentDetails(
			@RequestParam(value="orgName",required=true) Integer organizationId,
			@RequestParam(value="agentName",required=true) String agentName,@RequestParam(value="mobile",required=true) Long mobile,
			@RequestParam(value="agentPhoto",required=true) MultipartFile agentPhoto[],@RequestParam(value="address",required=true) String address,
			@RequestParam(value="vehicleNo",required=true) String vehicleNo,@RequestParam(value="vehicleModel",required=true) String vehicleModel,
			@RequestParam(value="vehiclePhoto",required=true) MultipartFile vehiclePhoto[],
			@RequestParam(value="username",required=true) String userName,@RequestParam(value="password",required=true) String password)
	{
		JSONObject result = null;
		CartingAgent cartingAgent =null;
		UserDetails userDetails=null;
		List<CartingAgentImagePath> cartingAgentImagePathList = null;
		CartingAgentImagePath tempCartingAgentImagePath = null;
		List<CartingAgentVehiclePhotoImagePath> vehiclePhotoImagePathList = null;
		CartingAgentVehiclePhotoImagePath tempVehiclePhotoImagePath = null;
		try{
			/*if(!(agentPhoto.length>0) && !(vehiclePhoto.length>0)){
				result = new JSONObject();
				result.put("MSG","Please select atleast one file for both Agent Photo and Vehicle Photo...!");
				return result.toString();
			}*/
			userDetails = userDetailsService.checkUserNameAlreadyExitOrNot(userName);
			if(userDetails!=null){
				result = new JSONObject();
				result.put("MSG","User Name already exists");
				result.put("FLAG", true);
				return result.toString();
			}
			cartingAgent = new CartingAgent();
			
			cartingAgentImagePathList = new ArrayList<CartingAgentImagePath>();
			for(MultipartFile file : agentPhoto){
				tempCartingAgentImagePath = new CartingAgentImagePath();
				tempCartingAgentImagePath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
				cartingAgentImagePathList.add(tempCartingAgentImagePath);
			}
			cartingAgent.setCartingAgentImagePathList(cartingAgentImagePathList);
			vehiclePhotoImagePathList = new ArrayList<CartingAgentVehiclePhotoImagePath>();
			for(MultipartFile file : vehiclePhoto){
				tempVehiclePhotoImagePath = new CartingAgentVehiclePhotoImagePath();
				tempVehiclePhotoImagePath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
				vehiclePhotoImagePathList.add(tempVehiclePhotoImagePath);
			}
			cartingAgent.setCartingAgentVehiclePhotoImagePathList(vehiclePhotoImagePathList);
			cartingAgent.setAddress(address);
			cartingAgent.setModel(vehicleModel);
			cartingAgent.setName(agentName);
			cartingAgent.setVehicleNumber(vehicleNo);
			cartingAgent.setStatus(1);
			cartingAgent.setMobileNo(String.valueOf(mobile));
			//set organization submit date
			Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    Date date = df2.parse(IST);
		    cartingAgent.setSubmitDate(date);
		    
		    if(!"".equals(userName.trim())&&!"".equals(password.trim())&&userName!=null&&password!=null){
				userDetails = new UserDetails();
				userDetails.setUserName(userName);
				userDetails.setPassword(password);
			}
		    result = new JSONObject();
		    if(cartingAgentService.saveCartingAgentDetails(organizationId,cartingAgent,userDetails)){
		    	result.put("result", true);
		    	result.put("MSG","Add Catering Agent Registration Successful....!");
		    }else{
		    	result.put("result", false);
		    	result.put("MSG","Add Catering Agent Registration Failed....!");
		    }
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
			result = new JSONObject();
			try {
				result.put("MSG","Operation Failed....!");
			} catch (JSONException e1) {
				e1.printStackTrace();
				log.error(e1);
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/cartingAgentListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String cartingAgentListing()
	{
		JSONObject tempJson = null;
		JSONArray cartingAgentArray = null;
		List<CartingAgent> cartingAgentList =null;
		try{
			Integer organizationId = (Integer) session.getAttribute("ORGANIZATION_ID");
			cartingAgentList = cartingAgentService.getCartingAgentList(organizationId);
			cartingAgentArray = new JSONArray();
			for(CartingAgent cartingAgentTemp:cartingAgentList){
				tempJson = new JSONObject();
				tempJson.put("ORG_NAME", cartingAgentTemp.getOrganization().getOrganizationName());
				//tempJson.put("TRAN_NAME", cartingAgentTemp.getTransporterDetails().getTransporterName());
				tempJson.put("AGENT_NAME", cartingAgentTemp.getName());
				tempJson.put("VEHICLE_NO", cartingAgentTemp.getVehicleNumber());
				tempJson.put("VEHICLE_MODEL", cartingAgentTemp.getModel());
				tempJson.put("MOBILE", cartingAgentTemp.getMobileNo());
				tempJson.put("AGENT_ID", cartingAgentTemp.getCartingAgentId());
				cartingAgentArray.put(tempJson);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return cartingAgentArray.toString();
	}
	
	
	@RequestMapping(value="/cartingAgentListingUsignOrgId",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String cartingAgentListingUsignOrgId(@RequestParam(value="organizationId",required=true) Integer organizationId)
	{
		JSONObject tempJson = null;
		JSONArray cartingAgentArray = new JSONArray();
		List<CartingAgent> cartingAgentList =null;
		try{
			cartingAgentList = cartingAgentService.getCartingAgentList(organizationId);
			for(CartingAgent cartingAgentTemp:cartingAgentList){
				tempJson = new JSONObject();
				tempJson.put("CARTING_AGENT_NAME", cartingAgentTemp.getName());
				tempJson.put("CARTING_AGENT_ID", cartingAgentTemp.getCartingAgentId());
				cartingAgentArray.put(tempJson);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return cartingAgentArray.toString();
	}
	
	@RequestMapping(value="/viewCartingAgentDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewCartingAgentDetails(@RequestParam(value="agentId",required=true) Integer agentId)
	{
//		JSONObject tempJson = null;
//		CartingAgent cartingAgent = null;
//		try{
//			cartingAgent = cartingAgentService.getCartingAgentDetailsById(agentId);
//			tempJson = new JSONObject();
//			if(cartingAgent!=null){
//				tempJson.put("ORG_NAME", cartingAgent.getOrganization().getOrganizationName());
//				//tempJson.put("TRAN_NAME", cartingAgent.getTransporterDetails().getTransporterName());
//				tempJson.put("AGENT_NAME", cartingAgent.getName());
//				tempJson.put("VEHICLE_NO", cartingAgent.getVehicleNumber());
//				tempJson.put("VEHICLE_MODEL", cartingAgent.getModel());
//				tempJson.put("MOBILE", cartingAgent.getMobileNo());
//				tempJson.put("AGENT_ID", cartingAgent.getCartingAgentId());
//				tempJson.put("AGENT_PHOTO", cartingAgent.getImagePath());
//				tempJson.put("VEHICLE_PHOTO", cartingAgent.getVehiclePhotoImagePath());
//				tempJson.put("ADDRESS", cartingAgent.getAddress());
//				//tempJson.put("TRANSPORTER", cartingAgent.getTransporterDetails().getTransporterName());
//			}
//			else{
//				tempJson.put("MSG", "Details Not Available ....!");
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			log.error(e);
//		}
		return cartingAgentService.getCartingAgentDetailsById(agentId);
	}
	//Delete Carting Agent Details
	@RequestMapping(value="/deleteCartingAgentDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteCartingAgentDetails(@RequestParam(value="agentId",required=false) Integer agentId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(cartingAgentService.deleteCartingAgentDetails(agentId)){
				result.put("MSG","Carting Agent Details  Deleted Successfully");
			}else{
				result.put("MSG","Carting Agent Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){e1.printStackTrace();log.error(e1);}
		}
		return result.toString();
	}
	//get Catering Agent DropDown By Org
	@RequestMapping(value="/getCateringAgentDropDownByOrg",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String getCateringAgentDropDownByOrg(@RequestParam(value="orgId",required=true) Integer organizationId)
	{
		JSONObject tempJson = null;
		JSONArray cartingAgentArray = null;
		List<CartingAgent> cartingAgentList =null;
		try{
			if(organizationId==null)
				organizationId=-1;
			cartingAgentList = cartingAgentService.getCartingAgentList(organizationId);
			cartingAgentArray = new JSONArray();
			for(CartingAgent cartingAgentTemp:cartingAgentList){
				tempJson = new JSONObject();
				tempJson.put("AGENT_NAME", cartingAgentTemp.getName());
				tempJson.put("AGENT_ID", cartingAgentTemp.getCartingAgentId());
				cartingAgentArray.put(tempJson);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return cartingAgentArray.toString();
	}
	
	//get Catering Agent DropDown By Org
	@RequestMapping(value="/cartingAgentUploadDocument",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String cartingAgentUploadDocument(
			@RequestParam(value="orgName",required=true) Integer organizationId,@RequestParam(value="cartingAgent",required=true) Integer cartingAgentId,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId,@RequestParam(value="uploadFile",required=true) MultipartFile uploadFile[])
	{
		JSONObject tempJson = null;
		CartingAgentUploadedDocument cartingAgentUploadedDocument =null;
		List<CartingAgentUploadedDocumentImagePath> cartingAgentUploadedDocumentImagePathList = null;
		CartingAgentUploadedDocumentImagePath tempImgPathObj = null;
		try{
			/*if(!(uploadFile.length>0)){
				 tempJson=new JSONObject();
				 tempJson.put("MSG", "Please Select at lest one file....!");
				 return tempJson.toString();
			}*/
			cartingAgentUploadedDocument = new CartingAgentUploadedDocument();
			cartingAgentUploadedDocument.setStatus(1);
			cartingAgentUploadedDocumentImagePathList = new ArrayList<CartingAgentUploadedDocumentImagePath>();
			for(MultipartFile file : uploadFile){
				tempImgPathObj = new CartingAgentUploadedDocumentImagePath();
				tempImgPathObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
				cartingAgentUploadedDocumentImagePathList.add(tempImgPathObj);
			}
			cartingAgentUploadedDocument.setCartingAgentUploadedDocumentImagePathList(cartingAgentUploadedDocumentImagePathList);
			//set organization submit date
			java.util.Date today = new java.util.Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date = df2.parse(IST);
		    cartingAgentUploadedDocument.setSubmitDate(date);
		    tempJson=new JSONObject();
			if(cartingAgentService.saveCartingAgentUploadDocument(organizationId,cartingAgentId,documentTypeId,cartingAgentUploadedDocument))
				tempJson.put("MSG", "Catering Agent Document Uploaded Successfully....!");
			else
				tempJson.put("MSG", "Catering Agent Document Upload Failed....!");
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
			tempJson=new JSONObject();
			try {
				tempJson.put("MSG", "Operation Failed....!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}
		}
		return tempJson.toString();
	}
	//get Catering Agent Document List
	@RequestMapping(value="/cartingAgentUploadedDocListing",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String cartingAgentUploadedDocListing()
	{
		//JSONObject tempJson = null;
		JSONArray cartingAgentDocArray = null;
		//List<CartingAgentUploadedDocument> docList =null;
		try{
			Integer organizationId = (Integer) session.getAttribute("ORGANIZATION_ID");
			cartingAgentDocArray = cartingAgentService.getCartingAgentUploadedDocList(organizationId);
			/*cartingAgentDocArray = new JSONArray();
			for(CartingAgentUploadedDocument tempObj:docList){
				tempJson = new JSONObject();
				tempJson.put("DOC_ID", tempObj.getCartingAgentUploadedDocumentId());
				tempJson.put("ORG_NAME", tempObj.getOrganization().getOrganizationName());
				tempJson.put("AGENT_NAME", tempObj.getCartingAgent().getName());
				tempJson.put("DOC_TYPE", tempObj.getDocumentListType().getDocumentName());
				tempJson.put("DOCUMENT", tempObj.getImagePath());
				cartingAgentDocArray.put(tempJson);
			}*/
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return cartingAgentDocArray.toString();
	}
	
	@RequestMapping(value="/updateCartingAgentDocumentInfo",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateCartingAgentDocumentInfo(@RequestParam(value="cartingAgentDocId",required=true) Integer cartingAgentDocId,
			@RequestParam(value="cartingAgent",required=true) Integer cartingAgentId,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId)
	{
		JSONObject result= null;
		try
		{
			result = cartingAgentService.updateCartingAgentDocumentInfo(documentTypeId,cartingAgentId,cartingAgentDocId);
		}
		catch (Exception e) 
		{
			result = new JSONObject();
			try {
				result.put("MSG", "Employee Uploaded Document Details Updation failed...!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	
	@RequestMapping(value="/updateCartingAgentUploadDocumentImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateCartingAgentUploadDocumentImage(
			@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
			@RequestParam(value="registrationId",required=true) Integer cartingAgentDocumentId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<CartingAgentUploadedDocumentImagePath> documentList = null;
		CartingAgentUploadedDocumentImagePath tempDocumentPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(documentImage!=null&&!(documentImage.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write  file
			documentList  = new ArrayList<CartingAgentUploadedDocumentImagePath>();
			for(MultipartFile file : documentImage){
				if(file!=null){
					tempDocumentPath = new CartingAgentUploadedDocumentImagePath();
					tempDocumentPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					documentList.add(tempDocumentPath);
				}
			}
			json_ImageArray = cartingAgentService.updateCartingAgentUploadDocumentImage(cartingAgentDocumentId, documentList);
			result.put("MSG", "Carting Agent Document Image File Upload successful...!");
			result.put("DOCUMENT", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/deleteCartingAgentDocumentFileImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteCartingAgentDocumentFileImage(@RequestParam(value="cartingAgentDocumentPathId",required=false) Integer cartingAgentDocumentPathId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(cartingAgentService.deleteCartingAgentDocumentFileImage(cartingAgentDocumentPathId)){
				result.put("RESULT",true);
				result.put("MSG","Carting Agent Upload Document Image Deleted Successfully");
			}else{
				result.put("MSG","Carting Agent Upload Document Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	//Delete Carting Agent Document Details
	@RequestMapping(value="/deleteCartingAgentDocumentDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteCartingAgentDocumentDetails(@RequestParam(value="docId",required=false) Integer docId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(cartingAgentService.deleteCartingAgentDocumentDetails(docId)){
				result.put("MSG","Carting Agent Uploaded Document Details Deleted Successfully");
			}else{
				result.put("MSG","Carting Agent Uploaded Document Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){e1.printStackTrace();log.error(e1);}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/searchCartingAgentsDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchCartingAgentsDetails(@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request)
	{
	
		int searchOptioinINT=Integer.parseInt(searchOption);
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			Map<String, Object> map = cartingAgentService.searchCartingAgentsDetails(searchOptioinINT,searchText,from,(Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
			List<CartingAgent>searchCartingAgentsList = (List<CartingAgent>)map.get("cartingAgentList");
			Integer totalPages = (Integer)map.get("totalPages");
			if(searchCartingAgentsList.size()>0)
			{
				for(CartingAgent obj : searchCartingAgentsList)
				{
					//if("1".equals(obj.getOrganization().getOrganizationStatus())){
					json=new JSONObject();
					json.put("AGENT_ID",obj.getCartingAgentId());
					json.put("ORG_NAME",obj.getOrganization().getOrganizationName());
					json.put("AGENT_NAME", obj.getName());
					json.put("VEHICLE_NO", obj.getVehicleNumber());
					json.put("VEHICLE_MODEL", obj.getModel());
					json.put("MOBILE",obj.getMobileNo());
					json_data_array.put(json);
					//}
				}
			}
			json=new JSONObject();
			json.put("paginationCount",totalPages);
			json_data_array.put(json);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/UpdateCartingAgentDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String UpdateCartingAgentDetails(
			@RequestParam(value="cartingAgentId",required=true) Integer cartingAgentId,
			@RequestParam(value="agentName",required=true) String agentName,
			@RequestParam(value="mobile",required=true) String mobile,
			@RequestParam(value="address",required=true) String address,
			@RequestParam(value="vehicleNo",required=true) String vehicleNo,
			@RequestParam(value="vehicleModel",required=true) String vehicleModel,
			@RequestParam(value="username",required=true) String userName,
			@RequestParam(value="password",required=true) String password)
	{
		JSONObject result = null;
		CartingAgent cartingAgent =null;
		UserDetails userDetails=null;
		Boolean insertNewRecordFlag = false;
		try{
			result = new JSONObject();
			cartingAgent = cartingAgentService.loadCartingAgentObjectUsingAgentId(cartingAgentId);
			userDetails = cartingAgent.getUserDetails();
			
			if(!"".equals(userName)&&!"".equals(password)&&userName!=null&&password!=null){
				if(!userDetails.getUserName().equals(userName)){
					UserDetails userDetailsTemp = userDetailsService.checkUserNameAlreadyExitOrNot(userName);
					if(userDetailsTemp!=null){
						result.put("MSG","User Name already exists");
						result.put("RESULT",false);
						return result.toString();
					}
				}
			}else{
				result.put("MSG","User Name and password are required....!");
				return result.toString();
			}
			
			userDetails.setPassword(password);
			
			if(!cartingAgent.getVehicleNumber().equalsIgnoreCase(vehicleNo)){
				insertNewRecordFlag = true;
			}
			
			if(insertNewRecordFlag == true){
				Date submitDate = cartingAgent.getSubmitDate();
				Organization organization = cartingAgent.getOrganization();
				CartingAgentTripCount cartingAgentTripCountObj = cartingAgent.getCartingAgentTripCount();
				Integer tripCount = null;
				if(cartingAgentTripCountObj!=null)
			       tripCount = cartingAgentTripCountObj.getTripCount();
			    cartingAgent = new CartingAgent();
			    cartingAgent.setOrganization(organization);
			    cartingAgent.setSubmitDate(submitDate);
			    cartingAgent.setStatus(1);
			    if(tripCount!=null){
				    cartingAgentTripCountObj = new CartingAgentTripCount();
				    cartingAgentTripCountObj.setOrganization(organization);
				    cartingAgentTripCountObj.setTripCount(tripCount); 
				    cartingAgent.setCartingAgentTripCount(cartingAgentTripCountObj);
			    }
			}
				
			cartingAgent.setAddress(address);
			cartingAgent.setModel(vehicleModel);
			cartingAgent.setName(agentName);
			cartingAgent.setVehicleNumber(vehicleNo);
			cartingAgent.setMobileNo(String.valueOf(mobile));
			
			result.put("MSG","Carting Agent Registration Details updation failed....!");
			if(insertNewRecordFlag){
				Integer id = cartingAgentService.insertCartingAgentDetilsAndDeleteOldCartingAgentDetails(cartingAgentId, cartingAgent);
				if(id!=null && id>0){
					cartingAgent.setUserDetails(userDetails);
					cartingAgentService.updateCartingAgentObject(cartingAgent);
					cartingAgentService.updateCartingAgentImagesObjectByNewCartingAgentId(cartingAgentId, id);
					result.put("MSG","Carting Agent Registration Details updated successfully....!");
					result.put("STOCKIST_ID",id);
					result.put("RESULT",true);
				}
			}else{
				cartingAgent.setUserDetails(userDetails);
				if(cartingAgentService.updateCartingAgentObject(cartingAgent)){
					result.put("MSG","Carting Agent Registration Details updated successfully....!");
					result.put("RESULT",true);
			     }
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
			result = new JSONObject();
			try {
				result.put("MSG","Operation Failed....!");
			} catch (JSONException e1) {
				e1.printStackTrace();
				log.error(e1);
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/updateCartingAgentPhotoImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateCartingAgentPhotoImage(
			@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
			@RequestParam(value="registrationId",required=true) Integer cartingAgentId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<CartingAgentImagePath> cartingAgentPhotoImgList = null;
		CartingAgentImagePath tempCartingAgentPhotoImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(documentImage!=null&&!(documentImage.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write  file
			cartingAgentPhotoImgList  = new ArrayList<CartingAgentImagePath>();
			for(MultipartFile file : documentImage){
				if(file!=null){
					tempCartingAgentPhotoImgPath = new CartingAgentImagePath();
					tempCartingAgentPhotoImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					cartingAgentPhotoImgList.add(tempCartingAgentPhotoImgPath);
				}
			}
			json_ImageArray = cartingAgentService.updateCartingAgentPhotoImage(cartingAgentId, cartingAgentPhotoImgList);
			result.put("MSG", "Carting Agent Photo Image File Upload successful...!");
			result.put("AGENT_PHOTO", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/deleteCartingAgentPhotoImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteCartingAgentPhotoImage(@RequestParam(value="cartingAgentPhotoImageId",required=false) Integer cartingAgentPhotoImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(cartingAgentService.deleteCartingAgentPhotoImage(cartingAgentPhotoImageId)){
				result.put("RESULT",true);
				result.put("MSG","Carting Agent Photo Image Deleted Successfully");
			}else{
				result.put("MSG","Carting Agent Photo Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/updateCartingAgentVehiclePhotoImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateCartingAgentVehiclePhotoImage(
			@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
			@RequestParam(value="registrationId",required=true) Integer cartingAgentId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<CartingAgentVehiclePhotoImagePath> cartingAgentVehiclePhotoImgList = null;
		CartingAgentVehiclePhotoImagePath tempCartingAgentVehiclePhotoImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(documentImage!=null&&!(documentImage.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write  file
			cartingAgentVehiclePhotoImgList  = new ArrayList<CartingAgentVehiclePhotoImagePath>();
			for(MultipartFile file : documentImage){
				if(file!=null){
					tempCartingAgentVehiclePhotoImgPath = new CartingAgentVehiclePhotoImagePath();
					tempCartingAgentVehiclePhotoImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					cartingAgentVehiclePhotoImgList.add(tempCartingAgentVehiclePhotoImgPath);
				}
			}
			json_ImageArray = cartingAgentService.updateCartingAgentVehiclePhotoImage(cartingAgentId, cartingAgentVehiclePhotoImgList);
			result.put("MSG", "Carting Agent Vehicle Photo Image File Upload successful...!");
			result.put("VEHICLE_PHOTO", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/deleteCartingAgentVehiclePhotoImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteCartingAgentVehiclePhotoImage(@RequestParam(value="cartingAgentVehiclePhotoImageId",required=false) Integer cartingAgentVehiclePhotoImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(cartingAgentService.deleteCartingAgentVehiclePhotoImage(cartingAgentVehiclePhotoImageId)){
				result.put("RESULT",true);
				result.put("MSG","Carting Agent Vehicle Photo Image Deleted Successfully");
			}else{
				result.put("MSG","Carting Agent Vehicle Photo Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	
	@RequestMapping(value="/searchCartingAgentUploadedDocumentDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchCartingAgentUploadedDocumentDetails(@RequestParam(value="searchOption",required=true) String searchOption,
			@RequestParam(value="searchText",required=true) String searchText,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
	
//		int searchOptioinINT=Integer.parseInt(searchOption);
		JSONArray json_data_array= new JSONArray();
		try 
		{
			json_data_array=cartingAgentService.searchCartingAgentsUploadedDocumentDetails(searchOption,searchText,(Integer) request.getSession().getAttribute("ORGANIZATION_ID"),from);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
}
