package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.ClaimRates;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.RatePerStation;
import com.protocol.wms.model.State;
import com.protocol.wms.model.StockistApprovalPending;
import com.protocol.wms.model.TransporterDetails;
import com.protocol.wms.model.TransporterDocument;
import com.protocol.wms.model.TransporterDocumentImagePath;
import com.protocol.wms.model.TransporterStationDetails;
import com.protocol.wms.model.TransporterStockistDetails;
import com.protocol.wms.model.TransporterStockistTransfer;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.model.UserType;
import com.protocol.wms.service.CityService;
import com.protocol.wms.service.ClaimRatesService;
import com.protocol.wms.service.DistrictService;
import com.protocol.wms.service.DocumentListTypeService;
import com.protocol.wms.service.OrganizationService;
import com.protocol.wms.service.RatePerStationService;
import com.protocol.wms.service.StateService;
import com.protocol.wms.service.TransporterDetailsService;
import com.protocol.wms.service.TransporterDocumentService;
import com.protocol.wms.service.TransporterStationDetailsService;
import com.protocol.wms.service.UserDetailsService;
import com.protocol.wms.service.UserTypeService;
/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/TransporterDetailsController")
public class TransporterDetailsController {
	private Logger log = Logger.getLogger(TransporterDetailsController.class.getClass());
	
	private StateService stateService;
	
	private UserDetailsService userDetailsService;
	
	private DistrictService districtService;
	
	private CityService cityService;
	
	private OrganizationService organizationService;
	
	private UserTypeService userTypeService;
	
	private TransporterStationDetailsService transporterStationDetailsService;
	
	private ClaimRatesService claimRatesService;
	
	private RatePerStationService ratePerStationService;
	
	private DocumentListTypeService documentListTypeService;
	
	private TransporterDocumentService transporterDocumentService;
	 
	

	@Autowired
	@Qualifier("StateServiceImpl")
	public void setStateService(StateService stateService) {
		this.stateService = stateService;
	}
	
	@Autowired
	@Qualifier("TransporterDetailsServiceImpl")
	private TransporterDetailsService transporterDetailsService;
	
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}
	
	@Autowired
	@Qualifier("DistrictServiceImpl")
	public void setDistrictService(DistrictService districtService) {
		this.districtService = districtService;
	}

	@Autowired
	@Qualifier("CityServiceImpl")
	public void setCityService(CityService cityService) {
		this.cityService = cityService;
	}
	
	@Autowired
	@Qualifier("OrganizationServiceImpl")
	public void setOrganizationService(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	@Autowired
	@Qualifier("UserTypeServiceImpl")
	public void setUserTypeService(UserTypeService userTypeService) {
		this.userTypeService = userTypeService;
	}
	
	@Autowired
	@Qualifier("TransporterStationDetailsServiceImpl")
	public void setTransporterStationDetailsService(
			TransporterStationDetailsService transporterStationDetailsService) {
		this.transporterStationDetailsService = transporterStationDetailsService;
	}

	
	@Autowired
	@Qualifier("ClaimRatesServiceImpl")
	public void setClaimRatesService(ClaimRatesService claimRatesService) {
		this.claimRatesService = claimRatesService;
	}
	
	@Autowired
	@Qualifier("RatePerStrationServiceImpl")
	public void setRatePerStationService(RatePerStationService ratePerStationService) {
		this.ratePerStationService = ratePerStationService;
	}

	@Autowired
	@Qualifier("DocumentListTypeServiceImpl")
	public void setDocumentListTypeService(
			DocumentListTypeService documentListTypeService) {
		this.documentListTypeService = documentListTypeService;
	}
	
	@Autowired
	@Qualifier("TransporterDocumentServiceImpl")
	public void setTransporterDocumentService(
			TransporterDocumentService transporterDocumentService) {
		this.transporterDocumentService = transporterDocumentService;
	}

	@RequestMapping(value="/stateList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String stateList(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			
			List<State> stateList=stateService.loadStateList((Integer)request.getSession().getAttribute("ORGANIZATION_ID"));;
			if(stateList.size()>0)
			{
				for(State obj : stateList)
				{
					json=new JSONObject();
					json.put("STATE_ID", obj.getStateId());
					json.put("STATE_NAME",obj.getStateName());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			
		}
		return json_data_array.toString();
	}

	@RequestMapping(value="/getTransporterList",method=RequestMethod.POST)
	private  @ResponseBody String getTransporterList(@RequestParam(value="orgId",required=false) Integer orgId){
		return transporterDetailsService.getTransporterList(orgId).toString();
	}
	
	@RequestMapping(value="/saveTransporter",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String saveTransporter(
			@RequestParam(value="organiztionNameId",required=true) Integer organiztionNameId,
			@RequestParam(value="state",required=true) Integer state,
			@RequestParam(value="city",required=true) Integer city,
			@RequestParam(value="address",required=true) String address,
			@RequestParam(value="pwd",required=true) String pwd,
			@RequestParam(value="transporterName",required=true) String transporterName,
			@RequestParam(value="district",required=true) Integer district,
			@RequestParam(value="panNo",required=true) String panNo,
			@RequestParam(value="userName",required=true) String userName)
	{
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			// check user name already register
			UserDetails userDetailsObj=userDetailsService.checkUserNameAlreadyExitOrNot(userName);
			if(userDetailsObj==null)
			{
				
				TransporterDetails transporterDetailsObj=new TransporterDetails();
				transporterDetailsObj.setTransporterName(transporterName);
				transporterDetailsObj.setTransporterAddress(address);
				transporterDetailsObj.setTransporterPanNo(panNo);
				transporterDetailsObj.setStatus(1);
				//set transporter submit date
				Date today = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			    String IST = df.format(today);
			    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    Date date = df2.parse(IST);
			    transporterDetailsObj.setSubmitDate(date);
			    
			    // set sate 
			    transporterDetailsObj.setState(stateService.loadSateObjectUsingStateId(state));
			    // set district
			    transporterDetailsObj.setDistrict(districtService.loadDistricObjectUsingDstrictId(district));
			    
			    // set city 
			    transporterDetailsObj.setCity(cityService.loadCityObjectUsingCityId(city));
			    // set organization 
			    transporterDetailsObj.setOrganization(organizationService.loadOrganizationObjectUsingOrganiztionId(organiztionNameId));
			    
			    // save transporter object
			    Integer transporterDetailsId=transporterDetailsService.saveTransporterDetails(transporterDetailsObj);
			    
			    if(transporterDetailsId!=null)
			    {
			    	// save user details 
			    	UserDetails newUserDetailsObj=new UserDetails();
			    	newUserDetailsObj.setUserName(userName);
			    	newUserDetailsObj.setPassword(pwd);
			    	newUserDetailsObj.setUserType(userTypeService.loadUserTypeUsingUserTypeId(Constant.TRANSPORTER));
			    	
			    	// save user details object
			    	Integer userDetailsId=userDetailsService.saveUserDetails(newUserDetailsObj);
			    	if(userDetailsId!=null)
			    	{
			    		// update transporter object
			    		
			    		// load transporter object
			    		TransporterDetails oldTransporterDetailsObj=transporterDetailsService.loadTransporterDetailsUsingTransporterDetailsId(transporterDetailsId);
			    		//load user details object and set
			    		oldTransporterDetailsObj.setUserDetails(userDetailsService.loadUserDetailsUsingUserDetailsId(userDetailsId));
			    		// update transporter object
			    		transporterDetailsService.updateTransporterDetails(oldTransporterDetailsObj);
			    	}
			    	json=new JSONObject();
					json.put("MSG","Transporter added successfully");
					json_data_array.put(json);
			    }
			    
			}
			else
			{
				json=new JSONObject();
				json.put("MSG","User Name already exists");
				json_data_array.put(json);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	//
	@RequestMapping(value="/updateTransporterDetails",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String updateTransporterDetails(
			@RequestParam(value="transporterDetailsId",required=true) Integer transporterDetailsId,
			@RequestParam(value="state",required=true) Integer stateId,
			@RequestParam(value="city",required=true) Integer cityId,
			@RequestParam(value="address",required=true) String address,
			@RequestParam(value="pwd",required=true) String pwd,
			@RequestParam(value="transporterName",required=true) String transporterName,
			@RequestParam(value="district",required=true) Integer districtId,
			@RequestParam(value="panNo",required=true) String panNo,
			@RequestParam(value="userName",required=true) String userName)
	{
		
		JSONObject result = new JSONObject();
		TransporterDetails transporterDetailsObj = null;
		UserType userType = null; 
		Boolean insertNewRecordFlag = false;
		try 
		{
			transporterDetailsObj = transporterDetailsService.loadTransporterDetailsUsingTransporterDetailsId(transporterDetailsId);
			// check user name already register
			UserDetails userDetailsObj= transporterDetailsObj.getUserDetails();
			userType = userDetailsObj.getUserType();
			if(!"".equals(userName)&&!"".equals(pwd)&&userName!=null&&pwd!=null){
				if(!userDetailsObj.getUserName().equals(userName)){
					if(userDetailsService.checkUserNameAlreadyExitOrNot(userName)!=null)
					{
						result.put("MSG", "User Name already exists.Please try unique user name....!");
						result.put("RESULT", false);
						return result.toString();
					}
				}
			}else{
				result.put("MSG","User Name and password are required....!");
				return result.toString();
			}
			if(transporterDetailsObj.getState().getStateId()!=stateId||transporterDetailsObj.getDistrict().getDistrictId()!=districtId||transporterDetailsObj.getCity().getCityId()!=cityId){
				insertNewRecordFlag=true;
				Organization organization = transporterDetailsObj.getOrganization();
				java.util.Date submitDate = transporterDetailsObj.getSubmitDate();
				transporterDetailsObj=new TransporterDetails();
				transporterDetailsObj.setOrganization(organization);
				transporterDetailsObj.setSubmitDate(submitDate);
				transporterDetailsObj.setStatus(1);
				userDetailsObj=new UserDetails();
				userDetailsObj.setUserName(userName);
				userDetailsObj.setPassword(pwd);
				userDetailsObj.setUserType(userType);
				transporterDetailsObj.setUserDetails(userDetailsObj);
			}else{
				userDetailsObj.setUserName(userName);
				userDetailsObj.setPassword(pwd);
				transporterDetailsObj.setUserDetails(userDetailsObj);
			}
			transporterDetailsObj.setTransporterName(transporterName);
			transporterDetailsObj.setTransporterAddress(address);
			transporterDetailsObj.setTransporterPanNo(panNo);
				
			 //Integer transporterDetailsId=transporterDetailsService.saveTransporterDetails(transporterDetailsObj);
			result.put("MSG", "Transporter Details updation failed....!");
			 if(insertNewRecordFlag){
				 result = transporterDetailsService.insertNewTransporterAndDeleteOldTransporter(transporterDetailsId, stateId, districtId, cityId, transporterDetailsObj);
			 }
			 else{
				 transporterDetailsService.updateTransporterDetails(transporterDetailsObj);
				 result.put("MSG", "Transporter Details updated successfully....!");
				 result.put("RESULT", true);
			 }
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			 try {
				result.put("RESULT", "Operation Failed...!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}
		}
		return result.toString();
		
	}
	
	@RequestMapping(value="/transporterDetailsList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String transporterDetailsList(
			HttpServletRequest request)
	{
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<TransporterDetails> transporterDetailsList=transporterDetailsService.loadTransporterDetailsListUsingOrganizationId((Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
			if(transporterDetailsList.size()>0)
			{
				for(TransporterDetails obj : transporterDetailsList)
				{
					if("1".equals(obj.getOrganization().getOrganizationStatus())){
						json=new JSONObject();
						json.put("TRAN_DETAILS_ID", obj.getTransporterDetailsId());
						json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
						json.put("TRAN_NAME", obj.getTransporterName());
						json.put("ADDRESS", obj.getTransporterAddress());
						json.put("CITY", obj.getCity().getCityName());
						json.put("STATA", obj.getState().getStateName());
						json.put("DISTRICT",obj.getDistrict().getDistrictName());
						json_data_array.put(json);
					}
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	
	@RequestMapping(value="/searchTransporterDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchTransporterDetails(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from", required= true)Integer from, HttpServletRequest request)
	{
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		List<TransporterDetails> transporterDetailsList = null;
		try 
		{
			Map<String,Object> map =transporterDetailsService.searchTransporterDetails(searchOption,searchText,from,(Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
			transporterDetailsList = (List<TransporterDetails>) map.get("transporterDetails");
			Integer totalPages = (Integer)map.get("totalPages");
			
			/*if(transporterDetailsList.size()>0)
			{*/
				for(TransporterDetails obj : transporterDetailsList)
				{
					if("1".equals(obj.getOrganization().getOrganizationStatus())){
						json=new JSONObject();
						json.put("TRAN_DETAILS_ID", obj.getTransporterDetailsId());
						json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
						json.put("TRAN_NAME", obj.getTransporterName());
						json.put("ADDRESS", obj.getTransporterAddress());
						json.put("CITY", obj.getCity().getCityName());
						json.put("STATE", obj.getState().getStateName());
						json.put("DISTRICT",obj.getDistrict().getDistrictName());
						json_data_array.put(json);
					}
				}
				json=new JSONObject();
				json.put("paginationCount",totalPages);
				json_data_array.put(json);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	@RequestMapping(value="/ViewAllDetailsOfTransporterStationDetailsId",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String ViewAllDetailsOfTransporterStationDetailsId(@RequestParam(value="transporterStationDetailsId",required=true) Integer transporterStationDetailsId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			TransporterStationDetails transStationObj=transporterStationDetailsService.laodTransporterStationDetailsUsignTransporterStationDetailsId((transporterStationDetailsId));
						json=new JSONObject();
						json.put("ORG_NAME", transStationObj.getOrganization().getOrganizationName());
						json.put("TRAN_NAME", transStationObj.getTransporterDetails().getTransporterName());
						if(transStationObj.getStationContactName().equalsIgnoreCase(""))
						{
						json.put("CONTACT_NAME", "");
						}else{
							json.put("CONTACT_NAME", transStationObj.getStationContactName());
						}
						json.put("ADDRESS", transStationObj.getTransporterStationAddress());
						if(transStationObj.getTransporterStationEmailId().equalsIgnoreCase(""))
						{
						json.put("EMAIL_ID", "");
						}else{
							json.put("EMAIL_ID", transStationObj.getTransporterStationEmailId());
						}
						json.put("STATION_NAME", transStationObj.getTransporterStationName());
						if(transStationObj.getTransporterStationPhoneNo() == null)
						{
						json.put("TELEPHONE","");
						}
						else
						{
							json.put("TELEPHONE",transStationObj.getTransporterStationPhoneNo());
						}
						if(transStationObj.getTransporterStationMobileNo() == null)
						{
						json.put("MOBILE", "");
						}
						else
						{
							json.put("MOBILE", transStationObj.getTransporterStationMobileNo());
						}
						CommonMethods common = new CommonMethods();
						json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(transStationObj.getSubmitDate().toString()));
						json.put("CLAIM_RATE", transStationObj.getClaimRates().getClaimRates());
						json.put("RATE_PER_LR", transStationObj.getClaimRates().getRatePerLR());
						json.put("DD_PER_CASE", transStationObj.getClaimRates().getDdPerCase());
						json.put("LABOUR_PER_CASE", transStationObj.getClaimRates().getLabourPerCase());
						json.put("RPC_RATE_PER_CASE", transStationObj.getRatePerStation().getRatePerCase());
						json.put("RPC_RATE_PER_LR", transStationObj.getRatePerStation().getRatePerLR());
						json.put("RPC_DD_PER_CASE", transStationObj.getRatePerStation().getDdPerCase());
						json.put("RPC_LABOUR_PER_CASE", transStationObj.getRatePerStation().getLabourPerCase());
						json_data_array.put(json);
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	
	@RequestMapping(value="/ViewAllDetailsOfTransporterDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String ViewAllDetailsOfTransporterDetails(@RequestParam(value="TransporterDetailsId",required=true) Integer TransporterDetailsId,
			@RequestParam(value="editStatus",required=false) Boolean editStatus,HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			TransporterDetails transObj=transporterDetailsService.loadTransporterDetailsUsingTransporterDetailsId((TransporterDetailsId));
						json=new JSONObject();
						json.put("ORG_NAME", transObj.getOrganization().getOrganizationName());
						json.put("TRAN_NAME", transObj.getTransporterName());
						json.put("PAN_NUM", transObj.getTransporterPanNo());
						json.put("ADDRESS", transObj.getTransporterAddress());
						json.put("USER_NAME", transObj.getUserDetails().getUserName());
						
						if(editStatus!=null&&editStatus==true){
							json.put("CITY_ID", transObj.getCity().getCityId());
							json.put("STATE_ID", transObj.getState().getStateId());
							json.put("DISTRICT_ID",transObj.getDistrict().getDistrictId());
							json.put("PASSWORD", transObj.getUserDetails().getPassword());
						}else{
							json.put("CITY", transObj.getCity().getCityName());
							json.put("STATE", transObj.getState().getStateName());
							json.put("DISTRICT",transObj.getDistrict().getDistrictName());
							CommonMethods common = new CommonMethods();
							json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(transObj.getSubmitDate().toString()));
						}
						json_data_array.put(json);
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	
	
	
	@RequestMapping(value="/transporterDetailsListUsingOrgId",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String transporterDetailsListUsingOrgId(
			@RequestParam(value="organizationId",required=true) Integer organizationId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<TransporterDetails> transporterDetailsList=transporterDetailsService.loadTransporterDetailsListUsingOrganizationId(organizationId);
			if(transporterDetailsList.size()>0)
			{
				for(TransporterDetails obj : transporterDetailsList)
				{
					json=new JSONObject();
					json.put("TRAN_DETAILS_ID", obj.getTransporterDetailsId());
					json.put("TRAN_NAME", obj.getTransporterName());
					json_data_array.put(json);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/saveStationPersonContactDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveStationPersonContactDetails(
			@RequestParam(value="TransOrgId",required=true) Integer organizationId,
			@RequestParam(value="transporterId",required=true) Integer transporterId,
			@RequestParam(value="stationPersonContactName",required=true) String stationPersonContactName,
			@RequestParam(value="emailId",required=true) String emailId,
			//
			@RequestParam(value="cityName",required=true) Integer cityId,
			@RequestParam(value="address",required=true) String address,
			@RequestParam(value="telephone",required=true) Long telephone,
			@RequestParam(value="mobile",required=true) Long mobile,
			@RequestParam(value="claimRate",required=true) Double claimRate,
			@RequestParam(value="cRatePerLR",required=true) Double cRatePerLR,
			@RequestParam(value="cDDPerCase",required=true) Double cDDPerCase,
			@RequestParam(value="cLaburPerCase",required=true) Double cLaburPerCase,
			@RequestParam(value="rRatePerCase",required=true) Double rRatePerCase,
			@RequestParam(value="rRatePerLR",required=true) Double rRatePerLR,
			@RequestParam(value="rDDPerCase",required=true) Double rDDPerCase,
			@RequestParam(value="rLabourPerCase",required=true) Double rLabourPerCase,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
		// save transporter station details	
			TransporterStationDetails tsdObj=new TransporterStationDetails();
			tsdObj.setStationContactName(stationPersonContactName);
			//tsdObj.setTransporterStationName(StationName);
			tsdObj.setTransporterStationEmailId(emailId);
			tsdObj.setTransporterStationAddress(address);
			tsdObj.setTransporterStationMobileNo(mobile);
			tsdObj.setTransporterStationPhoneNo(telephone);
			tsdObj.setStatus(1);
			//set transporter submit date
			Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    Date date = df2.parse(IST);
		    tsdObj.setSubmitDate(date);
		    //set Station Name
		    tsdObj.setTransporterStationName(cityService.getCityName(cityId).toString());
		    // set organization 
			tsdObj.setOrganization(organizationService.loadOrganizationObjectUsingOrganiztionId(organizationId));
			
			//set transporter 
			tsdObj.setTransporterDetails(transporterDetailsService.loadTransporterDetailsUsingTransporterDetailsId(transporterId));
			
			
			// save Transporter Station Details
			Integer transporterStationDetailsId=transporterStationDetailsService.saveTransporterStationDetails(tsdObj);
			if(transporterStationDetailsId!=null)
			{
				// save station claim rate
				ClaimRates claimRatesObj=new ClaimRates();
				claimRatesObj.setClaimRates(claimRate);
				claimRatesObj.setRatePerLR(cRatePerLR);
				claimRatesObj.setDdPerCase(cDDPerCase);
				claimRatesObj.setLabourPerCase(cLaburPerCase);
				// load transporter Station Details object usign transporterStationDetailsId
				claimRatesObj.setTransporterStationDetails(transporterStationDetailsService.laodTransporterStationDetailsUsignTransporterStationDetailsId(transporterStationDetailsId));
				
				// save claim Rates Object
				Integer claimRatesId=claimRatesService.saveClaimRate(claimRatesObj);
				System.out.println("claimRatesId="+claimRatesId);
				// save Rate Per Station
				RatePerStation ratePerStationObj=new RatePerStation();
				ratePerStationObj.setRatePerCase(rRatePerCase);
				ratePerStationObj.setRatePerLR(rRatePerLR);
				ratePerStationObj.setDdPerCase(rDDPerCase);
				ratePerStationObj.setLabourPerCase(rLabourPerCase);
				//
				ratePerStationObj.setTransporterStationDetails(transporterStationDetailsService.laodTransporterStationDetailsUsignTransporterStationDetailsId(transporterStationDetailsId));
				
				// save rate per station object
				Integer ratePerStationId=ratePerStationService.saveRatePerStationObject(ratePerStationObj);
				if(ratePerStationId!=null)
				{
					json=new JSONObject();
					json.put("MSG","Transporter added successfully");
					json_data_array.put(json);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/updateTransporterStationPersonContactDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateTransporterStationPersonContactDetails(
			@RequestParam(value="stationPersonContactDetailsId",required=true) Integer stationPersonContactDetailsId,
			@RequestParam(value="stationPersonContactName",required=true) String stationPersonContactName,
			@RequestParam(value="emailId",required=true) String emailId,
			@RequestParam(value="StationName",required=true) String StationName,
			@RequestParam(value="address",required=true) String address,
			@RequestParam(value="telephone",required=true) Long telephone,
			@RequestParam(value="mobile",required=true) Long mobile,
			@RequestParam(value="claimRate",required=true) Double claimRate,
			@RequestParam(value="cRatePerLR",required=true) Double cRatePerLR,
			@RequestParam(value="cDDPerCase",required=true) Double cDDPerCase,
			@RequestParam(value="cLaburPerCase",required=true) Double cLaburPerCase,
			@RequestParam(value="rRatePerCase",required=true) Double rRatePerCase,
			@RequestParam(value="rRatePerLR",required=true) Double rRatePerLR,
			@RequestParam(value="rDDPerCase",required=true) Double rDDPerCase,
			@RequestParam(value="rLabourPerCase",required=true) Double rLabourPerCase,
			HttpServletRequest request)
	{
		JSONObject result = new JSONObject();
		Boolean insertNewRecordFlag = false;
		try 
		{
			TransporterStationDetails tsdObj = transporterStationDetailsService.laodTransporterStationDetailsUsignTransporterStationDetailsId(stationPersonContactDetailsId);
			ClaimRates claimRatesObj = tsdObj.getClaimRates();
			RatePerStation ratePerStationObj = tsdObj.getRatePerStation();
			if(claimRatesObj.getClaimRates().compareTo(claimRate)!=0 || claimRatesObj.getDdPerCase().compareTo(cDDPerCase)!=0 || claimRatesObj.getLabourPerCase().compareTo(cLaburPerCase)!=0 || claimRatesObj.getRatePerLR().compareTo(cRatePerLR)!=0){
				insertNewRecordFlag = true;
			}
			else if(ratePerStationObj.getDdPerCase().compareTo(rDDPerCase)!=0  || ratePerStationObj.getLabourPerCase().compareTo(rLabourPerCase)!=0 || ratePerStationObj.getRatePerCase().compareTo(rRatePerCase)!=0 || ratePerStationObj.getRatePerLR().compareTo(rRatePerLR)!=0){
				insertNewRecordFlag = true;
			}
			if(insertNewRecordFlag){
				Organization organization = tsdObj.getOrganization();
				TransporterDetails transporterDetails = tsdObj.getTransporterDetails();
				java.util.Date submitDate = tsdObj.getSubmitDate();
				tsdObj=new TransporterStationDetails();
				tsdObj.setStatus(1);
				tsdObj.setSubmitDate(submitDate);
				tsdObj.setOrganization(organization);
				tsdObj.setTransporterDetails(transporterDetails);
				
				claimRatesObj=new ClaimRates();
				claimRatesObj.setClaimRates(claimRate);
				claimRatesObj.setRatePerLR(cRatePerLR);
				claimRatesObj.setDdPerCase(cDDPerCase);
				claimRatesObj.setLabourPerCase(cLaburPerCase);
				tsdObj.setClaimRates(claimRatesObj);
				
				ratePerStationObj=new RatePerStation();
				ratePerStationObj.setRatePerCase(rRatePerCase);
				ratePerStationObj.setRatePerLR(rRatePerLR);
				ratePerStationObj.setDdPerCase(rDDPerCase);
				ratePerStationObj.setLabourPerCase(rLabourPerCase);
				tsdObj.setRatePerStation(ratePerStationObj);
			}
			tsdObj.setStationContactName(stationPersonContactName);
			tsdObj.setTransporterStationName(StationName);
			tsdObj.setTransporterStationEmailId(emailId);
			tsdObj.setTransporterStationAddress(address);
			tsdObj.setTransporterStationMobileNo(mobile);
			tsdObj.setTransporterStationPhoneNo(telephone);
			result.put("MSG","Transporter Station Person Contact Details updation failed....!");
		    if(insertNewRecordFlag){
		    	Integer id = transporterStationDetailsService.insertNewTransporterStationDetailsAndDeleteOldRecordDetails(stationPersonContactDetailsId, tsdObj);
		    	if(id!=null && id>0){
		    		result.put("MSG","Transporter Station Person Contact Details updated successfully....!");
		    		result.put("RESULT",true);
		    		result.put("STAION_CONTACT_PER_ID",id);
		    	}
		    }else{
		    	if(transporterStationDetailsService.updateTransporterStationDetails(tsdObj)){
		    		result.put("MSG","Transporter Station Person Contact Details updated successfully....!");
		    		result.put("RESULT",true);
		    	}
		    }
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	
	@RequestMapping(value="/stationPersonContactDetailsList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String stationPersonContactDetailsList(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<TransporterStationDetails> transporterStationDetailsList=transporterStationDetailsService.loadTransporterStationDetailsListUsignOrganizationId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(transporterStationDetailsList.size()>0)
			{
				for (TransporterStationDetails obj : transporterStationDetailsList)
				{
					if("1".equals(obj.getOrganization().getOrganizationStatus())&&obj.getTransporterDetails().getStatus()==1){
						json=new JSONObject();
						json.put("TRAN_STATION_DETAILS_ID", obj.getTransporterStationDetailsId());
						json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
						json.put("TRANSPORTER_NAME", obj.getTransporterDetails().getTransporterName());
						json.put("STATION_NAME",obj.getTransporterStationName());
						if(obj.getStationContactName().equalsIgnoreCase(""))
						{
						json.put("CONTACT_NAME", "");
						}else{
							json.put("CONTACT_NAME", obj.getStationContactName());
						}
						if(obj.getTransporterStationMobileNo() == null)
						{
						json.put("MOBILE_NO", "");
						}else{
							json.put("MOBILE_NO", obj.getTransporterStationMobileNo());
						}
						if(obj.getTransporterStationEmailId().equalsIgnoreCase(""))
						{
						json.put("EMAIL", "");
						}else{
							json.put("EMAIL", obj.getTransporterStationEmailId());
						}
						json_data_array.put(json);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/searchStationPersonContactDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchStationPersonContactDetails(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=false) Integer from, HttpServletRequest request)
	{
		
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			Map<String,Object> map=transporterStationDetailsService.searchStationPersonContactDetails(searchOption,searchText,from,(Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			List<TransporterStationDetails> transporterStationDetailsList= (List<TransporterStationDetails>)map.get("transporterStationDetailsList");
			Integer totalPages = (Integer)map.get("totalPages");
			//if(transporterStationDetailsList.size()>0)
			//{
				for (TransporterStationDetails obj : transporterStationDetailsList)
				{
					//if("1".equals(obj.getOrganization().getOrganizationStatus())&&obj.getTransporterDetails().getStatus()==1)
					//{
						json=new JSONObject();
						json.put("TRAN_STATION_DETAILS_ID", obj.getTransporterStationDetailsId());
						json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
						json.put("TRANSPORTER_NAME", obj.getTransporterDetails().getTransporterName());
						json.put("STATION_NAME",obj.getTransporterStationName());
						json.put("CONTACT_NAME", obj.getStationContactName());
					//	json.put("MOBILE_NO", obj.getTransporterStationMobileNo());
						
						if(obj.getTransporterStationMobileNo()==null)
						{
							json.put("MOBILE_NO", "");
						}
						else
						{
							json.put("MOBILE_NO", obj.getTransporterStationMobileNo());
						}
						
						json.put("EMAIL", obj.getTransporterStationEmailId());
						json_data_array.put(json);
					}
			//}
				json=new JSONObject();
				json.put("paginationCount",totalPages);
				json_data_array.put(json);
			}
		//} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/saveTransporterUploadDocument",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveTransporterUploadDocument(
			@RequestParam(value="TranUploadDocumetOrgId",required=true) Integer organizationId,
			@RequestParam(value="transporter",required=true) Integer transporter,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId,
			@RequestParam(value="imageFile",required=true) MultipartFile documntFile[],
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		List<TransporterDocumentImagePath> documentFileList = null;
		TransporterDocumentImagePath tempDocumentFile = null;
		try 
		{
			/*if(!(documntFile.length>0)){
				json=new JSONObject();
				json.put("MSG", "Please Select at least one file...!");
				json_data_array.put(json);
				return json_data_array.toString();
			}*/
			documentFileList = new ArrayList<TransporterDocumentImagePath>();
			for(MultipartFile file : documntFile){
				tempDocumentFile = new TransporterDocumentImagePath();
				tempDocumentFile.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
				documentFileList.add(tempDocumentFile);
			}
			TransporterDocument transporterDocumentObj=new TransporterDocument();
			transporterDocumentObj.setTransporterDocumentImagePathList(documentFileList);
			transporterDocumentObj.setStatus(1);
			transporterDocumentObj.setOrganization(organizationService.loadOrganizationObjectUsingOrganiztionId(organizationId));
			transporterDocumentObj.setTransporterDetails(transporterDetailsService.loadTransporterDetailsUsingTransporterDetailsId(transporter));
			transporterDocumentObj.setDocumentListType(documentListTypeService.loadDocumentListTypeUsingDocumentListTypeId(documentTypeId));
			
			//set organization submit date
			Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    transporterDocumentObj.setSubmitDate(CommonMethods.setSubmitedDate());
			
			// save Transporter Document
			Integer transporterDocumentId=transporterDocumentService.saveTransporterDocument(transporterDocumentObj);
			if(transporterDocumentId!=null)
			{
				json=new JSONObject();
				json.put("RESULT", true);
				json.put("MSG", "Transporter document added successfully");
				json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//
	@RequestMapping(value="/updateTransporterDocumentInfo",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateTransporterDocumentInfo(
			@RequestParam(value="transporterDocId",required=true) Integer transporterDocumentId,
			@RequestParam(value="transporter",required=true) Integer transporterId,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId,
			HttpServletRequest request)
	{
		JSONObject result= null;
		try
		{
			result = transporterDocumentService.updateTransporterDocumentInfo(documentTypeId,transporterId,transporterDocumentId);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/updateTransportUploadDocumentImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateTransportUploadDocumentImage(
			@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
			@RequestParam(value="registrationId",required=true) Integer transportDocumentId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<TransporterDocumentImagePath> documentList = null;
		TransporterDocumentImagePath tempDocumentPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(documentImage!=null&&!(documentImage.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write image
			documentList  = new ArrayList<TransporterDocumentImagePath>();
			for(MultipartFile file : documentImage){
				if(file!=null){
					tempDocumentPath = new TransporterDocumentImagePath();
					tempDocumentPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					documentList.add(tempDocumentPath);
				}
			}
			json_ImageArray = transporterDocumentService.updateTransportUploadDocumentImage(transportDocumentId, documentList);
			result.put("MSG", "Transporter Document File Upload successful...!");
			result.put("FILE_NAME", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/deleteTrnsporterDocumentFileImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteTrnsporterDocumentFileImage(@RequestParam(value="transporterDocumentPathId",required=false) Integer transporterDocumentPathId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(transporterDocumentService.deleteTrnsporterDocumentFileImage(transporterDocumentPathId)){
				result.put("RESULT",true);
				result.put("MSG","Trnsporter Upload Document Image Deleted Successfully");
			}else{
				result.put("MSG","Trnsporter Upload Document Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	@RequestMapping(value="/loadTransporterDocument",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadTransporterDocument(HttpServletRequest request)
	{
		JSONArray json_data_array= null;
		try 
		{
			json_data_array = transporterDocumentService.loadTransporterDocumentListUsignOrganizationId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/searchTransporterUploadDocument",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchTransporterUploadDocument(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		try 
		{
			json_data_array=transporterDocumentService.searchTransporterUploadDocument(searchOption,searchText,(Integer) request.getSession().getAttribute("ORGANIZATION_ID"),from);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	//Delete Transporter Details
	@RequestMapping(value="/deleteTransporter",method=RequestMethod.POST)
	private  @ResponseBody String deleteTransporter(@RequestParam(value="transporterDetailsId",required=false) Integer transporterDetailsId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(transporterDetailsService.deleteTransporterDetails(transporterDetailsId)){
				result.put("MSG","Transporter Details Deleted Successfully");
			}else{
				result.put("MSG","Transporter Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		
		return result.toString();
	}
	//Delete Transporter  Station Person Contact Details
	@RequestMapping(value="/deleteStationPersonContactDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteStationPersonContactDetails(@RequestParam(value="stationPersonContactDetailsId",required=false) Integer stationPersonContactDetailsId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(transporterDetailsService.deleteStationPersonContactDetails(stationPersonContactDetailsId)){
				result.put("MSG","Transporter Station Person Contact Details Deleted Successfully");
			}else{
				result.put("MSG","Transporter Station Person Contact Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		
		return result.toString();
	}
	//Delete Transporter Document Details
	@RequestMapping(value="/deleteTransporterDocument",method=RequestMethod.POST)
	private  @ResponseBody String deleteTransporterDocument(@RequestParam(value="transporterDocumentId",required=false) Integer transporterDocumentId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(transporterDetailsService.deleteTransporterDocumentDetails(transporterDocumentId)){
				result.put("MSG","Transporter Document Details Deleted Successfully");
			}else{
				result.put("MSG","Transporter Document Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		
		return result.toString();
	}
	
	@RequestMapping(value="/searchTransporterStockistDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchTransporterStockistDetails(@RequestParam(value="OrgId",required=false) Integer orgId,
			@RequestParam(value="CompanyId",required=false) Integer companyId,
			@RequestParam(value="TransporterId", required= true)Integer transporterId, HttpServletRequest request)
	{
		JSONArray json_array=new JSONArray();
		JSONObject result=null;
		//List<TransporterStockistDetails>transporterStockistDetails=null;
		try
		{
		System.out.println("Orginazationid-"+orgId+"CompanyId-"+companyId+"TransporterId-"+transporterId);
		Map<String,Object> map =transporterDetailsService.searchTransporterStockistDetails(orgId,companyId,transporterId);
		List<TransporterStockistDetails>transporterStockistDetails=(List<TransporterStockistDetails>)map.get("transporterStockistDetails");
		for(TransporterStockistDetails transporter_Stockist_Details:transporterStockistDetails)
		{
			System.out.println("Transporter Details id controller-"+transporter_Stockist_Details.getTransporterDetailsId()+"Transporter Stations Details id controller-"+transporter_Stockist_Details.getTransporterStationDetailsId());
			result=new JSONObject();
			result.put("STOCKIST_ID",transporter_Stockist_Details.getStockistId());
			result.put("TRANSPORTER_ID",transporter_Stockist_Details.getTransporterDetailsId());
			result.put("TRANSPORTER_STATION_ID",transporter_Stockist_Details.getTransporterStationDetailsId());
			result.put("STOCKIST_NAME", transporter_Stockist_Details.getStockistName());
			result.put("CITY", transporter_Stockist_Details.getCity());
			result.put("ACTUAL_RATE", transporter_Stockist_Details.getRatePerCase());
			result.put("CLAIM_RATE", transporter_Stockist_Details.getClaimRates());
			result.put("ACTUAL_LR", transporter_Stockist_Details.getRatePerLR());
			result.put("CLAIM_LR",transporter_Stockist_Details.getClaimRatePerLR());
			result.put("PENDING_STATUS",transporter_Stockist_Details.getStockistApprovalStatus());
			json_array.put(result);
		}
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return json_array.toString();
	}
	@RequestMapping(value="/getTransporter",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String getTransporter(@RequestParam(value="orgId",required=false) Integer OrgId,
			@RequestParam(value="compId",required=false)Integer companyId)
	{
	
		return transporterDetailsService.getTransporter(OrgId,companyId).toString();
	
}
	
	@RequestMapping(value="/saveTranfereSelectedStockist",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveTranfereSelectedStockist(@RequestParam(value="stockistId",required=false) Integer stockistId[],
			@RequestParam(value="transporterStationArrayId",required=false)Integer transporterStationArrayId[],
			@RequestParam(value="fromTransporterId",required=false)Integer fromTransporterId,
			@RequestParam(value="toTransporterId",required=false)Integer toTransporterId)
	{
		
		JSONObject json_object=null;
		JSONArray json_array_object=new JSONArray();
		Integer count=0;
		try{
		for(int i=0;i<stockistId.length;i++)
		{
			System.out.println("Stockist id Conroller-"+stockistId[i]+"- "+transporterStationArrayId[i]+" -"+fromTransporterId+"- "+toTransporterId);
		}
		count=transporterDetailsService.saveTranfereSelectedStockist(stockistId,transporterStationArrayId,fromTransporterId,toTransporterId);
		if(count>0)
		{
			json_object=new JSONObject();
			json_object.put("MSG","Request Sent For Admin Approval");
			json_array_object.put(json_object);
		}
		else
		{

			json_object=new JSONObject();
			json_object.put("MSG","Transfer Request Failed");
			json_array_object.put(json_object);
		}
		}
		catch(Exception e){}
		
		return json_array_object.toString();
	}
	
	
	@RequestMapping(value="/loadPendingStockist",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadPendingStockist(@RequestParam(value="rateId",required=false) Integer rateId)
			{
		JSONObject json_object=null;
		JSONArray json_array=new JSONArray();
		try
		{
		System.out.println(rateId);
		Map<String,Object> map=transporterDetailsService.loadPendingStockist(rateId);
		List<StockistApprovalPending>transporterStockistTransfer=(List<StockistApprovalPending>)map.get("stockistApproval");
		for(StockistApprovalPending transporterStockistTransferObj:transporterStockistTransfer)
		{
			json_object=new JSONObject();
			json_object.put("TransporterStockistTransferId",transporterStockistTransferObj.getTransporterStockistTransfer());
			json_object.put("StockistId",transporterStockistTransferObj.getStockistId());
			json_object.put("StockistName",transporterStockistTransferObj.getStockistName());
			json_object.put("FromTransporterId",transporterStockistTransferObj.getFromTransporterId());
			json_object.put("FromTransporterName",transporterStockistTransferObj.getFromTransporterName());
			json_object.put("ToTransporterId",transporterStockistTransferObj.getToTransporterId());
			json_object.put("ToTransferName",transporterStockistTransferObj.getToTransporterName());
			json_object.put("StationId",transporterStockistTransferObj.getTransporterStationId());
			json_object.put("StationName",transporterStockistTransferObj.getTransporterStationName());
			json_object.put("FromRate",transporterStockistTransferObj.getActualRate());
			json_object.put("ToRate",transporterStockistTransferObj.getToActualRate());
			json_object.put("FromLRRate",transporterStockistTransferObj.getActualLR());
			json_object.put("ToLRRate",transporterStockistTransferObj.getToActualLR());
			json_object.put("Difference",transporterStockistTransferObj.getDifference());
			json_object.put("LRDifference",transporterStockistTransferObj.getLrDifference());
			json_object.put("status",transporterStockistTransferObj.getStatus());
			json_array.put(json_object);
		}
		
		}
		catch(Exception e){}
		return json_array.toString();
			}

	@RequestMapping(value="/loadAdminApprovalStatus",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadAdminApprovalStatus(@RequestParam(value="rateId",required=false) Integer rateId)
			{
		JSONObject json_object=null;
		JSONArray json_array=new JSONArray();
		try
		{
		System.out.println(rateId);
		Map<String,Object> map=transporterDetailsService.loadAdminApprovalStatus(rateId);
		List<StockistApprovalPending>transporterStockistTransfer=(List<StockistApprovalPending>)map.get("stockistApproval");
		for(StockistApprovalPending transporterStockistTransferObj:transporterStockistTransfer)
		{
			System.out.println("Lr Rate="+transporterStockistTransferObj.getActualLR()+transporterStockistTransferObj.getToActualLR()+transporterStockistTransferObj.getLrDifference());
			
			json_object=new JSONObject();
			json_object.put("TransporterStockistTransferId",transporterStockistTransferObj.getTransporterStockistTransfer());
			json_object.put("StockistId",transporterStockistTransferObj.getStockistId());
			json_object.put("StockistName",transporterStockistTransferObj.getStockistName());
			json_object.put("FromTransporterId",transporterStockistTransferObj.getFromTransporterId());
			json_object.put("FromTransporterName",transporterStockistTransferObj.getFromTransporterName());
			json_object.put("ToTransporterId",transporterStockistTransferObj.getToTransporterId());
			json_object.put("ToTransferName",transporterStockistTransferObj.getToTransporterName());
			json_object.put("StationId",transporterStockistTransferObj.getTransporterStationId());
			json_object.put("StationName",transporterStockistTransferObj.getTransporterStationName());
			json_object.put("FromRate",transporterStockistTransferObj.getActualRate());
			json_object.put("ToRate",transporterStockistTransferObj.getToActualRate());
			
			json_object.put("Difference",transporterStockistTransferObj.getDifference());
			
			json_object.put("status",transporterStockistTransferObj.getStatus());
			json_array.put(json_object);
			
		}
	
		}
		catch(Exception e){}
		return json_array.toString();
			}
	
	@RequestMapping(value="/saveApprovedData",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveApprovedData(@RequestParam(value="totransporterArray",required=false) String totransporterArray,
			@RequestParam(value="stockistArray",required=false)String stockistArray,
			@RequestParam(value="transporterStockistTransferArray",required=false)String transporterStockistTransfer)
	{
		JSONObject json_object=null;
		JSONArray json_array_object=new JSONArray();
		try
		{
		System.out.println("In SaveApprovedData 1266");
	String toTransporterArray[]=totransporterArray.split(",");
	String stockistApprovalArray[]=stockistArray.split(",");
	String transporterStockistTransferArray[]=transporterStockistTransfer.split(",");
	
	transporterDetailsService.saveApprovedData(toTransporterArray,stockistApprovalArray,transporterStockistTransferArray);
	json_object=new JSONObject();
	json_object.put("msg","Stockist Successfully Transfered");
	json_array_object.put(json_object);
		}
		catch(Exception e){e.printStackTrace();}
	return json_array_object.toString() ;	
	}
	
	@RequestMapping(value="/saveRejectedData",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveRejectedData(@RequestParam(value="transporterStockistTransferArray",required=false) String transporterStockistTransfer)
	{
		JSONObject json_object=null;
		JSONArray json_array_object=new JSONArray();
		try
		{
		System.out.println("In SaveApprovedData 1266");
	String transporterStockistTransferArray[]=transporterStockistTransfer.split(",");
	
	transporterDetailsService.saveRejectedData(transporterStockistTransferArray);
	json_object=new JSONObject();
	json_object.put("msg","Transfere Rejected");
	json_array_object.put(json_object);
		}
		catch(Exception e){e.printStackTrace();}
	return json_array_object.toString() ;	
	}
}


