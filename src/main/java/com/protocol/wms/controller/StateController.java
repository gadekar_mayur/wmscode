package com.protocol.wms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protocol.wms.model.State;
import com.protocol.wms.service.StateService;

@Controller
@RequestMapping(value="/StateController")
public class StateController {

	private Logger log = Logger.getLogger(StateController.class.getClass());
	
	@Autowired
	@Qualifier("StateServiceImpl")
	private StateService stateService;
	
	@RequestMapping(value="/stateDropDownList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String stateDropDownList(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
				List<State> stateList=stateService.loadStateList((Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
				if(stateList!=null)
				{
					for(State stateObj : stateList)
					{
					json=new JSONObject();
					json.put("STATE_NAME", stateObj.getStateName());
					json.put("STATE_ID", stateObj.getStateId());
					json_data_array.put(json);
					}
				}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
}
