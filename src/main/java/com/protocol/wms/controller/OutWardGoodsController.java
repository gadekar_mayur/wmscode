/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.dao.OutwardOrderEntryRegistrationImageCopyPathDao;
import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentTripCount;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.CompanyProduct;
import com.protocol.wms.model.InvoicePaymentInformation;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationTripCount;
import com.protocol.wms.model.OutWardDispatchEnteryRegistration;
import com.protocol.wms.model.OutWardGatePass;
import com.protocol.wms.model.OutWardGatePassLrImagePath;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistrationOrderCopyPath;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.OutWardProductOrderEntryRegistration;
import com.protocol.wms.model.OutWardTrip;
import com.protocol.wms.model.OutWardTripTransporterDetails;
import com.protocol.wms.model.OutwardTripReport;
import com.protocol.wms.model.PrintSticker;
import com.protocol.wms.model.RemainingChequeAmount;
import com.protocol.wms.model.TransporterDetails;
import com.protocol.wms.service.OutWardGoodsService;

/**
 * @author Sudhakar
 *
 */
@Controller
@RequestMapping(value = "/OutWardGoodsController")
public class OutWardGoodsController {

	private Logger log = Logger.getLogger(OutWardGoodsController.class
			.getClass());

	private OutWardGoodsService outWardGoodsService;
	@Autowired
	private HttpSession session;

	@Autowired
	@Qualifier("OutWardGoodsServiceImpl")
	public void setOutWardGoodsService(OutWardGoodsService outWardGoodsService) {
		this.outWardGoodsService = outWardGoodsService;
	}

	@RequestMapping(value = "/getProductListUsignCompanyId", method = RequestMethod.POST)
	private @ResponseBody String getProductListUsignCompanyId(
			@RequestParam(value = "companyId", required = true) Integer companyId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<CompanyProduct> companyProducts = outWardGoodsService
					.loadCompanyProductListUsignCompanyId(companyId);
			if (companyProducts.size() > 0) {
				for (CompanyProduct obj : companyProducts) {
					json = new JSONObject();
					json.put("PRODUCT_ID", obj.getCompanyProductId());
					json.put("PRODUCT_NAME", obj.getCompanyProductName());
					json_data_array.put(json);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/saveOrderEntryRegistration", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String saveOrderEntryRegistration(
			@RequestParam(value = "selectedOrganiztionNameId", required = true) Integer selectedOrganiztionNameId,
			@RequestParam(value = "orderNo", required = true) String orderNo,
			@RequestParam(value = "orderModeName", required = true) Integer orderModeName,
			@RequestParam(value = "remark", required = true) String remark,
			@RequestParam(value = "stockistId", required = true) Integer stockistId,
			@RequestParam(value = "comapnyId", required = true) Integer comapnyId,
			@RequestParam(value = "orderDate", required = true) String orderDate,
			@RequestParam(value = "orderCopy", required = true) MultipartFile orderCopy[]) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		String jsonResponse = "";
		ObjectMapper objectMapper = null;
		List<OutWardOrderEntryRegistrationOrderCopyPath> orderCopyPathList = null;
		OutWardOrderEntryRegistrationOrderCopyPath tempOrderCopyPath = null;
		OutWardOrderEntryRegistration a = new OutWardOrderEntryRegistration();
		try {
			System.out.println("organizationNameId-"+selectedOrganiztionNameId+"OrderModeName"+orderModeName);

			// check company credit and discount set or not
			CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
					.loadCompanyCreditAndDiscountObjectUsignComapnyId(comapnyId);
			if (companyCreditAndDiscountObj != null) {
				DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
				OutWardOrderEntryRegistration orderEneterObj = new OutWardOrderEntryRegistration();
				// write lr image
				orderCopyPathList = new ArrayList<OutWardOrderEntryRegistrationOrderCopyPath>();
				for (MultipartFile file : orderCopy) {
					if (file != null) {
						tempOrderCopyPath = new OutWardOrderEntryRegistrationOrderCopyPath();
						tempOrderCopyPath.setImagePath(CommonMethods
								.writeDocumentAndGetFileDB_Path(file));
						orderCopyPathList.add(tempOrderCopyPath);
					}
				}
				orderEneterObj
						.setOutWardOrderEntryRegistrationOrderCopyPathList(orderCopyPathList);
				orderEneterObj.setOrderNo(orderNo);
				orderEneterObj.setCompanyIndex(Integer.parseInt(orderNo));
				java.util.Date d1 = dtf.parse(orderDate);
				orderEneterObj.setOrderDate(new java.sql.Date(d1.getTime()));
				// submit date
				orderEneterObj.setSubmitDate(CommonMethods.setSubmitedDate());
				orderEneterObj.setRemark(remark);
				// set organization
				orderEneterObj
						.setOrganization(outWardGoodsService
								.loadOrganizationObjectUsingOrganiztionId(selectedOrganiztionNameId));
				// set company
				Company companyObj = outWardGoodsService
						.loadCompanyObjectUsingCompanyId(comapnyId);
				orderEneterObj.setCompany(companyObj);
				// set Stockist
				orderEneterObj.setStockist(outWardGoodsService
						.loadStockistObjectByStockistId(stockistId));
				// set order mode
				orderEneterObj.setOrderMode(outWardGoodsService
						.loadOrderModeObjectUsignOrderModeId(orderModeName));
				orderEneterObj.setInvoiceStatus(0);
				orderEneterObj.setProductStatus(0);
				orderEneterObj.setStatus(1);
				// save Out Ward Order entry Registration
				Integer OutWardOrderEntryRegistrationId = outWardGoodsService
						.saveOutWardOrderEntryRegistration(orderEneterObj);
				if (OutWardOrderEntryRegistrationId != null) {
					// // Update order id;
					OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj = outWardGoodsService
							.loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(OutWardOrderEntryRegistrationId);
					if (companyObj.getCompanyName().length() > 0) {
						outWardOrderEntryRegistrationObj.setOrderId(companyObj
								.getCompanyName().substring(0, 2)
								+ OutWardOrderEntryRegistrationId);
						outWardOrderEntryRegistrationObj
								.setCompanyIndex(Integer.parseInt(orderNo));
						outWardGoodsService
								.updateOutWardOrderEntryRegistration(outWardOrderEntryRegistrationObj);
					}

					// create company wise order id

					json = new JSONObject();
					json.put("STATUS", "true");
					json.put("MSG", "Order Added Successfully");
					json_data_array.put(json);
				}
			} else {
				json = new JSONObject();
				json.put("STATUS", "false");
				json.put("MSG", "Please first add company credit and discount");
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/checkName", method = RequestMethod.POST)
	public @ResponseBody String checkName(String orderNo, Integer companyId)
			throws Exception {

		Boolean flag = false;
		JSONObject result = new JSONObject();
		try {
			flag = outWardGoodsService.checkName(orderNo, companyId);
			System.out.println("flag=" + flag);
			if (flag) {
				result.put("MSG", "Order No. already exists");
				result.put("FLAG", true);
				return result.toString();
			}
			result.put("MSG", "Order No. available");
			result.put("FLAG", false);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("MSG", "Order No. available");
			result.put("FLAG", false);
		}
		return result.toString();
	}

	@RequestMapping(value = "/getOutWardOrderEntryRegistrationOfProductStatusZero", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardOrderEntryRegistrationOfProductStatusZero(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardOrderEntryRegistration> outWardOrderEntryRegistrations = outWardGoodsService
					.loadOutWardOrderEntryRegistrationOfProductStatusIsZero((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardOrderEntryRegistrations.size() > 0) {
				for (OutWardOrderEntryRegistration obj : outWardOrderEntryRegistrations) {
					json = new JSONObject();
					json.put("OutWardOrderEntryRegistrationId",
							obj.getOutWardOrderEnteryRegistrationId());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("COMPANY_ID", obj.getCompany().getCompanyId());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("ORDER_MODE", obj.getOrderMode()
							.getOrderModeName());
					json.put("DATE", obj.getOrderDate());
					json.put("ORDER_ID", obj.getOrderId());
					json.put("ORDER_NO", obj.getOrderNo());
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/saveOutWardOrderProduct", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String saveOutWardOrderProduct(
			@RequestParam(value = "outWardOrderEntryRegistrationId", required = true) Integer outWardOrderEntryRegistrationId,
			@RequestParam(value = "criteria", required = true) String criteria,
			@RequestParam(value = "quantity", required = true) Integer quantity[],
			@RequestParam(value = "productName", required = true) String productName[]) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			for (int i = 0; i < productName.length - 1; i++) {
				OutWardProductOrderEntryRegistration outWardProductOrderEnteryRegistrationObj = new OutWardProductOrderEntryRegistration();
				outWardProductOrderEnteryRegistrationObj
						.setQuantity(quantity[i]);
				outWardProductOrderEnteryRegistrationObj
						.setCompanyProduct(outWardGoodsService
								.loadCompanyProductObjectUsignCompanyProductId(Integer
										.parseInt(productName[i])));
				outWardProductOrderEnteryRegistrationObj.setStatus(1);
				// set submit date
				Date today = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
				df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
				String IST = df.format(today);
				DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
				Date date1 = df2.parse(IST);
				outWardProductOrderEnteryRegistrationObj.setSubmitDate(date1);

				// set outWard Order entry RegistrationObj
				outWardProductOrderEnteryRegistrationObj
						.setOutWardOrderEntryRegistration(outWardGoodsService
								.loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(outWardOrderEntryRegistrationId));
				// save outWardProductOrderEnteryRegistrationObj
				outWardGoodsService
						.saveOutWardProductOrderEnteryRegistration(outWardProductOrderEnteryRegistrationObj);
			}
			// update out ward order entry registration
			OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj = outWardGoodsService
					.loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(outWardOrderEntryRegistrationId);
			outWardOrderEntryRegistrationObj.setProductStatus(1);
			outWardGoodsService
					.updateOutWardOrderEntryRegistration(outWardOrderEntryRegistrationObj);
			json = new JSONObject();
			json.put("MSG", "Product added successfully");
			json.put("CRITERIA", criteria);
			json_data_array.put(json);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardOrderEntryRegistrationOfProductStatusOne", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardOrderEntryRegistrationOfProductStatusOne(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardOrderEntryRegistration> outWardOrderEntryRegistrations = outWardGoodsService
					.loadOutWardOrderEntryRegistrationOfProductStatusIsOne((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardOrderEntryRegistrations.size() > 0) {
				for (OutWardOrderEntryRegistration obj : outWardOrderEntryRegistrations) {
					json = new JSONObject();
					json.put("OutWardOrderEntryRegistrationId",
							obj.getOutWardOrderEnteryRegistrationId());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("ORANIZATION_ID", obj.getOrganization()
							.getOrganizationId());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("COMPANY_ID", obj.getCompany().getCompanyId());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("ORDER_MODE", obj.getOrderMode()
							.getOrderModeName());
					json.put("DATE", obj.getOrderDate());
					json.put("ORDER_ID", obj.getOrderId());
					json.put("ORDER_NO", obj.getOrderNo());
					json.put("PRODUCT_STATUS", obj.getProductStatus());

					// get discount percentages
					String stockistLocation = obj.getStockist()
							.getStockistLocation();
					if (stockistLocation.equalsIgnoreCase("Local")) {
						CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
								.loadCompanyCreditAndDiscountObjectUsignComapnyId(obj
										.getCompany().getCompanyId());
						json.put("PERCENTAGE", companyCreditAndDiscountObj
								.getLocalPercentage());
					} else {
						CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
								.loadCompanyCreditAndDiscountObjectUsignComapnyId(obj
										.getCompany().getCompanyId());
						json.put("PERCENTAGE", companyCreditAndDiscountObj
								.getOutsideStationPercentage());
					}
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/saveInvoiceEntry", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String saveInvoiceEntry(
			@RequestParam(value = "outWardOrderEntryRegistrationId", required = true) Integer outWardOrderEntryRegistrationId,
			@RequestParam(value = "organizationId", required = true) Integer organizationId,
			@RequestParam(value = "invoiceNo", required = true) String invoiceNo[],
			@RequestParam(value = "invoiceDate", required = true) String invoiceDate[],
			@RequestParam(value = "grossAmount", required = true) Double grossAmount[],
			@RequestParam(value = "taxAmount", required = true) Double taxAmount[],
			@RequestParam(value = "vatAmount", required = true) Double vatAmount[],
			@RequestParam(value = "netAmount", required = true) Double netAmount[],
			@RequestParam(value = "radioDiscount", required = true) String radioDiscount,
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		try {
			// load organization object
			Organization organizationObj = outWardGoodsService
					.loadOrganizationObjectUsingOrganiztionId(organizationId);

			String[] discount = radioDiscount.split(",");
			List<String> list = new ArrayList<String>(Arrays.asList(discount));

			for (int i = 0; i < list.size(); i++)

			{
				if (list.get(i).contains("-")) {
					list.remove(i);
					// as element is removed, next element will have decremented
					// index
					i--;
				}
			}

			// list.remove("-");
			discount = list.toArray(new String[0]);
			// load
			OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj = outWardGoodsService
					.loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(outWardOrderEntryRegistrationId);

			// check stockist days
			String stockistLocation = outWardOrderEntryRegistrationObj
					.getStockist().getStockistLocation();
			Integer noOfDays = 0;
			if (stockistLocation.equalsIgnoreCase("Local")) {
				CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
						.loadCompanyCreditAndDiscountObjectUsignComapnyId(outWardOrderEntryRegistrationObj
								.getCompany().getCompanyId());
				noOfDays = companyCreditAndDiscountObj.getLocalDays();
			} else {
				CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
						.loadCompanyCreditAndDiscountObjectUsignComapnyId(outWardOrderEntryRegistrationObj
								.getCompany().getCompanyId());
				noOfDays = companyCreditAndDiscountObj.getOutsideStationDays();
			}
			Double invoiceAmount;
			Double remainingInvoiceAmount;
			Integer outWardOrderInvoiceEnteryRegistrationId = null;
			for (int i = 0; i < invoiceNo.length - 1; i++) {
				invoiceAmount = 0.0;
				// check remaining invoice amount available or not
				List<RemainingChequeAmount> remainingChequeAmountList = outWardGoodsService
						.loadremainingChequeAmountListUsignOrgIdAndCompIdAndStockistId(
								outWardOrderEntryRegistrationObj
										.getOrganization().getOrganizationId(),
								outWardOrderEntryRegistrationObj.getCompany()
										.getCompanyId(),
								outWardOrderEntryRegistrationObj.getStockist()
										.getStockistId());
				if (remainingChequeAmountList.size() > 0) {

					// set submit date
					java.util.Date today = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
					String IST = df.format(today);
					DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					java.util.Date date = df2.parse(IST);

					invoiceAmount = netAmount[i];
					int flag = 0;
					//
					for (RemainingChequeAmount obj : remainingChequeAmountList) {
						// load cheque number information object
						ChequeNumberInfo chequeNumberInfoObj = outWardGoodsService
								.loadChequeNumberInfoObjUsignChequeNumberInfoId(obj
										.getChequeNumberInfo()
										.getChequeNumberInfoId());
						Double ChequeAmount = obj.getRemainingChequeAmount();
						// pay invoice
						if (invoiceAmount <= ChequeAmount) {
							ChequeAmount = ChequeAmount - invoiceAmount;
							if (flag == 0) {
								OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = new OutWardOrderInvoiceEnteryRegistration();
								outWardOrderInvoiceEnteryRegistrationObj
										.setDispatchEnteryRegistrationStatus(0);
								outWardOrderInvoiceEnteryRegistrationObj
										.setGrossAmount(grossAmount[i]);
								// set invoice date
								java.util.Date str_invoiceDate = dtf
										.parse(invoiceDate[i]);
								java.sql.Date sqlInvoiceDate = new java.sql.Date(
										str_invoiceDate.getTime());
								outWardOrderInvoiceEnteryRegistrationObj
										.setInvoiceDate(sqlInvoiceDate);
								outWardOrderInvoiceEnteryRegistrationObj
										.setInvoiceNo(invoiceNo[i]);
								outWardOrderInvoiceEnteryRegistrationObj
										.setNetAmount(netAmount[i]);
								outWardOrderInvoiceEnteryRegistrationObj
										.setTaxAmout(taxAmount[i]);
								outWardOrderInvoiceEnteryRegistrationObj
										.setVatAmount(vatAmount[i]);
								outWardOrderInvoiceEnteryRegistrationObj
										.setOutWardOrderEnteryRegistration(outWardOrderEntryRegistrationObj);
								// // set submit date
								// java.util.Date today = new Date();
								// DateFormat df = new
								// SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
								// df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
								// String IST = df.format(today);
								// DateFormat df2 = new
								// SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
								// java.util.Date date = df2.parse(IST);
								outWardOrderInvoiceEnteryRegistrationObj
										.setSubmitDate(date);

								outWardOrderInvoiceEnteryRegistrationObj
										.setPaymentStatus(1);
								outWardOrderInvoiceEnteryRegistrationObj
										.setRemainingPayment(0.0);

								outWardOrderInvoiceEnteryRegistrationObj
										.setStatus(1);
								// calculate no of days
								outWardOrderInvoiceEnteryRegistrationObj
										.setDiscountIsTakenOrNot(discount[i]);
								// load Stockist station
								if (discount[i].equalsIgnoreCase("Yes")) {
									// java.sql.Date sqlDate = new
									// java.sql.Date(date.getTime());
									outWardOrderInvoiceEnteryRegistrationObj
											.setNoOfDays(0);
									outWardOrderInvoiceEnteryRegistrationObj
											.setFirstDate(sqlInvoiceDate);
									outWardOrderInvoiceEnteryRegistrationObj
											.setLastDate(sqlInvoiceDate);
								} else {
									// java.sql.Date todaysDate = new
									// java.sql.Date(date.getTime());
									outWardOrderInvoiceEnteryRegistrationObj
											.setNoOfDays(noOfDays);
									outWardOrderInvoiceEnteryRegistrationObj
											.setFirstDate(sqlInvoiceDate);
									// calculate next date after some days
									Calendar cal = Calendar.getInstance();
									cal.setTime(str_invoiceDate);
									cal.add(Calendar.DATE, noOfDays); // minus
																		// number
																		// would
																		// decrement
																		// the
																		// days
									java.sql.Date afterDate = new java.sql.Date(
											cal.getTimeInMillis());
									outWardOrderInvoiceEnteryRegistrationObj
											.setLastDate(afterDate);
								}
								// save out Ward Order Invoice entry
								// Registration Object
								outWardOrderInvoiceEnteryRegistrationObj
										.setOrganization(organizationObj);
								outWardOrderInvoiceEnteryRegistrationId = outWardGoodsService
										.saveOutWardOrderInvoiceEnteryRegistrationObject(outWardOrderInvoiceEnteryRegistrationObj);
								flag = 1;
							}
							if (outWardOrderInvoiceEnteryRegistrationId != null
									&& flag == 1) {
								// paid invoice
								InvoicePaymentInformation invoicePaymentInformationObj = new InvoicePaymentInformation();
								// set submit date
								invoicePaymentInformationObj
										.setSubmitDate(date);
								invoicePaymentInformationObj
										.setPaidAmount(invoiceAmount);
								// setOutWardOrderInvoiceEnteryRegistration
								invoicePaymentInformationObj
										.setOutWardOrderInvoiceEnteryRegistration(outWardGoodsService
												.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationId));
								// set chequeNumberInfoObj
								invoicePaymentInformationObj
										.setChequeNumberInfo(chequeNumberInfoObj);
								// save invoicePaymentInformationObj
								Integer invoicePaymentInformationId = outWardGoodsService
										.saveInvoicePaymentInformationObj(invoicePaymentInformationObj);
								if (invoicePaymentInformationId != null) {
									// update remaining cheque amount
									RemainingChequeAmount remainingChequeAmountObj = outWardGoodsService
											.loadRemainingChequeAmountUsignRemainingChequeAmountId(obj
													.getRemainingChequeAmountId());
									remainingChequeAmountObj
											.setRemainingChequeAmount(ChequeAmount);
									if (ChequeAmount.equals(0.0)) {
										remainingChequeAmountObj.setStatus(0);
									}
									outWardGoodsService
											.updateRemainingChequeAmountObj(remainingChequeAmountObj);
								}
								invoiceAmount = 0.0;
								// update invoice remaining payment Status
								if (invoiceAmount.equals(0.0)) {
									OutWardOrderInvoiceEnteryRegistration oldOutWardOrderInvoiceEnteryRegistrationObj = outWardGoodsService
											.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationId);
									oldOutWardOrderInvoiceEnteryRegistrationObj
											.setRemainingPayment(0.0);
									oldOutWardOrderInvoiceEnteryRegistrationObj
											.setPaymentStatus(1);
									outWardGoodsService
											.updateOutWardOrderInvoiceEnteryRegistrationObj(oldOutWardOrderInvoiceEnteryRegistrationObj);
								}
							}
							break;
						} else {
							remainingInvoiceAmount = invoiceAmount
									- ChequeAmount;
							if (netAmount[i] == invoiceAmount) {
								invoiceAmount = remainingInvoiceAmount;
								if (flag == 0) {
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = new OutWardOrderInvoiceEnteryRegistration();
									outWardOrderInvoiceEnteryRegistrationObj
											.setDispatchEnteryRegistrationStatus(0);
									outWardOrderInvoiceEnteryRegistrationObj
											.setGrossAmount(grossAmount[i]);
									// set invoice date
									java.util.Date str_invoiceDate = dtf
											.parse(invoiceDate[i]);
									java.sql.Date sqlInvoiceDate = new java.sql.Date(
											str_invoiceDate.getTime());
									outWardOrderInvoiceEnteryRegistrationObj
											.setInvoiceDate(sqlInvoiceDate);
									outWardOrderInvoiceEnteryRegistrationObj
											.setInvoiceNo(invoiceNo[i]);
									outWardOrderInvoiceEnteryRegistrationObj
											.setNetAmount(netAmount[i]);
									outWardOrderInvoiceEnteryRegistrationObj
											.setTaxAmout(taxAmount[i]);
									outWardOrderInvoiceEnteryRegistrationObj
											.setVatAmount(vatAmount[i]);
									outWardOrderInvoiceEnteryRegistrationObj
											.setOutWardOrderEnteryRegistration(outWardOrderEntryRegistrationObj);
									outWardOrderInvoiceEnteryRegistrationObj
											.setSubmitDate(date);
									outWardOrderInvoiceEnteryRegistrationObj
											.setPaymentStatus(0);
									outWardOrderInvoiceEnteryRegistrationObj
											.setRemainingPayment(remainingInvoiceAmount);
									outWardOrderInvoiceEnteryRegistrationObj
											.setStatus(1);
									// calculate no of days
									outWardOrderInvoiceEnteryRegistrationObj
											.setDiscountIsTakenOrNot(discount[i]);
									// load Stockist station
									if (discount[i].equalsIgnoreCase("Yes")) {
										outWardOrderInvoiceEnteryRegistrationObj
												.setNoOfDays(0);
										outWardOrderInvoiceEnteryRegistrationObj
												.setFirstDate(sqlInvoiceDate);
										outWardOrderInvoiceEnteryRegistrationObj
												.setLastDate(sqlInvoiceDate);
									} else {
										outWardOrderInvoiceEnteryRegistrationObj
												.setNoOfDays(noOfDays);
										outWardOrderInvoiceEnteryRegistrationObj
												.setFirstDate(sqlInvoiceDate);
										// calculate next date after some days
										Calendar cal = Calendar.getInstance();
										cal.setTime(str_invoiceDate);
										cal.add(Calendar.DATE, noOfDays); // minus
																			// number
																			// would
																			// decrement
																			// the
																			// days
										java.sql.Date afterDate = new java.sql.Date(
												cal.getTimeInMillis());
										outWardOrderInvoiceEnteryRegistrationObj
												.setLastDate(afterDate);
									}
									// save out Ward Order Invoice entry
									// Registration Object
									outWardOrderInvoiceEnteryRegistrationObj
											.setOrganization(organizationObj);
									outWardOrderInvoiceEnteryRegistrationId = outWardGoodsService
											.saveOutWardOrderInvoiceEnteryRegistrationObject(outWardOrderInvoiceEnteryRegistrationObj);
									flag = 1;
								}
								if (outWardOrderInvoiceEnteryRegistrationId != null
										&& flag == 1) {
									// paid invoice
									InvoicePaymentInformation invoicePaymentInformationObj = new InvoicePaymentInformation();
									// set submit date
									invoicePaymentInformationObj
											.setSubmitDate(date);
									invoicePaymentInformationObj
											.setPaidAmount(ChequeAmount);
									// setOutWardOrderInvoiceEnteryRegistration
									invoicePaymentInformationObj
											.setOutWardOrderInvoiceEnteryRegistration(outWardGoodsService
													.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationId));
									// set chequeNumberInfoObj
									invoicePaymentInformationObj
											.setChequeNumberInfo(chequeNumberInfoObj);
									// save invoicePaymentInformationObj
									Integer invoicePaymentInformationId = outWardGoodsService
											.saveInvoicePaymentInformationObj(invoicePaymentInformationObj);
									if (invoicePaymentInformationId != null) {
										// update into data base and status zero
										RemainingChequeAmount remainingChequeAmountObj = outWardGoodsService
												.loadRemainingChequeAmountUsignRemainingChequeAmountId(obj
														.getRemainingChequeAmountId());
										remainingChequeAmountObj
												.setRemainingChequeAmount(0.0);
										remainingChequeAmountObj.setStatus(0);
										outWardGoodsService
												.updateRemainingChequeAmountObj(remainingChequeAmountObj);
									}
								}
							} else {
								remainingInvoiceAmount = invoiceAmount
										- ChequeAmount;
								invoiceAmount = remainingInvoiceAmount;
								// update remaining payment information
								OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = outWardGoodsService
										.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationId);

								// paid invoice
								InvoicePaymentInformation invoicePaymentInformationObj = new InvoicePaymentInformation();
								// //set submit date
								// java.util.Date today = new Date();
								// DateFormat df = new
								// SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
								// df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
								// String IST = df.format(today);
								// DateFormat df2 = new
								// SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
								// java.util.Date date = df2.parse(IST);
								invoicePaymentInformationObj
										.setSubmitDate(date);
								invoicePaymentInformationObj
										.setPaidAmount(ChequeAmount);
								// setOutWardOrderInvoiceEnteryRegistration
								invoicePaymentInformationObj
										.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObj);
								// set chequeNumberInfoObj
								invoicePaymentInformationObj
										.setChequeNumberInfo(chequeNumberInfoObj);
								// save invoicePaymentInformationObj
								Integer invoicePaymentInformationId = outWardGoodsService
										.saveInvoicePaymentInformationObj(invoicePaymentInformationObj);
								if (invoicePaymentInformationId != null) {
									// update into data base and status zero
									RemainingChequeAmount remainingChequeAmountObj = outWardGoodsService
											.loadRemainingChequeAmountUsignRemainingChequeAmountId(obj
													.getRemainingChequeAmountId());
									remainingChequeAmountObj
											.setRemainingChequeAmount(0.0);
									remainingChequeAmountObj.setStatus(0);
									outWardGoodsService
											.updateRemainingChequeAmountObj(remainingChequeAmountObj);

									// update
									// outWardOrderInvoiceEnteryRegistrationObj
									outWardOrderInvoiceEnteryRegistrationObj
											.setRemainingPayment(remainingInvoiceAmount);
									outWardGoodsService
											.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
								}
							}
						}

					}
				} else {
					OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = new OutWardOrderInvoiceEnteryRegistration();
					outWardOrderInvoiceEnteryRegistrationObj
							.setDispatchEnteryRegistrationStatus(0);
					outWardOrderInvoiceEnteryRegistrationObj
							.setGrossAmount(grossAmount[i]);
					// set invoice date
					java.util.Date str_invoiceDate = dtf.parse(invoiceDate[i]);
					java.sql.Date sqlInvoiceDate = new java.sql.Date(
							str_invoiceDate.getTime());
					outWardOrderInvoiceEnteryRegistrationObj
							.setInvoiceDate(sqlInvoiceDate);
					outWardOrderInvoiceEnteryRegistrationObj
							.setInvoiceNo(invoiceNo[i]);
					outWardOrderInvoiceEnteryRegistrationObj
							.setNetAmount(netAmount[i]);
					outWardOrderInvoiceEnteryRegistrationObj
							.setTaxAmout(taxAmount[i]);
					outWardOrderInvoiceEnteryRegistrationObj
							.setVatAmount(vatAmount[i]);
					outWardOrderInvoiceEnteryRegistrationObj
							.setOutWardOrderEnteryRegistration(outWardOrderEntryRegistrationObj);
					outWardOrderInvoiceEnteryRegistrationObj
							.setPaymentStatus(0);
					outWardOrderInvoiceEnteryRegistrationObj
							.setRemainingPayment(netAmount[i]);
					// set submit date
					java.util.Date today = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
					String IST = df.format(today);
					DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					java.util.Date date = df2.parse(IST);
					outWardOrderInvoiceEnteryRegistrationObj
							.setSubmitDate(date);
					outWardOrderInvoiceEnteryRegistrationObj.setStatus(1);
					// calculate no of days
					outWardOrderInvoiceEnteryRegistrationObj
							.setDiscountIsTakenOrNot(discount[i]);
					// load Stockist station
					if (discount[i].equalsIgnoreCase("Yes")) {
						// java.sql.Date sqlDate = new
						// java.sql.Date(date.getTime());
						outWardOrderInvoiceEnteryRegistrationObj.setNoOfDays(0);
						outWardOrderInvoiceEnteryRegistrationObj
								.setFirstDate(sqlInvoiceDate);
						outWardOrderInvoiceEnteryRegistrationObj
								.setLastDate(sqlInvoiceDate);
						System.out.println("Last Date : " + sqlInvoiceDate);
					} else {
						// java.sql.Date todaysDate = new
						// java.sql.Date(date.getTime());
						outWardOrderInvoiceEnteryRegistrationObj
								.setNoOfDays(noOfDays);
						outWardOrderInvoiceEnteryRegistrationObj
								.setFirstDate(sqlInvoiceDate);
						// calculate next date after some days
						Calendar cal = Calendar.getInstance();
						cal.setTime(str_invoiceDate);
						cal.add(Calendar.DATE, noOfDays); // minus number would
															// decrement the
															// days
						java.sql.Date afterDate = new java.sql.Date(
								cal.getTimeInMillis());
						outWardOrderInvoiceEnteryRegistrationObj
								.setLastDate(afterDate);
					}

					// save out Ward Order Invoice entry Registration Object
					outWardOrderInvoiceEnteryRegistrationObj
							.setOrganization(organizationObj);
					outWardGoodsService
							.saveOutWardOrderInvoiceEnteryRegistrationObject(outWardOrderInvoiceEnteryRegistrationObj);
				}

			}

			// update out Ward Order Entry Registration Object
			outWardOrderEntryRegistrationObj.setInvoiceStatus(1);
			outWardGoodsService
					.updateOutWardOrderEntryRegistration(outWardOrderEntryRegistrationObj);
			json = new JSONObject();
			json.put("MSG", "Invoice added successfully");
			json_data_array.put(json);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardOrderEntryRegistrationOfProductStatusOneAndInvoiceStatusIsOne", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardOrderEntryRegistrationOfProductStatusOneAndInvoiceStatusIsOne(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardOrderEntryRegistration> outWardOrderEntryRegistrations = outWardGoodsService
					.loadOutWardOrderEntryRegistrationOfProductStatusIsOneAndInvoiceStatusIsOne((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardOrderEntryRegistrations.size() > 0) {
				for (OutWardOrderEntryRegistration obj : outWardOrderEntryRegistrations) {
					json = new JSONObject();
					json.put("OutWardOrderEntryRegistrationId",
							obj.getOutWardOrderEnteryRegistrationId());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("ORGANIZATION_ID", obj.getOrganization()
							.getOrganizationId());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("ORDER_MODE", obj.getOrderMode()
							.getOrderModeName());
					json.put("DATE", obj.getOrderDate());
					json.put("ORDER_ID", obj.getOrderId());
					json.put("ORDER_NO", obj.getOrderNo());

					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationsList = outWardGoodsService
					.loadOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardOrderInvoiceEnteryRegistrationsList.size() > 0) {
				for (OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEnteryRegistrationsList) {
					json = new JSONObject();
					json.put("OUTWARD_INVOICE_ENTRY_REGISTRATION_ID",
							obj.getOutWardOrderInvoiceEnteryRegistrationId());
					json.put("ORGANIZATION", obj.getOrganization()
							.getOrganizationName());
					json.put("ORG_ID", obj.getOrganization()
							.getOrganizationId());
					json.put("INVOICE_NO", obj.getInvoiceNo());
					json.put("ORDER_NO", obj
							.getOutWardOrderEnteryRegistration().getOrderNo());
					json.put("ORDER_ID", obj
							.getOutWardOrderEnteryRegistration().getOrderId());
					json.put("STOCKIST", obj
							.getOutWardOrderEnteryRegistration().getStockist()
							.getStockistName());
					json.put("STOCKIST_ID", obj
							.getOutWardOrderEnteryRegistration().getStockist()
							.getStockistId());
					json.put("NET_AMOUNT", obj.getNetAmount());
					json.put("COMPANY", obj.getOutWardOrderEnteryRegistration()
							.getCompany().getCompanyName());
					json.put("COMPANY_ID", obj
							.getOutWardOrderEnteryRegistration().getCompany()
							.getCompanyId());
					json.put("ORDER_DATE", obj
							.getOutWardOrderEnteryRegistration().getOrderDate());
					json.put("INVOICE_DATE", obj.getInvoiceDate());
					json.put("DAYS_COUNT", obj.getNoOfDays());
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/loadSelectedOutWardOrderInvoiceEnteryRegistration", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String loadSelectedOutWardOrderInvoiceEnteryRegistration(
			@RequestParam(value = "selectedOutWardOrderInvoiceEnteryRegistrationId", required = true) String selectedOutWardOrderInvoiceEnteryRegistrationId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			System.out
					.println("selectedOutWardOrderInvoiceEnteryRegistrationId==="
							+ selectedOutWardOrderInvoiceEnteryRegistrationId);
			String[] selectedOutWardOrderInvoiceEnteryRegistrationIds = selectedOutWardOrderInvoiceEnteryRegistrationId
					.split(",");
			for (int i = 0; i < selectedOutWardOrderInvoiceEnteryRegistrationIds.length; i++) {
				// load Out Ward Order Invoice entry Registration
				OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = outWardGoodsService
						.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(Integer
								.parseInt(selectedOutWardOrderInvoiceEnteryRegistrationIds[i]));
				if (outWardOrderInvoiceEnteryRegistrationObj != null) {
					json = new JSONObject();
					json.put(
							"OUTWARD_INVOICE_ENTRY_REGISTRATION_ID",
							outWardOrderInvoiceEnteryRegistrationObj
									.getOutWardOrderInvoiceEnteryRegistrationId());
					json.put("ORGANIZATION",
							outWardOrderInvoiceEnteryRegistrationObj
									.getOrganization().getOrganizationName());
					json.put("ORG_ID", outWardOrderInvoiceEnteryRegistrationObj
							.getOrganization().getOrganizationId());
					json.put("INVOICE_NO",
							outWardOrderInvoiceEnteryRegistrationObj
									.getInvoiceNo());
					json.put("ORDER_NO",
							outWardOrderInvoiceEnteryRegistrationObj
									.getOutWardOrderEnteryRegistration()
									.getOrderNo());
					json.put("ORDER_ID",
							outWardOrderInvoiceEnteryRegistrationObj
									.getOutWardOrderEnteryRegistration()
									.getOrderId());
					json.put("STOCKIST",
							outWardOrderInvoiceEnteryRegistrationObj
									.getOutWardOrderEnteryRegistration()
									.getStockist().getStockistName());
					json.put("NET_AMOUNT",
							outWardOrderInvoiceEnteryRegistrationObj
									.getNetAmount());
					json.put("COMPANY",
							outWardOrderInvoiceEnteryRegistrationObj
									.getOutWardOrderEnteryRegistration()
									.getCompany().getCompanyName());
					json.put("ORDER_DATE",
							outWardOrderInvoiceEnteryRegistrationObj
									.getOutWardOrderEnteryRegistration()
									.getOrderDate());
					json.put("INVOICE_DATE",
							outWardOrderInvoiceEnteryRegistrationObj
									.getInvoiceDate());
					json.put("DAYS_COUNT",
							outWardOrderInvoiceEnteryRegistrationObj
									.getNoOfDays());
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/saveCases", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String saveCases(
			@RequestParam(value = "organizationId", required = true) Integer organizationId[],
			@RequestParam(value = "OutWardOrderInvoiceEnteryRegistrationId", required = true) Integer OutWardOrderInvoiceEnteryRegistrationId[],
			@RequestParam(value = "stockistId", required = true) Integer stockistId,
			@RequestParam(value = "companyId", required = true) Integer companyId,
			@RequestParam(value = "noOfCases", required = true) Integer noOfCases) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {

			OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationOldObj = outWardGoodsService
					.loadOutWardDispatchEnteryRegistrationObjUsigOrgIdAndStockistIdAndCompanyId(
							organizationId[0], stockistId, companyId);
			if (outWardDispatchEnteryRegistrationOldObj != null) {
				Integer newNoOfCases = outWardDispatchEnteryRegistrationOldObj
						.getNoOfCases() + noOfCases;
				outWardDispatchEnteryRegistrationOldObj
						.setNoOfCases(newNoOfCases);
				// update outWardDispatchEnteryRegistrationOldObj
				outWardGoodsService
						.updateOutWardDispatchEnteryRegistrationObj(outWardDispatchEnteryRegistrationOldObj);
				for (int i = 0; i < OutWardOrderInvoiceEnteryRegistrationId.length; i++) {
					OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = outWardGoodsService
							.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(OutWardOrderInvoiceEnteryRegistrationId[i]);
					outWardOrderInvoiceEnteryRegistrationObj
							.setOutWardDispatchEnteryRegistration(outWardDispatchEnteryRegistrationOldObj);
					outWardOrderInvoiceEnteryRegistrationObj
							.setDispatchEnteryRegistrationStatus(1);
					outWardGoodsService
							.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
				}
			} else {
				OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj = new OutWardDispatchEnteryRegistration();
				outWardDispatchEnteryRegistrationObj.setNoOfCases(noOfCases);
				outWardDispatchEnteryRegistrationObj.setStatus(1);
				outWardDispatchEnteryRegistrationObj.setTripStatus(0);
				outWardDispatchEnteryRegistrationObj.setGetPassStatus(0);
				outWardDispatchEnteryRegistrationObj.setPrintStickerStatus(0);
				// set submit date
				java.util.Date today = new java.util.Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
				df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
				String IST = df.format(today);
				DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
				java.util.Date date = df2.parse(IST);
				outWardDispatchEnteryRegistrationObj.setSubmitDate(date);
				// set orgnization
				outWardDispatchEnteryRegistrationObj
						.setOrganization(outWardGoodsService
								.loadOrganizationObjectUsingOrganiztionId(organizationId[0]));
				// set stockist
				outWardDispatchEnteryRegistrationObj
						.setStockist(outWardGoodsService
								.loadStockistObjectByStockistId(stockistId));
				// set company
				outWardDispatchEnteryRegistrationObj
						.setCompany(outWardGoodsService
								.loadCompanyObjectUsingCompanyId(companyId));
				// save Out Ward Dispatch entry Registration
				Integer OutWardDispatchEnteryRegistrationId = outWardGoodsService
						.saveOutWardDispatchEnteryRegistrationObj(outWardDispatchEnteryRegistrationObj);
				if (OutWardDispatchEnteryRegistrationId != null) {
					// load Out Ward Dispatch entry Registration object
					OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationSaveObj = outWardGoodsService
							.loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(OutWardDispatchEnteryRegistrationId);
					for (int i = 0; i < OutWardOrderInvoiceEnteryRegistrationId.length; i++) {
						OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = outWardGoodsService
								.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(OutWardOrderInvoiceEnteryRegistrationId[i]);
						outWardOrderInvoiceEnteryRegistrationObj
								.setOutWardDispatchEnteryRegistration(outWardDispatchEnteryRegistrationSaveObj);
						outWardOrderInvoiceEnteryRegistrationObj
								.setDispatchEnteryRegistrationStatus(1);
						outWardGoodsService
								.updateOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj);
					}
				}
			}
			json = new JSONObject();
			json.put("MSG", "Invoice Dispatch Successfully");
			json_data_array.put(json);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardDispatchEnteryRegistrationOfStatusOne", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardDispatchEnteryRegistrationOfStatusOne(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationsList = outWardGoodsService
					.loadOutWardDispatchEnteryRegistrationListOfStatusOne((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardDispatchEnteryRegistrationsList != null) {
				for (OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistrationsList) {
					json = new JSONObject();
					json.put("OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID",
							obj.getOutWardDispatchEnteryRegistrationId());
					json.put("NO_OF_CASES", obj.getNoOfCases());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("TRIP_STATUS", obj.getTripStatus());
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();

	}

	@RequestMapping(value = "/searchOutwardViewDispatchEntryRegistrationList", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardViewDispatchEntryRegistrationList(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {

		// JSONArray json_data_array= null;
		JSONArray outwardDispEntryArr = null;
		JSONObject json = null;

		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationsList1 = null;
		try {
			// json_data_array = new JSONArray();
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			Map<String, Object> map = outWardGoodsService
					.searchOutwardViewDispatchEntryRegistrationList(
							selectedValue, searchText, fromSpecialData,
							toSpecialData, from, organizationId);
			outWardDispatchEnteryRegistrationsList1 = (List<OutWardDispatchEnteryRegistration>) map
					.get("outWardDispatchEnteryRegistration");
			Integer totalPages = (Integer) map.get("totalPages");
			outwardDispEntryArr = new JSONArray();
			if (outWardDispatchEnteryRegistrationsList1 != null) {
				for (OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistrationsList1) {
					json = new JSONObject();
					json.put("OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID",
							obj.getOutWardDispatchEnteryRegistrationId());
					json.put("ORG_ID", obj.getOrganization()
							.getOrganizationId());
					json.put("NO_OF_CASES", obj.getNoOfCases());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("paginationCount", totalPages);
					outwardDispEntryArr.put(json);
				}
			}
			// json=new JSONObject();
			// json.put("paginationCount",totalPages);
			// outwardDispEntryArr.put(json);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return outwardDispEntryArr.toString();
	}

	@RequestMapping(value = "/searchOutwardViewDispatchEntryPrintStickerList", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardViewDispatchEntryPrintStickerList(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {

		// JSONArray json_data_array= null;
		JSONArray outwardDispEntryArr = null;
		JSONObject json = null;

		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationsList1 = null;
		try {
			// json_data_array = new JSONArray();
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			Map<String, Object> map = outWardGoodsService
					.searchOutwardViewDispatchEntryPrintStickerList(
							selectedValue, searchText, fromSpecialData,
							toSpecialData, from, organizationId);
			outWardDispatchEnteryRegistrationsList1 = (List<OutWardDispatchEnteryRegistration>) map
					.get("outWardDispatchEnteryRegistration");
			Integer totalPages = (Integer) map.get("totalPages");
			outwardDispEntryArr = new JSONArray();
			if (outWardDispatchEnteryRegistrationsList1 != null) {
				for (OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistrationsList1) {
					json = new JSONObject();
					json.put("OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID",
							obj.getOutWardDispatchEnteryRegistrationId());
					json.put("ORG_ID", obj.getOrganization()
							.getOrganizationId());
					json.put("NO_OF_CASES", obj.getNoOfCases());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("paginationCount", totalPages);
					outwardDispEntryArr.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return outwardDispEntryArr.toString();
	}

	@RequestMapping(value = "/getOutWardOrderEntryRegistrationOfTripStatusZero", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardOrderEntryRegistrationOfTripStatusZero(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationsList = outWardGoodsService
					.loadOutWardDispatchEnteryRegistrationListOfTripStatusZero((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardDispatchEnteryRegistrationsList != null) {
				for (OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistrationsList) {
					json = new JSONObject();
					json.put("DispatchEnteryRegistrationId",
							obj.getOutWardDispatchEnteryRegistrationId());
					json.put("ORG_ID", obj.getOrganization()
							.getOrganizationId());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPNAY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("NO_OF_CASES", obj.getNoOfCases());
					json.put("SUBMIT_DATE", obj.getSubmitDate());
					json.put("TRANSPORTER", obj.getStockist()
							.getTransporterDetails().getTransporterName());

					// load all invoice no usign DispatchEnteryRegistrationId

					List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService
							.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(obj
									.getOutWardDispatchEnteryRegistrationId());
					if (outWardOrderInvoiceEnteryRegistrationList.size() > 0) {
						JSONArray json_data_array_for_invoice = new JSONArray();
						JSONObject json_invoice = null;
						for (OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj : outWardOrderInvoiceEnteryRegistrationList) {
							json_invoice = new JSONObject();
							json_invoice.put("INVOICE_NO",
									outWardOrderInvoiceEnteryRegistrationObj
											.getInvoiceNo());
							json_data_array_for_invoice.put(json_invoice);
						}
						json.put("INVOICE_DETAILS", json_data_array_for_invoice);
					}
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json_data_array.toString();
	}

	@SuppressWarnings("null")
	@RequestMapping(value = "/saveTripEntry", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String saveTripEntry(
			@RequestParam(value = "organizationId", required = true) Integer organizationId,
			@RequestParam(value = "transporterId", required = true) Integer transporterId[],
			@RequestParam(value = "cartingAgentId", required = true) Integer cartingAgentId,
			@RequestParam(value = "loadingCharges", required = true) Double loadingCharges,
			@RequestParam(value = "dispatchBy", required = true) String dispatchBy,
			@RequestParam(value = "VehicleNo", required = true) String VehicleNo,
			@RequestParam(value = "DriverNo", required = true) String DriverNo,
			@RequestParam(value = "totalNoOfCases", required = true) Integer totalNoOfCases,
			@RequestParam(value = "DispatchEnteryRegistrationId", required = true) String DispatchEnteryRegistrationId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			CartingAgent cartingAgentObj = outWardGoodsService
					.getCartingAgentDetailsById(cartingAgentId);
			OutWardTrip outWardTripObj = new OutWardTrip();
			outWardTripObj.setDispatchBy(dispatchBy);
			outWardTripObj.setDriverNo(DriverNo);
			// outWardTripObj.setLoadingCharges(loadingCharges);

			if (loadingCharges != null) {
				outWardTripObj.setLoadingCharges(loadingCharges);
			} else {
				outWardTripObj.setLoadingCharges(0.0);
			}

			outWardTripObj.setNoOfCases(totalNoOfCases);
			outWardTripObj.setStatus(1);
			outWardTripObj.setVehicleNo(VehicleNo);
			// set submit date
			Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			String IST = df.format(today);
			DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			Date date = df2.parse(IST);
			outWardTripObj.setSubmitDate(date);

			// set organization
			Organization organizationObj = outWardGoodsService
					.loadOrganizationObjectUsingOrganiztionId(organizationId);
			outWardTripObj.setOrganization(organizationObj);
			// set carting agent
			outWardTripObj.setCartingAgent(cartingAgentObj);

			// save out ward trip object
			Integer outWardTripId = outWardGoodsService
					.saveOutWardTripObj(outWardTripObj);
			if (outWardTripId != null) {
				// load outward trip object
				OutWardTrip saveOutWardTripObj = outWardGoodsService
						.loadOutWardTripObjUsignOutWardTripiD(outWardTripId);

				// update trip no
				String tripNo = "TRIP_" + outWardTripId;
				saveOutWardTripObj.setTripNo(tripNo);
				outWardGoodsService.updateOutWardTripObj(saveOutWardTripObj);

				// save outward trip transporter details
				for (int t = 0; t < transporterId.length; t++) {
					// load transporter details object
					OutWardTripTransporterDetails outWardTripTransporterDetailsObj = new OutWardTripTransporterDetails();
					outWardTripTransporterDetailsObj
							.setOutWardTrip(saveOutWardTripObj);
					TransporterDetails transporterDetailsObj = outWardGoodsService
							.loadTransporterDetailsUsingTransporterDetailsId(transporterId[t]);
					outWardTripTransporterDetailsObj
							.setTransporterDetails(transporterDetailsObj);
					// save OutWardTripTransporterDetails
					outWardGoodsService
							.saveOutWardTripTransporterDetailsObj(outWardTripTransporterDetailsObj);
				}

				// update dispatch entry object
				String[] DispatchEnteryRegistrationIdArray = DispatchEnteryRegistrationId
						.split(",");
				for (int i = 0; i < DispatchEnteryRegistrationIdArray.length; i++) {
					OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj = outWardGoodsService
							.loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(Integer
									.parseInt(DispatchEnteryRegistrationIdArray[i]));
					outWardDispatchEnteryRegistrationObj.setTripStatus(1);
					outWardDispatchEnteryRegistrationObj
							.setOutWardTrip(saveOutWardTripObj);
					outWardGoodsService
							.updateOutWardDispatchEnteryRegistrationObj(outWardDispatchEnteryRegistrationObj);
				}

				// increment org trip count
				OrganizationTripCount organizationTripCountObj = outWardGoodsService
						.loadOrganizationTripCountObjUsignOrgId(organizationId);
				if (organizationTripCountObj != null) {
					Integer tripcount = organizationTripCountObj.getTripCount() + 1;
					organizationTripCountObj.setTripCount(tripcount);
					// update org trip count
					outWardGoodsService
							.updateOrganizationTripCount(organizationTripCountObj);
				} else {
					Integer tripcount = 1;
					// insert org trip count
					OrganizationTripCount newOrganizationTripCountObj = new OrganizationTripCount();
					newOrganizationTripCountObj.setTripCount(tripcount);
					newOrganizationTripCountObj
							.setOrganization(organizationObj);
					outWardGoodsService
							.saveOrganizationTripCountObj(newOrganizationTripCountObj);
				}

				// increment carting agent trip count
				CartingAgentTripCount cartingAgentTripCountObj = outWardGoodsService
						.loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(
								organizationId, cartingAgentId);
				if (cartingAgentTripCountObj != null) {
					Integer cartingAgentTripCount = cartingAgentTripCountObj
							.getTripCount() + 1;
					cartingAgentTripCountObj
							.setTripCount(cartingAgentTripCount);
					// update cartingAgentTripCount
					outWardGoodsService
							.updateCartingAgentTripCountObj(cartingAgentTripCountObj);
				} else {
					Integer cartingAgentTripCount = 1;
					CartingAgentTripCount newCartingAgentTripCountObj = new CartingAgentTripCount();
					newCartingAgentTripCountObj
							.setTripCount(cartingAgentTripCount);
					newCartingAgentTripCountObj
							.setOrganization(organizationObj);
					newCartingAgentTripCountObj
							.setCartingAgent(cartingAgentObj);
					outWardGoodsService
							.saveCartingAgentTripCountObj(newCartingAgentTripCountObj);
				}
				json = new JSONObject();
				json.put("TRIP_NO", tripNo);
				json.put("MSG", "Trip Entry Added Successfully");
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardTripEntryOfStatusOne", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardTripEntryOfStatusOne(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardTrip> outWardTripList = outWardGoodsService
					.loadOutWardTripListOfStatusOneUsignOrganizationId((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardTripList.size() > 0) {
				for (OutWardTrip obj : outWardTripList) {
					json = new JSONObject();
					json.put("OUTWARD_TRIP_ID", obj.getOutWardTripId());
					json.put("TRIP_NO", obj.getTripNo());
					json.put("LOADING_CHARGES", obj.getLoadingCharges());
					json.put("ORG_NAME", obj.getOrganization()
							.getOrganizationName());
					// load out ward trip transporter detail list usign outward
					// trip id
					List<OutWardTripTransporterDetails> outWardTripTransporterDetailList = outWardGoodsService
							.loadOutWardTripTransporterDetailsListUsignOutWardTripId(obj
									.getOutWardTripId());
					if (outWardTripTransporterDetailList.size() > 0) {
						json.put("TRAN", outWardTripTransporterDetailList
								.get(0).getTransporterDetails()
								.getTransporterName());
					} else {
						json.put("TRAN", "-");
					}
					json.put("DISPATCH_BY", obj.getDispatchBy());
					json.put("VEHICAL_NO", obj.getVehicleNo());
					json.put("DRIVER_NO", obj.getDriverNo());
					json.put("NO_OF_CASES", obj.getNoOfCases());
					json.put("CARTING_AGENT", obj.getCartingAgent().getName());
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardTripEntryOfStatusOneAndGetPassStatusZero", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardTripEntryOfStatusOneAndGetPassStatusZero(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardTrip> outWardTripList = outWardGoodsService
					.loadOutWardTripListOfGetPassStatusZeroAndStatusOneUsignOrganizationId((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardTripList.size() > 0) {
				for (OutWardTrip obj : outWardTripList) {
					json = new JSONObject();
					json.put("OUTWARD_TRIP_ID", obj.getOutWardTripId());
					json.put("TRIP_NO", obj.getTripNo());
					json.put("LOADING_CHARGES", obj.getLoadingCharges());
					json.put("ORG_NAME", obj.getOrganization()
							.getOrganizationName());
					json.put("ORG_ID", obj.getOrganization()
							.getOrganizationId());
					// json.put("TRAN",
					// obj.getTransporterDetails().getTransporterName());
					json.put("DISPATCH_BY", obj.getDispatchBy());
					json.put("VEHICAL_NO", obj.getVehicleNo());
					json.put("DRIVER_NO", obj.getDriverNo());
					json.put("NO_OF_CASES", obj.getNoOfCases());
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/saveGetPassLR", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String saveGetPassLR(
			@RequestParam(value = "organizationId", required = true) Integer organizationId,
			@RequestParam(value = "outwardDispatchEntryRegId", required = true) Integer outwardDispatchEntryRegId,
			@RequestParam(value = "LRNo", required = true) String LRNo,
			@RequestParam(value = "LRImage") MultipartFile LRImage[]) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		List<OutWardGatePassLrImagePath> lrDocList = null;
		OutWardGatePassLrImagePath tempLrDocObj = null;
		try {
			OutWardGatePass outWardGatePassObj = new OutWardGatePass();
			outWardGatePassObj.setLRNo(LRNo);
			outWardGatePassObj.setStatus(1);
			// set submit date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			String IST = df.format(today);
			DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			java.util.Date date = df2.parse(IST);
			outWardGatePassObj.setSubmitDate(date);
			lrDocList = new ArrayList<OutWardGatePassLrImagePath>();
			// write lr image
			for (MultipartFile file : LRImage) {
				if (file != null) {
					tempLrDocObj = new OutWardGatePassLrImagePath();
					tempLrDocObj.setImagePath(CommonMethods
							.writeDocumentAndGetFileDB_Path(file));
					lrDocList.add(tempLrDocObj);
				}
			}
			outWardGatePassObj.setOutWardGatePassLrImagePathList(lrDocList);
			// set organization
			outWardGatePassObj.setOrganization(outWardGoodsService
					.loadOrganizationObjectUsingOrganiztionId(organizationId));
			// save out ward get pass object
			Integer outWardGatePassId = outWardGoodsService
					.saveOutWardGatePass(outWardGatePassObj);
			if (outWardGatePassId != null) {
				// load save out ward get pass object
				OutWardGatePass saveOutWardGatePassObj = outWardGoodsService
						.loadOutWardGatePassObjUsignOutWardGatePassId(outWardGatePassId);
				// update outward dispatch entry object
				OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj = outWardGoodsService
						.loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(outwardDispatchEntryRegId);
				// OutWardTrip
				// outWardTripObj=outWardGoodsService.loadOutWardTripObjUsignOutWardTripiD(tripId);
				outWardDispatchEnteryRegistrationObj.setGetPassStatus(1);
				outWardDispatchEnteryRegistrationObj
						.setOutWardGatePass(saveOutWardGatePassObj);
				outWardGoodsService
						.updateOutWardDispatchEnteryRegistrationObj(outWardDispatchEnteryRegistrationObj);
				json = new JSONObject();
				json.put("MSG", "LR added successfully");
				json_data_array.put(json);

			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardDispatchEntryOfGetPassStatusOne", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardDispatchEntryOfGetPassStatusOne(
			HttpServletRequest request) {
		JSONArray json_data_array = null;
		// JSONArray json_data_array= new JSONArray();
		// JSONObject json=null;
		try {
			json_data_array = outWardGoodsService
					.getOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			/*
			 * List<OutWardDispatchEnteryRegistration>
			 * outWardDispatchEnteryRegistration=outWardGoodsService.
			 * loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne
			 * ((Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
			 * if(outWardDispatchEnteryRegistration.size()>0) {
			 * for(OutWardDispatchEnteryRegistration obj :
			 * outWardDispatchEnteryRegistration) { json=new JSONObject();
			 * json.put("OUTWARD_DISPATCH_ENTERY_REG_ID",
			 * obj.getOutWardDispatchEnteryRegistrationId());
			 * json.put("TRIP_NO", obj.getOutWardTrip().getTripNo()); //
			 * json.put
			 * ("LOADING_CHARGES",obj.getOutWardTrip().getLoadingCharges());
			 * json.put("ORG_NAME",
			 * obj.getOrganization().getOrganizationName()); json.put("ORG_ID",
			 * obj.getOrganization().getOrganizationId()); json.put("COMPANY",
			 * obj.getCompany().getCompanyName()); json.put("STOCKIST",
			 * obj.getStockist().getStockistName()); json.put("TRAN",
			 * obj.getStockist().getTransporterDetails().getTransporterName());
			 * // json.put("DISPATCH_BY",obj.getOutWardTrip().getDispatchBy());
			 * // json.put("VEHICAL_NO",obj.getOutWardTrip().getVehicleNo()); //
			 * json.put("DRIVER_NO", obj.getOutWardTrip().getDriverNo());
			 * json.put("NO_OF_CASES", obj.getNoOfCases()); // json.put("LR_NO",
			 * obj.getOutWardGatePass().getLRNo()); json.put("LR_IMAGE_PATH",
			 * obj.getOutWardGatePass().getLrImagePath());
			 * json.put("OUTWARD_GATEPASS_ID",
			 * obj.getOutWardGatePass().getOutWardGatePassId());
			 * json.put("LR_NO", obj.getOutWardGatePass().getLRNo());
			 * 
			 * // load all invoice no String invoiceNo="";
			 * List<OutWardOrderInvoiceEnteryRegistration>
			 * outWardOrderInvoiceEnteryRegistrationList=outWardGoodsService.
			 * getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId
			 * (obj.getOutWardDispatchEnteryRegistrationId());
			 * if(outWardOrderInvoiceEnteryRegistrationList.size()>0) {
			 * for(OutWardOrderInvoiceEnteryRegistration ooierObj :
			 * outWardOrderInvoiceEnteryRegistrationList) {
			 * invoiceNo+=ooierObj.getInvoiceNo()+","; } }
			 * json.put("INVOICE_NO", invoiceNo); json_data_array.put(json); } }
			 */
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardOrderEntryRegistrationListOfStatusOne", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardOrderEntryRegistrationListOfStatusOne(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardOrderEntryRegistration> outWardOrderEntryRegistrations = outWardGoodsService
					.loadOutWardOrderEntryRegistrationOfStatusIsOne((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardOrderEntryRegistrations.size() > 0) {
				for (OutWardOrderEntryRegistration obj : outWardOrderEntryRegistrations) {
					json = new JSONObject();
					json.put("OutWardOrderEntryRegistrationId",
							obj.getOutWardOrderEnteryRegistrationId());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("ORANIZATION_ID", obj.getOrganization()
							.getOrganizationId());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("COMPANY_ID", obj.getCompany().getCompanyId());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					// json.put("ORDER_MODE",obj.getOrderMode().getOrderModeName());
					json.put("DATE", obj.getOrderDate());
					json.put("ORDER_ID", obj.getOrderId());
					json.put("ORDER_NO", obj.getOrderNo());
					json.put("INVOICE_STATUS", obj.getInvoiceStatus());
					// get discount percentages
					String stockistLocation = obj.getStockist()
							.getStockistLocation();
					if (stockistLocation.equalsIgnoreCase("Local")) {
						CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
								.loadCompanyCreditAndDiscountObjectUsignComapnyId(obj
										.getCompany().getCompanyId());
						json.put("PERCENTAGE", companyCreditAndDiscountObj
								.getLocalPercentage());
					} else {
						CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
								.loadCompanyCreditAndDiscountObjectUsignComapnyId(obj
										.getCompany().getCompanyId());
						json.put("PERCENTAGE", companyCreditAndDiscountObj
								.getOutsideStationPercentage());
					}
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getStockistUsignDispatchEnteryRegistrationId", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getStockistUsignDispatchEnteryRegistrationId(
			HttpServletRequest request,
			@RequestParam(value = "dispatchEnteryRegistrationId", required = true) String dispatchEnteryRegistrationId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			String dispatchEnteryRegistrationIdArray[] = dispatchEnteryRegistrationId
					.split(",");
			for (int i = 0; i < dispatchEnteryRegistrationIdArray.length; i++) {
				OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj = outWardGoodsService
						.loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(Integer
								.parseInt(dispatchEnteryRegistrationIdArray[i]));
				if (outWardDispatchEnteryRegistrationObj != null) {
					json = new JSONObject();
					json.put("TRANSPORTER",
							outWardDispatchEnteryRegistrationObj.getStockist()
									.getTransporterDetails()
									.getTransporterName());
					json.put("TRANSPORTER_ID",
							outWardDispatchEnteryRegistrationObj.getStockist()
									.getTransporterDetails()
									.getTransporterDetailsId());
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOrganizationTripCountObjUsignOrgId", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOrganizationTripCountObjUsignOrgId(
			@RequestParam(value = "organizationId", required = true) Integer organizationId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			OrganizationTripCount organizationTripCountObjCount = outWardGoodsService
					.loadOrganizationTripCountObjUsignOrgId(organizationId);
			if (organizationTripCountObjCount != null) {
				json = new JSONObject();
				json.put("ORG_TRIP_COUNT",
						organizationTripCountObjCount.getTripCount());
				json_data_array.put(json);
			} else {
				json = new JSONObject();
				json.put("ORG_TRIP_COUNT", 0);
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getCartingAgentTripCountObjUsignOrgIdAndCartingAgentId", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(
			@RequestParam(value = "organizationId", required = true) Integer organizationId,
			@RequestParam(value = "cartingAgentId", required = true) Integer cartingAgentId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			CartingAgent cartingAgentObj = outWardGoodsService
					.getCartingAgentDetailsById(cartingAgentId);
			CartingAgentTripCount cartingAgentTripCountObj = outWardGoodsService
					.loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(
							organizationId, cartingAgentId);
			if (cartingAgentTripCountObj != null) {
				json = new JSONObject();
				json.put("CARTING_AGENT_TRIP_COUNT",
						cartingAgentTripCountObj.getTripCount());
				json.put("VEHICLE_NO", cartingAgentObj.getVehicleNumber());
				json.put("DRIVER_NO", cartingAgentObj.getMobileNo());
				json_data_array.put(json);
			} else {
				json = new JSONObject();
				json.put("CARTING_AGENT_TRIP_COUNT", 0);
				json.put("VEHICLE_NO", cartingAgentObj.getVehicleNumber());
				json.put("DRIVER_NO", cartingAgentObj.getMobileNo());
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardDispatchEntryListOfStatusOneAndTripStatusOne", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String getOutWardDispatchEntryListOfStatusOneAndTripStatusOne(
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList = outWardGoodsService
					.loadOutWardDispatchEnteryRegistrationListOfStatusOneAndTripStatusOne((Integer) request
							.getSession().getAttribute("ORGANIZATION_ID"));
			if (outWardDispatchEnteryRegistrationList.size() > 0) {
				for (OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistrationList) {
					json = new JSONObject();
					json.put("TRIP_NO", obj.getOutWardTrip().getTripNo());
					json.put("ORG_NAME", obj.getOrganization()
							.getOrganizationName());
					json.put("ORG_ID", obj.getOrganization()
							.getOrganizationId());
					// json.put("TRAN",
					// obj.getTransporterDetails().getTransporterName());
					json.put("NO_OF_CASES", obj.getNoOfCases());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("OUTWARD_DISPATCH_ENTRY_REG_ID",
							obj.getOutWardDispatchEnteryRegistrationId());
					json.put("DISPATCH_BY", obj.getOutWardTrip()
							.getDispatchBy());
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/ViewAllDetailsOfOutwardGoodsRegistration", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String ViewAllDetailsOfOutwardGoodsRegistration(
			@RequestParam(value = "OutWardOrderEntryRegistrationId", required = true) Integer OutWardOrderEntryRegistrationId,
			HttpServletRequest request) {
		JSONArray json_data_array = new JSONArray();
		/*
		 * JSONArray json_cheque_array= new JSONArray(); JSONArray
		 * json_invoice_array= new JSONArray(); JSONObject json=null; JSONObject
		 * jsonCheque = null; JSONObject jsonInvoice = null;
		 */
		try {
			json_data_array = outWardGoodsService
					.viewAllDetailsOfOutwardGoodsRegistration(OutWardOrderEntryRegistrationId);
			/*
			 * CommonMethods common = new CommonMethods();
			 * OutWardOrderEntryRegistration
			 * outWardOrderEntryRegistrationObj=outWardGoodsService.
			 * loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId
			 * (OutWardOrderEntryRegistrationId);
			 * if(outWardOrderEntryRegistrationObj!=null) { json=new
			 * JSONObject(); json.put("ORG_NAME",
			 * outWardOrderEntryRegistrationObj
			 * .getOrganization().getOrganizationName()); json.put("COMP_NAME",
			 * outWardOrderEntryRegistrationObj.getCompany().getCompanyName());
			 * json.put("ORDER_NUM",
			 * outWardOrderEntryRegistrationObj.getOrderNo());
			 * 
			 * String [] reg_date_arr =
			 * outWardOrderEntryRegistrationObj.getOrderDate
			 * ().toString().split("-"); json.put("DATE",
			 * reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
			 * 
			 * json.put("MODE",
			 * outWardOrderEntryRegistrationObj.getOrderMode().getOrderModeName
			 * ()); json.put("ORDER_MODE_ID",outWardOrderEntryRegistrationObj.
			 * getOrderMode().getOrderModeId()); json.put("REMARK",
			 * outWardOrderEntryRegistrationObj.getRemark());
			 * json.put("ORDER_COPY",
			 * outWardOrderEntryRegistrationObj.getOrderCopyPath());
			 * if(outWardOrderEntryRegistrationObj.getStockist()!= null) {
			 * json.put("STOCKIST",
			 * outWardOrderEntryRegistrationObj.getStockist(
			 * ).getStockistName()); } else{ json.put("STOCKIST", "-"); }
			 * json.put("SUBMITTED_DATE",
			 * common.submittedDateFormateIn12Hrs(outWardOrderEntryRegistrationObj
			 * .getSubmitDate().toString()));
			 * 
			 * if(outWardOrderEntryRegistrationObj.getProductStatus()==1) {
			 * List<OutWardProductOrderEntryRegistration>
			 * outWardProductOrderEntryRegistrationList = outWardGoodsService.
			 * getProductOrderEnteryRegistrationDetailsUsingOutWardOrderEntryRegistrationId
			 * (OutWardOrderEntryRegistrationId);
			 * if(outWardProductOrderEntryRegistrationList.size()>0) {
			 * for(OutWardProductOrderEntryRegistration obj :
			 * outWardProductOrderEntryRegistrationList) { jsonCheque = new
			 * JSONObject(); jsonCheque.put("PRODUCT_NAME",
			 * obj.getCompanyProduct().getCompanyProductName());
			 * jsonCheque.put("QUANTITY", obj.getQuantity());
			 * jsonCheque.put("OUTWARD_PRODUCT_ORDER_ENRTY_REG_ID",
			 * obj.getOutWardProductOrderEnteryRegistrationId());
			 * json_cheque_array.put(jsonCheque); } } }
			 * 
			 * List<OutWardOrderInvoiceEnteryRegistration>
			 * outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService.
			 * getOutWardOrderInvoiceEnteryRegistrationUsignoutWardOrderEnteryRegistrationId
			 * (OutWardOrderEntryRegistrationId);
			 * if(outWardOrderInvoiceEnteryRegistrationList.size() > 0) {
			 * for(OutWardOrderInvoiceEnteryRegistration obj :
			 * outWardOrderInvoiceEnteryRegistrationList) { jsonInvoice = new
			 * JSONObject();
			 * jsonInvoice.put("OUTWARD_ORDER_INVOICE_ENTRY_REG_ID",
			 * obj.getOutWardOrderInvoiceEnteryRegistrationId());
			 * jsonInvoice.put("INVOICE_NO", obj.getInvoiceNo());
			 * jsonInvoice.put("INVOICE_DATE", obj.getInvoiceDate());
			 * jsonInvoice.put("GROSS_AMOUNT", obj.getGrossAmount());
			 * jsonInvoice.put("TAX_AMOUNT", obj.getTaxAmout());
			 * jsonInvoice.put("VAT_AMOUNT", obj.getVatAmount());
			 * jsonInvoice.put("DISCOUNT", obj.getDiscountIsTakenOrNot());
			 * jsonInvoice.put("NET_AMOUNT", obj.getNetAmount());
			 * if(obj.getOutWardDispatchEnteryRegistration()==null) {
			 * jsonInvoice.put("DISPATCH_STATUS", "No"); } else {
			 * jsonInvoice.put("DISPATCH_STATUS", "Yes"); }
			 * 
			 * json_invoice_array.put(jsonInvoice); } }
			 * json.put("PRODUCT_ARRAY", json_cheque_array);
			 * json.put("INVOICE_ARRAY", json_invoice_array);
			 * json_data_array.put(json); }
			 */
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/ViewAllCasesOfDispatchEntryRegistration", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String ViewAllCasesOfDispatchEntryRegistration(
			@RequestParam(value = "OutWardDispatchEnteryRegistrationId", required = true) Integer OutWardDispatchEnteryRegistrationId,
			HttpServletRequest request) {
		System.out.println(OutWardDispatchEnteryRegistrationId);
		JSONArray json_data_array = new JSONArray();
		JSONArray json_invoice_array = new JSONArray();
		JSONObject json = null;
		JSONObject jsonInvoice = null;
		try {
			CommonMethods common = new CommonMethods();
			OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationIdObj = outWardGoodsService
					.loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(OutWardDispatchEnteryRegistrationId);
			if (outWardDispatchEnteryRegistrationIdObj != null) {
				json = new JSONObject();
				json.put("ORG_NAME", outWardDispatchEnteryRegistrationIdObj
						.getOrganization().getOrganizationName());
				json.put("COMP_NAME", outWardDispatchEnteryRegistrationIdObj
						.getCompany().getCompanyName());
				json.put("STOCKIST", outWardDispatchEnteryRegistrationIdObj
						.getStockist().getStockistName());
				json.put("NO_OF_CASES",
						outWardDispatchEnteryRegistrationIdObj.getNoOfCases());

				json.put(
						"SUBMITTED_DATE",
						common.submittedDateFormateIn12Hrs(outWardDispatchEnteryRegistrationIdObj
								.getSubmitDate().toString()));

				List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService
						.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(OutWardDispatchEnteryRegistrationId);

				if (outWardOrderInvoiceEnteryRegistrationList.size() > 0) {
					for (OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEnteryRegistrationList) {
						jsonInvoice = new JSONObject();
						jsonInvoice.put("INVOICE_NO", obj.getInvoiceNo());
						jsonInvoice.put("ORDER_NO", obj
								.getOutWardOrderEnteryRegistration()
								.getOrderNo());
						jsonInvoice.put("ORDER_ID", obj
								.getOutWardOrderEnteryRegistration()
								.getOrderId());
						jsonInvoice.put("ORDER_DATE", obj
								.getOutWardOrderEnteryRegistration()
								.getOrderDate());
						jsonInvoice.put("INVOICE_DATE", obj.getInvoiceDate());
						jsonInvoice.put("DAYS_COUNT", obj.getNoOfDays());
						jsonInvoice.put("NET_AMOUNT", obj.getNetAmount());
						json_invoice_array.put(jsonInvoice);
					}
				}
				json.put("INVOICE_ARRAY", json_invoice_array);
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/ViewTripDetails", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String ViewTripDetails(
			@RequestParam(value = "tripEntryId", required = true) Integer tripEntryId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load outward trip object usign trip entry id
			OutWardTrip outWardTripObj = outWardGoodsService
					.loadOutWardTripObjUsignOutWardTripiD(tripEntryId);
			CommonMethods commonObj = new CommonMethods();
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList = outWardGoodsService
					.loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(tripEntryId);
			if (outWardDispatchEnteryRegistrationList.size() > 0) {
				for (OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistrationList) {
					json = new JSONObject();
					json.put("TRANSPORTER", obj.getStockist()
							.getTransporterDetails().getTransporterName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("COMPNAY", obj.getCompany().getCompanyName());
					json.put("NO_OF_CASES", obj.getNoOfCases());
					json.put("TRIP_DATE", commonObj
							.submittedDateFormateIn12Hrs(outWardTripObj
									.getSubmitDate().toString()));
					// load invoice no usign outward dispatch reg id

					double invoiceAmount = 0;
					List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService
							.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(obj
									.getOutWardDispatchEnteryRegistrationId());
					if (outWardOrderInvoiceEnteryRegistrationList.size() > 0) {
						JSONArray json_data_array_for_invoice = new JSONArray();
						JSONObject json_invoice = null;
						for (OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj : outWardOrderInvoiceEnteryRegistrationList) {
							json_invoice = new JSONObject();
							json_invoice.put("INVOICE_NO",
									outWardOrderInvoiceEnteryRegistrationObj
											.getInvoiceNo());
							json_data_array_for_invoice.put(json_invoice);
							invoiceAmount = invoiceAmount
									+ outWardOrderInvoiceEnteryRegistrationObj
											.getNetAmount();
						}
						json.put("INVOICE_DETAILS", json_data_array_for_invoice);
					}
					DecimalFormat df = new DecimalFormat("#.##");
					json.put("PAYBLE_AMOUNT",
							Double.valueOf(df.format(invoiceAmount)));
					// json.put("TOTAL_NO_OF_CASES",outWardTripObj.getNoOfCases());
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();

	}

	@RequestMapping(value = "/PrintTripDetailsReport", method = RequestMethod.GET)
	public ModelAndView printTripDetailsReport(
			@RequestParam(value = "outwardTripId", required = true) Integer outwardTripId,
			ModelAndView modelAndView, HttpServletResponse response) {
		try {
			// load outward trip object usign trip entry id
			DecimalFormat decimalFormat = new DecimalFormat("#.0000");
			List<OutwardTripReport> printedOutwardTripReportObj = new ArrayList<OutwardTripReport>();
			Integer transporterTotalCase = 0;
			OutWardTrip outWardTripObj = outWardGoodsService
					.loadOutWardTripObjUsignOutWardTripiD(outwardTripId);
			List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList = outWardGoodsService
					.loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(outwardTripId);
			CommonMethods commonObj = new CommonMethods();
			if (outWardDispatchEnteryRegistrationList.size() > 0) {
				OutwardTripReport outwardTripReportObj = new OutwardTripReport();
				for (OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistrationList) {
					outwardTripReportObj.setTransporter(obj.getStockist()
							.getTransporterDetails().getTransporterName());
					outwardTripReportObj.setStockist(obj.getStockist()
							.getStockistName());
					outwardTripReportObj.setCompany(obj.getCompany()
							.getCompanyName());
					outwardTripReportObj.setNoOfCases(obj.getNoOfCases());
					transporterTotalCase += obj.getNoOfCases();
					outwardTripReportObj.setOrgnization(outWardTripObj
							.getOrganization().getOrganizationName());
					// load invoice no usign outward dispatch reg id
					List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService
							.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(obj
									.getOutWardDispatchEnteryRegistrationId());
					String invoiceNo = "";
					double invoicePaybleAmount = 0;
					if (outWardOrderInvoiceEnteryRegistrationList.size() > 0) {
						for (OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj : outWardOrderInvoiceEnteryRegistrationList) {
							invoiceNo = invoiceNo
									+ outWardOrderInvoiceEnteryRegistrationObj
											.getInvoiceNo() + ",";
							invoicePaybleAmount = invoicePaybleAmount
									+ outWardOrderInvoiceEnteryRegistrationObj
											.getNetAmount();
						}
					}
					outwardTripReportObj.setInvoiceNo(invoiceNo);
					String str_invoicePaybleAmount = decimalFormat
							.format(invoicePaybleAmount);
					outwardTripReportObj
							.setInvoicePaybleAmount(str_invoicePaybleAmount);
					outwardTripReportObj.setOutWardTrip(outWardTripObj);
					outwardTripReportObj.setTripDate(commonObj
							.submittedDateFormateIn12Hrs(outWardTripObj
									.getSubmitDate().toString()));
					// set trip details
					outwardTripReportObj.setTripNo(outWardTripObj.getTripNo());
					outwardTripReportObj.setTotalTripCases(outWardTripObj
							.getNoOfCases());
					outwardTripReportObj.setCartingAgent(outWardTripObj
							.getCartingAgent().getName());
					outwardTripReportObj.setVehicleNo(outWardTripObj
							.getVehicleNo());
					outwardTripReportObj.setDispatchBy(outWardTripObj
							.getDispatchBy());
					outwardTripReportObj.setDriverNo(outWardTripObj
							.getDriverNo());
					String str_loadingCharges = decimalFormat
							.format(outWardTripObj.getLoadingCharges());
					outwardTripReportObj.setLoadingCharges(str_loadingCharges);
					Integer outwardTripReportId = outWardGoodsService
							.saveOutwardTripReport(outwardTripReportObj);

				}
			}

			// load all jasper data of out ward trip id
			List<OutwardTripReport> OutwardTripReportList = outWardGoodsService
					.loadOutwardTripReportListUsignOutWardTripId(outwardTripId);
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(
					OutwardTripReportList);
			parameterMap.put("datasource", JRdataSource);
			modelAndView = new ModelAndView("pdfReport", parameterMap);
			// delete printedOutwardTripReportObjList
			printedOutwardTripReportObj.addAll(OutwardTripReportList);
			outWardGoodsService
					.deletePrintedOutwardTripReportObj(printedOutwardTripReportObj);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return modelAndView;
	}

	@RequestMapping(value = "/printStickerDetailsReport", method = RequestMethod.GET)
	public ModelAndView printStickerDetailsReport(
			@RequestParam(value = "outWardDispatchEnteryRegistrationId", required = true) String multipleOutWardDispatchEnteryRegistrationId,
			ModelAndView modelAndView, HttpServletResponse response) {
		try {
			// load out
			List<PrintSticker> printStickerListObj = new ArrayList<PrintSticker>();
			List<PrintSticker> printStickerList = new ArrayList<PrintSticker>();
			String[] outWardDispatchEnteryRegistrationIds = multipleOutWardDispatchEnteryRegistrationId
					.split(",");
			for (int j = 0; j < outWardDispatchEnteryRegistrationIds.length; j++) {
				OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj = outWardGoodsService
						.loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(Integer
								.parseInt(outWardDispatchEnteryRegistrationIds[j]));
				if (outWardDispatchEnteryRegistrationObj != null) {
					PrintSticker printStickerObj = new PrintSticker();
					printStickerObj
							.setStockist(outWardDispatchEnteryRegistrationObj
									.getStockist().getStockistName());
					printStickerObj
							.setStockistAddress(outWardDispatchEnteryRegistrationObj
									.getStockist().getStockistAddress());
					printStickerObj
							.setTransporter(outWardDispatchEnteryRegistrationObj
									.getStockist().getTransporterDetails()
									.getTransporterName());
					printStickerObj
							.setMobileNo(outWardDispatchEnteryRegistrationObj
									.getStockist().getStockistMobileNo()
									.toString());
					printStickerObj
							.setNoOfCases(outWardDispatchEnteryRegistrationObj
									.getNoOfCases());
					printStickerObj
							.setOrgnaization(outWardDispatchEnteryRegistrationObj
									.getOrganization().getOrganizationName());
					printStickerObj
							.setStockistCity(outWardDispatchEnteryRegistrationObj
									.getStockist().getCity().getCityName());
					// load all invoice no
					String invoiceNo = "";
					List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService
							.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(Integer
									.parseInt(outWardDispatchEnteryRegistrationIds[j]));
					if (outWardOrderInvoiceEnteryRegistrationList.size() > 0) {
						for (OutWardOrderInvoiceEnteryRegistration ooierObj : outWardOrderInvoiceEnteryRegistrationList) {
							invoiceNo += ooierObj.getInvoiceNo() + ",";
						}
					}

					printStickerObj.setInvoiceNo(invoiceNo);
					printStickerObj
							.setOutWardDispatchEnteryRegistration(outWardDispatchEnteryRegistrationObj);
					// save print sticker object

					Integer printStickerId = outWardGoodsService
							.savePrintSticker(printStickerObj);
					if (printStickerId != null) {
						// load print sticker object using
						// outWardDispatchEnteryRegistrationId
						PrintSticker savePrintStickerObj = outWardGoodsService
								.loadPrintStickerObjUsingOutWardDispatchEnteryRegistrationId(Integer
										.parseInt(outWardDispatchEnteryRegistrationIds[j]));
						printStickerListObj.add(savePrintStickerObj);
						for (int i = 0; i < savePrintStickerObj.getNoOfCases(); i++) {
							PrintSticker obj = new PrintSticker();
							obj = savePrintStickerObj;
							printStickerList.add(obj);
						}

						// update print sticker status
						outWardDispatchEnteryRegistrationObj
								.setPrintStickerStatus(1);
						outWardGoodsService
								.updateOutWardDispatchEnteryRegistrationObj(outWardDispatchEnteryRegistrationObj);
					}
				}
			}
			// printStickerList.add(savePrintStickerObj);
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(
					printStickerList);
			parameterMap.put("datasource", JRdataSource);
			modelAndView = new ModelAndView("stickerReport", parameterMap);

			// delete print sticker entry

			for (PrintSticker obj : printStickerListObj) {
				outWardGoodsService.deletePrintStickerObj(obj);
				;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return modelAndView;
	}

	@RequestMapping(value = "/checkCompanyCreditAndDiscountSetOrNot", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String checkCompanyCreditAndDiscountSetOrNot(
			@RequestParam(value = "companyId", required = true) Integer companyId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			CompanyCreditAndDiscount companyCreditAndDiscountObjAndDiscount = outWardGoodsService
					.loadCompanyCreditAndDiscountObjectUsignComapnyId(companyId);
			if (companyCreditAndDiscountObjAndDiscount != null) {
				json = new JSONObject();
				json.put("STATUS", "true");
				json_data_array.put(json);
			} else {
				json = new JSONObject();
				json.put("STATUS", "false");
				json.put("MSG",
						"Please add selected company credit and discount");
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/editOutwardGoodsOrderEntryRegistartion", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String editOutwardGoodsOrderEntryRegistartion(
			@RequestParam(value = "outWardOrderEntryRegistrationId", required = true) Integer outWardOrderEntryRegistrationId,
			@RequestParam(value = "Remark", required = true) String remark,
			@RequestParam(value = "orderDate", required = true) String orderDate,
			@RequestParam(value = "orderNo", required = true) String orderNo,
			@RequestParam(value = "orderModeName", required = true) Integer orderModeName
	// ,@RequestParam(value="orderCopy",required=false) MultipartFile orderCopy
	) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load OutwardGoodsOrderEntryRegistartion object
			OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj = outWardGoodsService
					.loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(outWardOrderEntryRegistrationId);
			if (outWardOrderEntryRegistrationObj != null) {
				DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
				java.util.Date d1 = dtf.parse(orderDate);
				outWardOrderEntryRegistrationObj
						.setOrderDate((new java.sql.Date(d1.getTime())));
				outWardOrderEntryRegistrationObj.setRemark(remark);
				outWardOrderEntryRegistrationObj.setOrderNo(orderNo);
				/*
				 * if(orderCopy!=null) {
				 * outWardOrderEntryRegistrationObj.setOrderCopyPath
				 * (CommonMethods.writeDocumentAndGetFileDB_Path(orderCopy)); }
				 */
				boolean status = outWardGoodsService
						.updateOutWardOrderEntryRegistrationObj(
								outWardOrderEntryRegistrationObj, orderModeName);
				if (status == true) {
					json = new JSONObject();
					json.put("MSG", "Order Entry updated successfully");
					/*
					 * json.put("ORDER_DATE",
					 * outWardOrderEntryRegistrationObj.getOrderDate());
					 * json.put("ORDER_NO",
					 * outWardOrderEntryRegistrationObj.getOrderNo());
					 * json.put("RESULT", true);
					 */
					json_data_array.put(json);
				} else {
					json = new JSONObject();
					json.put("MSG", "Order Entry not updated");
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	//
	@RequestMapping(value = "/updateOutwardGoodsRegOrderCopyImage", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String updateOutwardGoodsRegOrderCopyImage(
			@RequestParam(value = "orderCopy", required = true) MultipartFile LR_File[],
			@RequestParam(value = "registrationId", required = true) Integer outwardGoodsRegistrationId)
			throws IOException {
		JSONArray json_ImageArray = null;
		JSONObject result = new JSONObject();
		List<OutWardOrderEntryRegistrationOrderCopyPath> orderCopyImgList = null;
		OutWardOrderEntryRegistrationOrderCopyPath tempOrderCopyImgPath = null;
		try {
			result.put("MSG", "File Upload failed...!");
			if (LR_File != null && !(LR_File.length > 0)) {
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			orderCopyImgList = new ArrayList<OutWardOrderEntryRegistrationOrderCopyPath>();
			for (MultipartFile file : LR_File) {
				if (file != null) {
					tempOrderCopyImgPath = new OutWardOrderEntryRegistrationOrderCopyPath();
					tempOrderCopyImgPath.setImagePath(CommonMethods
							.writeDocumentAndGetFileDB_Path(file));
					orderCopyImgList.add(tempOrderCopyImgPath);
				}
			}
			json_ImageArray = outWardGoodsService
					.updateOutwardGoodsRegOrderCopyImage(
							outwardGoodsRegistrationId, orderCopyImgList);
			result.put("MSG", "Outward Order Copy File Upload successful...!");
			result.put("ORDER_COPY", json_ImageArray);
			result.put("RESULT", true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}

	//
	@RequestMapping(value = "/deleteOutwardGoodsRegistrationOrderCopyImage", method = RequestMethod.POST)
	private @ResponseBody String deleteOutwardGoodsRegistrationOrderCopyImage(
			@RequestParam(value = "orderCopyImageId", required = false) Integer orderCopyImageId) {
		JSONObject result = null;
		try {
			result = new JSONObject();
			if (outWardGoodsService
					.deleteOutwardGoodsRegistrationOrderCopyImage(orderCopyImageId)) {
				result.put("RESULT", true);
				result.put("MSG",
						"Outward Goods Registration Order Copy Image Deleted Successfully");
			} else {
				result.put("MSG",
						"Inward Goods Registration Order Copy Image Deletion Failed");
			}
		} catch (Exception e) {
			try {
				e.printStackTrace();
				log.error(e);
				result = new JSONObject();
				result.put("MSG", "Operation Failed");
			} catch (Exception e1) {
			}
		}
		return result.toString();
	}

	@RequestMapping(value = "/updatetOutWardOrderProduct", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String updatetOutWardOrderProduct(
			@RequestParam(value = "OutWardProductOrderEnteryRegistrationId", required = true) Integer OutWardProductOrderEnteryRegistrationId,
			@RequestParam(value = "quantity", required = true) Integer quantity) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj = outWardGoodsService
					.loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(OutWardProductOrderEnteryRegistrationId);
			outWardProductOrderEntryRegistrationObj.setQuantity(quantity);
			boolean status = outWardGoodsService
					.updateOutWardProductOrderEntryRegistrationObj(outWardProductOrderEntryRegistrationObj);
			if (status == true) {
				json = new JSONObject();
				json.put("MSG", "Product quantity update successfully");
				json.put("OutWardProductOrderEnteryRegistrationId",
						OutWardProductOrderEnteryRegistrationId);
				json.put("quantity", quantity);
				json_data_array.put(json);
			} else {
				json = new JSONObject();
				json.put("MSG", "Product quantity not updated successfully");
				json.put("OutWardProductOrderEnteryRegistrationId",
						OutWardProductOrderEnteryRegistrationId);
				json.put("quantity", quantity);
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardOrderProductQuantity", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String getOutWardOrderProductQuantity(
			@RequestParam(value = "OutWardProductOrderEnteryRegistrationId", required = true) Integer OutWardProductOrderEnteryRegistrationId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj = outWardGoodsService
					.loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(OutWardProductOrderEnteryRegistrationId);
			if (outWardProductOrderEntryRegistrationObj != null) {
				json = new JSONObject();
				json.put("QUANTITY",
						outWardProductOrderEntryRegistrationObj.getQuantity());
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/editOutWardTrip", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String editOutWardTrip(
			@RequestParam(value = "tripEntryId", required = true) Integer tripEntryId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load outward trip entry object

			OutWardTrip outWardTripObj = outWardGoodsService
					.loadOutWardTripObjUsignOutWardTripiD(tripEntryId);
			if (outWardTripObj != null) {
				json = new JSONObject();
				json.put("ORG", outWardTripObj.getOrganization()
						.getOrganizationName());
				json.put("ORG_ID", outWardTripObj.getOrganization()
						.getOrganizationId());
				json.put("ORG_TRIP_COUNT", outWardTripObj.getOrganization()
						.getOrganizationTripCount().getTripCount());
				json.put("CARTING_AGENT_ID", outWardTripObj.getCartingAgent()
						.getCartingAgentId());
				json.put("CARTING_AGENT_TRIP_COUNT", outWardTripObj
						.getCartingAgent().getCartingAgentTripCount()
						.getTripCount());
				json.put("LOADING_CHARGES", outWardTripObj.getLoadingCharges());
				json.put("DISPATCH_BY", outWardTripObj.getDispatchBy());
				json.put("VEHICLE_NO", outWardTripObj.getVehicleNo());
				json.put("DRIVER_NO", outWardTripObj.getDriverNo());
				json.put("NO_OF_CASES", outWardTripObj.getNoOfCases());
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/updateOutWardTrip", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String updateOutWardTrip(
			@RequestParam(value = "tripEntryId", required = true) Integer tripEntryId,
			@RequestParam(value = "oldCartingAgentId", required = true) Integer oldCartingAgentId,
			@RequestParam(value = "organizationId", required = true) Integer organizationId,
			@RequestParam(value = "cartingAgentId", required = true) Integer selectcartingAgentId,
			@RequestParam(value = "loadingCharges", required = true) Double loadingCharges,
			@RequestParam(value = "dispatchBy", required = true) String dispatchBy,
			@RequestParam(value = "VehicleNo", required = true) String VehicleNo,
			@RequestParam(value = "DriverNo", required = true) String DriverNo) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load outward trip object using ourward trip id
			OutWardTrip outWardTripObj = outWardGoodsService
					.loadOutWardTripObjUsignOutWardTripiD(tripEntryId);
			// check carting agent change or not
			outWardTripObj.setLoadingCharges(loadingCharges);

			if (loadingCharges == null) {
				outWardTripObj.setLoadingCharges(0.0);
			} else {
				outWardTripObj.setLoadingCharges(loadingCharges);
			}

			outWardTripObj.setDispatchBy(dispatchBy);
			outWardTripObj.setVehicleNo(VehicleNo);
			outWardTripObj.setDriverNo(DriverNo);

			json = new JSONObject();
			json.put("MSG", "Trip entry updation failed....!");
			if (outWardGoodsService.updateOutWardTripDetails(
					selectcartingAgentId, outWardTripObj)) {
				json.put("MSG", "Trip entry updated successfully.....!");
			}
			json_data_array.put(json);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/updateLrDetails", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String updateLrDetails(
			@RequestParam(value = "OutWardGatePassId", required = true) Integer OutWardGatePassId,
			@RequestParam(value = "lrno", required = false) String lrno
	// ,@RequestParam(value="lrImage",required=false) MultipartFile lrImage
	) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load OutWardGatePass obj using OutWardGatePassId
			OutWardGatePass outWardGatePassObj = outWardGoodsService
					.loadOutWardGatePassObjUsignOutWardGatePassId(OutWardGatePassId);
			outWardGatePassObj.setLRNo(lrno);
			/*
			 * if(lrImage!=null) {
			 * outWardGatePassObj.setLrImagePath(CommonMethods
			 * .writeDocumentAndGetFileDB_Path(lrImage)); }
			 */
			json = new JSONObject();
			json.put("MSG", "Opreation failed.........!");
			if (outWardGoodsService
					.updateOutWardGatePassObj(outWardGatePassObj)) {
				json.put("MSG", "LR Details updated successfully.........!");
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	//
	@RequestMapping(value = "/updateOutwardGoodsGatePassLrImage", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String updateOutwardGoodsGatePassLrImage(
			@RequestParam(value = "lrImage", required = true) MultipartFile LR_File[],
			@RequestParam(value = "registrationId", required = true) Integer outwardGatePassId)
			throws IOException {
		JSONArray json_ImageArray = null;
		JSONObject result = new JSONObject();
		List<OutWardGatePassLrImagePath> lrImgList = null;
		OutWardGatePassLrImagePath tempLrImgPath = null;
		try {
			result.put("MSG", "File Upload failed...!");
			if (LR_File != null && !(LR_File.length > 0)) {
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			lrImgList = new ArrayList<OutWardGatePassLrImagePath>();
			for (MultipartFile file : LR_File) {
				if (file != null) {
					tempLrImgPath = new OutWardGatePassLrImagePath();
					tempLrImgPath.setImagePath(CommonMethods
							.writeDocumentAndGetFileDB_Path(file));
					lrImgList.add(tempLrImgPath);
				}
			}
			json_ImageArray = outWardGoodsService
					.updateOutwardGoodsGatePassLrImage(outwardGatePassId,
							lrImgList);
			result.put("MSG", "Outward Gate Pass LR File Upload successful...!");
			result.put("LR_IMAGE_PATH", json_ImageArray);
			result.put("RESULT", true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}

	//
	@RequestMapping(value = "/deleteOutwardGoodsGatePassLrImage", method = RequestMethod.POST)
	private @ResponseBody String deleteOutwardGoodsGatePassLrImage(
			@RequestParam(value = "lrImageId", required = false) Integer lrImageId) {
		JSONObject result = null;
		try {
			result = new JSONObject();
			if (outWardGoodsService
					.deleteOutwardGoodsGatePassLrImage(lrImageId)) {
				result.put("RESULT", true);
				result.put("MSG",
						"Outward Gate Pass Registration LR Image Deleted Successfully");
			} else {
				result.put("MSG",
						"Outward Gate Pass Registration LR Image Deletion Failed");
			}
		} catch (Exception e) {
			try {
				e.printStackTrace();
				log.error(e);
				result = new JSONObject();
				result.put("MSG", "Operation Failed");
			} catch (Exception e1) {
			}
		}
		return result.toString();
	}

	@RequestMapping(value = "/updateNoOfCases", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String updateNoOfCases(
			@RequestParam(value = "OutWardDispatchEnteryRegistrationId", required = true) Integer OutWardDispatchEnteryRegistrationId,
			@RequestParam(value = "noOfCases", required = true) Integer noOfCases) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load OutWardDispatchEnteryRegistration obj using
			// OutWardDispatchEnteryRegistrationId
			json = new JSONObject();
			json.put("MSG", "Opreation failed.........!");
			if (outWardGoodsService
					.updateOutWardDispatchEnteryRegistrationObjDetails(
							OutWardDispatchEnteryRegistrationId, noOfCases)) {
				json.put("MSG", "NO of cases updated successfully..........!");
				json.put("OutWardDispatchEnteryRegistrationId",
						OutWardDispatchEnteryRegistrationId);
				json.put("noOfCases", noOfCases);
				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/getOutWardOrderInvoiceEnteryRegistrationObj", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String getOutWardOrderInvoiceEnteryRegistrationObj(
			@RequestParam(value = "outWardOrderInvoiceEnteryRegistrationId", required = true) Integer outWardOrderInvoiceEnteryRegistrationId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = outWardGoodsService
					.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationId);
			if (outWardOrderInvoiceEnteryRegistrationObj != null) {
				json = new JSONObject();
				json.put("INVOICE_NO",
						outWardOrderInvoiceEnteryRegistrationObj.getInvoiceNo());
				String[] reg_date_arr = outWardOrderInvoiceEnteryRegistrationObj
						.getInvoiceDate().toString().split("-");
				json.put("INVOICE_DATE", reg_date_arr[1] + "/"
						+ reg_date_arr[2] + "/" + reg_date_arr[0]);
				if (outWardOrderInvoiceEnteryRegistrationObj.getGrossAmount() == null)
					json.put("GROSS_AMOUNT", "");
				else
					json.put("GROSS_AMOUNT",
							outWardOrderInvoiceEnteryRegistrationObj
									.getGrossAmount());
				if (outWardOrderInvoiceEnteryRegistrationObj.getTaxAmout() != null)
					json.put("TAX_AMOUNT",
							outWardOrderInvoiceEnteryRegistrationObj
									.getTaxAmout());
				else
					json.put("TAX_AMOUNT", "");
				if (outWardOrderInvoiceEnteryRegistrationObj.getVatAmount() != null)
					json.put("VAT_AMOUNT",
							outWardOrderInvoiceEnteryRegistrationObj
									.getVatAmount());
				else
					json.put("VAT_AMOUNT", "");
				json.put("DISCOUNT_YES_OR_NO",
						outWardOrderInvoiceEnteryRegistrationObj
								.getDiscountIsTakenOrNot());
				json.put("NET_AMOUNT",
						outWardOrderInvoiceEnteryRegistrationObj.getNetAmount());
				json.put("OutWardOrderEnteryRegistrationId",
						outWardOrderInvoiceEnteryRegistrationObj
								.getOutWardOrderEnteryRegistration()
								.getOutWardOrderEnteryRegistrationId());

				// get discount percentages
				String stockistLocation = outWardOrderInvoiceEnteryRegistrationObj
						.getOutWardOrderEnteryRegistration().getStockist()
						.getStockistLocation();
				if (stockistLocation.equalsIgnoreCase("Local")) {
					CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
							.loadCompanyCreditAndDiscountObjectUsignComapnyId(outWardOrderInvoiceEnteryRegistrationObj
									.getOutWardOrderEnteryRegistration()
									.getCompany().getCompanyId());
					json.put("PERCENTAGE",
							companyCreditAndDiscountObj.getLocalPercentage());
				} else {
					CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
							.loadCompanyCreditAndDiscountObjectUsignComapnyId(outWardOrderInvoiceEnteryRegistrationObj
									.getOutWardOrderEnteryRegistration()
									.getCompany().getCompanyId());
					json.put("PERCENTAGE", companyCreditAndDiscountObj
							.getOutsideStationPercentage());
				}

				json_data_array.put(json);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/updateInvoiceEntry", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String updateInvoiceEntry(
			@RequestParam(value = "outWardOrderInvoiceEnteryRegistrationId", required = true) Integer outWardOrderInvoiceEnteryRegistrationId,
			@RequestParam(value = "invoiceNo", required = true) String invoiceNo,
			@RequestParam(value = "invoiceDate", required = true) String invoiceDate,
			@RequestParam(value = "grossAmount", required = true) Double grossAmount,
			@RequestParam(value = "taxAmount", required = true) Double taxAmount,
			@RequestParam(value = "vatAmount", required = true) Double vatAmount,
			@RequestParam(value = "discount", required = true) String discount,
			@RequestParam(value = "netAmount", required = true) Double netAmount) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load outWardOrderInvoiceEnteryRegistration obj using
			// outWardOrderInvoiceEnteryRegistrationId
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = outWardGoodsService
					.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationId);
			if (outWardOrderInvoiceEnteryRegistrationObj != null) {
				outWardOrderInvoiceEnteryRegistrationObj
						.setInvoiceNo(invoiceNo);
				outWardOrderInvoiceEnteryRegistrationObj
						.setGrossAmount(grossAmount);
				outWardOrderInvoiceEnteryRegistrationObj.setTaxAmout(taxAmount);
				outWardOrderInvoiceEnteryRegistrationObj
						.setVatAmount(vatAmount);
				outWardOrderInvoiceEnteryRegistrationObj
						.setDiscountIsTakenOrNot(discount);
				// outWardOrderInvoiceEnteryRegistrationObj.setNetAmount(netAmount);
				// update invoice entry obj
				json = new JSONObject();
				json.put("MSG", "Operation Falied..........!");
				if (outWardGoodsService.updateInvoiceObj(
						outWardOrderInvoiceEnteryRegistrationObj, invoiceDate,
						discount, netAmount)) {
					json.put("MSG", "Invoice Updated successfully....!");
					json.put("outWardOrderInvoiceEnteryRegistrationId",
							outWardOrderInvoiceEnteryRegistrationId);
					json.put("INVOICE_NO", invoiceNo);
					json.put("GROSS_AMOUNT", grossAmount);
					json.put("TAX_AMOUNT", taxAmount);
					json.put("VAT_AMOUNT", vatAmount);
					json.put("NET_AMOUNT", netAmount);
					DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					java.util.Date str_invoiceDate = dtf.parse(invoiceDate);
					java.sql.Date selectedInvoiceDate = new java.sql.Date(
							str_invoiceDate.getTime());
					json.put("INVOICE_DATE", selectedInvoiceDate);
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	// ///// Vijay
	@RequestMapping(value = "/searchOutwardOrderEntryRegistration", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardOrderEntryRegistration(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {

		JSONObject result = null;
		JSONArray outwardOrdEntryArr = null;
		JSONArray json_data_array = new JSONArray();
		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistration = null;
		try {
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");
			Map<String, Object> map = outWardGoodsService
					.searchOutwardOrderEntryRegistration(selectedValue,
							searchText, fromSpecialData, toSpecialData, from,
							organizationId);
			outWardOrderEntryRegistration = (List<OutWardOrderEntryRegistration>) map
					.get("outWardOrderEntryRegistration");
			Integer totalPages = (Integer) map.get("totalPages");
			outwardOrdEntryArr = new JSONArray();

			for (OutWardOrderEntryRegistration tempOrderEntry : outWardOrderEntryRegistration) {
				result = new JSONObject();
				result.put("OutWardOrderEntryRegistrationId",
						tempOrderEntry.getOutWardOrderEnteryRegistrationId());
				result.put("ORG", tempOrderEntry.getOrganization()
						.getOrganizationName());
				result.put("COMPANY", tempOrderEntry.getCompany()
						.getCompanyName());
				result.put("COMPANY_ID", tempOrderEntry.getCompany()
						.getCompanyId());
				result.put("STOCKIST", tempOrderEntry.getStockist()
						.getStockistName());
				result.put("ORDER_MODE", tempOrderEntry.getOrderMode()
						.getOrderModeName());
				result.put("DATE", tempOrderEntry.getOrderDate());
				result.put("ORDER_ID", tempOrderEntry.getOrderId());
				result.put("ORDER_NO", tempOrderEntry.getOrderNo());
				outwardOrdEntryArr.put(result);

			}
			result = new JSONObject();
			result.put("paginationCount", totalPages);
			outwardOrdEntryArr.put(result);
		} catch (Exception e) {
			log.error(e);
		}
		return outwardOrdEntryArr.toString();
	}

	@RequestMapping(value = "/deleteLrDetails", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String deleteLrDetails(
			@RequestParam(value = "OUTWARD_GATEPASS_ID", required = true) Integer OUTWARD_GATEPASS_ID) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load outwardgatePass obj using outwardgatePass id
			OutWardGatePass outWardGatePassObj = outWardGoodsService
					.loadOutWardGatePassObjUsignOutWardGatePassId(OUTWARD_GATEPASS_ID);
			if (outWardGoodsService
					.updateOutWardDispatchObjOfOutWardGetPassStatusZero(outWardGatePassObj)) {
				if (outWardGoodsService.deleteLrDetailsObj(outWardGatePassObj)) {
					json = new JSONObject();
					json.put("MSG", "LR Details Deleted Successfully.........!");
					json_data_array.put(json);
				} else {
					json = new JSONObject();
					json.put("MSG", "Opreation failed.........!");
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/deleteTripEntry", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String deleteTripEntry(
			@RequestParam(value = "tripEntryId", required = true) Integer tripEntryId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			json = new JSONObject();
			// load out ward trip obj using outward trip id
			OutWardTrip outWardTripObj = outWardGoodsService
					.loadOutWardTripObjUsignOutWardTripiD(tripEntryId);
			if (outWardTripObj != null) {
				if (outWardGoodsService
						.updateOutWardDispatchObjOfOutWardTripStatusZero(outWardTripObj)) {
					if (outWardGoodsService
							.deleteOutWardTripEntryObj(outWardTripObj)) {

						json.put("MSG",
								"Trip Entry Deleted Successfully.........!");
						json_data_array.put(json);
					} else {

						json.put("MSG", "Opreation failed.........!");
						json_data_array.put(json);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/deleteOutWardOrderProduct", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String deleteOutWardOrderProduct(
			@RequestParam(value = "OutWardProductOrderEnteryRegistrationId", required = true) Integer OutWardProductOrderEnteryRegistrationId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load ward product order entry obj using
			// OutWardProductOrderEnteryRegistrationId
			OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj = outWardGoodsService
					.loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(OutWardProductOrderEnteryRegistrationId);
			if (outWardProductOrderEntryRegistrationObj != null) {
				if (outWardGoodsService
						.updateOutWardOrderEntryRegistrationObjOfProductStatusZero(outWardProductOrderEntryRegistrationObj)) {

					if (outWardGoodsService
							.deleteOutWardProductOrderEntryRegistrationObj(outWardProductOrderEntryRegistrationObj)) {
						json = new JSONObject();
						json.put("MSG",
								"Product Deleted Successfully.........!");
						json.put("OutWardProductOrderEnteryRegistrationId",
								OutWardProductOrderEnteryRegistrationId);
						json_data_array.put(json);
					} else {
						json = new JSONObject();
						json.put("MSG", "Opreation failed.........!");
						json_data_array.put(json);
					}
				} else {
					json = new JSONObject();
					json.put("MSG", "Opreation failed.........!");
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/deleteOutWardOrderInvoiceEntry", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String deleteOutWardOrderInvoiceEntry(
			@RequestParam(value = "OUTWARD_ORDER_INVOICE_ENTRY_REG_ID", required = true) Integer OUTWARD_ORDER_INVOICE_ENTRY_REG_ID) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			// load outward order invoice entry object using
			// outWardInvoiceEntryId
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = outWardGoodsService
					.loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(OUTWARD_ORDER_INVOICE_ENTRY_REG_ID);

			// check net amount and remaining amount are equals or not
			if ((outWardOrderInvoiceEnteryRegistrationObj.getNetAmount()
					.equals(outWardOrderInvoiceEnteryRegistrationObj
							.getRemainingPayment()) && outWardOrderInvoiceEnteryRegistrationObj
					.getPaymentStatus() == 0)) {
				// check no of invoice present for outWard order entry
				List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService
						.getOutWardOrderInvoiceEnteryRegistrationUsignoutWardOrderEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationObj
								.getOutWardOrderEnteryRegistration()
								.getOutWardOrderEnteryRegistrationId());
				if (outWardOrderInvoiceEnteryRegistrationList.size() == 1) {
					// update invoice status to zero in outward order entry
					// registration obj
					OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj = outWardOrderInvoiceEnteryRegistrationObj
							.getOutWardOrderEnteryRegistration();
					outWardOrderEntryRegistrationObj.setInvoiceStatus(0);
					outWardGoodsService
							.updateOutWardOrderEntryRegistration(outWardOrderEntryRegistrationObj);

					// delete invoice entry
					if (outWardGoodsService
							.deleteOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj)) {
						json = new JSONObject();
						json.put("MSG", "Invoice Delete Successfully....!");
						json.put("OUTWARD_ORDER_INVOICE_ENTRY_REG_ID",
								OUTWARD_ORDER_INVOICE_ENTRY_REG_ID);
						json_data_array.put(json);
					} else {
						json = new JSONObject();
						json.put("MSG", "Opreation failed.........!");
						json_data_array.put(json);
					}
				} else {
					// delete invoice entry
					if (outWardGoodsService
							.deleteOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj)) {
						json = new JSONObject();
						json.put("MSG", "Invoice Delete Successfully....!");
						json.put("OUTWARD_ORDER_INVOICE_ENTRY_REG_ID",
								OUTWARD_ORDER_INVOICE_ENTRY_REG_ID);
						json_data_array.put(json);
					} else {
						json = new JSONObject();
						json.put("MSG", "Opreation failed.........!");
						json_data_array.put(json);
					}
				}
			} else {
				// load all invoice payment information
				List<InvoicePaymentInformation> invoicePaymentInformationList = outWardGoodsService
						.loadinInvoicePaymentInformationsListUsingOutWardOrderInvoiceEnteryRegistrationId(OUTWARD_ORDER_INVOICE_ENTRY_REG_ID);
				if (invoicePaymentInformationList.size() > 0) {
					for (InvoicePaymentInformation obj : invoicePaymentInformationList) {
						// add paid invoice payment in remaining payment
						RemainingChequeAmount remainingChequeAmountObjAmount = outWardGoodsService
								.loadRemainingChequeAmountObjUsignChequeNumberInfoId(obj
										.getChequeNumberInfo()
										.getChequeNumberInfoId());
						if (remainingChequeAmountObjAmount != null) {
							remainingChequeAmountObjAmount
									.setRemainingChequeAmount(remainingChequeAmountObjAmount
											.getRemainingChequeAmount()
											+ obj.getPaidAmount());
							outWardGoodsService
									.updateRemainingChequeAmountObj(remainingChequeAmountObjAmount);
						} else {
							RemainingChequeAmount newremainingChequeAmountObj = new RemainingChequeAmount();
							newremainingChequeAmountObj.setStatus(1);
							newremainingChequeAmountObj
									.setChequeNumberInfo(outWardGoodsService
											.loadChequeNumberInfoObjUsignChequeNumberInfoId(obj
													.getChequeNumberInfo()
													.getChequeNumberInfoId()));
							newremainingChequeAmountObj
									.setCompany(outWardGoodsService
											.loadCompanyObjectUsingCompanyId(obj
													.getChequeNumberInfo()
													.getAdvancedChequeInformation()
													.getCompany()
													.getCompanyId()));
							newremainingChequeAmountObj
									.setOrganization(outWardGoodsService
											.loadOrganizationObjectUsingOrganiztionId(obj
													.getChequeNumberInfo()
													.getAdvancedChequeInformation()
													.getOrganization()
													.getOrganizationId()));
							newremainingChequeAmountObj
									.setStockist(outWardGoodsService
											.loadStockistObjUsigStokistId(obj
													.getChequeNumberInfo()
													.getAdvancedChequeInformation()
													.getStockist()
													.getStockistId()));
							newremainingChequeAmountObj
									.setStockistBankDetails(outWardGoodsService
											.loadStockistBankDetailsUsignStockistBankDetailsId(obj
													.getChequeNumberInfo()
													.getAdvancedChequeInformation()
													.getStockistBankDetails()
													.getStockistBankDetailsId()));
							newremainingChequeAmountObj
									.setRemainingChequeAmount(obj
											.getPaidAmount());
							outWardGoodsService
									.saveRemainingChequeAmountObj(newremainingChequeAmountObj);
						}
					}
				}
				// check no of invoice present for outWard order entry
				List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService
						.getOutWardOrderInvoiceEnteryRegistrationUsignoutWardOrderEnteryRegistrationId(outWardOrderInvoiceEnteryRegistrationObj
								.getOutWardOrderEnteryRegistration()
								.getOutWardOrderEnteryRegistrationId());
				if (outWardOrderInvoiceEnteryRegistrationList.size() == 1) {
					// update invoice status to zero in outward order entry
					// registration obj
					OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj = outWardOrderInvoiceEnteryRegistrationObj
							.getOutWardOrderEnteryRegistration();
					outWardOrderEntryRegistrationObj.setInvoiceStatus(0);
					outWardGoodsService
							.updateOutWardOrderEntryRegistration(outWardOrderEntryRegistrationObj);

					// delete invoice entry
					if (outWardGoodsService
							.deleteOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj)) {
						json = new JSONObject();
						json.put("MSG", "Invoice Delete Successfully....!");
						json.put("OUTWARD_ORDER_INVOICE_ENTRY_REG_ID",
								OUTWARD_ORDER_INVOICE_ENTRY_REG_ID);
						json_data_array.put(json);
					} else {
						json = new JSONObject();
						json.put("MSG", "Opreation failed.........!");
						json_data_array.put(json);
					}
				} else {
					// delete invoice entry
					if (outWardGoodsService
							.deleteOutWardOrderInvoiceEnteryRegistrationObj(outWardOrderInvoiceEnteryRegistrationObj)) {
						json = new JSONObject();
						json.put("MSG", "Invoice Delete Successfully....!");
						json.put("OUTWARD_ORDER_INVOICE_ENTRY_REG_ID",
								OUTWARD_ORDER_INVOICE_ENTRY_REG_ID);
						json_data_array.put(json);
					} else {
						json = new JSONObject();
						json.put("MSG", "Opreation failed.........!");
						json_data_array.put(json);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/deleteCases", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String deleteCases(
			@RequestParam(value = "OutWardDispatchEnteryRegistrationId", required = true) Integer OutWardDispatchEnteryRegistrationId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			if (outWardGoodsService
					.changeDispatchEntryRegistrationStatusToZero(OutWardDispatchEnteryRegistrationId)) {
				OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj = outWardGoodsService
						.loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(OutWardDispatchEnteryRegistrationId);
				if (outWardGoodsService
						.deleteOutWardDispatchEnteryRegistrationObjDetails(outWardDispatchEnteryRegistrationObj)) {
					json = new JSONObject();
					json.put("MSG", "Cases Deleted Successfully....!");
					json.put("OutWardDispatchEnteryRegistrationId",
							OutWardDispatchEnteryRegistrationId);
					json_data_array.put(json);
				} else {
					json = new JSONObject();
					json.put("MSG", "Opreation failed.........!");
					json_data_array.put(json);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	// /////// Vijay
	@RequestMapping(value = "/searchOutwardViewOrderEntryRegistration", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardViewOrderEntryRegistration(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {
		JSONObject result = null;
		JSONArray outwardOrdEntryArr = null;
		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistration = null;

		try {
			outwardOrdEntryArr = new JSONArray();
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			Map<String, Object> map = outWardGoodsService
					.searchOutwardViewOrderEntryRegistration(selectedValue,
							searchText, fromSpecialData, toSpecialData, from,
							organizationId);
			Integer totalPages = (Integer) map.get("totalPages");
			outWardOrderEntryRegistration = (List<OutWardOrderEntryRegistration>) map
					.get("outWardOrderEntryRegistration");
			for (OutWardOrderEntryRegistration tempOrderEntry : outWardOrderEntryRegistration) {
				result = new JSONObject();
				result.put("OutWardOrderEntryRegistrationId",
						tempOrderEntry.getOutWardOrderEnteryRegistrationId());
				result.put("ORG", tempOrderEntry.getOrganization()
						.getOrganizationName());
				result.put("COMPANY", tempOrderEntry.getCompany()
						.getCompanyName());
				result.put("COMPANY_ID", tempOrderEntry.getCompany()
						.getCompanyId());
				result.put("STOCKIST", tempOrderEntry.getStockist()
						.getStockistName());
				// result.put("ORDER_MODE",tempOrderEntry.getOrderMode().getOrderModeName());
				result.put("DATE", tempOrderEntry.getOrderDate());
				result.put("ORDER_ID", tempOrderEntry.getOrderId());
				result.put("ORDER_NO", tempOrderEntry.getOrderNo());
				result.put("INVOICE_STATUS", tempOrderEntry.getInvoiceStatus());
				outwardOrdEntryArr.put(result);
			}
			result = new JSONObject();
			result.put("paginationCount", totalPages);
			outwardOrdEntryArr.put(result);
		} catch (Exception e) {
			log.error(e);
		}
		return outwardOrdEntryArr.toString();
	}

	@RequestMapping(value = "/searchOutwardinvoiceEntryRegistration", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardinvoiceEntryRegistration(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {

		// ---
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		// --

		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistration = null;
		try {
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			Map<String, Object> map = outWardGoodsService
					.searchOutwardinvoiceEntryRegistration(selectedValue,
							searchText, fromSpecialData, toSpecialData, from,
							organizationId);
			outWardOrderEntryRegistration = (List<OutWardOrderEntryRegistration>) map
					.get("outWardOrderEntryRegistration");
			Integer totalPages = (Integer) map.get("totalPages");

			// ----
			for (OutWardOrderEntryRegistration obj : outWardOrderEntryRegistration) {
				json = new JSONObject();
				json.put("OutWardOrderEntryRegistrationId",
						obj.getOutWardOrderEnteryRegistrationId());
				json.put("ORG", obj.getOrganization().getOrganizationName());
				json.put("ORANIZATION_ID", obj.getOrganization()
						.getOrganizationId());
				json.put("COMPANY", obj.getCompany().getCompanyName());
				json.put("COMPANY_ID", obj.getCompany().getCompanyId());
				json.put("STOCKIST", obj.getStockist().getStockistName());
				json.put("ORDER_MODE", obj.getOrderMode().getOrderModeName());
				json.put("DATE", obj.getOrderDate());
				json.put("ORDER_ID", obj.getOrderId());
				json.put("ORDER_NO", obj.getOrderNo());
				json.put("PRODUCT_STATUS", obj.getProductStatus());
				// System.out.println("check value="+obj.getStockist());

				// get discount percentages
				String stockistLocation = obj.getStockist()
						.getStockistLocation();
				if (stockistLocation.equalsIgnoreCase("Local")) {
					CompanyCreditAndDiscount companyCreditAndDiscountObj = outWardGoodsService
							.loadCompanyCreditAndDiscountObjectUsignComapnyId(obj
									.getCompany().getCompanyId());
					json.put("PERCENTAGE",companyCreditAndDiscountObj.getLocalPercentage());

				}
				else
				{
					CompanyCreditAndDiscount companyCreditAndDiscountObj=outWardGoodsService.loadCompanyCreditAndDiscountObjectUsignComapnyId(obj.getCompany().getCompanyId());
//					System.out.println("data is="+companyCreditAndDiscountObj.getOutsideStationPercentage());
					json.put("PERCENTAGE", companyCreditAndDiscountObj.getOutsideStationPercentage());
				}
				json_data_array.put(json);
			}
			// ----
			json = new JSONObject();
			json.put("paginationCount", totalPages);
			json_data_array.put(json);
		}catch(Exception e){
			log.error(e);
		} 
		return json_data_array.toString();
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/searchOutwardInvoiceEntryAddedRegistrationList", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardInvoiceEntryAddedRegistrationList(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {

		JSONObject result = null;
		JSONArray outwardOrdInvEntryArr = null;

		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistration = null;
		try {
			outwardOrdInvEntryArr = new JSONArray();
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			Map<String, Object> map = outWardGoodsService
					.searchOutwardInvoiceEntryAddedRegistrationList(
							selectedValue, searchText, fromSpecialData,
							toSpecialData, from, organizationId);
			outWardOrderEntryRegistration = (List<OutWardOrderEntryRegistration>) map
					.get("outWardOrderEntryRegistrationList");
			Integer totalPages = (Integer) map.get("totalPages");
			if (outWardOrderEntryRegistration != null) {
				for (OutWardOrderEntryRegistration tempOrderEntry : outWardOrderEntryRegistration) {
					result = new JSONObject();

					result.put("OutWardOrderEntryRegistrationId",
							tempOrderEntry
									.getOutWardOrderEnteryRegistrationId());
					result.put("ORG", tempOrderEntry.getOrganization()
							.getOrganizationName());
					result.put("COMPANY", tempOrderEntry.getCompany()
							.getCompanyName());
					result.put("COMPANY_ID", tempOrderEntry.getCompany()
							.getCompanyId());
					result.put("STOCKIST", tempOrderEntry.getStockist()
							.getStockistName());
					result.put("DATE", tempOrderEntry.getOrderDate());
					result.put("ORDER_ID", tempOrderEntry.getOrderId());
					result.put("ORDER_NO", tempOrderEntry.getOrderNo());
					result.put("ORDER_MODE", tempOrderEntry.getOrderMode()
							.getOrderModeName());
					outwardOrdInvEntryArr.put(result);

				}
			}
			result = new JSONObject();
			result.put("paginationCount", totalPages);
			outwardOrdInvEntryArr.put(result);

		} catch (Exception e) {
			log.error(e);
		}
		return outwardOrdInvEntryArr.toString();
	}

	@RequestMapping(value = "/searchOutwardDispatchEntryRegistrationList", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardDispatchEntryRegistrationList(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {

		JSONObject json = null;
		JSONArray outwardOrdDispEntryArr = null;
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationsList = null;

		try {
			outwardOrdDispEntryArr = new JSONArray();
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			// List<OutWardOrderInvoiceEnteryRegistration>
			// outWardOrderInvoiceEnteryRegistrationsList
			Map<String, Object> map = outWardGoodsService
					.searchOutwardDispatchEntryRegistrationList(selectedValue,
							searchText, fromSpecialData, toSpecialData, from,
							organizationId);
			outWardOrderInvoiceEnteryRegistrationsList = (List<OutWardOrderInvoiceEnteryRegistration>) map
					.get("outWardOrderInvoiceEnteryRegistrationList");
			Integer totalPages = (Integer) map.get("totalPages");

			// if(outWardOrderInvoiceEnteryRegistrationsList.size()>0)
			// {
			for (OutWardOrderInvoiceEnteryRegistration obj : outWardOrderInvoiceEnteryRegistrationsList) {
				json = new JSONObject();
				json.put("OUTWARD_INVOICE_ENTRY_REGISTRATION_ID",
						obj.getOutWardOrderInvoiceEnteryRegistrationId());
				json.put("ORGANIZATION", obj.getOrganization()
						.getOrganizationName());
				json.put("ORG_ID", obj.getOrganization().getOrganizationId());
				json.put("INVOICE_NO", obj.getInvoiceNo());
				json.put("ORDER_NO", obj.getOutWardOrderEnteryRegistration()
						.getOrderNo());
				json.put("ORDER_ID", obj.getOutWardOrderEnteryRegistration()
						.getOrderId());
				json.put("STOCKIST", obj.getOutWardOrderEnteryRegistration()
						.getStockist().getStockistName());
				json.put("STOCKIST_ID", obj.getOutWardOrderEnteryRegistration()
						.getStockist().getStockistId());
				json.put("NET_AMOUNT", obj.getNetAmount());
				json.put("COMPANY", obj.getOutWardOrderEnteryRegistration()
						.getCompany().getCompanyName());
				json.put("COMPANY_ID", obj.getOutWardOrderEnteryRegistration()
						.getCompany().getCompanyId());
				json.put("ORDER_DATE", obj.getOutWardOrderEnteryRegistration()
						.getOrderDate());
				json.put("INVOICE_DATE", obj.getInvoiceDate());
				json.put("DAYS_COUNT", obj.getNoOfDays());
				json.put("paginationCount", totalPages);
				outwardOrdDispEntryArr.put(json);
			}
			// json=new JSONObject();
			// json.put("paginationCount",totalPages);
			// outwardOrdDispEntryArr.put(json);

		} catch (Exception e) {
			log.error(e);
		}
		return outwardOrdDispEntryArr.toString();
	}

	@RequestMapping(value = "/searchOutwardAddTrip", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardAddTrip(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {
		List<OutWardDispatchEnteryRegistration> outWardOrderEntryRegistration = null;
		JSONArray json_data_array = null;
		JSONObject json = null;
		try {
			json_data_array = new JSONArray();
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			Map<String, Object> map = outWardGoodsService.searchOutwardAddTrip(
					selectedValue, searchText, fromSpecialData, toSpecialData,
					from, organizationId);
			outWardOrderEntryRegistration = (List<OutWardDispatchEnteryRegistration>) map
					.get("outWardDispatchEnteryRegistrationList");
			Integer totalPages = (Integer) map.get("totalPages");
			if (outWardOrderEntryRegistration != null) {
				for (OutWardDispatchEnteryRegistration obj : outWardOrderEntryRegistration) {
					json = new JSONObject();
					json.put("DispatchEnteryRegistrationId",
							obj.getOutWardDispatchEnteryRegistrationId());
					json.put("ORG_ID", obj.getOrganization()
							.getOrganizationId());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPNAY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("NO_OF_CASES", obj.getNoOfCases());
					json.put("SUBMIT_DATE", obj.getSubmitDate());
					json.put("TRANSPORTER", obj.getStockist()
							.getTransporterDetails().getTransporterName());
					json.put("paginationCount", totalPages);
					// load all invoice no usign DispatchEnteryRegistrationId

					List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList = outWardGoodsService
							.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(obj
									.getOutWardDispatchEnteryRegistrationId());
					System.out.println("outwardgoodsController 3388-"+outWardOrderInvoiceEnteryRegistrationList.size());
					if (outWardOrderInvoiceEnteryRegistrationList.size() > 0) {
						JSONArray json_data_array_for_invoice = new JSONArray();
						JSONObject json_invoice = null;
						for (OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj : outWardOrderInvoiceEnteryRegistrationList) {
							json_invoice = new JSONObject();
							json_invoice.put("INVOICE_NO",
									outWardOrderInvoiceEnteryRegistrationObj
											.getInvoiceNo());
							json_data_array_for_invoice.put(json_invoice);
						}
						json.put("INVOICE_DETAILS", json_data_array_for_invoice);
					}
					json_data_array.put(json);
				}
			}
			// json=new JSONObject();
			// json.put("paginationCount",totalPages);
			// json_data_array.put(json);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/searchOutwardViewTripList", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardViewTripList(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from)

	{
		List<OutWardTrip> outWardOrderEntryRegistration = null;
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			Map<String, Object> map = outWardGoodsService
					.searchOutwardViewTripList(selectedValue, searchText,
							fromSpecialData, toSpecialData, from,
							organizationId);
			outWardOrderEntryRegistration = (List<OutWardTrip>) map
					.get("outWardTripList");
			Integer totalPages = (Integer) map.get("totalPages");
			// if(outWardOrderEntryRegistration.size()>0)
			// {
			for (OutWardTrip obj : outWardOrderEntryRegistration) {
				json = new JSONObject();
				json.put("OUTWARD_TRIP_ID", obj.getOutWardTripId());
				json.put("TRIP_NO", obj.getTripNo());
				json.put("LOADING_CHARGES", obj.getLoadingCharges());
				json.put("ORG_NAME", obj.getOrganization()
						.getOrganizationName());
				// load out ward trip transporter detail list using outward trip
				// id
				List<OutWardTripTransporterDetails> outWardTripTransporterDetailList = outWardGoodsService
						.loadOutWardTripTransporterDetailsListUsignOutWardTripId(obj
								.getOutWardTripId());
				if (outWardTripTransporterDetailList.size() > 0) {
					json.put("TRAN", outWardTripTransporterDetailList.get(0)
							.getTransporterDetails().getTransporterName());
				} else {
					json.put("TRAN", "-");
				}
				json.put("DISPATCH_BY", obj.getDispatchBy());
				json.put("VEHICAL_NO", obj.getVehicleNo());
				json.put("DRIVER_NO", obj.getDriverNo());
				json.put("NO_OF_CASES", obj.getNoOfCases());
				json.put("CARTING_AGENT", obj.getCartingAgent().getName());
				json_data_array.put(json);
			}
			json = new JSONObject();
			json.put("paginationCount", totalPages);
			json_data_array.put(json);
			// }
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/searchOutwardGetPassList", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchOutwardGetPassList(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList = null;
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		JSONArray json_ImageArray = null;
		JSONObject tempJson = null;
		JSONObject jsonInvoiceADD = null;
		JSONArray json_array= new JSONArray();
		try {
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			Map<String, Object> map = outWardGoodsService
					.searchOutwardGetPassList(selectedValue, searchText,
							fromSpecialData, toSpecialData, from,
							organizationId);
			outWardDispatchEnteryRegistrationList = (List<OutWardDispatchEnteryRegistration>) map.get("outWardDispatchEnteryRegistrationList");
			// if(outWardDispatchEnteryRegistrationList.size()>0)
			// {
			Integer totalPages = (Integer) map.get("totalPages");
			for (OutWardDispatchEnteryRegistration obj : outWardDispatchEnteryRegistrationList) {
				json = new JSONObject();
				json.put("TRIP_NO", obj.getOutWardTrip().getTripNo());
				json.put("ORG_NAME", obj.getOrganization()
						.getOrganizationName());
				json.put("ORG_ID", obj.getOrganization().getOrganizationId());
				json.put("NO_OF_CASES", obj.getNoOfCases());
				json.put("STOCKIST", obj.getStockist().getStockistName());
				json.put("COMPANY", obj.getCompany().getCompanyName());
				json.put("OUTWARD_DISPATCH_ENTRY_REG_ID",
						obj.getOutWardDispatchEnteryRegistrationId());
				json.put("DISPATCH_BY", obj.getOutWardTrip().getDispatchBy());
				json.put("VEHICAL_NO", obj.getOutWardTrip().getVehicleNo());
				json.put("LOADING_CHARGES", obj.getOutWardTrip()
						.getLoadingCharges());
				json.put("TRIP_CASE", obj.getOutWardTrip().getNoOfCases());
				json.put("CARTING", obj.getOutWardTrip().getCartingAgent()
						.getName());
				json.put("DRIVER_NO", obj.getOutWardTrip().getDriverNo());
				// json.put("DATE", obj.getOutWardTrip().getSubmitDate());

				String date = "";
				date += obj.getOutWardTrip().getSubmitDate();
				String DATE = obj.getOutWardTrip().getSubmitDate().toString()
						.substring(0, 10);

				json.put("DATE", DATE); // obj.getOutWardTrip().getSubmitDate().toString().length()-2

				String invoiceNo = "";
				String tempInvoiceNo = "";
				List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList1 = outWardGoodsService
						.getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(obj
								.getOutWardDispatchEnteryRegistrationId());
				if (outWardOrderInvoiceEnteryRegistrationList1.size() > 0) {
					json_array= new JSONArray();
					for (OutWardOrderInvoiceEnteryRegistration ooierObj : outWardOrderInvoiceEnteryRegistrationList1) {
						//
						if (ooierObj.getInvoiceNo().length() >= 5) {
							//
							tempInvoiceNo += ooierObj.getInvoiceNo().substring(
									ooierObj.getInvoiceNo().length() - 4)
									+ ",";
						} else {
							tempInvoiceNo += ooierObj.getInvoiceNo() + ",";
							// Manish
						}
						// for invoice data
						jsonInvoiceADD = new JSONObject();
						jsonInvoiceADD.put("INVOICE", ooierObj.getInvoiceNo());
					
						jsonInvoiceADD.put("GROSS_AMT", ooierObj.getGrossAmount());
//						System.out.println("AMOUNT IS"+ooierObj.getGrossAmount());
						jsonInvoiceADD.put("IN_DATE",ooierObj.getInvoiceDate());
						jsonInvoiceADD.put("TAX_AMT", ooierObj.getTaxAmout());
						jsonInvoiceADD.put("VAT_AMT", ooierObj.getVatAmount());
						jsonInvoiceADD.put("DISCOUNT", ooierObj.getDiscountIsTakenOrNot());
						jsonInvoiceADD.put("NET_AMT", ooierObj.getNetAmount());
						
						
// for organization data-----------------------------------------
						json.put("ORGN_NAME", ooierObj
								.getOutWardOrderEnteryRegistration()
								.getOrganization().getOrganizationName());
						json.put("COMPANY_NAME", ooierObj
								.getOutWardOrderEnteryRegistration()
								.getCompany().getCompanyName());
						json.put("ORDER_NAME", ooierObj
								.getOutWardOrderEnteryRegistration()
								.getOrderMode().getOrderModeName());
						json.put("STOCKIST_NAME", ooierObj
								.getOutWardOrderEnteryRegistration()
								.getStockist().getStockistName());
						json.put("ORDER_NO", ooierObj
								.getOutWardOrderEnteryRegistration()
								.getOrderNo());
						json.put("ORG_ID", ooierObj
								.getOutWardOrderEnteryRegistration()
								.getOrganization().getOrganizationId());
						json.put("ORDER_DATE", ooierObj
								.getOutWardOrderEnteryRegistration()
								.getOrderDate());
//--------------------------------------------------
						
						List<OutWardOrderEntryRegistrationOrderCopyPath> list = outWardGoodsService.loadImages(ooierObj.getOutWardOrderEnteryRegistration().getOutWardOrderEnteryRegistrationId());
						json_ImageArray = new JSONArray();
						for (OutWardOrderEntryRegistrationOrderCopyPath tempObj : list) {
							tempJson = new JSONObject();
							tempJson.put("IMAGE_PATH", tempObj.getImagePath());
						
							tempJson.put("IMAGE_ID",tempObj.getOutWardOrderEntryRegistrationOrderCopyPathId());
							json_ImageArray.put(tempJson);

						}
						json.put("ORDER_COPY", json_ImageArray);
						json_array.put(jsonInvoiceADD);
//						System.out.println("full invoice is="+json_array);
					}
					json.put("INVOICE_NO", tempInvoiceNo);
					json_data_array.put(json);
					json.put("INVOICE_LR_ARRAY",json_array);        //for arrayList of invoice info
				}
				
			}
			json = new JSONObject();
			json.put("paginationCount", totalPages);
			json_data_array.put(json);
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();

	}

	@RequestMapping(value = "/searchViewGetPassLRList", method = RequestMethod.POST, produces = "application/json")
	private @ResponseBody String searchViewGetPassLRList(
			@RequestParam(value = "selectedValue", required = true) String selectedValue,
			@RequestParam(value = "searchText", required = true) String searchText,
			@RequestParam(value = "fromSpecialData", required = true) String fromSpecialData,
			@RequestParam(value = "toSpecialData", required = true) String toSpecialData,
			@RequestParam(value = "from", required = true) Integer from) {

		JSONArray json_data_array = null;
		// JSONArray json_data_array= new JSONArray();
		// JSONObject json=null;
		try {
			Integer organizationId = (Integer) session
					.getAttribute("ORGANIZATION_ID");

			json_data_array = outWardGoodsService.searchViewGetPassLRList(
					selectedValue, searchText, fromSpecialData, toSpecialData,
					from, organizationId);
			/*
			 * List<OutWardDispatchEnteryRegistration>
			 * outWardDispatchEnteryRegistration=outWardGoodsService.
			 * loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne
			 * ((Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
			 * if(outWardDispatchEnteryRegistration.size()>0) {
			 * for(OutWardDispatchEnteryRegistration obj :
			 * outWardDispatchEnteryRegistration) { json=new JSONObject();
			 * json.put("OUTWARD_DISPATCH_ENTERY_REG_ID",
			 * obj.getOutWardDispatchEnteryRegistrationId());
			 * json.put("TRIP_NO", obj.getOutWardTrip().getTripNo()); //
			 * json.put
			 * ("LOADING_CHARGES",obj.getOutWardTrip().getLoadingCharges());
			 * json.put("ORG_NAME",
			 * obj.getOrganization().getOrganizationName()); json.put("ORG_ID",
			 * obj.getOrganization().getOrganizationId()); json.put("COMPANY",
			 * obj.getCompany().getCompanyName()); json.put("STOCKIST",
			 * obj.getStockist().getStockistName()); json.put("TRAN",
			 * obj.getStockist().getTransporterDetails().getTransporterName());
			 * // json.put("DISPATCH_BY",obj.getOutWardTrip().getDispatchBy());
			 * // json.put("VEHICAL_NO",obj.getOutWardTrip().getVehicleNo()); //
			 * json.put("DRIVER_NO", obj.getOutWardTrip().getDriverNo());
			 * json.put("NO_OF_CASES", obj.getNoOfCases()); // json.put("LR_NO",
			 * obj.getOutWardGatePass().getLRNo()); json.put("LR_IMAGE_PATH",
			 * obj.getOutWardGatePass().getLrImagePath());
			 * json.put("OUTWARD_GATEPASS_ID",
			 * obj.getOutWardGatePass().getOutWardGatePassId());
			 * json.put("LR_NO", obj.getOutWardGatePass().getLRNo());
			 * 
			 * // load all invoice no String invoiceNo="";
			 * List<OutWardOrderInvoiceEnteryRegistration>
			 * outWardOrderInvoiceEnteryRegistrationList=outWardGoodsService.
			 * getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId
			 * (obj.getOutWardDispatchEnteryRegistrationId());
			 * if(outWardOrderInvoiceEnteryRegistrationList.size()>0) {
			 * for(OutWardOrderInvoiceEnteryRegistration ooierObj :
			 * outWardOrderInvoiceEnteryRegistrationList) {
			 * invoiceNo+=ooierObj.getInvoiceNo()+","; } }
			 * json.put("INVOICE_NO", invoiceNo); json_data_array.put(json); } }
			 */
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}

	@RequestMapping(value = "/deleteOrder", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String deleteOrder(
			@RequestParam(value = "OutWardOrderEntryRegistrationId", required = true) Integer OutWardOrderEntryRegistrationId) {
		JSONArray json_data_array = new JSONArray();
		JSONObject json = null;
		try {
			OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj = outWardGoodsService
					.loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(OutWardOrderEntryRegistrationId);
			if (outWardOrderEntryRegistrationObj != null) {
				if (outWardGoodsService
						.deleteOutWardOrderEntry(outWardOrderEntryRegistrationObj)) {
					json = new JSONObject();
					json.put("MSG", "Order Delete Successfully....!");
					json.put("OutWardOrderEntryRegistrationId",
							OutWardOrderEntryRegistrationId);
					json_data_array.put(json);
				} else {
					json = new JSONObject();
					json.put("MSG", "Opreation failed.........!");
					json_data_array.put(json);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();

	}

}
