/**
 * 
 */
package com.protocol.wms.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.protocol.wms.model.MenuAuthentication;
import com.protocol.wms.model.SubMenuAuthentication;
import com.protocol.wms.service.MenuAuthenticationService;
import com.protocol.wms.service.SubMenuAuthenticationService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/MenuController")
public class MenuController {
	private Logger log = Logger.getLogger(MenuController.class.getClass());
	
	
	private MenuAuthenticationService menuAuthenticationService;
	
	private SubMenuAuthenticationService subMenuAuthenticationService;

	/**
	 * @param menuAuthenticationService the menuAuthenticationService to set
	 */
	@Autowired
	@Qualifier("MenuAuthenticationServiceImpl")
	public void setMenuAuthenticationService(
			MenuAuthenticationService menuAuthenticationService) {
		this.menuAuthenticationService = menuAuthenticationService;
	}
	
	/**
	 * @param subMenuAuthenticationService the subMenuAuthenticationService to set
	 */
	@Autowired
	@Qualifier("SubMenuAuthenticationServiceImpl")
	public void setSubMenuAuthenticationService(
			SubMenuAuthenticationService subMenuAuthenticationService) {
		this.subMenuAuthenticationService = subMenuAuthenticationService;
	}




	@RequestMapping(value="/loadMenu",method=RequestMethod.POST,produces="application/json")
	private  @ResponseBody String loadMenu(HttpServletRequest request)
			{
				JSONArray json_data_array= new JSONArray();
				JSONObject json=null;
				
				
				try 
				{
					//load main menu
					List<MenuAuthentication> menuAuthenticationsList=menuAuthenticationService.loadMainMenuUsingUserTypeId((Integer) request.getSession().getAttribute("USER_TYPE"));
					if(menuAuthenticationsList.size()>0)
					{
						for(MenuAuthentication obj :menuAuthenticationsList)
						{
							json=new JSONObject();
							json.put("MENU", obj.getMenu().getMenuName());
							// load sub menu using Menu Authentication Id
							List<SubMenuAuthentication> subMenuAuthenticationsList=subMenuAuthenticationService.loadSubMenuUsingMenuAuthenticationId(obj.getMenuAuthenticationId());
							if(subMenuAuthenticationsList.size()>0)
							{
								JSONArray json_subMenuArray=new JSONArray();
								JSONObject jsonSubMenu=null;
								for(SubMenuAuthentication subMemuobj : subMenuAuthenticationsList)
								{
									jsonSubMenu=new JSONObject();
									jsonSubMenu.put("SUBMENU", subMemuobj.getSubMenu().getSubMenuName());
									jsonSubMenu.put("SUBMENU_FUNCTION", subMemuobj.getSubMenu().getSubMenuFunctionName());
									json_subMenuArray.put(jsonSubMenu);
								}
								json.put("SUBMENUARRAY", json_subMenuArray);
							}
							json_data_array.put(json);
						}
					}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
					log.error(e);
				}
				return json_data_array.toString();
			}
	
}
