/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.BloodGroup;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.EmployeeDetailsAddressProofPath;
import com.protocol.wms.model.EmployeeDetailsImagePath;
import com.protocol.wms.model.EmployeeDocument;
import com.protocol.wms.model.EmployeeDocumentImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationLRImgPath;
import com.protocol.wms.model.RoleType;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.service.BloodGroupService;
import com.protocol.wms.service.CompanyService;
import com.protocol.wms.service.DocumentListTypeService;
import com.protocol.wms.service.EmployeeDetailsService;
import com.protocol.wms.service.EmployeeDocumentService;
import com.protocol.wms.service.OrganizationService;
import com.protocol.wms.service.RoleTypeService;
import com.protocol.wms.service.UserDetailsService;
import com.protocol.wms.service.UserTypeService;

/**
 * @author Sudhakar
 *
 */
@Controller
@RequestMapping(value="/EmployeeController")
public class EmployeeController {
	
	private Logger log = Logger.getLogger(EmployeeController.class.getClass());
	
	private BloodGroupService bloodGroupService;
	
	private RoleTypeService roleTypeService;
	
	private UserDetailsService userDetailsService;
	
	private OrganizationService organizationservices;
	
	private CompanyService companyService;
	
	private EmployeeDetailsService employeeDetailsService;
	
	private UserTypeService userTypeService;
	
	private DocumentListTypeService documentListTypeService;
	
	private EmployeeDocumentService employeeDocumentService;
	

	@Autowired
	@Qualifier("BloodGroupServiceImpl")
	public void setBloodGroupService(BloodGroupService bloodGroupService) {
		this.bloodGroupService = bloodGroupService;
	}
	
	@Autowired
	@Qualifier("RoleTypeerviceImpl")
	public void setRoleTypeService(RoleTypeService roleTypeService) {
		this.roleTypeService = roleTypeService;
	}
	
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@Autowired
	@Qualifier("OrganizationServiceImpl")
	public void setOrganizationservices(OrganizationService organizationservices) {
		this.organizationservices = organizationservices;
	}

	@Autowired
	@Qualifier("CompanyServiceImpl")
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	@Autowired
	@Qualifier("EmployeeDetailsServiceImpl")
	public void setEmployeeDetailsService(
			EmployeeDetailsService employeeDetailsService) {
		this.employeeDetailsService = employeeDetailsService;
	}
	
	@Autowired
	@Qualifier("UserTypeServiceImpl")
	public void setUserTypeService(UserTypeService userTypeService) {
		this.userTypeService = userTypeService;
	}
	
	@Autowired
	@Qualifier("DocumentListTypeServiceImpl")
	public void setDocumentListTypeService(
			DocumentListTypeService documentListTypeService) {
		this.documentListTypeService = documentListTypeService;
	}

	@Autowired
	@Qualifier("EmployeeDocumentServiceImpl")
	public void setEmployeeDocumentService(
			EmployeeDocumentService employeeDocumentService) {
		this.employeeDocumentService = employeeDocumentService;
	}

	@RequestMapping(value="/bloodGroupList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String bloodGroupList(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<BloodGroup> bloodGroupList=bloodGroupService.loadBloodGroupList();
			if(bloodGroupList.size()>0)
			{
				for(BloodGroup obj : bloodGroupList)
				{
					json=new JSONObject();
					json.put("BLOOD_GROUP_ID", obj.getBloodGroupId());
					json.put("BLOOD_GROUP",obj.getBloodGroup());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return json_data_array.toString();
	}
	

	@RequestMapping(value="/loadRoleType",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadRoleType(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<RoleType> roleTypeList=roleTypeService.loadRoleTypeList();
			if(roleTypeList.size()>0)
			{
				for(RoleType obj : roleTypeList)
				{
					json=new JSONObject();
					json.put("ROLE_TYPE_ID",obj.getRoleTypeId());
					json.put("ROLE_TYPE",obj.getRoleName());
					json_data_array.put(json);
				}
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/saveEmployee",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveEmployee(@RequestParam(value="eName",required=true) String eName,
			@RequestParam(value="MaritalStatus",required=true) String maritalStatus,
			@RequestParam(value="mobileNo",required=true) Long mobileNo,
			@RequestParam(value="currentAddress",required=true) String currentAddress,
			@RequestParam(value="PermanentAddress",required=true) String PermanentAddress,
			@RequestParam(value="sex",required=true) String sex,
			@RequestParam(value="qulification",required=true) String qulification,
			@RequestParam(value="telephoneNo",required=true) Long telephoneNo,
			@RequestParam(value="emailId",required=true) String emailId,
			@RequestParam(value="pincode",required=true) String pincode,
			@RequestParam(value="panno",required=true) String panno,
			@RequestParam(value="basicSalary",required=true) Double basicSalary,
			@RequestParam(value="uname",required=true) String uname,
			@RequestParam(value="pwd",required=true) String pwd,
			@RequestParam(value="doj",required=true) String doj,
			@RequestParam(value="dob",required=true) String dob,
			@RequestParam(value="comapnyId",required=true) Integer comapnyId,
			@RequestParam(value="selectedOrganiztionNameId",required=true) Integer selectedOrganiztionNameId,
			@RequestParam(value="bloodroupName",required=true) Integer bloodroupName,
			@RequestParam(value="roleType",required=true) Integer roleType,
			@RequestParam(value="empImage") MultipartFile empImage[],
			@RequestParam(value="AddressProofImage") MultipartFile AddressProofImage[],
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		List<EmployeeDetailsImagePath> employeeImageList = null;
		EmployeeDetailsImagePath tempEmpImage = null;
		List<EmployeeDetailsAddressProofPath> addressProofImageList = null;
		EmployeeDetailsAddressProofPath addressProofImgage = null;
		try 
		{
			// check user name already register
			UserDetails userDetailsObj=userDetailsService.checkUserNameAlreadyExitOrNot(uname);
			if(userDetailsObj==null)
			{
				employeeImageList = new ArrayList<EmployeeDetailsImagePath>();
				for(MultipartFile file : empImage){
					tempEmpImage = new EmployeeDetailsImagePath();
					tempEmpImage.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					employeeImageList.add(tempEmpImage);
				}
				addressProofImageList = new ArrayList<EmployeeDetailsAddressProofPath>();
				for(MultipartFile file : AddressProofImage){
					addressProofImgage = new EmployeeDetailsAddressProofPath();
					addressProofImgage.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					addressProofImageList.add(addressProofImgage);
				}
				DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
				EmployeeDetails empObj=new EmployeeDetails();
				empObj.setEmployeeDetailsAddressProofPathList(addressProofImageList);
				empObj.setEmployeeDetailsImagePathList(employeeImageList);
				empObj.setName(eName);
				empObj.setMaritalStats(maritalStatus);
				empObj.setMobileNo(mobileNo);
				empObj.setCurrentAddress(currentAddress);
				empObj.setPermanetAddress(PermanentAddress);
				empObj.setGender(sex);
				empObj.setQualification(qulification);
				empObj.setTelephone(telephoneNo);
				empObj.setEmailId(emailId);
				empObj.setPinCode(pincode);
				empObj.setPanNo(panno);
				empObj.setBasicSalary(basicSalary);
				empObj.setStatus(1);
				
				//set submit date
				//set organization submit date
				java.util.Date today = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			    String IST = df.format(today);
			    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    java.util.Date date = df2.parse(IST);
				empObj.setSubmitDate(date);
				//set DOB
				if(dob!="")
				{
					java.util.Date d1 = dtf.parse(dob);
					empObj.setDOB(new java.sql.Date(d1.getTime()));
				}
				if(doj!="")
				{
					java.util.Date d1 = dtf.parse(doj);
					empObj.setDOJ(new java.sql.Date(d1.getTime()));
				}
				
				//load organization object and set organization
				empObj.setOrganization(organizationservices.loadOrganizationObjectUsingOrganiztionId(selectedOrganiztionNameId));
				
				//load company object and set company object
				empObj.setCompany(companyService.loadCompanyObjectUsingCompanyId(comapnyId));
				
				// load role type object and set
				empObj.setRoleType(roleTypeService.loadRoleTypeUsingRoleTypeId(roleType));
				
				// load blood group object
				empObj.setBloodGroup(bloodGroupService.loadBloodGroupObjectUsingBloodGroupId(bloodroupName));
				
				// save employee object
				Integer empDetailsId=employeeDetailsService.saveEmployeeDetails(empObj);
				if(empDetailsId!=null)
				{
					// save user details
					UserDetails userDetails=new UserDetails();
					userDetails.setUserName(uname);
					userDetails.setPassword(pwd);
					// load company object and set
					userDetails.setUserType(userTypeService.loadUserTypeUsingUserTypeId(Constant.EMPLOYEE));
					// save user details object
					Integer userDetailsId=userDetailsService.saveUserDetails(userDetails);
					if(userDetailsId!=null)
					{
						// update employee object
						
						// load employee object based on employee details id
						EmployeeDetails employeeDetailsObj=employeeDetailsService.loadEmployeeObjectUsingEmployeeDetailsId(empDetailsId);
						employeeDetailsObj.setUserDetails(userDetailsService.loadUserDetailsUsingUserDetailsId(userDetailsId));
						employeeDetailsService.updateEmployeeDetails(employeeDetailsObj);
						
						json=new JSONObject();
						json.put("MSG","Employee added successfully");
						json_data_array.put(json);
						
					}
				}
				
				
			}
			else
			{
				json=new JSONObject();
				json.put("MSG","User Name already exists");
				json_data_array.put(json);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/updateEmployeeDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateEmployeeDetails(
			@RequestParam(value="empDetailsId",required=true) Integer employeeDetailsId,
			@RequestParam(value="eName",required=true) String eName,
			@RequestParam(value="MaritalStatus",required=true) String maritalStatus,
			@RequestParam(value="mobileNo",required=true) Long mobileNo,
			@RequestParam(value="currentAddress",required=true) String currentAddress,
			@RequestParam(value="PermanentAddress",required=true) String PermanentAddress,
			@RequestParam(value="sex",required=true) String sex,
			@RequestParam(value="qulification",required=true) String qulification,
			@RequestParam(value="telephoneNo",required=true) Long telephoneNo,
			@RequestParam(value="emailId",required=true) String emailId,
			@RequestParam(value="pincode",required=true) String pincode,
			@RequestParam(value="panno",required=true) String panno,
			@RequestParam(value="basicSalary",required=true) Double basicSalary,
			@RequestParam(value="uname",required=true) String uname,
			@RequestParam(value="pwd",required=true) String pwd,
			@RequestParam(value="doj",required=true) String doj,
			@RequestParam(value="dob",required=true) String dob,
			@RequestParam(value="bloodroupName",required=true) Integer bloodroupNameId,
			@RequestParam(value="roleType",required=true) Integer roleTypeId,
			//@RequestParam(value="empImage",required=false) MultipartFile empImage,
			//@RequestParam(value="AddressProofImage",required=false) MultipartFile AddressProofImage,
			HttpServletRequest request)
	{
		JSONObject result = new JSONObject();
		UserDetails userDetailsObj = null;
		EmployeeDetails empObj = null;
		try 
		{
			empObj=employeeDetailsService.loadEmployeeObjectUsingEmployeeDetailsId(employeeDetailsId);
			if(!empObj.getUserDetails().getUserName().equals(uname)){
				// check user name already register
				userDetailsObj=userDetailsService.checkUserNameAlreadyExitOrNot(uname);
				if(userDetailsObj!=null)
				{
					result.put("MSG","User Name already exists....!");
					result.put("RESULT",false);
					return result.toString();
				}else{
					empObj.getUserDetails().setUserName(uname);
					empObj.getUserDetails().setPassword(pwd);
				}
			}
			DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
			empObj.setName(eName);
			empObj.setMaritalStats(maritalStatus);
			empObj.setMobileNo(mobileNo);
			empObj.setCurrentAddress(currentAddress);
			empObj.setPermanetAddress(PermanentAddress);
			empObj.setGender(sex);
			empObj.setQualification(qulification);
			empObj.setTelephone(telephoneNo);
			empObj.setEmailId(emailId);
			empObj.setPinCode(pincode);
			empObj.setPanNo(panno);
			empObj.setBasicSalary(basicSalary);
			if(dob!=""&&doj!="")
			{
				java.util.Date d1 = dtf.parse(dob);
				empObj.setDOB(new java.sql.Date(d1.getTime()));
				
				d1 = dtf.parse(doj);
				empObj.setDOJ(new java.sql.Date(d1.getTime()));
			}
			else{
				result.put("MSG","Date of Birth and Date of joinig is required....!");
				return result.toString();
			}
			/*if(empImage!=null){
			empObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(empImage));
			}
			if(AddressProofImage!=null){
				// save address proof image
				empObj.setAddressProofPath(CommonMethods.writeDocumentAndGetFileDB_Path(AddressProofImage));
			}*/
			
			result.put("MSG","Employee Details updation failed....!");
			if(employeeDetailsService.updateEmployeeDetails(roleTypeId,bloodroupNameId,empObj))
			{
				result.put("MSG","Employee Details updated successfully....!");
				result.put("RESULT",true);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	
	@RequestMapping(value="/updateEmployeeImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateEmployeeImage(
			@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
			@RequestParam(value="registrationId",required=true) Integer employeeDetailsId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<EmployeeDetailsImagePath> employeeImgList = null;
		EmployeeDetailsImagePath tempEmployeeImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(documentImage!=null&&!(documentImage.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write  file
			employeeImgList  = new ArrayList<EmployeeDetailsImagePath>();
			for(MultipartFile file : documentImage){
				if(file!=null){
					tempEmployeeImgPath = new EmployeeDetailsImagePath();
					tempEmployeeImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					employeeImgList.add(tempEmployeeImgPath);
				}
			}
			json_ImageArray = employeeDetailsService.updateEmployeeImages(employeeDetailsId, employeeImgList);
			result.put("MSG", "Employee Image File Upload successful...!");
			result.put("IMAGE", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/deleteEmployeeImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteEmployeeImage(@RequestParam(value="employeeImageId",required=false) Integer employeeImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(employeeDetailsService.deleteEmployeeImage(employeeImageId)){
				result.put("RESULT",true);
				result.put("MSG","Employee Image Deleted Successfully");
			}else{
				result.put("MSG","Employee Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/updateEmployeeAddressProofImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateEmployeeAddressProofImage(
			@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
			@RequestParam(value="registrationId",required=true) Integer employeeDetailsId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<EmployeeDetailsAddressProofPath> employeeAddressProofPathList = null;
		EmployeeDetailsAddressProofPath tempEmployeeAddressProofImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(documentImage!=null&&!(documentImage.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write  file
			employeeAddressProofPathList  = new ArrayList<EmployeeDetailsAddressProofPath>();
			for(MultipartFile file : documentImage){
				if(file!=null){
					tempEmployeeAddressProofImgPath = new EmployeeDetailsAddressProofPath();
					tempEmployeeAddressProofImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					employeeAddressProofPathList.add(tempEmployeeAddressProofImgPath);
				}
			}
			json_ImageArray = employeeDetailsService.updateEmployeeAddressProofImage(employeeDetailsId, employeeAddressProofPathList);
			result.put("MSG", "Employee Address Proof File Upload successful...!");
			result.put("ADDR_PROOF", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/deleteEmployeeAddressProofImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteEmployeeAddressProofImage(@RequestParam(value="employeeAddressProofImageId",required=false) Integer employeeImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(employeeDetailsService.deleteEmployeeAddressProofImage(employeeImageId)){
				result.put("RESULT",true);
				result.put("MSG","Employee Address Proof Image Deleted Successfully");
			}else{
				result.put("MSG","Employee Address Proof Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/listEmployeeDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listEmployeeDetails(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<EmployeeDetails> employeeDetailsList=employeeDetailsService.loadEmployeeDetailsListUsingOrganizationId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(employeeDetailsList.size()>0)
			{
				for(EmployeeDetails obj : employeeDetailsList)
				{
					if("1".equals(obj.getOrganization().getOrganizationStatus())){
					json=new JSONObject();
					json.put("ORGANIZATION", obj.getOrganization().getOrganizationName());
					json.put("EMP_DETAILS_ID", obj.getEmployeeDetailsId());
					json.put("EMP_NAME", obj.getName());
					json.put("GENDER", obj.getGender());
					json.put("Qualification", obj.getQualification());
					json.put("Mobile_No", obj.getMobileNo());
					if("".equals(obj.getEmailId()) || obj.getEmailId()==null)
					{
						json.put("Email_ID", "-");
					}else{
					json.put("Email_ID", obj.getEmailId());
					}
					json_data_array.put(json);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/searchEmployeeDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchEmployeeDetails(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		java.util.Map<Object, Object>map = null;
		try 
		{
			map=employeeDetailsService.searchEmployeeDetails(searchOption,searchText,(Integer) request.getSession().getAttribute("ORGANIZATION_ID"),from);
			List<EmployeeDetails> employeeDetailsList = (List<EmployeeDetails>) map.get("empList");
			Integer totalPage = (Integer) map.get("totalPages");
			
			if(employeeDetailsList.size()>0)
			{
				for(EmployeeDetails obj : employeeDetailsList)
				{
//					if("1".equals(obj.getOrganization().getOrganizationStatus())){
					json=new JSONObject();
					json.put("ORGANIZATION", obj.getOrganization().getOrganizationName());
					json.put("EMP_DETAILS_ID", obj.getEmployeeDetailsId());
					json.put("EMP_NAME", obj.getName());
					json.put("GENDER", obj.getGender());
					json.put("Qualification", obj.getQualification());
					json.put("Mobile_No", obj.getMobileNo());
					json.put("Email_ID", obj.getEmailId());
					json_data_array.put(json);
//					}
				}
			}
			json=new JSONObject();
			json.put("paginationCount", totalPage);
			json_data_array.put(json);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/loadEmployeeList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadEmployeeListUsingBasedOnOrganizationId(
			@RequestParam(value="organizationId",required=true) Integer orgnizationId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<EmployeeDetails> employeeDetailsList=employeeDetailsService.loadEmployeeDetailsListUsingOrganizationId(orgnizationId);
			if(employeeDetailsList.size()>0)
			{
				for(EmployeeDetails obj : employeeDetailsList)
				{
					json=new JSONObject();
					json.put("EMP_ID", obj.getEmployeeDetailsId());
					json.put("EMP_NAME",obj.getName());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return json_data_array.toString();
		
	}
	
	@RequestMapping(value="/saveEmployeeDocument",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveEmployeeDocument(
			@RequestParam(value="empUploadDocumetOrgId",required=true) Integer empUploadDocumetOrgId,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId,
			@RequestParam(value="employeeDetailsId",required=true) Integer employeeDetailsId,
			@RequestParam(value="documentName",required=true) MultipartFile documentFile[], 
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		List<EmployeeDocumentImagePath> documentList = null;
		EmployeeDocumentImagePath  tempDocumentObj = null; 
		try 
		{
			EmployeeDocument empDocObj=new EmployeeDocument();
			empDocObj.setStatus(1);
			documentList = new ArrayList<EmployeeDocumentImagePath>();
			 for(MultipartFile file : documentFile){
					if(file!=null){
						tempDocumentObj = new EmployeeDocumentImagePath();
						tempDocumentObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
						documentList.add(tempDocumentObj);
					}
				}
		    empDocObj.setSubmitDate(CommonMethods.setSubmitedDate());
		    empDocObj.setEmployeeDocumentImagePathList(documentList);
			
			// set documet type list object
			empDocObj.setDocumentListType(documentListTypeService.loadDocumentListTypeUsingDocumentListTypeId(documentTypeId));
			
			// set organization object
			empDocObj.setOrganization(organizationservices.loadOrganizationObjectUsingOrganiztionId(empUploadDocumetOrgId));
			
			// set employee object
			empDocObj.setEmployeeDetails(employeeDetailsService.loadEmployeeObjectUsingEmployeeDetailsId(employeeDetailsId));
			
			// save employee document object
			Integer employeeDocumetId=employeeDocumentService.saveEmployeeDocument(empDocObj);
			if(employeeDocumetId!=null)
			{
				json=new JSONObject();
				json.put("MSG", "Employee document added successfully");
				json_data_array.put(json);
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}

	@RequestMapping(value="/employeeDocumentList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String employeeDocumentList(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		try 
		{
			json_data_array = employeeDocumentService.viewEmployeeDocumetListUsingOrganizationId((Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//
	@RequestMapping(value="/updateEmployeeDocumentInfo",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateEmployeeDocumentInfo(@RequestParam(value="employeeDocId",required=true) Integer employeeDocumentId,
			@RequestParam(value="emplyeeDetailsId",required=true) Integer emplyeeDetailsId,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId)
	{
		JSONObject result= null;
		try
		{
			result = employeeDocumentService.updateEmployeeDocumentInfo(documentTypeId,emplyeeDetailsId,employeeDocumentId);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	
	@RequestMapping(value="/updateEmployeeUploadDocumentImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateEmployeeUploadDocumentImage(
			@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
			@RequestParam(value="registrationId",required=true) Integer employeeDocumentId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<EmployeeDocumentImagePath> documentList = null;
		EmployeeDocumentImagePath tempDocumentPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(documentImage!=null&&!(documentImage.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write  file
			documentList  = new ArrayList<EmployeeDocumentImagePath>();
			for(MultipartFile file : documentImage){
				if(file!=null){
					tempDocumentPath = new EmployeeDocumentImagePath();
					tempDocumentPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					documentList.add(tempDocumentPath);
				}
			}
			json_ImageArray = employeeDocumentService.updateEmployeeImages(employeeDocumentId, documentList);
			result.put("MSG", "Employee Document Image File Upload successful...!");
			result.put("FILE_NAME", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/deleteEmployeeDocumentFileImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteEmployeeDocumentFileImage(@RequestParam(value="employeeDocumentPathId",required=false) Integer employeeDocumentPathId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(employeeDocumentService.deleteEmployeeUploadDocumentFileImage(employeeDocumentPathId)){
				result.put("RESULT",true);
				result.put("MSG","Employee Upload Document Image Deleted Successfully");
			}else{
				result.put("MSG","Employee Upload Document Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/searchEmployeeDocumet",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String searchEmployeeDocumet(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=true) Integer from, HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		
		try 
		{
			json_data_array=employeeDocumentService.searchEmployeeDocumet(searchOption,searchText,from,(Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//Delete Employee Details
	@RequestMapping(value="/deleteEmployeeDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteEmployeeDetails(@RequestParam(value="empDetailsId",required=false) Integer empDetailsId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(employeeDetailsService.deleteEmployeeDetails(empDetailsId)){
				result.put("MSG","Employee Details Deleted Successfully");
			}else{
				result.put("MSG","Employee Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		
		return result.toString();
	}
	
	//Delete Employee Document Details
	@RequestMapping(value="/deleteEmployeeDocumet",method=RequestMethod.POST)
	private  @ResponseBody String deleteEmployeeDocumet(@RequestParam(value="empDocumentId",required=false) Integer empDocumentId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(employeeDocumentService.deleteEmployeeDocumet(empDocumentId)){
				result.put("MSG","Employee Document Details Deleted Successfully");
			}else{
				result.put("MSG","Employee Document Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		
		return result.toString();
	}
	
	@RequestMapping(value="/ViewAllDetailsOfEmployeeDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String ViewAllDetailsOfEmployeeDetails(@RequestParam(value="empDetailsId",required=true) Integer empDetailsId,
			@RequestParam(value="editStatus",required=false) Boolean editStatus,HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		try 
		{
			json_data_array = employeeDetailsService.viewEmployeeDetailsUsingEmployeeDetailsId(empDetailsId,editStatus);
					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	
}
