/**
 * 
 */
package com.protocol.wms.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.BloodGroup;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.EmployeeDocument;
import com.protocol.wms.model.RoleType;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.service.BloodGroupService;
import com.protocol.wms.service.CompanyService;
import com.protocol.wms.service.DocumentListTypeService;
import com.protocol.wms.service.EmployeeDetailsService;
import com.protocol.wms.service.EmployeeDocumentService;
import com.protocol.wms.service.OrganizationService;
import com.protocol.wms.service.RoleTypeService;
import com.protocol.wms.service.UserDetailsService;
import com.protocol.wms.service.UserTypeService;

/**
 * @author Sudhakar
 *
 */
@Controller
@RequestMapping(value="/EmloyeeController")
public class EmloyeeController {
	
	private Logger log = Logger.getLogger(EmloyeeController.class.getClass());
	
	private BloodGroupService bloodGroupService;
	
	private RoleTypeService roleTypeService;
	
	private UserDetailsService userDetailsService;
	
	private OrganizationService organizationservices;
	
	private CompanyService companyService;
	
	private EmployeeDetailsService employeeDetailsService;
	
	private UserTypeService userTypeService;
	
	private DocumentListTypeService documentListTypeService;
	
	private EmployeeDocumentService employeeDocumentService;
	

	@Autowired
	@Qualifier("BloodGroupServiceImpl")
	public void setBloodGroupService(BloodGroupService bloodGroupService) {
		this.bloodGroupService = bloodGroupService;
	}
	
	@Autowired
	@Qualifier("RoleTypeerviceImpl")
	public void setRoleTypeService(RoleTypeService roleTypeService) {
		this.roleTypeService = roleTypeService;
	}
	
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@Autowired
	@Qualifier("OrganizationServiceImpl")
	public void setOrganizationservices(OrganizationService organizationservices) {
		this.organizationservices = organizationservices;
	}

	@Autowired
	@Qualifier("CompanyServiceImpl")
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	@Autowired
	@Qualifier("EmployeeDetailsServiceImpl")
	public void setEmployeeDetailsService(
			EmployeeDetailsService employeeDetailsService) {
		this.employeeDetailsService = employeeDetailsService;
	}
	
	@Autowired
	@Qualifier("UserTypeServiceImpl")
	public void setUserTypeService(UserTypeService userTypeService) {
		this.userTypeService = userTypeService;
	}
	
	@Autowired
	@Qualifier("DocumentListTypeServiceImpl")
	public void setDocumentListTypeService(
			DocumentListTypeService documentListTypeService) {
		this.documentListTypeService = documentListTypeService;
	}

	@Autowired
	@Qualifier("EmployeeDocumentServiceImpl")
	public void setEmployeeDocumentService(
			EmployeeDocumentService employeeDocumentService) {
		this.employeeDocumentService = employeeDocumentService;
	}

	@RequestMapping(value="/bloodGroupList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String bloodGroupList(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<BloodGroup> bloodGroupList=bloodGroupService.loadBloodGroupList();
			if(bloodGroupList.size()>0)
			{
				for(BloodGroup obj : bloodGroupList)
				{
					json=new JSONObject();
					json.put("BLOOD_GROUP_ID", obj.getBloodGroupId());
					json.put("BLOOD_GROUP",obj.getBloodGroup());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return json_data_array.toString();
	}
	

	@RequestMapping(value="/loadRoleType",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadRoleType(
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<RoleType> roleTypeList=roleTypeService.loadRoleTypeList();
			if(roleTypeList.size()>0)
			{
				for(RoleType obj : roleTypeList)
				{
					json=new JSONObject();
					json.put("ROLE_TYPE_ID",obj.getRoleTypeId());
					json.put("ROLE_TYPE",obj.getRoleName());
					json_data_array.put(json);
				}
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/saveEmployee",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveEmployee(@RequestParam(value="eName",required=true) String eName,
			@RequestParam(value="MaritalStatus",required=true) String maritalStatus,
			@RequestParam(value="mobileNo",required=true) Long mobileNo,
			@RequestParam(value="currentAddress",required=true) String currentAddress,
			@RequestParam(value="PermanentAddress",required=true) String PermanentAddress,
			@RequestParam(value="sex",required=true) String sex,
			@RequestParam(value="sex",required=true) String qulification,
			@RequestParam(value="telephoneNo",required=true) Long telephoneNo,
			@RequestParam(value="emailId",required=true) String emailId,
			@RequestParam(value="pincode",required=true) String pincode,
			@RequestParam(value="panno",required=true) String panno,
			@RequestParam(value="basicSalary",required=true) Double basicSalary,
			@RequestParam(value="uname",required=true) String uname,
			@RequestParam(value="pwd",required=true) String pwd,
			@RequestParam(value="doj",required=true) String doj,
			@RequestParam(value="dob",required=true) String dob,
			@RequestParam(value="comapnyId",required=true) Integer comapnyId,
			@RequestParam(value="selectedOrganiztionNameId",required=true) Integer selectedOrganiztionNameId,
			@RequestParam(value="bloodroupName",required=true) Integer bloodroupName,
			@RequestParam(value="roleType",required=true) Integer roleType,
			@RequestParam(value="empImage") MultipartFile empImage,
			@RequestParam(value="AddressProofImage") MultipartFile AddressProofImage,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			// check user name already register
			UserDetails userDetailsObj=userDetailsService.checkUserNameAlreadyExitOrNot(uname);
			if(userDetailsObj==null)
			{
				DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
				EmployeeDetails empObj=new EmployeeDetails();
				empObj.setName(eName);
				empObj.setMaritalStats(maritalStatus);
				empObj.setMobileNo(mobileNo);
				empObj.setCurrentAddress(currentAddress);
				empObj.setPermanetAddress(PermanentAddress);
				empObj.setGender(sex);
				empObj.setQualification(qulification);
				empObj.setTelephone(telephoneNo);
				empObj.setEmailId(emailId);
				empObj.setPinCode(pincode);
				empObj.setPanNo(panno);
				empObj.setBasicSalary(basicSalary);
				empObj.setStatus(1);
				
				//set submit date
				//set organization submit date
				java.util.Date today = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
			    String IST = df.format(today);
			    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			    java.util.Date date = df2.parse(IST);
				empObj.setSubmitDate(date);
				//set DOB
				if(dob!="")
				{
					java.util.Date d1 = dtf.parse(dob);
					empObj.setDOB(new java.sql.Date(d1.getTime()));
				}
				if(doj!="")
				{
					java.util.Date d1 = dtf.parse(doj);
					empObj.setDOJ(new java.sql.Date(d1.getTime()));
				}
				// save employee image file
				byte[] bytes = empImage.getBytes();
				String orgNameFile = empImage.getOriginalFilename();
				int index = orgNameFile.indexOf(".");
				String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
				File serverFile = new File(Constant.IMAGE_PATH+ fileName);
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				empObj.setImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
				stream.write(bytes);
				stream.close();
				
				// save address proof image
				byte[] bytes1 = AddressProofImage.getBytes();
				String orgNameFile1 = empImage.getOriginalFilename();
				int index1 = orgNameFile.indexOf(".");
				String fileName1=UUID.randomUUID().toString()+orgNameFile1.substring(index1);
				File serverFile1 = new File(Constant.IMAGE_PATH+ fileName1);
				BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
				empObj.setAddressProofPath(Constant.DATABASE_IMAGE_PATH+ fileName1);
				stream1.write(bytes1);
				stream1.close();
				
				//load organization object and set organization
				empObj.setOrganization(organizationservices.loadOrganizationObjectUsingOrganiztionId(selectedOrganiztionNameId));
				
				//load company object and set company object
				empObj.setCompany(companyService.loadCompanyObjectUsingCompanyId(comapnyId));
				
				// load role type object and set
				empObj.setRoleType(roleTypeService.loadRoleTypeUsingRoleTypeId(roleType));
				
				// load blood group object
				empObj.setBloodGroup(bloodGroupService.loadBloodGroupObjectUsingBloodGroupId(bloodroupName));
				
				// save employee object
				Integer empDetailsId=employeeDetailsService.saveEmployeeDetails(empObj);
				if(empDetailsId!=null)
				{
					// save user details
					UserDetails userDetails=new UserDetails();
					userDetails.setUserName(uname);
					userDetails.setPassword(pwd);
					// load company object and set
					userDetails.setUserType(userTypeService.loadUserTypeUsingUserTypeId(Constant.COMPANY));
					// save user details object
					Integer userDetailsId=userDetailsService.saveUserDetails(userDetails);
					if(userDetailsId!=null)
					{
						// update employee object
						
						// load employee object based on employee details id
						EmployeeDetails employeeDetailsObj=employeeDetailsService.loadEmployeeObjectUsingEmployeeDetailsId(empDetailsId);
						employeeDetailsObj.setUserDetails(userDetailsService.loadUserDetailsUsingUserDetailsId(userDetailsId));
						employeeDetailsService.updateEmployeeDetails(employeeDetailsObj);
						
						json=new JSONObject();
						json.put("MSG","Employee added successfully");
						json_data_array.put(json);
						
					}
				}
				
				
			}
			else
			{
				json=new JSONObject();
				json.put("MSG","User Name already exists");
				json_data_array.put(json);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/listEmployeeDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listEmployeeDetails(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<EmployeeDetails> employeeDetailsList=employeeDetailsService.loadEmployeeDetailsListUsingOrganizationId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(employeeDetailsList.size()>0)
			{
				for(EmployeeDetails obj : employeeDetailsList)
				{
					json=new JSONObject();
					json.put("EMP_DETAILS_ID", obj.getEmployeeDetailsId());
					json.put("EMP_NAME", obj.getName());
					json.put("GENDER", obj.getGender());
					json.put("Qualification", obj.getQualification());
					json.put("Mobile_No", obj.getMobileNo());
					json.put("Email_ID", obj.getEmailId());
					json_data_array.put(json);
					
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/loadEmployeeList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadEmployeeListUsingBasedOnOrganizationId(
			@RequestParam(value="organizationId",required=true) Integer orgnizationId,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<EmployeeDetails> employeeDetailsList=employeeDetailsService.loadEmployeeDetailsListUsingOrganizationId(orgnizationId);
			if(employeeDetailsList.size()>0)
			{
				for(EmployeeDetails obj : employeeDetailsList)
				{
					json=new JSONObject();
					json.put("EMP_ID", obj.getEmployeeDetailsId());
					json.put("EMP_NAME",obj.getName());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return json_data_array.toString();
		
	}
	
	@RequestMapping(value="/saveEmployeeDocument",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveEmployeeDocument(
			@RequestParam(value="empUploadDocumetOrgId",required=true) Integer empUploadDocumetOrgId,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId,
			@RequestParam(value="employeeDetailsId",required=true) Integer employeeDetailsId,
			@RequestParam(value="documentName",required=true) MultipartFile file, 
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			EmployeeDocument empDocObj=new EmployeeDocument();
			empDocObj.setStatus(1);
			//set  submit date
			Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    Date date = df2.parse(IST);
		    empDocObj.setSubmitDate(date);
		    
		    // write image
		    // set the unique name
		    byte[] bytes = file.getBytes();
		    String orgNameFile = file.getOriginalFilename();
			int index = orgNameFile.indexOf(".");
			String fileName=UUID.randomUUID().toString()+orgNameFile.substring(index);
			File serverFile = new File(Constant.IMAGE_PATH+ fileName);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			empDocObj.setImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
			stream.write(bytes);
			stream.close();
			
			// set documet type list object
			empDocObj.setDocumentListType(documentListTypeService.loadDocumentListTypeUsingDocumentListTypeId(documentTypeId));
			
			// set organization object
			empDocObj.setOrganization(organizationservices.loadOrganizationObjectUsingOrganiztionId(empUploadDocumetOrgId));
			
			// set employee object
			empDocObj.setEmployeeDetails(employeeDetailsService.loadEmployeeObjectUsingEmployeeDetailsId(employeeDetailsId));
			
			// save employee document object
			Integer employeeDocumetId=employeeDocumentService.saveEmployeeDocument(empDocObj);
			if(employeeDocumetId!=null)
			{
				json=new JSONObject();
				json.put("MSG", "Employee document added successfully");
				json_data_array.put(json);
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}

	@RequestMapping(value="/employeeDocumentList",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String employeeDocumentList(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<EmployeeDocument> employeeDocumentsList=employeeDocumentService.loadEmployeeDocumetListUsingOrganizationId((Integer)request.getSession().getAttribute("ORGANIZATION_ID"));
			if(employeeDocumentsList.size()>0)
			{
				for(EmployeeDocument obj : employeeDocumentsList)
				{
					json=new JSONObject();
					json.put("EMP_DOCUMENT_ID", obj.getEmployeeDocumentId());
					json.put("ORG_NAME", obj.getOrganization().getOrganizationName());
					json.put("EMP_NAME", obj.getEmployeeDetails().getName());
					json.put("DOCUMENT_TYPE", obj.getDocumentListType().getDocumentName());
					json.put("FILE_NAME", obj.getImagePath().substring(obj.getImagePath().length() - 40));
					json_data_array.put(json);
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
}
