/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.InwardCourierRegistration;
import com.protocol.wms.model.InwardCourierRegistrationDocketFilePath;
import com.protocol.wms.model.OutWardReturnGoodsCreditNote;
import com.protocol.wms.model.Particulars;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.service.InwardCourierService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/InwardCourierController")
public class InwardCourierController {

	private Logger log = Logger.getLogger(InwardCourierController.class.getClass());
	@Autowired
	private HttpSession session;
	@Autowired
	@Qualifier("InwardCourierServiceImpl")
	private	InwardCourierService inwardCourierService;
	
	@RequestMapping(value="/getParticularsDropdownList",method=RequestMethod.POST)
	private  @ResponseBody String getParticularsDropdownList(@RequestParam(value="distId",required=false) Integer distId){
		JSONObject result = null;
		JSONArray particularsArr = null;
		List<Particulars> particularListing = null;
		try{
			particularListing = inwardCourierService.getParticularsDropdownList();
			particularsArr = new JSONArray();
			for(Particulars tempParticulars :particularListing){
				result = new JSONObject();
				result.put("PARTICULAR_ID",tempParticulars.getParticularsId());
				result.put("PARTICULAR_NAME",tempParticulars.getParticularsName());
				particularsArr.put(result);
			}
			result = new JSONObject();
			result.put("particularsArr", particularsArr);
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return result.toString();
	}
	@RequestMapping(value="/addInwardCourierRegistration",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String addInwardCourierRegistration(@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="courierRegDate",required=false) String courierRegistrationDate ,@RequestParam(value="state",required=false) Integer state,
			@RequestParam(value="district",required=false) Integer district ,@RequestParam(value="city",required=false) Integer city,
			@RequestParam(value="courierName",required=false) String courierName ,@RequestParam(value="deliveryPerson",required=false) String deliveryPerson,
			@RequestParam(value="time",required=false) String time ,@RequestParam(value="docketNo",required=false) String docketNo,
			@RequestParam(value="docketFile",required=false)  MultipartFile docketFile[] ,@RequestParam(value="remark",required=false) String remark,
			@RequestParam(value="particular",required=false) Integer particular ,@RequestParam(value="companyBank",required=false) Integer companyBankId,
			@RequestParam(value="receivedDate",required=false) String courierReceivedDate ,@RequestParam(value="stockistId",required=false) Integer stockistId,
			@RequestParam(value="chequeNo",required=false) String chequeNo[],@RequestParam(value="companyStockistOption",required=false) String companyStockistOption,
			@RequestParam(value="other",required=false) String other){
		
		JSONObject result = null;
		InwardCourierRegistration inwardCourierRegistration = null;
		AdvancedChequeInformation advancedChequeInformation = null;
		List<ChequeNumberInfo> chequeNumberInfoList = null;
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		//DateFormat dtf2 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date d1= null;
		List<InwardCourierRegistrationDocketFilePath> docketFileList = null;
		InwardCourierRegistrationDocketFilePath tempDocketFileObj = null;
		try{
			result = new JSONObject();
			result.put("result", false);
			inwardCourierRegistration = new InwardCourierRegistration();
			//
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
			java.util.Date d5 = format.parse(time);
			java.sql.Time	ppstime = new java.sql.Time(d5.getTime());
		    docketFileList = new ArrayList<InwardCourierRegistrationDocketFilePath>();
			for(MultipartFile file : docketFile){
				if(file!=null){
					tempDocketFileObj = new InwardCourierRegistrationDocketFilePath();
					tempDocketFileObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					docketFileList.add(tempDocketFileObj);
				}
			}
			inwardCourierRegistration.setInwardCourierRegistrationDocketFilePathList(docketFileList);
			if(companyBankId!=null){
				advancedChequeInformation = new AdvancedChequeInformation();
				advancedChequeInformation.setStatus(1);
				advancedChequeInformation.setSubmitDate(CommonMethods.setSubmitedDate());
				d1 = dtf.parse(courierReceivedDate);
				advancedChequeInformation.setCourierReceivedDate(new java.sql.Date(d1.getTime()));
				
				chequeNumberInfoList = new ArrayList<ChequeNumberInfo>();
				for(String temp : chequeNo){
					if(!"".equals(temp)&&!temp.equals(null)){
						ChequeNumberInfo tempObj  = null;
						tempObj = new ChequeNumberInfo();
						tempObj.setChequeNumber(temp);
						tempObj.setStatus(1);
						chequeNumberInfoList.add(tempObj);
					}
				}
				advancedChequeInformation.setNoOfCheque(chequeNumberInfoList.size());
			}
			
			inwardCourierRegistration.setCourierName(courierName);
			d1 = dtf.parse(courierRegistrationDate);
			inwardCourierRegistration.setCourierRegistrationDate(new java.sql.Date(d1.getTime()));
			inwardCourierRegistration.setDeliveryPerson(deliveryPerson);
			inwardCourierRegistration.setDocketNo(docketNo);
			inwardCourierRegistration.setRemark(remark);
			inwardCourierRegistration.setSubmitDate(CommonMethods.setSubmitedDate());
			inwardCourierRegistration.setStatus(1);
			inwardCourierRegistration.setTime(ppstime);
			inwardCourierRegistration.setOther(other);
			if(inwardCourierService.addInwardCourierRegistration(companyStockistOption,orgId,companyId ,state ,district ,city ,stockistId ,particular ,companyBankId,inwardCourierRegistration,advancedChequeInformation,chequeNumberInfoList)){
				result.put("result", true);
			}
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			try {
				result.put("result", false);
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	@RequestMapping(value="/editInwardCourierRegistration",method=RequestMethod.POST,produces="application/json")
	private @ResponseBody String editInwardCourierRegistration(@RequestParam(value="courierId",required=false) Integer inwardCourierRegId,
			@RequestParam(value="orgName",required=true) Integer orgId,@RequestParam(value="companyName",required=false) Integer companyId,
			@RequestParam(value="courierRegDate",required=true) String courierRegistrationDate ,@RequestParam(value="state",required=false) Integer state,
			@RequestParam(value="district",required=false) Integer district ,@RequestParam(value="city",required=false) Integer city,
			@RequestParam(value="courierName",required=true) String courierName ,@RequestParam(value="deliveryPerson",required=true) String deliveryPerson,
			@RequestParam(value="time",required=true) String time ,@RequestParam(value="docketNo",required=true) String docketNo,
			//@RequestParam(value="docketFile",required=false)  MultipartFile docketFile ,
			@RequestParam(value="remark",required=true) String remark,
			@RequestParam(value="particular",required=false) Integer particular ,@RequestParam(value="companyBank",required=false) Integer companyBankId,
			@RequestParam(value="receivedDate",required=false) String courierReceivedDate ,@RequestParam(value="stockistId",required=false) Integer stockistId,
			@RequestParam(value="chequeNo",required=false) String chequeNo[],@RequestParam(value="option",required=false) String companyStockistOption,
			@RequestParam(value="other",required=false) String other){
		
		JSONObject result = null;
		InwardCourierRegistration inwardCourierRegistration = null;
		AdvancedChequeInformation advancedChequeInformation = null;
		List<ChequeNumberInfo> chequeNumberInfoList = null;
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		try{
			
			inwardCourierRegistration = inwardCourierService.loadInwardCourierRegistrationObjectUsignInwardCourierRegId(inwardCourierRegId);
			//
			SimpleDateFormat format = new SimpleDateFormat("hh : mm : a");
			java.util.Date d5 = format.parse(time);
			java.sql.Time	ppstime = new java.sql.Time(d5.getTime());
		    /*if(docketFile!= null){
				inwardCourierRegistration.setDocketFileImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(docketFile));
		    }*/
			if(companyBankId!=null){
				advancedChequeInformation = new AdvancedChequeInformation();
				advancedChequeInformation.setStatus(1);
				advancedChequeInformation.setSubmitDate(inwardCourierRegistration.getSubmitDate());
				d1 = dtf.parse(courierReceivedDate);
				advancedChequeInformation.setCourierReceivedDate(new java.sql.Date(d1.getTime()));
				
				chequeNumberInfoList = new ArrayList<ChequeNumberInfo>();
				for(String temp : chequeNo){
					if(!"".equals(temp)&&!temp.equals(null)){
						ChequeNumberInfo tempObj  = null;
						tempObj = new ChequeNumberInfo();
						tempObj.setChequeNumber(temp);
						tempObj.setStatus(1);
						chequeNumberInfoList.add(tempObj);
					}
				}
				advancedChequeInformation.setNoOfCheque(chequeNumberInfoList.size());
			}
			
			inwardCourierRegistration.setCourierName(courierName);
			d1 = dtf.parse(courierRegistrationDate);
			inwardCourierRegistration.setCourierRegistrationDate(new java.sql.Date(d1.getTime()));
			inwardCourierRegistration.setDeliveryPerson(deliveryPerson);
			inwardCourierRegistration.setDocketNo(docketNo);
			inwardCourierRegistration.setRemark(remark);
			inwardCourierRegistration.setStatus(1);
			inwardCourierRegistration.setTime(ppstime);
			inwardCourierRegistration.setOther(other);
			result = inwardCourierService.editInwardCourierRegistration(companyStockistOption,orgId,companyId ,state ,district ,city ,stockistId ,particular ,companyBankId,inwardCourierRegistration,advancedChequeInformation,chequeNumberInfoList);
		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			try {
				result = new JSONObject();
				result.put("MSG", "Operation failed....!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	//
	@RequestMapping(value="/updateInwardCourierRegDocketFileImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateInwardCourierRegDocketFileImage(
			@RequestParam(value="docketFile",required=true) MultipartFile docketFile[],
			@RequestParam(value="registrationId",required=true) Integer inwardCourierRegId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<InwardCourierRegistrationDocketFilePath> docketFileImgList = null;
		InwardCourierRegistrationDocketFilePath tempdocketFileImgPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(docketFile!=null&&!(docketFile.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write lr image
			docketFileImgList  = new ArrayList<InwardCourierRegistrationDocketFilePath>();
			for(MultipartFile file : docketFile){
				if(file!=null){
					tempdocketFileImgPath = new InwardCourierRegistrationDocketFilePath();
					tempdocketFileImgPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					docketFileImgList.add(tempdocketFileImgPath);
				}
			}
			json_ImageArray = inwardCourierService.updateInwardCourierRegDocketFileImage(inwardCourierRegId, docketFileImgList);
			result.put("MSG", "Docket File Upload successful...!");
			result.put("DOCKET_FILE", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	@RequestMapping(value="/inwardCourierRegistrationListing",method=RequestMethod.POST)
	private  @ResponseBody String inwardCourierRegistrationListing(){
		JSONObject result = null;
		JSONArray inwardCourierRegArr = null;
		List<InwardCourierRegistration> inwardCourierRegList = null;
		try{
			inwardCourierRegArr = new JSONArray();
			Integer organizationId = (Integer)session.getAttribute("ORGANIZATION_ID");
			
			inwardCourierRegList = inwardCourierService.inwardCourierRegistrationListing(organizationId);
			for(InwardCourierRegistration tempCourierReg :inwardCourierRegList){
				Boolean stat = true;
				Stockist stockist = tempCourierReg.getStockist();
				if(stockist!=null){
					if(!(stockist.getStatus()==1))
						stat=false;
				}
				if(stat){
					result = new JSONObject();
					result.put("COURIER_ID",tempCourierReg.getInwardCourierRegistrationId());
					result.put("COURIER_NAME",tempCourierReg.getCourierName());
					result.put("REG_DATE",tempCourierReg.getCourierRegistrationDate());
					result.put("ORG_NAME",tempCourierReg.getOrganization().getOrganizationName());
					if(tempCourierReg.getCompany()==null)
						result.put("COMPANY","-");
					else
						result.put("COMPANY",tempCourierReg.getCompany().getCompanyName());
					String stocistName="-";
					if(stockist!=null)
						stocistName=stockist.getStockistName();
					result.put("STOCKIST_NAME",stocistName);
					result.put("CITY",tempCourierReg.getCity().getCityName());
					result.put("DOCKET_NO",tempCourierReg.getDocketNo());
					inwardCourierRegArr.put(result);
				}
			}
			result = new JSONObject();
			result.put("inwardCourierRegArr", inwardCourierRegArr);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	
	@RequestMapping(value="/searchInwardCourierRegistrationDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchInwardCourierRegistrationDetails(
			@RequestParam(value="searchOption",required=false) String selectedValue,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="fromSpecialData",required=false) String fromSpecialData,
			@RequestParam(value="toSpecialData",required=false) String toSpecialData,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		JSONObject result = null;
		JSONArray inwardCourierRegArr = null;
		java.util.Map<Object, Object>map = null;
		try{
			inwardCourierRegArr = new JSONArray();
			Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
//			System.out.println("OrganizationId : "+(Integer)session.getAttribute("ORGANIZATION_ID")+" : "+orgId);
			map = inwardCourierService.searchInwardCourierRegistrationDetails(selectedValue,searchText,fromSpecialData,toSpecialData,(Integer)session.getAttribute("ORGANIZATION_ID"),from);
			List<InwardCourierRegistration> inwardCourierRegList = (List<InwardCourierRegistration>) map.get("InwardCourierRegistration");
		
			Integer totalPage = (Integer) map.get("totalPages");
			for(InwardCourierRegistration tempCourierReg :inwardCourierRegList)
			{
				//Boolean stat = true;
				Stockist stockist = tempCourierReg.getStockist();
				/*if(stockist!=null)
				{
					if(!(stockist.getStatus()==1))
						stat=false;
				}
				if(stat){*/
					result = new JSONObject();
					result.put("COURIER_ID",tempCourierReg.getInwardCourierRegistrationId());
					result.put("COURIER_NAME",tempCourierReg.getCourierName());
					result.put("ORG_NAME",tempCourierReg.getOrganization().getOrganizationName());
					result.put("REG_DATE",tempCourierReg.getCourierRegistrationDate());
					if(tempCourierReg.getCompany()!=null)
						result.put("COMPANY",tempCourierReg.getCompany().getCompanyName());
					else
						result.put("COMPANY","-");
					String stocistName="-";
					if(stockist!=null)
						stocistName=stockist.getStockistName();
					result.put("STOCKIST_NAME",stocistName);
					result.put("CITY",tempCourierReg.getCity().getCityName());
					result.put("DOCKET_NO",tempCourierReg.getDocketNo());
					inwardCourierRegArr.put(result);
				//}
			}
			result=new JSONObject();
		result.put("inwardCourierRegArr", inwardCourierRegArr);
			result.put("paginationCount", totalPage);
			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		
		return result.toString();
	}
	//Delete Stockist Details
	@RequestMapping(value="/deleteInwardCourierReg",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardCourierReg(@RequestParam(value="inwardCourierRegId",required=true) Integer inwardCourierRegId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(inwardCourierService.deleteInwardCourierRegDetails(inwardCourierRegId)){
				result.put("MSG","Inward Courier Registration Details Deleted Successfully");
			}else{
				result.put("MSG","Inward Courier Registration Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
				}catch(Exception e1){}
			}
			
			return result.toString();
		}
	//
	@RequestMapping(value="/deleteInwardCourierRegistrationDocketFileImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteInwardCourierRegistrationDocketFileImage(@RequestParam(value="docketFileImageId",required=false) Integer docketFileImageId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(inwardCourierService.deleteInwardCourierRegistrationDocketFileImage(docketFileImageId)){
				result.put("RESULT",true);
				result.put("MSG","Inward Courier Registration Docket File Image Deleted Successfully.....!");
			}else{
				result.put("MSG","Inward Courier Registration Docket File Image Deletion Failed.....!");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
//  Add by nutan
  @RequestMapping(value="/viewInwardCourierRegistration",method=RequestMethod.POST,produces="application/json")
  public @ResponseBody String viewInwardCourierRegistration(@RequestParam(value="inwardCourierRegId",required=true) Integer inwardCourierRegId,
		  @RequestParam(value="editStatus",required=false) Boolean editStatus,
			HttpServletRequest request)
  {
      JSONArray json_data_array= null;
      try
      {
    	  json_data_array = inwardCourierService.loadInwardCourierRegistrationViewUsignInwardCourierRegId(editStatus, inwardCourierRegId);
      }
      catch (Exception e)
      {
          e.printStackTrace();
          log.error(e);
      }
      return json_data_array.toString();
  }
}
