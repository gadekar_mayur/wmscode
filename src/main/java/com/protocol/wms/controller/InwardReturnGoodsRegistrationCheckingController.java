/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.CheckingDoneCheckingSlipImagePath;
import com.protocol.wms.model.CreditNote;
import com.protocol.wms.model.CreditNoteCreditNoteImagePath;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnProducts;
import com.protocol.wms.service.InwardReturnGoodsCheckingService;

/**
 * @author Sudhakar
 *
 */
@Controller
@RequestMapping(value="/InwardReturnGoodsRegistrationCheckingController")
public class InwardReturnGoodsRegistrationCheckingController {

	private Logger log = Logger.getLogger(InwardReturnGoodsRegistrationCheckingController.class.getClass());
	
	private InwardReturnGoodsCheckingService inwardReturnGoodsCheckingService;

	@Autowired
	@Qualifier("InwardReturnGoodsCheckingServiceImpl")
	public void setInwardReturnGoodsCheckingService(
			InwardReturnGoodsCheckingService inwardReturnGoodsCheckingService) {
		this.inwardReturnGoodsCheckingService = inwardReturnGoodsCheckingService;
	}
	
	
	@RequestMapping(value="/listInwardReturnGoodsRegistration",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listInwardReturnGoodsRegistration(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList=inwardReturnGoodsCheckingService.loadInwardReturnGoodsRegistrationListUsingOrganizationId((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(inwardReturnGoodsRegistrationList.size()>0)
			{
				for(InwardReturnGoodsRegistration  obj : inwardReturnGoodsRegistrationList)
				{
					json=new JSONObject();
					json.put("Inward_Return_Goods_Registration_id", obj.getInwardReturnGoodRegistrationId());
					json.put("ID_NO", obj.getInwardReturnGoodRegistrationId());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("ORG_ID", obj.getOrganization().getOrganizationId());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("STOCKIST_ID",obj.getStockist().getStockistId());
					json.put("TRAN", obj.getTransporterDetails().getTransporterName());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	@RequestMapping(value="/saveCheckingDone",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveCheckingDone(
			@RequestParam(value="selectedOrganiztionNameId",required=true) Integer selectedOrganiztionNameId,
			@RequestParam(value="CheckingSlip",required=true) MultipartFile file[],
			@RequestParam(value="date",required=true) String date,
			@RequestParam(value="time",required=true) String time,
			@RequestParam(value="stockistId",required=true) Integer stockistId,
			@RequestParam(value="remark",required=true) String remark,
			@RequestParam(value="employeeDetailsId",required=true) Integer employeeDetailsId,
			@RequestParam(value="Inward_Return_Goods_Registration_id",required=true) Integer Inward_Return_Goods_Registration_id,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		List<CheckingDoneCheckingSlipImagePath> chekingSlipList = null;
		CheckingDoneCheckingSlipImagePath checkingSlipImagePath = null;
		try 
		{
			DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
			CheckingDone chObj=new CheckingDone();
			// set submit date
			java.util.Date today = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		    String IST = df.format(today);
		    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		    java.util.Date date1 = df2.parse(IST);
		    chObj.setSubmitDate(date1);
		    chObj.setRemark(remark);
		    // set check done date
		    java.util.Date d1 = (java.util.Date) dtf.parse(date);
		    chObj.setCheckingDoneDate(new java.sql.Date(d1.getTime()));
		    // set time
		    // set time
		 	SimpleDateFormat format = new SimpleDateFormat("hh : mm : a"); // 12 hour format
		 	java.util.Date time1 =(java.util.Date)format.parse(time);
		 	java.sql.Time ppstime = new java.sql.Time(time1.getTime());
		 	chObj.setTime(ppstime); 
		 	// set organization
		 	chObj.setOrganization(inwardReturnGoodsCheckingService.loadOrganizationObjectUsingOrganiztionId(selectedOrganiztionNameId));
			// set stockist
		 	chObj.setStockist(inwardReturnGoodsCheckingService.loadStockistObjUsigStokistId(stockistId));
		 	// set company object 
		 	chObj.setEmployeeDetails(inwardReturnGoodsCheckingService.loadEmployeeObjectUsingEmployeeDetailsId(employeeDetailsId));
		 	
		 	chekingSlipList = new ArrayList<CheckingDoneCheckingSlipImagePath>();
			// write lr image
			for(MultipartFile file1 : file){
				if(file!=null){
					checkingSlipImagePath = new CheckingDoneCheckingSlipImagePath();
					checkingSlipImagePath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file1));
					chekingSlipList.add(checkingSlipImagePath);
				}
			}
		 	chObj.setCheckingDoneCheckingSlipImagePathList(chekingSlipList);
			// set Inward Return Goods Registration
			chObj.setInwardReturnGoodsRegistration(inwardReturnGoodsCheckingService.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(Inward_Return_Goods_Registration_id));
		 	// save checking object
			Integer checkingDoneId=inwardReturnGoodsCheckingService.saveCheckingDone(chObj);
			if(checkingDoneId!=null)
			{
				// update Inward Return Goods Registration
				InwardReturnGoodsRegistration inwardReturnGoodsRegistrationObj=inwardReturnGoodsCheckingService.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(Inward_Return_Goods_Registration_id);
				inwardReturnGoodsRegistrationObj.setCheckingDoneStatus(1);
				// update object
				inwardReturnGoodsCheckingService.updateInwardReturnGoodsRegistrationObj(inwardReturnGoodsRegistrationObj);
				
				json=new JSONObject();
				json.put("MSG", "Checking Done successfuly");
				json_data_array.put(json);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
		
	}
	
	@RequestMapping(value="/listInwardReturnGoodsRegistrationWithCheckingDoneStausOne",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String listInwardReturnGoodsRegistrationWithCheckingDoneStausOne(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList=inwardReturnGoodsCheckingService.loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOne((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(inwardReturnGoodsRegistrationList.size()>0)
			{
				for(InwardReturnGoodsRegistration  obj : inwardReturnGoodsRegistrationList)
				{
					json=new JSONObject();
					json.put("Inward_Return_Goods_Registration_id", obj.getInwardReturnGoodRegistrationId());
					json.put("ID_NO", obj.getInwardReturnGoodRegistrationId());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("TRAN", obj.getTransporterDetails().getTransporterName());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/saveCreditNote",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String saveCreditNote(
			@RequestParam(value="Selected_Inward_Return_Goods_Registration_id",required=true) String Selected_Inward_Return_Goods_Registration_id,
			@RequestParam(value="creditNoteNo",required=true) String creditNoteNo,
			@RequestParam(value="creditNoteAmount",required=true) Float creditNoteAmount,
			@RequestParam(value="remark",required=true) String remark,
			@RequestParam(value="creditNoteImage",required=true) MultipartFile file[])
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		List<CreditNoteCreditNoteImagePath> creditNoteImgList = null;
		CreditNoteCreditNoteImagePath creditNoteImgPathObj = null;
		try 
		{
			// save credit note object
			CreditNote cnObj=new CreditNote();
			cnObj.setCreditNoteAmount(creditNoteAmount);
			cnObj.setCreditNoteNo(creditNoteNo);
			cnObj.setRemark(remark);
			cnObj.setStatus(1);
		    cnObj.setSubmitDate(CommonMethods.setSubmitedDate());
		    
		    creditNoteImgList = new ArrayList<CreditNoteCreditNoteImagePath>();
			// write lr image
			for(MultipartFile file1 : file){
				if(file!=null){
					creditNoteImgPathObj = new CreditNoteCreditNoteImagePath();
					creditNoteImgPathObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file1));
					creditNoteImgList.add(creditNoteImgPathObj);
				}
			}
			cnObj.setCreditNoteCreditNoteImagePathList(creditNoteImgList);
			// save credit note object
			Integer creditNoteId=inwardReturnGoodsCheckingService.saveCreditNoteObj(cnObj);
			if(creditNoteId!=null)
			{
				// update inward Return Goods
				String Inward_Return_Goods_Registration_id[]=Selected_Inward_Return_Goods_Registration_id.split(",");
				for(int i=0;i<Inward_Return_Goods_Registration_id.length;i++)
				{
					// load Inward_Return_Goods_Registration_object
					InwardReturnGoodsRegistration inwardReturnGoodsRegistrationObj=inwardReturnGoodsCheckingService.loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(Integer.parseInt(Inward_Return_Goods_Registration_id[i]));
					inwardReturnGoodsRegistrationObj.setCreditNoteStatus(1);
					// load credit note object and set
					inwardReturnGoodsRegistrationObj.setCreditNote(inwardReturnGoodsCheckingService.loadCreditNoteObjectUsingCreditNoteId(creditNoteId));
					// update inward Return Goods Registration Obj
					inwardReturnGoodsCheckingService.updateInwardReturnGoodsRegistrationObj(inwardReturnGoodsRegistrationObj);
				}
				
			}
			json=new JSONObject();
			json.put("MSG", "Credit Note added successfuly");
			json_data_array.put(json);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne(HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList=inwardReturnGoodsCheckingService.loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne((Integer) request.getSession().getAttribute("ORGANIZATION_ID"));
			if(inwardReturnGoodsRegistrationList.size()>0)
			{
				for(InwardReturnGoodsRegistration  obj : inwardReturnGoodsRegistrationList)
				{
					json=new JSONObject();
					json.put("Inward_Return_Goods_Registration_id", obj.getInwardReturnGoodRegistrationId());
					json.put("ID_NO", obj.getInwardReturnGoodRegistrationId());
					json.put("ORG", obj.getOrganization().getOrganizationName());
					json.put("COMPANY", obj.getCompany().getCompanyName());
					json.put("STOCKIST", obj.getStockist().getStockistName());
					json.put("TRAN", obj.getTransporterDetails().getTransporterName());
					json_data_array.put(json);
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
	@RequestMapping(value="/addProductsToInwardReturnGoodsLRInCheckingPending",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String addProductsToInwardReturnGoodsLRInCheckingPending(@RequestParam(value="lrId",required=false) Integer lrId,
			@RequestParam(value="productName",required=false) String[] productName,@RequestParam(value="claimQuantity",required=false) Integer[] claimQuantity,
			@RequestParam(value="receivedQuantity",required=false) Integer[] receivedQuantity,@RequestParam(value="batch",required=false) String[] batch,
			@RequestParam(value="mfgDate",required=false) String[] mfgDate,@RequestParam(value="expiryDate",required=false) String[] expiryDate,
			@RequestParam(value="mfgCompany",required=false) String[] mfgCompany,@RequestParam(value="reason",required=false) String[] reason,
			@RequestParam(value="quantityVariance",required=false) Integer[] quantityVariance) throws IOException
	{
		InwardReturnProducts inwardReturnProducts = null;
		List<InwardReturnProducts> inwardReturnProductsList = null;
		//Format Date
		DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date d1= null;
		JSONObject result = null;
		try{
			result = new JSONObject();
			result.put("result", false);
			result.put("MSG", "Inward Return Goods Product Registration Failed....!");
			inwardReturnProductsList = new ArrayList<InwardReturnProducts>();
			for(int i=0;i<(productName.length-1);i++){
				inwardReturnProducts = new InwardReturnProducts();
				inwardReturnProducts.setProductName(productName[i]);
				inwardReturnProducts.setClaimQuatity(claimQuantity[i]);
				inwardReturnProducts.setReceivedQuantity(receivedQuantity[i]);
				inwardReturnProducts.setBatch(batch[i]);
				d1 = dtf.parse(mfgDate[i]);
				inwardReturnProducts.setMfgDate(new java.sql.Date(d1.getTime()));
				d1 = dtf.parse(expiryDate[i]);
				inwardReturnProducts.setExpiryDate(new java.sql.Date(d1.getTime()));
				inwardReturnProducts.setMfgCompany(mfgCompany[i]);
				inwardReturnProducts.setReason(reason[i]);
				inwardReturnProducts.setQuantityVariance(quantityVariance[i]);
				inwardReturnProducts.setStatus(1);
				inwardReturnProductsList.add(inwardReturnProducts);
			}
			if(inwardReturnGoodsCheckingService.addProductsToInwardReturnGoodsLRInCheckingPending(lrId,inwardReturnProductsList)){
				result.put("result", true);
				result.put("MSG", "Inward Return Goods Product Registration Successful....!");
			}

		}
		catch(Exception e){
			try{
				result = new JSONObject();
				result.put("result", false);
				result.put("MSG", "Operation Failed....!");
			}catch(JSONException j1){log.error(j1);}
			e.printStackTrace();
			log.error(e);
		}
	return result.toString();
	}//  
}
