/**
 * 
 */
package com.protocol.wms.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyDocumentImagePath;
import com.protocol.wms.model.DrugLicence;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistDocument;
import com.protocol.wms.model.StockistDocumentImagePath;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.model.UserType;
import com.protocol.wms.service.StockistService;
import com.protocol.wms.service.UserDetailsService;

/**
 * @author admin
 *
 */
@Controller
@RequestMapping(value="/StockistController")

public class StockistController {
	
	private Logger log = Logger.getLogger(StockistController.class.getClass());
	@Autowired
	private HttpSession session;
	@Autowired
	@Qualifier("StockistServiceImpl")
	private StockistService stockistService;
	
	@Autowired
	@Qualifier("UserDetailsServiceImpl")
	private UserDetailsService userDetailsService;
	
	@RequestMapping(value="/displayStockistDetailsForm",method=RequestMethod.POST)
	private  @ResponseBody String displayStockistDetailsForm(){
		return stockistService.displayStockistDetailsForm((Integer)session.getAttribute("ORGANIZATION_ID")).toString();
	}
	
	@RequestMapping(value="/organizationListDropdown",method=RequestMethod.POST)
	private  @ResponseBody String organizationListDropdown(){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		return stockistService.organizationListDropdown(orgId).toString();
	}
	
	/*@RequestMapping(value="/getDistrict",method=RequestMethod.POST)
	private  @ResponseBody String getDistrictList(@RequestParam(value="stateId",required=false) Integer stateId){
		
		return stockistService.getDistrictList(stateId).toString();
	}*/
	
	/*@RequestMapping(value="/getCities",method=RequestMethod.POST)
	private  @ResponseBody String getCityList(@RequestParam(value="distId",required=false) Integer distId){
		
		return stockistService.getCityList(distId).toString();
	}*/
	
	@RequestMapping(value="/getStockistByOrg",method=RequestMethod.POST)
	private  @ResponseBody String getStockistByOrgganization(@RequestParam(value="orgId",required=false) Integer orgId){
		return stockistService.getStockistByOrganization(orgId).toString();
	} 
	
	@RequestMapping(value="/addCompanyToStockist",method=RequestMethod.POST)
	private  @ResponseBody String addCompanyToStockist(@RequestParam(value="companies",required=false) Integer[] companies,@RequestParam(value="stockist",required=false) Integer stockistId,
			@RequestParam(value="orgName",required=false) Integer orgId ){
		return stockistService.addCompanyToStockist(companies,stockistId,orgId).toString();
	} 
	//
	@RequestMapping(value="/addCompanyToStockistList",method=RequestMethod.POST)
	private  @ResponseBody String stockistListingAccToOrg(){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		return stockistService.stockistListingAccToOrg(orgId).toString();
	}
	//
	@RequestMapping(value="/searchOrganizationStockist",method=RequestMethod.POST)
	private  @ResponseBody String searchOrganizationStockist(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=false) Integer from){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		return stockistService.searchOrganizationStockist(searchOption,searchText,orgId,from).toString();
	}
	@RequestMapping(value="/getStockist",method=RequestMethod.POST)
	private  @ResponseBody String getStockistByOrgAndCompany(@RequestParam(value="orgId",required=false) Integer orgId,
			@RequestParam(value="compId",required=false) Integer companyId){
		if(companyId==null)
			companyId=0;
		
		return stockistService.getStockistByOrgAndCompany(orgId,companyId).toString();
	}
	
	@RequestMapping(value="/getStokistList",method=RequestMethod.POST)
	private  @ResponseBody String getStokistList(){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		return stockistService.getStokistList(orgId).toString();
	}
	@RequestMapping(value="/searchStockistDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchStockistDetails(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=false) Integer from){
		return stockistService.searchStockistDetails(searchOption,searchText,(Integer)session.getAttribute("ORGANIZATION_ID"),from).toString();
	}
	//Add Stockist
	@RequestMapping(value="/saveStockist",method=RequestMethod.POST)
	private  @ResponseBody String saveStockist(
			@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="stockistName",required=false) String stockistName,
			@RequestParam(value="city",required=false) Integer cityId,@RequestParam(value="state",required=false) Integer stateId,
			@RequestParam(value="email",required=false) String email,@RequestParam(value="contactPerson",required=false) String contactPerson,
			@RequestParam(value="location",required=false) String location,@RequestParam(value="vat",required=false) String vat,
			@RequestParam(value="panNo",required=false) String panNo,@RequestParam(value="company",required=false) Integer companyId,
			@RequestParam(value="address",required=false) String address,@RequestParam(value="district",required=false) Integer districtId,
			@RequestParam(value="mobile",required=false) Long mobile,@RequestParam(value="fax",required=false) String fax,
			@RequestParam(value="pro_part",required=false) String stockistPartner,@RequestParam(value="transporter",required=false)Integer transporterDetailsId,
			@RequestParam(value="cst",required=false) String cst,@RequestParam(value="uname",required=false) String userName,@RequestParam(value="password",required=false) String password,
			@RequestParam(value="license[]",required=false) String[] license,@RequestParam(value="frmDate[]",required=false) String[] frmDate,
			@RequestParam(value="toDate[]",required=false) String[] toDate,@RequestParam(value="validities[]",required=false) String[] validities,HttpServletRequest request)
			{
				Stockist stockist=null;
				UserDetails userDetails=null;
				List<DrugLicence> drugLicenceList=null;
				DrugLicence drugLicence = null;
				DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
//				DateFormat dtf2 = new SimpleDateFormat("yyyy-MM-dd");
				try{
					userDetails = userDetailsService.checkUserNameAlreadyExitOrNot(userName);
					if(userDetails!=null){
						JSONObject result = new JSONObject();
						result.put("MSG","User Name already exists");
						result.put("FLAG", true);
						return result.toString();
					}
					stockist = new Stockist();
					stockist.setStatus(1);
					stockist.setStockistAddress(address);
					stockist.setStockistName(stockistName);
					stockist.setStockistVAT(vat);
					stockist.setStockistPartner(stockistPartner);
					stockist.setStockistPanNo(panNo);
					stockist.setStockistName(stockistName);
					stockist.setStockistMobileNo(mobile);
					stockist.setStockistLocation(location);
					stockist.setStockistFaxNo(fax);
					stockist.setStockistContactPerson(contactPerson);
					stockist.setStockistCST(cst);
					stockist.setStockistEmailId(email);
					drugLicenceList = new ArrayList<DrugLicence>();
					
					//Date temp=null;
					for(int i=0;i<license.length;i++){
						java.util.Date d1= null;
						//System.out.println(license[i]);
						
						d1 = dtf.parse(frmDate[i]);
						drugLicence = new DrugLicence();
						drugLicence.setDrugLicenceNo(license[i]);
						drugLicence.setFromDate(new java.sql.Date(d1.getTime()));
						d1 = dtf.parse(toDate[i]);
						drugLicence.setToDate(new java.sql.Date(d1.getTime()));
						drugLicence.setValidity(validities[i]);
						drugLicenceList.add(drugLicence);
						//System.out.println(validities[i]);
					}
					stockist.setDrugLicenceList(drugLicenceList);
					if(!"".equals(userName)&&!"".equals(password)&&userName!=null&&password!=null){
						userDetails = new UserDetails();
						userDetails.setUserName(userName);
						userDetails.setPassword(password);
					}
				}catch(Exception e){
					e.printStackTrace();log.error(e);
					log.error(e);
				}
				return stockistService.saveStockist(stockist,orgId,stateId,cityId,companyId,districtId,transporterDetailsId,userDetails).toString();
			}
	@RequestMapping(value="/updateStockistRegistrationDetails",method=RequestMethod.POST)
	private  @ResponseBody String updateStockistRegistrationDetails(
			@RequestParam(value="stockistId",required=true) Integer stockistId,@RequestParam(value="stockistName",required=false) String stockistName,
			@RequestParam(value="city",required=false) Integer cityId,@RequestParam(value="state",required=false) Integer stateId,
			@RequestParam(value="email",required=false) String email,@RequestParam(value="contactPerson",required=false) String contactPerson,
			@RequestParam(value="location",required=false) String location,@RequestParam(value="vat",required=false) String vat,
			@RequestParam(value="panNo",required=false) String panNo,@RequestParam(value="company",required=false) Integer companyId,
			@RequestParam(value="address",required=false) String address,@RequestParam(value="district",required=false) Integer districtId,
			@RequestParam(value="mobile",required=false) Long mobile,@RequestParam(value="fax",required=false) String fax,
			@RequestParam(value="pro_part",required=false) String stockistPartner,@RequestParam(value="transporter",required=false)Integer transporterDetailsId,
			@RequestParam(value="cst",required=false) String cst,@RequestParam(value="uname",required=true) String userName,@RequestParam(value="password",required=true) String password,
			HttpServletRequest request)
			{
				Stockist stockist=null;
				UserDetails userDetails=null;
				UserType userType = null;
				JSONObject result = new JSONObject();
				Boolean insertNewRecordFlag = false;
				try{
					stockist = stockistService.loadStockistObjectByStockistId(stockistId);
					userDetails = stockist.getUserDetails();
					userType = userDetails.getUserType();
					if(!"".equals(userName)&&!"".equals(password)&&userName!=null&&password!=null){
						if(!userDetails.getUserName().equals(userName)){
							UserDetails userDetailsTemp = userDetailsService.checkUserNameAlreadyExitOrNot(userName);
							if(userDetailsTemp!=null){
								result.put("MSG","User Name already exists");
								result.put("RESULT",false);
								return result.toString();
							}
						}
					}else{
						result.put("MSG","User Name and password are required....!");
						return result.toString();
					}
					if(stockist.getState().getStateId()!=stateId||stockist.getDistrict().getDistrictId()!=districtId||stockist.getCity().getCityId()!=cityId||stockist.getTransporterDetails().getTransporterDetailsId()!=transporterDetailsId||(!stockist.getStockistLocation().equals(location))){
						insertNewRecordFlag=true;
					}
					if(insertNewRecordFlag){
						java.util.Date submitDate = stockist.getSubmitDate();
						Organization organization = stockist.getOrganization();
						stockist = new Stockist();
						stockist.setStatus(1);
						stockist.setSubmitDate(submitDate);
						stockist.setOrganization(organization);
						userDetails = new UserDetails();
					}
					stockist.setStockistAddress(address);
					stockist.setStockistName(stockistName);
					stockist.setStockistVAT(vat);
					stockist.setStockistPartner(stockistPartner);
					stockist.setStockistPanNo(panNo);
					stockist.setStockistName(stockistName);
					stockist.setStockistMobileNo(mobile);
					stockist.setStockistLocation(location);
					stockist.setStockistFaxNo(fax);
					stockist.setStockistContactPerson(contactPerson);
					stockist.setStockistCST(cst);
					
					stockist.setStockistEmailId(email);
					userDetails.setUserName(userName);
					userDetails.setPassword(password);
					userDetails.setUserType(userType);
					stockist.setUserDetails(userDetails);
					result.put("MSG","Stockist Registration Details updation failed....!");
					if(insertNewRecordFlag){
						Integer id = stockistService.insertStockistDetilsAndDeleteOldStockistDetails(stockistId,transporterDetailsId,stateId,districtId,cityId,stockist);
						if(id!=null && id>0){
							result.put("MSG","Stockist Registration Details updated successfully....!");
							result.put("STOCKIST_ID",id);
							result.put("RESULT",true);
						}
					}else if(stockistService.updateStockistDetails(stockist)){
							result.put("MSG","Stockist Registration Details updated successfully....!");
							result.put("RESULT",true);
					}
				}catch(Exception e){
					e.printStackTrace();log.error(e);
					try {
						result.put("MSG","Operation Failed...!");
					} catch (JSONException e1) {
						e1.printStackTrace();log.error(e1);
					}
					log.error(e);
				}
				return result.toString();
			}
	@RequestMapping(value="/updateStockistDrugLicenseRegDetails",method=RequestMethod.POST)
	private  @ResponseBody String updateStockistDrugLicenseRegDetails(
			@RequestParam(value="drugLicenseId",required=true) Integer drugLicenceId,@RequestParam(value="licenseNo",required=false) String licenseNo,
			@RequestParam(value="frmDate",required=false) String frmDate,@RequestParam(value="toDate",required=false) String toDate,
			@RequestParam(value="validity",required=false) String validity)
			{//licenseNo frmDate toDate validity
				DrugLicence drugLicence = null; 
				JSONObject result = new JSONObject();
				DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
				java.util.Date d1= null;
				try{
					drugLicence = stockistService.getDrugLicenceObjectByDrugLicenceId(drugLicenceId);
					drugLicence.setDrugLicenceNo(licenseNo);
					d1 = dtf.parse(frmDate);
					drugLicence.setFromDate(new java.sql.Date(d1.getTime()));
					d1 = dtf.parse(toDate);
					drugLicence.setToDate(new java.sql.Date(d1.getTime()));
					drugLicence.setValidity(validity);
					result.put("MSG","Stockist Drug License Details updation failed....!");
					if(stockistService.updateDrugLicenceDetails(drugLicence)){
						result.put("MSG","Stockist Registration Details updated successfully....!");
						result.put("RESULT",true);
					}
				}catch(Exception e){
					e.printStackTrace();log.error(e);
					try {
						result.put("MSG","Operation Failed...!");
					} catch (JSONException e1) {
						e1.printStackTrace();log.error(e1);
					}
					log.error(e);
				}
				return result.toString();
			}
	@RequestMapping(value="/stockistUploadDocument",method=RequestMethod.POST)
	private  @ResponseBody String uploadStockistDocument(
			@RequestParam(value="orgName",required=false) Integer orgId,@RequestParam(value="stockistId",required=false) Integer stockistId,
			@RequestParam(value="company",required=false) Integer companyId,@RequestParam(value="documentTypeId",required=false) Integer documentTypeId,
			@RequestParam(value="documentImage",required=true) MultipartFile documentFile[])
			{
				StockistDocument stockistDocument = null;
				List<StockistDocumentImagePath> documentList = null;
				StockistDocumentImagePath tempDocumentObj = null;
				JSONObject result = null;
				try{
					/*if(documentFile.length<=0){                   // Unable to submit details without selecting a file...hence commenting this lines
						result = new JSONObject();
						result.put("MSg", "Please Select at least one file...!");
						return result.toString();
					}*/
					stockistDocument = new StockistDocument();
					// write image
					documentList = new  ArrayList<StockistDocumentImagePath>();
					 for(MultipartFile file : documentFile){
							if(file!=null){
								tempDocumentObj = new StockistDocumentImagePath();
								tempDocumentObj.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
								documentList.add(tempDocumentObj);
							}
						}
					 stockistDocument.setStockistDocumentImagePathList(documentList);
					//set organization submit date
					java.util.Date today = new java.util.Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
				    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
				    String IST = df.format(today);
				    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
				    java.util.Date date = df2.parse(IST);
				    stockistDocument.setSubmitDate(date);
				    
				    stockistDocument.setStatus(1);
				}catch(Exception e){
					e.printStackTrace();log.error(e);
				}
				return stockistService.uploadStockistDocument(orgId,stockistId,companyId,documentTypeId,stockistDocument).toString();//stockistService.saveStockist(stockist,orgId,stateId,cityId,companyId,districtId,transporterDetailsId,userDetails).toString();
			}
	
	@RequestMapping(value="/updatesStockistUploadDocumentImage",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updatesStockistUploadDocumentImage(
			@RequestParam(value="documentImage",required=true) MultipartFile documentImage[],
			@RequestParam(value="registrationId",required=true) Integer stockistDocumentId) throws IOException
	{
		JSONArray json_ImageArray= null;
		JSONObject result =  new JSONObject();
		List<StockistDocumentImagePath> documentList = null;
		StockistDocumentImagePath tempDocumentPath = null;
		try 
		{
			result.put("MSG", "File Upload failed...!");
			if(documentImage!=null&&!(documentImage.length>0)){
				result.put("MSG", "Please Select at least one file...!");
				return result.toString();
			}
			// write image
			documentList  = new ArrayList<StockistDocumentImagePath>();
			for(MultipartFile file : documentImage){
				if(file!=null){
					tempDocumentPath = new StockistDocumentImagePath();
					tempDocumentPath.setImagePath(CommonMethods.writeDocumentAndGetFileDB_Path(file));
					documentList.add(tempDocumentPath);
				}
			}
			json_ImageArray = stockistService.updatesStockistUploadDocumentImage(stockistDocumentId, documentList);
			result.put("MSG", "Company Document File Upload successful...!");
			result.put("FILE_NAME", json_ImageArray);
			result.put("RESULT", true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
			try {
				result.put("MSG", "File Upload failed...!");
			} catch (JSONException e1) {
				log.error(e1);
				e1.printStackTrace();
			}
		}
		return result.toString();
	}
	
	@RequestMapping(value="/deleteStockistDocumentFileImage",method=RequestMethod.POST)
	private  @ResponseBody String deleteStockistDocumentFileImage(@RequestParam(value="stockistDocumentPathId",required=false) Integer stockistDocumentPathId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			if(stockistService.deleteStockistDocumentFileImage(stockistDocumentPathId)){
				result.put("RESULT",true);
				result.put("MSG","Stockist Upload Document Image Deleted Successfully");
			}else{
				result.put("MSG","Stockist Upload Document Image Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		return result.toString();
	}
	@RequestMapping(value="/uploadDocumentListing",method=RequestMethod.POST)
	private  @ResponseBody String uploadDocumentListing(){
		Integer orgId = (Integer)session.getAttribute("ORGANIZATION_ID");
		return stockistService.uploadDocumentListing(orgId).toString();
	}
	
	@RequestMapping(value="/updateStockistDocumentInfo",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String updateStockistDocumentInfo(@RequestParam(value="stockistDocId",required=true) Integer stockistDocumentId,
			@RequestParam(value="stockistId",required=false) Integer stockistId,
			@RequestParam(value="company",required=true) Integer companyId,
			@RequestParam(value="documentTypeId",required=true) Integer documentTypeId)
	{
		JSONObject result= null;
		try
		{
			result = stockistService.updateStockistDocumentInfo(documentTypeId,companyId,stockistId,stockistDocumentId);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return result.toString();
	}
	@RequestMapping(value="/searchUploadedDocumentsDetails",method=RequestMethod.POST)
	private  @ResponseBody String searchUploadedDocumentsDetails(@RequestParam(value="searchOption",required=false) String searchOption,
			@RequestParam(value="searchText",required=false) String searchText,
			@RequestParam(value="from",required=true) Integer from,HttpServletRequest request){
		return stockistService.searchUploadedDocumentsDetails(searchOption,searchText,(Integer)session.getAttribute("ORGANIZATION_ID"),from).toString();
	}
	
	//Delete Stockist Details
	@RequestMapping(value="/deleteStockistDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteStockistDetails(@RequestParam(value="stockistId",required=false) Integer stockistId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(stockistService.deleteStockistDetails(stockistId)){
				result.put("MSG","Stockist Details Deleted Successfully");
			}else{
				result.put("MSG","Stockist Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		
		return result.toString();
	}
	//Delete Stockist Details
	@RequestMapping(value="/deleteStockistDocDetails",method=RequestMethod.POST)
	private  @ResponseBody String deleteStockistDocDetails(@RequestParam(value="stockistDocId",required=false) Integer stockistDocId){
		JSONObject result = null;
		try{
			result = new JSONObject();
			//result.put("result",false);
			if(stockistService.deleteStockistDocumentDetails(stockistDocId)){
				result.put("MSG","Stockist Document Details Deleted Successfully");
			}else{
				result.put("MSG","Stockist Document Details Deletion Failed");
			}
		}catch(Exception e){
			try{
				e.printStackTrace();log.error(e);
				result = new JSONObject();result.put("MSG","Operation Failed");
			}catch(Exception e1){}
		}
		
		return result.toString();
	}
	
	//Add by nutan
	@RequestMapping(value="/ViewStockistDetails",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String ViewStockistDetails(@RequestParam(value="stockistId",required=true) Integer stockistId,
			@RequestParam(value="editStatus",required=false) Boolean editStatus,
			HttpServletRequest request)
	{
		JSONArray json_data_array= new JSONArray();
		JSONArray json_data_Drugarray= new JSONArray();
		JSONObject json=null;
		JSONObject jsonDrug=null;
		try 
		{
						Stockist stockistObj=stockistService.loadStockistObjectByStockistId(stockistId);
						json=new JSONObject();
						json.put("ORG_NAME", stockistObj.getOrganization().getOrganizationName());
						json.put("STOCKIST_NAME", stockistObj.getStockistName());
						json.put("ADDRESS", stockistObj.getStockistAddress());
						json.put("MOBILE", stockistObj.getStockistMobileNo());
						CommonMethods common = new CommonMethods();
						json.put("SUBMITTED_DATE", common.submittedDateFormateIn12Hrs(stockistObj.getSubmitDate().toString()));
						json.put("STATE", stockistObj.getState().getStateName());
						json.put("DISTRICT",stockistObj.getDistrict().getDistrictName());
						json.put("CITY", stockistObj.getCity().getCityName());  
						if(stockistObj.getStockistFaxNo().equalsIgnoreCase(""))
						{
						json.put("FAX_NO", "-");
						}else{
							json.put("FAX_NO", stockistObj.getStockistFaxNo());
						}
						if(stockistObj.getStockistEmailId().equalsIgnoreCase(""))
						{
						json.put("EMAIL_ID", "-");
						}else
						{
							json.put("EMAIL_ID", stockistObj.getStockistEmailId());
						}
						json.put("PARTNER", stockistObj.getStockistPartner());
						if(stockistObj.getStockistContactPerson().equalsIgnoreCase(""))
						{
						json.put("CONTACT_PERSON", "-");
						}else{
							json.put("CONTACT_PERSON", stockistObj.getStockistContactPerson());
						}
						json.put("TRANSPORTER", stockistObj.getTransporterDetails().getTransporterName());
						//json.put("VALIDITY", stockistObj.getS);  
						json.put("LOCATION", stockistObj.getStockistLocation());  
						json.put("PAN_NO", stockistObj.getStockistPanNo());
						if(stockistObj.getStockistCST().equalsIgnoreCase(""))
						{
						json.put("CTS","-");
						}else{
							json.put("CTS", stockistObj.getStockistCST());
						}
						json.put("VAT", stockistObj.getStockistVAT());
						json.put("USER_NAME", stockistObj.getUserDetails().getUserName());
						List<DrugLicence> drugLicencesList= stockistService.loadDrugLicenseListUsingStockistId(stockistId);
						if(drugLicencesList.size() > 0)
						{
							String [] reg_date_arr;
							for(DrugLicence obj : drugLicencesList )
							{
								jsonDrug = new JSONObject();
								jsonDrug.put("DRUG_LICENSE_NO", obj.getDrugLicenceNo());
								reg_date_arr =  obj.getFromDate().toString().split("-");
								jsonDrug.put("FROM_DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
				                reg_date_arr =  obj.getToDate().toString().split("-");
				                jsonDrug.put("TO_DATE", reg_date_arr[1]+"/"+reg_date_arr[2]+"/"+reg_date_arr[0]);
								jsonDrug.put("VALIDITY", obj.getValidity()); 
								jsonDrug.put("DRUG_LICENSE_Id", obj.getDrugLicenceId());
								json_data_Drugarray.put(jsonDrug);
							}
						}
						json.put("DRUG_ARRAY", json_data_Drugarray);
						if(editStatus!=null&&editStatus){
							json.put("ORG_ID", stockistObj.getOrganization().getOrganizationId());
							json.put("PASSWORD", stockistObj.getUserDetails().getPassword());
							json.put("STATE_ID", stockistObj.getState().getStateId());
							json.put("DISTRICT_ID",stockistObj.getDistrict().getDistrictId());
							json.put("CITY_ID", stockistObj.getCity().getCityId());  
							json.put("TRANSPORTER_ID", stockistObj.getTransporterDetails().getTransporterDetailsId());
						}
						json_data_array.put(json);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	//BY DARSHAN
	@RequestMapping(value="/viewStockistCompanies",method=RequestMethod.POST,produces="application/json")
	public @ResponseBody String viewStockistCompanies(@RequestParam(value="stockistId",required=true) Integer stockistId,
			HttpServletRequest request)
	{
		JSONArray json_data_array = null;
		try 
		{
			json_data_array = stockistService.getStockistCompanyList(stockistId);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array.toString();
	}
	
}
