package com.protocol.wms.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.service.UploadDocumentService;





@Controller
@RequestMapping(value="/UploadDocumentController")
public class UploadDocumentController {
	
	
	private Logger log = Logger.getLogger(UploadDocumentController.class.getClass());
	@Autowired
	@Qualifier("UploadDocumentServiceImpl")
	private UploadDocumentService uploadDocumentService;
	
	@RequestMapping(value = "/uploadReport", method = RequestMethod.POST,produces="application/json")
	public @ResponseBody String uploadReport(
			@RequestParam(value="uploadReportExcelFile",required=true) MultipartFile uploadReportExcelFile,
			String jsonReqString ,HttpServletRequest request) throws ClassNotFoundException, Exception{
		System.out.println("in UploadDocumentController");
		String reportName="invoice_report";
		   JSONObject json=new JSONObject();
		   String file= CommonMethods.writeDocumentAndGetFileDB_Path2(uploadReportExcelFile);
		   json.put("msg",uploadDocumentService.uploadReport(file,request,reportName));
		   return json.toString();
	}
}
