package com.protocol.wms.constant;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;



public class CommonMethods {

	private Logger log = Logger.getLogger(CommonMethods.class.getClass());
	public String submittedDateFormateIn12Hrs(String submittedDate)
	{
		 String submittedDateToReturn = "";
		try {
		DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //HH for hour of the day (0 - 23)
		Date d = f1.parse(submittedDate);
		DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd h:mma");
		f2.format(d).toLowerCase(); // "12:18am"
	    submittedDateToReturn = f2.format(d).toLowerCase().toString();
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return submittedDateToReturn;
	}
	
	public static String writeDocumentAndGetFileDB_Path(MultipartFile file) throws IOException{
		byte[] bytes = file.getBytes();
	    String orignalFileName = file.getOriginalFilename();
		int index = orignalFileName.indexOf(".");
		String fileName=UUID.randomUUID().toString()+orignalFileName.substring(index);
		File serverFile = new File(Constant.IMAGE_PATH+ fileName);
		BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
		//checkingDone.setCheckingSlipImagePath(Constant.DATABASE_IMAGE_PATH+ fileName);
		stream.write(bytes);
		stream.close();
		return Constant.DATABASE_IMAGE_PATH+ fileName;
	}
	
	public static String getTimeIn12HrFormat(String time) throws ParseException{
		 SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
	      SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh : mm : a");
	      java.util.Date _24HourDt = null;
	      _24HourDt = _24HourSDF.parse(time);
          return _12HourSDF.format(_24HourDt);
	}
	
	//this method is use to set submit date(return current date and time)
	public static Date setSubmitedDate() throws ParseException
	{
		//set reg date
		java.util.Date today = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
	    df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
	    String IST = df.format(today);
	    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
	    java.util.Date date = df2.parse(IST);
	   return date;
	}
	
	public static void upLoadFTP(File serverFile) {
        String ftpUrl = "ftp://%s:%s$%s/%s;type=i";
        String host = "166.62.120.209:22";
        String user = "chat@meetrav.com";
        String pass = "chat@123";
       String uploadPath = "uploadImages/test.jpg";
 
        ftpUrl = String.format(ftpUrl, user, pass, host, uploadPath);
        System.out.println("Upload URL: " + ftpUrl);
 
        try {
            URL url = new URL(ftpUrl);
            URLConnection conn = url.openConnection();
            OutputStream outputStream = conn.getOutputStream();
            FileInputStream inputStream = new FileInputStream(serverFile);
 
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
 
            inputStream.close();
            outputStream.close();
 
            System.out.println("File uploaded");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
	
	
	public static String writeDocumentAndGetFileDB_Path2(MultipartFile file) throws IOException{
		if(file==null)
			return null;
		System.out.println(file);
		byte[] bytes = file.getBytes();
	    String orignalFileName = file.getOriginalFilename();
		int index = orignalFileName.indexOf(".");
		String fileName=UUID.randomUUID().toString()+orignalFileName.substring(index);
		File serverFile = new File(Constant.FILE_PATH_FOR_PDF+ fileName);
		BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
		stream.write(bytes);
		stream.close();
		return Constant.FILE_PATH_FOR_PDF+ fileName;
	}
	
	public static String SplitDate(String date)
	{
		String day=date.substring(0,3);
		String month=date.substring(3,5);
		String year=date.substring(6);
		return year+"/"+month+"/"+day;
	}

}
