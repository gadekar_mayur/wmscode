package com.protocol.wms.dao;

import java.util.Map;

public interface DashboardDao {
	
	public Map<String,Object> loadDashboardTable(Integer[] organizations,
			Integer[] companies, Integer[] stockists, Integer year,Integer organizationId);

}
