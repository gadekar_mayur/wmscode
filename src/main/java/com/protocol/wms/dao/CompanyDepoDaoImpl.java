/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.City;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyDepo;
import com.protocol.wms.model.District;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.State;

/**
 * @author Sudhakar
 *
 */
@Repository(value="CompanyDepoDaoImpl")
public class CompanyDepoDaoImpl implements CompanyDepoDao{

	private Logger log = Logger.getLogger(CompanyDepoDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDepoDao#loadCompanyDepoListUsignCompanyIdAndOrgId(java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyDepo> loadCompanyDepoListUsignCompanyIdAndOrgId(
			Integer orgId, Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyDepo> companyDepoList=null;
		try 
		{
			Criteria criteria = session.createCriteria(CompanyDepo.class,"CompanyDepo")
					.createAlias("CompanyDepo.company", "company")
					.createAlias("CompanyDepo.organization", "organization")
					.add(Restrictions.eq("company.companyId",companyId))
					.add(Restrictions.eq("organization.organizationId", orgId))
					.add(Restrictions.eq("CompanyDepo.status", 1))
					.add(Restrictions.eq("company.status", 1))
					.add(Restrictions.eq("organization.organizationStatus", "1"));
			companyDepoList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return companyDepoList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDepoDao#loadCompanyDepoObjectUsignCompanyDepoId(java.lang.Integer)
	 */
	@Override
	public CompanyDepo loadCompanyDepoObjectUsignCompanyDepoId(
			Integer companyDepoId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyDepo companyDepoObj=null;
		try 
		{
			companyDepoObj=(CompanyDepo)session.get(CompanyDepo.class,companyDepoId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return companyDepoObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDepoDao#saveCompanyDepoDetails(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyDepo)
	 */
	@Override
	public Boolean saveCompanyDepoDetails(Integer organizationId,
			Integer companyId, Integer stateId, Integer districtId,
			Integer cityId, CompanyDepo companyDepo) {
		Organization organization  = null;
		Company company = null;
		State state = null;
		District district = null;
		City city = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization) session.load(Organization.class,organizationId);
			company = (Company) session.load(Company.class,companyId);
			state = (State) session.load(State.class,stateId);
			district = (District) session.load(District.class,districtId);
			city = (City) session.load(City.class,cityId);
			
			companyDepo.setOrganization(organization);
			companyDepo.setCompany(company);
			companyDepo.setState(state);
			companyDepo.setCity(city);
			companyDepo.setDistrict(district);
			
			int i = (Integer) session.save(companyDepo);
			if(i>0)
				return true;
			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDepoDao#getCompanyDepoListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyDepo> getCompanyDepoListing(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyDepo> companyDepoList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyDepo.class,"companyDepo")
					.createAlias("companyDepo.organization", "organization")
					.add(Restrictions.eq("companyDepo.status",1))
					.addOrder(Order.desc("companyDepo.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			companyDepoList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		
		return companyDepoList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDepoDao#getCompanyDepoListByCompanyId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyDepo> getCompanyDepoListByCompanyId(Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyDepo> companyDepoList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyDepo.class,"companyDepo")
					.createAlias("companyDepo.company", "company")
					.add(Restrictions.eq("companyDepo.status",1))
					.add(Restrictions.eq("company.companyId",companyId))
					.addOrder(Order.desc("companyDepo.submitDate"));
			
			companyDepoList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		
		return companyDepoList;
	}
	

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDepoDao#deleteCompanyDepo(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCompanyDepo(Integer depoId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyDepo companyDepo = null;
		try{
			companyDepo = (CompanyDepo)session.get(CompanyDepo.class, depoId);
			companyDepo.setStatus(0);
			session.update(companyDepo);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDepoDao#searchCompanyDepotDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> searchCompanyDepotDetails(String searchOption,
			String searchText,Integer from, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<CompanyDepo> companyDepoList = null;
		Map<String,Object>map=null;
		
		try{
			Criteria criteria = session.createCriteria(CompanyDepo.class,"companyDepo")
					.createAlias("companyDepo.organization", "organization")
					.createAlias("companyDepo.company", "company")
					.add(Restrictions.eq("companyDepo.status",1))
					.add(Restrictions.eq("organization.organizationStatus","1"))
					.add(Restrictions.eq("company.status",1))
					.addOrder(Order.desc("companyDepo.submitDate"))
					.setProjection(Projections.rowCount());
			
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("DepoName".equals(searchOption))
				criteria.add(Restrictions.like("companyDepo.depoName", searchText,MatchMode.START));
			else if("ContactPerson".equals(searchOption))
				criteria.add(Restrictions.like("companyDepo.contactPerson", searchText,MatchMode.START));
			else if("Mobile".equals(searchOption)){
				try{criteria.add(Restrictions.eq("companyDepo.mobileNo", Long.parseLong(searchText)));}catch(Exception e){}
			}
			else if("Email".equals(searchOption))
				criteria.add(Restrictions.like("companyDepo.emailId", searchText,MatchMode.START));
			
			Long totalRecordCount = (Long)criteria.uniqueResult();
			
			 criteria = session.createCriteria(CompanyDepo.class,"companyDepo")
					.createAlias("companyDepo.organization", "organization")
					.createAlias("companyDepo.company", "company")
					.add(Restrictions.eq("companyDepo.status",1))
					.add(Restrictions.eq("organization.organizationStatus","1"))
					.add(Restrictions.eq("company.status",1))
					.addOrder(Order.desc("companyDepo.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("DepoName".equals(searchOption))
				criteria.add(Restrictions.like("companyDepo.depoName", searchText,MatchMode.START));
			else if("ContactPerson".equals(searchOption))
				criteria.add(Restrictions.like("companyDepo.contactPerson", searchText,MatchMode.START));
			else if("Mobile".equals(searchOption)){
				try{criteria.add(Restrictions.eq("companyDepo.mobileNo", Long.parseLong(searchText)));}catch(Exception e){}
			}
			else if("Email".equals(searchOption))
				criteria.add(Restrictions.like("companyDepo.emailId", searchText,MatchMode.START));
			
			from = from - 1 ;
		    criteria.setFirstResult(from*Constant.PAGE_COUNT);
		    criteria.setMaxResults(Constant.PAGE_COUNT);
			companyDepoList = criteria.list();
			
			Integer totalPage = 0;
			if(totalRecordCount>0){
				totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPage = totalPage +1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("companyDepo", companyDepoList);
			map.put("totalPages", totalPage );
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDepoDao#insertNewCompanyDepoDetailsAndDeleteExistingRecord(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyDepo)
	 */
	@Override
	public Integer insertNewCompanyDepoDetailsAndDeleteExistingRecord(
			Integer stateId, Integer districtId, Integer cityId,
			Integer companyDepoId, CompanyDepo companyDepo) {
		CompanyDepo oldCompanyDepoObj = null;
		State state = null;
		District district = null;
		City city = null;
		Integer id = 0;
		Session session =sessionFactory.getCurrentSession();
		try{
			state = (State) session.load(State.class,stateId);
			district = (District) session.load(District.class,districtId);
			city = (City) session.load(City.class,cityId);
			
			companyDepo.setState(state);
			companyDepo.setCity(city);
			companyDepo.setDistrict(district);
			id = (Integer) session.save(companyDepo);
			if(id>0){
				oldCompanyDepoObj = (CompanyDepo) session.load(CompanyDepo.class, companyDepoId);
				oldCompanyDepoObj.setStatus(0);
				session.update(oldCompanyDepoObj);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return id;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDepoDao#updateCompanyDepoDetails(com.protocol.wms.model.CompanyDepo)
	 */
	@Override
	public Boolean updateCompanyDepoDetails(CompanyDepo companyDepo) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.update(companyDepo);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

}
