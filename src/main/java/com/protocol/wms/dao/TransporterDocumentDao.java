/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;



import java.util.Map;

import com.protocol.wms.model.TransporterDocument;
import com.protocol.wms.model.TransporterDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
public interface TransporterDocumentDao {
	
	public Integer saveTransporterDocument(TransporterDocument transporterDocumentObj);
	
	public List<TransporterDocument> loadTransporterDocumentListUsignOrganizationId(Integer organizationId);

	public Map<Object,Object> searchTransporterUploadDocument(String searchOption,String searchText,Integer organizationId, Integer from);
	
	public TransporterDocument  loadTransporterDocumentObjById(Integer transporterDocumentId);
	
	public Boolean updateTransporterDocumentObj(TransporterDocument transporterDocument);
	
	public List<TransporterDocumentImagePath> updateTransportUploadDocumentImage(Integer transportDocumentId, List<TransporterDocumentImagePath> documentList);
	
	public Boolean deleteTrnsporterDocumentFileImage(Integer transporterDocumentPathId);
}
