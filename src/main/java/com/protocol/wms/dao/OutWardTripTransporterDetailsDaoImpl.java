/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.OutWardTripTransporterDetails;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OutWardTripTransporterDetailsDaoImpl")
public class OutWardTripTransporterDetailsDaoImpl implements OutWardTripTransporterDetailsDao{

	private Logger log = Logger.getLogger(OutWardTripTransporterDetailsDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardTripTransporterDetailsDao#saveOutWardTripTransporterDetailsObj(com.protocol.wms.model.OutWardTripTransporterDetails)
	 */
	@Override
	public Integer saveOutWardTripTransporterDetailsObj(
			OutWardTripTransporterDetails outWardTripTransporterDetailsObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer outWardTripTransporterDetailsId = null;
		try 
		{
			outWardTripTransporterDetailsId=(Integer) session.save(outWardTripTransporterDetailsObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardTripTransporterDetailsId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardTripTransporterDetailsDao#loadOutWardTripTransporterDetailsUsignOutWardTripId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardTripTransporterDetails> loadOutWardTripTransporterDetailsListUsignOutWardTripId(
			Integer outWardTripId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardTripTransporterDetails> outWardTripTransporterDetails=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardTripTransporterDetails.class,"OutWardTripTransporterDetails")
				     .createAlias("OutWardTripTransporterDetails.outWardTrip", "outWardTrip")
				     .add(Restrictions.eq("outWardTrip.outWardTripId", outWardTripId));
				   outWardTripTransporterDetails=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardTripTransporterDetails;
	}
}
