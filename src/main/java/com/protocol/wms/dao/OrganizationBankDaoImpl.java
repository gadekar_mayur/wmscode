/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.OrganizationBank;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OrganizationBankDaoImpl")
public class OrganizationBankDaoImpl implements OrganizationBankDao {
	
	private Logger log = Logger.getLogger(OrganizationBankDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
		
		@Autowired
		@Qualifier("sessionFactory")
		public void setSessionFactory(SessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.OrganizationBankDao#saveOrganizationBank(com.protocol.wms.model.OrganizationBank)
		 */
		@Override
		public Integer saveOrganizationBank(OrganizationBank organizationBank) {
			Session session =sessionFactory.getCurrentSession();
			Integer organizationBankId = null;
			try 
			{
				organizationBankId=(Integer) session.save(organizationBank);
			} 
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return organizationBankId;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.OrganizationBankDao#loadOrganizationBanksListUsingOrganizationId(java.lang.Integer)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public List<OrganizationBank> loadOrganizationBanksListUsingOrganizationId(
				Integer organizationId) {
			Session session =sessionFactory.getCurrentSession();
			List<OrganizationBank> organizationBanksList=null;
			try 
			{
				Criteria criteria = session.createCriteria(OrganizationBank.class,"OrganizationBank")
					     .createAlias("OrganizationBank.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .addOrder(Order.desc("submitDate"));;
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   organizationBanksList=criteria.list();
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return organizationBanksList;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.OrganizationBankDao#searchOrganiztionBankDetails(java.lang.String, java.lang.String, java.lang.Integer)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public Map<String,Object> searchOrganiztionBankDetails(String searchOption, String searchText,Integer from, Integer organizationId) {
			
			Session session =sessionFactory.getCurrentSession();
			List<OrganizationBank> organizationBanksList=null;
			Map<String,Object>map=null;
			
			try 
			{
				Criteria criteria = session.createCriteria(OrganizationBank.class,"OrganizationBank")
					     .createAlias("OrganizationBank.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.eq("organization.organizationStatus", "1"))
					     .addOrder(Order.desc("submitDate"))
					     .setProjection(Projections.rowCount());
				
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   if("Organization".equals(searchOption))
						     criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					   else if("BankName".equals(searchOption))
						     criteria.add(Restrictions.like("OrganizationBank.organizationBankName", searchText,MatchMode.START));
					   else if("Branch".equals(searchOption))
						     criteria.add(Restrictions.like("OrganizationBank.organizationBankBranch", searchText,MatchMode.START));
					   else if("AccountNo".equals(searchOption))
						     criteria.add(Restrictions.like("OrganizationBank.organizationBankAccountNo", searchText,MatchMode.START));
					   else if("IFSC".equals(searchOption))
						     criteria.add(Restrictions.like("OrganizationBank.organizationBankIfscCode", searchText,MatchMode.START));
				
					   Long totalRecordCount = (Long)criteria.uniqueResult();
				
				 criteria = session.createCriteria(OrganizationBank.class,"OrganizationBank")
					     .createAlias("OrganizationBank.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.eq("organization.organizationStatus", "1"))
					     .addOrder(Order.desc("submitDate"));
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   if("Organization".equals(searchOption))
						     criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					   else if("BankName".equals(searchOption))
						     criteria.add(Restrictions.like("OrganizationBank.organizationBankName", searchText,MatchMode.START));
					   else if("Branch".equals(searchOption))
						     criteria.add(Restrictions.like("OrganizationBank.organizationBankBranch", searchText,MatchMode.START));
					   else if("AccountNo".equals(searchOption))
						     criteria.add(Restrictions.like("OrganizationBank.organizationBankAccountNo", searchText,MatchMode.START));
					   else if("IFSC".equals(searchOption))
						     criteria.add(Restrictions.like("OrganizationBank.organizationBankIfscCode", searchText,MatchMode.START));
					   
					   from = from - 1 ;
					    criteria.setFirstResult(from*Constant.PAGE_COUNT);
					    criteria.setMaxResults(Constant.PAGE_COUNT);
					   organizationBanksList=criteria.list();
					   
					   Integer totalPage = 0;
						if(totalRecordCount>0){
							totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
							if((totalRecordCount%Constant.PAGE_COUNT)!=0){
								totalPage = totalPage +1;
							}
						}
						map = new HashMap<String, Object>();
						map.put("organizationBank", organizationBanksList);
						map.put("totalPages", totalPage );
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return map;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.OrganizationBankDao#deleteOrganizationBank(java.lang.Integer)
		 */
		@Override
		public Boolean deleteOrganizationBank(Integer organizationBankId) {
			Session session =sessionFactory.getCurrentSession();
			OrganizationBank organizationBank = null;
			try{
				organizationBank = (OrganizationBank)session.get(OrganizationBank.class, organizationBankId);
				organizationBank.setStatus(0);
				session.update(organizationBank);
				return true;
			}
			catch(Exception e){
				e.printStackTrace();
				log.error(e);
			}
			return false;
		}

		@Override
		public OrganizationBank loadOrganizationBankObjectUsingOrganizationBankId(
				Integer organizationBankId) {
			Session session =sessionFactory.getCurrentSession();
			OrganizationBank organizationBankObj=null;
			try 
			{
				organizationBankObj=(OrganizationBank)session.get(OrganizationBank.class,organizationBankId );
			} 
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return organizationBankObj;
		}

		@Override
		public Boolean updateOrganizationBankObj(
				OrganizationBank organizationBankObj) {
			Session session =sessionFactory.getCurrentSession();
			try 
			{
				session.update(organizationBankObj);
				return true;
			} 
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return false;
		}

		

//		@Override
//		public List<String> getOrganizationBankNameUsingBankIds(List<Integer> OrganizationBankIdList) {
//			Session session =sessionFactory.getCurrentSession();
//			Integer companyId;
//			List<Integer> organizationBankIdList=CompanyService.loadOrganizationBankIdListUsingCompanyId(companyId);
//			try 
//			{
//				Criteria criteria = session.createCriteria(OrganizationBank.class,"OrganizationBank")
//					     .createAlias("OrganizationBank.organization", "organization")
//					     .add(Restrictions.eq("status", 1))
//					     .addOrder(Order.desc("submitDate"));;
//					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
//					   organizationBanksList=criteria.list();
//			}
//			catch(DataAccessException e)
//			{
//				e.printStackTrace();
//				log.error(e);
//			}
//			catch (Exception e) 
//			{
//				e.printStackTrace();
//				log.error(e);
//			}
//			return organizationBanksList;
//		}


		
//		public List<String> getOrganizationBankNameUsingBankIds(List<Integer> OrganizationBankIdList) {
//			Session session =sessionFactory.getCurrentSession();
//			List<OrganizationBank> organizationBanksList=null;
//			try 
//			{
//				Criteria criteria = session.createCriteria(OrganizationBank.class,"OrganizationBank")
//					     .createAlias("OrganizationBank.organization", "organization")
//					     .add(Restrictions.eq("status", 1))
//					     .addOrder(Order.desc("submitDate"));;
//					   if(organizationId!=null)
//					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
//					   organizationBanksList=criteria.list();
//			}
//			catch(DataAccessException e)
//			{
//				e.printStackTrace();
//				log.error(e);
//			}
//			catch (Exception e) 
//			{
//				e.printStackTrace();
//				log.error(e);
//			}
//			return organizationBanksList;
//		}

		
		
}
