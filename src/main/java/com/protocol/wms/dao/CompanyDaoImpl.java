package com.protocol.wms.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.City;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyBank;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.CompanyDocument;
import com.protocol.wms.model.CompanyProduct;
import com.protocol.wms.model.District;
import com.protocol.wms.model.DocumentListType;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationBank;
import com.protocol.wms.model.RoleType;
import com.protocol.wms.model.State;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.model.UserType;

@Repository(value="CompanyDaoImpl")
public class CompanyDaoImpl implements CompanyDao {
	
	private Logger log = Logger.getLogger(CompanyDaoImpl.class.getClass());

	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getCompanyList(Integer orgId) {
		Session session =sessionFactory.getCurrentSession();
		List<Company> companyList = null;
		JSONObject companyJson = null;
		JSONArray  companyArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(Company.class,"comp")
					.createAlias("comp.organization", "org")
					.add(Restrictions.eq("org.organizationId", orgId))
					.add(Restrictions.eq("org.organizationStatus", "1"))
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.asc("comp.companyName"));
			companyList = criteria.list();
			Iterator<Company> companyIt = companyList.iterator();
			//System.out.println("Darrrrrrrr");
			while(companyIt.hasNext()){
				Company temp = companyIt.next();
				companyJson = new JSONObject();
				companyJson.put("compId", temp.getCompanyId());
				companyJson.put("compName", temp.getCompanyName());
				companyArray.put(companyJson);
			}
			companyJson = new JSONObject();
			companyJson.put("companyArray",companyArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return companyJson;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#loadCompanyObjectUsingCompanyId(java.lang.Integer)
	 */
	@Override
	public Company loadCompanyObjectUsingCompanyId(Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		Company companyObj=null;
		try 
		{
			companyObj=(Company)session.get(Company.class,companyId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return companyObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#getCompanyListAccToStockist(java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getCompanyListAccToStockist(Integer stockistId,Integer orgId) {
		Session session =sessionFactory.getCurrentSession();
		Stockist stockist = null;
		List<Company> companyList = null;
		JSONObject companyJson = null;
		JSONObject result = new JSONObject();
		JSONArray  companyArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(Company.class,"comp")
					.createAlias("comp.organization", "org")
					.add(Restrictions.eq("org.organizationId", orgId))
					.add(Restrictions.eq("status", 1));
			companyList = criteria.list();
			Iterator<Company> companyIt = companyList.iterator();
			//System.out.println("Darrrrrrrr");
			while(companyIt.hasNext()){
				Company temp = companyIt.next();
				companyJson = new JSONObject();
				companyJson.put("COMPANY_ID", temp.getCompanyId());
				companyJson.put("COMPANY_NAME", temp.getCompanyName());
				companyArray.put(companyJson);
			}
			result.put("companyArray",companyArray);
			
			try{
				stockist =(Stockist) session.get(Stockist.class, stockistId);
				companyList = stockist.getCompanyList();
				companyArray = new JSONArray();
				companyIt = companyList.iterator();
				while(companyIt.hasNext()){
					Company temp = companyIt.next();
					companyJson = new JSONObject();
					companyJson.put("COMPANY_ID", temp.getCompanyId());
					companyJson.put("COMPANY_NAME", temp.getCompanyName());
					companyArray.put(companyJson);
				}
				result.put("stockistCompanyArray",companyArray);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#saveCompanyDetails(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.Company)
	 */
	@Override
	public Boolean saveCompanyDetails(Integer orgId, Integer stateId,Integer districtId,Integer cityId, Integer roleTypeId,UserDetails userDetails,Company company) {
		Session session =sessionFactory.getCurrentSession();
		Organization organization  = null;
		RoleType roleType = null;
		State state = null;
		District district = null;
		City city = null;
		boolean result = false;
		Integer i = 0;
		try{
			
			 if(userDetails!=null){
		    	   UserType userType = (UserType) session.load(UserType.class,Constant.COMPANY);
		    	   userDetails.setUserType(userType);
		    	   int j = (Integer) session.save(userDetails);
		    	   System.out.println("UserDetails Id = "+j);
		    	   company.setUserDetails(userDetails);
		       }
			organization = (Organization) session.load(Organization.class, orgId);
			roleType = (RoleType) session.load(RoleType.class, roleTypeId);
			state = (State) session.load(State.class, stateId);
			district = (District) session.load(District.class, districtId);
			city = (City) session.load(City.class, cityId);
			
			company.setOrganizationg(organization);
			company.setRoleType(roleType);
			company.setState(state);
			company.setDistrict(district);
			company.setCity(city);
			
			i = 0;
			i = (Integer)session.save(company);
			if(i>0){
				result=true;
			}
		}catch(DataAccessException e){
			e.printStackTrace();result=false;return result;
		}catch(Exception e){
			e.printStackTrace();result=false;return result;
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#companyDetailsList(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Company> companyDetailsList(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<Company> companyList = null;
		try{
			Criteria criteria = session.createCriteria(Company.class,"company")
					.createAlias("company.organization", "organization")
					.add(Restrictions.eq("company.status",1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			companyList = criteria.list();
		}catch(Exception e){
			
		}
		
		return companyList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#searchAssignedBankDetailsOfOrgToCompany(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> searchAssignedBankDetailsOfOrgToCompany(
			String searchOption, String searchText,Integer from, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<Company> companyList = null;
		Map<String,Object>map=null;
		
		try{
			Criteria criteria = session.createCriteria(Company.class,"company")
					.createAlias("company.organization", "organization")
					.add(Restrictions.eq("company.status",1))
					.add(Restrictions.eq("organization.organizationStatus","1"))
					.setProjection(Projections.rowCount());
					
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			
			Long totalRecordCount = (Long)criteria.uniqueResult();
			
			criteria = session.createCriteria(Company.class,"company")
					.createAlias("company.organization", "organization")
					.add(Restrictions.eq("company.status",1))
					.add(Restrictions.eq("organization.organizationStatus","1"))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			
			from = from - 1 ;
			    criteria.setFirstResult(from*Constant.PAGE_COUNT);
			    criteria.setMaxResults(Constant.PAGE_COUNT);
			companyList = criteria.list();
			
			Integer totalPage = 0;
			if(totalRecordCount>0){
				totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPage = totalPage +1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("company", companyList);
			map.put("totalPages", totalPage );
		}catch(Exception e){
			
		}
		
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#searchCompanyDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String , Object> searchCompanyDetails(String searchOption,
			String searchText, Integer from,Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<Company> companyList = null;
		Map<String , Object> map = null;
		try{
			/*if("Mobile".equals(searchOption)){
				Query query = session.createSQLQuery("CALL searchCompanyDetails(:searchText,:organizationId)")
						.addEntity(Company.class)
						.setParameter("searchText", searchText+"%")
						.setParameter("organizationId", organizationId);
				companyList = query.list();
			}
			else{*/
			Criteria criteria = session.createCriteria(Company.class,"company")
					.createAlias("company.organization", "organization")
					.add(Restrictions.eq("organization.organizationStatus","1"))
					.add(Restrictions.eq("company.status",1))
					.setProjection(Projections.rowCount());
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("ContactPerson".equals(searchOption))
				criteria.add(Restrictions.like("company.companyPerson", searchText,MatchMode.START));
			else if("Email".equals(searchOption))
				criteria.add(Restrictions.like("company.companyEmailId", searchText,MatchMode.START));
			Long totalRecordCount = (Long)criteria.uniqueResult();
			
			criteria = session.createCriteria(Company.class,"company")
					.createAlias("company.organization", "organization")
					.add(Restrictions.eq("organization.organizationStatus","1"))
					.add(Restrictions.eq("company.status",1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("ContactPerson".equals(searchOption))
				criteria.add(Restrictions.like("company.companyPerson", searchText,MatchMode.START));
			else if("Email".equals(searchOption))
				criteria.add(Restrictions.like("company.companyEmailId", searchText,MatchMode.START));
			from = from - 1 ;
		    criteria.setFirstResult(from*Constant.PAGE_COUNT);
		    criteria.setMaxResults(Constant.PAGE_COUNT);
			companyList = criteria.list();
			
			Integer tatalPage = 0;
			if(totalRecordCount>0){
				tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					tatalPage=tatalPage+1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("companyList", companyList);
			map.put("totalPages", tatalPage);
			//}
		}catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#saveCompanyBankDetails(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyBank)
	 */
	@Override
	public boolean saveCompanyBankDetails(Integer orgId, Integer companyId,CompanyBank companyBank) {
		Organization organization  = null;
		Company company = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization) session.load(Organization.class,orgId);
			company = (Company) session.load(Company.class,companyId);
			companyBank.setOrganization(organization);
			companyBank.setCompany(company);
			int i = (Integer) session.save(companyBank);
			if(i>0)
				return true;
			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#companyBankDetailsListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyBank> companyBankDetailsListing(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyBank> companyBankList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyBank.class,"companyBank")
					.createAlias("companyBank.organization", "organization")
					.add(Restrictions.eq("companyBank.status",1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			companyBankList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		
		return companyBankList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#companyBankDropdown(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyBank> companyBankDropdown(Integer companyId,Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyBank> companyBankList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyBank.class,"companyBank")
					.createAlias("companyBank.organization", "organization")
					.createAlias("companyBank.company", "company")
					.add(Restrictions.eq("companyBank.status",1))
					.add(Restrictions.eq("organization.organizationId",organizationId))
					.add(Restrictions.eq("company.companyId",companyId))
					.addOrder(Order.desc("submitDate"));
			companyBankList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		
		return companyBankList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#saveCompanyDepoDetails(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyDepo)
	 */
	/*@Override
	public Boolean saveCompanyDepoDetails(Integer organizationId,
			Integer companyId, Integer stateId, Integer districtId,
			Integer cityId, CompanyDepo companyDepo) {
		Organization organization  = null;
		Company company = null;
		State state = null;
		District district = null;
		City city = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization) session.load(Organization.class,organizationId);
			company = (Company) session.load(Company.class,companyId);
			state = (State) session.load(State.class,stateId);
			district = (District) session.load(District.class,districtId);
			city = (City) session.load(City.class,cityId);
			
			companyDepo.setOrganization(organization);
			companyDepo.setCompany(company);
			companyDepo.setState(state);
			companyDepo.setCity(city);
			companyDepo.setDistrict(district);
			
			int i = (Integer) session.save(companyDepo);
			if(i>0)
				return true;
			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}*/

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#getCompanyDepoListing(java.lang.Integer)
	 */
	/*@Override
	public List<CompanyDepo> getCompanyDepoListing(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyDepo> companyDepoList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyDepo.class,"companyDepo")
					.createAlias("companyDepo.organization", "organization")
					.add(Restrictions.eq("companyDepo.status",1))
					.addOrder(Order.desc("companyDepo.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			companyDepoList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		
		return companyDepoList;
	}*/

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#getCompanyDepoListByCompanyId(java.lang.Integer)
	 */
	/*@Override
	public List<CompanyDepo> getCompanyDepoListByCompanyId(Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyDepo> companyDepoList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyDepo.class,"companyDepo")
					.createAlias("companyDepo.company", "company")
					.add(Restrictions.eq("companyDepo.status",1))
					.add(Restrictions.eq("company.companyId",companyId))
					.addOrder(Order.desc("companyDepo.submitDate"));
			
			companyDepoList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		
		return companyDepoList;
	}*/

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#saveCompanyProduct(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyProduct)
	 */
	@Override
	public Boolean saveCompanyProduct(Integer organizationId,Integer companyId, CompanyProduct companyProduct) {
		Organization organization  = null;
		Company company = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization) session.load(Organization.class,organizationId);
			company = (Company) session.load(Company.class,companyId);
			
			companyProduct.setOrganization(organization);
			companyProduct.setCompany(company);
			int i = (Integer) session.save(companyProduct);
			if(i>0)
				return true;
			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#getCompanyProductListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyProduct> getCompanyProductListing(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyProduct> companyProductList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyProduct.class,"companyProduct")
					.createAlias("companyProduct.organization", "organization")
					.add(Restrictions.eq("companyProduct.status",1))
					.addOrder(Order.desc("companyProduct.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			companyProductList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return companyProductList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#getCompanyOragainzationBankList(java.lang.Integer)
	 */
	@Override
	public JSONArray getCompanyOragainzationBankList(Integer companyId) {
		List<OrganizationBank> companyOrgBankList = null;
		Iterator<OrganizationBank> orgBamkItr = null;
		Company company = null;
		OrganizationBank organizationBank = null;
		Session session =sessionFactory.getCurrentSession();
		JSONObject orgBankJson = null;
		JSONArray orgBankArray = new JSONArray();
		try{
			company = (Company) session.get(Company.class,companyId);
			companyOrgBankList = company.getOrganizationBankList();
			orgBamkItr = companyOrgBankList.iterator();
			while(orgBamkItr.hasNext()){
				organizationBank = orgBamkItr.next();
				orgBankJson=new JSONObject();
				orgBankJson.put("ORG_BANK_ID",	organizationBank.getOrganizationBankId());
				orgBankJson.put("BANK_NAME", organizationBank.getOrganizationBankName()+","+organizationBank.getOrganizationBankBranch());
				orgBankArray.put(orgBankJson);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return orgBankArray;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#saveOrganizationBankToCompany(java.lang.Integer, java.lang.Integer, java.lang.Integer[])
	 */
	@Override
	public Boolean saveOrganizationBankToCompany(Integer organizationId,Integer companyId, Integer[] orgBanks) {
		Company company=null;
		OrganizationBank organizationBank = null; 
		List<OrganizationBank>orgBankList = null ;
		Session session =sessionFactory.getCurrentSession();
		try{
			orgBankList = new ArrayList<OrganizationBank>();
			if(orgBanks==null){
				company = (Company) session.get(Company.class, companyId);
				company.setOrganizationBankList(orgBankList);
				session.update(company);
				return true;
			}else{
				for(int i=0 ; i<orgBanks.length;i++){
					organizationBank = (OrganizationBank)session.get(OrganizationBank.class, orgBanks[i]);
					orgBankList.add(organizationBank);
				}
				company = (Company) session.get(Company.class, companyId);
				company.setOrganizationBankList(orgBankList);
				session.update(company);
				return true;
			}
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#uploadCompanyDocs(java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyDocument)
	 */
	@Override
	public Boolean uploadCompanyDocs(Integer orgId, Integer companyId,Integer documentTypeId, CompanyDocument companyDocument) {
		Company company=null;
		Organization organization = null; 
		DocumentListType documentListType = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization) session.get(Organization.class, orgId);
			company = (Company) session.get(Company.class, companyId);
			documentListType = (DocumentListType) session.get(DocumentListType.class, documentTypeId);
			companyDocument.setOrganization(organization);
			companyDocument.setCompany(company);
			companyDocument.setDocumentListType(documentListType);
			int i = 0;
			i= (Integer)session.save(companyDocument);
			if(i>0)
				return true;
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
			return false;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#getCompanyDocsListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyDocument> getCompanyDocsListing(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyDocument> companyDocsList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyDocument.class,"companyDocs")
					.createAlias("companyDocs.organization", "organization")
					.add(Restrictions.eq("companyDocs.status",1))
					.addOrder(Order.desc("companyDocs.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			companyDocsList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return companyDocsList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#saveCompanyCreditAndDiscount(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyCreditAndDiscount)
	 */
	@Override
	public Integer saveCompanyCreditAndDiscount(Integer organizationId,Integer companyId, CompanyCreditAndDiscount companyCreditAndDiscount) {
		Company company=null;
		Organization organization = null;
		Session session =sessionFactory.getCurrentSession();
		Integer id = null;
		try{
			organization = (Organization) session.get(Organization.class, organizationId);
			company = (Company) session.get(Company.class, companyId);
			companyCreditAndDiscount.setOrganization(organization);
			companyCreditAndDiscount.setCompany(company);
			id = 0;
			id= (Integer)session.save(companyCreditAndDiscount);
			if(id>0){
				company.setCompanyCreditAndDiscountStatus(1);
				session.update(company);
				return id;
			}
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
			return null;
		}
		return id;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#deleteCompanyDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCompanyDetails(Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		Company company = null;
		try{
			company = (Company)session.get(Company.class, companyId);
			company.setStatus(0);
			session.update(company);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#deleteCompanyBankDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCompanyBankDetails(Integer companyBankId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyBank companyBank = null;
		try{
			companyBank = (CompanyBank)session.get(CompanyBank.class, companyBankId);
			companyBank.setStatus(0);
			session.update(companyBank);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#deleteCompanyDepo(java.lang.Integer)
	 */
/*	@Override
	public Boolean deleteCompanyDepo(Integer depoId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyDepo companyDepo = null;
		try{
			companyDepo = (CompanyDepo)session.get(CompanyDepo.class, depoId);
			companyDepo.setStatus(0);
			session.update(companyDepo);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}*/

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#deleteCompanyProduct(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCompanyProduct(Integer productId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyProduct companyProduct = null;
		try{
			companyProduct = (CompanyProduct)session.get(CompanyProduct.class, productId);
			companyProduct.setStatus(0);
			session.update(companyProduct);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDao#deleteCompanyUploadedDocs(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCompanyUploadedDocs(Integer companyDocId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyDocument companyDocument = null;
		try{
			companyDocument = (CompanyDocument)session.get(CompanyDocument.class, companyDocId);
			companyDocument.setStatus(0);
			session.update(companyDocument);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public Boolean updateCompanyObj(Integer roleTypeId,Integer stateId,Integer districtId,Integer cityId,Company companyObj) {
		RoleType  roleType = null;
		State state=null;
		District district=null;
		City city = null;
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			if(roleTypeId!=null){
				roleType = (RoleType) session.load(RoleType.class,roleTypeId);
				state = (State) session.load(State.class, stateId);
				district = (District) session.load(District.class, districtId);
				city = (City) session.load(City.class, cityId);
			}
			companyObj.setRoleType(roleType);
			companyObj.setState(state);
			companyObj.setDistrict(district);
			companyObj.setCity(city);
			session.update(companyObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

/*	@Override
	public List<Company> searchCreditAndDiscount(String searchOption,
			String searchText, Integer orgId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<Company> companyList = null;
		try{
				Criteria criteria = session.createCriteria(Company.class,"company")
						.createAlias("company.organization", "organization")
						.add(Restrictions.eq("company.status",1));
						//.addOrder(Order.desc("submitDate"));
				if(orgId!=null)
					criteria.add(Restrictions.eq("organization.organizationId", orgId));
				
				if("Organization".equals(searchOption))
					criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				else if("CompanyName".equals(searchOption))
					criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
								
				companyList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		
		return companyList;
	}*/
	
}
