/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.OrderMode;

/**
 * @author Sudhakar
 *
 */
public interface OrderModeDao {
	
	public List<OrderMode> loadAllOrderMode();
	
	public OrderMode loadOrderModeObjectUsignOrderModeId(Integer orderModeId);

}
