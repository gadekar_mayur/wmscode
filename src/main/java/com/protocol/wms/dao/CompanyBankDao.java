/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.CompanyBank;

/**
 * @author Sudhakar
 *
 */
public interface CompanyBankDao {
	
	public List<CompanyBank> companyBankDropdown(Integer companyId,Integer organizationId);
	
	public CompanyBank loadCompanyBankUsignCompanyBankId(Integer companyBankId);

	public Boolean updateCompanyBankDetails(CompanyBank companyBank);
}
