/**
 * 
 */
package com.protocol.wms.dao;

import com.protocol.wms.model.PrintSticker;

/**
 * @author Sudhakar
 *
 */
public interface PrintStickerDao {

	public Integer savePrintSticker(PrintSticker printStickerObj);
	
	public PrintSticker loadPrintStickerObjUsingOutWardDispatchEnteryRegistrationId(Integer outWardDispatchEnteryRegistrationId);
	
	public void deletePrintStickerObj(PrintSticker printStickerObj);
	
}
