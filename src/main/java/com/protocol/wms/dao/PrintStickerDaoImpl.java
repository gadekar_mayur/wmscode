/**
 * 
 */
package com.protocol.wms.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.PrintSticker;

/**
 * @author Sudhakar
 *
 */
@Repository(value="PrintStickerDaoImpl")
public class PrintStickerDaoImpl implements PrintStickerDao{

	private Logger log = Logger.getLogger(PrintStickerDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.PrintStickerDao#savePrintSticker(com.protocol.wms.model.PrintSticker)
	 */
	@Override
	public Integer savePrintSticker(PrintSticker printStickerObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer printStickerId = null;
		try 
		{
			printStickerId=(Integer) session.save(printStickerObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return printStickerId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.PrintStickerDao#loadPrintStickerObjUsingOutWardDispatchEnteryRegistrationId(java.lang.Integer)
	 */
	@Override
	public PrintSticker loadPrintStickerObjUsingOutWardDispatchEnteryRegistrationId(
			Integer outWardDispatchEnteryRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		PrintSticker printStickerObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(PrintSticker.class,"PrintSticker")
					.createAlias("PrintSticker.outWardDispatchEnteryRegistration", "outWardDispatchEnteryRegistration")
					.add(Restrictions.eq("outWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegistrationId));
			printStickerObj=(PrintSticker) criteria.uniqueResult();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return printStickerObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.PrintStickerDao#deletePrintStickerObj(com.protocol.wms.model.PrintSticker)
	 */
	@Override
	public void deletePrintStickerObj(PrintSticker printStickerObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(printStickerObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}
}
