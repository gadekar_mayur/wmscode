/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails;
import com.protocol.wms.model.InwardReturnProducts;

/**
 * @author Sudhakar
 *
 */
public interface InwardReturnGoodsRegistrationDao {

	public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationId(Integer organizationId);
	
	public InwardReturnGoodsRegistration loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(Integer InwardReturnGoodsRegistrationoId);
	
	public InwardReturnGoodsRegistrationLRDetails loadInwardReturnGoodsRegistrationLRDetails(Integer InwardReturnGoodsRegistrationLRDetailsId);
	
	public void updateInwardReturnGoodsRegistrationObj(InwardReturnGoodsRegistration inwardReturnGoodsRegistrationObj);
	
	public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOne(Integer organizationId);
	
	public Boolean saveInwardReturnGoodsRegistration(Integer organizationId,Integer companyId,Integer stockistId,Integer transporterId,Integer employeeId,List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList,Map<Integer, List<InwardReturnProducts>> inwardReturnProductsMap,InwardReturnGoodsRegistration inwardReturnGoodsRegistration);
	
	public Boolean updateInwardReturnGoodsLrReg(List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList);
	
    public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne(Integer organizationId);
	
	public List<InwardReturnGoodsRegistration> inwardReturnGoodsRegListing(Integer organizationId);
	
	public List<InwardReturnGoodsRegistrationLRDetails> inwardReturnGoodsRegLrListing(Integer inwardReturnGoodRegistrationId);
	
	public List<Integer> inwardReturnGoodsRegLrIdDoNotHaveProduct(Integer inwardReturnGoodRegistrationId);
	
	public List<InwardReturnProducts> inwardProductListByLR_NO(Integer LR_ID);
	
	public Boolean deleteInwardReturnGoodsRegDetails(Integer inwardReturnGoodRegistrationId);
	
	public Boolean deleteInwardReturnGoodsRegLrDetails(Integer LR_ID);
	
	public Boolean deleteInwardReturnGoodsRegLRProduct(Integer PRODUCT_ID);
	
	public Boolean addProductsToInwardReturnGoodsLRInCheckingPending(Integer lrId,List<InwardReturnProducts> inwardReturnProductsList);
	
	public Boolean editInwardReturnGoodsRegistration(Integer orgId,Integer companyId,Integer stockistId,Integer transporterId,Integer employeeId,InwardReturnGoodsRegistration inwardReturnGoodsRegistration);
	
	public Boolean editInwardReturnGoodsRegistration(Integer orgId,Integer companyId,Integer stockistId,Integer transporterId,Integer employeeId,InwardReturnGoodsRegistration inwardReturnGoodsRegistration,Integer checkingDoneEmployeeDetailsId, CheckingDone checkingDone);
	
}
