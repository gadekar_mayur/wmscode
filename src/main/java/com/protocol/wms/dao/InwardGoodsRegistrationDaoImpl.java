/**
 * 
 */
package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyDepo;
import com.protocol.wms.model.InwardGoodsRegistration;
import com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails;
import com.protocol.wms.model.InwardGoodsRegistrationLRImgPath;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.TransporterDetails;

/**
 * @author Sudhakar
 *
 */
@Repository(value="InwardGoodsRegistrationDaoImpl")
public class InwardGoodsRegistrationDaoImpl implements InwardGoodsRegistrationDao{

private Logger log = Logger.getLogger(InwardGoodsRegistrationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationDao#saveInwardGoodsRegistration(com.protocol.wms.model.InwardGoodsRegistration)
	 */
	@Override
	public Integer saveInwardGoodsRegistration(
			InwardGoodsRegistration inwardGoodsRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer inwardGoodsRegistrationId = null;
		try 
		{
			inwardGoodsRegistrationId=(Integer) session.save(inwardGoodsRegistrationObj);
			List<InwardGoodsRegistrationLRImgPath> lst = inwardGoodsRegistrationObj.getInwardGoodsRegistrationLRImgPathList();
			for(InwardGoodsRegistrationLRImgPath tempObj : lst){
				tempObj.setInwardGoodsRegistration(inwardGoodsRegistrationObj);
				session.save(tempObj);
			}
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardGoodsRegistrationId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationDao#editInwardGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardGoodsRegistration)
	 */
	@Override
	public Boolean editInwardGoodsRegistration(Integer organizationId,
			Integer comapnyId, Integer sendingDepo, Integer transporterId,
			InwardGoodsRegistration inwardGoodsRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		Organization organization = null;
		Company company = null;
		CompanyDepo companyDepo = null;
		TransporterDetails transporterDetails = null;
		try 
		{
			organization = (Organization) session.load(Organization.class, organizationId);
			company = (Company) session.load(Company.class, comapnyId);
			companyDepo = (CompanyDepo) session.load(CompanyDepo.class, sendingDepo);
			transporterDetails = (TransporterDetails) session.load(TransporterDetails.class, transporterId);
			
			inwardGoodsRegistrationObj.setOrganization(organization);
			inwardGoodsRegistrationObj.setCompany(company);
			inwardGoodsRegistrationObj.setCompanyDepo(companyDepo);
			inwardGoodsRegistrationObj.setTransporterDetails(transporterDetails);
			session.update(inwardGoodsRegistrationObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationDao#loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(java.lang.Integer)
	 */
	@Override
	public InwardGoodsRegistration loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(
			Integer inwardGoodsRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		InwardGoodsRegistration inwardGoodsRegistrationObj=null;
		try 
		{
			inwardGoodsRegistrationObj=(InwardGoodsRegistration)session.get(InwardGoodsRegistration.class,inwardGoodsRegistrationId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardGoodsRegistrationObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationDao#loadInwardGoodsRegistrationListUsignOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InwardGoodsRegistration> loadInwardGoodsRegistrationListUsignOrganizationId(
			Integer orgId) {
		Session session =sessionFactory.getCurrentSession();
		List<InwardGoodsRegistration> inwardGoodsRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(InwardGoodsRegistration.class,"InwardGoodsRegistration")
				     .createAlias("InwardGoodsRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));;
				   if(orgId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", orgId));
				   inwardGoodsRegistrationList=criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardGoodsRegistrationList;
	}

	

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationDao#updateInwardGoodsRegistrationObject(com.protocol.wms.model.InwardGoodsRegistration)
	 */
	@Override
	public void updateInwardGoodsRegistrationObject(
			InwardGoodsRegistration inwardGoodsRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(inwardGoodsRegistrationObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationDao#deleteInwardGoodsRegistrationDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteInwardGoodsRegistrationDetails(Integer inwardGoodsRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		InwardGoodsRegistration inwardGoodsRegistration= null;
		try{
			inwardGoodsRegistration = (InwardGoodsRegistration)session.load(InwardGoodsRegistration.class, inwardGoodsRegistrationId);
			inwardGoodsRegistration.setStatus(0);
			session.update(inwardGoodsRegistration);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationDao#deleteInwardGoodsRegistrationLrImage(java.lang.Integer)
	 */
	@Override
	public Boolean deleteInwardGoodsRegistrationLrImage(Integer lrImageId) {
		Session session =sessionFactory.getCurrentSession();
		InwardGoodsRegistrationLRImgPath inwardGoodsRegistrationLRImgPath= null;
		try{
			inwardGoodsRegistrationLRImgPath = (InwardGoodsRegistrationLRImgPath)session.load(InwardGoodsRegistrationLRImgPath.class, lrImageId);
			session.delete(inwardGoodsRegistrationLRImgPath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationDao#updateInwardGoodsRegLrImages(java.lang.Integer, java.util.List)
	 */
	@Override
	public List<InwardGoodsRegistrationLRImgPath> updateInwardGoodsRegLrImages(
			Integer inwardGoodsRegistrationId,
			List<InwardGoodsRegistrationLRImgPath> lrImgList) {
		InwardGoodsRegistration inwardGoodsRegistration = null;
		List<InwardGoodsRegistrationLRImgPath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			inwardGoodsRegistration = (InwardGoodsRegistration) session.load(InwardGoodsRegistration.class, inwardGoodsRegistrationId);
			for(InwardGoodsRegistrationLRImgPath tempObj : lrImgList){
				tempObj.setInwardGoodsRegistration(inwardGoodsRegistration);
				session.save(tempObj);
			}
			lst = inwardGoodsRegistration.getInwardGoodsRegistrationLRImgPathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Map<Object, Object> searchInwardGoodsRegDetails(String selectedValue,String searchText,String fromSpecialData,String toSpecialData, Integer organizationId,Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<InwardGoodsRegistration> inwardGoodsRegistrationList=null;
		Map<Object, Object>map = null;
		try 
		{//STNNo STNNetAmount STNDate
			if("STNNo".equals(selectedValue)||"STNNetAmount".equals(selectedValue)||"STNDate".equals(selectedValue))
			{
				Criteria criteria = session.createCriteria(InwardGoodsRegistrationInvoiceDetails.class,"InvoiceDetails")
					     .createAlias("InvoiceDetails.inwardGoodsRegistration", "inwardGoodsRegistration")
					     .createAlias("inwardGoodsRegistration.organization", "organization")
					     .setProjection(Projections.distinct(Projections.property("inwardGoodsRegistration.InwardGoodsRegistrationId")));
				 if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				 
				 switch(selectedValue)
				 {
				 	case "STNNo": 
						 criteria.add(Restrictions.like("InvoiceDetails.stnNO", searchText,MatchMode.START));
						 break;
				 	case "STNNetAmount":
//				 		  criteria.add(Restrictions.ge("InvoiceDetails.grossAmount", Float.parseFloat(fromSpecialData)));
//						  criteria.add(Restrictions.le("InvoiceDetails.grossAmount", Float.parseFloat(toSpecialData)));
				 		 criteria.add(Restrictions.between("InvoiceDetails.netAmount", Float.parseFloat(fromSpecialData),Float.parseFloat(toSpecialData)));
						  break;
				 	case "STNDate":
				 		if(fromSpecialData!=""&&toSpecialData!=""){
							   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
							   java.util.Date d1= null;
								d1 = dtf.parse(fromSpecialData);
								java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
								d1 = dtf.parse(toSpecialData);
								java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
								 criteria.add(Restrictions.ge("InvoiceDetails.stnDate", sqlFromDate));
								 criteria.add(Restrictions.le("InvoiceDetails.stnDate", sqlToDate));
//								   criteria.add(Restrictions.between("InvoiceDetails.stnDate", sqlFromDate,sqlToDate));
						   }
				 }

				   
				 List<Integer> igrIdList = criteria.list();
//				  totalRecordCount=null;
					long totalRecordCount = 0;
				if(igrIdList.size()>0)
				{
				 criteria = session.createCriteria(InwardGoodsRegistration.class,"InwardGoodsRegistration")
						 .add(Restrictions.eq("status", 1))
						  .setProjection(Projections.rowCount())
					     .add(Restrictions.in("InwardGoodsRegistrationId", igrIdList));
//				}
			
				 totalRecordCount = (Long)criteria.uniqueResult();
			}

					 criteria = session.createCriteria(InwardGoodsRegistrationInvoiceDetails.class,"InvoiceDetails")
						     .createAlias("InvoiceDetails.inwardGoodsRegistration", "inwardGoodsRegistration")
						     .createAlias("inwardGoodsRegistration.organization", "organization")
						     .setProjection(Projections.distinct(Projections.property("inwardGoodsRegistration.InwardGoodsRegistrationId")));
					 if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					 
					 switch(selectedValue)
					 {
					 	case "STNNo": 
							 criteria.add(Restrictions.like("InvoiceDetails.stnNO", searchText,MatchMode.START));
							 break;
					 	case "STNNetAmount":
//					 		  criteria.add(Restrictions.ge("InvoiceDetails.netAmount", Float.parseFloat(fromSpecialData)));
//							  criteria.add(Restrictions.le("InvoiceDetails.netAmount", Float.parseFloat(toSpecialData)));
					 		 criteria.add(Restrictions.between("InvoiceDetails.netAmount", Float.parseFloat(fromSpecialData),Float.parseFloat(toSpecialData)));
							  break;
					 	case "STNDate":
					 		if(fromSpecialData!=""&&toSpecialData!=""){
								   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
								   java.util.Date d1= null;
									d1 = dtf.parse(fromSpecialData);
									java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
									d1 = dtf.parse(toSpecialData);
									java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
									 criteria.add(Restrictions.ge("InvoiceDetails.stnDate", sqlFromDate));
									 criteria.add(Restrictions.le("InvoiceDetails.stnDate", sqlToDate));
//									   criteria.add(Restrictions.between("InvoiceDetails.stnDate", sqlFromDate,sqlToDate));
							   }
					 }
					  igrIdList = criteria.list();
					  if(igrIdList.size()>0 || igrIdList==null)
						 {
					 criteria = session.createCriteria(InwardGoodsRegistration.class,"InwardGoodsRegistration")
							 .add(Restrictions.eq("status", 1))
						     .addOrder(Order.desc("submitDate"))
						     .add(Restrictions.in("InwardGoodsRegistrationId", igrIdList));
						 }
					   from=from-1;
					   criteria.setFirstResult(from *Constant.PAGE_COUNT);
					   criteria.setMaxResults(Constant.PAGE_COUNT);
						 
					   inwardGoodsRegistrationList=criteria.list();
					   
					   Integer tatalPage = 0;
						if(totalRecordCount>0)
						{
							 
							tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
							if((totalRecordCount%Constant.PAGE_COUNT)!=0)
							{
								tatalPage=tatalPage+1;
							}
						}
//						System.out.println("totalRecordCount : "+totalRecordCount);
//						System.out.println("tatalPage : "+tatalPage);
					
							map = new HashMap<Object, Object>();
							map.put("inwardGoodsList", inwardGoodsRegistrationList);
							map.put("totalPages", tatalPage);
				 
			}
		
			else
			{
			Criteria criteria = session.createCriteria(InwardGoodsRegistration.class,"InwardGoodsRegistration")
				     .createAlias("InwardGoodsRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				       .setProjection(Projections.rowCount())
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   
					 switch(selectedValue)
					 {
					 	case "Organization": 
							   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
					 	case "Transporter": 
					 		criteria.createAlias("InwardGoodsRegistration.transporterDetails", "transporterDetails");
							criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
							 break;
						case "Company": 
							criteria.createAlias("InwardGoodsRegistration.company", "company");
							criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
						case "LRNo": 
							   criteria.add(Restrictions.like("InwardGoodsRegistration.LRNo", searchText,MatchMode.START));
							 break;
						case "SendingDepo": 
							   criteria.createAlias("InwardGoodsRegistration.companyDepo", "companyDepo");
							   criteria.add(Restrictions.like("companyDepo.depoName", searchText,MatchMode.START));
							 break;
						case "DriverName": 
							   criteria.add(Restrictions.like("InwardGoodsRegistration.driverName", searchText,MatchMode.START));
							 break;
						case "RegDate":
							if(fromSpecialData!=""&&toSpecialData!=""){
								   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
								   java.util.Date d1= null;
									d1 = dtf.parse(fromSpecialData);
									java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
									d1 = dtf.parse(toSpecialData);
									java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
									 criteria.add(Restrictions.ge("InwardGoodsRegistration.InwardGoodsRegistrationDate", sqlFromDate));
									 criteria.add(Restrictions.le("InwardGoodsRegistration.InwardGoodsRegistrationDate", sqlToDate));
//									   criteria.add(Restrictions.between("InvoiceDetails.stnDate", sqlFromDate,sqlToDate));
							   }
					 }
					  Long totalRecordCount = (Long)criteria.uniqueResult();
					 
					 
						 criteria = session.createCriteria(InwardGoodsRegistration.class,"InwardGoodsRegistration")
							     .createAlias("InwardGoodsRegistration.organization", "organization")
							     .add(Restrictions.eq("status", 1))
							     .addOrder(Order.desc("submitDate"));
							   if(organizationId!=null)
							     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
							   
							   
								 switch(selectedValue)
								 {
								 	case "Organization": 
										   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
										 break;
								 	case "Transporter": 
								 		criteria.createAlias("InwardGoodsRegistration.transporterDetails", "transporterDetails");
										criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
										 break;
									case "Company": 
										criteria.createAlias("InwardGoodsRegistration.company", "company");
										criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
										 break;
									case "LRNo": 
										   criteria.add(Restrictions.like("InwardGoodsRegistration.LRNo", searchText,MatchMode.START));
										 break;
									case "SendingDepo": 
										   criteria.createAlias("InwardGoodsRegistration.companyDepo", "companyDepo");
										   criteria.add(Restrictions.like("companyDepo.depoName", searchText,MatchMode.START));
										 break;
									case "DriverName": 
										   criteria.add(Restrictions.like("InwardGoodsRegistration.driverName", searchText,MatchMode.START));
										 break;
									case "RegDate":
										if(fromSpecialData!=""&&toSpecialData!=""){
											   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
											   java.util.Date d1= null;
												d1 = dtf.parse(fromSpecialData);
												java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
												d1 = dtf.parse(toSpecialData);
												java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
												 criteria.add(Restrictions.ge("InwardGoodsRegistration.InwardGoodsRegistrationDate", sqlFromDate));
												 criteria.add(Restrictions.le("InwardGoodsRegistration.InwardGoodsRegistrationDate", sqlToDate));
//												   criteria.add(Restrictions.between("InvoiceDetails.stnDate", sqlFromDate,sqlToDate));
										   }
								 }
								 Integer tatalPage = 0;
								
								 from=from-1;
								   criteria.setFirstResult(from *Constant.PAGE_COUNT);
								   criteria.setMaxResults(Constant.PAGE_COUNT);
								   inwardGoodsRegistrationList=criteria.list();
								   
								   
									if(totalRecordCount>0)
									{
										tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
										if((totalRecordCount%Constant.PAGE_COUNT)!=0)
										{
											tatalPage=tatalPage+1;
										}
									}
//									System.out.println("totalRecordCount : "+totalRecordCount);
//									System.out.println("tatalPage : "+tatalPage);
								
										map = new HashMap<Object, Object>();
										map.put("inwardGoodsList", inwardGoodsRegistrationList);
										map.put("totalPages", tatalPage);
								 
								 
								 
				 
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
	
}
