/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.CompanyBank;

/**
 * @author admin
 *
 */
@Repository(value="CompanyBankDetailsDaoImpl")
public class CompanyBankDetailsDaoImpl implements CompanyBankDetailsDao {

	private Logger log = Logger.getLogger(CompanyBankDetailsDaoImpl.class.getClass());

	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyBankDetailsDao#companyBankDetailsListing(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Object,Object> searchCompanyBankDetails(String searchOption,String searchText,Integer organizationId, Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyBank> companyBankList = null;
		Map<Object,Object> map=null;
		try{
			Criteria criteria = session.createCriteria(CompanyBank.class,"companyBank")
					.createAlias("companyBank.organization", "organization")
					.createAlias("companyBank.company", "company")
					.add(Restrictions.eq("organization.organizationStatus","1"))
					.add(Restrictions.eq("company.status",1))
					.add(Restrictions.eq("companyBank.status",1))
					 .setProjection(Projections.rowCount());
			
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("BankName".equals(searchOption))
				criteria.add(Restrictions.like("companyBank.companyBankName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("Branch".equals(searchOption))
				criteria.add(Restrictions.like("companyBank.companyBankBranch", searchText,MatchMode.START));
			else if("AccountNo".equals(searchOption))
				criteria.add(Restrictions.like("companyBank.companyBankAccountNo", searchText,MatchMode.START));
			else if("IFSC".equals(searchOption))
				criteria.add(Restrictions.like("companyBank.companyBankIfscCode", searchText,MatchMode.START));
			
			 Long totalRecordCount = (Long)criteria.uniqueResult();
			 
			 
			 criteria = session.createCriteria(CompanyBank.class,"companyBank")
					.createAlias("companyBank.organization", "organization")
					.createAlias("companyBank.company", "company")
					.add(Restrictions.eq("organization.organizationStatus","1"))
					.add(Restrictions.eq("company.status",1))
					.add(Restrictions.eq("companyBank.status",1))
					.addOrder(Order.desc("submitDate"));
			 
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("BankName".equals(searchOption))
				criteria.add(Restrictions.like("companyBank.companyBankName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("Branch".equals(searchOption))
				criteria.add(Restrictions.like("companyBank.companyBankBranch", searchText,MatchMode.START));
			else if("AccountNo".equals(searchOption))
				criteria.add(Restrictions.like("companyBank.companyBankAccountNo", searchText,MatchMode.START));
			else if("IFSC".equals(searchOption))
				criteria.add(Restrictions.like("companyBank.companyBankIfscCode", searchText,MatchMode.START));
			
			
			 from=from-1;
			   criteria.setFirstResult(from *Constant.PAGE_COUNT);
			   criteria.setMaxResults(Constant.PAGE_COUNT);
				 
			   companyBankList=criteria.list();
			   
			   Integer tatalPage = 0;
				if(totalRecordCount>0)
				{
					 
					tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0)
					{
						tatalPage=tatalPage+1;
					}
				}
					map = new HashMap<Object, Object>();
					map.put("compBank", companyBankList);
					map.put("totalPages", tatalPage);

			
		}catch(Exception e)
		{
			e.printStackTrace();
			log.error(e);
		}
		
		return map;
	}

}
