package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

//Add by nutan
@Repository("OrganizationBankCompanyJoinDAOImpl")
public class OrganizationBankCompanyJoinDAOImpl implements
		OrganizationBankCompanyJoinDAO {

	private Logger log = Logger.getLogger(OrganizationBankDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
		
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> loadOrganizationBankIdListUsingCompanyId(Integer companyId)  {
			Session session =sessionFactory.getCurrentSession();
			List<Integer> OrganizationBankIdList = null;
			try 
			{
				String sql="select organizationBankId from  ORGANIZATIONBANK_COMPANY_JOIN as o_c_j where o_c_j.companyId="+companyId+"";
				SQLQuery query=session.createSQLQuery(sql);//.addScalar("organizationBankId",IntegerType.INSTANCE);
				OrganizationBankIdList=query.list();
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return OrganizationBankIdList;
	}
}
