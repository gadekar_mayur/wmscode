/**
 * 
 */
package com.protocol.wms.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.ClaimRates;

/**
 * @author Sudhakar
 *
 */
@Repository(value="ClaimRatesDaoImpl")
public class ClaimRatesDaoImpl implements ClaimRatesDao{
	
	private Logger log = Logger.getLogger(ClaimRatesDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.ClaimRatesDao#saveClaimRate(com.protocol.wms.model.ClaimRates)
	 */
	@Override
	public Integer saveClaimRate(ClaimRates claimRates) {
		Session session =sessionFactory.getCurrentSession();
		Integer claimRatesId=null;
		try 
		{
			claimRatesId=(Integer) session.save(claimRates);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return claimRatesId;
	}

}
