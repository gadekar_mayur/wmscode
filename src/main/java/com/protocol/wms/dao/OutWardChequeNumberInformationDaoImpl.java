package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.OutWardChequeNumberInformation;

@Repository("OutWardChequeNumberInformationDaoImpl")
public class OutWardChequeNumberInformationDaoImpl implements
		OutWardChequeNumberInformationDao {

	
	
	
	private Logger log = Logger.getLogger(AdvancedChequeInformationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardChequeNumberInformation> loadChequeInformationUsigngetOutWardAdvancedChequeInformationId(
			Integer outWardAdvancedChequeInformationId) {
			Session session =sessionFactory.getCurrentSession();
			List<OutWardChequeNumberInformation> outWardChequeNumberInformationList=null;
			try 
			{
				Criteria criteria = session.createCriteria(OutWardChequeNumberInformation.class,"OutWardChequeNumberInformation")
					     .createAlias("OutWardChequeNumberInformation.outWardAdvancedChequeInformation", "outWardAdvancedChequeInformation")
					     .add(Restrictions.eq("outWardAdvancedChequeInformation.outWardAdvancedChequeInformationId", outWardAdvancedChequeInformationId));
					   outWardChequeNumberInformationList=criteria.list();
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return outWardChequeNumberInformationList;
		}

	
}
