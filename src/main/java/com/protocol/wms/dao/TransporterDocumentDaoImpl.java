/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.TransporterDocument;
import com.protocol.wms.model.TransporterDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
@Repository(value="TransporterDocumentDaoImpl")
public class TransporterDocumentDaoImpl implements TransporterDocumentDao{

	private Logger log = Logger.getLogger(TransporterDocumentDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;

	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDocumentDao#saveTransporterDocument(com.protocol.wms.model.TransporterDocument)
	 */
	@Override
	public Integer saveTransporterDocument(
			TransporterDocument transporterDocumentObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer transporterDocumentId = null;
		try 
		{
			transporterDocumentId=(Integer) session.save(transporterDocumentObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return transporterDocumentId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDocumentDao#loadTransporterDocumentListUsignOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TransporterDocument> loadTransporterDocumentListUsignOrganizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<TransporterDocument> transporterDocumentList=null;
		try 
		{
			Criteria criteria = session.createCriteria(TransporterDocument.class,"TransporterDocument")
				     .createAlias("TransporterDocument.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   transporterDocumentList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return transporterDocumentList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDocumentDao#searchTransporterUploadDocument(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Object,Object> searchTransporterUploadDocument(
			String searchOption, String searchText, Integer organizationId, Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<TransporterDocument> transporterDocumentList=null;
		Map<Object, Object>map = null;
		try 
		{
			Criteria criteria = session.createCriteria(TransporterDocument.class,"TransporterDocument")
				     .createAlias("TransporterDocument.organization", "organization")
				     .createAlias("TransporterDocument.transporterDetails", "transporterDetails")
				     .createAlias("TransporterDocument.documentListType", "documentListType")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("organization.organizationStatus", "1"))
				     .add(Restrictions.eq("transporterDetails.status", 1))
				      .setProjection(Projections.rowCount())
				     .addOrder(Order.desc("submitDate"));
			
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   if("Organization".equals(searchOption))
					   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				   else if("TransporterName".equals(searchOption))
					   criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
				   else if("DocumentType".equals(searchOption))
					   criteria.add(Restrictions.like("documentListType.documentName", searchText,MatchMode.START));
				   
				   Long totalRecordCount = (Long)criteria.uniqueResult();
				   
					 criteria = session.createCriteria(TransporterDocument.class,"TransporterDocument")
						      .createAlias("TransporterDocument.organization", "organization")
						      .createAlias("TransporterDocument.transporterDetails", "transporterDetails")
						      .createAlias("TransporterDocument.documentListType", "documentListType")
						      .add(Restrictions.eq("status", 1))
						      .add(Restrictions.eq("organization.organizationStatus", "1"))
						      .add(Restrictions.eq("transporterDetails.status", 1))
						     .addOrder(Order.desc("submitDate"));;
						   if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   if("Organization".equals(searchOption))
							   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
						   else if("TransporterName".equals(searchOption))
							   criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
						   else if("DocumentType".equals(searchOption))
							   criteria.add(Restrictions.like("documentListType.documentName", searchText,MatchMode.START));
						   
						   from=from-1;
						   criteria.setFirstResult(from *Constant.PAGE_COUNT);
						   criteria.setMaxResults(Constant.PAGE_COUNT);
						   transporterDocumentList = criteria.list();
						   
						   Integer tatalPage = 0;
							if(totalRecordCount>0)
							{
								tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
								if((totalRecordCount%Constant.PAGE_COUNT)!=0)
								{
									tatalPage=tatalPage+1;
								}
							}
						
								map = new HashMap<Object, Object>();
								map.put("transporterDocList", transporterDocumentList);
								map.put("totalPages", tatalPage);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

	@Override
	public TransporterDocument loadTransporterDocumentObjById(Integer transporterDocumentId) {
		Session session =sessionFactory.getCurrentSession();
		TransporterDocument transporterDocument=null;
		try 
		{
			transporterDocument = (TransporterDocument) session.load(TransporterDocument.class, transporterDocumentId);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return transporterDocument;
	}

	@Override
	public Boolean updateTransporterDocumentObj(TransporterDocument transporterDocument) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(transporterDocument);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<TransporterDocumentImagePath> updateTransportUploadDocumentImage(Integer transportDocumentId,
			List<TransporterDocumentImagePath> documentList) {
		Session session =sessionFactory.getCurrentSession();
		TransporterDocument transporterDocument = null;
		try 
		{
			transporterDocument = (TransporterDocument) session.load(TransporterDocument.class, transportDocumentId);
			for(TransporterDocumentImagePath tempObj : documentList){
				tempObj.setTransporterDocument(transporterDocument);
				session.save(tempObj);
			}
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return transporterDocument.getTransporterDocumentImagePathList();
	}

	@Override
	public Boolean deleteTrnsporterDocumentFileImage(
			Integer transporterDocumentPathId) {
		Session session =sessionFactory.getCurrentSession();
		TransporterDocumentImagePath trnsporterDocumentFileImage = null;
		try 
		{
			trnsporterDocumentFileImage = (TransporterDocumentImagePath) session.load(TransporterDocumentImagePath.class, transporterDocumentPathId);
			session.delete(trnsporterDocumentFileImage);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
}
