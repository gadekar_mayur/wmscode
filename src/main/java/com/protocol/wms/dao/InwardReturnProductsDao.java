/**
 * 
 */
package com.protocol.wms.dao;

import com.protocol.wms.model.InwardReturnProducts;

/**
 * @author admin
 *
 */
public interface InwardReturnProductsDao {

	public InwardReturnProducts loadInwardReturnProductsObjById(Integer InwardReturnProductsId);
	
	public Boolean editInwardReturnGoodsRegLreditProductDetails(InwardReturnProducts inwardReturnProducts);
}
