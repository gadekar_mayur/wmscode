/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails;
import com.protocol.wms.model.InwardGoodsRegistrationInvoiceStnNoImagePath;

/**
 * @author Sudhakar
 *
 */
@Repository(value="InwardGoodsRegistrationInvoiceDetailsDaoImpl")
public class InwardGoodsRegistrationInvoiceDetailsDaoImpl implements InwardGoodsRegistrationInvoiceDetailsDao{

	private Logger log = Logger.getLogger(InwardGoodsRegistrationInvoiceDetailsDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationInvoiceDetailsDao#saveInwardGoodsRegistrationInvoiceDetails(com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails)
	 */
	@Override
	public Integer saveInwardGoodsRegistrationInvoiceDetails(
			InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetailsObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer inwardGoodsRegistrationInvoiceDetailsId = null;
		try 
		{
			inwardGoodsRegistrationInvoiceDetailsId=(Integer) session.save(inwardGoodsRegistrationInvoiceDetailsObj);
			List<InwardGoodsRegistrationInvoiceStnNoImagePath> lst =inwardGoodsRegistrationInvoiceDetailsObj.getInwardGoodsRegistrationInvoiceStnNoImagePathList();
			for(InwardGoodsRegistrationInvoiceStnNoImagePath tempObj : lst){
				tempObj.setInwardGoodsRegistrationInvoiceDetails(inwardGoodsRegistrationInvoiceDetailsObj);
				session.save(tempObj);
			}
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardGoodsRegistrationInvoiceDetailsId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationInvoiceDetailsDao#loadInwardGoodsRegistrationInvoiceDetailsListUsignInwardGoodsRegistrationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InwardGoodsRegistrationInvoiceDetails> loadInwardGoodsRegistrationInvoiceDetailsListUsignInwardGoodsRegistrationId(
			Integer inwardGoodsRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		List<InwardGoodsRegistrationInvoiceDetails> inwardGoodsRegistrationInvoiceDetailsList=null;
		
		try 
		{
			Criteria criteria = session.createCriteria(InwardGoodsRegistrationInvoiceDetails.class,"InwardGoodsRegistrationInvoiceDetails")
					.createAlias("InwardGoodsRegistrationInvoiceDetails.inwardGoodsRegistration", "inwardGoodsRegistration")
					.add(Restrictions.eq("inwardGoodsRegistration.InwardGoodsRegistrationId", inwardGoodsRegistrationId));
			inwardGoodsRegistrationInvoiceDetailsList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardGoodsRegistrationInvoiceDetailsList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationInvoiceDetailsDao#getInwardGoodsRegistrationInvoiceDetailsObjById(java.lang.Integer)
	 */
	
	@Override
	public InwardGoodsRegistrationInvoiceDetails getInwardGoodsRegistrationInvoiceDetailsObjById(
			Integer inwardGoodsRegistrationInvoiceDetailsId) {
		InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetails = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			inwardGoodsRegistrationInvoiceDetails = (InwardGoodsRegistrationInvoiceDetails) session.get(InwardGoodsRegistrationInvoiceDetails.class, inwardGoodsRegistrationInvoiceDetailsId);
		}catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardGoodsRegistrationInvoiceDetails;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationInvoiceDetailsDao#editInwardGoodsRegistrationInvoiceDetails(com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails)
	 */
	@Override
	public Boolean editInwardGoodsRegistrationInvoiceDetails(InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetails) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.update(inwardGoodsRegistrationInvoiceDetails);
			return true;
		}catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationInvoiceDetailsDao#deleteInwardGoodsRegInvoiceStnImage(java.lang.Integer)
	 */
	@Override
	public Boolean deleteInwardGoodsRegInvoiceStnImage(Integer stnImageId) {
		Session session =sessionFactory.getCurrentSession();
		InwardGoodsRegistrationInvoiceStnNoImagePath inwardGoodsRegistrationInvoiceStnNoImagePath =null;
		try{
			inwardGoodsRegistrationInvoiceStnNoImagePath = (InwardGoodsRegistrationInvoiceStnNoImagePath) session.load(InwardGoodsRegistrationInvoiceStnNoImagePath.class, stnImageId);
			session.delete(inwardGoodsRegistrationInvoiceStnNoImagePath);
			return true;
		}catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationInvoiceDetailsDao#updateInwardGoodsRegInvoiceStnImageupdateInwardGoodsRegInvoiceStnImage(java.lang.Integer, java.util.List)
	 */
	@Override
	public List<InwardGoodsRegistrationInvoiceStnNoImagePath> updateInwardGoodsRegInvoiceStnImage(
			Integer inwardGoodsRegistrationInvoiceId,
			List<InwardGoodsRegistrationInvoiceStnNoImagePath> stnImgList) {
		InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetails = null;
		List<InwardGoodsRegistrationInvoiceStnNoImagePath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			inwardGoodsRegistrationInvoiceDetails = (InwardGoodsRegistrationInvoiceDetails) session.load(InwardGoodsRegistrationInvoiceDetails.class, inwardGoodsRegistrationInvoiceId);
			for(InwardGoodsRegistrationInvoiceStnNoImagePath tempObj : stnImgList){
				tempObj.setInwardGoodsRegistrationInvoiceDetails(inwardGoodsRegistrationInvoiceDetails);
				session.save(tempObj);
			}
			lst = inwardGoodsRegistrationInvoiceDetails.getInwardGoodsRegistrationInvoiceStnNoImagePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}
	
}
