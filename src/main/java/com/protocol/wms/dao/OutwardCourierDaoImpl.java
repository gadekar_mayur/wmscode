/**
 * 
 */
package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.City;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyDepo;
import com.protocol.wms.model.District;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationBank;
import com.protocol.wms.model.OutWardAdvancedChequeInformation;
import com.protocol.wms.model.OutWardChequeNumberInformation;
import com.protocol.wms.model.OutWardCourierRegistration;
import com.protocol.wms.model.Particulars;
import com.protocol.wms.model.State;
import com.protocol.wms.model.Stockist;

/**
 * @author admin
 *
 */
@Repository("OutwardCourierDaoImpl")
public class OutwardCourierDaoImpl implements OutwardCourierDao {

private Logger log = Logger.getLogger(OutwardCourierDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardCourierDao#saveOutwardCourierRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.OutWardCourierRegistration)
	 */
	@Override
	public Boolean saveOutwardCourierRegistration(String companyStockistOption,Integer orgId,Integer companyId, Integer depoId, Integer particularId,Integer stockistId,Integer companyBankId,OutWardCourierRegistration outWardCourierRegistration,OutWardAdvancedChequeInformation advancedChequeInfo,List<OutWardChequeNumberInformation> chequeNoInfoList) {
		Session session =sessionFactory.getCurrentSession();
		Organization organization = null;
		Company company = null;
		CompanyDepo companyDepo = null;
		State state = null;
		District district = null;
		City city = null;
		Particulars particulars = null;
		Stockist stockist = null;
		OrganizationBank organizationBank = null;
		try 
		{
			organization = (Organization) session.load(Organization.class, orgId);
			company = (Company) session.load(Company.class, companyId);
			if(depoId!=null)
				companyDepo = (CompanyDepo) session.load(CompanyDepo.class, depoId);
			//state = (State) session.load(State.class, stateId);
			//district = (District) session.load(District.class, districtId);
			//city = (City) session.load(City.class, cityId);
			particulars = (Particulars) session.load(Particulars.class, particularId);
			
			if(stockistId!=null){
				stockist = (Stockist) session.load(Stockist.class, stockistId);
				state = stockist.getState();
				district = stockist.getDistrict();
				city = stockist.getCity();
				outWardCourierRegistration.setStockist(stockist);
			}else{
				state = company.getState();
				district = company.getDistrict();
				city = company.getCity();
			}
			
			if(companyBankId!=null){
				if(stockistId!=null)
					advancedChequeInfo.setStockist(stockist);
				
				advancedChequeInfo.setCompany(company);
				organizationBank = (OrganizationBank) session.load(OrganizationBank.class, companyBankId);
				advancedChequeInfo.setOrganizationBank(organizationBank);
				session.save(advancedChequeInfo);
			
				if(chequeNoInfoList!=null){
					for(OutWardChequeNumberInformation tempObj : chequeNoInfoList){
						tempObj.setOutWardAdvancedChequeInformation(advancedChequeInfo);
						session.save(tempObj);
					}
				}
				
				advancedChequeInfo.setOutWardChequeNumberInformation(chequeNoInfoList);
				session.update(advancedChequeInfo);
				outWardCourierRegistration.setOutWardAdvancedChequeInformation(advancedChequeInfo);
			}
			outWardCourierRegistration.setOrganization(organization);
			outWardCourierRegistration.setCompany(company);
			outWardCourierRegistration.setCompanyDepo(companyDepo);
			outWardCourierRegistration.setState(state);
			outWardCourierRegistration.setDistrict(district);
			outWardCourierRegistration.setCity(city);
			outWardCourierRegistration.setParticulars(particulars);
			outWardCourierRegistration.setStockist(stockist);
			
			Integer i = (Integer) session.save(outWardCourierRegistration);
			if(i!=null)
				return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardCourierDao#editOutwardCourierRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.OutWardCourierRegistration, com.protocol.wms.model.OutWardAdvancedChequeInformation, java.util.List)
	 */
	@Override
	public Boolean editOutwardCourierRegistration(Integer orgId,
			Integer companyId, Integer depoId, Integer stateId,
			Integer districtId, Integer cityId, Integer particularId,
			Integer stockistId, Integer organizationBankId,
			OutWardCourierRegistration outWardCourierRegistration,
			OutWardAdvancedChequeInformation advancedChequeInfo,
			List<OutWardChequeNumberInformation> chequeNoInfoList) {
		Session session =sessionFactory.getCurrentSession();
		Organization organization = null;
		Company company = null;
		CompanyDepo companyDepo = null;
		State state = null;
		District district = null;
		City city = null;
		Particulars particulars = null;
		Stockist stockist = null;
		OrganizationBank organizationBank = null;
		try 
		{
			organization = (Organization) session.load(Organization.class, orgId);
			company = (Company) session.load(Company.class, companyId);
			if(depoId!=null)
			companyDepo = (CompanyDepo) session.load(CompanyDepo.class, depoId);
			/*state = (State) session.load(State.class, stateId);
			district = (District) session.load(District.class, districtId);
			city = (City) session.load(City.class, cityId);*/
			particulars = (Particulars) session.load(Particulars.class, particularId);
			if(stockistId!=null){	
				stockist = (Stockist) session.load(Stockist.class, stockistId);
				outWardCourierRegistration.setStockist(stockist);
				state = stockist.getState();
				district = stockist.getDistrict();
				city = stockist.getCity();
			}else{
				state = company.getState();
				district = company.getDistrict();
				city = company.getCity();
			}
			
			if(organizationBankId!=null){
				if(stockistId!=null)
					advancedChequeInfo.setStockist(stockist);
				
				advancedChequeInfo.setCompany(company);
				organizationBank = (OrganizationBank) session.load(OrganizationBank.class, organizationBankId);
				advancedChequeInfo.setOrganizationBank(organizationBank);
				session.save(advancedChequeInfo);
			
				if(chequeNoInfoList!=null){
					for(OutWardChequeNumberInformation tempObj : chequeNoInfoList){
						tempObj.setOutWardAdvancedChequeInformation(advancedChequeInfo);
						session.save(tempObj);
					}
				}
				
				advancedChequeInfo.setOutWardChequeNumberInformation(chequeNoInfoList);
				session.update(advancedChequeInfo);
				outWardCourierRegistration.setOutWardAdvancedChequeInformation(advancedChequeInfo);
			}
			outWardCourierRegistration.setOrganization(organization);
			outWardCourierRegistration.setCompany(company);
			outWardCourierRegistration.setCompanyDepo(companyDepo);
			outWardCourierRegistration.setState(state);
			outWardCourierRegistration.setDistrict(district);
			outWardCourierRegistration.setCity(city);
			outWardCourierRegistration.setParticulars(particulars);
			outWardCourierRegistration.setStockist(stockist);
			
			session.update(outWardCourierRegistration);
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardCourierDao#outwardCourierRegistrationListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardCourierRegistration> outwardCourierRegistrationListing(
			Integer organizationId) {
		List<OutWardCourierRegistration> outwardCourierRegistrationList = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(OutWardCourierRegistration.class,"courier")
					.createAlias("courier.organization", "organization")
					.add(Restrictions.eq("courier.status",1))
					.addOrder(Order.desc("courier.submitDate"));
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			
			outwardCourierRegistrationList = criteria.list();
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return outwardCourierRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardCourierDao#deleteOutwardCourierRegistration(java.lang.Integer)
	 */
	@Override
	public Boolean deleteOutwardCourierRegistration(Integer outwardCourierRegId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardCourierRegistration outWardCourierRegistration = null;
		try{
			outWardCourierRegistration = (OutWardCourierRegistration)session.get(OutWardCourierRegistration.class, outwardCourierRegId);
			outWardCourierRegistration.setStatus(0);
			session.update(outWardCourierRegistration);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	@Override
	public OutWardCourierRegistration loadOutwardCourierRegistrationObjectUsingoutwardCourierRegId(
			Integer outwardCourierRegId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardCourierRegistration outWardCourierRegistrationObj=null;
		try 
		{
			outWardCourierRegistrationObj=(OutWardCourierRegistration)session.get(OutWardCourierRegistration.class,outwardCourierRegId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardCourierRegistrationObj;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Map<Object,Object> searchoutwardCourierRegistration(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from) {
		List<OutWardCourierRegistration> OutWardCourierRegistration = null;
		Map<Object, Object>map = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(OutWardCourierRegistration.class,"outwardcourier")
					.createAlias("outwardcourier.organization", "organization")
					.add(Restrictions.eq("outwardcourier.status",1))
						 .setProjection(Projections.rowCount());
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("outwardcourier.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
				 
			case "Stockist" :
				  criteria.createAlias("outwardcourier.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
				  
			case "SendingToDepot" :
				 criteria.createAlias("outwardcourier.companyDepo", "companyDepo");
				 criteria.add(Restrictions.like("companyDepo.depoName", searchText,MatchMode.START));
				 break;
				 
			case "CourierName" :
				  criteria.add(Restrictions.like("outwardcourier.courierName", searchText,MatchMode.START));
				  break;
				  
			case "DocketNo" :
				  criteria.add(Restrictions.like("outwardcourier.docketNo", searchText,MatchMode.START));
				  break;
				  
			case "DeliveryPerson" :
				  criteria.add(Restrictions.like("outwardcourier.deliveryPerson", searchText,MatchMode.START));
				  break;
				  
			case "CourierPerson" :
				  criteria.add(Restrictions.like("outwardcourier.courierPerson", searchText,MatchMode.START));
				  break;
				  
		
			/*case "Weight" :
				 criteria.add(Restrictions.ge("outwardcourier.weight", fromSpecialData));
				  criteria.add(Restrictions.le("outwardcourier.weight", toSpecialData));
				  break;*/
				  
			case "Quantity" :
				 criteria.add(Restrictions.ge("outwardcourier.quantity", Integer.parseInt(fromSpecialData)));
				  criteria.add(Restrictions.le("outwardcourier.quantity", Integer.parseInt(toSpecialData)));
				  break;
				  
			/*case "CHEQUENO" :
				 criteria.createAlias("outwardcourier.outWardAdvancedChequeInformation", "outWardAdvancedChequeInformation");
				 criteria.createAlias("outWardAdvancedChequeInformation.outWardChequeNumberInformation", "outWardChequeNumberInformation");
				 criteria.add(Restrictions.like("outWardChequeNumberInformation.chequeNumber", searchText,MatchMode.START));
				 break;
				 
			case "BANKNAME" :
				 criteria.createAlias("outwardcourier.outWardAdvancedChequeInformation", "outWardAdvancedChequeInformation");
				 criteria.createAlias("outWardAdvancedChequeInformation.organizationBank", "organizationBank");
				 criteria.add(Restrictions.like("organizationBank.organizationBankName", searchText,MatchMode.START));
				 break;*/
				 
			case "State" :
				 criteria.createAlias("outwardcourier.state", "state");
				 criteria.add(Restrictions.like("state.stateName", searchText,MatchMode.START));
				 break;
				 
			case "district" :
				 criteria.createAlias("outwardcourier.district", "district");
				 criteria.add(Restrictions.like("district.districtName", searchText,MatchMode.START));
				 break;
				 
			case "City" :
				 criteria.createAlias("outwardcourier.city", "city");
				 criteria.add(Restrictions.like("city.cityName", searchText,MatchMode.START));
				 break;
					  
			case "Date" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.add(Restrictions.between("outwardcourier.courierRegistrationDate", sqlFromDate, sqlToDate));
				   }
				
				}
			 Long totalRecordCount = (Long)criteria.uniqueResult();
			
			 criteria = session.createCriteria(OutWardCourierRegistration.class,"outwardcourier")
					.createAlias("outwardcourier.organization", "organization")
					.add(Restrictions.eq("outwardcourier.status",1))
					.addOrder(Order.desc("outwardcourier.submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("outwardcourier.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
				 
			case "Stockist" :
				  criteria.createAlias("outwardcourier.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
				  
			case "SendingToDepot" :
				 criteria.createAlias("outwardcourier.companyDepo", "companyDepo");
				 criteria.add(Restrictions.like("companyDepo.depoName", searchText,MatchMode.START));
				 break;
				 
			case "CourierName" :
				  criteria.add(Restrictions.like("outwardcourier.courierName", searchText,MatchMode.START));
				  break;
				  
			case "DocketNo" :
				  criteria.add(Restrictions.like("outwardcourier.docketNo", searchText,MatchMode.START));
				  break;
				  
			case "DeliveryPerson" :
				  criteria.add(Restrictions.like("outwardcourier.deliveryPerson", searchText,MatchMode.START));
				  break;
				  
			case "CourierPerson" :
				  criteria.add(Restrictions.like("outwardcourier.courierPerson", searchText,MatchMode.START));
				  break;
				  
		
			/*case "Weight" :
				 criteria.add(Restrictions.ge("outwardcourier.weight", fromSpecialData));
				  criteria.add(Restrictions.le("outwardcourier.weight", toSpecialData));
				  break;*/
				  
			case "Quantity" :
				 criteria.add(Restrictions.ge("outwardcourier.quantity", Integer.parseInt(fromSpecialData)));
				  criteria.add(Restrictions.le("outwardcourier.quantity", Integer.parseInt(toSpecialData)));
				  break;
				  
			/*case "CHEQUENO" :
				 criteria.createAlias("outwardcourier.outWardAdvancedChequeInformation", "outWardAdvancedChequeInformation");
				 criteria.createAlias("outWardAdvancedChequeInformation.outWardChequeNumberInformation", "outWardChequeNumberInformation");
				 criteria.add(Restrictions.like("outWardChequeNumberInformation.chequeNumber", searchText,MatchMode.START));
				 break;
				 
			case "BANKNAME" :
				 criteria.createAlias("outwardcourier.outWardAdvancedChequeInformation", "outWardAdvancedChequeInformation");
				 criteria.createAlias("outWardAdvancedChequeInformation.organizationBank", "organizationBank");
				 criteria.add(Restrictions.like("organizationBank.organizationBankName", searchText,MatchMode.START));
				 break;*/
				 
			case "State" :
				 criteria.createAlias("outwardcourier.state", "state");
				 criteria.add(Restrictions.like("state.stateName", searchText,MatchMode.START));
				 break;
				 
			case "district" :
				 criteria.createAlias("outwardcourier.district", "district");
				 criteria.add(Restrictions.like("district.districtName", searchText,MatchMode.START));
				 break;
				 
			case "City" :
				 criteria.createAlias("outwardcourier.city", "city");
				 criteria.add(Restrictions.like("city.cityName", searchText,MatchMode.START));
				 break;
					  
			case "Date" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.add(Restrictions.between("outwardcourier.courierRegistrationDate", sqlFromDate, sqlToDate));
				   }
				
				}
			 from=from-1;
			   criteria.setFirstResult(from *Constant.PAGE_COUNT);
			   criteria.setMaxResults(Constant.PAGE_COUNT);
				 
			   OutWardCourierRegistration=criteria.list();
			   
			   Integer tatalPage = 0;
				if(totalRecordCount>0)
				{
				tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0)
					{
						tatalPage=tatalPage+1;
					}
				}
					map = new HashMap<Object, Object>();
					map.put("outwardCourier", OutWardCourierRegistration);
					map.put("totalPages", tatalPage);
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return map;
	}
}


