/**
 * 
 */
package com.protocol.wms.dao;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.ChequeNumberInfo;

/**
 * @author Sudhakar
 *
 */
@Repository(value="ChequeNumberInfoDaoImpl")
public class ChequeNumberInfoDaoImpl implements ChequeNumberInfoDao{

	private Logger log = Logger.getLogger(ChequeNumberInfoDaoImpl.class.getClass());
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.ChequeNumberInfoDao#loadChequeNumberInfoObjUsignChequeNumberInfoId(java.lang.Integer)
	 */
	@Override
	public ChequeNumberInfo loadChequeNumberInfoObjUsignChequeNumberInfoId(
			Integer ChequeNumberInfoId) {
		Session session =sessionFactory.getCurrentSession();
		ChequeNumberInfo chequeNumberInfoObj=null;
		try 
		{
			chequeNumberInfoObj=(ChequeNumberInfo)session.get(ChequeNumberInfo.class,ChequeNumberInfoId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return chequeNumberInfoObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.ChequeNumberInfoDao#updateChequeNumberInfoObj(com.protocol.wms.model.ChequeNumberInfo)
	 */
	@Override
	public boolean updateChequeNumberInfoObj(ChequeNumberInfo chequeNumberInfoObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(chequeNumberInfoObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer returnNoOfChequeUsingAdvancedChequeInformationId(
			Integer AdvancedChequeInformationId) {
		Session session =sessionFactory.getCurrentSession();
		List<ChequeNumberInfo> chequeNumberInfoList=null;
		try 
		{
			Criteria criteria = session.createCriteria(ChequeNumberInfo.class,"ChequeNumberInfo")
					.createAlias("ChequeNumberInfo.advancedChequeInformation", "advancedChequeInformation")
					.add(Restrictions.eq("advancedChequeInformation.advancedChequeInformationId", AdvancedChequeInformationId));
			chequeNumberInfoList=criteria.list();
			return chequeNumberInfoList.size();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return 0;
	}

	@Override
	public boolean deleteChequeNumberInfoObj(
			ChequeNumberInfo chequeNumberInfoObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(chequeNumberInfoObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

}
