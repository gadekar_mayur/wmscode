/**
 * 
 */
package com.protocol.wms.dao;



import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;






import com.protocol.wms.model.Company;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.UserDetails;

/**
 * @author admin
 *
 */
@Repository(value="UserDetailsDaoImpl")
public class UserDetailsDaoImpl implements UserDetailsDao{
	
	@Autowired
	private HttpSession httpSession;
	
	private Logger log = Logger.getLogger(SubMenuAuthenticationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.UserDetailsDao#loginValidate(java.lang.String, java.lang.String)
	 */
	@Override
	public UserDetails loginValidate(String uname, String password) {
		Session session =sessionFactory.getCurrentSession();
		UserDetails userDetails=null;
		try 
		{
			Criteria criteria = session.createCriteria(UserDetails.class)
					.add(Restrictions.eq("userName", uname))
					.add(Restrictions.eq("password", password));
			userDetails=(UserDetails) criteria.uniqueResult();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);	
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		if(userDetails!=null)
		{
			String un=userDetails.getUserName();
			String p=userDetails.getPassword();
			if(un.equals(uname)&& p.equals(password) )
			{
					switch(userDetails.getUserType().getUserTypeId()){
						case 1:
									httpSession.setAttribute("ORGANIZATION_ID", null);
									break;	
						case 2:
									try{
										Criteria criteria = session.createCriteria(Organization.class,"org")
												.createAlias("org.userDetails", "user")
												.add(Restrictions.eq("user.userDetailsId", userDetails.getUserDetailsId()));
										Organization org =(Organization) criteria.uniqueResult();
										httpSession.setAttribute("ORGANIZATION_ID", org.getOrganizationId());
										}
									catch(Exception e){System.out.println("Super Admin login");e.printStackTrace();}
									break;
						case 3 :  
									Criteria criteria = session.createCriteria(Company.class,"company")
										.createAlias("company.userDetails", "user")
										.add(Restrictions.eq("user.userDetailsId", userDetails.getUserDetailsId()));
									Company company =(Company) criteria.uniqueResult();
									httpSession.setAttribute("ORGANIZATION_ID", company.getOrganization().getOrganizationId());
									break;
						case 4 :  
								criteria = session.createCriteria(Stockist.class,"company")
								.createAlias("company.userDetails", "user")
								.add(Restrictions.eq("user.userDetailsId", userDetails.getUserDetailsId()));
								Stockist stockist =(Stockist) criteria.uniqueResult();
								httpSession.setAttribute("ORGANIZATION_ID", stockist.getOrganization().getOrganizationId());
								break;
				}
				return userDetails;
			}
			else
			{
				UserDetails u=null;
				return u;
			}
		}
		else
		{
			UserDetails u=null;
			return u;
		}
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.UserDetailsDao#checkUserNameAlreadyExitOrNot(java.lang.String)
	 */
	@Override
	public UserDetails checkUserNameAlreadyExitOrNot(String uname) {
		Session session =sessionFactory.getCurrentSession();
		UserDetails userDetailsObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(UserDetails.class)
					.add(Restrictions.eq("userName", uname.trim()));
			userDetailsObj=(UserDetails) criteria.uniqueResult();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return userDetailsObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.UserDetailsDao#checkUserNameAlreadyExitOrNot(java.lang.Integer, java.lang.String)
	 */
	@Override
	public Boolean checkUserNameAlreadyExitOrNot(Integer userDetailsId,
			String uname) {
		Session session =sessionFactory.getCurrentSession();
		UserDetails userDetailsObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(UserDetails.class)
					.add(Restrictions.eq("userName", uname.trim()))
					.add(Restrictions.not(Restrictions.eq("userDetailsId", userDetailsId)));
			userDetailsObj=(UserDetails) criteria.uniqueResult();
			if(userDetailsObj!=null)
				return false;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.UserDetailsDao#saveUserDetails(com.protocol.wms.model.UserDetails)
	 */
	@Override
	public Integer saveUserDetails(UserDetails userDetailsObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer userDetailsId=null;
		try 
		{
			userDetailsId=(Integer) session.save(userDetailsObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return userDetailsId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.UserDetailsDao#loadUserDetailsUsingUserDetailsId(java.lang.Integer)
	 */
	@Override
	public UserDetails loadUserDetailsUsingUserDetailsId(Integer userDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		UserDetails userDetailsObj=null;
		try 
		{
			userDetailsObj = (UserDetails)session.get(UserDetails.class,userDetailsId);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return userDetailsObj;
	}

	@Override
	public void updateUserDetailsObj(UserDetails userDetailsObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(userDetailsObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		
	}

}
