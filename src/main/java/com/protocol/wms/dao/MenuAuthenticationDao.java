/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.MenuAuthentication;

/**
 * @author admin
 *
 */
public interface MenuAuthenticationDao {

	public List<MenuAuthentication> loadMainMenuUsingUserTypeId(Integer userTypeId);
	
}
