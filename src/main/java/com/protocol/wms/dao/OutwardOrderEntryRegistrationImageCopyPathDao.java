package com.protocol.wms.dao;


import java.util.List;

import com.protocol.wms.model.OutWardOrderEntryRegistrationOrderCopyPath;


public interface OutwardOrderEntryRegistrationImageCopyPathDao {
	
	public List<OutWardOrderEntryRegistrationOrderCopyPath> loadImages(Integer outWardOrderEnteryRegistrationId);

}
