/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.CompanyCreditAndDiscount;

/**
 * @author Sudhakar
 *
 */
@Repository(value="AdvancedChequeInformationDaoImpl")
public class AdvancedChequeInformationDaoImpl implements AdvancedChequeInformationDao{

	private Logger log = Logger.getLogger(AdvancedChequeInformationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.AdvancedChequeInformationDao#saveAdvancedChequeInformationObj(com.protocol.wms.model.AdvancedChequeInformation, java.lang.String[])
	 */
	@Override
	public boolean saveAdvancedChequeInformationObj(
			AdvancedChequeInformation AdvancedChequeInformationObj,
			String[] checkNo) {
		Session session =sessionFactory.getCurrentSession();
		Integer advancedChequeInformationId = null;
		try 
		{
			advancedChequeInformationId=(Integer) session.save(AdvancedChequeInformationObj);
			
//			fetch save object
			
			AdvancedChequeInformation advancedChequeInformationObj=(AdvancedChequeInformation) session.get(AdvancedChequeInformation.class, advancedChequeInformationId);
			for(int i=0;i<checkNo.length;i++)
			{
				ChequeNumberInfo chequeNumberInfoObj=new ChequeNumberInfo();
				chequeNumberInfoObj.setStatus(1);
				chequeNumberInfoObj.setDepositStatus(0);
				chequeNumberInfoObj.setChequeNumber(checkNo[i]);
				chequeNumberInfoObj.setAdvancedChequeInformation(advancedChequeInformationObj);
				session.save(chequeNumberInfoObj);
			}
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.AdvancedChequeInformationDao#loadAdvancedChequeInformationUsignOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<AdvancedChequeInformation> loadAdvancedChequeInformationListUsignOrganizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<AdvancedChequeInformation> advancedChequeInformationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(AdvancedChequeInformation.class,"AdvancedChequeInformation")
				     .createAlias("AdvancedChequeInformation.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   advancedChequeInformationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return advancedChequeInformationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.AdvancedChequeInformationDao#loadChequeInformationUsignOrgIdAndCompanyIdAndCompanyBankId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray loadChequeInformationUsignOrgIdAndCompanyIdAndCompanyBankId(
			Integer orgId, Integer companyId, Integer companyBankId) {
		Session session =sessionFactory.getCurrentSession();
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			// load Advanced Cheque Information list
			Criteria criteria = session.createCriteria(AdvancedChequeInformation.class,"AdvancedChequeInformation")
					.createAlias("AdvancedChequeInformation.organization", "organization")
					.createAlias("AdvancedChequeInformation.company", "company")
					.createAlias("AdvancedChequeInformation.CompanyBank", "CompanyBank")
					.add(Restrictions.eq("status", 1))
					.add(Restrictions.eq("organization.organizationId", orgId))
					.add(Restrictions.eq("company.companyId", companyId))
					.add(Restrictions.eq("CompanyBank.companyBankId", companyBankId));
					criteria.setProjection(Projections.property("advancedChequeInformationId"));
					criteria.addOrder(Order.asc("advancedChequeInformationId"));

					List<Integer> AdvancedChequeInformationList=criteria.list();
					
					for(int i=0;i<AdvancedChequeInformationList.size();i++)
					{
					// load check information object usign advance 	Cheque id
						Criteria criteria1=session.createCriteria(ChequeNumberInfo.class,"chequeNumberInfo")
								.createAlias("chequeNumberInfo.advancedChequeInformation", "advancedChequeInformation")
								.add(Restrictions.eq("advancedChequeInformation.advancedChequeInformationId", AdvancedChequeInformationList.get(i)))
								.add((Restrictions.eq("status", 1)))
								.add(Restrictions.eq("depositStatus", 0));
						List<ChequeNumberInfo> chequeNumberInfoList=criteria1.list();
						if(chequeNumberInfoList.size()>0)
						{
							for(ChequeNumberInfo obj : chequeNumberInfoList)
							{
								json=new JSONObject();
								json.put("CHEQUE_NUMBER_INFO_ID", obj.getChequeNumberInfoId());
								json.put("CHEQUE_NO", obj.getChequeNumber());
								json.put("ADVANCE_CHEQUE_INFORMATION_ID", obj.getAdvancedChequeInformation().getAdvancedChequeInformationId());
								json_data_array.put(json);
							}
						}
					}
			System.out.println("AdvancedChequeInformationList=="+AdvancedChequeInformationList.size());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.AdvancedChequeInformationDao#loadChequeInformationUsignOrgIdAndCompanyIdAndStockistAndStockistBankId(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray loadChequeInformationUsignOrgIdAndCompanyIdAndStockistAndStockistBankId(
			Integer orgId, Integer companyId, Integer stockistId,
			Integer stockistBankId) {
		Session session =sessionFactory.getCurrentSession();
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			// load Advanced Cheque Information list
						Criteria criteria = session.createCriteria(AdvancedChequeInformation.class,"AdvancedChequeInformation")
								.createAlias("AdvancedChequeInformation.organization", "organization")
								.createAlias("AdvancedChequeInformation.company", "company")
								.createAlias("AdvancedChequeInformation.stockist", "stockist")
								.createAlias("AdvancedChequeInformation.stockistBankDetails", "stockistBankDetails")
								.add(Restrictions.eq("status", 1))
								.add(Restrictions.eq("organization.organizationId", orgId))
								.add(Restrictions.eq("company.companyId", companyId))
								.add(Restrictions.eq("stockist.stockistId", stockistId))
								.add(Restrictions.eq("stockistBankDetails.stockistBankDetailsId", stockistBankId));
								criteria.setProjection(Projections.property("advancedChequeInformationId"));
								criteria.addOrder(Order.asc("advancedChequeInformationId"));
								List<Integer> AdvancedChequeInformationList=criteria.list();
								for(int i=0;i<AdvancedChequeInformationList.size();i++)
								{
								// load check information object usign advance 	Cheque id
									Criteria criteria1=session.createCriteria(ChequeNumberInfo.class,"chequeNumberInfo")
											.createAlias("chequeNumberInfo.advancedChequeInformation", "advancedChequeInformation")
											.add(Restrictions.eq("advancedChequeInformation.advancedChequeInformationId", AdvancedChequeInformationList.get(i)))
											.add((Restrictions.eq("status", 1)))
											.add(Restrictions.eq("depositStatus", 0));
									List<ChequeNumberInfo> chequeNumberInfoList=criteria1.list();
									if(chequeNumberInfoList.size()>0)
									{
										for(ChequeNumberInfo obj : chequeNumberInfoList)
										{
											json=new JSONObject();
											json.put("CHEQUE_NUMBER_INFO_ID", obj.getChequeNumberInfoId());
											json.put("CHEQUE_NO", obj.getChequeNumber());
											json.put("ADVANCE_CHEQUE_INFORMATION_ID", obj.getAdvancedChequeInformation().getAdvancedChequeInformationId());
											json_data_array.put(json);
										}
									}
								}	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array;
	}

	@Override
	public AdvancedChequeInformation loadAdvancedChequeInformationObjUsignAdvanceChequeId(
			Integer advanceChequeid) {

		Session session =sessionFactory.getCurrentSession();
		AdvancedChequeInformation advancedChequeInformationObj=null;
		try 
		{
//			advancedChequeInformationObj=(AdvancedChequeInformation)session.get(AdvancedChequeInformation.class,advanceChequeid);
			
			Criteria criteria = session.createCriteria(AdvancedChequeInformation.class,"AdvancedChequeInformation")
					.add(Restrictions.eq("AdvancedChequeInformation.advancedChequeInformationId", advanceChequeid))
					.add((Restrictions.eq("AdvancedChequeInformation.status", 1)));
			advancedChequeInformationObj=(AdvancedChequeInformation) criteria.uniqueResult();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return advancedChequeInformationObj;
	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ChequeNumberInfo> loadChequeNumberInfoObjUsignAdvanceChequeId(
			Integer advanceChequeid) {

		Session session =sessionFactory.getCurrentSession();
		List<ChequeNumberInfo> chequeNumberInfoList=null;
		try 
		{
			Criteria criteria = session.createCriteria(ChequeNumberInfo.class,"ChequeNumberInfo")
					.createAlias("ChequeNumberInfo.advancedChequeInformation", "advancedChequeInformation")
					.add(Restrictions.eq("advancedChequeInformation.advancedChequeInformationId", advanceChequeid))
					.add((Restrictions.eq("status", 1)));
				   chequeNumberInfoList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return chequeNumberInfoList;
	
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.AdvancedChequeInformationDao#updateAdvancedChequeInformationObj(com.protocol.wms.model.AdvancedChequeInformation)
	 */
	@Override
	public boolean updateAdvancedChequeInformationObj(
			AdvancedChequeInformation advancedChequeInformationObj) 
	{
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(advancedChequeInformationObj);
			return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public boolean deleteAdvancedChequeInformationObj(
			AdvancedChequeInformation advancedChequeInformationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(advancedChequeInformationObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Object, Object> searchAdvancedChequeInformation(
			String searchOption, String searchText, Integer organizationId,Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<AdvancedChequeInformation> AdvancedChequeInformationList = null;
		Map<Object, Object>map = null;
		
		try{
			Criteria criteria = session.createCriteria(AdvancedChequeInformation.class,"advancedChequeInformation")
					.createAlias("advancedChequeInformation.organization", "organization")
					.add(Restrictions.eq("advancedChequeInformation.status",1))
				    .setProjection(Projections.rowCount())
					.addOrder(Order.desc("advancedChequeInformation.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			 switch(searchOption){
				case "Company" : 
					criteria.createAlias("advancedChequeInformation.company", "company")
					 .add(Restrictions.like("company.companyName", searchText,MatchMode.START));
					 break;
				case "Organization" :
					 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					 break;
				case "CompanyBank" :
					 criteria.createAlias("advancedChequeInformation.CompanyBank", "companybank")
					 .add(Restrictions.like("companybank.companyBankName", searchText,MatchMode.START));
					 break;
				case "Stockist" :
					 criteria.createAlias("advancedChequeInformation.stockist", "stockist")
					 .add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
					 break;
				case "StockistBank" :
					 criteria.createAlias("advancedChequeInformation.stockistBankDetails", "stockistbankdetails")
					 .add(Restrictions.like("stockistbankdetails.StockistBankName", searchText,MatchMode.START));
					 break;
				case "NoOfCheque" :
					 criteria.add(Restrictions.eq("advancedChequeInformation.noOfCheque",Integer.parseInt(searchText)));
					 break;
				case "ChequeNo" :
					criteria.createAlias("advancedChequeInformation.chequeNumberInfoList", "chequeNumberInfoList")
					.add(Restrictions.like("chequeNumberInfoList.chequeNumber", searchText,MatchMode.START));
					 break;
			 }
			 
			 
			Long  totalRecordCount = (Long)criteria.uniqueResult();
			 
			 
			  criteria = session.createCriteria(AdvancedChequeInformation.class,"advancedChequeInformation")
						.createAlias("advancedChequeInformation.organization", "organization")
						.add(Restrictions.eq("advancedChequeInformation.status",1))
						.addOrder(Order.desc("advancedChequeInformation.submitDate"));
				if(organizationId!=null)
					criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				 switch(searchOption){
					case "Company" : 
						criteria.createAlias("advancedChequeInformation.company", "company")
						 .add(Restrictions.like("company.companyName", searchText,MatchMode.START));
						 break;
					case "Organization" :
						 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
						 break;
					case "CompanyBank" :
						 criteria.createAlias("advancedChequeInformation.CompanyBank", "companybank")
						 .add(Restrictions.like("companybank.companyBankName", searchText,MatchMode.START));
						 break;
					case "Stockist" :
						 criteria.createAlias("advancedChequeInformation.stockist", "stockist")
						 .add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
						 break;
					case "StockistBank" :
						 criteria.createAlias("advancedChequeInformation.stockistBankDetails", "stockistbankdetails")
						 .add(Restrictions.like("stockistbankdetails.StockistBankName", searchText,MatchMode.START));
						 break;
					case "NoOfCheque" :
						 criteria.add(Restrictions.eq("advancedChequeInformation.noOfCheque",Integer.parseInt(searchText)));
						 break;
					case "ChequeNo" :
						criteria.createAlias("advancedChequeInformation.chequeNumberInfoList", "chequeNumberInfoList")
						.add(Restrictions.like("chequeNumberInfoList.chequeNumber", searchText,MatchMode.START));
						 break;
				 }
				 
				  from=from-1;
				   criteria.setFirstResult(from *Constant.PAGE_COUNT);
				   criteria.setMaxResults(Constant.PAGE_COUNT);
				   AdvancedChequeInformationList = criteria.list();
				   
				   Integer tatalPage = 0;
					if(totalRecordCount>0)
					{
						tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
						if((totalRecordCount%Constant.PAGE_COUNT)!=0)
						{
							tatalPage=tatalPage+1;
						}
					}
				
						map = new HashMap<Object, Object>();
						map.put("advancedChequeInformationList", AdvancedChequeInformationList);
						map.put("totalPages", tatalPage);
				 
				 
			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
}
