/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentImagePath;
import com.protocol.wms.model.CartingAgentVehiclePhotoImagePath;
import com.protocol.wms.model.UserDetails;

/**
 * @author admin
 *
 */
public interface CartingAgentDao {

	public Boolean saveCartingAgentDetails(Integer organizationId,CartingAgent cartingAgent,UserDetails userDetails);
	
	public Boolean updateCartingAgent(CartingAgent cartingAgent);
	
	public List<CartingAgent> getCartingAgentList(Integer organizationId);
	
	public CartingAgent getCartingAgentDetailsById(Integer agentId);
	
	public Boolean deleteCartingAgentDetails(Integer agentId);

	public Map<String,Object>searchCartingAgentsDetails(int searchOptioinINT,String searchText,Integer from,Integer organizationId);

	public CartingAgent loadCartingAgentObjectUsingAgentId(Integer agentId);

	public Boolean updateCartingAgentObject(CartingAgent cartingAgent);

	public Integer insertCartingAgentDetilsAndDeleteOldCartingAgentDetails(Integer cartingAgentId,  CartingAgent cartingAgent);
	
	public void updateCartingAgentImagesObjectByNewCartingAgentId(Integer oldCartingAgentId, Integer newCartingAgentId);
	
	public List<CartingAgentImagePath> updateCartingAgentPhotoImage(Integer cartingAgentId, List<CartingAgentImagePath> cartingAgentPhotoImgList);
	
	public Boolean deleteCartingAgentPhotoImage(Integer cartingAgentPhotoImageId);
	
	public List<CartingAgentVehiclePhotoImagePath> updateCartingAgentVehiclePhotoImage(Integer cartingAgentId, List<CartingAgentVehiclePhotoImagePath> cartingAgentVehiclePhotoImgList);
	
	public Boolean deleteCartingAgentVehiclePhotoImage(Integer cartingAgentVehiclePhotoImageId);
}
