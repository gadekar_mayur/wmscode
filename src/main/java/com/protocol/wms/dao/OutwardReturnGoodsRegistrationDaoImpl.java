/**
 * 
 */
package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OutWardReturnGoodsLRDetails;
import com.protocol.wms.model.OutWardReturnGoodsRegistration;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.TransporterDetails;

/**
 * @author admin
 *
 */
@Repository(value="OutwardReturnGoodsRegistrationDaoImpl")
public class OutwardReturnGoodsRegistrationDaoImpl implements
		OutwardReturnGoodsRegistrationDao {

	private Logger log = Logger.getLogger(OutwardReturnGoodsRegistrationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsRegistrationDao#getOutWardReturnGoodsRegistrationDetails(java.lang.Integer)
	 */
	@Override
	public OutWardReturnGoodsRegistration getOutWardReturnGoodsRegistrationDetails(
			Integer outWardReturnGoodsRegistrationId) {
		OutWardReturnGoodsRegistration outWardReturnGoodsRegistration = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardReturnGoodsRegistration = (OutWardReturnGoodsRegistration) session.get(OutWardReturnGoodsRegistration.class, outWardReturnGoodsRegistrationId);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return outWardReturnGoodsRegistration;
	}


	@Override
	public List<OutWardReturnGoodsLRDetails> getOutWardReturnGoodsLRDetailsHavingStatusOneUsingOutwardReturnGoodsRegId(
			Integer outWardReturnGoodsRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardReturnGoodsLRDetails> outWardReturnGoodsLRDetailsList = null;
		try
		{
			Criteria criteria = session.createCriteria(OutWardReturnGoodsLRDetails.class,"outWardReturnGoodsLRDetails")
					.createAlias("outWardReturnGoodsLRDetails.outWardReturnGoodsRegistration", "outWardReturnGoodsRegistration")
					.add(Restrictions.eq("outWardReturnGoodsRegistration.outWardReturnGoodsRegistrationId", outWardReturnGoodsRegistrationId))
				     .add(Restrictions.eq("status", 1));
				   outWardReturnGoodsLRDetailsList = criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardReturnGoodsLRDetailsList;
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsRegistrationDao#saveOutwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.util.List, java.util.List, java.util.Map, com.protocol.wms.model.OutWardReturnGoodsRegistration)
	 */
	@Override
	public Boolean saveOutwardReturnGoodsRegistration(
			Integer orgId,
			Integer companyId,
			Integer stockistId,
			Integer transporterId,
			Integer employeeId,
			List<OutWardReturnGoodsLRDetails> lrDetailsList,
			OutWardReturnGoodsRegistration outWardReturnGoodsRegistration) {

		Session session =sessionFactory.getCurrentSession();
		Organization organization = null;
		Company company = null;
		Stockist stockist = null;
		TransporterDetails transporterDetails = null;
		EmployeeDetails employeeDetails = null;
		
		try{
			organization =(Organization)session.load(Organization.class, orgId);
			company =(Company)session.load(Company.class, companyId);
			if(stockistId!=null)
				stockist =(Stockist)session.load(Stockist.class, stockistId);
			transporterDetails =(TransporterDetails)session.load(TransporterDetails.class, transporterId);
			if(employeeId!=null)
				employeeDetails =(EmployeeDetails)session.load(EmployeeDetails.class, employeeId);
			outWardReturnGoodsRegistration.setOrganization(organization);
			outWardReturnGoodsRegistration.setCompany(company);
			outWardReturnGoodsRegistration.setStockist(stockist);
			outWardReturnGoodsRegistration.setTransporterDetails(transporterDetails);
			outWardReturnGoodsRegistration.setEmployeeDetails(employeeDetails);
			
			Integer id = (Integer) session.save(outWardReturnGoodsRegistration);
			if(id!=null){
				for(int i = 0;i<lrDetailsList.size();i++){
					lrDetailsList.get(i).setOutWardReturnGoodsRegistration(outWardReturnGoodsRegistration);
					session.save(lrDetailsList.get(i));
				}
				return true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsRegistrationDao#updateInwardReturnGoodsLrReg(java.util.List, java.util.List)
	 */
	@Override
	public Boolean updateInwardReturnGoodsLrReg(
			List<OutWardReturnGoodsLRDetails> lrDetailsList) {
		Session session =sessionFactory.getCurrentSession();
		try{
			for(int i = 0;i<lrDetailsList.size();i++){
				OutWardReturnGoodsLRDetails tempObj = (OutWardReturnGoodsLRDetails) session.load(OutWardReturnGoodsLRDetails.class, lrDetailsList.get(i).getOutWardReturnGoodsLRDetailsId());
				//tempObj.setClaimImagePath(lrDetailsList.get(i).getClaimImagePath());
				//tempObj.setLrImagePath(lrDetailsList.get(i).getLrImagePath());
				session.update(tempObj);
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsRegistrationDao#outwardReturnGoodsRegListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardReturnGoodsRegistration> outwardReturnGoodsRegListing(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardReturnGoodsRegistration> outWardReturnGoodsRegList = null;
		try
		{
			Criteria criteria = session.createCriteria(OutWardReturnGoodsRegistration.class,"outWardReturnGoodsReg")
				     .createAlias("outWardReturnGoodsReg.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardReturnGoodsRegList = criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardReturnGoodsRegList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsRegistrationDao#deleteOutwardReturnGoodsRegDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteOutwardReturnGoodsRegDetails(Integer outwardRetGoodsRegId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardReturnGoodsRegistration outWardReturnGoodsRegistration= null;
		try{
			outWardReturnGoodsRegistration = (OutWardReturnGoodsRegistration)session.get(OutWardReturnGoodsRegistration.class, outwardRetGoodsRegId);
			outWardReturnGoodsRegistration.setStatus(0);
			session.update(outWardReturnGoodsRegistration);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsRegistrationDao#loadOutWardReturnGoodsRegistrationObjByOutwardReturnGoodsRegId(java.lang.Integer)
	 */
	@Override
	public OutWardReturnGoodsRegistration loadOutWardReturnGoodsRegistrationObjByOutwardReturnGoodsRegId(Integer outWardReturnGoodsRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardReturnGoodsRegistration outWardReturnGoodsRegistration= null;
		try{
			outWardReturnGoodsRegistration = (OutWardReturnGoodsRegistration)session.get(OutWardReturnGoodsRegistration.class, outWardReturnGoodsRegistrationId);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return outWardReturnGoodsRegistration;
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsRegistrationDao#editInwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.OutWardReturnGoodsRegistration)
	 */
	@Override
	public Boolean editOtwardReturnGoodsRegistration(Integer orgId,
			Integer companyId, Integer stockistId, Integer transporterId,
			Integer employeeId,
			OutWardReturnGoodsRegistration outwardReturnGoodsRegistration) {
		Organization organization = null;
		Company company = null;
		Stockist stockist = null;
		TransporterDetails transporterDetails = null;
		EmployeeDetails employeeDetails = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization =(Organization)session.load(Organization.class, orgId);
			company =(Company)session.load(Company.class, companyId);
			if(stockistId!=null)
			stockist =(Stockist)session.load(Stockist.class, stockistId);
			transporterDetails =(TransporterDetails)session.load(TransporterDetails.class, transporterId);
			if(employeeId!=null)
			employeeDetails =(EmployeeDetails)session.load(EmployeeDetails.class, employeeId);
			outwardReturnGoodsRegistration.setOrganization(organization);
			outwardReturnGoodsRegistration.setCompany(company);
			outwardReturnGoodsRegistration.setStockist(stockist);
			outwardReturnGoodsRegistration.setTransporterDetails(transporterDetails);
			outwardReturnGoodsRegistration.setEmployeeDetails(employeeDetails);
			session.update(outwardReturnGoodsRegistration);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Map<Object,Object> searchOutReturnGoodsRegListing(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from) {
		
		List<OutWardReturnGoodsRegistration> OutReturnGoodsRegistration = null;
		Map<Object, Object>map = null;

		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(OutWardReturnGoodsRegistration.class,"outwardgoodreturn")
					.createAlias("outwardgoodreturn.organization", "organization")
					.add(Restrictions.eq("outwardgoodreturn.status",1))
					 .setProjection(Projections.rowCount())
					.addOrder(Order.desc("outwardgoodreturn.submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
		
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("outwardgoodreturn.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
			case "Transporter" :
				 criteria.createAlias("outwardgoodreturn.transporterDetails", "transporterDetails");
				 criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
				 break;
			case "LrNo" :
				   criteria.createAlias("outwardgoodreturn.outWardReturnGoodsLRDetails", "DataFromAnotherTable");
				   criteria.add(Restrictions.like("DataFromAnotherTable.lrNo", searchText,MatchMode.START));
				   break;
			case "ClaimNo" :
				 criteria.createAlias("outwardgoodreturn.outWardReturnGoodsLRDetails", "DataFromAnotherTable");
				 criteria.add(Restrictions.like("DataFromAnotherTable.claimNo", searchText,MatchMode.START));
				 break;
			case "DriverName" :
				  criteria.add(Restrictions.like("outwardgoodreturn.driverName", searchText,MatchMode.START));
				  break;
			case "DeliveredEmployee" :
				  criteria.createAlias("outwardgoodreturn.employeeDetails", "employeeDetails");
				  criteria.add(Restrictions.like("employeeDetails.name", searchText,MatchMode.START));
				  break;
	  
			case "Stockist" :
				  criteria.createAlias("outwardgoodreturn.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
			case "ClaimAmount" :
				 criteria.createAlias("outwardgoodreturn.outWardReturnGoodsLRDetails", "DataFromAnotherTable");
				 criteria.add(Restrictions.ge("DataFromAnotherTable.claimAmount", Float.parseFloat(fromSpecialData)));
				  criteria.add(Restrictions.le("DataFromAnotherTable.claimAmount", Float.parseFloat(toSpecialData)));
				  break;
			case "Date": 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.add(Restrictions.between("outwardgoodreturn.registrationDate", sqlFromDate, sqlToDate));
				   }
				
			}
			  Long totalRecordCount = (Long)criteria.uniqueResult();
			
			
			
			 criteria = session.createCriteria(OutWardReturnGoodsRegistration.class,"outwardgoodreturn")
					.createAlias("outwardgoodreturn.organization", "organization")
					.add(Restrictions.eq("outwardgoodreturn.status",1))
					.addOrder(Order.desc("outwardgoodreturn.submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
		
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("outwardgoodreturn.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
			case "Transporter" :
				 criteria.createAlias("outwardgoodreturn.transporterDetails", "transporterDetails");
				 criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
				 break;
			case "LrNo" :
				   criteria.createAlias("outwardgoodreturn.outWardReturnGoodsLRDetails", "DataFromAnotherTable");
				   criteria.add(Restrictions.like("DataFromAnotherTable.lrNo", searchText,MatchMode.START));
				   break;
			case "ClaimNo" :
				 criteria.createAlias("outwardgoodreturn.outWardReturnGoodsLRDetails", "DataFromAnotherTable");
				 criteria.add(Restrictions.like("DataFromAnotherTable.claimNo", searchText,MatchMode.START));
				 break;
			case "DriverName" :
				  criteria.add(Restrictions.like("outwardgoodreturn.driverName", searchText,MatchMode.START));
				  break;
			case "DeliveredEmployee" :
				  criteria.createAlias("outwardgoodreturn.employeeDetails", "employeeDetails");
				  criteria.add(Restrictions.like("employeeDetails.name", searchText,MatchMode.START));
				  break;
	  
			case "Stockist" :
				  criteria.createAlias("outwardgoodreturn.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
			case "ClaimAmount" :
				 criteria.createAlias("outwardgoodreturn.outWardReturnGoodsLRDetails", "DataFromAnotherTable");
				 criteria.add(Restrictions.ge("DataFromAnotherTable.claimAmount", Float.parseFloat(fromSpecialData)));
				  criteria.add(Restrictions.le("DataFromAnotherTable.claimAmount", Float.parseFloat(toSpecialData)));
				  break;
			case "Date": 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.add(Restrictions.between("outwardgoodreturn.registrationDate", sqlFromDate, sqlToDate));
				   }
			}
			
			
			 from=from-1;
			   criteria.setFirstResult(from *Constant.PAGE_COUNT);
			   criteria.setMaxResults(Constant.PAGE_COUNT);
				 
			   OutReturnGoodsRegistration=criteria.list();
			   
			   Integer tatalPage = 0;
				if(totalRecordCount>0)
				{
					 
					tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0)
					{
						tatalPage=tatalPage+1;
					}
				}
//				System.out.println("totalRecordCount : "+totalRecordCount);
//				System.out.println("tatalPage : "+tatalPage);
			
					map = new HashMap<Object, Object>();
					map.put("paidInvoice", OutReturnGoodsRegistration);
					map.put("totalPages", tatalPage);
			   

		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return map;
	}

}
