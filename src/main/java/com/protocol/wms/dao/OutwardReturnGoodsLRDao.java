/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.OutWardReturnGoodsLRDetails;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsClaimImagePath;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsLrImagePath;

/**
 * @author admin
 *
 */
public interface OutwardReturnGoodsLRDao {

	public OutWardReturnGoodsLRDetails getOutWardReturnGoodsLRDetailsByLrId(Integer outWardReturnGoodsLRDetailsId);
	
	public List<OutWardReturnGoodsLRDetails> outWardReturnGoodsLRDetailsList(Integer outWardReturnGoodsRegistrationId);
	
	public Boolean deleteOutwardReturnGoodsRegLrDetails(Integer LR_ID);
	
	public OutWardReturnGoodsLRDetails loadOutWardReturnGoodsLRDetailsByLrRegId(Integer outWardReturnGoodsLRDetailsId);
	
	public Boolean editOutwardReturnGoodsRegLrDetails(OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails);
	
	public List<OutWardReturnGoodsLRDetailsLrImagePath> updateOutwardReturnGoodsLrRegLrImage(Integer outwardReturnGoodsLrRegistrationId, List<OutWardReturnGoodsLRDetailsLrImagePath> lrImgList);
	
	public Boolean deleteOutwardReturnGoodsLrRegLrImage(Integer lrImageId);
	
	public  List<OutWardReturnGoodsLRDetailsClaimImagePath> updateOutwardReturnGoodsLrRegClaimCopyImage(Integer outwardReturnGoodsLrRegistrationId, List<OutWardReturnGoodsLRDetailsClaimImagePath> claimCopyImgList);
	
	public Boolean deleteOutwardReturnGoodsLrRegClaimCopyImage(Integer claimCopyImageId);
}
