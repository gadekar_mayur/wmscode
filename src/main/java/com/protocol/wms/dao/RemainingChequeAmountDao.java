/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.RemainingChequeAmount;

/**
 * @author Sudhakar
 *
 */
public interface RemainingChequeAmountDao {
	
	public Integer saveRemainingChequeAmountObj(RemainingChequeAmount RemainingChequeAmountObj);
	
	public RemainingChequeAmount loadRemainingChequeAmountUsignOrgIdAndCompnayIdAndStockistId(Integer organizationId,Integer companyId,Integer stockistId);
	
	public void deleteRemainingChequeAmountObj(RemainingChequeAmount remainingChequeAmountObj);
	
	public RemainingChequeAmount loadRemainingChequeAmountUsignRemainingChequeAmountId(Integer RemainingChequeAmountId);
	
	public List<RemainingChequeAmount> loadremainingChequeAmountListUsignOrgIdAndCompIdAndStockistId(Integer orgId,Integer companyId,Integer stockistId);
	
	public void updateRemainingChequeAmountObj(RemainingChequeAmount remainingChequeAmountObj);
	
	public RemainingChequeAmount loadRemainingChequeAmountObjUsignChequeNumberInfoId(Integer chequeNumberInfoId);

}
