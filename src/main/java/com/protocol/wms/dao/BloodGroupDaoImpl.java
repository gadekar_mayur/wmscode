/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.BloodGroup;

/**
 * @author Sudhakar
 *
 */
@Repository(value="BloodGroupDaoImpl")
public class BloodGroupDaoImpl implements BloodGroupDao{
	
	private Logger log = Logger.getLogger(BloodGroupDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.BloodGroupDao#loadBloodGroupList()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<BloodGroup> loadBloodGroupList() {
		Session session =sessionFactory.getCurrentSession();
		List<BloodGroup> bloodGroupsList=null;
		try 
		{
			Criteria criteria = session.createCriteria(BloodGroup.class,"BloodGroup")
					.add(Restrictions.eq("BloodGroup.status", 1));
			bloodGroupsList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return bloodGroupsList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.BloodGroupDao#loadBloodGroupObjectUsingBloodGroupId(java.lang.Integer)
	 */
	@Override
	public BloodGroup loadBloodGroupObjectUsingBloodGroupId(Integer bloodGroupId) {
		Session session =sessionFactory.getCurrentSession();
		BloodGroup bloodGroup=null;
		try 
		{
			bloodGroup=(BloodGroup)session.get(BloodGroup.class,bloodGroupId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return bloodGroup;
	}

}
