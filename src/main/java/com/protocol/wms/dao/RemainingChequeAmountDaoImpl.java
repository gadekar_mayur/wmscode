/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.RemainingChequeAmount;

/**
 * @author Sudhakar
 *
 */
@Repository(value="RemainingChequeAmountDaoImpl")
public class RemainingChequeAmountDaoImpl implements RemainingChequeAmountDao{

	private Logger log = Logger.getLogger(RemainingChequeAmountDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;

	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RemainingChequeAmountDao#saveRemainingChequeAmountObj(com.protocol.wms.model.RemainingChequeAmount)
	 */
	@Override
	public Integer saveRemainingChequeAmountObj(
			RemainingChequeAmount RemainingChequeAmountObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer remainingChequeAmountId = null;
		try 
		{
			remainingChequeAmountId=(Integer) session.save(RemainingChequeAmountObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return remainingChequeAmountId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RemainingChequeAmountDao#loadRemainingChequeAmountUsignOrgIdAndCompnayIdAndStockistId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public RemainingChequeAmount loadRemainingChequeAmountUsignOrgIdAndCompnayIdAndStockistId(
			Integer organizationId, Integer companyId, Integer stockistId) {
		Session session =sessionFactory.getCurrentSession();
		RemainingChequeAmount remainingChequeAmountObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(RemainingChequeAmount.class,"RemainingChequeAmount")
					.createAlias("RemainingChequeAmount.organization", "organization")
					.createAlias("RemainingChequeAmount.stockist", "stockist")
					.createAlias("RemainingChequeAmount.company", "company")
					.add(Restrictions.eq("organization.organizationId", organizationId))
					.add(Restrictions.eq("stockist.stockistId", stockistId))
					.add(Restrictions.eq("company.companyId", companyId));
			remainingChequeAmountObj=(RemainingChequeAmount) criteria.uniqueResult();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return remainingChequeAmountObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RemainingChequeAmountDao#deleteRemainingChequeAmountObj(java.lang.Integer)
	 */
	@Override
	public void deleteRemainingChequeAmountObj(RemainingChequeAmount remainingChequeAmountObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(remainingChequeAmountObj);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RemainingChequeAmountDao#laodRemainingChequeAmountUsignRemainingChequeAmountId(java.lang.Integer)
	 */
	@Override
	public RemainingChequeAmount loadRemainingChequeAmountUsignRemainingChequeAmountId(
			Integer RemainingChequeAmountId) {
		Session session =sessionFactory.getCurrentSession();
		RemainingChequeAmount remainingChequeAmountObj=null;
		try 
		{
			remainingChequeAmountObj=(RemainingChequeAmount)session.get(RemainingChequeAmount.class,RemainingChequeAmountId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return remainingChequeAmountObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RemainingChequeAmountDao#loadremainingChequeAmountListUsignOrgIdAndCompIdAndStockistId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RemainingChequeAmount> loadremainingChequeAmountListUsignOrgIdAndCompIdAndStockistId(
			Integer orgId, Integer companyId, Integer stockistId) {
		Session session =sessionFactory.getCurrentSession();
		List<RemainingChequeAmount> remainingChequeAmountList=null;
		try 
		{
			Criteria criteria = session.createCriteria(RemainingChequeAmount.class,"RemainingChequeAmount")
					.createAlias("RemainingChequeAmount.organization", "organization")
					.createAlias("RemainingChequeAmount.stockist", "stockist")
					.createAlias("RemainingChequeAmount.company", "company")
					.add(Restrictions.eq("organization.organizationId", orgId))
					.add(Restrictions.eq("stockist.stockistId", stockistId))
					.add(Restrictions.eq("status", 1))
					.add(Restrictions.eq("company.companyId", companyId));
			remainingChequeAmountList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return remainingChequeAmountList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RemainingChequeAmountDao#updateRemainingChequeAmountObj(com.protocol.wms.model.RemainingChequeAmount)
	 */
	@Override
	public void updateRemainingChequeAmountObj(
			RemainingChequeAmount remainingChequeAmountObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(remainingChequeAmountObj);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RemainingChequeAmountDao#loadRemainingChequeAmountObjUsignChequeNumberInfoId(java.lang.Integer)
	 */
	@Override
	public RemainingChequeAmount loadRemainingChequeAmountObjUsignChequeNumberInfoId(
			Integer chequeNumberInfoId) {
			Session session =sessionFactory.getCurrentSession();
			RemainingChequeAmount remainingChequeAmountObj=null;
			try 
			{
				Criteria criteria = session.createCriteria(RemainingChequeAmount.class,"RemainingChequeAmount")
						.createAlias("RemainingChequeAmount.chequeNumberInfo", "chequeNumberInfo")
						.add(Restrictions.eq("chequeNumberInfo.chequeNumberInfoId", chequeNumberInfoId))
						.add(Restrictions.eq("status", 1));
				remainingChequeAmountObj=(RemainingChequeAmount) criteria.uniqueResult();
			} 
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return remainingChequeAmountObj;
	}

}
