/**
 * 
 */
package com.protocol.wms.dao;

import com.protocol.wms.model.CartingAgentTripCount;

/**
 * @author Sudhakar
 *
 */
public interface CartingAgentTripCountDao {
	
	public CartingAgentTripCount loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(Integer orgId,Integer cartingAgentId);
	
	public void updateCartingAgentTripCountObj(CartingAgentTripCount cartingAgentTripCountObj);
	
	public Integer saveCartingAgentTripCountObj(CartingAgentTripCount cartingAgentTripCountObj);
	

}
