/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;


import java.util.Map;

import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistrationOrderCopyPath;

/**
 * @author Sudhakar
 *
 */
public interface OutWardOrderEntryRegistrationDao {
	public Boolean checkName(String orderNo,Integer companyId);
	
	public Integer saveOutWardOrderEntryRegistration(OutWardOrderEntryRegistration obj);

	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsZero(Integer organizationId);
	
	public OutWardOrderEntryRegistration loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(Integer OutWardOrderEntryRegistrationId);
	
	public void updateOutWardOrderEntryRegistration(OutWardOrderEntryRegistration obj);
	
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsOne(Integer organizationId);
	
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsOneAndInvoiceStatusIsOne(Integer organizationId);
	
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfStatusIsOne(Integer organizationId);
	
	public List<Integer> loadOutWardOrderEnteryRegistrationIdUsignOrgIdAndCompIdAndStockistId(Integer orgid,Integer companyId,Integer stockistId);
	
	public boolean updateOutWardOrderEntryRegistrationObj(OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj,Integer orderModeId);

	public Map<String,Object>searchOutwardOrderEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);
	
	public List<OutWardOrderEntryRegistrationOrderCopyPath> updateOutwardGoodsRegOrderCopyImage(Integer outwardGoodsRegistrationId, List<OutWardOrderEntryRegistrationOrderCopyPath> orderCopyImgList);
	
	public Boolean deleteOutwardGoodsRegistrationOrderCopyImage(Integer orderCopyImageId);

	public Map<String,Object>searchOutwardViewOrderEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer from,Integer organizationId);

	public Map<String,Object>searchOutwardinvoiceEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer String, Integer organizationId);

	public Map<String,Object>searchOutwardInvoiceEntryAddedRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from,Integer organizationId);
	
	public boolean deleteOutWardOrderEntry(OutWardOrderEntryRegistration outWardOrderEntryRegistration);

	
}
