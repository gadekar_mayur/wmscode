/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OrganizationDocument;

/**
 * @author Sudhakar
 *
 */

@Repository(value="OrganizationDaoImpl")
public class OrganizationDaoImpl implements OrganizationDao{

	private Logger log = Logger.getLogger(OrganizationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationDao#saveOrganization(com.protocol.wms.model.Organization)
	 */
	@Override
	public Integer saveOrganization(Organization organizationObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer organizationId = null;
		try 
		{
			organizationId=(Integer) session.save(organizationObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return organizationId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationDao#editOrganizationDetails(com.protocol.wms.model.Organization)
	 */
	@Override
	public Boolean editOrganizationDetails(Organization organizationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(organizationObj.getUserDetails());
			session.update(organizationObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationDao#loadOrganizationObjectUsingOrganiztionId(java.lang.Integer)
	 */
	@Override
	public Organization loadOrganizationObjectUsingOrganiztionId(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		Organization organizationObj=null;
		try 
		{
			organizationObj=(Organization)session.get(Organization.class,organizationId );
			if("0".equals(organizationObj.getOrganizationStatus()))
				return null;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return organizationObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationDao#updateOrganization(com.protocol.wms.model.Organization)
	 */
	@Override
	public void updateOrganization(Organization organizationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(organizationObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationDao#loadListOfOrganization()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Organization> loadListOfOrganization() {
		Session session =sessionFactory.getCurrentSession();
		List<Organization> organizationsList=null;
		try 
		{
			Criteria criteria = session.createCriteria(Organization.class,"Organization")
					.add(Restrictions.eq("Organization.organizationStatus", "1"))
					.addOrder(Order.desc("Organization.submitDate"));;
			organizationsList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return organizationsList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationDao#searchOrganizationDetails(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String , Object> searchOrganizationDetails(String searchOption,String searchText, Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<Organization> organizationsList=null;
		Map<String , Object> map = null;
		try 
		{
			searchText=searchText+"%";
			/*if("Mobile".equals(searchOption)){
				Query query = session.createSQLQuery("CALL searchOrganizationDetails(:searchText)")
						.addEntity(Organization.class)
						.setParameter("searchText", searchText);
				organizationsList = query.list();
				
			}else{*/
				Criteria criteria = session.createCriteria(Organization.class,"Organization")
						.add(Restrictions.eq("Organization.organizationStatus", "1"))
						.setProjection(Projections.rowCount());

				if("Organization".equals(searchOption))
					criteria.add(Restrictions.like("Organization.organizationName", searchText));
				else if("ContactPerson".equals(searchOption))
					criteria.add(Restrictions.like("Organization.organizationContactPerson", searchText));
				else if("Email".equals(searchOption))
					criteria.add(Restrictions.like("Organization.organizationEmailId", searchText));
			   Long totalRecordCount = (Long)criteria.uniqueResult();
			   
			   criteria = session.createCriteria(Organization.class,"Organization")
						.add(Restrictions.eq("Organization.organizationStatus", "1"))
						.addOrder(Order.desc("organizationId"));
				if("Organization".equals(searchOption))
					criteria.add(Restrictions.like("Organization.organizationName", searchText));
				else if("ContactPerson".equals(searchOption))
					criteria.add(Restrictions.like("Organization.organizationContactPerson", searchText));
				else if("Email".equals(searchOption))
					criteria.add(Restrictions.like("Organization.organizationEmailId", searchText));
				from = from - 1 ;
			    criteria.setFirstResult(from*Constant.PAGE_COUNT);
			    criteria.setMaxResults(Constant.PAGE_COUNT);
				organizationsList=criteria.list();
				
				//= = = = = = Pagination Logic = = = = = = 
				Integer tatalPage = 0;
				if(totalRecordCount>0){
					tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						tatalPage=tatalPage+1;
					}
				}
				//= = = = = =End  Pagination Logic = = = = = = 
				map = new HashMap<String, Object>();
				map.put("organizationsList", organizationsList);
				map.put("totalPages", tatalPage);
			//}
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationDao#loasOrganiztionObjectUsingUserDetailsId(java.lang.Integer)
	 */
	@Override
	public Organization loadOrganiztionObjectUsingUserDetailsId(
			Integer userDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		Organization organizationObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(Organization.class,"Organization")
					.createAlias("Organization.userDetails", "userDetails")
					.add(Restrictions.eq("userDetails.userDetailsId", userDetailsId))
					.add(Restrictions.eq("Organization.organizationStatus", "1"));
					
			organizationObj=(Organization) criteria.uniqueResult();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return organizationObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationDao#deleteOrganiztion(java.lang.Integer)
	 */
	@Override
	public Boolean deleteOrganiztion(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		Organization organization = null;
		try{
			organization = (Organization)session.get(Organization.class, organizationId);
			organization.setOrganizationStatus("0");
			session.update(organization);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationDao#deleteOrganizationDocument(java.lang.Integer)
	 */
	@Override
	public Boolean deleteOrganizationDocument(Integer organizationDocumentId) {
		Session session =sessionFactory.getCurrentSession();
		OrganizationDocument OrganizationDocument = null;
		try{
			OrganizationDocument = (OrganizationDocument)session.get(OrganizationDocument.class, organizationDocumentId);
			OrganizationDocument.setStatus(0);
			session.update(OrganizationDocument);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

}
