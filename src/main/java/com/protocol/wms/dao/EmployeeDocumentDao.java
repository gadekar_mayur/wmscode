/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.EmployeeDocument;
import com.protocol.wms.model.EmployeeDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
public interface EmployeeDocumentDao {
	
	public Integer saveEmployeeDocument(EmployeeDocument employeeDocumentObj);
	
	public List<EmployeeDocument> loadEmployeeDocumetListUsingOrganizationId(Integer organizationId);
	
	public Map<String,Object> searchEmployeeDocumet(String searchOption,String searchText,Integer from, Integer organizationId);

	public Boolean deleteEmployeeDocumet(Integer empDocumentId);
	
	public EmployeeDocument loadEmployeeDocumentObjById(Integer employeeDocumentId);
	
	public Boolean updateEmployeeDocumentObj(EmployeeDocument employeeDocument);
	
	public List<EmployeeDocumentImagePath> updateEmployeeImages(Integer employeeDocumentId, List<EmployeeDocumentImagePath> documentList);
	
	public Boolean deleteEmployeeUploadDocumentFileImage(Integer employeeDocumentPathId);
}
