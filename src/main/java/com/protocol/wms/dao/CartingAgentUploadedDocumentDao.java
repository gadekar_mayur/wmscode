/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentUploadedDocument;
import com.protocol.wms.model.CartingAgentUploadedDocumentImagePath;

/**
 * @author admin
 *
 */
public interface CartingAgentUploadedDocumentDao {
	
	public Boolean saveCartingAgentUploadDocument(Integer organizationId,Integer cartingAgentId, Integer documentTypeId,CartingAgentUploadedDocument cartingAgentUploadedDocument);
	
	public List<CartingAgentUploadedDocument> getCartingAgentUploadedDocList(Integer organizationId);
	
	public Boolean deleteCartingAgentDocumentDetails(Integer docId);
	
	public CartingAgentUploadedDocument updateCartingAgentDocumentInfo(Integer documentTypeId, Integer cartingAgentId, Integer cartingAgentDocId);
	
	public List<CartingAgentUploadedDocumentImagePath> updateCartingAgentUploadDocumentImage(Integer cartingAgentDocumentId, List<CartingAgentUploadedDocumentImagePath> documentList);
	
	public Boolean deleteCartingAgentDocumentFileImage(Integer cartingAgentUploadDocumentPathId);

	public Map<Object,Object> searchCartingAgentsUploadedDocumentDetails(
			String searchOptioin, String searchText, Integer attribute, Integer from);
}
