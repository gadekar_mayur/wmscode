/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.OutWardReturnGoodsCreditNote;
import com.protocol.wms.model.OutWardReturnGoodsCreditNoteCreditNoteImagePath;

/**
 * @author admin
 *
 */
public interface OutWardReturnGoodsCreditNoteDao {
 
	//public OutWardReturnGoodsCreditNote getOutWardReturnGoodsCreditNoteDetailsByLR_ID(Integer outWardReturnGoodsLRDetailsId);
	
	public Boolean saveOutwardReturnGoodsRegCreditNote(Integer organiztionId,OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote);
	
	public Boolean updateOutwardReturnGoodsRegCreditNote(OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote);
	
	public List<OutWardReturnGoodsCreditNote> getOutwardReturnGoodsRegCreditNoteList(Integer organizationId);
	
	public Boolean deleteOutwardReturnGoodsRegCreditNote(Integer creditNoteId);
	
	public OutWardReturnGoodsCreditNote loadOutWardReturnGoodsCreditNoteObjById(Integer outWardReturnGoodsCreditNoteId);
	
	public Boolean editOutwardReturnGoodsRegCreditNote(Integer organiztionId,OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote);

	public Map<Object,Object> searchOutwardReturnGoodsRegCreditNote(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from);
	
	public List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> updateOutwardGoodsRegCreditNoteAttachImage(Integer creditNoteId, List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> creditNoteAttachList);
	
	public Boolean deleteOutwardGoodsRegCreditNoteAttachImage(Integer creditNoteAttachImageId);
}
