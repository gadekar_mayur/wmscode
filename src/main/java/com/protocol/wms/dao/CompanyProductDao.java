package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.CompanyProduct;

public interface CompanyProductDao {

	public Integer saveNewCompanyProductDetailsAndDeleteOldOne(Integer companyProductId,CompanyProduct companyProduct);
	
	public Boolean updateCompanyProductDetails(CompanyProduct companyProduct);
	//Add by nutan
	public CompanyProduct loadCompanyProductObjectUsignCompanyProductId(Integer companyProductId);
	
	public Map<String , Object> searchCompanyProductDetails(String searchOption,String searchText,Double fromValue,Double toValue, Integer from, Integer organizationId);
	
	public List<CompanyProduct> loadCompanyProductListUsignCompanyId(Integer companyId);
}
