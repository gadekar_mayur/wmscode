/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.InwardGoodsRegistration;
import com.protocol.wms.model.InwardGoodsRegistrationLRImgPath;

/**
 * @author Sudhakar
 *
 */
public interface InwardGoodsRegistrationDao {

	public Integer saveInwardGoodsRegistration(InwardGoodsRegistration inwardGoodsRegistrationObj);
	
	public Boolean editInwardGoodsRegistration(Integer organizationId,Integer comapnyId,Integer sendingDepo,Integer transporterId,InwardGoodsRegistration inwardGoodsRegistrationObj);
	
	public InwardGoodsRegistration loadInwardGoodsRegistrationUsignInwardGoodsRegistrationId(Integer inwardGoodsRegistrationId);
	
	public List<InwardGoodsRegistration> loadInwardGoodsRegistrationListUsignOrganizationId(Integer orgId);
	
	public Map<Object, Object> searchInwardGoodsRegDetails(String selectedValue,String searchText,String fromSpecialData,String toSpecialData,Integer organizationId, Integer from);
	
	public void updateInwardGoodsRegistrationObject(InwardGoodsRegistration inwardGoodsRegistrationObj);
	
	public Boolean deleteInwardGoodsRegistrationDetails(Integer inwardGoodsRegistrationId);
	
	public Boolean deleteInwardGoodsRegistrationLrImage(Integer lrImageId);
	
	public List<InwardGoodsRegistrationLRImgPath> updateInwardGoodsRegLrImages(Integer inwardGoodsRegistrationId,List<InwardGoodsRegistrationLRImgPath> lrImgList);
}
