/**
 * 
 */
package com.protocol.wms.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.UserType;

/**
 * @author Sudhakar
 *
 */
@Repository(value="UserTypeDaoImpl")
public class UserTypeDaoImpl implements UserTypeDao{
	
private Logger log = Logger.getLogger(UserTypeDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.UserTypeDao#loadUserTypeUsingUserTypeId(java.lang.Integer)
	 */
	@Override
	public UserType loadUserTypeUsingUserTypeId(Integer userTypeId) {
		Session session =sessionFactory.getCurrentSession();
		UserType usaTypeObj=null;
		try 
		{
			usaTypeObj = (UserType)session.get(UserType.class, userTypeId);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);	
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return usaTypeObj;
	}

}
