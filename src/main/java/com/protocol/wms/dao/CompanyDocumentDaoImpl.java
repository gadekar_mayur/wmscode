/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.CompanyDocument;
import com.protocol.wms.model.CompanyDocumentImagePath;

/**
 * @author admin
 *
 */
@Repository(value="CompanyDocumentDaoImpl")
public class CompanyDocumentDaoImpl implements CompanyDocumentDao{

	private Logger log = Logger.getLogger(CompanyDocumentDaoImpl.class.getClass());

	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyDocumentDao#searchUploadedCompanyDocsDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Object,Object> searchUploadedCompanyDocsDetails(String searchOption, String searchText, Integer organizationId, Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyDocument> companyDocsList = null;
		Map<Object, Object>map = null;
		try{
			Criteria criteria = session.createCriteria(CompanyDocument.class,"companyDocs")
					.createAlias("companyDocs.organization", "organization")
					.createAlias("companyDocs.company", "company")
					.add(Restrictions.eq("organization.organizationStatus", "1"))
					.add(Restrictions.eq("company.status",1))
					.createAlias("companyDocs.documentListType", "docType")
					.add(Restrictions.eq("companyDocs.status",1))
					  .setProjection(Projections.rowCount())
					.addOrder(Order.desc("companyDocs.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("DocumentType".equals(searchOption))
				criteria.add(Restrictions.like("docType.documentName", searchText,MatchMode.START));
			
			 Long totalRecordCount = (Long)criteria.uniqueResult();
			 
			 criteria = session.createCriteria(CompanyDocument.class,"companyDocs")
					.createAlias("companyDocs.organization", "organization")
					.createAlias("companyDocs.company", "company")
					.createAlias("companyDocs.documentListType", "docType")
					.add(Restrictions.eq("organization.organizationStatus", "1"))
					.add(Restrictions.eq("company.status",1))
					.add(Restrictions.eq("companyDocs.status",1))
					.addOrder(Order.desc("companyDocs.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("DocumentType".equals(searchOption))
				criteria.add(Restrictions.like("docType.documentName", searchText,MatchMode.START));
			
			  from=from-1;
			   criteria.setFirstResult(from *Constant.PAGE_COUNT);
			   criteria.setMaxResults(Constant.PAGE_COUNT);
			   companyDocsList=criteria.list();
			   
			   Integer tatalPage = 0;
				if(totalRecordCount>0)
				{
					tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0)
					{
						tatalPage=tatalPage+1;
					}
				}
			
					map = new HashMap<Object, Object>();
					map.put("companyDoc", companyDocsList);
					map.put("totalPages", tatalPage);
			   
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

	@Override
	public CompanyDocument loadCompanyDocumentObjById(Integer companyDocumentId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyDocument companyDocument = null;
		try{
			companyDocument = (CompanyDocument) session.get(CompanyDocument.class, companyDocumentId);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return companyDocument;
	}

	@Override
	public Boolean deleteCompanyDocumentFileImage(Integer companyDocumentPathId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyDocumentImagePath companyDocumentImagePath = null;
		try{
			companyDocumentImagePath = (CompanyDocumentImagePath) session.get(CompanyDocumentImagePath.class, companyDocumentPathId);
			session.delete(companyDocumentImagePath);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public Boolean updateCompanyDocumentObj(CompanyDocument companyDocument) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.update(companyDocument);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<CompanyDocumentImagePath> updateCompanyUploadDocumentImage(
			Integer companyDocumentId,
			List<CompanyDocumentImagePath> documentList) {
		List<CompanyDocumentImagePath> lst = null;
		CompanyDocument companyDocument = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			companyDocument = (CompanyDocument) session.load(CompanyDocument.class, companyDocumentId);
			for(CompanyDocumentImagePath tempObj : documentList){
				tempObj.setCompanyDocument(companyDocument);
				session.save(tempObj);
			}
			lst = companyDocument.getCompanyDocumentImagePathList();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}
}
