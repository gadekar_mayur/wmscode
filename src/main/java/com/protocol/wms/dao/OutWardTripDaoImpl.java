/**
 * 
 */
package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.OutWardDispatchEnteryRegistration;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.OutWardTrip;
import com.protocol.wms.model.OutWardTripTransporterDetails;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OutWardTripDaoImpl")
public class OutWardTripDaoImpl implements OutWardTripDao{

	private Logger log = Logger.getLogger(OutWardTripDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardTripDao#saveOutWardTripObj(com.protocol.wms.model.OutWardTrip)
	 */
	@Override
	public Integer saveOutWardTripObj(OutWardTrip obj) {
		Session session =sessionFactory.getCurrentSession();
		Integer OutWardTripId = null;
		try 
		{
			OutWardTripId=(Integer) session.save(obj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return OutWardTripId;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardTripDao#loadOutWardTripObjUsignOutWardTripiD(java.lang.Integer)
	 */
	@Override
	public OutWardTrip loadOutWardTripObjUsignOutWardTripiD(
			Integer outWardTripId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardTrip outWardTripObj=null;
		try 
		{
			outWardTripObj=(OutWardTrip)session.get(OutWardTrip.class,outWardTripId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardTripObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardTripDao#loadOutWardTripListOfStatusOneUsignOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardTrip> loadOutWardTripListOfStatusOneUsignOrganizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardTrip> outWardTripList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
				     .createAlias("OutWardTrip.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardTripList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardTripList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardTripDao#loadOutWardTripListOfGetPassStatusOneUsignOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardTrip> loadOutWardTripListOfGetPassStatusZeroAndStatusOneUsignOrganizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardTrip> outWardTripList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
				     .createAlias("OutWardTrip.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("getPassStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardTripList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardTripList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardTripDao#updateOutWardTripObj(com.protocol.wms.model.OutWardTrip)
	 */
	@Override
	public void updateOutWardTripObj(OutWardTrip obj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(obj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardTripDao#loadloadOutWardTripListOfGetPassStatusOneUsignOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardTrip> loadloadOutWardTripListOfGetPassStatusOneUsignOrganizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardTrip> outWardTripList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
				     .createAlias("OutWardTrip.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("getPassStatus", 1))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardTripList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardTripList;
	}
	@Override
	public Boolean updateOutWardTripDetails(Integer cartingAgentId,
			OutWardTrip outWardTripObj) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgent cartingAgent =null;
		try 
		{
			cartingAgent = (CartingAgent) session.load(CartingAgent.class, cartingAgentId);
			outWardTripObj.setCartingAgent(cartingAgent);
			session.update(outWardTripObj);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardTripDao#deleteOutWardTripEntryObj(com.protocol.wms.model.OutWardTrip)
	 */
	@Override
	public boolean deleteOutWardTripEntryObj(OutWardTrip outWardTripObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(outWardTripObj);
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
	@Override
	public Map<String,Object>searchOutwardViewTripList(String selectedValue,
			String searchText, String fromSpecialData, String toSpecialData,
			Integer from,Integer organizationId) {

		Session session =sessionFactory.getCurrentSession();
		List<OutWardTrip> outWardTripList=null;
		Map<String,Object>map=null;
		try 
		{
			map = new HashMap<String, Object>();
			if("InvoiceNumber".equals(selectedValue))
			{
				List<Integer> outWardOrderDispatchEnteryRegIdList = null;	
				
				Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
						 .createAlias("OutWardOrderInvoiceEnteryRegistration.outWardDispatchEnteryRegistration", "outWardDispatchEnteryRegistration")
						 .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
						 .add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText, MatchMode.START))
						 .add(Restrictions.eq("status", 1))
					     .setProjection(Projections.distinct(Projections.property("outWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId")));
				
				if(organizationId!=null)
					criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
				outWardOrderDispatchEnteryRegIdList = criteria.list();
				
				List<Integer> outWardTripIdList = null;
				criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
						 .createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip")
					     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.eq("tripStatus", 1))
					     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardOrderDispatchEnteryRegIdList))
					     .setProjection(Projections.distinct(Projections.property("outWardTrip.outWardTripId")));
				
			   if(organizationId!=null)
			     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			   outWardTripIdList = criteria.list();
			   
			   criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
					     .createAlias("OutWardTrip.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardTrip.outWardTripId", outWardTripIdList))
					      .setProjection(Projections.rowCount());
				
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
			   Long totalRecordCount = (Long)criteria.uniqueResult();
					  
					   
			   criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
					     .createAlias("OutWardTrip.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardTrip.outWardTripId", outWardTripIdList))
					     .addOrder(Order.desc("submitDate"));
				
			   if(organizationId!=null)
			     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			   
			   from = from - 1 ;
			    criteria.setFirstResult(from*Constant.PAGE_COUNT);
			    criteria.setMaxResults(Constant.PAGE_COUNT);
				   
				outWardTripList=criteria.list();
					 
				Integer totalPages = 0;
				if(totalRecordCount>0){
					totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPages=totalPages+1;
					}
				}
				map.put("outWardTripList", outWardTripList);
				map.put("totalPages", totalPages);
							 
			}
			else if("TransporterName".equals(selectedValue)){
				
				List<Integer> outwardTripIdList = null;
				Criteria criteria = session.createCriteria(OutWardTripTransporterDetails.class,"OutWardTripTransporterDetails")
											.createAlias("OutWardTripTransporterDetails.outWardTrip", "outWardTrip")
											.createAlias("OutWardTripTransporterDetails.transporterDetails", "transporterDetails")
											.add(Restrictions.like("transporterDetails.transporterName",searchText,MatchMode.START))
											.setProjection(Projections.distinct(Projections.property("outWardTrip.outWardTripId")));
					
				 outwardTripIdList = criteria.list();
					
				 criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
					     .createAlias("OutWardTrip.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardTrip.outWardTripId", outwardTripIdList))
					     .setProjection(Projections.rowCount());
				 
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   Long totalRecordCount = (Long)criteria.uniqueResult();
			   
				 criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
					     .createAlias("OutWardTrip.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardTrip.outWardTripId", outwardTripIdList))
					     .addOrder(Order.desc("submitDate"));
				 
			   if(organizationId!=null)
			     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
	   
				from = from - 1 ;
			    criteria.setFirstResult(from*Constant.PAGE_COUNT);
			    criteria.setMaxResults(Constant.PAGE_COUNT);
			    outWardTripList=criteria.list();
				
				Integer totalPages = 0;
				if(totalRecordCount>0){
					totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPages=totalPages+1;
					}
				}
				map.put("outWardTripList", outWardTripList);
				map.put("totalPages", totalPages);
								 
			}
			else if("StockistName".equals(selectedValue) || "CompanyName".equals(selectedValue)){
				
				List<Integer> outwardTripIdList = null;
				
				Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
							.createCriteria("OutWardDispatchEnteryRegistration.outWardTrip","outWardTrip")
							.createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
							.add(Restrictions.eq("status", 1))
							.setProjection(Projections.distinct(Projections.property("outWardTrip.outWardTripId")));
					  
				switch(selectedValue){
				
				case "StockistName":
						criteria.createCriteria("OutWardDispatchEnteryRegistration.stockist","stockist");
						criteria.add(Restrictions.like("stockist.stockistName", searchText, MatchMode.START));
						break;
						
				case "CompanyName":
						criteria.createCriteria("OutWardDispatchEnteryRegistration.company","company");
						criteria.add(Restrictions.like("company.companyName", searchText, MatchMode.START));
						break;
				}
				
				outwardTripIdList = criteria.list();
				
				criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
					     .createAlias("OutWardTrip.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardTrip.outWardTripId", outwardTripIdList))
					     .addOrder(Order.desc("submitDate"))
					     .setProjection(Projections.rowCount());
				
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   //outWardTripList = criteria.list();
				Long totalRecordCount = (Long)criteria.uniqueResult();  
					   
						
				criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
					     .createAlias("OutWardTrip.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardTrip.outWardTripId", outwardTripIdList))
					     .addOrder(Order.desc("submitDate"));
				
			   if(organizationId!=null)
			     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				
			   from = from - 1 ;
			   criteria.setFirstResult(from*Constant.PAGE_COUNT);
			   criteria.setMaxResults(Constant.PAGE_COUNT);
			   outWardTripList = criteria.list();
						
				Integer totalPages = 0;
				if (totalRecordCount > 0) {
					totalPages = (int) (totalRecordCount / Constant.PAGE_COUNT);
					if ((totalRecordCount % Constant.PAGE_COUNT) != 0) {
						totalPages = totalPages + 1;
					}
				}
				map.put("outWardTripList", outWardTripList);
				map.put("totalPages", totalPages);
								 
			}
			else
			{
				Criteria criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
					     .createAlias("OutWardTrip.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .addOrder(Order.desc("submitDate")).setProjection(Projections.rowCount());;
				
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   switch(selectedValue)
					   {
					   case "TripNo":
						   criteria.add(Restrictions.like("OutWardTrip.tripNo",searchText, MatchMode.START));
						   break;
					   
					   case "LoadingCharges":
						   criteria.add(Restrictions.ge("OutWardTrip.loadingCharges",Double.parseDouble(fromSpecialData)));
						   criteria.add(Restrictions.le("OutWardTrip.loadingCharges",Double.parseDouble(toSpecialData)));
						   break; 
						   
					   case "Organization":
						   criteria.add(Restrictions.like("organization.organizationName", searchText, MatchMode.START));
						   break;
						   
					   case "DispatchBy":
						   criteria.add(Restrictions.like("OutWardTrip.dispatchBy",searchText, MatchMode.START));
						   break; 
						   
					   case "VehicleNo":
						   criteria.add(Restrictions.like("OutWardTrip.vehicleNo",searchText, MatchMode.START));
						   break;  
						   
					   case "DriverNo":
						   criteria.add(Restrictions.like("OutWardTrip.driverNo",searchText, MatchMode.START));
						   break;
						   
					   case "NoOfCases" :
							  criteria.add(Restrictions.ge("OutWardTrip.noOfCases", Integer.parseInt((fromSpecialData))));
							  criteria.add(Restrictions.le("OutWardTrip.noOfCases", Integer.parseInt((toSpecialData))));
							  break; 
					   
						case "CartingAgent":
							 criteria.createAlias("OutWardTrip.cartingAgent", "cartingAgent");
							 criteria.add(Restrictions.like("cartingAgent.name", searchText,MatchMode.START));
							 break;
							 
						case "Date":
					 		if(fromSpecialData!=""&&toSpecialData!=""){
							    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
							    java.util.Date d1= null;
								d1 = dtf.parse(fromSpecialData);
								java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
								d1 = dtf.parse(toSpecialData);
								java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
								
								criteria.add(Restrictions.between("OutWardTrip.submitDate", sqlFromDate,sqlToDate));
					 		}
					   }
								
								
					  Long totalRecordCount = (Long)criteria.uniqueResult();
							
							//---------------------------------else----
							
						 criteria = session.createCriteria(OutWardTrip.class,"OutWardTrip")
								     .createAlias("OutWardTrip.organization", "organization")
								     .add(Restrictions.eq("status", 1))
								     .addOrder(Order.desc("submitDate"));
							
								   if(organizationId!=null)
								     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
								   
								   switch(selectedValue)
								   {
								   case "TripNo":
									   criteria.add(Restrictions.like("OutWardTrip.tripNo",searchText, MatchMode.START));
									   break;
								   
								   case "LoadingCharges":
									   criteria.add(Restrictions.ge("OutWardTrip.loadingCharges",Double.parseDouble(fromSpecialData)));
									   criteria.add(Restrictions.le("OutWardTrip.loadingCharges",Double.parseDouble(toSpecialData)));
									   break; 
									   
								   case "Organization":
									   criteria.add(Restrictions.like("organization.organizationName", searchText, MatchMode.START));
									   break;
									   
								   case "DispatchBy":
									   criteria.add(Restrictions.like("OutWardTrip.dispatchBy",searchText, MatchMode.START));
									   break; 
									   
								   case "VehicleNo":
									   criteria.add(Restrictions.like("OutWardTrip.vehicleNo",searchText, MatchMode.START));
									   break;  
									   
								   case "DriverNo":
									   criteria.add(Restrictions.like("OutWardTrip.driverNo",searchText, MatchMode.START));
									   break;
									   
								   case "NoOfCases" :
										  criteria.add(Restrictions.ge("OutWardTrip.noOfCases", Integer.parseInt((fromSpecialData))));
										  criteria.add(Restrictions.le("OutWardTrip.noOfCases", Integer.parseInt((toSpecialData))));
										  break; 
								   
									case "CartingAgent":
										 criteria.createAlias("OutWardTrip.cartingAgent", "cartingAgent");
										 criteria.add(Restrictions.like("cartingAgent.name", searchText,MatchMode.START));
										 break;
										 
									case "Date":
								 		if(fromSpecialData!=""&&toSpecialData!=""){
										    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
										    java.util.Date d1= null;
											d1 = dtf.parse(fromSpecialData);
											java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
											d1 = dtf.parse(toSpecialData);
											java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
											
											criteria.add(Restrictions.between("OutWardTrip.submitDate", sqlFromDate,sqlToDate));
								 		}
								   }
											
						from = from - 1 ;
					    criteria.setFirstResult(from*Constant.PAGE_COUNT);
					    criteria.setMaxResults(Constant.PAGE_COUNT);
					    outWardTripList=criteria.list();
								
						Integer tatalPage = 0;
						if(totalRecordCount>0){
							tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
							if((totalRecordCount%Constant.PAGE_COUNT)!=0){
								tatalPage=tatalPage+1;
							}
						}
						
						map.put("outWardTripList", outWardTripList);
						map.put("totalPages", tatalPage);			
//=-=------else
						}
				   }
		
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

}
