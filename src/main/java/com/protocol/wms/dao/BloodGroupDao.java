/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.BloodGroup;

/**
 * @author Sudhakar
 *
 */
public interface BloodGroupDao 
{
	public List<BloodGroup> loadBloodGroupList();
	
	public BloodGroup loadBloodGroupObjectUsingBloodGroupId(Integer bloodGroupId);
}
