/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;


import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.CheckingDoneCheckingSlipImagePath;

/**
 * @author Sudhakar
 *
 */
public interface CheckingDoneDao {

	public Integer saveCheckingDone(CheckingDone checkingDoneObj);
	
	public CheckingDone loadCheckingDoneObjByInwardReturnGoodsRegId(Integer inwardReturnGoodRegistrationId);
	
	public CheckingDone loadCheckingDoneObjByCheckingDoneId(Integer checkingDoneId);
	
	public Boolean editInwardReturnGoodsRegCheckingDoneDetails(CheckingDone checkingDone,Integer checkingDoneEmployeeDetailsId);
	
	public List<CheckingDoneCheckingSlipImagePath> updateCheckingDoneRegCheckingSlipImage(Integer checkingDoneId, List<CheckingDoneCheckingSlipImagePath> checkingSlipImgList);
	
	public Boolean deleteCheckingDoneCheckingSlipImage(Integer checkingSlipImageId);
}
