/**
 * 
 */
package com.protocol.wms.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.CartingAgentTripCount;

/**
 * @author Sudhakar
 *
 */
@Repository(value="CartingAgentTripCountDaoImpl")
public class CartingAgentTripCountDaoImpl implements CartingAgentTripCountDao {

	private Logger log = Logger.getLogger(CartingAgentTripCountDaoImpl.class.getClass());

	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentTripCountDao#loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public CartingAgentTripCount loadCartingAgentTripCountObjUsignOrgIdAndCartingAgentId(
			Integer orgId, Integer cartingAgentId) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgentTripCount cartingAgentTripCountObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(CartingAgentTripCount.class,"CartingAgentTripCount")
					.createAlias("CartingAgentTripCount.organization", "organization")
					.createAlias("CartingAgentTripCount.cartingAgent", "cartingAgent")
					.add(Restrictions.eq("organization.organizationId", orgId))
					.add(Restrictions.eq("cartingAgent.cartingAgentId", cartingAgentId));
			cartingAgentTripCountObj=(CartingAgentTripCount) criteria.uniqueResult();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return cartingAgentTripCountObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentTripCountDao#updateCartingAgentTripCountObj(com.protocol.wms.model.CartingAgentTripCount)
	 */
	@Override
	public void updateCartingAgentTripCountObj(
			CartingAgentTripCount cartingAgentTripCountObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(cartingAgentTripCountObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentTripCountDao#saveCartingAgentTripCountObj(com.protocol.wms.model.CartingAgentTripCount)
	 */
	@Override
	public Integer saveCartingAgentTripCountObj(
			CartingAgentTripCount cartingAgentTripCountObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer cartingAgentTripCountId = null;
		try 
		{
			cartingAgentTripCountId=(Integer) session.save(cartingAgentTripCountObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return cartingAgentTripCountId;
	}

}
