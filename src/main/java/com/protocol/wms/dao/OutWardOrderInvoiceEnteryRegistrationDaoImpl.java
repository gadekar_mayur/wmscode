/**
 * 
 */
package com.protocol.wms.dao;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardDispatchEnteryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OutWardOrderInvoiceEnteryRegistrationDaoImpl")
public class OutWardOrderInvoiceEnteryRegistrationDaoImpl implements OutWardOrderInvoiceEnteryRegistrationDao {

	private Logger log = Logger.getLogger(OutWardOrderInvoiceEnteryRegistrationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#saveOutWardOrderInvoiceEnteryRegistrationObject(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration)
	 */
	@Override
	public Integer saveOutWardOrderInvoiceEnteryRegistrationObject(
			OutWardOrderInvoiceEnteryRegistration obj) {
		Session session =sessionFactory.getCurrentSession();
		Integer outWardOrderInvoiceEnteryRegistrationId = null;
		try 
		{
			outWardOrderInvoiceEnteryRegistrationId=(Integer) session.save(obj);
		} 
		catch(DataAccessException e)
		{
			System.out.println("In catch");
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#loadOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("dispatchEnteryRegistrationStatus", 0))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(java.lang.Integer)
	 */
	@Override
	public OutWardOrderInvoiceEnteryRegistration loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(
			Integer OutWardOrderInvoiceEnteryRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj=null;
		try 
		{
			outWardOrderInvoiceEnteryRegistrationObj=(OutWardOrderInvoiceEnteryRegistration)session.get(OutWardOrderInvoiceEnteryRegistration.class,OutWardOrderInvoiceEnteryRegistrationId );
			System.out.println("list="+ outWardOrderInvoiceEnteryRegistrationObj.getOutWardOrderEnteryRegistration().getOutWardOrderEntryRegistrationOrderCopyPathList().size());
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#updateOutWardOrderInvoiceEnteryRegistrationObj(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration)
	 */
	@Override
	public void updateOutWardOrderInvoiceEnteryRegistrationObj(
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.saveOrUpdate(outWardOrderInvoiceEnteryRegistrationObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#loadOutWardOrderInvoiceEnteryRegistrationListEqualToCurrentDate(java.sql.Date, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListEqualToCurrentDate(
			Date currentDate, Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("lastDate", currentDate))
				     .add(Restrictions.eq("paymentStatus", 0))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(java.sql.Date, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListLessThanToCurrentDate(
			Date currentDate, Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.lt("lastDate", currentDate))
				     .add(Restrictions.eq("paymentStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(java.sql.Date, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(
			Date currentDate, Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.gt("lastDate", currentDate))
				     .add(Restrictions.eq("paymentStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#loadOutWardOrderInvoiceEnteryRegistrationListUsignoutWardOrderEnteryRegistrationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListUsignoutWardOrderEnteryRegistrationId(
			Integer outWardOrderEnteryRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
					 .createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "outWardOrderEnteryRegistration")
					 .add(Restrictions.eq("outWardOrderEnteryRegistration.outWardOrderEnteryRegistrationId", outWardOrderEnteryRegistrationId))
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("paymentStatus", 0))
				     .addOrder(Order.desc("submitDate"));;
				   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#outWardOrderInvoiceEnteryRegistrationListWithStatusOne(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListWithStatusOne(
			Integer orgnizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));
				     if(orgnizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", orgnizationId));
				     outWardOrderInvoiceEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistrationUsignoutWardOrderEnteryRegistrationId(
			Integer outWardOrderEnteryRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
					 .createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "outWardOrderEnteryRegistration")
					 .add(Restrictions.eq("outWardOrderEnteryRegistration.outWardOrderEnteryRegistrationId", outWardOrderEnteryRegistrationId))
				     .add(Restrictions.eq("status", 1));
				   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(
			Integer outWardDispatchEnteryRegistrationId) {
		OutWardOrderInvoiceEnteryRegistration a=null;
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
					 .createAlias("OutWardOrderInvoiceEnteryRegistration.outWardDispatchEnteryRegistration", "outWardDispatchEnteryRegistration")
					 .add(Restrictions.eq("status", 1))
					 .add(Restrictions.eq("outWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegistrationId));	   
			outWardOrderInvoiceEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#updateInvoiceObj(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration)
	 */
	@Override
	public boolean updateInvoiceObj(
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj,String invoiceDate,String invoiceDiscountTakentOrNot,Double netAmount) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.merge(outWardOrderInvoiceEnteryRegistrationObj);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#deleteOutWardOrderInvoiceEnteryRegistrationObj(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration)
	 */
	@Override
	public boolean deleteOutWardOrderInvoiceEnteryRegistrationObj(
			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(outWardOrderInvoiceEnteryRegistrationObj);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

//	/* (non-Javadoc)
//	 * @see com.protocol.wms.dao.OutWardOrderInvoiceEnteryRegistrationDao#updateInvoiceObject(com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration)
//	 */
//	@Override
//	public boolean updateInvoiceObject(
//			OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj) {
//		Session session =sessionFactory.getCurrentSession();
//		try{
//			session.update(outWardOrderInvoiceEnteryRegistrationObj);
//			return true;
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			log.error(e);
//		}
//		return false;
//	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> searchOutwardDispatchEntryRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer from, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		Map<String,Object>map=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("dispatchEnteryRegistrationStatus", 0))
				     .setProjection(Projections.rowCount());
			
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   //outWardOrderEnteryRegistration
				   
				   
				   switch(selectedValue)
					 {
					 	case "InvoiceNumber": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
							 break;
					 	case "NetAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "InvoiceDate":
					 		if(fromSpecialData!=""&&toSpecialData!=""){
							    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
							    java.util.Date d1= null;
								d1 = dtf.parse(fromSpecialData);
								java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
								d1 = dtf.parse(toSpecialData);
								java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
								
								criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
							  }
					 	/*case "DaysCount": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
							 break;	*/
					 		
					 	case "Company" : 
					 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
							 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
						case "Organization" :
							 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
							 
						case "Stockist" :
							  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
							  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
							  break;
							  
						case "OrderId" :
							 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
							 break;
							 
						case "OrderNo" :
						   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
							  break;
							  
						case "Date" : 
							if(fromSpecialData!=""&&toSpecialData!=""){
								   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
								   java.util.Date d1= null;
									d1 = dtf.parse(fromSpecialData);
									java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
									d1 = dtf.parse(toSpecialData);
									java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
									
									criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									criteria.add(Restrictions.between("OutWardOrderEntryRegistration.orderDate", sqlFromDate, sqlToDate));
							   }
					 }
				   
				   Long totalRecordCount = (Long)criteria.uniqueResult();
				   
			 criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("dispatchEnteryRegistrationStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   //outWardOrderEnteryRegistration
				   
				   
				   switch(selectedValue)
					 {
					 	case "InvoiceNumber": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
							 break;
					 	case "NetAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "InvoiceDate":
					 		if(fromSpecialData!=""&&toSpecialData!=""){
							    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
							    java.util.Date d1= null;
								d1 = dtf.parse(fromSpecialData);
								java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
								d1 = dtf.parse(toSpecialData);
								java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
								
								criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
							  }
					 	/*case "DaysCount": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
							 break;	*/
					 		
					 	case "Company" : 
					 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
							 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
						case "Organization" :
							 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
							 
						case "Stockist" :
							  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
							  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
							  break;
							  
						case "OrderId" :
							 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
							 break;
							 
						case "OrderNo" :
						   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
							  break;
							  
						case "Date" : 
							if(fromSpecialData!=""&&toSpecialData!=""){
								   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
								   java.util.Date d1= null;
									d1 = dtf.parse(fromSpecialData);
									java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
									d1 = dtf.parse(toSpecialData);
									java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
									
									criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									criteria.add(Restrictions.between("OutWardOrderEntryRegistration.orderDate", sqlFromDate, sqlToDate));
							   }
					 }
				   	from = from - 1 ;
				    criteria.setFirstResult(from*Constant.PAGE_COUNT);
				    criteria.setMaxResults(Constant.PAGE_COUNT);
				   
				    outWardOrderInvoiceEnteryRegistrationList=criteria.list();
				   
				   Integer totalPage = 0;
					if(totalRecordCount>0){
						totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
						if((totalRecordCount%Constant.PAGE_COUNT)!=0){
							totalPage = totalPage+1;
						}
					}
					map = new HashMap<String, Object>();
					map.put("outWardOrderInvoiceEnteryRegistrationList", outWardOrderInvoiceEnteryRegistrationList);
					map.put("totalPages", totalPage);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

	/*@Override
	public List<OutWardOrderInvoiceEnteryRegistration> searchOutwardViewDispatchEntryRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		try 
		{
			if("InvoiceNumber".equals(selectedValue)||"NetAmount".equals(selectedValue) || "InvoiceDate".equals(selectedValue))
			{
			
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardDispatchEnteryRegistrationList=criteria.list();
			
			
				Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"InvoiceEntry")
					     .createAlias("InvoiceEntry.outWardDispatchEnteryRegistration", "outWardDispatchEnteryRegistration")
					     .createAlias("InvoiceEntry.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				  //   .add(Restrictions.eq("dispatchEnteryRegistrationStatus", 0))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   //outWardOrderEnteryRegistration
				   
				   
				   switch(selectedValue)
					 {
					 	case "InvoiceNumber": 
							 criteria.add(Restrictions.like("InvoiceEntry.invoiceNo", searchText,MatchMode.START));
							 break;
					 	case "NetAmount":
					 		  criteria.add(Restrictions.ge("InvoiceEntry.netAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("InvoiceEntry.netAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "InvoiceDate":
					 		if(fromSpecialData!=""&&toSpecialData!=""){
							    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
							    java.util.Date d1= null;
								d1 = dtf.parse(fromSpecialData);
								java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
								d1 = dtf.parse(toSpecialData);
								java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
								
								criteria.add(Restrictions.between("InvoiceEntry.invoiceDate", sqlFromDate,sqlToDate));
							  }
					 		
					 }
				   List<Integer> outWardOrderEnteryRegistrationIdList = criteria.list();
					
				   criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
						     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .addOrder(Order.desc("submitDate"));
						   if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   
						   outWardOrderInvoiceEnteryRegistrationList = criteria.list();
			}
			else
			{
					 	case "DaysCount": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
							 break;	
				Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"OrderEntry")
					     .createAlias("OrderEntry.outWardDispatchEnteryRegistration", "outWardDispatchEnteryRegistration")
					     .createAlias("OrderEntry.organization", "organization")
						 .add(Restrictions.eq("invoiceStatus", 1))
						 .addOrder(Order.desc("submitDate"));
					if(organizationId!=null){
						criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					}
					switch(selectedValue){
					 		
					 	case "Company" : 
							 criteria.createAlias("OrderEntry.company", "company");
							 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
						case "Organization" :
							 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
							 
						case "Stockist" :
							  criteria.createAlias("OrderEntry.stockist", "stockist");
							  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
							  break;
							  
						case "OrderId" :
							 criteria.add(Restrictions.like("OrderEntry.orderId", searchText,MatchMode.START));
							 break;
							 
						case "OrderNo" :
							 criteria.add(Restrictions.like("OrderEntry.orderNo", searchText,MatchMode.START));  
							  break;
							  
					}
							  outWardOrderInvoiceEnteryRegistrationList = criteria.list();
			}
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderInvoiceEnteryRegistrationList;
	
	}*/

	@Override
	public Map<Object, Object> searchOutstandingTodaysDue(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		Map<Object, Object>map = null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				        .setProjection(Projections.rowCount())
				     .add(Restrictions.eq("lastDate", ourJavaDateObject))
				     .add(Restrictions.eq("paymentStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   switch(selectedValue)
					 {
					 	case "InvoiceNumber": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
							 break;
					 	case "NetAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "GrossAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "InvoiceDate":
					 		if(fromSpecialData!=""&&toSpecialData!=""){
							    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
							    java.util.Date d1= null;
								d1 = dtf.parse(fromSpecialData);
								java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
								d1 = dtf.parse(toSpecialData);
								java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
								
								criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
							  }
					 	/*case "DaysCount": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
							 break;	*/
					 		
					 	case "Company" : 
					 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
							 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
						case "Organization" :
							 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
							 
						case "Stockist" :
							  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
							  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
							  break;
							  
						case "OrderId" :
							 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
							 break;
							 
						case "OrderNo" :
						   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
							  break;
					 }
				   Long totalRecordCount = (Long)criteria.uniqueResult();

				   
				   
				    criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
						     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.eq("lastDate", ourJavaDateObject))
						     .add(Restrictions.eq("paymentStatus", 0))
						     .addOrder(Order.desc("submitDate"));
						   if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   switch(selectedValue)
							 {
							 	case "InvoiceNumber": 
									 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
									 break;
							 	case "NetAmount":
							 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
									  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
									  break;
							 	case "GrossAmount":
							 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(fromSpecialData)));
									  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(toSpecialData)));
									  break;
							 	case "InvoiceDate":
							 		if(fromSpecialData!=""&&toSpecialData!=""){
									    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
									    java.util.Date d1= null;
										d1 = dtf.parse(fromSpecialData);
										java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
										d1 = dtf.parse(toSpecialData);
										java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
										
										criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
									  }
							 	/*case "DaysCount": 
									 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
									 break;	*/
							 		
							 	case "Company" : 
							 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
									 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
									 break;
								case "Organization" :
									 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
									 break;
									 
								case "Stockist" :
									  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
									  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
									  break;
									  
								case "OrderId" :
									 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
									 break;
									 
								case "OrderNo" :
								   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
									  break;
							 }
						   from=from-1;
						   criteria.setFirstResult(from *Constant.PAGE_COUNT);
						   criteria.setMaxResults(Constant.PAGE_COUNT);
						   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
						   
						   Integer tatalPage = 0;
							if(totalRecordCount>0)
							{
								tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
								if((totalRecordCount%Constant.PAGE_COUNT)!=0)
								{
									tatalPage=tatalPage+1;
								}
							}
						
								map = new HashMap<Object, Object>();
								map.put("OutWardOrderInvoiceEntery", outWardOrderInvoiceEnteryRegistrationList);
								map.put("totalPages", tatalPage);
						   
						   
						   
						   
				   
				
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

	@Override
	public Map<Object, Object> searchOutstandingLapsed(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		Map<Object, Object>map = null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.lt("lastDate", ourJavaDateObject))
				     .add(Restrictions.eq("paymentStatus", 0))
				        .setProjection(Projections.rowCount())
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   switch(selectedValue)
					 {
					 	case "InvoiceNumber": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
							 break;
					 	case "NetAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "GrossAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "InvoiceDate":
					 		if(fromSpecialData!=""&&toSpecialData!=""){
							    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
							    java.util.Date d1= null;
								d1 = dtf.parse(fromSpecialData);
								java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
								d1 = dtf.parse(toSpecialData);
								java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
								
								criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
							  }
					 	/*case "DaysCount": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
							 break;	*/
					 		
					 	case "Company" : 
					 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
							 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
						case "Organization" :
							 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
							 
						case "Stockist" :
							  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
							  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
							  break;
							  
						case "OrderId" :
							 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
							 break;
							 
						case "OrderNo" :
						   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
							  break;
					 }
				   Long totalRecordCount = (Long)criteria.uniqueResult();
				   
				   
				   
				    criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
						     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.lt("lastDate", ourJavaDateObject))
						     .add(Restrictions.eq("paymentStatus", 0))
						     .addOrder(Order.desc("submitDate"));
						   if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   switch(selectedValue)
							 {
							 	case "InvoiceNumber": 
									 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
									 break;
							 	case "NetAmount":
							 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
									  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
									  break;
							 	case "GrossAmount":
							 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(fromSpecialData)));
									  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(toSpecialData)));
									  break;
							 	case "InvoiceDate":
							 		if(fromSpecialData!=""&&toSpecialData!=""){
									    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
									    java.util.Date d1= null;
										d1 = dtf.parse(fromSpecialData);
										java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
										d1 = dtf.parse(toSpecialData);
										java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
										
										criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
									  }
							 	/*case "DaysCount": 
									 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
									 break;	*/
							 		
							 	case "Company" : 
							 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
									 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
									 break;
								case "Organization" :
									 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
									 break;
									 
								case "Stockist" :
									  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
									  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
									  break;
									  
								case "OrderId" :
									 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
									 break;
									 
								case "OrderNo" :
								   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
									  break;
							 }
						   from=from-1;
						   criteria.setFirstResult(from *Constant.PAGE_COUNT);
						   criteria.setMaxResults(Constant.PAGE_COUNT);
						   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
						   
						   Integer tatalPage = 0;
							if(totalRecordCount>0)
							{
								tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
								if((totalRecordCount%Constant.PAGE_COUNT)!=0)
								{
									tatalPage=tatalPage+1;
								}
							}
						
								map = new HashMap<Object, Object>();
								map.put("laps", outWardOrderInvoiceEnteryRegistrationList);
								map.put("totalPages", tatalPage);
						   
						   
						   
				  
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	
	}

	@Override
	public Map<Object,Object> searchOutstandingUpcoming(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {

		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		Map<Object, Object>map = null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.gt("lastDate", ourJavaDateObject))
				     .add(Restrictions.eq("paymentStatus", 0))
				       .setProjection(Projections.rowCount())
				     .addOrder(Order.desc("submitDate"));				  
			         if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   switch(selectedValue)
					 {
					 	case "InvoiceNumber": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
							 break;
					 	case "NetAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "GrossAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "InvoiceDate":
					 		if(fromSpecialData!=""&&toSpecialData!=""){
							    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
							    java.util.Date d1= null;
								d1 = dtf.parse(fromSpecialData);
								java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
								d1 = dtf.parse(toSpecialData);
								java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
								
								criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
							  }
					 	/*case "DaysCount": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
							 break;	*/
					 		
					 	case "Company" : 
					 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
							 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
						case "Organization" :
							 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
							 
						case "Stockist" :
							  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
							  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
							  break;
							  
						case "OrderId" :
							 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
							 break;
							 
						case "OrderNo" :
						   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
							  break;
					 }
				  Long totalRecordCount = (Long)criteria.uniqueResult();
				   
				   
				    criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
						     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.gt("lastDate", ourJavaDateObject))
						     .add(Restrictions.eq("paymentStatus", 0));
					         if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   switch(selectedValue)
							 {
							 	case "InvoiceNumber": 
									 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
									 break;
							 	case "NetAmount":
							 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
									  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
									  break;
							 	case "GrossAmount":
							 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(fromSpecialData)));
									  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(toSpecialData)));
									  break;
							 	case "InvoiceDate":
							 		if(fromSpecialData!=""&&toSpecialData!=""){
									    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
									    java.util.Date d1= null;
										d1 = dtf.parse(fromSpecialData);
										java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
										d1 = dtf.parse(toSpecialData);
										java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
										
										criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
									  }
							 	/*case "DaysCount": 
									 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
									 break;	*/
							 		
							 	case "Company" : 
							 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
									 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
									 break;
								case "Organization" :
									 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
									 break;
									 
								case "Stockist" :
									  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
									  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
									  break;
									  
								case "OrderId" :
									 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
									 break;
									 
								case "OrderNo" :
								   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
									  break;
							 }
						   
						   from=from-1;
						   criteria.setFirstResult(from *Constant.PAGE_COUNT);
						   criteria.setMaxResults(Constant.PAGE_COUNT);
							 
						   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
						   
						   Integer tatalPage = 0;
							if(totalRecordCount>0)
							{
								 
								tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
								if((totalRecordCount%Constant.PAGE_COUNT)!=0)
								{
									tatalPage=tatalPage+1;
								}
							}
//							System.out.println("totalRecordCount : "+totalRecordCount);
//							System.out.println("tatalPage : "+tatalPage);
						
								map = new HashMap<Object, Object>();
								map.put("upcoming", outWardOrderInvoiceEnteryRegistrationList);
								map.put("totalPages", tatalPage);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	
	
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Object,Object> searchOutstandingPaidInvoice(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {


		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderInvoiceEnteryRegistration> outWardOrderInvoiceEnteryRegistrationList=null;
		Map<Object, Object>map = null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
				     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				      .setProjection(Projections.rowCount())
				      .add(Restrictions.neProperty("OutWardOrderInvoiceEnteryRegistration.remainingPayment", "OutWardOrderInvoiceEnteryRegistration.netAmount"))
				     .addOrder(Order.desc("submitDate"));
				     if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   switch(selectedValue)
					 {
					 	case "InvoiceNumber": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
							 break;
					 	case "NetAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "GrossAmount":
					 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(fromSpecialData)));
							  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(toSpecialData)));
							  break;
					 	case "InvoiceDate":
					 		if(fromSpecialData!=""&&toSpecialData!=""){
							    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
							    java.util.Date d1= null;
								d1 = dtf.parse(fromSpecialData);
								java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
								d1 = dtf.parse(toSpecialData);
								java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
								
								criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
							  }
					 	/*case "DaysCount": 
							 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
							 break;	*/
					 		
					 	case "Company" : 
					 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
							 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
						case "Organization" :
							 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
							 
						case "Stockist" :
							  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
							  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
							  break;
							  
						case "OrderId" :
							 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
							 break;
							 
						case "OrderNo" :
						   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
							 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
							  break;
					 }
				   Long totalRecordCount = (Long)criteria.uniqueResult();
				   
				   
				    criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"OutWardOrderInvoiceEnteryRegistration")
						     .createAlias("OutWardOrderInvoiceEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .addOrder(Order.desc("submitDate"));
						     if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   switch(selectedValue)
							 {
							 	case "InvoiceNumber": 
									 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.invoiceNo", searchText,MatchMode.START));
									 break;
							 	case "NetAmount":
							 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(fromSpecialData)));
									  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.netAmount", Double.parseDouble(toSpecialData)));
									  break;
							 	case "GrossAmount":
							 		  criteria.add(Restrictions.ge("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(fromSpecialData)));
									  criteria.add(Restrictions.le("OutWardOrderInvoiceEnteryRegistration.grossAmount", Double.parseDouble(toSpecialData)));
									  break;
							 	case "InvoiceDate":
							 		if(fromSpecialData!=""&&toSpecialData!=""){
									    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
									    java.util.Date d1= null;
										d1 = dtf.parse(fromSpecialData);
										java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
										d1 = dtf.parse(toSpecialData);
										java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
										
										criteria.add(Restrictions.between("OutWardOrderInvoiceEnteryRegistration.invoiceDate", sqlFromDate,sqlToDate));
									  }
							 	/*case "DaysCount": 
									 criteria.add(Restrictions.like("OutWardOrderInvoiceEnteryRegistration.noOfDays", searchText,MatchMode.START));
									 break;	*/
							 		
							 	case "Company" : 
							 		 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.createAlias("OutWardOrderEntryRegistration.company", "company");
									 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
									 break;
								case "Organization" :
									 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
									 break;
									 
								case "Stockist" :
									  criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									  criteria.createAlias("OutWardOrderEntryRegistration.stockist", "stockist");
									  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
									  break;
									  
								case "OrderId" :
									 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
									 break;
									 
								case "OrderNo" :
								   	 criteria.createAlias("OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration", "OutWardOrderEntryRegistration");
									 criteria.add(Restrictions.like("OutWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));  
									  break;
							 }
						   
						   from=from-1;
						   criteria.setFirstResult(from *Constant.PAGE_COUNT);
						   criteria.setMaxResults(Constant.PAGE_COUNT);
							 
						   outWardOrderInvoiceEnteryRegistrationList=criteria.list();
						   
						   Integer tatalPage = 0;
							if(totalRecordCount>0)
							{
								 
								tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
								if((totalRecordCount%Constant.PAGE_COUNT)!=0)
								{
									tatalPage=tatalPage+1;
								}
							}
//							System.out.println("totalRecordCount : "+totalRecordCount);
//							System.out.println("tatalPage : "+tatalPage);
						
								map = new HashMap<Object, Object>();
								map.put("paidInvoice", outWardOrderInvoiceEnteryRegistrationList);
								map.put("totalPages", tatalPage);
						   
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
}
