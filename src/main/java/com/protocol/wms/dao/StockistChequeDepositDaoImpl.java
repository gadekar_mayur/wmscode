/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.StockistChequeDeposit;
import com.protocol.wms.model.StockistChequeReturn;

/**
 * @author Sudhakar
 *
 */
@Repository(value="StockistChequeDepositDaoImpl")
public class StockistChequeDepositDaoImpl implements StockistChequeDepositDao{

	private Logger log = Logger.getLogger(StockistChequeDepositDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistChequeDepositDao#saveStockistChequeDepositObj(com.protocol.wms.model.StockistChequeDeposit)
	 */
	@Override
	public Integer saveStockistChequeDepositObj(
			StockistChequeDeposit StockistChequeDepositObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer StockistChequeDepositId = null;
		try 
		{
			StockistChequeDepositId=(Integer) session.save(StockistChequeDepositObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return StockistChequeDepositId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistChequeDepositDao#loadStockistChequeDepositListUsignOrgnizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<StockistChequeDeposit> loadStockistChequeDepositListUsignOrgnizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<StockistChequeDeposit> stockistChequeDepositList=null;
		try 
		{
			Criteria criteria = session.createCriteria(StockistChequeDeposit.class,"StockistChequeDeposit")
				     .createAlias("StockistChequeDeposit.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("returnStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   stockistChequeDepositList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stockistChequeDepositList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistChequeDepositDao#loadStockistChequeDepositObjUsignStockistChequeDepositId(java.lang.Integer)
	 */
	@Override
	public StockistChequeDeposit loadStockistChequeDepositObjUsignStockistChequeDepositId(
			Integer StockistChequeDepositId) {
		Session session =sessionFactory.getCurrentSession();
		StockistChequeDeposit stockistChequeDepositObj=null;
		try 
		{
			stockistChequeDepositObj=(StockistChequeDeposit)session.get(StockistChequeDeposit.class,StockistChequeDepositId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stockistChequeDepositObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistChequeDepositDao#updateStockistChequeDepositObj(com.protocol.wms.model.StockistChequeDeposit)
	 */
	@Override
	public void updateStockistChequeDepositObj(
			StockistChequeDeposit stockistChequeDepositObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(stockistChequeDepositObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistChequeDepositDao#reDepositReturnCheque(java.lang.String[], java.lang.String[])
	 */
	@Override
	public boolean reDepositReturnCheque(String[] stockistReturnChequeIdArray,
			String[] stockistDepositChequeIdArray) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			for(int i=0;i<stockistDepositChequeIdArray.length;i++)
			{
				// load stockist Deposit Cheque object
				StockistChequeDeposit stockistChequeDepositObj=(StockistChequeDeposit) session.get(StockistChequeDeposit.class, Integer.parseInt(stockistDepositChequeIdArray[i]));
				stockistChequeDepositObj.setReturnStatus(0);
				//update stockist Deposit Cheque object
				session.update(stockistChequeDepositObj);
				//delete stockist Return Cheque object
				StockistChequeReturn stockistChequeReturnobj=(StockistChequeReturn) session.get(StockistChequeReturn.class, Integer.parseInt(stockistReturnChequeIdArray[i]));
				session.delete(stockistChequeReturnobj);
			}
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Object, Object> searchStockistDepositedChequeEntries(String searchOption, String searchText, String fromSpecialData, String toSpecialData, Integer organizationId, Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<StockistChequeDeposit> stockistChequeDepositList = null;
		Map<Object, Object>map = null;
		try{
			 Criteria criteria = session.createCriteria(StockistChequeDeposit.class,"StockistChequeDeposit")
				     .createAlias("StockistChequeDeposit.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				      .setProjection(Projections.rowCount())
				     .add(Restrictions.eq("returnStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			 switch(searchOption){
				case "Company" : 
					criteria.createAlias("StockistChequeDeposit.company", "company")
					 .add(Restrictions.like("company.companyName", searchText,MatchMode.START));
					 break;
				case "Organization" :
					 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					 break;
				case "CompanyBank" :
					 criteria.createAlias("StockistChequeDeposit.companyBank", "companybank")
					 .add(Restrictions.like("companybank.companyBankName", searchText,MatchMode.START));
					 break;
				case "Stockist" :
					 criteria.createAlias("StockistChequeDeposit.stockist", "stockist")
					 .add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
					 break;
				case "StockistBank" :
					 criteria.createAlias("StockistChequeDeposit.stockistBankDetails", "stockistbankdetails")
					 .add(Restrictions.like("stockistbankdetails.StockistBankName", searchText,MatchMode.START));
					 break;
				case "ChequeNo" :
					criteria.createAlias("StockistChequeDeposit.chequeNumberInfo", "chequeNumberInfo")
					.add(Restrictions.like("chequeNumberInfo.chequeNumber", searchText,MatchMode.START));
					 break;
				case "ChequeAmount" :
//					  criteria.add(Restrictions.ge("StockistChequeDeposit.chequeAmount", Double.parseDouble(fromSpecialData)));
//					  criteria.add(Restrictions.le("StockistChequeDeposit.chequeAmount", Double.parseDouble(toSpecialData)));
					criteria.add(Restrictions.between("StockistChequeDeposit.chequeAmount",  Double.parseDouble(fromSpecialData), Double.parseDouble(toSpecialData)));  
					break;
			 }
			  Long totalRecordCount = (Long)criteria.uniqueResult();
			 
			 
			  criteria = session.createCriteria(StockistChequeDeposit.class,"StockistChequeDeposit")
				     .createAlias("StockistChequeDeposit.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("returnStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			 switch(searchOption){
				case "Company" : 
					criteria.createAlias("StockistChequeDeposit.company", "company")
					 .add(Restrictions.like("company.companyName", searchText,MatchMode.START));
					 break;
				case "Organization" :
					 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					 break;
				case "CompanyBank" :
					 criteria.createAlias("StockistChequeDeposit.companyBank", "companybank")
					 .add(Restrictions.like("companybank.companyBankName", searchText,MatchMode.START));
					 break;
				case "Stockist" :
					 criteria.createAlias("StockistChequeDeposit.stockist", "stockist")
					 .add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
					 break;
				case "StockistBank" :
					 criteria.createAlias("StockistChequeDeposit.stockistBankDetails", "stockistbankdetails")
					 .add(Restrictions.like("stockistbankdetails.StockistBankName", searchText,MatchMode.START));
					 break;
				case "ChequeNo" :
					criteria.createAlias("StockistChequeDeposit.chequeNumberInfo", "chequeNumberInfo")
					.add(Restrictions.like("chequeNumberInfo.chequeNumber", searchText,MatchMode.START));
					 break;
				case "ChequeAmount" :
//					  criteria.add(Restrictions.ge("StockistChequeDeposit.chequeAmount", Double.parseDouble(fromSpecialData)));
//					  criteria.add(Restrictions.le("StockistChequeDeposit.chequeAmount", Double.parseDouble(toSpecialData)));
					criteria.add(Restrictions.between("StockistChequeDeposit.chequeAmount",  Double.parseDouble(fromSpecialData), Double.parseDouble(toSpecialData)));  
					break;
			 }
			 
			 
			   from=from-1;
			   criteria.setFirstResult(from *Constant.PAGE_COUNT);
			   criteria.setMaxResults(Constant.PAGE_COUNT);
			   stockistChequeDepositList=criteria.list();
			   
			   Integer tatalPage = 0;
				if(totalRecordCount>0)
				{
					tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0)
					{
						tatalPage=tatalPage+1;
					}
				}
			
					map = new HashMap<Object, Object>();
					map.put("empList", stockistChequeDepositList);
					map.put("totalPages", tatalPage);
			 
			 
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
}
