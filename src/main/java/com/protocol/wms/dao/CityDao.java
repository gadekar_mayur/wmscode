package com.protocol.wms.dao;

import java.util.List;

import org.json.JSONObject;

import com.protocol.wms.model.City;

public interface CityDao {
	
	public JSONObject getCityList(Integer districtId);
	
	public City loadCityObjectUsingCityId(Integer cityId);
	
	public JSONObject saveCity(Integer districtId,String cityName);
	
	public Boolean deleteCityDetails(Integer cityId);

	public boolean updateCityObject(City cityObj);

	public boolean cityAlreadyExistsOrNot(Integer districtId, String cityName);

	public List<City> searchOptionForCity(String searchOption,
			String searchText, Integer organizationId);

	public String getCityName(Integer cityId);
}
