package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.OutWardChequeNumberInformation;

public interface OutWardChequeNumberInformationDao {

public List<OutWardChequeNumberInformation> loadChequeInformationUsigngetOutWardAdvancedChequeInformationId(
		Integer outWardAdvancedChequeInformationId);
}
