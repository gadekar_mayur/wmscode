/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.OrderMode;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OrderModeDaoImpl")
public class OrderModeDaoImpl implements OrderModeDao{

	private Logger log = Logger.getLogger(OrderModeDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrderModeDao#loadAllOrderMode()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OrderMode> loadAllOrderMode() {
		Session session =sessionFactory.getCurrentSession();
		List<OrderMode> OrderModeList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OrderMode.class,"order")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.asc("order.orderModeName"));
				     OrderModeList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return OrderModeList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrderModeDao#loadOrderModeObjectUsignOrderModeId(java.lang.Integer)
	 */
	@Override
	public OrderMode loadOrderModeObjectUsignOrderModeId(Integer orderModeId) {
		Session session =sessionFactory.getCurrentSession();
		OrderMode OrderModeObj=null;
		try 
		{
			OrderModeObj=(OrderMode)session.get(OrderMode.class,orderModeId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return OrderModeObj;
	}
}
