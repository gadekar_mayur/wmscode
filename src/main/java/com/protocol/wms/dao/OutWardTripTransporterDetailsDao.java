/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.OutWardTripTransporterDetails;

/**
 * @author Sudhakar
 *
 */
public interface OutWardTripTransporterDetailsDao {

	public Integer saveOutWardTripTransporterDetailsObj(OutWardTripTransporterDetails outWardTripTransporterDetailsObj);
	
	public List<OutWardTripTransporterDetails> loadOutWardTripTransporterDetailsListUsignOutWardTripId(Integer outWardTripId);
	
}
