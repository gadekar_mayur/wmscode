package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.protocol.wms.model.TransporterDetails;

public interface TransporterDetailsDao {

	public JSONObject getTransporterList(Integer orgId);
	
	public Integer saveTransporterDetails(TransporterDetails TranObj);
	
	public TransporterDetails loadTransporterDetailsUsingTransporterDetailsId(Integer TransporterDetailsId);
	
	public void updateTransporterDetails(TransporterDetails TransporterDetailsObj);
	
	public List<TransporterDetails> loadTransporterDetailsListUsingOrganizationId(Integer organizationId);
	

	public Map<String,Object> searchTransporterDetails(String searchOption,String searchText,Integer from,Integer organizationId);
	
	public Boolean deleteTransporterDetails(Integer transporterDetailsId);
	
	public Boolean deleteStationPersonContactDetails(Integer stationPersonContactDetailsId);
	
	public Boolean deleteTransporterDocumentDetails(Integer transporterDocumentId);
	
	public Integer insertNewTransporterAndDeleteOldTransporter(Integer transporterDetailsId, Integer stateId, Integer districtId, Integer cityId, TransporterDetails transporterDetailsObj);

	public Map<String, Object> searchTransporterStockistDetails(Integer orgId, Integer companyId, Integer transporterId);

	public JSONObject getTransporterDetails(Integer orgId, Integer companyId);

	public Integer saveTranfereSelectedStockist(Integer[] stockistId, Integer[] transporterStationArrayId,
			Integer fromTransporterId, Integer toTransporterId);

	public Map<String, Object> loadPendingStockist(Integer rateId);

	public Map<String, Object> loadAdminApprovalStatus(Integer rateId);

	public void saveApprovedData(String[] toTransporterArray, String[] stockistApprovalArray,
			String[] transporterStockistTransferArray);

	public void saveRejectedData(String[] transporterStockistTransferArray);
}
