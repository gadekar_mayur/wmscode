/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.StockistChequeDeposit;

/**
 * @author Sudhakar
 *
 */
public interface StockistChequeDepositDao {
	
	public Integer saveStockistChequeDepositObj(StockistChequeDeposit StockistChequeDepositObj);
	
	public List<StockistChequeDeposit> loadStockistChequeDepositListUsignOrgnizationId(Integer organizationId);
	
	public StockistChequeDeposit loadStockistChequeDepositObjUsignStockistChequeDepositId(Integer StockistChequeDepositId);
	
	public void updateStockistChequeDepositObj(StockistChequeDeposit stockistChequeDepositObj);
	
	public boolean reDepositReturnCheque(String[] stockistReturnChequeIdArray,String[] stockistDepositChequeIdArray);

	public Map<Object, Object> searchStockistDepositedChequeEntries(String searchOption, String searchText, String fromSpecialData, String toSpecialData, Integer organizationId, Integer from);

}
