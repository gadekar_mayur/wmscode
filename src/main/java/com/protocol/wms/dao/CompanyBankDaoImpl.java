/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.CompanyBank;

/**
 * @author Sudhakar
 *
 */
@Repository(value="CompanyBankDaoImpl")
public class CompanyBankDaoImpl implements CompanyBankDao{

	private Logger log = Logger.getLogger(CompanyBankDaoImpl.class.getClass());

	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyBankDao#companyBankDropdown(java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyBank> companyBankDropdown(Integer companyId,
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyBank> companyBankList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyBank.class,"companyBank")
					.createAlias("companyBank.organization", "organization")
					.createAlias("companyBank.company", "company")
					.add(Restrictions.eq("companyBank.status",1))
					.add(Restrictions.eq("organization.organizationId",organizationId))
					.add(Restrictions.eq("company.companyId",companyId))
					.addOrder(Order.desc("submitDate"));
			companyBankList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		
		return companyBankList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyBankDao#loadCompanyBankUsignCompanyBankId(java.lang.Integer)
	 */
	@Override
	public CompanyBank loadCompanyBankUsignCompanyBankId(Integer companyBankId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyBank companyBankObj=null;
		try 
		{
			companyBankObj=(CompanyBank)session.get(CompanyBank.class,companyBankId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return companyBankObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyBankDao#updateCompanyBankDetails(com.protocol.wms.model.CompanyBank)
	 */
	@Override
	public Boolean updateCompanyBankDetails(CompanyBank companyBank) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(companyBank);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
}
