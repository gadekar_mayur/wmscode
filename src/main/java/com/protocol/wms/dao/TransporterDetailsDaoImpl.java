package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.City;
import com.protocol.wms.model.District;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.State;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistApprovalPending;
import com.protocol.wms.model.TransporterDetails;
import com.protocol.wms.model.TransporterDocument;
import com.protocol.wms.model.TransporterStationDetails;
import com.protocol.wms.model.TransporterStockistDetails;
import com.protocol.wms.model.TransporterStockistTransfer;
import com.protocol.wms.model.UserDetails;

@Repository(value="TransporterDetailsDaoImpl")
public class TransporterDetailsDaoImpl implements TransporterDetailsDao {
	private Logger log = Logger.getLogger(TransporterDetailsDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getTransporterList(Integer orgId) {
		Session session =sessionFactory.getCurrentSession();
		List<TransporterDetails> transporterList = null;
		JSONObject transporterJson = null;
		JSONArray  transporterArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(TransporterDetails.class,"trans")
					.createAlias("trans.organization", "org")
					.add(Restrictions.eq("org.organizationId", orgId))
					.addOrder(Order.asc("trans.transporterName"))
					.add(Restrictions.eq("status", 1));
			transporterList = criteria.list();
			Iterator<TransporterDetails> transporterIt = transporterList.iterator();
			while(transporterIt.hasNext()){
				TransporterDetails temp = transporterIt.next();
			System.out.println("transporter list transporterDetailsDao 75"+temp.getTransporterName()+"id"+temp.getTransporterDetailsId());
				transporterJson = new JSONObject();
				transporterJson.put("transId", temp.getTransporterDetailsId());
				transporterJson.put("transName", temp.getTransporterName());
				transporterArray.put(transporterJson);
			}
			transporterJson = new JSONObject();
			transporterJson.put("transporterArray",transporterArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return transporterJson;
	}



	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDetailsDao#saveTransporterDetails(com.protocol.wms.model.TransporterDetails)
	 */
	@Override
	public Integer saveTransporterDetails(TransporterDetails TranObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer transporterDetailsId = null;
		try 
		{
			transporterDetailsId=(Integer) session.save(TranObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return transporterDetailsId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDetailsDao#loadTransporterDetailsUsingTransporterDetailsId(java.lang.Integer)
	 */
	@Override
	public TransporterDetails loadTransporterDetailsUsingTransporterDetailsId(
			Integer TransporterDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		TransporterDetails transporterDetailsObj=null;
		try 
		{
			transporterDetailsObj=(TransporterDetails) session.get(TransporterDetails.class, TransporterDetailsId);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return transporterDetailsObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDetailsDao#updateTransporterDetails(com.protocol.wms.model.TransporterDetails)
	 */
	@Override
	public void updateTransporterDetails(TransporterDetails TransporterDetailsObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(TransporterDetailsObj);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDetailsDao#loadTransporterDetailsListUsingOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TransporterDetails> loadTransporterDetailsListUsingOrganizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<TransporterDetails> transporterDetailsList=null;
		try 
		{
			Criteria criteria = session.createCriteria(TransporterDetails.class,"TransporterDetails")
				     .createAlias("TransporterDetails.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   transporterDetailsList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return transporterDetailsList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDetailsDao#searchTransporterDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	public Map<String,Object> searchTransporterDetails(
			String searchOption, String searchText,Integer from, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<TransporterDetails> transporterDetailsList=null;
		Map<String,Object>map=null;
		
		try 
		{
			Criteria criteria = session.createCriteria(TransporterDetails.class,"TransporterDetails")
				     .createAlias("TransporterDetails.organization", "organization")
				     .createAlias("TransporterDetails.city", "city")
				     .createAlias("TransporterDetails.state", "state")
				     .createAlias("TransporterDetails.district", "district")
				     .add(Restrictions.eq("status", 1))
				     .setProjection(Projections.rowCount());
			
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   if("Organization".equals(searchOption))
					   criteria.add(Restrictions.like("organization.organizationName", searchText, MatchMode.START));
				   else if("TransporterName".equals(searchOption))
					   criteria.add(Restrictions.like("TransporterDetails.transporterName", searchText, MatchMode.START));
				   else if("Address".equals(searchOption))
					   criteria.add(Restrictions.like("TransporterDetails.transporterAddress", searchText, MatchMode.ANYWHERE));
				   else if("City".equals(searchOption))
					   criteria.add(Restrictions.like("city.cityName", searchText, MatchMode.START));
				   else if("State".equals(searchOption))
					   criteria.add(Restrictions.like("state.stateName", searchText, MatchMode.START));
				   else if("District".equals(searchOption))
					   criteria.add(Restrictions.like("district.districtName", searchText, MatchMode.START));
			
				   Long totalRecordCount = (Long)criteria.uniqueResult();
			
			
			criteria = session.createCriteria(TransporterDetails.class,"TransporterDetails")
				     .createAlias("TransporterDetails.organization", "organization")
				     .createAlias("TransporterDetails.city", "city")
				     .createAlias("TransporterDetails.state", "state")
				     .createAlias("TransporterDetails.district", "district")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   if("Organization".equals(searchOption))
					   criteria.add(Restrictions.like("organization.organizationName", searchText, MatchMode.START));
				   else if("TransporterName".equals(searchOption))
					   criteria.add(Restrictions.like("TransporterDetails.transporterName", searchText, MatchMode.START));
				   else if("Address".equals(searchOption))
					   criteria.add(Restrictions.like("TransporterDetails.transporterAddress", searchText, MatchMode.ANYWHERE));
				   else if("City".equals(searchOption))
					   criteria.add(Restrictions.like("city.cityName", searchText, MatchMode.START));
				   else if("State".equals(searchOption))
					   criteria.add(Restrictions.like("state.stateName", searchText, MatchMode.START));
				   else if("District".equals(searchOption))
					   criteria.add(Restrictions.like("district.districtName", searchText, MatchMode.START));
				   
				   from = from - 1 ;
				    criteria.setFirstResult(from*Constant.PAGE_COUNT);
				    criteria.setMaxResults(Constant.PAGE_COUNT);
				transporterDetailsList=criteria.list();
				
				Integer totalPage = 0;
				if(totalRecordCount>0){
					totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPage = totalPage +1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("transporterDetails", transporterDetailsList);
				map.put("totalPages", totalPage );
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
	
	
	

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDetailsDao#deleteTransporterDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteTransporterDetails(Integer transporterDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		TransporterDetails transporterDetails = null;
		try{
			transporterDetails = (TransporterDetails)session.get(TransporterDetails.class, transporterDetailsId);
			transporterDetails.setStatus(0);
			session.update(transporterDetails);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDetailsDao#deleteStationPersonContactDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteStationPersonContactDetails(Integer stationPersonContactDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		TransporterStationDetails transporterStationDetails = null;
		try{
			transporterStationDetails = (TransporterStationDetails)session.get(TransporterStationDetails.class, stationPersonContactDetailsId);
			transporterStationDetails.setStatus(0);
			session.update(transporterStationDetails);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDetailsDao#deleteTransporterDocumentDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteTransporterDocumentDetails(Integer transporterDocumentId) {
		Session session =sessionFactory.getCurrentSession();
		TransporterDocument transporterDocument = null;
		try{
			transporterDocument = (TransporterDocument)session.get(TransporterDocument.class, transporterDocumentId);
			transporterDocument.setStatus(0);
			session.update(transporterDocument);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.TransporterDetailsDao#insertNewTransporterAndDeleteOldTransporter(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.TransporterDetails)
	 */
	@Override
	public Integer insertNewTransporterAndDeleteOldTransporter(Integer transporterDetailsId, Integer stateId, Integer districtId,Integer cityId, TransporterDetails transporterDetailsObj) {
		Session session =sessionFactory.getCurrentSession();
		City city = null;
		District district = null;
		State state = null;
		Integer id = null;
		TransporterDetails oldTransporterObj = null;
		try
		{
			state = (State) session.load(State.class, stateId);
			district = (District) session.load(District.class, districtId);
			city = (City) session.load(City.class, cityId);
			transporterDetailsObj.setState(state);
			transporterDetailsObj.setDistrict(district);
			transporterDetailsObj.setCity(city);
			UserDetails userDetails = transporterDetailsObj.getUserDetails();
			session.save(userDetails);
			transporterDetailsObj.setUserDetails(userDetails);
			id = (Integer) session.save(transporterDetailsObj);
			if(id!=null&&id>0)
			{
				oldTransporterObj = (TransporterDetails) session.load(TransporterDetails.class, transporterDetailsId);
				oldTransporterObj.getUserDetails().setUserName(UUID.randomUUID().toString());
				oldTransporterObj.setStatus(0);
				session.update(oldTransporterObj);
				return id;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return id;
	}

	@Override
	public Map<String, Object> searchTransporterStockistDetails(Integer orgId, Integer companyId, Integer transporterId) 
	{
	Session session=sessionFactory.getCurrentSession();
	TransporterDetails transporterDetails=null;
	TransporterStationDetails transporterstationdetails=null;
	TransporterStockistTransfer transporterStockistTransfer=null;
	Stockist stockist=null;
	List<Stockist> stockist_list=null;
	List<TransporterStockistDetails>transporterstockistdetails=null;
	Map<String,Object>map=null;
	Long count;
	try{
		transporterstockistdetails=new ArrayList<TransporterStockistDetails>();
	Criteria criteria=session.createCriteria(Stockist.class,"stockist")
	.createAlias("stockist.transporterDetails","transporterdetails")
	.createAlias("stockist.companyList","company",Criteria.INNER_JOIN)
	.add(Restrictions.eq("transporterdetails.transporterDetailsId",transporterId))
	.add(Restrictions.eq("company.companyId",companyId))
	.add(Restrictions.eq("status",1))
	.addOrder(Order.asc("stockistName"));
	stockist_list=criteria.list();
	System.out.println(stockist_list.size());
	for(Stockist transporter_stockist:stockist_list)
	{
		TransporterStockistDetails transporterstockistdetailsObj= new TransporterStockistDetails();
		
		transporterstockistdetailsObj.setStockistId(transporter_stockist.getStockistId());
		
		transporterstockistdetailsObj.setStockistName(transporter_stockist.getStockistName());
	 transporterStockistTransfer=(TransporterStockistTransfer) session.get(TransporterStockistTransfer.class,transporter_stockist.getStockistId());
	Criteria stockistcheck=session.createCriteria(TransporterStockistTransfer.class,"transporter")
			.createAlias("transporter.stockist","stockist_details")
			.add(Restrictions.eq("stockist_details.stockistId", transporter_stockist.getStockistId()))
			.add(Restrictions.eq("adminApprovalStatus",0))
			.setProjection(Projections.rowCount());
		count=(Long) stockistcheck.uniqueResult();	
	if(count>0)
	 transporterstockistdetailsObj.setStockistApprovalStatus(0);
	else
		transporterstockistdetailsObj.setStockistApprovalStatus(1);
	 transporterstockistdetailsObj.setCity(transporter_stockist.getCity().getCityName());
	//	transporterstockistdetailsObj.setCompany(transporter_stockist.getCompany().getCompanyName());
		System.out.println("Stockist Name-"+transporter_stockist.getStockistName());
	Criteria transporter_Rates=session.createCriteria(TransporterStationDetails.class,"TransporterStationsDetails")
			.createAlias("TransporterStationsDetails.transporterDetails","transporterdetails")
			.add(Restrictions.eq("transporterdetails.transporterDetailsId",transporterId));
	transporterstationdetails=(TransporterStationDetails) transporter_Rates.uniqueResult();
	
	transporterstockistdetailsObj.setTransporterDetailsId(transporterId);
	transporterstockistdetailsObj.setTransporterStationDetailsId(transporterstationdetails.getTransporterStationDetailsId());
	
	
	transporterstockistdetailsObj.setClaimRates(transporterstationdetails.getClaimRates().getClaimRates());
	
	
	transporterstockistdetailsObj.setClaimRatePerLR(transporterstationdetails.getClaimRates().getRatePerLR());
	
	transporterstockistdetailsObj.setRatePerCase(transporterstationdetails.getRatePerStation().getRatePerCase());
	
	
	transporterstockistdetailsObj.setRatePerLR(transporterstationdetails.getRatePerStation().getRatePerLR());
	System.out.println("claim rate"+transporterstationdetails.getClaimRates().getClaimRates()+"claim per lr"+transporterstationdetails.getClaimRates().getRatePerLR());
	System.out.println("Actula  rate"+transporterstationdetails.getRatePerStation().getRatePerCase()+"Actual per lr"+transporterstationdetails.getRatePerStation().getRatePerLR());
	
	transporterstockistdetails.add(transporterstockistdetailsObj);
	System.out.println("transporterstockistdetails"+transporterstockistdetails.size());
	}
	
	map = new HashMap<String, Object>();
	map.put("transporterStockistDetails", transporterstockistdetails);
	
	}
	catch(Exception e){
		e.printStackTrace();
	}
			return map;
	}

	@Override
	public JSONObject getTransporterDetails(Integer orgId, Integer companyId) {
		Session session=sessionFactory.getCurrentSession();
		List<TransporterDetails> transporterDetails;
		TransporterDetails transporterDetailsObj;
		JSONObject transporter_json_object=null;
		JSONArray transporter_json_array=new JSONArray();
		try
		{
			transporter_json_object=new JSONObject();
		Organization organization=(Organization) session.get(Organization.class, orgId);
		transporterDetails=organization.getTransporterDetailsList();
		Iterator<TransporterDetails>transporterDetailsIt=transporterDetails.iterator();
		while(transporterDetailsIt.hasNext())
		{
			TransporterDetails temp=transporterDetailsIt.next();
			System.out.println("Transporter list 477 transporterDetailsDaoImpl"+temp.getTransporterName());
			if(temp.getStatus()==1)
			{  
				transporter_json_object=new JSONObject();
				transporter_json_object.put("transporterId",temp.getTransporterDetailsId());
				transporter_json_object.put("transporterName",temp.getTransporterName());
				transporter_json_array.put(transporter_json_object);
			}
		
	}
		transporter_json_object=new JSONObject();
		transporter_json_object.put("transporterArray",transporter_json_array);
		
}
		catch(Exception e){
			e.printStackTrace();
		}
		return transporter_json_object;
}

	@Override
	public Integer saveTranfereSelectedStockist(Integer[] stockistId, Integer[] transporterStationArrayId,
			Integer fromTransporterId, Integer toTransporterId) {
		Session session=sessionFactory.getCurrentSession();
		
		TransporterStockistTransfer	transporterStockistTransfer=null;
		Integer transporterStockistTransferId[]=null;
		Stockist stockist=null;
		TransporterStationDetails transporterStationDetails=null;
		
		try{
			
		for(int i=0;i<stockistId.length;i++)
		{
			
			
			transporterStockistTransfer= new TransporterStockistTransfer() ;
			transporterStockistTransfer.setTransferSubmitDate(CommonMethods.setSubmitedDate());
         	transporterStockistTransfer.setAdminApprovalStatus(0);
			transporterStockistTransfer.setFromTransporterDetailsId(fromTransporterId);
			 stockist=(Stockist) session.load(Stockist.class,stockistId[i]);
			transporterStockistTransfer.setStockist(stockist);
			transporterStockistTransfer.setToTransporterDetailsId(toTransporterId);
    		transporterStationDetails=(TransporterStationDetails) session.load(TransporterStationDetails.class,transporterStationArrayId[i]);
    		transporterStockistTransfer.setTransporterStationDetails(transporterStationDetails);
    		
    		System.out.println("StockistName-"+stockist.getStockistName()+"transporterStationDetails"+transporterStationDetails.getTransporterStationName());
			//System.out.println("Stockist name-"+stockist.getStockistId()+" "+transporterStationDetails.getTransporterStationDetailsId());
    		 session.save(transporterStockistTransfer);	
			
    		 System.out.println(transporterStockistTransfer.getTransporterStockistTransferId());
		}
		
		}
		catch(Exception e){
			
		}
		finally
		{
			
		}
		
		return 1;
		
		
		
	}

	@Override
	public Map<String, Object> loadPendingStockist(Integer rateId) {
		Session session=sessionFactory.getCurrentSession();
		StockistApprovalPending stockistApprovalPending;
		List<TransporterStockistTransfer>transporterStockistTransferList;
		List<TransporterStationDetails>transporterStationDetailsList=null;
		List<StockistApprovalPending>stockistApprovalPendingList=new ArrayList<StockistApprovalPending>();
		Map<String,Object>map=null;
		System.out.println("in loadPendingStockist 531");
		try
		{
		Criteria criteria=session.createCriteria(TransporterStockistTransfer.class)
				.add(Restrictions.eq("adminApprovalStatus",0));
		transporterStockistTransferList=criteria.list();
		for(TransporterStockistTransfer transporterStockistTransfer:transporterStockistTransferList)
		{
			System.out.println("Stockist ID-"+transporterStockistTransfer.getStockist().getStockistId());
			stockistApprovalPending = new StockistApprovalPending();
			Stockist stockist=(Stockist) session.get(Stockist.class,transporterStockistTransfer.getStockist().getStockistId());
			stockistApprovalPending.setStockistId(stockist.getStockistId());
			stockistApprovalPending.setStockistName(stockist.getStockistName());
			stockistApprovalPending.setTransporterStockistTransfer(transporterStockistTransfer.getTransporterStockistTransferId());
			TransporterDetails transporterDetails=(TransporterDetails) session.get(TransporterDetails.class,transporterStockistTransfer.getFromTransporterDetailsId());
			stockistApprovalPending.setFromTransporterId(transporterDetails.getTransporterDetailsId());
			stockistApprovalPending.setFromTransporterName(transporterDetails.getTransporterName());
			stockistApprovalPending.setStatus(transporterStockistTransfer.getAdminApprovalStatus());
			TransporterDetails toTransporterDetails=(TransporterDetails) session.get(TransporterDetails.class,transporterStockistTransfer.getToTransporterDetailsId());
			stockistApprovalPending.setToTransporterId(toTransporterDetails.getTransporterDetailsId());
			stockistApprovalPending.setToTransporterName(toTransporterDetails.getTransporterName());
			
			TransporterStationDetails transporterStationDetails=(TransporterStationDetails) session.get(TransporterStationDetails.class, transporterStockistTransfer.getTransporterStationDetails().getTransporterStationDetailsId());
			stockistApprovalPending.setTransporterStationName(transporterStationDetails.getTransporterStationName());
			stockistApprovalPending.setTransporterStationId(transporterStationDetails.getTransporterStationDetailsId());
			if(rateId==1)
			{
				stockistApprovalPending.setActualRate(transporterStationDetails.getRatePerStation().getRatePerCase());
			stockistApprovalPending.setActualLR(transporterStationDetails.getRatePerStation().getRatePerLR());
			}
			if(rateId==2)
			{
				stockistApprovalPending.setActualRate(transporterStationDetails.getClaimRates().getClaimRates());
			stockistApprovalPending.setActualLR(transporterStationDetails.getClaimRates().getRatePerLR());
			}
			System.out.println("to transporter ID-"+transporterStockistTransfer.getToTransporterDetailsId()+"TransporterStationName-"+transporterStationDetails.getTransporterStationName());
			
			Criteria criteriaTransporter=session.createCriteria(TransporterStationDetails.class,"transporterStationDetails")
					.createAlias("transporterStationDetails.transporterDetails","transporter")
					.add(Restrictions.eq("transporter.transporterDetailsId",transporterStockistTransfer.getToTransporterDetailsId()))
					.add(Restrictions.ilike("transporterStationName",transporterStationDetails.getTransporterStationName(),MatchMode.ANYWHERE))
					.add(Restrictions.eq("status",1));
			TransporterStationDetails transporterStationDetailsObj=(TransporterStationDetails) criteriaTransporter.uniqueResult();
		if(rateId==1)
		{
			stockistApprovalPending.setToActualRate(transporterStationDetailsObj.getRatePerStation().getRatePerCase());
		stockistApprovalPending.setToActualLR(transporterStationDetailsObj.getRatePerStation().getRatePerLR());
		}
		if(rateId==2)
		{
			stockistApprovalPending.setToActualRate(transporterStationDetailsObj.getClaimRates().getClaimRates());
		stockistApprovalPending.setToActualLR(transporterStationDetailsObj.getClaimRates().getRatePerLR());
		}
		if(rateId==1)
		{
			stockistApprovalPending.setDifference(stockistApprovalPending.getActualRate()-stockistApprovalPending.getToActualRate());
		stockistApprovalPending.setLrDifference(stockistApprovalPending.getActualLR()-stockistApprovalPending.getToActualLR());
		}
		if(rateId==2)
		{
			stockistApprovalPending.setDifference(stockistApprovalPending.getActualRate()-stockistApprovalPending.getToActualRate());
		stockistApprovalPending.setLrDifference(stockistApprovalPending.getActualLR()-stockistApprovalPending.getToActualLR());
		}
		
		System.out.println("difference-"+stockistApprovalPending.getDifference());
		stockistApprovalPendingList.add(stockistApprovalPending);
		System.out.println("In loadPendingStockist dao 592");
		}
		map = new HashMap<String, Object>();
		map.put("stockistApproval", stockistApprovalPendingList);	
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return map;
	}

	@Override
	public Map<String, Object> loadAdminApprovalStatus(Integer rateId) {
		Session session=sessionFactory.getCurrentSession();
		 DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	        Calendar cal = Calendar.getInstance();
	        Calendar calReturn = Calendar.getInstance();
	        
	        calReturn.add(Calendar.DATE,-30);
	       
		StockistApprovalPending stockistApprovalPending;
		List<TransporterStockistTransfer>transporterStockistTransferList;
		List<TransporterStationDetails>transporterStationDetailsList=null;
		List<StockistApprovalPending>stockistApprovalPendingList=new ArrayList<StockistApprovalPending>();
		Map<String,Object>map=null;
		System.out.println("in loadAdminApprovalStatus 642"+dateFormat.format(cal.getTime())+dateFormat.format(calReturn.getTime()));
		try
		{
			 java.util.Date d1=dateFormat.parse(dateFormat.format(cal.getTime()));
			 java.util.Date d2=dateFormat.parse(dateFormat.format(calReturn.getTime()));
		Criteria criteria=session.createCriteria(TransporterStockistTransfer.class)
				.add(Restrictions.between("transferSubmitDate",new java.sql.Date(d2.getTime()),new java.sql.Date(d1.getTime())))
				.addOrder(Order.desc("transferSubmitDate"));
		transporterStockistTransferList=criteria.list();
		for(TransporterStockistTransfer transporterStockistTransfer:transporterStockistTransferList)
		{
			System.out.println("Stockist ID-"+transporterStockistTransfer.getStockist().getStockistId());
			stockistApprovalPending = new StockistApprovalPending();
			Stockist stockist=(Stockist) session.get(Stockist.class,transporterStockistTransfer.getStockist().getStockistId());
			stockistApprovalPending.setStockistId(stockist.getStockistId());
			stockistApprovalPending.setStockistName(stockist.getStockistName());
			stockistApprovalPending.setTransporterStockistTransfer(transporterStockistTransfer.getTransporterStockistTransferId());
			TransporterDetails transporterDetails=(TransporterDetails) session.get(TransporterDetails.class,transporterStockistTransfer.getFromTransporterDetailsId());
			stockistApprovalPending.setFromTransporterId(transporterDetails.getTransporterDetailsId());
			stockistApprovalPending.setFromTransporterName(transporterDetails.getTransporterName());
			stockistApprovalPending.setStatus(transporterStockistTransfer.getAdminApprovalStatus());
			TransporterDetails toTransporterDetails=(TransporterDetails) session.get(TransporterDetails.class,transporterStockistTransfer.getToTransporterDetailsId());
			stockistApprovalPending.setToTransporterId(toTransporterDetails.getTransporterDetailsId());
			stockistApprovalPending.setToTransporterName(toTransporterDetails.getTransporterName());
			
			TransporterStationDetails transporterStationDetails=(TransporterStationDetails) session.get(TransporterStationDetails.class, transporterStockistTransfer.getTransporterStationDetails().getTransporterStationDetailsId());
			stockistApprovalPending.setTransporterStationName(transporterStationDetails.getTransporterStationName());
			stockistApprovalPending.setTransporterStationId(transporterStationDetails.getTransporterStationDetailsId());
			if(rateId==1)
				stockistApprovalPending.setActualRate(transporterStationDetails.getRatePerStation().getRatePerCase());
			if(rateId==2)
				stockistApprovalPending.setActualRate(transporterStationDetails.getClaimRates().getClaimRates());
			if(rateId==3)
				stockistApprovalPending.setActualRate(transporterStationDetails.getRatePerStation().getRatePerLR());
			if(rateId==4)
			{
				stockistApprovalPending.setActualRate(transporterStationDetails.getClaimRates().getRatePerLR());
			}
			System.out.println("to transporter ID-"+transporterStockistTransfer.getToTransporterDetailsId()+"TransporterStationName-"+transporterStationDetails.getTransporterStationName());
			
			Criteria criteriaTransporter=session.createCriteria(TransporterStationDetails.class,"transporterStationDetails")
					.createAlias("transporterStationDetails.transporterDetails","transporter")
					.add(Restrictions.eq("transporter.transporterDetailsId",transporterStockistTransfer.getToTransporterDetailsId()))
					.add(Restrictions.like("transporterStationName",transporterStationDetails.getTransporterStationName()))
					.add(Restrictions.eq("status",1));
			TransporterStationDetails transporterStationDetailsObj=(TransporterStationDetails) criteriaTransporter.uniqueResult();
		if(rateId==1)
			stockistApprovalPending.setToActualRate(transporterStationDetailsObj.getRatePerStation().getRatePerCase());
		if(rateId==2)
			stockistApprovalPending.setToActualRate(transporterStationDetailsObj.getClaimRates().getClaimRates());
		if(rateId==3)
			stockistApprovalPending.setToActualRate(transporterStationDetailsObj.getRatePerStation().getRatePerLR());
		if(rateId==4)
		{
			stockistApprovalPending.setToActualRate(transporterStationDetailsObj.getClaimRates().getRatePerLR());
		}
		if(rateId==1)
			stockistApprovalPending.setDifference(stockistApprovalPending.getActualRate()-stockistApprovalPending.getToActualRate());
		
		if(rateId==2)
			stockistApprovalPending.setDifference(stockistApprovalPending.getActualRate()-stockistApprovalPending.getToActualRate());
		if(rateId==3)
			stockistApprovalPending.setDifference(stockistApprovalPending.getActualRate()-stockistApprovalPending.getToActualRate());
		if(rateId==4)
			stockistApprovalPending.setDifference(stockistApprovalPending.getActualRate()-stockistApprovalPending.getToActualRate());
		
		System.out.println("difference-"+stockistApprovalPending.getDifference());
		stockistApprovalPendingList.add(stockistApprovalPending);
		System.out.println("In loadPendingStockist dao 592");
		}
		map = new HashMap<String, Object>();
		map.put("stockistApproval", stockistApprovalPendingList);	
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return map;
		
	}

	@Override
	public void saveApprovedData(String[] toTransporterArray, String[] stockistApprovalArray,
			String[] transporterStockistTransferArray)
	{
	Session session=sessionFactory.getCurrentSession();
	for(int i=0;i<stockistApprovalArray.length;i++)
	{ 
		Integer stockistId=Integer.parseInt(stockistApprovalArray[i]);
		Integer transporterId=Integer.parseInt(toTransporterArray[i]);
		Integer transporterStockistTransferId=Integer.parseInt(transporterStockistTransferArray[i]);
		TransporterStockistTransfer transporterStockistTransfer=(TransporterStockistTransfer) session.get(TransporterStockistTransfer.class,transporterStockistTransferId);
		transporterStockistTransfer.setAdminApprovalStatus(1);
		session.saveOrUpdate(transporterStockistTransfer);
		System.out.println("StokcistId-"+stockistApprovalArray[i]+"transporterArray="+toTransporterArray[i]+"transporterStockistTransfer"+transporterStockistTransferArray[i]);
		TransporterDetails transporter=(TransporterDetails) session.get(TransporterDetails.class,transporterId);
		Stockist stockist=(Stockist) session.get(Stockist.class,stockistId);
		stockist.setTransporterDetails(transporter);
		session.saveOrUpdate(stockist);
		
	}
		
	}

	@Override
	public void saveRejectedData(String[] transporterStockistTransferArray) {
		Session session=sessionFactory.getCurrentSession();
		try
		{
			for(int i=0;i<transporterStockistTransferArray.length;i++)
			{
			Integer id=Integer.parseInt(transporterStockistTransferArray[i]);
			TransporterStockistTransfer transporterStockistTransfer=(TransporterStockistTransfer) session.get(TransporterStockistTransfer.class, id);
			transporterStockistTransfer.setAdminApprovalStatus(2);
			session.saveOrUpdate(transporterStockistTransfer);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
