/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.CheckingDoneCheckingSlipImagePath;
import com.protocol.wms.model.EmployeeDetails;

/**
 * @author Sudhakar
 *
 */
@Repository(value="CheckingDoneDaoImpl")
public class CheckingDoneDaoImpl implements CheckingDoneDao{

	private Logger log = Logger.getLogger(CheckingDoneDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CheckingDoneDao#saveCheckingDone(com.protocol.wms.model.CheckingDone)
	 */
	@Override
	public Integer saveCheckingDone(CheckingDone checkingDoneObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer checkingDoneId = null;
		try 
		{
			checkingDoneId=(Integer) session.save(checkingDoneObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return checkingDoneId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CheckingDoneDao#loadCheckingDoneObjByInwardReturnGoodsRegId(java.lang.Integer)
	 */
	@Override
	public CheckingDone loadCheckingDoneObjByInwardReturnGoodsRegId(Integer inwardReturnGoodRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		CheckingDone checkingDone = null;
		try 
		{
			Criteria criteria = session.createCriteria(CheckingDone.class,"CheckingDone")
					.createAlias("CheckingDone.inwardReturnGoodsRegistration", "inwardReturnGoodsRegistration")
					.add(Restrictions.eq("inwardReturnGoodsRegistration.inwardReturnGoodRegistrationId", inwardReturnGoodRegistrationId));
			checkingDone = 	(CheckingDone) criteria.uniqueResult();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return checkingDone;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CheckingDoneDao#loadCheckingDoneObjByCheckingDoneId(java.lang.Integer)
	 */
	@Override
	public CheckingDone loadCheckingDoneObjByCheckingDoneId(Integer checkingDoneId) {
		Session session =sessionFactory.getCurrentSession();
		CheckingDone checkingDone = null;
		try 
		{
			checkingDone = (CheckingDone) session.get(CheckingDone.class,checkingDoneId);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return checkingDone;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CheckingDoneDao#editInwardReturnGoodsRegCheckingDoneDetails(com.protocol.wms.model.CheckingDone, java.lang.Integer)
	 */
	@Override
	public Boolean editInwardReturnGoodsRegCheckingDoneDetails(CheckingDone checkingDone, Integer checkingDoneEmployeeDetailsId) {
		EmployeeDetails employeeDetails = null;
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			employeeDetails = (EmployeeDetails)session.load(EmployeeDetails.class, checkingDoneEmployeeDetailsId);
			checkingDone.setEmployeeDetails(employeeDetails);
			session.update(checkingDone);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<CheckingDoneCheckingSlipImagePath> updateCheckingDoneRegCheckingSlipImage(
			Integer checkingDoneId,
			List<CheckingDoneCheckingSlipImagePath> checkingSlipImgList) {
		CheckingDone checkingDone = null;
		List<CheckingDoneCheckingSlipImagePath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			checkingDone = (CheckingDone) session.load(CheckingDone.class, checkingDoneId);
			for(CheckingDoneCheckingSlipImagePath tempObj : checkingSlipImgList){
				tempObj.setCheckingDone(checkingDone);
				session.save(tempObj);
			}
			lst = checkingDone.getCheckingDoneCheckingSlipImagePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}

	@Override
	public Boolean deleteCheckingDoneCheckingSlipImage(
			Integer checkingSlipImageId) {
		CheckingDoneCheckingSlipImagePath imageObj = null;
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			imageObj = (CheckingDoneCheckingSlipImagePath)session.load(CheckingDoneCheckingSlipImagePath.class, checkingSlipImageId);
			session.delete(imageObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
}
