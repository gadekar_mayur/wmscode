/**
 * 
 */
package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.OrderMode;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistrationOrderCopyPath;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.UserDetails;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OutWardOrderEntryRegistrationDaoImpl")
public class OutWardOrderEntryRegistrationDaoImpl implements OutWardOrderEntryRegistrationDao{

	private Logger log = Logger.getLogger(OutWardOrderEntryRegistrationDaoImpl.class.getClass());
	private SessionFactory sessionFactory;
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderEntryRegistrationDao#saveOutWardOrderEntryRegistration(com.protocol.wms.model.OutWardOrderEntryRegistration)
	 */
	@Override
	public Integer saveOutWardOrderEntryRegistration(
			OutWardOrderEntryRegistration obj) {
		Session session =sessionFactory.getCurrentSession();
		Integer outWardOrderEntryRegistrationId = null;
		try
		{
			outWardOrderEntryRegistrationId=(Integer) session.save(obj);
			
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderEntryRegistrationId;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderEntryRegistrationDao#loadOutWardOrderEntryRegistrationOfProductStatusIsZero()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsZero(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"OutWardOrderEntryRegistration")
					.createAlias("OutWardOrderEntryRegistration.organization", "organization")
					.add(Restrictions.eq("productStatus", 0))
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			 if(organizationId!=null)
			     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					outWardOrderEntryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderEntryRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderEntryRegistrationDao#loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(java.lang.Integer)
	 */
	@Override
	public OutWardOrderEntryRegistration loadOutWardOrderEntryRegistrationUsignOutWardOrderEntryRegistrationId(
			Integer OutWardOrderEntryRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardOrderEntryRegistration OutWardOrderEntryRegistrationObj=null;
		try 
		{
			OutWardOrderEntryRegistrationObj=(OutWardOrderEntryRegistration)session.get(OutWardOrderEntryRegistration.class,OutWardOrderEntryRegistrationId);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return OutWardOrderEntryRegistrationObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderEntryRegistrationDao#updateOutWardOrderEntryRegistration(com.protocol.wms.model.OutWardOrderEntryRegistration)
	 */
	@Override
	public void updateOutWardOrderEntryRegistration(
			OutWardOrderEntryRegistration obj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(obj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderEntryRegistrationDao#loadOutWardOrderEntryRegistrationOfProductStatusIsOne(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsOne(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"OutWardOrderEntryRegistration")
					.createAlias("OutWardOrderEntryRegistration.organization", "organization")
     				//.add(Restrictions.eq("productStatus", 1))
					.add(Restrictions.eq("invoiceStatus", 0))
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			 if(organizationId!=null)
			     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					outWardOrderEntryRegistrationList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderEntryRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderEntryRegistrationDao#loadOutWardOrderEntryRegistrationOfProductStatusIsOneAndInvoiceStatusIsOne(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfProductStatusIsOneAndInvoiceStatusIsOne(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"OutWardOrderEntryRegistration")
					.createAlias("OutWardOrderEntryRegistration.organization", "organization")
					//.add(Restrictions.eq("productStatus", 1))
					.add(Restrictions.eq("invoiceStatus", 1))
					.addOrder(Order.desc("submitDate"));
			 if(organizationId!=null)
			     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					outWardOrderEntryRegistrationList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderEntryRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderEntryRegistrationDao#loadOutWardOrderEntryRegistrationOfStatusIsOne(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardOrderEntryRegistration> loadOutWardOrderEntryRegistrationOfStatusIsOne(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"OutWardOrderEntryRegistration")
					.createAlias("OutWardOrderEntryRegistration.organization", "organization")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			 if(organizationId!=null)
			     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					outWardOrderEntryRegistrationList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderEntryRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderEntryRegistrationDao#loadOutWardOrderEnteryRegistrationIdUsignOrgIdAndCompIdAndStockistId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> loadOutWardOrderEnteryRegistrationIdUsignOrgIdAndCompIdAndStockistId(
			Integer orgid, Integer companyId, Integer stockistId) {
		Session session =sessionFactory.getCurrentSession();
		List<Integer> outWardOrderEntryRegistrationIdList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"OutWardOrderEntryRegistration")
					.createAlias("OutWardOrderEntryRegistration.organization", "organization")
					.createAlias("OutWardOrderEntryRegistration.company", "company")
					.createAlias("OutWardOrderEntryRegistration.stockist", "stockist")
					.add(Restrictions.eq("status", 1))
					.add(Restrictions.eq("organization.organizationId", orgid))
					.add(Restrictions.eq("company.companyId", companyId))
					.add(Restrictions.eq("stockist.stockistId", stockistId))
					.setProjection(Projections.property("outWardOrderEnteryRegistrationId"));;
					outWardOrderEntryRegistrationIdList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardOrderEntryRegistrationIdList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardOrderEntryRegistrationDao#updateOutWardOrderEntryRegistrationObj(com.protocol.wms.model.OutWardOrderEntryRegistration, java.lang.Integer)
	 */
	@Override
	public boolean updateOutWardOrderEntryRegistrationObj(
			OutWardOrderEntryRegistration outWardOrderEntryRegistrationObj,
			Integer orderModeId) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			outWardOrderEntryRegistrationObj.setOrderMode((OrderMode)session.get(OrderMode.class, orderModeId));
			session.update(outWardOrderEntryRegistrationObj);
			return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object>searchOutwardOrderEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {

		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistration = null;
		Session session =sessionFactory.getCurrentSession();
		Map<String,Object>map=null;
		try{
			
			Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outwardOrderEntry")
					.createAlias("outwardOrderEntry.organization", "organization")
					.add(Restrictions.eq("productStatus", 0))
					.add(Restrictions.eq("outwardOrderEntry.status",1))
					.setProjection(Projections.rowCount());
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("outwardOrderEntry.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
				 
			case "Stockist" :
				  criteria.createAlias("outwardOrderEntry.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
				  
			case "OrderId" :
				 criteria.add(Restrictions.like("outwardOrderEntry.orderId", searchText,MatchMode.START));
				 break;
				 
			case "OrderNo" :
				  criteria.add(Restrictions.like("outwardOrderEntry.orderNo", searchText,MatchMode.START));
				  break;
				  
			case "OrderMode" :
				  criteria.createAlias("outwardOrderEntry.orderMode", "orderMode");
				  criteria.add(Restrictions.like("orderMode.orderModeName", searchText,MatchMode.START));
				  break;
				  
			case "Date" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.add(Restrictions.between("outwardOrderEntry.orderDate", sqlFromDate, sqlToDate));
				   }
			}
		Long totalRecordCount = (Long)criteria.uniqueResult();
//-------------------------------------------------------------------------------------------------			
		criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outwardOrderEntry")
					.createAlias("outwardOrderEntry.organization", "organization")
					.add(Restrictions.eq("productStatus", 0))
					.add(Restrictions.eq("outwardOrderEntry.status",1))
					.addOrder(Order.desc("outwardOrderEntry.submitDate"));
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("outwardOrderEntry.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
				 
			case "Stockist" :
				  criteria.createAlias("outwardOrderEntry.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
				  
			case "OrderId" :
				 criteria.add(Restrictions.like("outwardOrderEntry.orderId", searchText,MatchMode.START));
				 break;
				 
			case "OrderNo" :
				  criteria.add(Restrictions.like("outwardOrderEntry.orderNo", searchText,MatchMode.START));
				  break;
				  
			case "OrderMode" :
				  criteria.createAlias("outwardOrderEntry.orderMode", "orderMode");
				  criteria.add(Restrictions.like("orderMode.orderModeName", searchText,MatchMode.START));
				  break;
				  
			case "Date" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
				   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
				   java.util.Date d1= null;
					d1 = dtf.parse(fromSpecialData);
					java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
					d1 = dtf.parse(toSpecialData);
					java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
					
				   criteria.add(Restrictions.between("outwardOrderEntry.orderDate", sqlFromDate, sqlToDate));
			   }
			}
//-----------------------------------------------------------------------------------------------------------			
	    from = from - 1 ;
	    criteria.setFirstResult(from*Constant.PAGE_COUNT);
	    criteria.setMaxResults(Constant.PAGE_COUNT);
	    outWardOrderEntryRegistration=criteria.list();
				
		Integer tatalPage = 0;
		if(totalRecordCount>0){
			tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
			if((totalRecordCount%Constant.PAGE_COUNT)!=0){
				tatalPage=tatalPage+1;
			}
		}
		map = new HashMap<String, Object>();
		map.put("outWardOrderEntryRegistration", outWardOrderEntryRegistration);
		map.put("totalPages", tatalPage);
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return map;
	}
	
	@Override
	public List<OutWardOrderEntryRegistrationOrderCopyPath> updateOutwardGoodsRegOrderCopyImage(
			Integer outwardGoodsRegistrationId,
			List<OutWardOrderEntryRegistrationOrderCopyPath> orderCopyImgList) {
		OutWardOrderEntryRegistration outWardOrderEntryRegistration =null;
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			outWardOrderEntryRegistration = (OutWardOrderEntryRegistration) session.load(OutWardOrderEntryRegistration.class, outwardGoodsRegistrationId);
			for(OutWardOrderEntryRegistrationOrderCopyPath tempObj :orderCopyImgList){
				tempObj.setOutWardOrderEntryRegistration(outWardOrderEntryRegistration);
				session.save(tempObj);
			}
			orderCopyImgList = outWardOrderEntryRegistration.getOutWardOrderEntryRegistrationOrderCopyPathList();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return orderCopyImgList;
	}
	@Override
	public Boolean deleteOutwardGoodsRegistrationOrderCopyImage(
			Integer orderCopyImageId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardOrderEntryRegistrationOrderCopyPath outWardOrderEntryRegistrationOrderCopyPath = null;
		try 
		{
			outWardOrderEntryRegistrationOrderCopyPath = (OutWardOrderEntryRegistrationOrderCopyPath) session.load(OutWardOrderEntryRegistrationOrderCopyPath.class, orderCopyImageId);
			session.delete(outWardOrderEntryRegistrationOrderCopyPath);
			return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object>searchOutwardViewOrderEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer from,Integer organizationId) {
Map<String,Object>map=null;
		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistration = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outwardViewOrderEntry")
					.createAlias("outwardViewOrderEntry.organization", "organization")
					.add(Restrictions.eq("status", 1))
					.setProjection(Projections.rowCount());
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			switch(selectedValue){
				case "Company" : 
					 criteria.createAlias("outwardViewOrderEntry.company", "company");
					 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
					 break;
				case "Organization" :
					 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					 break;
					 
				case "Stockist" :
					  criteria.createAlias("outwardViewOrderEntry.stockist", "stockist");
					  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
					  break;
					  
				case "OrderId" :
					 criteria.add(Restrictions.like("outwardViewOrderEntry.orderId", searchText,MatchMode.START));
					 break;
					 
				case "OrderNo" :
					  criteria.add(Restrictions.like("outwardViewOrderEntry.orderNo", searchText,MatchMode.START));
					  break;
					  
				case "Date" : 
					if(fromSpecialData!=""&&toSpecialData!=""){
						   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
						   java.util.Date d1= null;
							d1 = dtf.parse(fromSpecialData);
							java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
							d1 = dtf.parse(toSpecialData);
							java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
							
						   criteria.add(Restrictions.between("outwardViewOrderEntry.orderDate", sqlFromDate, sqlToDate));
					   }
				}
//-----------------------------------------------------------------------------------------------------------------			
			Long totalRecordCount = (Long)criteria.uniqueResult();
			criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outwardViewOrderEntry")
					.createAlias("outwardViewOrderEntry.organization", "organization")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			switch(selectedValue){
				case "Company" : 
					 criteria.createAlias("outwardViewOrderEntry.company", "company");
					 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
					 break;
				case "Organization" :
					 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					 break;
					 
				case "Stockist" :
					  criteria.createAlias("outwardViewOrderEntry.stockist", "stockist");
					  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
					  break;
					  
				case "OrderId" :
					 criteria.add(Restrictions.like("outwardViewOrderEntry.orderId", searchText,MatchMode.START));
					 break;
					 
				case "OrderNo" :
					  criteria.add(Restrictions.like("outwardViewOrderEntry.orderNo", searchText,MatchMode.START));
					  break;
					  
				case "Date" : 
					if(fromSpecialData!=""&&toSpecialData!=""){
						   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
						   java.util.Date d1= null;
							d1 = dtf.parse(fromSpecialData);
							java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
							d1 = dtf.parse(toSpecialData);
							java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
							
						   criteria.add(Restrictions.between("outwardViewOrderEntry.orderDate", sqlFromDate, sqlToDate));
					   }
				}
//----------------------------------------------------------------------------------------------------------			
			 from = from - 1 ;
			    criteria.setFirstResult(from*Constant.PAGE_COUNT);
			    criteria.setMaxResults(Constant.PAGE_COUNT);
			    outWardOrderEntryRegistration = criteria.list();
						
				Integer totalPages = 0;
				if(totalRecordCount>0){
					totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPages=totalPages+1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardOrderEntryRegistration", outWardOrderEntryRegistration);
				map.put("totalPages", totalPages);
				}
		
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object>searchOutwardinvoiceEntryRegistration(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer from,Integer organizationId) {
		Map<String,Object>map=null;
		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistration = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outwardInvoiceEntry")
					.createAlias("outwardInvoiceEntry.organization", "organization")
//					.add(Restrictions.eq("productStatus", 1))
					.add(Restrictions.eq("invoiceStatus", 0))
					.add(Restrictions.eq("status", 1))
					.setProjection(Projections.rowCount());
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("outwardInvoiceEntry.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
				 
			case "Stockist" :
				  criteria.createAlias("outwardInvoiceEntry.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
				  
			case "OrderId" :
				 criteria.add(Restrictions.like("outwardInvoiceEntry.orderId", searchText,MatchMode.START));
				 break;
				 
			case "OrderNo" :
				  criteria.add(Restrictions.like("outwardInvoiceEntry.orderNo", searchText,MatchMode.START));
				  break;
				  
			case "Date" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.add(Restrictions.between("outwardInvoiceEntry.orderDate", sqlFromDate, sqlToDate));
				   }
			}
			Long totalRecordCount = (Long)criteria.uniqueResult();
			
			 criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outwardInvoiceEntry")
					.createAlias("outwardInvoiceEntry.organization", "organization")
//					.add(Restrictions.eq("productStatus", 1))
					.add(Restrictions.eq("invoiceStatus", 0))
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("outwardInvoiceEntry.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
				 
			case "Stockist" :
				  criteria.createAlias("outwardInvoiceEntry.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
				  
			case "OrderId" :
				 criteria.add(Restrictions.like("outwardInvoiceEntry.orderId", searchText,MatchMode.START));
				 break;
				 
			case "OrderNo" :
				  criteria.add(Restrictions.like("outwardInvoiceEntry.orderNo", searchText,MatchMode.START));
				  break;
				  
			case "Date" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.add(Restrictions.between("outwardInvoiceEntry.orderDate", sqlFromDate, sqlToDate));
				   }
				}
			from = from - 1 ;
		    criteria.setFirstResult(from*Constant.PAGE_COUNT);
		    criteria.setMaxResults(Constant.PAGE_COUNT);
		    outWardOrderEntryRegistration=criteria.list();
					
			Integer tatalPage = 0;
			if(totalRecordCount>0){
				tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					tatalPage=tatalPage+1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("outWardOrderEntryRegistration", outWardOrderEntryRegistration);
			map.put("totalPages", tatalPage);
			}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object>searchOutwardInvoiceEntryAddedRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		
		List<OutWardOrderEntryRegistration> outWardOrderEntryRegistrationList = null;
		Session session =sessionFactory.getCurrentSession();
		Map<String,Object>map=null;
		try
		{
			if("InvoiceNumber".equals(selectedValue)||"GrossAmount".equals(selectedValue)||"NetAmount".equals(selectedValue) || "InvoiceDate".equals(selectedValue))
			{
				Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"InvoiceEntry")
					     .createAlias("InvoiceEntry.outWardOrderEnteryRegistration", "outWardOrderEnteryRegistration")
					     .createAlias("InvoiceEntry.organization", "organization")
					     .setProjection(Projections.distinct(Projections.property("outWardOrderEnteryRegistration.outWardOrderEnteryRegistrationId")));
				 if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				 
				 switch(selectedValue)
				 {
				 	case "InvoiceNumber": 
						 criteria.add(Restrictions.like("InvoiceEntry.invoiceNo", searchText,MatchMode.START));
						 break;
				 	case "GrossAmount":
				 		  criteria.add(Restrictions.ge("InvoiceEntry.grossAmount", Double.parseDouble(fromSpecialData)));
						  criteria.add(Restrictions.le("InvoiceEntry.grossAmount", Double.parseDouble(toSpecialData)));
						  break;
				 	case "NetAmount":
				 		  criteria.add(Restrictions.ge("InvoiceEntry.netAmount", Double.parseDouble(fromSpecialData)));
						  criteria.add(Restrictions.le("InvoiceEntry.netAmount", Double.parseDouble(toSpecialData)));
						  break;
				 	case "InvoiceDate":
				 		if(fromSpecialData!=""&&toSpecialData!=""){
						    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
						    java.util.Date d1= null;
							d1 = dtf.parse(fromSpecialData);
							java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
							d1 = dtf.parse(toSpecialData);
							java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
							
							criteria.add(Restrictions.between("InvoiceEntry.invoiceDate", sqlFromDate,sqlToDate));
						  }
				 }
				 List<Integer> outWardOrderEnteryRegistrationIdList = criteria.list();
				 Long totalRecordCount = 0l;
				 if(outWardOrderEnteryRegistrationIdList.size()>0){
					 criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outWardOrderEntryRegistration")
							 .add(Restrictions.eq("status", 1))
							 .add(Restrictions.eq("invoiceStatus", 1))
						     .setProjection(Projections.rowCount()).addOrder(Order.desc("submitDate"))
						     .add(Restrictions.in("outWardOrderEntryRegistration.outWardOrderEnteryRegistrationId", outWardOrderEnteryRegistrationIdList));
					 
					 
					 totalRecordCount = (Long)criteria.uniqueResult();
					 
					 criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outWardOrderEntryRegistration")
							 .add(Restrictions.eq("status", 1))
							 .add(Restrictions.eq("invoiceStatus", 1))
						     .addOrder(Order.desc("submitDate"))
						     .add(Restrictions.in("outWardOrderEntryRegistration.outWardOrderEnteryRegistrationId", outWardOrderEnteryRegistrationIdList));
					
					 from = from - 1 ;
				    criteria.setFirstResult(from*Constant.PAGE_COUNT);
				    criteria.setMaxResults(Constant.PAGE_COUNT);
				    outWardOrderEntryRegistrationList = criteria.list();
				 }	 
				Integer totalPages = 0;
				if(totalRecordCount>0){
					totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPages=totalPages+1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardOrderEntryRegistrationList", outWardOrderEntryRegistrationList);
				map.put("totalPages", totalPages);
			}
			else
				{
					Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outWardOrderEntryRegistration")
							.createAlias("outWardOrderEntryRegistration.organization", "organization")
	//						.add(Restrictions.eq("productStatus", 1))
							.add(Restrictions.eq("invoiceStatus", 1))
							.add(Restrictions.eq("status", 1))
							.setProjection(Projections.rowCount());
						if(organizationId!=null){
							criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						}
						switch(selectedValue){
							case "Company" : 
								 criteria.createAlias("outWardOrderEntryRegistration.company", "company");
								 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
								 break;
							case "Organization" :
								 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
								 break;
								 
							case "Stockist" :
								  criteria.createAlias("outWardOrderEntryRegistration.stockist", "stockist");
								  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
								  break;
								  
							case "OrderId" :
								 criteria.add(Restrictions.like("outWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
								 break;
								 
							case "OrderNo" :
								  criteria.add(Restrictions.like("outWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));
								  break;
								  
							case "OrderMode" :
								  criteria.createAlias("outWardOrderEntryRegistration.orderMode", "orderMode");
								  criteria.add(Restrictions.like("orderMode.orderModeName", searchText,MatchMode.START));
								  break;
								  
							case "Date" : 
								if(fromSpecialData!=""&&toSpecialData!=""){
									   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
									   java.util.Date d1= null;
										d1 = dtf.parse(fromSpecialData);
										java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
										d1 = dtf.parse(toSpecialData);
										java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
										
									   criteria.add(Restrictions.between("outWardOrderEntryRegistration.orderDate", sqlFromDate, sqlToDate));
								}
							}
						 Long totalRecordCount = (Long)criteria.uniqueResult();
						 
//------------ Insert fetch data here---------------------------------------------------------------------------
						criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outWardOrderEntryRegistration")
								.createAlias("outWardOrderEntryRegistration.organization", "organization")
	       						//.add(Restrictions.eq("productStatus", 1))
								.add(Restrictions.eq("invoiceStatus", 1))
								.add(Restrictions.eq("status", 1))
								.addOrder(Order.desc("submitDate"));
							if(organizationId!=null){
								criteria.add(Restrictions.eq("organization.organizationId", organizationId));
							}
							switch(selectedValue){
								case "Company" : 
									 criteria.createAlias("outWardOrderEntryRegistration.company", "company");
									 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
									 break;
								case "Organization" :
									 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
									 break;
									 
								case "Stockist" :
									  criteria.createAlias("outWardOrderEntryRegistration.stockist", "stockist");
									  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
									  break;
									  
								case "OrderId" :
									 criteria.add(Restrictions.like("outWardOrderEntryRegistration.orderId", searchText,MatchMode.START));
									 break;
									 
								case "OrderNo" :
									  criteria.add(Restrictions.like("outWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));
									  break;
									  
								case "OrderMode" :
									  criteria.createAlias("outWardOrderEntryRegistration.orderMode", "orderMode");
									  criteria.add(Restrictions.like("orderMode.orderModeName", searchText,MatchMode.START));
									  break;
									  
								case "Date" : 
									if(fromSpecialData!=""&&toSpecialData!=""){
										   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
										   java.util.Date d1= null;
											d1 = dtf.parse(fromSpecialData);
											java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
											d1 = dtf.parse(toSpecialData);
											java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
											
										   criteria.add(Restrictions.between("outWardOrderEntryRegistration.orderDate", sqlFromDate, sqlToDate));
									   }
								}
//--------------------------------------------------------------------------------------------------------------------							
					from = from - 1 ;
				    criteria.setFirstResult(from*Constant.PAGE_COUNT);
				    criteria.setMaxResults(Constant.PAGE_COUNT);
				    outWardOrderEntryRegistrationList = criteria.list();
						 
					Integer totalPages = 0;
					if(totalRecordCount>0){
						totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
						if((totalRecordCount%Constant.PAGE_COUNT)!=0){
							totalPages=totalPages+1;
						}
					}
					map = new HashMap<String, Object>();
					map.put("outWardOrderEntryRegistrationList", outWardOrderEntryRegistrationList);
					map.put("totalPages", totalPages);	
						
				}
			}
		
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return map;
		}
	
	@Override
	public boolean deleteOutWardOrderEntry(
			OutWardOrderEntryRegistration outWardOrderEntryRegistration) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(outWardOrderEntryRegistration);
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
	@Override
	public Boolean checkName(String orderNo, Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardOrderEntryRegistration obj=null;
		try 
		{
			Integer indexNo = Integer.parseInt(orderNo.trim());
			Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outWardOrderEntryRegistration")
					.createAlias("outWardOrderEntryRegistration.company", "company")
					.add(Restrictions.eq("company.companyId", companyId))
					.add(Restrictions.eq("outWardOrderEntryRegistration.companyIndex",indexNo));
		//			List<OutWardOrderEntryRegistration> obj1 = criteria.list();
			 obj=(OutWardOrderEntryRegistration)criteria.uniqueResult();
			// obj.getOrderId();
		//	 System.out.println(obj.getOrderId());
				if(obj!=null)
					return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
}

