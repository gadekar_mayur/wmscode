package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistBankDetails;

@Repository(value="StockistBankDetailsDaoImpl")
public class StockistBankDetailsDaoImpl implements StockistBankDetailsDao{

	private Logger log = Logger.getLogger(StockistBankDetailsDaoImpl.class.getClass());
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public JSONObject displayStockistBankDetailsForm() {
		JSONObject stockistFormJson = null;
		JSONArray tempArray = null;
		try{
			stockistFormJson = new JSONObject();
			tempArray = getOrganizationList();
			stockistFormJson.put("orgArray", tempArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return stockistFormJson;
	}
	@SuppressWarnings("unchecked")
	private JSONArray getOrganizationList(){
		Session session =sessionFactory.getCurrentSession();
		List<Organization> orgList = null;
		JSONObject orgJson = null;
		JSONArray  orgArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(Organization.class)
					.add(Restrictions.eq("organizationStatus", "1"));
			orgList = criteria.list();
			Iterator<Organization> orgIt = orgList.iterator();
			//System.out.println("Darrrrrrrr");
			while(orgIt.hasNext()){
				Organization temp = orgIt.next();
				orgJson = new JSONObject();
				orgJson.put("orgId", temp.getOrganizationId());
				orgJson.put("orgName", temp.getOrganizationName());
				orgArray.put(orgJson);
			}
			//stateJson = new JSONObject();
			//stateJson.put("stateArray",stateArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return orgArray;
	}

	@Override
	public JSONObject saveStockistBankDetails(Integer orgId,Integer stockistId, Integer companyId,StockistBankDetails bankDetatils) {
		Organization org=null;
		Company company=null;
		Stockist stockist = null;
		JSONObject result = new JSONObject();
		Session session =sessionFactory.getCurrentSession();
		try{
			org = (Organization) session.load(Organization.class, orgId);
			company = (Company) session.load(Company.class, companyId);
			stockist = (Stockist) session.load(Stockist.class, stockistId);
			bankDetatils.setOrganization(org);
			bankDetatils.setCompany(company);
			bankDetatils.setStockist(stockist);
			//
			Date today = new Date();
		    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		       df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		       String IST = df.format(today);
		       DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		       Date date = df2.parse(IST);
		       bankDetatils.setSubmitDate(date);
			//
		       int i =(Integer) session.save(bankDetatils);
		       if(i>0){
		    	   result.put("result", true);
		    	   System.out.println("Iserted successfully id = "+stockist.getStockistId());
		       }
		       else{
		    	   result.put("result", false);
		    	   System.out.println("Failed");
		       }
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject stockistBankDetailsList(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<StockistBankDetails> stockiatBankList = null;
		JSONObject stockiatBankJson = null;
		JSONArray  stockistBankArray = new JSONArray();
		Organization organization = null;
		Company company = null;
		//Stockist 
		try{
			Criteria criteria = session.createCriteria(StockistBankDetails.class,"stockistBank")
					.createAlias("stockistBank.organization", "org")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
					criteria.add(Restrictions.eq("org.organizationId", organizationId));
			
			stockiatBankList = criteria.list();
			Iterator<StockistBankDetails> stockiatBankIt = stockiatBankList.iterator();
			//System.out.println("Darrrrrrrr");
			while(stockiatBankIt.hasNext()){
				StockistBankDetails temp = stockiatBankIt.next();
				if(temp.getStockist().getStatus() == 1){
					organization = (Organization) session.get(Organization.class, temp.getOrganization().getOrganizationId());
					company = (Company) session.get(Company.class, temp.getCompany().getCompanyId());
					if("1".equals(organization.getOrganizationStatus())&&company.getStatus()==1){
						stockiatBankJson = new JSONObject();
						stockiatBankJson.put("ORG_NAME", organization.getOrganizationName());
						stockiatBankJson.put("COMPANY", company.getCompanyName());
						stockiatBankJson.put("STOCKIST_BANK_ID", temp.getStockistBankDetailsId());
						stockiatBankJson.put("STOCKIST_NAME", temp.getStockist().getStockistName());
						stockiatBankJson.put("BANK_NAME", temp.getStockistBankName());
						stockiatBankJson.put("BRANCH",temp.getStockistBankBranchName());
						stockiatBankJson.put("ACC_NO",temp.getStockistBankaccountNo());
						stockiatBankJson.put("IFSC",temp.getStockistBankIfscCode());
						stockistBankArray.put(stockiatBankJson);
					}
				}
			}
			stockiatBankJson = new JSONObject();
			stockiatBankJson.put("stockistBankArray",stockistBankArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return stockiatBankJson;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistBankDetailsDao#stockistBankDropdownList(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<StockistBankDetails> stockistBankDropdownList(Integer organizationId, Integer companyId, Integer stockistId) {
		List<StockistBankDetails> stockistBankList = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(StockistBankDetails.class,"stockistBank")
					.createAlias("stockistBank.organization", "org")
					.createAlias("stockistBank.company", "company")
					.createAlias("stockistBank.stockist", "stockist")
					.add(Restrictions.eq("status", 1))
					.add(Restrictions.eq("org.organizationId", organizationId))
					.add(Restrictions.eq("company.companyId",companyId))
					.add(Restrictions.eq("stockist.stockistId", stockistId))
					.addOrder(Order.desc("submitDate"));
			
			stockistBankList = criteria.list();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return stockistBankList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistBankDetailsDao#searchStokistBankDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject searchStokistBankDetails(String searchOption,
			String searchText, Integer organizationId,Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<StockistBankDetails> stockiatBankList = null;
		JSONObject stockiatBankJson = null;
		JSONArray  stockistBankArray = new JSONArray();
		Organization organization = null;
		Company company = null;
		Integer totalPage = 0;
		//Stockist 
		try{
			Criteria criteria = session.createCriteria(StockistBankDetails.class,"stockistBank")
					.createAlias("stockistBank.organization", "org")
					.createAlias("stockistBank.company", "company")
					.createAlias("stockistBank.stockist", "stockist")
					.add(Restrictions.eq("stockist.status", 1))
					.add(Restrictions.eq("org.organizationStatus", "1"))
					.add(Restrictions.eq("company.status", 1))
					.add(Restrictions.eq("status", 1))
					.setProjection(Projections.rowCount())
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("org.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("org.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("StockistName".equals(searchOption))
				criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
			else if("BankName".equals(searchOption))
				criteria.add(Restrictions.like("stockistBank.StockistBankName", searchText,MatchMode.START));
			else if("Branch".equals(searchOption))
				criteria.add(Restrictions.like("stockistBank.StockistBankBranchName", searchText,MatchMode.START));
			else if("AccountNo".equals(searchOption))
				criteria.add(Restrictions.like("stockistBank.StockistBankaccountNo", searchText,MatchMode.START));
			else if("IFSC".equals(searchOption))
				criteria.add(Restrictions.like("stockistBank.StockistBankIfscCode", searchText,MatchMode.START));
			Long totalRecordCount = (Long)criteria.uniqueResult();
			
			
			criteria = session.createCriteria(StockistBankDetails.class,"stockistBank")
					.createAlias("stockistBank.organization", "org")
					.createAlias("stockistBank.company", "company")
					.createAlias("stockistBank.stockist", "stockist")
					.add(Restrictions.eq("stockist.status", 1))
					.add(Restrictions.eq("org.organizationStatus", "1"))
					.add(Restrictions.eq("company.status", 1))
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("org.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("org.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("StockistName".equals(searchOption))
				criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
			else if("BankName".equals(searchOption))
				criteria.add(Restrictions.like("stockistBank.StockistBankName", searchText,MatchMode.START));
			else if("Branch".equals(searchOption))
				criteria.add(Restrictions.like("stockistBank.StockistBankBranchName", searchText,MatchMode.START));
			else if("AccountNo".equals(searchOption))
				criteria.add(Restrictions.like("stockistBank.StockistBankaccountNo", searchText,MatchMode.START));
			else if("IFSC".equals(searchOption))
				criteria.add(Restrictions.like("stockistBank.StockistBankIfscCode", searchText,MatchMode.START));
			
			from = from - 1 ;
			criteria.setFirstResult(from*Constant.PAGE_COUNT);
			criteria.setMaxResults(Constant.PAGE_COUNT);
			stockiatBankList = criteria.list();
			
			if(totalRecordCount>0){
				totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPage=totalPage+1;
				}
			}
			
			Iterator<StockistBankDetails> stockiatBankIt = stockiatBankList.iterator();
			while(stockiatBankIt.hasNext()){
				StockistBankDetails temp = stockiatBankIt.next();
//				if(temp.getStockist().getStatus() == 1){
					organization = (Organization) session.get(Organization.class, temp.getOrganization().getOrganizationId());
					company = (Company) session.get(Company.class, temp.getCompany().getCompanyId());
//					if("1".equals(organization.getOrganizationStatus())&&company.getStatus()==1){
						stockiatBankJson = new JSONObject();
						stockiatBankJson.put("ORG_NAME", organization.getOrganizationName());
						stockiatBankJson.put("COMPANY", company.getCompanyName());
						stockiatBankJson.put("STOCKIST_BANK_ID", temp.getStockistBankDetailsId());
						stockiatBankJson.put("STOCKIST_NAME", temp.getStockist().getStockistName());
						stockiatBankJson.put("BANK_NAME", temp.getStockistBankName());
						stockiatBankJson.put("BRANCH",temp.getStockistBankBranchName());
						stockiatBankJson.put("ACC_NO",temp.getStockistBankaccountNo());
						stockiatBankJson.put("IFSC",temp.getStockistBankIfscCode());
						stockistBankArray.put(stockiatBankJson);
//					}
//				}
			}
			stockiatBankJson = new JSONObject();
			stockiatBankJson.put("paginationCount", totalPage);
			stockiatBankJson.put("stockistBankArray",stockistBankArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return stockiatBankJson;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#deleteStockistBankDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteStockistBankDetails(Integer stockistBankId) {
		Session session =sessionFactory.getCurrentSession();
		StockistBankDetails stockistBankDetails = null;
		try{
			stockistBankDetails = (StockistBankDetails)session.get(StockistBankDetails.class, stockistBankId);
			stockistBankDetails.setStatus(0);
			session.update(stockistBankDetails);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistBankDetailsDao#loadStockistBankDetailsUsignStockistBankDetailsId(java.lang.Integer)
	 */
	@Override
	public StockistBankDetails loadStockistBankDetailsUsignStockistBankDetailsId(Integer stockistBankDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		StockistBankDetails stockistBankDetailsObj=null;
		try 
		{
			stockistBankDetailsObj=(StockistBankDetails)session.get(StockistBankDetails.class,stockistBankDetailsId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stockistBankDetailsObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistBankDetailsDao#updateStockistBankDetails(com.protocol.wms.model.StockistBankDetails)
	 */
	@Override
	public Boolean updateStockistBankDetails(StockistBankDetails stockistBankDetails) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(stockistBankDetails);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

}
