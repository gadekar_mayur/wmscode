/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.CheckingDone;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails;
import com.protocol.wms.model.InwardReturnProducts;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.TransporterDetails;

/**
 * @author Sudhakar
 *
 */
@Repository(value="InwardReturnGoodsRegistrationDaoImpl")
public class InwardReturnGoodsRegistrationDaoImpl implements InwardReturnGoodsRegistrationDao{
	
	private Logger log = Logger.getLogger(InwardReturnGoodsRegistrationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#loadInwardReturnGoodsRegistrationListUsingOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(InwardReturnGoodsRegistration.class,"InwardReturnGoodsRegistration")
				     .createAlias("InwardReturnGoodsRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("checkingDoneStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   inwardReturnGoodsRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardReturnGoodsRegistrationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(java.lang.Integer)
	 */
	@Override
	public InwardReturnGoodsRegistration loadInwardReturnGoodsRegistrationObjInwardReturnGoodsRegistrationoId(
			Integer InwardReturnGoodsRegistrationoId) {
		Session session =sessionFactory.getCurrentSession();
		InwardReturnGoodsRegistration inwardReturnGoodsRegistrationObj=null;
		try 
		{
			inwardReturnGoodsRegistrationObj=(InwardReturnGoodsRegistration)session.get(InwardReturnGoodsRegistration.class,InwardReturnGoodsRegistrationoId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardReturnGoodsRegistrationObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#loadInwardReturnGoodsRegistrationLRDetails(java.lang.Integer)
	 */
	@Override
	public InwardReturnGoodsRegistrationLRDetails loadInwardReturnGoodsRegistrationLRDetails(Integer InwardReturnGoodsRegistrationLRDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails=null;
		try 
		{
			inwardReturnGoodsRegistrationLRDetails=(InwardReturnGoodsRegistrationLRDetails)session.get(InwardReturnGoodsRegistrationLRDetails.class,InwardReturnGoodsRegistrationLRDetailsId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardReturnGoodsRegistrationLRDetails;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#updateInwardReturnGoodsRegistrationObj(com.protocol.wms.model.InwardReturnGoodsRegistration)
	 */
	@Override
	public void updateInwardReturnGoodsRegistrationObj(
			InwardReturnGoodsRegistration inwardReturnGoodsRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(inwardReturnGoodsRegistrationObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOne(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOne(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(InwardReturnGoodsRegistration.class,"InwardReturnGoodsRegistration")
				     .createAlias("InwardReturnGoodsRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("checkingDoneStatus", 1))
				     .add(Restrictions.eq("creditNoteStatus", 0))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   inwardReturnGoodsRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardReturnGoodsRegistrationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#inwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.util.List, java.util.Map)
	 */
	@Override
	public Boolean saveInwardReturnGoodsRegistration(Integer organizationId,
			Integer companyId, Integer stockistId, Integer transporterId,
			Integer employeeId,
			List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList,
			Map<Integer, List<InwardReturnProducts>> inwardReturnProductsMap,
			InwardReturnGoodsRegistration inwardReturnGoodsRegistration) {
		Session session =sessionFactory.getCurrentSession();
		Organization organization = null;
		Company company = null;
		Stockist stockist = null;
		TransporterDetails transporterDetails = null;
		EmployeeDetails employeeDetails = null;
		
		try{
			organization =(Organization)session.load(Organization.class, organizationId);
			company =(Company)session.load(Company.class, companyId);
			stockist =(Stockist)session.load(Stockist.class, stockistId);
			transporterDetails =(TransporterDetails)session.load(TransporterDetails.class, transporterId);
			employeeDetails =(EmployeeDetails)session.load(EmployeeDetails.class, employeeId);
			inwardReturnGoodsRegistration.setOrganization(organization);
			inwardReturnGoodsRegistration.setCompany(company);
			inwardReturnGoodsRegistration.setStockist(stockist);
			inwardReturnGoodsRegistration.setTransporterDetails(transporterDetails);
			inwardReturnGoodsRegistration.setEmployeeDetails(employeeDetails);
			
			Integer id = (Integer) session.save(inwardReturnGoodsRegistration);
			if(id!=null){
				for(int i = 0;i<lrDetailsList.size();i++){
					lrDetailsList.get(i).setInwardReturnGoodsRegistration(inwardReturnGoodsRegistration);
					session.save(lrDetailsList.get(i));
					List<InwardReturnProducts> inwardReturnProductsList = null;
					if(inwardReturnProductsMap.containsKey(i)){
						inwardReturnProductsList=inwardReturnProductsMap.get(i);
						for(int j = 0;j<inwardReturnProductsList.size();j++){
							inwardReturnProductsList.get(j).setInwardReturnGoodsRegistrationLRDetails(lrDetailsList.get(i));
							session.save(inwardReturnProductsList.get(j));
						}
					}
				}
				return true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#updateInwardReturnGoodsLrReg(java.util.List)
	 */
	@Override
	public Boolean updateInwardReturnGoodsLrReg(List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList) {
		Session session =sessionFactory.getCurrentSession();
		try{
			for(int i = 0;i<lrDetailsList.size();i++){
				InwardReturnGoodsRegistrationLRDetails tempObj = (InwardReturnGoodsRegistrationLRDetails) session.load(InwardReturnGoodsRegistrationLRDetails.class, lrDetailsList.get(i).getInwardReturnGoodsRegistrationLRDetailsId());
				//tempObj.setClaimCopyImagePath(lrDetailsList.get(i).getClaimCopyImagePath());
				//tempObj.setLRImagePath(lrDetailsList.get(i).getLRImagePath());
				session.update(tempObj);
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
    public List<InwardReturnGoodsRegistration> loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne(
            Integer organizationId) {

        Session session =sessionFactory.getCurrentSession();
        List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList=null;
        try
        {
            Criteria criteria = session.createCriteria(InwardReturnGoodsRegistration.class,"InwardReturnGoodsRegistration")
            		 .createAlias("InwardReturnGoodsRegistration.organization", "organization")
                     .add(Restrictions.eq("status", 1))
                     .add(Restrictions.eq("checkingDoneStatus", 1))
                     .add(Restrictions.eq("creditNoteStatus", 1))
                     .addOrder(Order.desc("submitDate"));;
                   if(organizationId!=null)
                	  criteria.add(Restrictions.eq("organization.organizationId", organizationId));
                   	  inwardReturnGoodsRegistrationList=criteria.list();
        }
        catch(DataAccessException e)
        {
            e.printStackTrace();
            log.error(e);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            log.error(e);
        }
        return inwardReturnGoodsRegistrationList;

    }
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#inwardReturnGoodsRegListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InwardReturnGoodsRegistration> inwardReturnGoodsRegListing(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<InwardReturnGoodsRegistration> inwardReturnGoodsRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(InwardReturnGoodsRegistration.class,"InwardReturnGoodsRegistration")
				     .createAlias("InwardReturnGoodsRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   inwardReturnGoodsRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardReturnGoodsRegistrationList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#inwardReturnGoodsRegLrListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InwardReturnGoodsRegistrationLRDetails> inwardReturnGoodsRegLrListing(Integer inwardReturnGoodRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		List<InwardReturnGoodsRegistrationLRDetails> inwardReturnGoodsRegistrationLRDetailsList=null;
		try 
		{
			Criteria criteria = session.createCriteria(InwardReturnGoodsRegistrationLRDetails.class,"InwardReturnGoodsRegistrationLR")
				     .createAlias("InwardReturnGoodsRegistrationLR.inwardReturnGoodsRegistration", "inwardReturnGoodsReg")
				     .add(Restrictions.eq("inwardReturnGoodsReg.inwardReturnGoodRegistrationId", inwardReturnGoodRegistrationId))
				     .add(Restrictions.eq("status", 1));
				   inwardReturnGoodsRegistrationLRDetailsList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardReturnGoodsRegistrationLRDetailsList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#inwardReturnGoodsRegLrIdDoNotHaveProduct(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> inwardReturnGoodsRegLrIdDoNotHaveProduct(
			Integer inwardReturnGoodRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		List<Integer> lrIdList=null;
		try 
		{
			Criteria criteria = session.createCriteria(InwardReturnGoodsRegistrationLRDetails.class,"InwardReturnGoodsRegistrationLR")
				     .createAlias("InwardReturnGoodsRegistrationLR.inwardReturnGoodsRegistration", "inwardReturnGoodsReg")
				     .add(Restrictions.eq("inwardReturnGoodsReg.inwardReturnGoodRegistrationId", inwardReturnGoodRegistrationId))
				     .add(Restrictions.eq("status", 1))
				     .setProjection(Projections.distinct(Projections.property("InwardReturnGoodsRegistrationLRDetailsId")));
			lrIdList = criteria.list();
			///System.out.println("before=>"+lrIdList);
		   criteria = session.createCriteria(InwardReturnProducts.class,"InwardReturnProducts")
				     .createAlias("InwardReturnProducts.inwardReturnGoodsRegistrationLRDetails", "inwardReturnGoodsReg")
				     .add(Restrictions.in("inwardReturnGoodsRegistrationLRDetails.InwardReturnGoodsRegistrationLRDetailsId", lrIdList))
				     .add(Restrictions.eq("status", 1))
				     .setProjection(Projections.distinct(Projections.property("inwardReturnGoodsRegistrationLRDetails.InwardReturnGoodsRegistrationLRDetailsId")));
		   lrIdList = criteria.list();
		  // System.out.println("After=>"+lrIdList);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return lrIdList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#inwardProductListByLR_NO(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InwardReturnProducts> inwardProductListByLR_NO(Integer LR_ID) {
		Session session =sessionFactory.getCurrentSession();
		List<InwardReturnProducts> inwardReturnProductsList=null;
		try 
		{
			Criteria criteria = session.createCriteria(InwardReturnProducts.class,"inwardReturnProducts")
				     .createAlias("inwardReturnProducts.inwardReturnGoodsRegistrationLRDetails", "irgrLRDetails")
				     .add(Restrictions.eq("irgrLRDetails.InwardReturnGoodsRegistrationLRDetailsId", LR_ID))
				     .add(Restrictions.eq("status", 1));
			
			inwardReturnProductsList = criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardReturnProductsList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#deleteInwardReturnGoodsRegDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteInwardReturnGoodsRegDetails(Integer inwardReturnGoodRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		InwardReturnGoodsRegistration inwardReturnGoodsRegistration= null;
		try{
			inwardReturnGoodsRegistration = (InwardReturnGoodsRegistration)session.get(InwardReturnGoodsRegistration.class, inwardReturnGoodRegistrationId);
			inwardReturnGoodsRegistration.setStatus(0);
			session.update(inwardReturnGoodsRegistration);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#deleteInwardReturnGoodsRegLrDetails(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Boolean deleteInwardReturnGoodsRegLrDetails(Integer LR_ID) {
		Session session =sessionFactory.getCurrentSession();
		InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails= null;
		List<InwardReturnGoodsRegistrationLRDetails>lrDetailsList = null;
		try{
			inwardReturnGoodsRegistrationLRDetails = (InwardReturnGoodsRegistrationLRDetails)session.get(InwardReturnGoodsRegistrationLRDetails.class, LR_ID);
			inwardReturnGoodsRegistrationLRDetails.setStatus(0);
			session.update(inwardReturnGoodsRegistrationLRDetails);
			
			Criteria criteria = session.createCriteria(InwardReturnGoodsRegistrationLRDetails.class,"LRDetails")
				     .createAlias("LRDetails.inwardReturnGoodsRegistration", "inwardReturnGoodsRegistration")
				     .add(Restrictions.eq("inwardReturnGoodsRegistration.inwardReturnGoodRegistrationId",inwardReturnGoodsRegistrationLRDetails.getInwardReturnGoodsRegistration().getInwardReturnGoodRegistrationId()))
				     .add(Restrictions.eq("status", 1));
			lrDetailsList = criteria.list();
			if(!(lrDetailsList.size()>0)){
				InwardReturnGoodsRegistration tempObj = (InwardReturnGoodsRegistration)session.load(InwardReturnGoodsRegistration.class, inwardReturnGoodsRegistrationLRDetails.getInwardReturnGoodsRegistration().getInwardReturnGoodRegistrationId());
				tempObj.setStatus(0);
				session.update(tempObj);
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#deleteInwardReturnGoodsRegLRProduct(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Boolean deleteInwardReturnGoodsRegLRProduct(Integer PRODUCT_ID) {
		Session session =sessionFactory.getCurrentSession();
		InwardReturnProducts inwardReturnProducts= null;
		List<InwardReturnProducts>inwardReturnProductsList = null;
		try{
			inwardReturnProducts = (InwardReturnProducts)session.get(InwardReturnProducts.class, PRODUCT_ID);
			inwardReturnProducts.setStatus(0);
			session.update(inwardReturnProducts);
			
			/*Criteria criteria = session.createCriteria(InwardReturnProducts.class,"inwardReturnProducts")
				     .createAlias("inwardReturnProducts.inwardReturnGoodsRegistrationLRDetails", "LR_Details")
				     .add(Restrictions.eq("LR_Details.InwardReturnGoodsRegistrationLRDetailsId",inwardReturnProducts.getInwardReturnGoodsRegistrationLRDetails().getInwardReturnGoodsRegistrationLRDetailsId()))
				     .add(Restrictions.eq("status", 1));
			inwardReturnProductsList = criteria.list();
			if(!(inwardReturnProductsList.size()>0)){
				InwardReturnGoodsRegistrationLRDetails tempObj = (InwardReturnGoodsRegistrationLRDetails)session.load(InwardReturnGoodsRegistrationLRDetails.class, inwardReturnProducts.getInwardReturnGoodsRegistrationLRDetails().getInwardReturnGoodsRegistrationLRDetailsId());
				tempObj.setStatus(0);
				session.update(tempObj);
				
				criteria = session.createCriteria(InwardReturnGoodsRegistrationLRDetails.class,"LRDetails")
					     .createAlias("LRDetails.inwardReturnGoodsRegistration", "inwardReturnGoodsRegistration")
					     .add(Restrictions.eq("inwardReturnGoodsRegistration.inwardReturnGoodRegistrationId",tempObj.getInwardReturnGoodsRegistration().getInwardReturnGoodRegistrationId()))
					     .add(Restrictions.eq("status", 1));
				List<InwardReturnGoodsRegistrationLRDetails> lrDetailsList = criteria.list();
				if(!(lrDetailsList.size()>0)){
					InwardReturnGoodsRegistration inwardReturnGoodsRegistration = (InwardReturnGoodsRegistration)session.load(InwardReturnGoodsRegistration.class, tempObj.getInwardReturnGoodsRegistration().getInwardReturnGoodRegistrationId());
					inwardReturnGoodsRegistration.setStatus(0);
					session.update(inwardReturnGoodsRegistration);
				}
			}*/
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}


	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#addProductsToInwardReturnGoodsLRInCheckingPending(java.lang.Integer, java.util.List)
	 */
	@Override
	public Boolean addProductsToInwardReturnGoodsLRInCheckingPending(
			Integer lrId, List<InwardReturnProducts> inwardReturnProductsList) {
		InwardReturnProducts inwardReturnProducts= null;
		InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails =null;
		Session session =sessionFactory.getCurrentSession();
		try{
			inwardReturnGoodsRegistrationLRDetails = (InwardReturnGoodsRegistrationLRDetails)session.load(InwardReturnGoodsRegistrationLRDetails.class, lrId);
			for(int i=0;i<inwardReturnProductsList.size();i++){
				inwardReturnProducts = inwardReturnProductsList.get(i);
				inwardReturnProducts.setInwardReturnGoodsRegistrationLRDetails(inwardReturnGoodsRegistrationLRDetails);
				session.save(inwardReturnProducts);
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#editInwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardReturnGoodsRegistration)
	 */
	@Override
	public Boolean editInwardReturnGoodsRegistration(Integer organizationId,
			Integer companyId, Integer stockistId, Integer transporterId,
			Integer employeeId,
			InwardReturnGoodsRegistration inwardReturnGoodsRegistration) {
		Organization organization = null;
		Company company = null;
		Stockist stockist = null;
		TransporterDetails transporterDetails = null;
		EmployeeDetails employeeDetails = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization =(Organization)session.load(Organization.class, organizationId);
			company =(Company)session.load(Company.class, companyId);
			stockist =(Stockist)session.load(Stockist.class, stockistId);
			transporterDetails =(TransporterDetails)session.load(TransporterDetails.class, transporterId);
			employeeDetails =(EmployeeDetails)session.load(EmployeeDetails.class, employeeId);
			inwardReturnGoodsRegistration.setOrganization(organization);
			inwardReturnGoodsRegistration.setCompany(company);
			inwardReturnGoodsRegistration.setStockist(stockist);
			inwardReturnGoodsRegistration.setTransporterDetails(transporterDetails);
			inwardReturnGoodsRegistration.setEmployeeDetails(employeeDetails);
			session.update(inwardReturnGoodsRegistration);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationDao#editInwardReturnGoodsRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardReturnGoodsRegistration, java.lang.Integer, com.protocol.wms.model.CheckingDone)
	 */
	@Override
	public Boolean editInwardReturnGoodsRegistration(Integer organizationId,
			Integer companyId, Integer stockistId, Integer transporterId,
			Integer employeeId,
			InwardReturnGoodsRegistration inwardReturnGoodsRegistration,
			Integer checkingDoneEmployeeDetailsId, CheckingDone checkingDone) {
		Organization organization = null;
		Company company = null;
		Stockist stockist = null;
		TransporterDetails transporterDetails = null;
		EmployeeDetails employeeDetails = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization =(Organization)session.load(Organization.class, organizationId);
			company =(Company)session.load(Company.class, companyId);
			stockist =(Stockist)session.load(Stockist.class, stockistId);
			transporterDetails =(TransporterDetails)session.load(TransporterDetails.class, transporterId);
			employeeDetails =(EmployeeDetails)session.load(EmployeeDetails.class, employeeId);
			inwardReturnGoodsRegistration.setOrganization(organization);
			inwardReturnGoodsRegistration.setCompany(company);
			inwardReturnGoodsRegistration.setStockist(stockist);
			inwardReturnGoodsRegistration.setTransporterDetails(transporterDetails);
			inwardReturnGoodsRegistration.setEmployeeDetails(employeeDetails);
			session.update(inwardReturnGoodsRegistration);
			if(checkingDone!=null){
				EmployeeDetails checkingDoneEmployeeDetails = (EmployeeDetails)session.load(EmployeeDetails.class, checkingDoneEmployeeDetailsId);
				checkingDone.setEmployeeDetails(checkingDoneEmployeeDetails);
				session.update(checkingDone);
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
}
