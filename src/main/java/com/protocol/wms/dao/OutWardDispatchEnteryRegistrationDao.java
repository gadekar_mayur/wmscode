/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;


import java.util.Map;

import com.protocol.wms.model.OutWardDispatchEnteryRegistration;

/**
 * @author Sudhakar
 *
 */
public interface OutWardDispatchEnteryRegistrationDao {
	
	public Integer saveOutWardDispatchEnteryRegistrationObj(OutWardDispatchEnteryRegistration obj);
	
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(Integer outWardDispatchEnteryRegistrationId);
	
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfTripStatusZero(Integer organizationId);

	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOne(Integer organizationId);
	
	public void updateOutWardDispatchEnteryRegistrationObj(OutWardDispatchEnteryRegistration OutWardDispatchEnteryRegistrationObj);
	
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsigOrgIdAndStockistIdAndCompanyId(Integer orgId,Integer stokistId,Integer companyId);
	
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOneAndTripStatusOne(Integer organizationId);
	
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(Integer organizationId);
	
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(Integer outwardTripId);
	
	public boolean updateOutWardDispatchEnteryRegistrationObjDetails(Integer outWardDispatchEnteryRegistrationId,Integer noOfCases);
	
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsignOutWardGatePassId(Integer outWardGetPassId);
	
	public boolean updateOutWardDispatchEnteryRegistrationObjDetails(OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj);

	public Map<String,Object> searchOutwardAddTrip(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);

	public Map<String,Object> searchOutwardViewDispatchEntryRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);
	
	public Map<String,Object> searchOutwardViewPrintStickerEntryRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);

	public Map<String,Object>searchOutwardGetPassList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);

	public Map<String,Object> searchViewGetPassLRList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);
	
	public boolean deleteOutWardDispatchEnteryRegistrationObjDetails(OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj);
}
