/**
 * 
 */
package com.protocol.wms.dao;

import com.protocol.wms.model.ClaimRates;

/**
 * @author Sudhakar
 *
 */
public interface ClaimRatesDao {
	
	public Integer saveClaimRate(ClaimRates claimRates);

}
