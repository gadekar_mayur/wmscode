/**
 * 
 */
package com.protocol.wms.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.RatePerStation;

/**
 * @author Sudhakar
 *
 */
@Repository(value="RatePerStationDaoImpl")
public class RatePerStationDaoImpl implements RatePerStationDao{

	private Logger log = Logger.getLogger(RatePerStationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RatePerStationDao#saveRatePerStationObject(com.protocol.wms.model.RatePerStation)
	 */
	@Override
	public Integer saveRatePerStationObject(RatePerStation ratePerStationObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer ratePerSationId = null;
		try 
		{
			ratePerSationId=(Integer) session.save(ratePerStationObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return ratePerSationId;
	}
}
