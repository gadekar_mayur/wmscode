/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistDocument;
import com.protocol.wms.model.StockistDocumentImagePath;
import com.protocol.wms.model.UserDetails;

/**
 * @author admin
 *
 */
public interface StockistDao {
	
	public JSONObject displayStockistDetailsForm(Integer organizationId);
	
	public JSONObject organizationListDropdown(Integer organizationId);
	
	public JSONObject getStockistByOrganization(Integer orgId);
	
	public JSONObject getStockistByOrgAndCompany(Integer orgId,Integer companyId);
	
	public JSONObject saveStockist(Stockist stockist,Integer orgId,Integer stateId,Integer cityId,Integer companyId,Integer districtId,Integer transporterDetailsId,UserDetails userDetails);
	
	public JSONObject getStokistList(Integer organizationId);
	
	public JSONObject searchStockistDetails(String searchOption,String searchText,Integer organizationId,Integer from);
	
	public JSONObject addCompanyToStockist(Integer[] companyId,Integer stockistId,Integer orgId);
	
	public JSONObject stockistListingAccToOrg(Integer orgnizationId);
	
	public JSONObject searchOrganizationStockist(String searchOption,String searchText,Integer orgnizationId,Integer from);
	
	public JSONObject uploadStockistDocument(Integer orgId,Integer stockistId,Integer companyId,Integer documentTypeId,StockistDocument stockistDocument);
	
	public JSONArray uploadDocumentListing(Integer organizationId);
	
	public JSONArray searchUploadedDocumentsDetails(String searchOption,String searchText,Integer organizationId,Integer from);
	
	public Boolean deleteStockistDetails(Integer stockistId);
	
	public Boolean deleteStockistDocumentDetails(Integer stockistDocId);
	
	public Stockist loadStockistObjUsigStokistId(Integer stokistId);
	
	public Stockist loadStockistObjectByStockistId(Integer stockistId);
	
	public JSONArray getStockistCompanyList(Integer stockistId);
	
	public Integer insertStockistDetilsAndDeleteOldStockistDetails(Integer stockistId, Integer transporterDetailsId, Integer stateId, Integer districtId,Integer cityId, Stockist stockist);
	
	public Boolean updateStockistDetails(Stockist stockist);
	
	public StockistDocument loadStockistDocumentObjById(Integer stockistDocumentId);
	
	public Boolean updateStockistDocumentObj(StockistDocument stockistDocument);
	
	public List<StockistDocumentImagePath> updateCompanyUploadDocumentImage(Integer stockistDocumentId, List<StockistDocumentImagePath>  documentList);
	
	public Boolean deleteStockistDocumentFileImage(Integer stockistDocumentPathId);
}
