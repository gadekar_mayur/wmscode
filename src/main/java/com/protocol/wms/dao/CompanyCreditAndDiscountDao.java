/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.protocol.wms.model.CompanyCreditAndDiscount;

/**
 * @author Sudhakar
 *
 */
public interface CompanyCreditAndDiscountDao {
	
	public CompanyCreditAndDiscount loadCompanyCreditAndDiscountObjectUsignComapnyId(Integer companyId);

	public JSONObject getCompanyDropDownForCredit_Discount(Integer organizationId);
	
	public List<CompanyCreditAndDiscount> getCompanyCreditAndDiscountList(Integer organizationId);
	
	public Boolean deleteCompanyCreditAndDiscountDetails(Integer companyCreditAndDiscountId);
	
	public CompanyCreditAndDiscount getCompanyCreditAndDiscountDetailsById(Integer creditAndDiscountId);
	
	public Integer updateCompanyCreditAndDiscount(Integer orgId, Integer companyId, Integer creditAndDiscountId,CompanyCreditAndDiscount companyCreditAndDiscount);

	public Map<String,Object>searchCreditAndDiscount(
			String searchOption, String searchText,Integer from, Integer organizationId);
}
