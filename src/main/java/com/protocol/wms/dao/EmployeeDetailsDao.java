/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.EmployeeDetailsAddressProofPath;
import com.protocol.wms.model.EmployeeDetailsImagePath;

/**
 * @author Sudhakar
 *
 */
public interface EmployeeDetailsDao {
	
	public Integer saveEmployeeDetails(EmployeeDetails employeeDetailsObj);
	
	public EmployeeDetails loadEmployeeObjectUsingEmployeeDetailsId(Integer empDetailsId);
	
	public void updateEmployeeDetails(EmployeeDetails empObject);
	
	public Boolean updateEmployeeDetails(Integer roleTypeId,Integer bloodroupNameId, EmployeeDetails employeeDetails);
	
	public List<EmployeeDetails> loadEmployeeDetailsListUsingOrganizationId(Integer organizationId);
	
	public Map<Object, Object> searchEmployeeDetails(String searchOption,String searchText,Integer organizationId, Integer from);
	
	public Boolean deleteEmployeeDetails(Integer empDetailsId);
	
	public List<EmployeeDetailsImagePath> updateEmployeeImages(Integer employeeDetailsId, List<EmployeeDetailsImagePath> employeeImgList); 
	
	public Boolean deleteEmployeeImage(Integer employeeImageId);
	
	public List<EmployeeDetailsAddressProofPath> updateEmployeeAddressProofImage(Integer employeeDetailsId, List<EmployeeDetailsAddressProofPath> employeeAddressProofPathList);
	
	public Boolean deleteEmployeeAddressProofImage(Integer employeeAddressProofImageId);
}
