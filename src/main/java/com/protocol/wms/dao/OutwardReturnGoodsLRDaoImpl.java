/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.OutWardReturnGoodsLRDetails;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsClaimImagePath;
import com.protocol.wms.model.OutWardReturnGoodsLRDetailsLrImagePath;
import com.protocol.wms.model.OutWardReturnGoodsRegistration;

/**
 * @author admin
 *
 */
@Repository(value="OutwardReturnGoodsLRDaoImpl")
public class OutwardReturnGoodsLRDaoImpl implements OutwardReturnGoodsLRDao {

	private Logger log = Logger.getLogger(OutwardReturnGoodsLRDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsLRDao#getOutWardReturnGoodsLRDetailsByLrId(java.lang.Integer)
	 */
	@Override
	public OutWardReturnGoodsLRDetails getOutWardReturnGoodsLRDetailsByLrId(
			Integer outWardReturnGoodsLRDetailsId) {
		OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardReturnGoodsLRDetails = (OutWardReturnGoodsLRDetails) session.get(OutWardReturnGoodsLRDetails.class, outWardReturnGoodsLRDetailsId);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardReturnGoodsLRDetails;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsLRDao#outWardReturnGoodsLRDetailsList(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardReturnGoodsLRDetails> outWardReturnGoodsLRDetailsList(Integer outWardReturnGoodsRegistrationId) {
		List<OutWardReturnGoodsLRDetails> lrList = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(OutWardReturnGoodsLRDetails.class,"LR_Details")
					.createAlias("LR_Details.outWardReturnGoodsRegistration", "outWardReturnGoodsRegistration")
					.add(Restrictions.eq("outWardReturnGoodsRegistration.outWardReturnGoodsRegistrationId", outWardReturnGoodsRegistrationId))
					.add(Restrictions.eq("status", 1));
			lrList = criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return lrList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsLRDao#deleteOutwardReturnGoodsRegLrDetails(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Boolean deleteOutwardReturnGoodsRegLrDetails(Integer LR_ID) {
		Session session =sessionFactory.getCurrentSession();
		OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails= null;
		List<OutWardReturnGoodsLRDetails>lrDetailsList = null;
		try{
			outWardReturnGoodsLRDetails = (OutWardReturnGoodsLRDetails)session.get(OutWardReturnGoodsLRDetails.class, LR_ID);
			outWardReturnGoodsLRDetails.setStatus(0);
			session.update(outWardReturnGoodsLRDetails);
			
			Criteria criteria = session.createCriteria(OutWardReturnGoodsLRDetails.class,"LRDetails")
				     .createAlias("LRDetails.outWardReturnGoodsRegistration", "outWardReturnGoodsRegistration")
				  .add(Restrictions.eq("outWardReturnGoodsRegistration.outWardReturnGoodsRegistrationId",outWardReturnGoodsLRDetails.getOutWardReturnGoodsRegistration().getOutWardReturnGoodsRegistrationId()))
				     .add(Restrictions.eq("status", 1));
			lrDetailsList = criteria.list();
			if(!(lrDetailsList.size()>0)){
				OutWardReturnGoodsRegistration tempObj = (OutWardReturnGoodsRegistration)session.load(OutWardReturnGoodsRegistration.class, outWardReturnGoodsLRDetails.getOutWardReturnGoodsRegistration().getOutWardReturnGoodsRegistrationId());
				tempObj.setStatus(0);
				session.update(tempObj);
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsLRDao#loadOutWardReturnGoodsLRDetailsByLrRegId(java.lang.Integer)
	 */
	@Override
	public OutWardReturnGoodsLRDetails loadOutWardReturnGoodsLRDetailsByLrRegId(
			Integer outWardReturnGoodsLRDetailsId) {
		OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails =null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardReturnGoodsLRDetails = (OutWardReturnGoodsLRDetails) session.get(OutWardReturnGoodsLRDetails.class, outWardReturnGoodsLRDetailsId);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardReturnGoodsLRDetails;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardReturnGoodsLRDao#editOutwardReturnGoodsRegLrDetails(com.protocol.wms.model.OutWardReturnGoodsLRDetails)
	 */
	@Override
	public Boolean editOutwardReturnGoodsRegLrDetails(OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.update(outWardReturnGoodsLRDetails);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<OutWardReturnGoodsLRDetailsLrImagePath> updateOutwardReturnGoodsLrRegLrImage(
			Integer outwardReturnGoodsLrRegistrationId,
			List<OutWardReturnGoodsLRDetailsLrImagePath> lrImgList) {
		OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails = null;
		List<OutWardReturnGoodsLRDetailsLrImagePath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardReturnGoodsLRDetails = (OutWardReturnGoodsLRDetails) session.load(OutWardReturnGoodsLRDetails.class, outwardReturnGoodsLrRegistrationId);
			for(OutWardReturnGoodsLRDetailsLrImagePath tempObj : lrImgList){
				tempObj.setOutWardReturnGoodsLRDetails(outWardReturnGoodsLRDetails);
				session.save(tempObj);
			}
			lst = outWardReturnGoodsLRDetails.getOutWardReturnGoodsLRDetailsLrImagePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}

	@Override
	public Boolean deleteOutwardReturnGoodsLrRegLrImage(Integer lrImageId) {
		OutWardReturnGoodsLRDetailsLrImagePath outWardReturnGoodsLRDetailsLrImagePath = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardReturnGoodsLRDetailsLrImagePath = (OutWardReturnGoodsLRDetailsLrImagePath) session.load(OutWardReturnGoodsLRDetailsLrImagePath.class, lrImageId);
			session.delete(outWardReturnGoodsLRDetailsLrImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<OutWardReturnGoodsLRDetailsClaimImagePath> updateOutwardReturnGoodsLrRegClaimCopyImage(
			Integer outwardReturnGoodsLrRegistrationId,
			List<OutWardReturnGoodsLRDetailsClaimImagePath> claimCopyImgList) {
		OutWardReturnGoodsLRDetails outWardReturnGoodsLRDetails = null;
		List<OutWardReturnGoodsLRDetailsClaimImagePath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardReturnGoodsLRDetails = (OutWardReturnGoodsLRDetails) session.load(OutWardReturnGoodsLRDetails.class, outwardReturnGoodsLrRegistrationId);
			for(OutWardReturnGoodsLRDetailsClaimImagePath tempObj : claimCopyImgList){
				tempObj.setOutWardReturnGoodsLRDetails(outWardReturnGoodsLRDetails);
				session.save(tempObj);
			}
			lst = outWardReturnGoodsLRDetails.getOutWardReturnGoodsLRDetailsClaimImagePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}

	@Override
	public Boolean deleteOutwardReturnGoodsLrRegClaimCopyImage(
			Integer claimCopyImageId) {
		OutWardReturnGoodsLRDetailsClaimImagePath outWardReturnGoodsLRDetailsClaimImagePath = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardReturnGoodsLRDetailsClaimImagePath = (OutWardReturnGoodsLRDetailsClaimImagePath) session.load(OutWardReturnGoodsLRDetailsClaimImagePath.class, claimCopyImageId);
			session.delete(outWardReturnGoodsLRDetailsClaimImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
}
