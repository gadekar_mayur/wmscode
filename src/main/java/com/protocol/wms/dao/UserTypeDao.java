/**
 * 
 */
package com.protocol.wms.dao;

import com.protocol.wms.model.UserType;

/**
 * @author Sudhakar
 *
 */
public interface UserTypeDao {
	
	public UserType loadUserTypeUsingUserTypeId(Integer userTypeId);

}
