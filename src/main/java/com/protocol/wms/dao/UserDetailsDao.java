/**
 * 
 */
package com.protocol.wms.dao;

import com.protocol.wms.model.UserDetails;

/**
 * @author admin
 *
 */
public interface UserDetailsDao {
	
	public UserDetails loginValidate(String uname,String password);
	
	public UserDetails checkUserNameAlreadyExitOrNot(String uname);
	
	public Boolean checkUserNameAlreadyExitOrNot(Integer userDetailsId,String uname);
	
	public Integer saveUserDetails(UserDetails userDetailsObj);
	
	public UserDetails loadUserDetailsUsingUserDetailsId(Integer userDetailsId);
	
	public void updateUserDetailsObj(UserDetails userDetailsObj);

}
