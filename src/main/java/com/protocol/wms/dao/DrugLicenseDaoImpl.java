package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.DrugLicence;

@Repository("DrugLicenseDaoImpl")
public class DrugLicenseDaoImpl implements DrugLicenseDao{

	private Logger log = Logger.getLogger(OrganizationBankDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
		
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	//Add By Nutan
	@SuppressWarnings("unchecked")
	@Override
	public List<DrugLicence> loadDrugLicenseListUsingStockistId(Integer stockistId) {Session session =sessionFactory.getCurrentSession();
	List<DrugLicence> StockistDrugNumberList = null;
	try 
	{
		Criteria criteria = session.createCriteria(DrugLicence.class,"DrugLicence")
				.createAlias("DrugLicence.stockist", "stockist")
				.add(Restrictions.eq("stockist.stockistId", stockistId));
		StockistDrugNumberList=criteria.list();
		//System.out.println("drugLicense");
	}
	catch(DataAccessException e)
	{
		e.printStackTrace();
		log.error(e);
	}
	catch (Exception e) 
	{
		e.printStackTrace();
		log.error(e);
	}
	return StockistDrugNumberList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.DrugLicenseDao#getDrugLicenceObjectByDrugLicenceId(java.lang.Integer)
	 */
	@Override
	public DrugLicence getDrugLicenceObjectByDrugLicenceId(Integer drugLicenceId) {
		Session session =sessionFactory.getCurrentSession();
		DrugLicence drugLicence = null;
		try 
		{
			drugLicence = (DrugLicence) session.get(DrugLicence.class, drugLicenceId);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return drugLicence;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.DrugLicenseDao#updateDrugLicenceDetails(com.protocol.wms.model.DrugLicence)
	 */
	@Override
	public Boolean updateDrugLicenceDetails(DrugLicence drugLicence) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(drugLicence);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
}
