/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyExecutive;
import com.protocol.wms.model.Organization;

/**
 * @author admin
 *
 */
@Repository(value="CompanyExecutiveDaoImpl")
public class CompanyExecutiveDaoImpl implements CompanyExecutiveDao {

	private Logger log = Logger.getLogger(CompanyExecutiveDaoImpl.class.getClass());

	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyExecutiveDao#saveExecutivesToCompany(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyExecutive)
	 */
	@Override
	public Boolean saveExecutivesToCompany(Integer orgId, Integer companyId,CompanyExecutive companyExecutive) {
		Organization organization  = null;
		Company company = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization) session.load(Organization.class,orgId);
			company = (Company) session.load(Company.class,companyId);
			companyExecutive.setOrganization(organization);
			companyExecutive.setCompany(company);
			int i = (Integer) session.save(companyExecutive);
			if(i>0)
				return true;
			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyExecutiveDao#getCompanyExecutiveObjById(java.lang.Integer)
	 */
	@Override
	public CompanyExecutive getCompanyExecutiveObjById(Integer companyExecutiveId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyExecutive companyExecutive = null;
		try{
			companyExecutive = (CompanyExecutive)session.get(CompanyExecutive.class,companyExecutiveId);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return companyExecutive;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyExecutiveDao#updateCompanyExecutivesDetails(com.protocol.wms.model.CompanyExecutive)
	 */
	@Override
	public Boolean updateCompanyExecutivesDetails(CompanyExecutive companyExecutive) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.update(companyExecutive);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyExecutiveDao#deleteCompanyExecutive(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCompanyExecutive(Integer companyExeId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyExecutive companyExecutive = null;
		try{
			companyExecutive = (CompanyExecutive)session.get(CompanyExecutive.class, companyExeId);
			companyExecutive.setStatus(0);
			session.update(companyExecutive);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyExecutiveDao#getCompanyExecutivesListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyExecutive> getCompanyExecutivesListing(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyExecutive> companyExecutiveList = null;
		try{
			Criteria criteria = session.createCriteria(CompanyExecutive.class,"companyExec")
					.createAlias("companyExec.organization", "organization")
					.add(Restrictions.eq("companyExec.status",1))
					.addOrder(Order.desc("companyExec.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			companyExecutiveList = criteria.list();
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return companyExecutiveList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyExecutiveDao#searchCompanyExecutiveDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> searchCompanyExecutiveDetails(String searchOption, String searchText,Integer from, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<CompanyExecutive> companyExecutiveList = null;
		Map<String,Object>map=null;
		
		try{
			Criteria criteria = session.createCriteria(CompanyExecutive.class,"companyExec")
				.createAlias("companyExec.organization", "organization")
				.createAlias("companyExec.company", "company")
				.add(Restrictions.eq("companyExec.status",1))
				.add(Restrictions.eq("organization.organizationStatus","1"))
				.add(Restrictions.eq("company.status",1))
				.addOrder(Order.desc("companyExec.submitDate"))
				.setProjection(Projections.rowCount());
			
		if(organizationId!=null)
			criteria.add(Restrictions.eq("organization.organizationId", organizationId));
		
		if("Organization".equals(searchOption))
			criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
		else if("Company".equals(searchOption))
			criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
		else if("ExecName".equals(searchOption))
			criteria.add(Restrictions.like("companyExec.executiveName", searchText,MatchMode.START));
		else if("Designation".equals(searchOption))
			criteria.add(Restrictions.like("companyExec.designation", searchText,MatchMode.START));
		/*else if("Mobile".equals(searchOption))
			criteria.add(Restrictions.eq("companyExec.mobileNo", Long.parseLong(searchText)));*/
		else if("Email".equals(searchOption))
			criteria.add(Restrictions.like("companyExec.emailId", searchText,MatchMode.START));
			
		Long totalRecordCount = (Long)criteria.uniqueResult();
			System.out.println("totalRecordCount : "+totalRecordCount);
			 criteria = session.createCriteria(CompanyExecutive.class,"companyExec")
					.createAlias("companyExec.organization", "organization")
					.createAlias("companyExec.company", "company")
					.add(Restrictions.eq("companyExec.status",1))
					.add(Restrictions.eq("organization.organizationStatus","1"))
					.add(Restrictions.eq("company.status",1))
					.addOrder(Order.desc("companyExec.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("ExecName".equals(searchOption))
				criteria.add(Restrictions.like("companyExec.executiveName", searchText,MatchMode.START));
			else if("Designation".equals(searchOption))
				criteria.add(Restrictions.like("companyExec.designation", searchText,MatchMode.START));
			/*else if("Mobile".equals(searchOption))
				criteria.add(Restrictions.eq("companyExec.mobileNo", Long.parseLong(searchText)));*/
			else if("Email".equals(searchOption))
				criteria.add(Restrictions.like("companyExec.emailId", searchText,MatchMode.START));
			
			from = from - 1 ;
		    criteria.setFirstResult(from*Constant.PAGE_COUNT);
		    criteria.setMaxResults(Constant.PAGE_COUNT);
			companyExecutiveList = criteria.list();
			
			Integer totalPage = 0;
			if(totalRecordCount>0){
				totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPage = totalPage +1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("companyExecutive", companyExecutiveList);
			map.put("totalPages", totalPage );
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

}
