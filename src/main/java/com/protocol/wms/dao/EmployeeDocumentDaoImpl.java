/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.EmployeeDocument;
import com.protocol.wms.model.EmployeeDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
@Repository(value="EmployeeDocumentDaoImpl")
public class EmployeeDocumentDaoImpl implements EmployeeDocumentDao{
	
	private Logger log = Logger.getLogger(EmployeeDocumentDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDocumentDao#saveEmployeeDocument(com.protocol.wms.model.EmployeeDocument)
	 */
	@Override
	public Integer saveEmployeeDocument(EmployeeDocument employeeDocumentObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer employeeDocumentId = null;
		try 
		{
			employeeDocumentId=(Integer) session.save(employeeDocumentObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return employeeDocumentId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDocumentDao#loadEmployeeDocumetListUsingOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<EmployeeDocument> loadEmployeeDocumetListUsingOrganizationId(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<EmployeeDocument> employeeDocumentsList=null;
		try 
		{
			Criteria criteria = session.createCriteria(EmployeeDocument.class,"EmployeeDocument")
				     .createAlias("EmployeeDocument.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   employeeDocumentsList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return employeeDocumentsList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDocumentDao#searchEmployeeDocumet(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> searchEmployeeDocumet(String searchOption,
			String searchText, Integer from, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<EmployeeDocument> employeeDocumentsList=null;
		Map<String,Object>map=null;
		
		try 
		{
			Criteria criteria = session.createCriteria(EmployeeDocument.class,"EmployeeDocument")
				     .createAlias("EmployeeDocument.organization", "organization")
				     .createAlias("EmployeeDocument.employeeDetails", "employeeDetails")
				     .createAlias("EmployeeDocument.documentListType", "documentListType")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("employeeDetails.status", 1))
				     .add(Restrictions.eq("organization.organizationStatus", "1"))
				     .setProjection(Projections.rowCount());
			
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   if("Organization".equals(searchOption)){
					   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));}
				   else if("EmployeeName".equals(searchOption)){
					   criteria.add(Restrictions.like("employeeDetails.name", searchText,MatchMode.START));}
				   else if("DocumentType".equals(searchOption)){
					   criteria.add(Restrictions.like("documentListType.documentName", searchText,MatchMode.START));}
				   
				   Long totalRecordCount = (Long)criteria.uniqueResult();
			
			 criteria = session.createCriteria(EmployeeDocument.class,"EmployeeDocument")
				     .createAlias("EmployeeDocument.organization", "organization")
				     .createAlias("EmployeeDocument.employeeDetails", "employeeDetails")
				     .createAlias("EmployeeDocument.documentListType", "documentListType")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("employeeDetails.status", 1))
				     .add(Restrictions.eq("organization.organizationStatus", "1"))
				     .addOrder(Order.desc("submitDate"));
			
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   if("Organization".equals(searchOption))
				   {
					   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				   }
				   else if("EmployeeName".equals(searchOption)){
					   criteria.add(Restrictions.like("employeeDetails.name", searchText,MatchMode.START));}
				   else if("DocumentType".equals(searchOption)){
					   criteria.add(Restrictions.like("documentListType.documentName", searchText,MatchMode.START));}
				   
				   from = from - 1 ;
				    criteria.setFirstResult(from*Constant.PAGE_COUNT);
				    criteria.setMaxResults(Constant.PAGE_COUNT);
				   employeeDocumentsList=criteria.list();
				   
				   Integer totalPage = 0;
					if(totalRecordCount>0){
						totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
						if((totalRecordCount%Constant.PAGE_COUNT)!=0){
							totalPage = totalPage +1;
						}
					}
					map = new HashMap<String, Object>();
					map.put("employeeDocument", employeeDocumentsList);
					map.put("totalPages", totalPage );
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDocumentDao#deleteEmployeeDocumet(java.lang.Integer)
	 */
	@Override
	public Boolean deleteEmployeeDocumet(Integer empDocumentId) {
		Session session =sessionFactory.getCurrentSession();
		EmployeeDocument employeeDocument = null;
		try{
			employeeDocument = (EmployeeDocument)session.get(EmployeeDocument.class, empDocumentId);
			employeeDocument.setStatus(0);
			session.update(employeeDocument);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public EmployeeDocument loadEmployeeDocumentObjById(Integer employeeDocumentId) {
		Session session =sessionFactory.getCurrentSession();
		EmployeeDocument employeeDocument = null;
		try{
			employeeDocument = (EmployeeDocument)session.get(EmployeeDocument.class, employeeDocumentId);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return employeeDocument;
	}

	@Override
	public Boolean updateEmployeeDocumentObj(EmployeeDocument employeeDocument) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.update(employeeDocument);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<EmployeeDocumentImagePath> updateEmployeeImages(Integer employeeDocumentId, List<EmployeeDocumentImagePath> documentList) {
		Session session =sessionFactory.getCurrentSession();
		EmployeeDocument employeeDocument = null;
		try{
			employeeDocument = (EmployeeDocument) session.load(EmployeeDocument.class, employeeDocumentId);
			for(EmployeeDocumentImagePath tempObj : documentList){
				tempObj.setEmployeeDocument(employeeDocument);
				session.save(tempObj);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return employeeDocument.getEmployeeDocumentImagePathList();
	}

	@Override
	public Boolean deleteEmployeeUploadDocumentFileImage(Integer employeeDocumentPathId) {
		EmployeeDocumentImagePath employeeDocumentImagePath = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			employeeDocumentImagePath = (EmployeeDocumentImagePath) session.load(EmployeeDocumentImagePath.class, employeeDocumentPathId);
			session.delete(employeeDocumentImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
}
