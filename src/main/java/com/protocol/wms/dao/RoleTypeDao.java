/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.RoleType;

/**
 * @author Sudhakar
 *
 */
public interface RoleTypeDao {
	
	public List<RoleType> loadRoleTypeList();
	
	public RoleType loadRoleTypeUsingRoleTypeId(Integer roleTypeId);

}
