package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.InvoicePaymentInformation;
import com.protocol.wms.model.OrderMode;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.RemainingChequeAmount;
import com.protocol.wms.model.Stockist;
@Repository(value="UploadDocumentDaoImpl")
public class UploadDocumentDaoImpl implements UploadDocumentDao{

private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	 @Transactional
	@Override
	public Integer saveInvReport(List<String> order_No,List<String> Order_Date, List<String> inv_No, List<String> inv_Date,
			List<String> inv_Amt,  List<String> stockist_Name, List<String> stockist_City,
			List<String> company_Name) {
		System.out.println("Order no-"+order_No.toString());
		System.out.println("Order no-"+Order_Date.toString());
		System.out.println("Invoice no-"+inv_No.toString());
		System.out.println("Invoice Date-"+inv_Date.toString());
		System.out.println("Invoice Amount-"+inv_Amt.toString());
		System.out.println("Stockist Name-"+stockist_Name.toString());
		System.out.println("Stockist_City-"+stockist_City.toString());
		System.out.println("Company_Name-"+company_Name.toString());
		Session session=sessionFactory.getCurrentSession();
		
		OutWardOrderEntryRegistration outwardOrderEntryRegistrationObj;
		OutWardOrderEntryRegistration outwardOrderEntryRegistrationObjOrderCheck;
		OutWardOrderInvoiceEnteryRegistration outwardOrderInvoiceEntryRegistrationObj;
		Organization organizationObj;
		OrderMode OrderModeObj=null;
		DateFormat dtf = new SimpleDateFormat("dd.MM.yyyy");
		Company companyObj=null;
		Stockist stockistObj=null;
		Integer outWardOrderEntryRegistrationId=null;
		Integer outWardOrderEntryId=null;
		Integer noOfDays=0;
		int status;
		try{
		for(int i=0;i<order_No.size();i++)
		{
			//----------check whether order number is present in db---------------------------//
			Criteria criteria_check_companyname=session.createCriteria(Company.class).add(Restrictions.eq("companyName",company_Name.get(i).toString()));
			Company company2=(Company) criteria_check_companyname.uniqueResult();
			Criteria order_count_check_criteria=session.createCriteria(OutWardOrderEntryRegistration.class,"OutWardOrderentryRegistration")
					.createAlias("OutWardOrderentryRegistration.company","company")
					.add(Restrictions.eq("orderNo",order_No.get(i)))
					.add(Restrictions.eq("company.companyId",company2.getCompanyId()))
					.setProjection(Projections.rowCount());
			Long count=(Long) order_count_check_criteria.uniqueResult();
			
		/*	Query query=session.createQuery("select count(*) from outwardorderentryregistration oer where oer.orderName=:orderName and oer.companyId=:companyId");
			query.setString("orderName",order_No.get(i));
			query.setString("companyId",company2.getCompanyId());*/
			
			
			
			
			/////if order number already present
			
			
			
			if(count>0)
			{	outwardOrderEntryRegistrationObjOrderCheck=new OutWardOrderEntryRegistration();
				System.out.println("company Id-"+company2.getCompanyId().toString());
				Criteria order_check_criteria=session.createCriteria(OutWardOrderEntryRegistration.class,"OutWardOrderentryRegistration")
						.createAlias("OutWardOrderentryRegistration.company","company")
						.add(Restrictions.eq("orderNo",order_No.get(i)))
						.add(Restrictions.eq("company.companyId",company2.getCompanyId()));
						outwardOrderEntryRegistrationObjOrderCheck=(OutWardOrderEntryRegistration) order_check_criteria.uniqueResult();
				
				
				status=0;
				
				organizationObj=(Organization)session.get(Organization.class,5);
				outWardOrderEntryId=outwardOrderEntryRegistrationObjOrderCheck.getOutWardOrderEnteryRegistrationId();
				
				String stockistLocation=outwardOrderEntryRegistrationObjOrderCheck.getStockist().getStockistLocation();
				if(stockistLocation.equalsIgnoreCase("Local"))
				{
					CompanyCreditAndDiscount companyCreditAndDiscountObj=new CompanyCreditAndDiscount();
					Criteria criteria_localDays = session.createCriteria(CompanyCreditAndDiscount.class,"CompanyCreditAndDiscount")
							.createAlias("CompanyCreditAndDiscount.company", "company")
							.add(Restrictions.eq("company.companyId", outwardOrderEntryRegistrationObjOrderCheck.getCompany().getCompanyId()))
							.add(Restrictions.eq("status", 1));
					companyCreditAndDiscountObj=(CompanyCreditAndDiscount) criteria_localDays.uniqueResult();
					//companyCreditAndDiscountObj=(CompanyCreditAndDiscount) session.get(CompanyCreditAndDiscount.class,outwardOrderEntryRegistration.getCompany().getCompanyId());
					noOfDays=companyCreditAndDiscountObj.getLocalDays();
				}
				else
				{
					CompanyCreditAndDiscount companyCreditAndDiscountObj=new CompanyCreditAndDiscount();
					Criteria criteria_outStationsDays = session.createCriteria(CompanyCreditAndDiscount.class,"CompanyCreditAndDiscount")
							.createAlias("CompanyCreditAndDiscount.company", "company")
							.add(Restrictions.eq("company.companyId", outwardOrderEntryRegistrationObjOrderCheck.getCompany().getCompanyId()))
							.add(Restrictions.eq("status", 1));
					companyCreditAndDiscountObj=(CompanyCreditAndDiscount) criteria_outStationsDays.uniqueResult();
		//			companyCreditAndDiscountObj=(CompanyCreditAndDiscount) session.get(CompanyCreditAndDiscount.class,outwardOrderEntryRegistration.getCompany().getCompanyId());
					noOfDays=companyCreditAndDiscountObj.getOutsideStationDays();
				}
				
				//==============================================//
				
				///===============================Remaing payment===============================//
			
				Double invoiceAmount;
				Double remainingInvoiceAmount;
				Integer outWardOrderInvoiceEnteryRegistrationId = null;
				List<RemainingChequeAmount> remainingCheckAmount;
				Criteria createria_Remaining_Cheque_Amt=session.createCriteria(RemainingChequeAmount.class,"RemainingChequeAmount")
						.createAlias("RemainingChequeAmount.organization","organization")
						.createAlias("RemainingChequeAmount.stockist","stockist")
						.createAlias("RemainingChequeAmount.company","company")
						.add(Restrictions.eq("organization.organizationId",outwardOrderEntryRegistrationObjOrderCheck.getOrganization().getOrganizationId()))
						.add(Restrictions.eq("stockist.stockistId",outwardOrderEntryRegistrationObjOrderCheck.getStockist().getStockistId()))
						.add(Restrictions.eq("status", 1))
						.add(Restrictions.eq("company.companyId",outwardOrderEntryRegistrationObjOrderCheck.getCompany().getCompanyId()));
				remainingCheckAmount=createria_Remaining_Cheque_Amt.list();
				if(remainingCheckAmount.size()>0)
				{
					invoiceAmount=Double.parseDouble(inv_Amt.get(i));
					int flag=0;
					
					
					////////===============Submit Date============================//
					java.util.Date today = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
					String IST = df.format(today);
					DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					java.util.Date date = df2.parse(IST);
					
					for(RemainingChequeAmount obj:remainingCheckAmount)
					{
						ChequeNumberInfo chequeNumberInfo=(ChequeNumberInfo) session.get(ChequeNumberInfo.class,obj.getChequeNumberInfo().getChequeNumberInfoId());
						
						Double chequeAmount=obj.getRemainingChequeAmount();
						if(invoiceAmount<=chequeAmount)
						{
							chequeAmount=chequeAmount-invoiceAmount;
							if(flag==0)
							{
								outwardOrderInvoiceEntryRegistrationObj=new OutWardOrderInvoiceEnteryRegistration();
								outwardOrderInvoiceEntryRegistrationObj.setInvoiceNo(inv_No.get(i));
								java.util.Date d2 = dtf.parse(inv_Date.get(i).toString());
								outwardOrderInvoiceEntryRegistrationObj.setInvoiceDate(new java.sql.Date(d2.getTime()));
								outwardOrderInvoiceEntryRegistrationObj.setNetAmount(Double.parseDouble(inv_Amt.get(i)));
								outwardOrderInvoiceEntryRegistrationObj.setDispatchEnteryRegistrationStatus(0);
								outwardOrderInvoiceEntryRegistrationObj.setSubmitDate(date);
								outwardOrderInvoiceEntryRegistrationObj.setPaymentStatus(1);
								outwardOrderInvoiceEntryRegistrationObj.setRemainingPayment(0.0);
                                outwardOrderInvoiceEntryRegistrationObj.setStatus(1);
                                outwardOrderInvoiceEntryRegistrationObj.setDiscountIsTakenOrNot("No");
                                outwardOrderInvoiceEntryRegistrationObj.setNoOfDays(noOfDays);
                                outwardOrderInvoiceEntryRegistrationObj.setFirstDate(new java.sql.Date(d2.getTime()));
                                outwardOrderInvoiceEntryRegistrationObj.setOutWardOrderEnteryRegistration(outwardOrderEntryRegistrationObjOrderCheck);
                                ////date Calculation
                                java.util.Date str_invoiceDate = dtf
										.parse(inv_Date.get(i));
                                Calendar cal = Calendar.getInstance();
								cal.setTime(str_invoiceDate);
								cal.add(Calendar.DATE, noOfDays); 
								java.sql.Date afterDate = new java.sql.Date(
										cal.getTimeInMillis());
								outwardOrderInvoiceEntryRegistrationObj.setLastDate(afterDate);
								outwardOrderInvoiceEntryRegistrationObj.setOrganization(organizationObj);
								outWardOrderInvoiceEnteryRegistrationId=(Integer) session.save(outwardOrderInvoiceEntryRegistrationObj);
							flag=1;
							
							}
							if(outWardOrderInvoiceEnteryRegistrationId!=null && flag ==1)
							{
								
						/////////Invoice payment info //////////////////////////////////		
								InvoicePaymentInformation invoicePaymentInformationObj = new InvoicePaymentInformation();
								invoicePaymentInformationObj.setSubmitDate(date);
								invoicePaymentInformationObj.setPaidAmount(Double.parseDouble(inv_Amt.get(i)));
								invoicePaymentInformationObj.setChequeNumberInfo(chequeNumberInfo);
								OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObjInvoicePaymentInfo=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class, outWardOrderInvoiceEnteryRegistrationId);
								invoicePaymentInformationObj.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObjInvoicePaymentInfo);
								Integer invoicePaymentInformationId=(Integer) session.save(invoicePaymentInformationObj);
								
								if(invoicePaymentInformationId!=null)
								{
								///////////////Update remaining cheque Amount////////////
									RemainingChequeAmount remainingChequeAmountObj=(RemainingChequeAmount) session.get(RemainingChequeAmount.class,obj.getRemainingChequeAmountId());
									remainingChequeAmountObj.setRemainingChequeAmount(chequeAmount);
									if(chequeAmount.equals(0.0))
									{
										remainingChequeAmountObj.setStatus(0);
									}
									
									session.update(remainingChequeAmountObj);
								}
								invoiceAmount = 0.0;
								if(invoiceAmount.equals(0.0))
								{
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEntryRegistrationUpdateInvAmt=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class,outWardOrderInvoiceEnteryRegistrationId);
									outWardOrderInvoiceEntryRegistrationUpdateInvAmt.setRemainingPayment(0.0);
									outWardOrderInvoiceEntryRegistrationUpdateInvAmt.setPaymentStatus(1);
									session.update(	outWardOrderInvoiceEntryRegistrationUpdateInvAmt);
								
								}
							}
							break;
						}
						else
						{
							remainingInvoiceAmount=invoiceAmount-chequeAmount;
							Double invAmt=Double.parseDouble(inv_Amt.get(i));
							if(invAmt==invoiceAmount)
							{
								invoiceAmount = remainingInvoiceAmount;
								if(flag==0)
								{
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = new OutWardOrderInvoiceEnteryRegistration();
									outWardOrderInvoiceEnteryRegistrationObj.setDispatchEnteryRegistrationStatus(0);
									outWardOrderInvoiceEnteryRegistrationObj.setInvoiceNo(inv_No.get(i));
									java.util.Date d2 = dtf.parse(inv_Date.get(i).toString());
									outWardOrderInvoiceEnteryRegistrationObj.setInvoiceDate(new java.sql.Date(d2.getTime()));
									outWardOrderInvoiceEnteryRegistrationObj.setNetAmount(Double.parseDouble(inv_Amt.get(i)));
									outWardOrderInvoiceEnteryRegistrationObj.setDispatchEnteryRegistrationStatus(0);
									outWardOrderInvoiceEnteryRegistrationObj.setSubmitDate(date);
									outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(1);
									outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(0.0);
									outWardOrderInvoiceEnteryRegistrationObj.setStatus(1);
									outWardOrderInvoiceEnteryRegistrationObj.setDiscountIsTakenOrNot("No");
									outWardOrderInvoiceEnteryRegistrationObj.setNoOfDays(noOfDays);
									outWardOrderInvoiceEnteryRegistrationObj.setFirstDate(new java.sql.Date(d2.getTime()));
									outWardOrderInvoiceEnteryRegistrationObj.setOutWardOrderEnteryRegistration(outwardOrderEntryRegistrationObjOrderCheck);
	                                ////date Calculation
	                                java.util.Date str_invoiceDate = dtf
											.parse(inv_Date.get(i));
	                                Calendar cal = Calendar.getInstance();
									cal.setTime(str_invoiceDate);
									cal.add(Calendar.DATE, noOfDays); 
									java.sql.Date afterDate = new java.sql.Date(
											cal.getTimeInMillis());
									outWardOrderInvoiceEnteryRegistrationObj.setLastDate(afterDate);
									outWardOrderInvoiceEnteryRegistrationObj.setOrganization(organizationObj);
									outWardOrderInvoiceEnteryRegistrationId=(Integer) session.save(outWardOrderInvoiceEnteryRegistrationObj);
									flag=1;
								}
								if(outWardOrderInvoiceEnteryRegistrationId!=null && flag ==1)
								{
									
							/////////Invoice payment info //////////////////////////////////		
									InvoicePaymentInformation invoicePaymentInformationObj = new InvoicePaymentInformation();
									invoicePaymentInformationObj.setSubmitDate(date);
									invoicePaymentInformationObj.setPaidAmount(Double.parseDouble(inv_Amt.get(i)));
									invoicePaymentInformationObj.setChequeNumberInfo(chequeNumberInfo);
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObjInvoicePaymentInfo=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class, outWardOrderInvoiceEnteryRegistrationId);
									invoicePaymentInformationObj.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObjInvoicePaymentInfo);
									Integer invoicePaymentInformationId=(Integer) session.save(invoicePaymentInformationObj);
									
									if(invoicePaymentInformationId!=null)
									{
									///////////////Update remaining cheque Amount////////////
										RemainingChequeAmount remainingChequeAmountObj=(RemainingChequeAmount) session.get(RemainingChequeAmount.class,obj.getRemainingChequeAmountId());
										remainingChequeAmountObj.setRemainingChequeAmount(chequeAmount);
										if(chequeAmount.equals(0.0))
										{
											remainingChequeAmountObj.setStatus(0);
										}
										
										session.update(remainingChequeAmountObj);
									}
									invoiceAmount = 0.0;
									if(invoiceAmount.equals(0.0))
									{
										OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEntryRegistrationUpdateInvAmt=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class,outWardOrderInvoiceEnteryRegistrationId);
										outWardOrderInvoiceEntryRegistrationUpdateInvAmt.setRemainingPayment(0.0);
										outWardOrderInvoiceEntryRegistrationUpdateInvAmt.setPaymentStatus(1);
										session.update(	outWardOrderInvoiceEntryRegistrationUpdateInvAmt);
									
									}
								}
								
							}
							else
							{
								remainingInvoiceAmount = invoiceAmount
										- chequeAmount;
								invoiceAmount = remainingInvoiceAmount;
								OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class,outWardOrderInvoiceEnteryRegistrationId);
								
								InvoicePaymentInformation invoicePaymentInformationObj = new InvoicePaymentInformation();
								
								invoicePaymentInformationObj
										.setSubmitDate(date);
								invoicePaymentInformationObj
										.setPaidAmount(chequeAmount);
								// setOutWardOrderInvoiceEnteryRegistration
								invoicePaymentInformationObj
										.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObj);
								// set chequeNumberInfoObj
								invoicePaymentInformationObj
										.setChequeNumberInfo(chequeNumberInfo);
								
								Integer invoicePaymentInformationId=(Integer) session.save(invoicePaymentInformationObj);
								if (invoicePaymentInformationId != null) {
									// update into data base and status zero
									RemainingChequeAmount remainingChequeAmountObj = (RemainingChequeAmount) session.get(RemainingChequeAmount.class,obj.getRemainingChequeAmountId());
									remainingChequeAmountObj
											.setRemainingChequeAmount(0.0);
									remainingChequeAmountObj.setStatus(0);
									
									session.update(remainingChequeAmountObj);

									
									outWardOrderInvoiceEnteryRegistrationObj
											.setRemainingPayment(remainingInvoiceAmount);
									
									session.update(outWardOrderInvoiceEnteryRegistrationObj);
								}
							}
						}
					}
				}
				else
				{
					
					java.util.Date today = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
					String IST = df.format(today);
					DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					java.util.Date date = df2.parse(IST);
					OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEntryReg=new OutWardOrderInvoiceEnteryRegistration();
					outWardOrderInvoiceEntryReg.setDispatchEnteryRegistrationStatus(0);
				//	java.util.Date str_invoiceDate = dtf.parse(inv_Date.get(i));
					java.util.Date d2 = dtf.parse(inv_Date.get(i).toString());

					outWardOrderInvoiceEntryReg.setInvoiceDate(new java.sql.Date(d2.getTime()));
					outWardOrderInvoiceEntryReg.setInvoiceNo(inv_No.get(i));
					outWardOrderInvoiceEntryReg.setNetAmount(Double.parseDouble(inv_Amt.get(i)));
					outWardOrderInvoiceEntryReg.setDispatchEnteryRegistrationStatus(0);
					outWardOrderInvoiceEntryReg.setSubmitDate(date);
					outWardOrderInvoiceEntryReg.setPaymentStatus(1);
					outWardOrderInvoiceEntryReg.setRemainingPayment(0.0);
					outWardOrderInvoiceEntryReg.setStatus(1);
					outWardOrderInvoiceEntryReg.setDiscountIsTakenOrNot("No");
					outWardOrderInvoiceEntryReg.setNoOfDays(noOfDays);
					outWardOrderInvoiceEntryReg.setFirstDate(new java.sql.Date(d2.getTime()));
					outWardOrderInvoiceEntryReg.setOutWardOrderEnteryRegistration(outwardOrderEntryRegistrationObjOrderCheck);
                    ////date Calculation
                    java.util.Date str_invoiceDate = dtf
							.parse(inv_Date.get(i));
                    Calendar cal = Calendar.getInstance();
					cal.setTime(str_invoiceDate);
					cal.add(Calendar.DATE, noOfDays); 
					java.sql.Date afterDate = new java.sql.Date(
							cal.getTimeInMillis());
					outWardOrderInvoiceEntryReg.setLastDate(afterDate);
					outWardOrderInvoiceEntryReg.setOrganization(organizationObj);
					outWardOrderInvoiceEnteryRegistrationId=(Integer) session.save(outWardOrderInvoiceEntryReg);
				}
				outwardOrderEntryRegistrationObjOrderCheck.setInvoiceStatus(1);
				session.update(outwardOrderEntryRegistrationObjOrderCheck);
}
			else{
				
				status=1;
				outwardOrderEntryRegistrationObj=new OutWardOrderEntryRegistration();
			outwardOrderEntryRegistrationObj.setOrderNo(order_No.get(i).toString());
			java.util.Date d1 = dtf.parse(Order_Date.get(i));
			outwardOrderEntryRegistrationObj.setOrderDate(new java.sql.Date(d1.getTime()));
			Criteria criteria=session.createCriteria(Company.class).add(Restrictions.eq("companyName",company_Name.get(i).toString()));
			Company company1=(Company) criteria.uniqueResult();
			
			System.out.println("company_id-"+company1.getCompanyId());
			
			
			Criteria criteria_stockist=session.createCriteria(Stockist.class).add(Restrictions.eq("stockistName",stockist_Name.get(i).toString()));
		Stockist stockist=(Stockist) criteria_stockist.uniqueResult();
		
		organizationObj=(Organization)session.get(Organization.class,5);
		OrderModeObj=(OrderMode)session.get(OrderMode.class,2 );
		outwardOrderEntryRegistrationObj.setSubmitDate(CommonMethods.setSubmitedDate());
			outwardOrderEntryRegistrationObj.setInvoiceStatus(0);
			outwardOrderEntryRegistrationObj.setProductStatus(0);
			outwardOrderEntryRegistrationObj.setStatus(1);
			outwardOrderEntryRegistrationObj.setCompany(company1);
			outwardOrderEntryRegistrationObj.setStockist(stockist);
			outwardOrderEntryRegistrationObj.setCompanyIndex(Integer.parseInt(order_No.get(i).toString()));
			outwardOrderEntryRegistrationObj.setOrganization(organizationObj);
			outwardOrderEntryRegistrationObj.setOrderMode(OrderModeObj);
			outWardOrderEntryRegistrationId=(Integer) session.save(outwardOrderEntryRegistrationObj);
			System.out.println("generated id-"+outWardOrderEntryRegistrationId);
			if(outWardOrderEntryRegistrationId !=null)
			{
				OutWardOrderEntryRegistration outwardOrderEntryRegistration=(OutWardOrderEntryRegistration)session.get(OutWardOrderEntryRegistration.class,outWardOrderEntryRegistrationId);
				if (company1.getCompanyName().length() > 0) {
					outwardOrderEntryRegistration.setOrderId(company1
							.getCompanyName().substring(0, 2)
							+ outWardOrderEntryRegistrationId);
					outwardOrderEntryRegistration
							.setCompanyIndex(Integer.parseInt(order_No.get(i).toString()));
					session.update(outwardOrderEntryRegistration);
						}
				
				///=======================Check stockistlocation==================///////////////
				String stockistLocation=outwardOrderEntryRegistration.getStockist().getStockistLocation();
				if(stockistLocation.equalsIgnoreCase("Local"))
				{
					CompanyCreditAndDiscount companyCreditAndDiscountObj=new CompanyCreditAndDiscount();
					Criteria criteria_localDays = session.createCriteria(CompanyCreditAndDiscount.class,"CompanyCreditAndDiscount")
							.createAlias("CompanyCreditAndDiscount.company", "company")
							.add(Restrictions.eq("company.companyId", outwardOrderEntryRegistration.getCompany().getCompanyId()))
							.add(Restrictions.eq("status", 1));
					companyCreditAndDiscountObj=(CompanyCreditAndDiscount) criteria_localDays.uniqueResult();
					//companyCreditAndDiscountObj=(CompanyCreditAndDiscount) session.get(CompanyCreditAndDiscount.class,outwardOrderEntryRegistration.getCompany().getCompanyId());
					noOfDays=companyCreditAndDiscountObj.getLocalDays();
				}
				else
				{
					CompanyCreditAndDiscount companyCreditAndDiscountObj=new CompanyCreditAndDiscount();
					Criteria criteria_outStationsDays = session.createCriteria(CompanyCreditAndDiscount.class,"CompanyCreditAndDiscount")
							.createAlias("CompanyCreditAndDiscount.company", "company")
							.add(Restrictions.eq("company.companyId", outwardOrderEntryRegistration.getCompany().getCompanyId()))
							.add(Restrictions.eq("status", 1));
					companyCreditAndDiscountObj=(CompanyCreditAndDiscount) criteria_outStationsDays.uniqueResult();
		//			companyCreditAndDiscountObj=(CompanyCreditAndDiscount) session.get(CompanyCreditAndDiscount.class,outwardOrderEntryRegistration.getCompany().getCompanyId());
					noOfDays=companyCreditAndDiscountObj.getOutsideStationDays();
				}
				
				//==============================================//
				
				///===============================Remaing payment===============================//
			
				Double invoiceAmount;
				Double remainingInvoiceAmount;
				Integer outWardOrderInvoiceEnteryRegistrationId = null;
				List<RemainingChequeAmount> remainingCheckAmount;
				Criteria createria_Remaining_Cheque_Amt=session.createCriteria(RemainingChequeAmount.class,"RemainingChequeAmount")
						.createAlias("RemainingChequeAmount.organization","organization")
						.createAlias("RemainingChequeAmount.stockist","stockist")
						.createAlias("RemainingChequeAmount.company","company")
						.add(Restrictions.eq("organization.organizationId",outwardOrderEntryRegistration.getOrganization().getOrganizationId()))
						.add(Restrictions.eq("stockist.stockistId",outwardOrderEntryRegistration.getStockist().getStockistId()))
						.add(Restrictions.eq("status", 1))
						.add(Restrictions.eq("company.companyId",outwardOrderEntryRegistration.getCompany().getCompanyId()));
				remainingCheckAmount=createria_Remaining_Cheque_Amt.list();
				if(remainingCheckAmount.size()>0)
				{
					invoiceAmount=Double.parseDouble(inv_Amt.get(i));
					int flag=0;
					
					
					////////===============Submit Date============================//
					java.util.Date today = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
					String IST = df.format(today);
					DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					java.util.Date date = df2.parse(IST);
					
					for(RemainingChequeAmount obj:remainingCheckAmount)
					{
						ChequeNumberInfo chequeNumberInfo=(ChequeNumberInfo) session.get(ChequeNumberInfo.class,obj.getChequeNumberInfo().getChequeNumberInfoId());
						
						Double chequeAmount=obj.getRemainingChequeAmount();
						if(invoiceAmount<=chequeAmount)
						{
							chequeAmount=chequeAmount-invoiceAmount;
							if(flag==0)
							{
								outwardOrderInvoiceEntryRegistrationObj=new OutWardOrderInvoiceEnteryRegistration();
								outwardOrderInvoiceEntryRegistrationObj.setInvoiceNo(inv_No.get(i));
								java.util.Date d2 = dtf.parse(inv_Date.get(i).toString());
								outwardOrderInvoiceEntryRegistrationObj.setInvoiceDate(new java.sql.Date(d2.getTime()));
								outwardOrderInvoiceEntryRegistrationObj.setNetAmount(Double.parseDouble(inv_Amt.get(i)));
								outwardOrderInvoiceEntryRegistrationObj.setDispatchEnteryRegistrationStatus(0);
								outwardOrderInvoiceEntryRegistrationObj.setSubmitDate(date);
								outwardOrderInvoiceEntryRegistrationObj.setPaymentStatus(1);
								outwardOrderInvoiceEntryRegistrationObj.setRemainingPayment(0.0);
                                outwardOrderInvoiceEntryRegistrationObj.setStatus(1);
                                outwardOrderInvoiceEntryRegistrationObj.setDiscountIsTakenOrNot("No");
                                outwardOrderInvoiceEntryRegistrationObj.setNoOfDays(noOfDays);
                                outwardOrderInvoiceEntryRegistrationObj.setFirstDate(new java.sql.Date(d2.getTime()));
                                outwardOrderInvoiceEntryRegistrationObj.setOutWardOrderEnteryRegistration(outwardOrderEntryRegistration);
                                ////date Calculation
                                java.util.Date str_invoiceDate = dtf
										.parse(inv_Date.get(i));
                                Calendar cal = Calendar.getInstance();
								cal.setTime(str_invoiceDate);
								cal.add(Calendar.DATE, noOfDays); 
								java.sql.Date afterDate = new java.sql.Date(
										cal.getTimeInMillis());
								outwardOrderInvoiceEntryRegistrationObj.setLastDate(afterDate);
								outwardOrderInvoiceEntryRegistrationObj.setOrganization(organizationObj);
								outWardOrderInvoiceEnteryRegistrationId=(Integer) session.save(outwardOrderEntryRegistration);
							flag=1;
							
							}
							if(outWardOrderInvoiceEnteryRegistrationId!=null && flag ==1)
							{
								
						/////////Invoice payment info //////////////////////////////////		
								InvoicePaymentInformation invoicePaymentInformationObj = new InvoicePaymentInformation();
								invoicePaymentInformationObj.setSubmitDate(date);
								invoicePaymentInformationObj.setPaidAmount(Double.parseDouble(inv_Amt.get(i)));
								invoicePaymentInformationObj.setChequeNumberInfo(chequeNumberInfo);
								OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObjInvoicePaymentInfo=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class, outWardOrderInvoiceEnteryRegistrationId);
								invoicePaymentInformationObj.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObjInvoicePaymentInfo);
								Integer invoicePaymentInformationId=(Integer) session.save(invoicePaymentInformationObj);
								
								if(invoicePaymentInformationId!=null)
								{
								///////////////Update remaining cheque Amount////////////
									RemainingChequeAmount remainingChequeAmountObj=(RemainingChequeAmount) session.get(RemainingChequeAmount.class,obj.getRemainingChequeAmountId());
									remainingChequeAmountObj.setRemainingChequeAmount(chequeAmount);
									if(chequeAmount.equals(0.0))
									{
										remainingChequeAmountObj.setStatus(0);
									}
									
									session.update(remainingChequeAmountObj);
								}
								invoiceAmount = 0.0;
								if(invoiceAmount.equals(0.0))
								{
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEntryRegistrationUpdateInvAmt=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class,outWardOrderEntryRegistrationId);
									outWardOrderInvoiceEntryRegistrationUpdateInvAmt.setRemainingPayment(0.0);
									outWardOrderInvoiceEntryRegistrationUpdateInvAmt.setPaymentStatus(1);
									session.update(	outWardOrderInvoiceEntryRegistrationUpdateInvAmt);
								
								}
							}
							break;
						}
						else
						{
							remainingInvoiceAmount=invoiceAmount-chequeAmount;
							Double invAmt=Double.parseDouble(inv_Amt.get(i));
							if(invAmt==invoiceAmount)
							{
								invoiceAmount = remainingInvoiceAmount;
								if(flag==0)
								{
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj = new OutWardOrderInvoiceEnteryRegistration();
									outWardOrderInvoiceEnteryRegistrationObj.setDispatchEnteryRegistrationStatus(0);
									outWardOrderInvoiceEnteryRegistrationObj.setInvoiceNo(inv_No.get(i));
									java.util.Date d2 = dtf.parse(inv_Date.get(i).toString());
									outWardOrderInvoiceEnteryRegistrationObj.setInvoiceDate(new java.sql.Date(d2.getTime()));
									outWardOrderInvoiceEnteryRegistrationObj.setNetAmount(Double.parseDouble(inv_Amt.get(i)));
									outWardOrderInvoiceEnteryRegistrationObj.setDispatchEnteryRegistrationStatus(0);
									outWardOrderInvoiceEnteryRegistrationObj.setSubmitDate(date);
									outWardOrderInvoiceEnteryRegistrationObj.setPaymentStatus(1);
									outWardOrderInvoiceEnteryRegistrationObj.setRemainingPayment(0.0);
									outWardOrderInvoiceEnteryRegistrationObj.setStatus(1);
									outWardOrderInvoiceEnteryRegistrationObj.setDiscountIsTakenOrNot("No");
									outWardOrderInvoiceEnteryRegistrationObj.setNoOfDays(noOfDays);
									outWardOrderInvoiceEnteryRegistrationObj.setFirstDate(new java.sql.Date(d2.getTime()));
									outWardOrderInvoiceEnteryRegistrationObj.setOutWardOrderEnteryRegistration(outwardOrderEntryRegistration);
	                                ////date Calculation
	                                java.util.Date str_invoiceDate = dtf
											.parse(inv_Date.get(i));
	                                Calendar cal = Calendar.getInstance();
									cal.setTime(str_invoiceDate);
									cal.add(Calendar.DATE, noOfDays); 
									java.sql.Date afterDate = new java.sql.Date(
											cal.getTimeInMillis());
									outWardOrderInvoiceEnteryRegistrationObj.setLastDate(afterDate);
									outWardOrderInvoiceEnteryRegistrationObj.setOrganization(organizationObj);
									outWardOrderInvoiceEnteryRegistrationId=(Integer) session.save(outWardOrderInvoiceEnteryRegistrationObj);
									flag=1;
								}
								if(outWardOrderInvoiceEnteryRegistrationId!=null && flag ==1)
								{
									
							/////////Invoice payment info //////////////////////////////////		
									InvoicePaymentInformation invoicePaymentInformationObj = new InvoicePaymentInformation();
									invoicePaymentInformationObj.setSubmitDate(date);
									invoicePaymentInformationObj.setPaidAmount(Double.parseDouble(inv_Amt.get(i)));
									invoicePaymentInformationObj.setChequeNumberInfo(chequeNumberInfo);
									OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObjInvoicePaymentInfo=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class, outWardOrderInvoiceEnteryRegistrationId);
									invoicePaymentInformationObj.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObjInvoicePaymentInfo);
									Integer invoicePaymentInformationId=(Integer) session.save(invoicePaymentInformationObj);
									
									if(invoicePaymentInformationId!=null)
									{
									///////////////Update remaining cheque Amount////////////
										RemainingChequeAmount remainingChequeAmountObj=(RemainingChequeAmount) session.get(RemainingChequeAmount.class,obj.getRemainingChequeAmountId());
										remainingChequeAmountObj.setRemainingChequeAmount(chequeAmount);
										if(chequeAmount.equals(0.0))
										{
											remainingChequeAmountObj.setStatus(0);
										}
										
										session.update(remainingChequeAmountObj);
									}
									invoiceAmount = 0.0;
									if(invoiceAmount.equals(0.0))
									{
										OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEntryRegistrationUpdateInvAmt=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class,outWardOrderEntryRegistrationId);
										outWardOrderInvoiceEntryRegistrationUpdateInvAmt.setRemainingPayment(0.0);
										outWardOrderInvoiceEntryRegistrationUpdateInvAmt.setPaymentStatus(1);
										session.update(	outWardOrderInvoiceEntryRegistrationUpdateInvAmt);
									
									}
								}
								
							}
							else
							{
								remainingInvoiceAmount = invoiceAmount
										- chequeAmount;
								invoiceAmount = remainingInvoiceAmount;
								OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj=(OutWardOrderInvoiceEnteryRegistration) session.get(OutWardOrderInvoiceEnteryRegistration.class,outWardOrderInvoiceEnteryRegistrationId);
								
								InvoicePaymentInformation invoicePaymentInformationObj = new InvoicePaymentInformation();
								
								invoicePaymentInformationObj
										.setSubmitDate(date);
								invoicePaymentInformationObj
										.setPaidAmount(chequeAmount);
								// setOutWardOrderInvoiceEnteryRegistration
								invoicePaymentInformationObj
										.setOutWardOrderInvoiceEnteryRegistration(outWardOrderInvoiceEnteryRegistrationObj);
								// set chequeNumberInfoObj
								invoicePaymentInformationObj
										.setChequeNumberInfo(chequeNumberInfo);
								
								Integer invoicePaymentInformationId=(Integer) session.save(invoicePaymentInformationObj);
								if (invoicePaymentInformationId != null) {
									// update into data base and status zero
									RemainingChequeAmount remainingChequeAmountObj = (RemainingChequeAmount) session.get(RemainingChequeAmount.class,obj.getRemainingChequeAmountId());
									remainingChequeAmountObj
											.setRemainingChequeAmount(0.0);
									remainingChequeAmountObj.setStatus(0);
									
									session.update(remainingChequeAmountObj);

									
									outWardOrderInvoiceEnteryRegistrationObj
											.setRemainingPayment(remainingInvoiceAmount);
									
									session.update(outWardOrderInvoiceEnteryRegistrationObj);
								}
							}
						}
					}
				}
				else
				{
					java.util.Date today = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
					String IST = df.format(today);
					DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
					java.util.Date date = df2.parse(IST);
					OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEntryReg=new OutWardOrderInvoiceEnteryRegistration();
					outWardOrderInvoiceEntryReg.setDispatchEnteryRegistrationStatus(0);
				//	java.util.Date str_invoiceDate = dtf.parse(inv_Date.get(i));
					java.util.Date d2 = dtf.parse(inv_Date.get(i).toString());

					outWardOrderInvoiceEntryReg.setInvoiceDate(new java.sql.Date(d2.getTime()));
					outWardOrderInvoiceEntryReg.setInvoiceNo(inv_No.get(i));
					outWardOrderInvoiceEntryReg.setNetAmount(Double.parseDouble(inv_Amt.get(i)));
					outWardOrderInvoiceEntryReg.setDispatchEnteryRegistrationStatus(0);
					outWardOrderInvoiceEntryReg.setSubmitDate(date);
					outWardOrderInvoiceEntryReg.setPaymentStatus(1);
					outWardOrderInvoiceEntryReg.setRemainingPayment(0.0);
					outWardOrderInvoiceEntryReg.setStatus(1);
					outWardOrderInvoiceEntryReg.setDiscountIsTakenOrNot("No");
					outWardOrderInvoiceEntryReg.setNoOfDays(noOfDays);
					outWardOrderInvoiceEntryReg.setFirstDate(new java.sql.Date(d2.getTime()));
					outWardOrderInvoiceEntryReg.setOutWardOrderEnteryRegistration(outwardOrderEntryRegistration);
                    ////date Calculation
                    java.util.Date str_invoiceDate = dtf
							.parse(inv_Date.get(i));
                    Calendar cal = Calendar.getInstance();
					cal.setTime(str_invoiceDate);
					cal.add(Calendar.DATE, noOfDays); 
					java.sql.Date afterDate = new java.sql.Date(
							cal.getTimeInMillis());
					outWardOrderInvoiceEntryReg.setLastDate(afterDate);
					outWardOrderInvoiceEntryReg.setOrganization(organizationObj);
					outWardOrderInvoiceEnteryRegistrationId=(Integer) session.save(outWardOrderInvoiceEntryReg);
				}
				
				//=========================================================//
				
			}
			outwardOrderEntryRegistrationObj.setInvoiceStatus(1);
			session.update(outwardOrderEntryRegistrationObj);
		}
			
			
		}
		
		}
		catch(Exception e){
			
			e.printStackTrace();}
		
		return outWardOrderEntryRegistrationId;
	}

}
