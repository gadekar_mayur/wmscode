/**
 * 
 */
package com.protocol.wms.dao;

import com.protocol.wms.model.OrganizationTripCount;

/**
 * @author Sudhakar
 *
 */
public interface OrganizationTripCountDao {
	
	public OrganizationTripCount loadOrganizationTripCountObjUsignOrgId(Integer orgId);
	
	public void updateOrganizationTripCount(OrganizationTripCount organizationTripCountObj);
	
	public Integer saveOrganizationTripCountObj(OrganizationTripCount organizationTripCountObj);

}
