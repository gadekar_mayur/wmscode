/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.InvoicePaymentInformation;

/**
 * @author Sudhakar
 *
 */
@Repository(value="InvoicePaymentInformationDaoImpl")
public class InvoicePaymentInformationDaoImpl implements InvoicePaymentInformationDao{

	private Logger log = Logger.getLogger(InvoicePaymentInformationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InvoicePaymentInformationDao#saveInvoicePaymentInformationObj(com.protocol.wms.model.InvoicePaymentInformation)
	 */
	@Override
	public Integer saveInvoicePaymentInformationObj(
			InvoicePaymentInformation InvoicePaymentInformationObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer invoicePaymentInformationId = null;
		try 
		{
			invoicePaymentInformationId=(Integer) session.save(InvoicePaymentInformationObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return invoicePaymentInformationId;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InvoicePaymentInformationDao#loadInvoicePaymentInformationObjUsignChequeNumberInfoId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InvoicePaymentInformation> loadInvoicePaymentInformationListUsignChequeNumberInfoId(
			Integer ChequeNumberInfoId) {
		Session session =sessionFactory.getCurrentSession();
		List<InvoicePaymentInformation> invoicePaymentInformationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(InvoicePaymentInformation.class,"InvoicePaymentInformation")
				     .createAlias("InvoicePaymentInformation.chequeNumberInfo", "chequeNumberInfo")
				     .add(Restrictions.eq("chequeNumberInfo.chequeNumberInfoId", ChequeNumberInfoId))
				     .addOrder(Order.desc("submitDate"));
				   invoicePaymentInformationList=criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return invoicePaymentInformationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InvoicePaymentInformationDao#deleteInvoicePaymentInformationObj(com.protocol.wms.model.InvoicePaymentInformation)
	 */
	@Override
	public void deleteInvoicePaymentInformationObj(
			InvoicePaymentInformation InvoicePaymentInformationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(InvoicePaymentInformationObj);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InvoicePaymentInformationDao#loadInvoicePaymentInformationUsigInvoicePaymentInformationId(java.lang.Integer)
	 */
	@Override
	public InvoicePaymentInformation loadInvoicePaymentInformationUsigInvoicePaymentInformationId(
			Integer invoicePaymentInformationId) {
		Session session =sessionFactory.getCurrentSession();
		InvoicePaymentInformation invoicePaymentInformationObj=null;
		try 
		{
			invoicePaymentInformationObj=(InvoicePaymentInformation)session.get(InvoicePaymentInformation.class,invoicePaymentInformationId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return invoicePaymentInformationObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InvoicePaymentInformationDao#loadinInvoicePaymentInformationsUsingOutWardOrderInvoiceEnteryRegistrationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InvoicePaymentInformation> loadinInvoicePaymentInformationsListUsingOutWardOrderInvoiceEnteryRegistrationId(
			Integer outWardOrderInvoiceEnteryRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		List<InvoicePaymentInformation> invoicePaymentInformationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(InvoicePaymentInformation.class,"InvoicePaymentInformation")
					.createAlias("InvoicePaymentInformation.outWardOrderInvoiceEnteryRegistration", "outWardOrderInvoiceEnteryRegistration")
					.add(Restrictions.eq("outWardOrderInvoiceEnteryRegistration.outWardOrderInvoiceEnteryRegistrationId", outWardOrderInvoiceEnteryRegistrationId))
					.addOrder(Order.desc("paidAmount"));
				   invoicePaymentInformationList=criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return invoicePaymentInformationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InvoicePaymentInformationDao#updateInvoicePaymentInformationObj(com.protocol.wms.model.InvoicePaymentInformation)
	 */
	@Override
	public boolean updateInvoicePaymentInformationObj(
			InvoicePaymentInformation invoicePaymentInformationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(invoicePaymentInformationObj);
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
}
