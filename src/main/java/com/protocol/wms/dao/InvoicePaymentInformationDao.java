/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.InvoicePaymentInformation;

/**
 * @author Sudhakar
 *
 */
public interface InvoicePaymentInformationDao {
	
	public Integer saveInvoicePaymentInformationObj(InvoicePaymentInformation InvoicePaymentInformationObj);
	
	public List<InvoicePaymentInformation> loadInvoicePaymentInformationListUsignChequeNumberInfoId(Integer ChequeNumberInfoId);
	
	public void deleteInvoicePaymentInformationObj(InvoicePaymentInformation InvoicePaymentInformationObj);
	
	public InvoicePaymentInformation loadInvoicePaymentInformationUsigInvoicePaymentInformationId(Integer invoicePaymentInformationId);

	public List<InvoicePaymentInformation> loadinInvoicePaymentInformationsListUsingOutWardOrderInvoiceEnteryRegistrationId(Integer outWardOrderInvoiceEnteryRegistrationId);
	
	public boolean updateInvoicePaymentInformationObj(InvoicePaymentInformation invoicePaymentInformationObj);
}
