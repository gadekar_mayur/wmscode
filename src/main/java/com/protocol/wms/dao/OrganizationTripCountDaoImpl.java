/**
 * 
 */
package com.protocol.wms.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.OrganizationTripCount;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OrganizationTripCountDaoImpl")
public class OrganizationTripCountDaoImpl implements OrganizationTripCountDao{
	
private Logger log = Logger.getLogger(OrganizationTripCountDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationTripCountDao#loadOrganizationTripCountObjUsignOrgId(java.lang.Integer)
	 */
	@Override
	public OrganizationTripCount loadOrganizationTripCountObjUsignOrgId(
			Integer orgId) {
		Session session =sessionFactory.getCurrentSession();
		OrganizationTripCount organizationTripCountObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(OrganizationTripCount.class,"OrganizationTripCount")
					.createAlias("OrganizationTripCount.organization", "organization")
					.add(Restrictions.eq("organization.organizationId", orgId));
					organizationTripCountObj=(OrganizationTripCount) criteria.uniqueResult();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return organizationTripCountObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationTripCountDao#updateOrganizationTripCount(com.protocol.wms.model.OrganizationTripCount)
	 */
	@Override
	public void updateOrganizationTripCount(
			OrganizationTripCount organizationTripCountObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(organizationTripCountObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OrganizationTripCountDao#saveOrganizationTripCountObj(com.protocol.wms.model.OrganizationTripCount)
	 */
	@Override
	public Integer saveOrganizationTripCountObj(
			OrganizationTripCount organizationTripCountObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer organizationTripCountId = null;
		try 
		{
			organizationTripCountId=(Integer) session.save(organizationTripCountObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return organizationTripCountId;
	}

}
