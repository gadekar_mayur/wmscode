/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.CreditNote;
import com.protocol.wms.model.CreditNoteCreditNoteImagePath;

/**
 * @author Sudhakar
 *
 */
@Repository(value="CreditNoteDaoImpl")
public class CreditNoteDaoImpl implements CreditNoteDao{

	private Logger log = Logger.getLogger(CreditNoteDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CreditNoteDao#saveCreditNoteObj(com.protocol.wms.model.CreditNote)
	 */
	@Override
	public Integer saveCreditNoteObj(CreditNote creditNoteObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer creditNoteId = null;
		try 
		{
			creditNoteId=(Integer) session.save(creditNoteObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return creditNoteId;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CreditNoteDao#loadCreditNoteObjectUsingCreditNoteId(java.lang.Integer)
	 */
	@Override
	public CreditNote loadCreditNoteObjectUsingCreditNoteId(Integer creditNoteId) {
		Session session =sessionFactory.getCurrentSession();
		CreditNote creditNoteObj=null;
		try 
		{
			creditNoteObj=(CreditNote)session.get(CreditNote.class,creditNoteId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return creditNoteObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CreditNoteDao#editInwardReturnGoodsRegCreditNoteDetails(com.protocol.wms.model.CreditNote)
	 */
	@Override
	public Boolean editInwardReturnGoodsRegCreditNoteDetails(CreditNote creditNote) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(creditNote);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	@Override
	public List<CreditNoteCreditNoteImagePath> updateCreditNoteRegCreditNoteAttachImage(
			Integer creditNoteId,
			List<CreditNoteCreditNoteImagePath> creditNoteImgList) {
		CreditNote creditNote = null;
		List<CreditNoteCreditNoteImagePath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			creditNote = (CreditNote) session.load(CreditNote.class, creditNoteId);
			for(CreditNoteCreditNoteImagePath tempObj : creditNoteImgList){
				tempObj.setCreditNote(creditNote);
				session.save(tempObj);
			}
			lst = creditNote.getCreditNoteCreditNoteImagePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}
	@Override
	public Boolean deleteCreditNoteRegCreditNoteAttachImage(Integer creditNoteAttachImageId) {
		CreditNoteCreditNoteImagePath creditNoteCreditNoteImagePath = null;
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			creditNoteCreditNoteImagePath = (CreditNoteCreditNoteImagePath) session.load(CreditNoteCreditNoteImagePath.class, creditNoteAttachImageId);
			session.delete(creditNoteCreditNoteImagePath);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
}
