/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.StockistChequeReturn;

/**
 * @author Sudhakar
 *
 */
@Repository(value="StockistChequeReturnDaoImpl")
public class StockistChequeReturnDaoImpl implements StockistChequeReturnDao{

	private Logger log = Logger.getLogger(StockistChequeReturnDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;

	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistChequeReturnDao#saveStockistChequeReturnObj(com.protocol.wms.model.StockistChequeReturn)
	 */
	@Override
	public Integer saveStockistChequeReturnObj(
			StockistChequeReturn stockistChequeReturnObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer stockistChequeReturnId = null;
		try 
		{
			stockistChequeReturnId=(Integer) session.save(stockistChequeReturnObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stockistChequeReturnId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistChequeReturnDao#loadStockistChequeReturnUsignOrgnizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<StockistChequeReturn> loadStockistChequeReturnListUsignOrgnizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<StockistChequeReturn> stockistChequeReturnList=null;
		try 
		{
			Criteria criteria = session.createCriteria(StockistChequeReturn.class,"StockistChequeReturn")
				     .createAlias("StockistChequeReturn.organization", "organization")
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   stockistChequeReturnList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stockistChequeReturnList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistChequeReturnDao#loadStockistChequeReturnObjUsignStockistChequeReturnId(java.lang.Integer)
	 */
	@Override
	public StockistChequeReturn loadStockistChequeReturnObjUsignStockistChequeReturnId(
			Integer stockistChequeReturnId) {
		Session session =sessionFactory.getCurrentSession();
		StockistChequeReturn stockistChequeReturnObj=null;
		try 
		{
			stockistChequeReturnObj=(StockistChequeReturn)session.get(StockistChequeReturn.class,stockistChequeReturnId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stockistChequeReturnObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistChequeReturnDao#deleteStockistChequeReturnObj(com.protocol.wms.model.StockistChequeReturn)
	 */
	@Override
	public void deleteStockistChequeReturnObj(
			StockistChequeReturn stockistChequeReturnObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(stockistChequeReturnObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}
}
