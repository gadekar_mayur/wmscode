/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;


import com.protocol.wms.model.OutWardProductOrderEntryRegistration;

/**
 * @author Sudhakar
 *
 */
public interface OutWardProductOrderEnteryRegistrationDao {
	
	public Integer saveOutWardProductOrderEnteryRegistration(OutWardProductOrderEntryRegistration outWardProductOrderEnteryRegistrationObj);

	public List<OutWardProductOrderEntryRegistration> getProductOrderEnteryRegistrationDetailsUsingOutWardOrderEntryRegistrationId(Integer OutWardOrderEntryRegistrationId);
	
	public OutWardProductOrderEntryRegistration loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(Integer outWardProductOrderEntryRegistrationId);
	
	public boolean updateOutWardProductOrderEntryRegistrationObj(OutWardProductOrderEntryRegistration OutWardProductOrderEntryRegistrationObj);
	
	public boolean deleteOutWardProductOrderEntryRegistrationObj(OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj);
}
