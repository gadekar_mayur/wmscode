package com.protocol.wms.dao;

import java.util.List;

import org.json.JSONObject;

import com.protocol.wms.model.StockistBankDetails;

public interface StockistBankDetailsDao {
	public JSONObject displayStockistBankDetailsForm();
	
	public JSONObject saveStockistBankDetails(Integer orgId,Integer stockistId, Integer companyId,StockistBankDetails bankDetatils);
	
	public JSONObject stockistBankDetailsList(Integer organizationId);
	
	public JSONObject searchStokistBankDetails(String searchOption,String searchText, Integer organizationId,Integer from);
	
	public List<StockistBankDetails> stockistBankDropdownList(Integer organizationId, Integer companyId, Integer stockistId);
	
	public Boolean deleteStockistBankDetails(Integer stockistBankId);
	
	public StockistBankDetails loadStockistBankDetailsUsignStockistBankDetailsId(Integer stockistBankDetailsId);
	
	public Boolean updateStockistBankDetails(StockistBankDetails stockistBankDetails);
}
