package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistrationOrderCopyPath;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.service.OutWardGoodsService;

@Repository(value="OutwardOrderEntryRegistrationImageCopyPathDaoImpl")
public class OutwardOrderEntryRegistrationImageCopyPathDaoImpl implements OutwardOrderEntryRegistrationImageCopyPathDao 
{
	private Logger log = Logger.getLogger(OutwardOrderEntryRegistrationImageCopyPathDaoImpl.class.getClass());
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}	
	
	@SuppressWarnings({ "unchecked"})
	public List<OutWardOrderEntryRegistrationOrderCopyPath> loadImages(Integer outWardOrderEnteryRegistrationId)
	{
		Session session =sessionFactory.getCurrentSession();
		
		List<OutWardOrderEntryRegistrationOrderCopyPath> outWardOrderInvoiceEnteryRegistrationList=null;
		try
		{
		Criteria criteria=session.createCriteria(OutWardOrderEntryRegistrationOrderCopyPath.class,"OutWardOrderEntryRegistrationOrderCopyPath")
				.createAlias("OutWardOrderEntryRegistrationOrderCopyPath.outWardOrderEntryRegistration", "outWardOrderEntryRegistration")
				.add(Restrictions.eq("outWardOrderEntryRegistration.outWardOrderEnteryRegistrationId", outWardOrderEnteryRegistrationId));
				outWardOrderInvoiceEnteryRegistrationList= criteria.list();
//		System.out.println("property is-"+outWardOrderInvoiceEnteryRegistrationList);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return  outWardOrderInvoiceEnteryRegistrationList;
	}
}
