package com.protocol.wms.dao;

import java.util.List;

public interface UploadDocumentDao {

	Integer saveInvReport(List<String> order_No,List<String> Order_Date, List<String> inv_No, List<String> inv_Date, List<String> inv_Amt,  List<String> stockist_Name, List<String> stockist_City, List<String> company_Name);

}
