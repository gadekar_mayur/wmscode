/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.BloodGroup;
import com.protocol.wms.model.EmployeeDetails;
import com.protocol.wms.model.EmployeeDetailsAddressProofPath;
import com.protocol.wms.model.EmployeeDetailsImagePath;
import com.protocol.wms.model.RoleType;

/**
 * @author Sudhakar
 *
 */
@Repository(value="EmployeeDetailsDaoImpl")
public class EmployeeDetailsDaoImpl implements EmployeeDetailsDao{

private Logger log = Logger.getLogger(EmployeeDetailsDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDetailsDao#saveEmployeeDetails(com.protocol.wms.model.EmployeeDetails)
	 */
	@Override
	public Integer saveEmployeeDetails(EmployeeDetails employeeDetailsObj) {
		
		Session session =sessionFactory.getCurrentSession();
		Integer empdetailsId = null;
		try 
		{
			empdetailsId=(Integer) session.save(employeeDetailsObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return empdetailsId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDetailsDao#loadEmployeeObjectUsingEmployeeDetailsId(java.lang.Integer)
	 */
	@Override
	public EmployeeDetails loadEmployeeObjectUsingEmployeeDetailsId(
			Integer empDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		EmployeeDetails employeeDetailsObj=null;
		try 
		{
			employeeDetailsObj=(EmployeeDetails)session.get(EmployeeDetails.class,empDetailsId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return employeeDetailsObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDetailsDao#updateEmployeeDetails(com.protocol.wms.model.EmployeeDetails)
	 */
	@Override
	public void updateEmployeeDetails(EmployeeDetails empObject) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(empObject);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDetailsDao#updateEmployeeDetails(java.lang.Integer, java.lang.Integer, com.protocol.wms.model.EmployeeDetails)
	 */
	@Override
	public Boolean updateEmployeeDetails(Integer roleTypeId,
			Integer bloodroupNameId, EmployeeDetails employeeDetails) {
		RoleType roleType = null;
		BloodGroup bloodGroup = null;
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			roleType = (RoleType) session.load(RoleType.class, roleTypeId);
			bloodGroup = (BloodGroup) session.load(BloodGroup.class, bloodroupNameId);
			employeeDetails.setRoleType(roleType);
			employeeDetails.setBloodGroup(bloodGroup);
			session.update(employeeDetails);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDetailsDao#loadEmployeeDetailsListUsingOrganizationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<EmployeeDetails> loadEmployeeDetailsListUsingOrganizationId(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<EmployeeDetails> employeeDetailsList=null;
		try 
		{
			Criteria criteria = session.createCriteria(EmployeeDetails.class,"EmployeeDetails")
				     .createAlias("EmployeeDetails.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.asc("EmployeeDetails.name"))
				     .addOrder(Order.desc("submitDate"));;
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   employeeDetailsList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return employeeDetailsList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDetailsDao#searchEmployeeDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Object, Object> searchEmployeeDetails(String searchOption,
			String searchText, Integer organizationId,Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<EmployeeDetails> employeeDetailsList=null;
		Map<Object, Object>map = null;
		try 
		{
			
			if("Mobile".equals(searchOption)){
				Query query = session.createSQLQuery("CALL searchEmployeeDetails(:searchText,:organizationId)")
						.addEntity(EmployeeDetails.class)
						.setParameter("searchText", searchText+"%")
						.setParameter("organizationId", organizationId);
				employeeDetailsList = query.list();
			}
			else
			{
				Criteria   criteria = session.createCriteria(EmployeeDetails.class,"EmployeeDetails")
					     .createAlias("EmployeeDetails.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.eq("organization.organizationStatus", "1"))
					     .setProjection(Projections.rowCount())
					     .addOrder(Order.desc("submitDate"));
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   if("Organization".equals(searchOption))
						   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					  /* else if("Company".equals(searchOption))
						   criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));*/
					   else if("EmployeeName".equals(searchOption))
						   criteria.add(Restrictions.like("EmployeeDetails.name", searchText,MatchMode.START));
					   else if("Gender".equals(searchOption))
						   criteria.add(Restrictions.like("EmployeeDetails.gender", searchText,MatchMode.START));
					   else if("Qualification".equals(searchOption))
						   criteria.add(Restrictions.like("EmployeeDetails.qualification", searchText,MatchMode.START));
					   else if("Email".equals(searchOption))
						   criteria.add(Restrictions.like("EmployeeDetails.emailId", searchText,MatchMode.START));
					   Long totalRecordCount = (Long)criteria.uniqueResult();
					   
					   
//					   System.out.println("totalRecordCount : "+totalRecordCount);
					   criteria = session.createCriteria(EmployeeDetails.class,"EmployeeDetails")
							     .createAlias("EmployeeDetails.organization", "organization")
							    // .createAlias("EmployeeDetails.company", "company")
							     .add(Restrictions.eq("status", 1))
							      .add(Restrictions.eq("organization.organizationStatus", "1"))
							     .addOrder(Order.desc("submitDate"));
							   if(organizationId!=null)
							     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
							   if("Organization".equals(searchOption))
								   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							  /* else if("Company".equals(searchOption))
								   criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));*/
							   else if("EmployeeName".equals(searchOption))
								   criteria.add(Restrictions.like("EmployeeDetails.name", searchText,MatchMode.START));
							   else if("Gender".equals(searchOption))
								   criteria.add(Restrictions.like("EmployeeDetails.gender", searchText,MatchMode.START));
							   else if("Qualification".equals(searchOption))
								   criteria.add(Restrictions.like("EmployeeDetails.qualification", searchText,MatchMode.START));
							   else if("Email".equals(searchOption))
								   criteria.add(Restrictions.like("EmployeeDetails.emailId", searchText,MatchMode.START));
							   
							   from=from-1;
							   criteria.setFirstResult(from *Constant.PAGE_COUNT);
							   criteria.setMaxResults(Constant.PAGE_COUNT);
							   employeeDetailsList=criteria.list();
							   
							   Integer tatalPage = 0;
								if(totalRecordCount>0)
								{
									tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
									if((totalRecordCount%Constant.PAGE_COUNT)!=0)
									{
										tatalPage=tatalPage+1;
									}
								}
							
									map = new HashMap<Object, Object>();
									map.put("empList", employeeDetailsList);
									map.put("totalPages", tatalPage);
		}
				}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.EmployeeDetailsDao#deleteEmployeeDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteEmployeeDetails(Integer empDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		EmployeeDetails employeeDetails = null;
		try{
			employeeDetails = (EmployeeDetails)session.get(EmployeeDetails.class, empDetailsId);
			employeeDetails.setStatus(0);
			session.update(employeeDetails);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<EmployeeDetailsImagePath> updateEmployeeImages(Integer employeeDetailsId, List<EmployeeDetailsImagePath> employeeImgList) {
		Session session =sessionFactory.getCurrentSession();
		EmployeeDetails employeeDetails = null;
		try{
			employeeDetails = (EmployeeDetails)session.get(EmployeeDetails.class, employeeDetailsId);
			for(EmployeeDetailsImagePath tempObj : employeeImgList ){
				tempObj.setEmployeeDetails(employeeDetails);
				session.save(tempObj);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return employeeDetails.getEmployeeDetailsImagePathList();
	}

	@Override
	public Boolean deleteEmployeeImage(Integer employeeImageId) {
		Session session =sessionFactory.getCurrentSession();
		EmployeeDetailsImagePath employeeDetailsImagePath = null;
		try{
			employeeDetailsImagePath = (EmployeeDetailsImagePath)session.get(EmployeeDetailsImagePath.class, employeeImageId);
			session.delete(employeeDetailsImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<EmployeeDetailsAddressProofPath> updateEmployeeAddressProofImage(
			Integer employeeDetailsId,
			List<EmployeeDetailsAddressProofPath> employeeAddressProofPathList) {
		Session session =sessionFactory.getCurrentSession();
		EmployeeDetails employeeDetails = null;
		try{
			employeeDetails = (EmployeeDetails)session.get(EmployeeDetails.class, employeeDetailsId);
			for(EmployeeDetailsAddressProofPath tempObj : employeeAddressProofPathList ){
				tempObj.setEmployeeDetails(employeeDetails);
				session.save(tempObj);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return employeeDetails.getEmployeeDetailsAddressProofPathList();
	}

	@Override
	public Boolean deleteEmployeeAddressProofImage(Integer employeeAddressProofImageId) {
		Session session =sessionFactory.getCurrentSession();
		EmployeeDetailsAddressProofPath employeeDetailsAddressProofPath = null;
		try{
			employeeDetailsAddressProofPath = (EmployeeDetailsAddressProofPath)session.get(EmployeeDetailsAddressProofPath.class, employeeAddressProofImageId);
			session.delete(employeeDetailsAddressProofPath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
}
