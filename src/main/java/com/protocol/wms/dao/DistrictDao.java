package com.protocol.wms.dao;

import java.util.List;

import org.json.JSONObject;

import com.protocol.wms.model.District;

public interface DistrictDao {

	public JSONObject getDistrictList(Integer stateId);
	
	public District loadDistricObjectUsingDstrictId(Integer districtId);
	
	public JSONObject saveDistrict(Integer stateId,String districtName);
	
	public Boolean deleteDistrictDetails(Integer districtId);

	public boolean updateDistrictObj(District districtObj);

	public List<District> loadDistrictList();

	public boolean districtAlreadyExistsOrNot(Integer stateId,
			String districtName);

	public List<District> searchOptionForDistrict(String searchOption,String searchText,Integer organizationId);
}
