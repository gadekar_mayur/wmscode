package com.protocol.wms.dao;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.District;
import com.protocol.wms.model.State;

@Repository(value="DistrictDaoImpl")
public class DistrictDaoImpl implements DistrictDao {

	private Logger log = Logger.getLogger(DistrictDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getDistrictList(Integer stateId) {
		Session session =sessionFactory.getCurrentSession();
		List<District> districtList = null;
		JSONObject districtJson = null;
		JSONArray  districtArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(District.class,"dist")
					.createAlias("dist.state", "state")
					.add(Restrictions.eq("state.stateId", stateId))
					.addOrder(Order.asc("dist.districtName"))
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("districtId"));
			districtList = criteria.list();
			Iterator<District> distIt = districtList.iterator();
			//System.out.println("Darrrrrrrr");
			while(distIt.hasNext()){
				District temp = distIt.next();
				districtJson = new JSONObject();
				districtJson.put("distId", temp.getDistrictId());
				districtJson.put("distName", temp.getDistrictName());
				districtArray.put(districtJson);
			}
			districtJson = new JSONObject();
			districtJson.put("districtArray",districtArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return districtJson;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.DistrictDao#loadDistricObjectUsingDstrictId(java.lang.Integer)
	 */
	@Override
	public District loadDistricObjectUsingDstrictId(Integer districtId) {
		Session session =sessionFactory.getCurrentSession();
		District districObject = null;
		try 
		{
			districObject=(District) session.get(District.class,districtId);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return districObject;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.DistrictDao#saveDistrict(java.lang.Integer, java.lang.String)
	 */
	@Override
	public JSONObject saveDistrict(Integer stateId,String districtName) {
		State state=null;
		District district = null;
		JSONObject result = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(District.class,"district")
					.createAlias("district.state", "state")
					.add(Restrictions.eq("districtName", districtName))
					.add(Restrictions.eq("status",1))
					.add(Restrictions.eq("state.stateId",stateId));
			district = (District)criteria.uniqueResult();
			
			if(district!=null){
				result = new JSONObject();
				result.put("result", false);
				result.put("MSG", "District Already Exists...!");
				result.put("FLAG", true);
				return result;
			}
			state = (State) session.load(State.class, stateId);
			district = new District();
			district.setDistrictName(districtName.toUpperCase());
			district.setState(state);
			district.setStatus(1);
			 int i =(Integer) session.save(district);
		       if(i>0){
		    	   result = new JSONObject();
		    	   result.put("result", true);
		    	   result.put("MSG", "District Added Succesfully...!");
		       }
		       else{
		    	   result = new JSONObject();
		    	   result.put("result", false);
		    	   result.put("MSG", "Add District failed...!");
		       }
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
			 /*try {
				result.put("result", false);
				result.put("MSG", "Operation failed...!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}*/
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.DistrictDao#deleteDistrictDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteDistrictDetails(Integer districtId) {
		Session session =sessionFactory.getCurrentSession();
		District district= null;
		try{
			district = (District)session.get(District.class, districtId);
			district.setStatus(0);
			session.update(district);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public boolean updateDistrictObj(District districtObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(districtObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<District> loadDistrictList() {

		Session session =sessionFactory.getCurrentSession();
		List<District> districtList=null;
		try 
		{
			Criteria criteria = session.createCriteria(District.class,"District")
					.add(Restrictions.eq("District.status", 1));
			districtList=criteria.list();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return districtList;
	}


	@Override
	public boolean districtAlreadyExistsOrNot(Integer stateId,
			String districtName) {
		District district=null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(District.class,"district")
					.createAlias("district.state", "state")
					.add(Restrictions.eq("districtName", districtName.toUpperCase()))
					.add(Restrictions.eq("status",1))
					.add(Restrictions.eq("state.stateId", stateId));
			district = (District)criteria.uniqueResult();
			
			if(district==null)
				return false;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<District> searchOptionForDistrict(String searchOption,
			String searchText,Integer organizationId) {
		// TODO Auto-generated method stub

		Session session =sessionFactory.getCurrentSession();
		List<District> districtList=null;
		
		try 
		{
			Criteria criteria = session.createCriteria(District.class,"district")
					.createAlias("district.state", "state")
					.createAlias("state.organization", "organization")
					.add(Restrictions.eq("status", 1))
					.add(Restrictions.eq("state.status", 1))
					.add(Restrictions.eq("organization.organizationStatus", "1"))
					.addOrder(Order.desc("districtId"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption)){
				criteria.add(Restrictions.like("organization.organizationName", searchText, MatchMode.START));
			}else if("District".equals(searchOption)){
				criteria.add(Restrictions.like("district.districtName", searchText, MatchMode.START));
			}
			else if("State".equals(searchOption)){
				criteria.add(Restrictions.like("state.stateName", searchText, MatchMode.START));
			}
			districtList = criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return districtList;
	}

}
