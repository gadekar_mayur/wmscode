/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;



import java.util.Map;

import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsClaimCopyPath;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsLrImgPath;

/**
 * @author admin
 *
 */
public interface InwardReturnGoodsRegistrationLRDetailsDao {
	public InwardReturnGoodsRegistrationLRDetails loadInwardReturnGoodsRegistrationLRDetails(Integer InwardReturnGoodsRegistrationLRDetailsId);
	
	public Boolean editInwardReturnGoodsRegLrDetails(InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails);

	public Map<String , Object> searchInwardReturnGoodsRegListing(String selectedValue, String searchText, String fromSpecialData,String toSpecialData,String searchOption, Integer from, Integer organizationId);
	
	public Boolean deleteInwardReturnGoodsLrRegLrImage(Integer lrImageId);
	
	public Boolean deleteInwardReturnGoodsLrRegClaimCopyImage(Integer lrImageId);
	
	public List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> updateInwardReturnGoodsLrRegLrImage(Integer inwardReturnGoodsLrRegistrationId, List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> lrImgList);
	
	public List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> updateInwardReturnGoodsLrRegClaimCopyImage(Integer inwardReturnGoodsLrRegistrationId, List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> claimCopyImgList);
}
