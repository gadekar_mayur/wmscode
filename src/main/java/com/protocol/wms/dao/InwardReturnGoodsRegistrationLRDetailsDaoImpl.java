/**
 * 
 */
package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsClaimCopyPath;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetailsLrImgPath;

/**
 * @author admin
 *
 */
@Repository(value="InwardReturnGoodsRegistrationLRDetailsDaoImpl")
public class InwardReturnGoodsRegistrationLRDetailsDaoImpl implements InwardReturnGoodsRegistrationLRDetailsDao{

	private Logger log = Logger.getLogger(InwardReturnGoodsRegistrationDaoImpl.class.getClass());
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationLRDetailsDao#loadInwardReturnGoodsRegistrationLRDetails(java.lang.Integer)
	 */
	@Override
	public InwardReturnGoodsRegistrationLRDetails loadInwardReturnGoodsRegistrationLRDetails(Integer InwardReturnGoodsRegistrationLRDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails=null;
		try 
		{
			inwardReturnGoodsRegistrationLRDetails=(InwardReturnGoodsRegistrationLRDetails)session.get(InwardReturnGoodsRegistrationLRDetails.class,InwardReturnGoodsRegistrationLRDetailsId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardReturnGoodsRegistrationLRDetails;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnGoodsRegistrationLRDetailsDao#editInwardReturnGoodsRegLrDetails(com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails)
	 */
	@Override
	public Boolean editInwardReturnGoodsRegLrDetails(
			InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(inwardReturnGoodsRegistrationLRDetails);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Map<String , Object> searchInwardReturnGoodsRegListing(String selectedValue, String searchText, String fromSpecialData,String toSpecialData,String searchOption, Integer from, Integer organizationId) {
		
		List<InwardReturnGoodsRegistration> InwardReturnGoodsRegistration = null;
		Session session =sessionFactory.getCurrentSession();
		Map<String,Object> map = null;
		try{
			//=  = = = = = End For Pagination = = = = 
			Criteria criteria = session.createCriteria(InwardReturnGoodsRegistration.class,"inwardgoodreturn")
					.createAlias("inwardgoodreturn.organization", "organization")
					.add(Restrictions.eq("inwardgoodreturn.status",1))
					.setProjection(Projections.rowCount());
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			if("CheckingDoneSearch".equals(searchOption)){
				criteria.add(Restrictions.eq("inwardgoodreturn.checkingDoneStatus",1));
				criteria.add(Restrictions.eq("inwardgoodreturn.creditNoteStatus",0));
			}else if("CreditNoteSearch".equals(searchOption)){
				criteria.add(Restrictions.eq("inwardgoodreturn.checkingDoneStatus",1));
				criteria.add(Restrictions.eq("inwardgoodreturn.creditNoteStatus",1));
			}else if("checkingPending".equals(searchOption)){
				criteria.add(Restrictions.eq("inwardgoodreturn.checkingDoneStatus",0));
			}
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("inwardgoodreturn.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
			case "Transporter" :
				 criteria.createAlias("inwardgoodreturn.transporterDetails", "transporterDetails");
				 criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
				 break;
			case "LrNo" :
				   criteria.createAlias("inwardgoodreturn.inwardReturnGoodsRegistrationLRDetailList", "DataFromAnotherTable");
				   criteria.add(Restrictions.like("DataFromAnotherTable.LRNo", searchText,MatchMode.START));
				   break;
			case "ClaimNo" :
				 criteria.createAlias("inwardgoodreturn.inwardReturnGoodsRegistrationLRDetailList", "DataFromAnotherTable");
				 criteria.add(Restrictions.like("DataFromAnotherTable.refNoOrClaimNo", searchText,MatchMode.START));
				 break;
			case "DriverName" :
				  criteria.add(Restrictions.like("inwardgoodreturn.driverName", searchText,MatchMode.START));
				  break;
			case "DeliveredEmployee" :
				  criteria.createAlias("inwardgoodreturn.employeeDetails", "employeeDetails");
				  criteria.add(Restrictions.like("employeeDetails.name", searchText,MatchMode.START));
				  break;
			case "CreditNoteNo":
				  criteria.createAlias("inwardgoodreturn.creditNote", "creditNote");
				  criteria.add(Restrictions.like("creditNote.creditNoteNo", searchText,MatchMode.START));
				  break;
		   case "stockist" :
				  criteria.createAlias("inwardgoodreturn.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
			case "ClaimAmount" :
				 criteria.createAlias("inwardgoodreturn.inwardReturnGoodsRegistrationLRDetailList", "DataFromAnotherTable");
				 criteria.add(Restrictions.ge("DataFromAnotherTable.claimAmount", Float.parseFloat(fromSpecialData)));
				 criteria.add(Restrictions.le("DataFromAnotherTable.claimAmount", Float.parseFloat(toSpecialData)));
//				 criteria.add(Restrictions.between("DataFromAnotherTable.claimAmount", Float.parseFloat(fromSpecialData), Float.parseFloat(toSpecialData)));
				 break;
			case "CreditNoteAmount":
				  criteria.createAlias("inwardgoodreturn.creditNote", "creditNote");
				  criteria.add(Restrictions.ge("creditNote.creditNoteAmount", Float.parseFloat(fromSpecialData)));
				  criteria.add(Restrictions.le("creditNote.creditNoteAmount", Float.parseFloat(toSpecialData)));
				  break;
			case "Date" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.add(Restrictions.between("inwardgoodreturn.returnGoodsRegistrationDate", sqlFromDate, sqlToDate));
				   }
				 break;
			case "CheckingDoneDate" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.createAlias("inwardgoodreturn.checkingDoneList", "checkingDoneList");
					   criteria.add(Restrictions.between("checkingDoneList.checkingDoneDate", sqlFromDate, sqlToDate));
				   }
				 break;
			}
			Long totalRecordCount = (Long)criteria.uniqueResult();
			//=  = = = = = End For Pagination = = = = 
			criteria = session.createCriteria(InwardReturnGoodsRegistration.class,"inwardgoodreturn")
					.createAlias("inwardgoodreturn.organization", "organization")
					.add(Restrictions.eq("inwardgoodreturn.status",1))
					.addOrder(Order.desc("submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			if("CheckingDoneSearch".equals(searchOption)){
				criteria.add(Restrictions.eq("inwardgoodreturn.checkingDoneStatus",1));
				criteria.add(Restrictions.eq("inwardgoodreturn.creditNoteStatus",0));
			}else if("CreditNoteSearch".equals(searchOption)){
				criteria.add(Restrictions.eq("inwardgoodreturn.checkingDoneStatus",1));
				criteria.add(Restrictions.eq("inwardgoodreturn.creditNoteStatus",1));
			}else if("checkingPending".equals(searchOption)){
				criteria.add(Restrictions.eq("inwardgoodreturn.checkingDoneStatus",0));
			}
			switch(selectedValue){
			case "Company" : 
				 criteria.createAlias("inwardgoodreturn.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
			case "Transporter" :
				 criteria.createAlias("inwardgoodreturn.transporterDetails", "transporterDetails");
				 criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
				 break;
			case "LrNo" :
				   criteria.createAlias("inwardgoodreturn.inwardReturnGoodsRegistrationLRDetailList", "DataFromAnotherTable");
				   criteria.add(Restrictions.like("DataFromAnotherTable.LRNo", searchText,MatchMode.START));
				   break;
			case "ClaimNo" :
				 criteria.createAlias("inwardgoodreturn.inwardReturnGoodsRegistrationLRDetailList", "DataFromAnotherTable");
				 criteria.add(Restrictions.like("DataFromAnotherTable.refNoOrClaimNo", searchText,MatchMode.START));
				 break;
			case "DriverName" :
				  criteria.add(Restrictions.like("inwardgoodreturn.driverName", searchText,MatchMode.START));
				  break;
			case "DeliveredEmployee" :
				  criteria.createAlias("inwardgoodreturn.employeeDetails", "employeeDetails");
				  criteria.add(Restrictions.like("employeeDetails.name", searchText,MatchMode.START));
				  break;
			case "CreditNoteNo":
				  criteria.createAlias("inwardgoodreturn.creditNote", "creditNote");
				  criteria.add(Restrictions.like("creditNote.creditNoteNo", searchText,MatchMode.START));
				  break;
		   case "stockist" :
				  criteria.createAlias("inwardgoodreturn.stockist", "stockist");
				  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				  break;
			case "ClaimAmount" :
				 criteria.createAlias("inwardgoodreturn.inwardReturnGoodsRegistrationLRDetailList", "DataFromAnotherTable");
				 criteria.add(Restrictions.ge("DataFromAnotherTable.claimAmount", Float.parseFloat(fromSpecialData)));
				 criteria.add(Restrictions.le("DataFromAnotherTable.claimAmount", Float.parseFloat(toSpecialData)));
//				 criteria.add(Restrictions.between("DataFromAnotherTable.claimAmount", Float.parseFloat(fromSpecialData), Float.parseFloat(toSpecialData)));
				 break;
			case "CreditNoteAmount":
				  criteria.createAlias("inwardgoodreturn.creditNote", "creditNote");
				  criteria.add(Restrictions.ge("creditNote.creditNoteAmount", Float.parseFloat(fromSpecialData)));
				  criteria.add(Restrictions.le("creditNote.creditNoteAmount", Float.parseFloat(toSpecialData)));
				 
				  break;
				  
			case "Date" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.add(Restrictions.between("inwardgoodreturn.returnGoodsRegistrationDate", sqlFromDate, sqlToDate));
				   }
				 break;
			case "CheckingDoneDate" : 
				if(fromSpecialData!=""&&toSpecialData!=""){
					   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					   java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
					   criteria.createAlias("inwardgoodreturn.checkingDoneList", "checkingDoneList");
					   criteria.add(Restrictions.between("checkingDoneList.checkingDoneDate", sqlFromDate, sqlToDate));
				   }
				 break;
			}
			from = from - 1;
		    criteria.setFirstResult(from*Constant.PAGE_COUNT);
		    criteria.setMaxResults(Constant.PAGE_COUNT);
			InwardReturnGoodsRegistration = criteria.list();
			Integer tatalPage = 0;
			if(totalRecordCount>0){
				tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					tatalPage=tatalPage+1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("InwardReturnGoodsRegistration", InwardReturnGoodsRegistration);
			map.put("totalPages", tatalPage);
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return map;
	}
	@Override
	public Boolean deleteInwardReturnGoodsLrRegLrImage(Integer lrImageId) {
		Session session =sessionFactory.getCurrentSession();
		InwardReturnGoodsRegistrationLRDetailsLrImgPath inwardReturnGoodsRegistrationLRDetailsLrImgPath= null;
		try{
			inwardReturnGoodsRegistrationLRDetailsLrImgPath = (InwardReturnGoodsRegistrationLRDetailsLrImgPath)session.load(InwardReturnGoodsRegistrationLRDetailsLrImgPath.class, lrImageId);
			session.delete(inwardReturnGoodsRegistrationLRDetailsLrImgPath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	@Override
	public Boolean deleteInwardReturnGoodsLrRegClaimCopyImage(Integer lrImageId) {
		Session session =sessionFactory.getCurrentSession();
		InwardReturnGoodsRegistrationLRDetailsClaimCopyPath inwardReturnGoodsRegistrationLRDetailsClaimCopyPath= null;
		try{
			inwardReturnGoodsRegistrationLRDetailsClaimCopyPath = (InwardReturnGoodsRegistrationLRDetailsClaimCopyPath)session.load(InwardReturnGoodsRegistrationLRDetailsClaimCopyPath.class, lrImageId);
			session.delete(inwardReturnGoodsRegistrationLRDetailsClaimCopyPath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	@Override
	public List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> updateInwardReturnGoodsLrRegLrImage(Integer inwardReturnGoodsLrRegistrationId,
			List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> lrImgList) {

		InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails = null;
		List<InwardReturnGoodsRegistrationLRDetailsLrImgPath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			inwardReturnGoodsRegistrationLRDetails = (InwardReturnGoodsRegistrationLRDetails) session.load(InwardReturnGoodsRegistrationLRDetails.class, inwardReturnGoodsLrRegistrationId);
			for(InwardReturnGoodsRegistrationLRDetailsLrImgPath tempObj : lrImgList){
				tempObj.setInwardReturnGoodsRegistrationLRDetails(inwardReturnGoodsRegistrationLRDetails);
				session.save(tempObj);
			}
			lst = inwardReturnGoodsRegistrationLRDetails.getInwardReturnGoodsRegistrationLRDetailsLrImgPathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}
	@Override
	public List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> updateInwardReturnGoodsLrRegClaimCopyImage(
			Integer inwardReturnGoodsLrRegistrationId,
			List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> claimCopyImgList) {
		InwardReturnGoodsRegistrationLRDetails inwardReturnGoodsRegistrationLRDetails = null;
		List<InwardReturnGoodsRegistrationLRDetailsClaimCopyPath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			inwardReturnGoodsRegistrationLRDetails = (InwardReturnGoodsRegistrationLRDetails) session.load(InwardReturnGoodsRegistrationLRDetails.class, inwardReturnGoodsLrRegistrationId);
			for(InwardReturnGoodsRegistrationLRDetailsClaimCopyPath tempObj : claimCopyImgList){
				tempObj.setInwardReturnGoodsRegistrationLRDetails(inwardReturnGoodsRegistrationLRDetails);
				session.save(tempObj);
			}
			lst = inwardReturnGoodsRegistrationLRDetails.getInwardReturnGoodsRegistrationLRDetailsClaimCopyPathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}
	
}
