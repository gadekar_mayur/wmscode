/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.DocumentListType;

/**
 * @author Sudhakar
 *
 */
@Repository(value="DocumentListTypeDaoImpl")
public class DocumentListTypeDaoImpl implements DocumentListTypeDao{

	private Logger log = Logger.getLogger(DocumentListTypeDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.DocumentListTypeDao#loadDocumentListType()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentListType> loadDocumentListType() {
		Session session =sessionFactory.getCurrentSession();
		List<DocumentListType> documentListTypes=null;
		try 
		{
			Criteria criteria = session.createCriteria(DocumentListType.class,"DocumentListType")
					.add(Restrictions.eq("DocumentListType.status", 1))
					.addOrder(Order.asc("DocumentListType.documentName"));
					documentListTypes=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return documentListTypes;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.DocumentListTypeDao#loadDocumentListTypeUsingDocumentListTypeId(java.lang.Integer)
	 */
	@Override
	public DocumentListType loadDocumentListTypeUsingDocumentListTypeId(
			Integer documentListTypeId) {
		Session session =sessionFactory.getCurrentSession();
		DocumentListType documentListTypeObj=null;
		try 
		{
			documentListTypeObj=(DocumentListType)session.get(DocumentListType.class,documentListTypeId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return documentListTypeObj;
	}

}
