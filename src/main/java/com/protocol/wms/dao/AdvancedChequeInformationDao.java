/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;

/**
 * @author Sudhakar
 *
 */
public interface AdvancedChequeInformationDao {

	public boolean saveAdvancedChequeInformationObj(AdvancedChequeInformation AdvancedChequeInformationObj,String[] checkNo);
	
	public List<AdvancedChequeInformation> loadAdvancedChequeInformationListUsignOrganizationId(Integer organizationId);
	
	public JSONArray loadChequeInformationUsignOrgIdAndCompanyIdAndCompanyBankId(Integer orgId,Integer companyId,Integer companyBankId);
	
	public JSONArray loadChequeInformationUsignOrgIdAndCompanyIdAndStockistAndStockistBankId(Integer orgId,Integer companyId,Integer stockistId,Integer stockistBankId);

	public AdvancedChequeInformation loadAdvancedChequeInformationObjUsignAdvanceChequeId(Integer advanceChequeid);

	public List<ChequeNumberInfo> loadChequeNumberInfoObjUsignAdvanceChequeId(Integer advanceChequeid);
	
	public boolean updateAdvancedChequeInformationObj(AdvancedChequeInformation advancedChequeInformationObj);
	
	public boolean deleteAdvancedChequeInformationObj(AdvancedChequeInformation advancedChequeInformationObj);

	public Map<Object, Object> searchAdvancedChequeInformation(String searchOption, String searchText, Integer organizationId, Integer from);
}
