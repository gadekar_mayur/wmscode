/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;



import java.util.Map;

import com.protocol.wms.model.CompanyDocument;
import com.protocol.wms.model.CompanyDocumentImagePath;

/**
 * @author admin
 *
 */
public interface CompanyDocumentDao {

	public Map<Object,Object> searchUploadedCompanyDocsDetails(String searchOption, String searchText, Integer organizationId, Integer from);
	
	public CompanyDocument loadCompanyDocumentObjById(Integer companyDocumentId);
	
	public Boolean updateCompanyDocumentObj(CompanyDocument companyDocument);
	
	public List<CompanyDocumentImagePath> updateCompanyUploadDocumentImage(Integer companyDocumentId, List<CompanyDocumentImagePath> documentList);
	
	public Boolean deleteCompanyDocumentFileImage(Integer companyDocumentPathId);
}
