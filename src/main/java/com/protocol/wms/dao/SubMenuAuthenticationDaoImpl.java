/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.SubMenuAuthentication;

/**
 * @author admin
 *
 */
@Repository(value="SubMenuAuthenticationDaoImpl")
public class SubMenuAuthenticationDaoImpl implements SubMenuAuthenticationDao{
	private Logger log = Logger.getLogger(SubMenuAuthenticationDaoImpl.class.getClass());
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.SubMenuAuthenticationDao#loadSubMenuUsingMenuAuthenticationId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SubMenuAuthentication> loadSubMenuUsingMenuAuthenticationId(
			Integer menuAuthenticationId) {
		Session session =sessionFactory.getCurrentSession();
		List<SubMenuAuthentication> subMenuAuthenticationsList=null;
		try 
		{
			Criteria criteria = session.createCriteria(SubMenuAuthentication.class,"SubMenuAuthentication")
					.createAlias("SubMenuAuthentication.menuAuthentication", "menuAuthentication")
					.add(Restrictions.eq("menuAuthentication.menuAuthenticationId", menuAuthenticationId));
			subMenuAuthenticationsList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return subMenuAuthenticationsList;
	}

}
