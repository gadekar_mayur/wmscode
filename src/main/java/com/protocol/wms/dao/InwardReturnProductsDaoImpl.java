/**
 * 
 */
package com.protocol.wms.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.InwardReturnProducts;

/**
 * @author admin
 *
 */
@Repository(value="InwardReturnProductsDaoImpl")
public class InwardReturnProductsDaoImpl implements InwardReturnProductsDao {

	private Logger log = Logger.getLogger(InwardReturnProductsDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnProductsDao#loadInwardReturnProductsObjById(java.lang.Integer)
	 */
	@Override
	public InwardReturnProducts loadInwardReturnProductsObjById(Integer InwardReturnProductsId) {
		Session session =sessionFactory.getCurrentSession();
		InwardReturnProducts inwardReturnProducts= null;
		try{
			inwardReturnProducts = (InwardReturnProducts)session.get(InwardReturnProducts.class, InwardReturnProductsId);
			return inwardReturnProducts;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return inwardReturnProducts;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardReturnProductsDao#editInwardReturnGoodsRegLreditProductDetails(com.protocol.wms.model.InwardReturnProducts)
	 */
	@Override
	public Boolean editInwardReturnGoodsRegLreditProductDetails(InwardReturnProducts inwardReturnProducts) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.update(inwardReturnProducts);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	
}
