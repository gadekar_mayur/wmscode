/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.OutwardTripReport;

/**
 * @author Sudhakar
 *
 */
public interface OutwardTripReportDao {

	public Integer saveOutwardTripReport(OutwardTripReport outwardTripReportObj);
	
	public List<OutwardTripReport> loadOutwardTripReportListUsignOutWardTripId(Integer outWardTripId);
	
	public void deletePrintedOutwardTripReportObj(List<OutwardTripReport> printedOutwardTripReportObjList);
}
