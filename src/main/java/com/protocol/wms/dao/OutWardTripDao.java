/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.OutWardTrip;

/**
 * @author Sudhakar
 *
 */
public interface OutWardTripDao {
	
	public Integer saveOutWardTripObj(OutWardTrip obj);
	
	public OutWardTrip loadOutWardTripObjUsignOutWardTripiD(Integer outWardTripId);
	
	public List<OutWardTrip> loadOutWardTripListOfStatusOneUsignOrganizationId(Integer organizationId);
	
	public List<OutWardTrip> loadOutWardTripListOfGetPassStatusZeroAndStatusOneUsignOrganizationId(Integer organizationId);
	
	public void updateOutWardTripObj(OutWardTrip obj);
	
	public List<OutWardTrip> loadloadOutWardTripListOfGetPassStatusOneUsignOrganizationId(Integer organizationId);
	
	public Boolean updateOutWardTripDetails(Integer cartingAgentId,OutWardTrip outWardTripObj);
	
	public boolean deleteOutWardTripEntryObj(OutWardTrip outWardTripObj);

	public Map<String,Object>searchOutwardViewTripList(String selectedValue,
			String searchText, String fromSpecialData, String toSpecialData,Integer from,
			Integer organizationId);

}
