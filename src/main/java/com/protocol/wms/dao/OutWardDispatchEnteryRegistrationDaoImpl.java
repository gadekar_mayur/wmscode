/**
 * 
 */
package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.OutWardDispatchEnteryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.OutWardTrip;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OutWardDispatchEnteryRegistrationDaoImpl")
public class OutWardDispatchEnteryRegistrationDaoImpl implements OutWardDispatchEnteryRegistrationDao{

	
	private Logger log = Logger.getLogger(OutWardDispatchEnteryRegistrationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#saveOutWardDispatchEnteryRegistrationObj(com.protocol.wms.model.OutWardDispatchEnteryRegistration)
	 */
	@Override
	public Integer saveOutWardDispatchEnteryRegistrationObj(
			OutWardDispatchEnteryRegistration obj) {
		Session session =sessionFactory.getCurrentSession();
		Integer outWardDispatchEnteryRegistrationId = null;
		try 
		{
			outWardDispatchEnteryRegistrationId=(Integer) session.save(obj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardDispatchEnteryRegistrationId;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(java.lang.Integer)
	 */
	@Override
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsignOutWardDispatchEnteryRegistrationId(
			Integer outWardDispatchEnteryRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj=null;
		try 
		{
			outWardDispatchEnteryRegistrationObj=(OutWardDispatchEnteryRegistration)session.get(OutWardDispatchEnteryRegistration.class,outWardDispatchEnteryRegistrationId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardDispatchEnteryRegistrationObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#loadOutWardDispatchEnteryRegistrationListOfTripStatusZero(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfTripStatusZero(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("tripStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardDispatchEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardDispatchEnteryRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#loadOutWardDispatchEnteryRegistrationListOfStatusOne(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOne(
			Integer organizationId) 
		{
		Session session =sessionFactory.getCurrentSession();
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardDispatchEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardDispatchEnteryRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#updateOutWardDispatchEnteryRegistrationObj(com.protocol.wms.model.OutWardDispatchEnteryRegistration)
	 */
	@Override
	public void updateOutWardDispatchEnteryRegistrationObj(
			OutWardDispatchEnteryRegistration OutWardDispatchEnteryRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(OutWardDispatchEnteryRegistrationObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#loadOutWardDispatchEnteryRegistrationObjUsigOrgIdAndStockistIdAndCompanyId(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsigOrgIdAndStockistIdAndCompanyId(
			Integer orgId, Integer stokistId, Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist")
				     .createAlias("OutWardDispatchEnteryRegistration.company", "company")
				     .add(Restrictions.eq("stockist.stockistId", stokistId))
				     .add(Restrictions.eq("company.companyId", companyId))
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("tripStatus", 0));
			outWardDispatchEnteryRegistrationObj=(OutWardDispatchEnteryRegistration) criteria.uniqueResult();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardDispatchEnteryRegistrationObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#loadOutWardDispatchEnteryRegistrationListOfStatusOneAndTripStatusOne(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOneAndTripStatusOne(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("tripStatus", 1))
				     .add(Restrictions.eq("getPassStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardDispatchEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardDispatchEnteryRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListOfStatusOneAndGetPassStatusOne(
			Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("getPassStatus", 1))
				     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   outWardDispatchEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardDispatchEnteryRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardDispatchEnteryRegistration> loadOutWardDispatchEnteryRegistrationListUsignOutWardTripId(
			Integer outwardTripId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
					.createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip")
					.add(Restrictions.eq("outWardTrip.outWardTripId", outwardTripId))
				     .add(Restrictions.eq("status", 1));
				   outWardDispatchEnteryRegistrationList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardDispatchEnteryRegistrationList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#updateOutWardDispatchEnteryRegistrationObjDetails(com.protocol.wms.model.OutWardDispatchEnteryRegistration)
	 */
	@Override
	public boolean updateOutWardDispatchEnteryRegistrationObjDetails(
			Integer OutWardDispatchEnteryRegistrationId,Integer newNoOfCases) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			//load out WardDispatchEntery Registration Obj using outWardDispatchEnteryRegistrationId
			
			OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj=(OutWardDispatchEnteryRegistration) session.get(OutWardDispatchEnteryRegistration.class, OutWardDispatchEnteryRegistrationId);
			Integer oldNoOfCases=outWardDispatchEnteryRegistrationObj.getNoOfCases();
			if(outWardDispatchEnteryRegistrationObj!=null)
			{
				outWardDispatchEnteryRegistrationObj.setNoOfCases(newNoOfCases);
				session.update(outWardDispatchEnteryRegistrationObj);
				if(outWardDispatchEnteryRegistrationObj.getOutWardTrip()!=null)
				{
					OutWardTrip outWardTripObj=outWardDispatchEnteryRegistrationObj.getOutWardTrip();
					Integer con=oldNoOfCases-newNoOfCases;
					if(con<0)
					{
						Integer newOutWardTripCases=outWardTripObj.getNoOfCases()-con;
						outWardTripObj.setNoOfCases(newOutWardTripCases);
						session.update(outWardTripObj);
					}
					else
					{
						outWardTripObj.setNoOfCases(outWardTripObj.getNoOfCases()-con);
						session.update(outWardTripObj);
					}
				}
				return true;
			}
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#loadOutWardDispatchEnteryRegistrationObjUsignOutWardGatePassId(java.lang.Integer)
	 */
	@Override
	public OutWardDispatchEnteryRegistration loadOutWardDispatchEnteryRegistrationObjUsignOutWardGatePassId(
			Integer outWardGetPassId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
					.createAlias("OutWardDispatchEnteryRegistration.outWardGatePass", "outWardGatePass")
					.add(Restrictions.eq("outWardGatePass.outWardGatePassId", outWardGetPassId))
				     .add(Restrictions.eq("status", 1));
			outWardDispatchEnteryRegistrationObj=(OutWardDispatchEnteryRegistration) criteria.uniqueResult();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardDispatchEnteryRegistrationObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#updateOutWardDispatchEnteryRegistrationObjDetails(com.protocol.wms.model.OutWardDispatchEnteryRegistration)
	 */
	@Override
	public boolean updateOutWardDispatchEnteryRegistrationObjDetails(
			OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(outWardDispatchEnteryRegistrationObj);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
	@Override
	public Map<String,Object> searchOutwardAddTrip(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		Map<String,Object>map=null;
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList=null;
		try 
		{
			if ("InvoiceNo".equals(selectedValue))
			{
				List<Integer> outWardDispatchEnteryRegIdList = null;
				Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"InvoiceEntry")
					     .createAlias("InvoiceEntry.outWardDispatchEnteryRegistration", "outWardDispatchEnteryRegistration")
					     .createAlias("InvoiceEntry.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.like("InvoiceEntry.invoiceNo", searchText,MatchMode.START))
					     .setProjection(Projections.distinct(Projections.property("outWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId")));
				
				if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				Long totalRecordCount =0l;
				outWardDispatchEnteryRegIdList = criteria.list();
				if(outWardDispatchEnteryRegIdList.size()>0){
					criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
						     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.eq("tripStatus", 0))
						     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegIdList))
						     .addOrder(Order.desc("submitDate"))
						       .setProjection(Projections.rowCount());
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   //----------------------------------------------------------
				
				   totalRecordCount = (Long)criteria.uniqueResult();
				  
					criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
						     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.eq("tripStatus", 0))
						     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegIdList))
						     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			   
					from = from - 1 ;
				    criteria.setFirstResult(from*Constant.PAGE_COUNT);
				    criteria.setMaxResults(Constant.PAGE_COUNT);
				    outWardDispatchEnteryRegistrationList = criteria.list();
				} 
				Integer totalPages = 0;
				if(totalRecordCount>0){
					totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPages=totalPages+1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardDispatchEnteryRegistrationList", outWardDispatchEnteryRegistrationList);
				map.put("totalPages", totalPages);
			 
			}
			else{
				Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("tripStatus", 0))
				     .addOrder(Order.desc("submitDate")).setProjection(Projections.rowCount());
			
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   switch(selectedValue)
					{	
					case "Company" : 
						 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
						 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
						 break;
						 
					case "Organization" :
						 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
						 break;
						 
					case "Stockist" :
						  criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
						  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
						  break;
						  
					case "NoOfCases" :
						  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
						  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
						  break;
						  
					case "Transporter" :
						  criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
						  criteria.createAlias("stockist.transporterDetails", "transporterDetails");
						  criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
						  break;
					}
				  
				   //----------------------------------------------------------------------------------------------
					 Long totalRecordCount = (Long)criteria.uniqueResult();
						//------------ Insert fetch data here
				criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
						     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.eq("tripStatus", 0))
						     .addOrder(Order.desc("submitDate"));
					
						   if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   
						   switch(selectedValue)
							{	
							case "Company" : 
								 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
								 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
								 break;
								 
							case "Organization" :
								 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
								 break;
								 
							case "Stockist" :
								  criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
								  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
								  break;
								  
							case "NoOfCases" :
								  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
								  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
								  break;
								  
							case "Transporter" :
								  criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
								  criteria.createAlias("stockist.transporterDetails", "transporterDetails");
								  criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
								  break;
							}
				   
		from = from - 1 ;
		    criteria.setFirstResult(from*Constant.PAGE_COUNT);
		    criteria.setMaxResults(Constant.PAGE_COUNT);
		    outWardDispatchEnteryRegistrationList=criteria.list();
			Integer totalPages = 0;
			if(totalRecordCount>0){
				totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPages=totalPages+1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("outWardDispatchEnteryRegistrationList", outWardDispatchEnteryRegistrationList);
			map.put("totalPages", totalPages);	
		}
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	
	}
	
	
	@Override
	public Map<String,Object> searchOutwardViewDispatchEntryRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList = null;
		Map<String,Object>map=null;
		
		try 
		{
			if("InvoiceNumber".equals(selectedValue)||"NetAmount".equals(selectedValue)||"InvoiceDate".equals(selectedValue))
			{

				List<Integer> outWardDispatchEnteryRegIdList = null;
				
				Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"InvoiceEntry")
						 .createCriteria("InvoiceEntry.outWardDispatchEnteryRegistration","outWardDispatchEnteryRegistration")
					     .createAlias("InvoiceEntry.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .setProjection(Projections.distinct(Projections.property("outWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId")));
				 
				if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				
				switch(selectedValue)
				{	
				case "InvoiceNumber":
					 criteria.add(Restrictions.like("InvoiceEntry.invoiceNo", searchText,MatchMode.START));
					 break;
					 
			 	case "NetAmount":
			 		  criteria.add(Restrictions.ge("InvoiceEntry.netAmount", Double.parseDouble(fromSpecialData)));
					  criteria.add(Restrictions.le("InvoiceEntry.netAmount", Double.parseDouble(toSpecialData)));
					  break;
					  
			 	case "InvoiceDate":
			 		if(fromSpecialData!=""&&toSpecialData!=""){
					    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					    java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
						criteria.add(Restrictions.between("InvoiceEntry.invoiceDate", sqlFromDate,sqlToDate));
					  }
				}
				outWardDispatchEnteryRegIdList = criteria.list();
				Long totalRecordCount = 0l;
				if(outWardDispatchEnteryRegIdList.size()>0){
					criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
					     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegIdList))
					     .addOrder(Order.desc("submitDate"))
					     .add(Restrictions.eq("printStickerStatus", 0))
					     .setProjection(Projections.rowCount());
				
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   totalRecordCount = (Long)criteria.uniqueResult();
				
					criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
					     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegIdList))
					     .add(Restrictions.eq("printStickerStatus", 0))
					     .addOrder(Order.desc("submitDate"));
				
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   from = from - 1 ;
					    criteria.setFirstResult(from*Constant.PAGE_COUNT);
					    criteria.setMaxResults(Constant.PAGE_COUNT);				   
					   outWardDispatchEnteryRegistrationList = criteria.list();
				}
			    Integer totalPage = 0;
				if(totalRecordCount>0){
					totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPage = totalPage +1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardDispatchEnteryRegistration", outWardDispatchEnteryRegistrationList);
				map.put("totalPages", totalPage );
			}
			else if("OrderId".equals(selectedValue)||"OrderNo".equals(selectedValue)){
				
				List<Integer> outWardOrderEnteryRegIdList = null;
				
				Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outWardOrderEntryRegistration")
					     .createAlias("outWardOrderEntryRegistration.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .setProjection(Projections.distinct(Projections.property("outWardOrderEntryRegistration.outWardOrderEnteryRegistrationId")));
				 
					if(organizationId!=null)
						criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				 
				 	switch(selectedValue){
					 	case "OrderId":
					 			criteria.add(Restrictions.like("outWardOrderEntryRegistration.orderId", searchText,MatchMode.START));break;
					 			
					 	case "OrderNo":
					 			criteria.add(Restrictions.like("outWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));break;
				 	}
				 	
				 	outWardOrderEnteryRegIdList = criteria.list();
				 	Long totalRecordCount=0l;
					if(outWardOrderEnteryRegIdList.size()>0){
					 	criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"InvoiceEntry")
								 .createCriteria("InvoiceEntry.outWardDispatchEnteryRegistration","outWardDispatchEnteryRegistration")
								 .createCriteria("InvoiceEntry.outWardOrderEnteryRegistration","outWardOrderEnteryRegistration")
							     .createAlias("InvoiceEntry.organization", "organization")
							     .add(Restrictions.eq("status", 1))
							     .add(Restrictions.in("outWardOrderEnteryRegistration.outWardOrderEnteryRegistrationId", outWardOrderEnteryRegIdList))
							     .setProjection(Projections.distinct(Projections.property("outWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId")));
						 
						if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
							   
					    List<Integer> outWardDispatchEnteryRegistrationIdList = criteria.list();
					   
					    if(outWardDispatchEnteryRegistrationIdList.size()>0){
							  criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
									     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
									     .add(Restrictions.eq("status", 1))
									     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegistrationIdList))
									     .addOrder(Order.desc("submitDate"))
									     .add(Restrictions.eq("printStickerStatus", 0))
									     .setProjection(Projections.rowCount());
								
									   if(organizationId!=null)
									     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
									   
							totalRecordCount = (Long)criteria.uniqueResult();
							
						  criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
								     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
								     .add(Restrictions.eq("status", 1))
								     .add(Restrictions.eq("printStickerStatus", 0))
								     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegistrationIdList))
								     .addOrder(Order.desc("submitDate"));
							
						   if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   
						   	from = from - 1 ;
						    criteria.setFirstResult(from*Constant.PAGE_COUNT);
						    criteria.setMaxResults(Constant.PAGE_COUNT);
						   outWardDispatchEnteryRegistrationList = criteria.list();
					    }
				}
			   Integer totalPage = 0;
				if(totalRecordCount>0){
					totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPage = totalPage +1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardDispatchEnteryRegistration", outWardDispatchEnteryRegistrationList);
				map.put("totalPages", totalPage );
			}
			else
			{
				Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
					     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.eq("printStickerStatus", 0))
					     .addOrder(Order.desc("submitDate"))
					     .setProjection(Projections.rowCount());
					 
					if(organizationId!=null){
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					}
						   
					switch(selectedValue)
					 {
					 	case "Company" : 
							 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
							 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
							 
						case "Organization" :
							 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
							 
						case "Stockist" :
							  criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
							  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
							  break;
							  
						case "NoOfCases" :
							  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
							  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
							  break;
					 }
				
					Long totalRecordCount = (Long)criteria.uniqueResult();
				
				  criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("printStickerStatus", 0))
				     .addOrder(Order.desc("submitDate"));
				 
				if(organizationId!=null){
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				}
					   
				switch(selectedValue)
				 {
				 	case "Company" : 
						 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
						 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
						 break;
						 
					case "Organization" :
						 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
						 break;
						 
					case "Stockist" :
						  criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
						  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
						  break;
						  
					case "NoOfCases" :
						  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
						  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
						  break;
				 }
				from = from - 1 ;
			    criteria.setFirstResult(from*Constant.PAGE_COUNT);
			    criteria.setMaxResults(Constant.PAGE_COUNT);
				outWardDispatchEnteryRegistrationList = criteria.list();
				
				Integer totalPage = 0;
				if(totalRecordCount>0){
					totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPage = totalPage +1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardDispatchEnteryRegistration", outWardDispatchEnteryRegistrationList);
				map.put("totalPages", totalPage );
			}
		}
			catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
	
	@Override
	public Map<String,Object> searchOutwardGetPassList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer from,Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList=null;
		Map<String,Object>map=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("tripStatus", 1))
				     .add(Restrictions.eq("getPassStatus", 0))
				     .setProjection(Projections.rowCount());
			
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   switch(selectedValue)
				   {
				   case "TripNo":
					   //	 criteria.createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip");
						 criteria.add(Restrictions.like("outWardTrip.tripNo", searchText,MatchMode.START));
						 break;
						 
				   case "Organization":
						 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
						 break;
						 
				   case "Company":
					   	 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
						 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
						 break;
						 
				   case "Stockist":
					   	 criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
						 criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
						 break;
						 
				   case "NoOfCases":
					   	  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
						  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
						  break;
						 
				   case "DispatchBy":
					   //	 criteria.createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip");
						 criteria.add(Restrictions.like("outWardTrip.dispatchBy", searchText,MatchMode.START));
						 break;
				   }
				   Long totalRecordCount = (Long)criteria.uniqueResult();
				   //-----------------------------------------------------------------
				   
				 criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
						     .createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip")
						     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.eq("tripStatus", 1))
						     .add(Restrictions.eq("getPassStatus", 0))
						     .addOrder(Order.desc("submitDate"));
					
			   if(organizationId!=null)
			     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			   
			   switch(selectedValue)
			   {
			   case "TripNo":
				   //	 criteria.createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip");
					 criteria.add(Restrictions.like("outWardTrip.tripNo", searchText,MatchMode.START));
					 break;
					 
			   case "Organization":
					 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					 break;
					 
			   case "Company":
				   	 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
					 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
					 break;
					 
			   case "Stockist":
				   	 criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
					 criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
					 break;
					 
			   case "NoOfCases":
				   	  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
					  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
					  break;
					 
			   case "DispatchBy":
				   //	 criteria.createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip");
					 criteria.add(Restrictions.like("outWardTrip.dispatchBy", searchText,MatchMode.START));
					 break;
			   }
			   
			   //-------------------------------------------------------------------
	
			    from = from - 1 ;
			    criteria.setFirstResult(from*Constant.PAGE_COUNT);
			    criteria.setMaxResults(Constant.PAGE_COUNT);
			    outWardDispatchEnteryRegistrationList = criteria.list();
					 
				Integer totalPages = 0;
				if(totalRecordCount>0){
					totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPages=totalPages+1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardDispatchEnteryRegistrationList", outWardDispatchEnteryRegistrationList);
				map.put("totalPages", totalPages);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
	
	
	@Override
	public Map<String,Object> searchViewGetPassLRList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId) {
		
		Session session =sessionFactory.getCurrentSession();
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList=null;
		Map<String,Object>map=null;
		try 
		{
			if("InvoiceNo".equals(selectedValue)){

					List<Integer> outWardDispatchEnteryRegIdList = null;
				
				Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"InvoiceEntry")
						 .createCriteria("InvoiceEntry.outWardDispatchEnteryRegistration","outWardDispatchEnteryRegistration")
					     .createAlias("InvoiceEntry.organization", "organization")
					     .add(Restrictions.like("InvoiceEntry.invoiceNo", searchText,MatchMode.START))
					     .add(Restrictions.eq("status", 1))
					     .setProjection(Projections.distinct(Projections.property("outWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId")));
				
				if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				
				outWardDispatchEnteryRegIdList = criteria.list();
				Long totalRecordCount = 0l;
				if(outWardDispatchEnteryRegIdList.size()>0){
					criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
						     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.eq("getPassStatus", 1))
						     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegIdList))
						     .addOrder(Order.desc("submitDate"))
						      .setProjection(Projections.rowCount());
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   totalRecordCount = (Long)criteria.uniqueResult();
					
					criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
						     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.eq("getPassStatus", 1))
						     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegIdList))
						     .addOrder(Order.desc("submitDate"));
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					
				   from = from - 1 ;
				    criteria.setFirstResult(from*Constant.PAGE_COUNT);
				    criteria.setMaxResults(Constant.PAGE_COUNT);
				    outWardDispatchEnteryRegistrationList=criteria.list();
				}
					Integer totalPages = 0;
					if(totalRecordCount>0){
						totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
						if((totalRecordCount%Constant.PAGE_COUNT)!=0){
							totalPages=totalPages+1;
						}
					}
					map = new HashMap<String, Object>();
					map.put("outWardDispatchEnteryRegistrationList", outWardDispatchEnteryRegistrationList);
					map.put("totalPages", totalPages);
			}
		else{
			Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
					 .createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("getPassStatus", 1))
				     .addOrder(Order.desc("submitDate"))
				     .setProjection(Projections.rowCount());
			
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   switch(selectedValue)
				   {
				   case "TripNo":
					   	criteria.add(Restrictions.like("outWardTrip.tripNo",searchText,MatchMode.START));
					   	break;
					   
				   case "Organization":
						 criteria.add(Restrictions.like("organization.organizationName",searchText,MatchMode.START));
						 break;
						   
				   case "Company":
					   	 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
						 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
						 break;
							   
				   case "Stockist":
					     criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
						 criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
						 break;
								   
				   case "Transporter":
					    criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
					    criteria.createAlias("stockist.transporterDetails", "transporterDetails");
						criteria.add(Restrictions.like("transporterDetails.transporterName",searchText,MatchMode.START));
						break;
						
				   case "NoOfCases":
					   	  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
						  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
						  break;
						  
				   case "LRNo":
					   	 criteria.createAlias("OutWardDispatchEnteryRegistration.outWardGatePass", "outWardGatePass");
						 criteria.add(Restrictions.like("outWardGatePass.LRNo", searchText,MatchMode.START));
						 break;
				   }
				   
				   
				   Long totalRecordCount = (Long)criteria.uniqueResult();
				   
				   //-----------------------------------------------------------------------
				   
				   
				   criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
							 .createAlias("OutWardDispatchEnteryRegistration.outWardTrip", "outWardTrip")
						     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
						     .add(Restrictions.eq("status", 1))
						     .add(Restrictions.eq("getPassStatus", 1))
						     .addOrder(Order.desc("submitDate"));
					
						   if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   
						   switch(selectedValue)
						   {
						   case "TripNo":
							   	criteria.add(Restrictions.like("outWardTrip.tripNo",searchText,MatchMode.START));
							   	break;
							   
						   case "Organization":
								 criteria.add(Restrictions.like("organization.organizationName",searchText,MatchMode.START));
								 break;
								   
						   case "Company":
							   	 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
								 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
								 break;
									   
						   case "Stockist":
							     criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
								 criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
								 break;
										   
						   case "Transporter":
							    criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
							    criteria.createAlias("stockist.transporterDetails", "transporterDetails");
								criteria.add(Restrictions.like("transporterDetails.transporterName",searchText,MatchMode.START));
								break;
								
						   case "NoOfCases":
							   	  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
								  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
								  break;
								  
						   case "LRNo":
							   	 criteria.createAlias("OutWardDispatchEnteryRegistration.outWardGatePass", "outWardGatePass");
								 criteria.add(Restrictions.like("outWardGatePass.LRNo", searchText,MatchMode.START));
								 break;
						   }
			from = from - 1 ;
		    criteria.setFirstResult(from*Constant.PAGE_COUNT);
		    criteria.setMaxResults(Constant.PAGE_COUNT);
		    outWardDispatchEnteryRegistrationList=criteria.list();
				 
			Integer totalPages = 0;
			if(totalRecordCount>0){
				totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPages=totalPages+1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("outWardDispatchEnteryRegistrationList", outWardDispatchEnteryRegistrationList);
			map.put("totalPages", totalPages);
			}
		}
		
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardDispatchEnteryRegistrationDao#deleteOutWardDispatchEnteryRegistrationObjDetails(com.protocol.wms.model.OutWardDispatchEnteryRegistration)
	 */
	@Override
	public boolean deleteOutWardDispatchEnteryRegistrationObjDetails(
			OutWardDispatchEnteryRegistration outWardDispatchEnteryRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.delete(outWardDispatchEnteryRegistrationObj);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	@Override
	public Map<String, Object> searchOutwardViewPrintStickerEntryRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer from, Integer organizationId) {

		
		Session session =sessionFactory.getCurrentSession();
		List<OutWardDispatchEnteryRegistration> outWardDispatchEnteryRegistrationList = null;
		Map<String,Object>map=null;
		
		try 
		{
			if("InvoiceNumber".equals(selectedValue)||"NetAmount".equals(selectedValue)||"InvoiceDate".equals(selectedValue))
			{

				List<Integer> outWardDispatchEnteryRegIdList = null;
				
				Criteria criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"InvoiceEntry")
						 .createCriteria("InvoiceEntry.outWardDispatchEnteryRegistration","outWardDispatchEnteryRegistration")
					     .createAlias("InvoiceEntry.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .setProjection(Projections.distinct(Projections.property("outWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId")));
				 
				if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				
				switch(selectedValue)
				{	
				case "InvoiceNumber":
					 criteria.add(Restrictions.like("InvoiceEntry.invoiceNo", searchText,MatchMode.START));
					 break;
					 
			 	case "NetAmount":
			 		  criteria.add(Restrictions.ge("InvoiceEntry.netAmount", Double.parseDouble(fromSpecialData)));
					  criteria.add(Restrictions.le("InvoiceEntry.netAmount", Double.parseDouble(toSpecialData)));
					  break;
					  
			 	case "InvoiceDate":
			 		if(fromSpecialData!=""&&toSpecialData!=""){
					    DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
					    java.util.Date d1= null;
						d1 = dtf.parse(fromSpecialData);
						java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
						d1 = dtf.parse(toSpecialData);
						java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
						
						criteria.add(Restrictions.between("InvoiceEntry.invoiceDate", sqlFromDate,sqlToDate));
					  }
				}
				outWardDispatchEnteryRegIdList = criteria.list();
				Long totalRecordCount = 0l;
				if(outWardDispatchEnteryRegIdList.size()>0){
					criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
					     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegIdList))
					     .addOrder(Order.desc("submitDate"))
					     .add(Restrictions.eq("printStickerStatus", 1))
					     .setProjection(Projections.rowCount());
				
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   totalRecordCount = (Long)criteria.uniqueResult();
				
					criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
					     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegIdList))
					     .add(Restrictions.eq("printStickerStatus", 1))
					     .addOrder(Order.desc("submitDate"));
				
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   from = from - 1 ;
					    criteria.setFirstResult(from*Constant.PAGE_COUNT);
					    criteria.setMaxResults(Constant.PAGE_COUNT);				   
					   outWardDispatchEnteryRegistrationList = criteria.list();
				}
			    Integer totalPage = 0;
				if(totalRecordCount>0){
					totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPage = totalPage +1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardDispatchEnteryRegistration", outWardDispatchEnteryRegistrationList);
				map.put("totalPages", totalPage );
			}
			else if("OrderId".equals(selectedValue)||"OrderNo".equals(selectedValue)){
				
				List<Integer> outWardOrderEnteryRegIdList = null;
				
				Criteria criteria = session.createCriteria(OutWardOrderEntryRegistration.class,"outWardOrderEntryRegistration")
					     .createAlias("outWardOrderEntryRegistration.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .setProjection(Projections.distinct(Projections.property("outWardOrderEntryRegistration.outWardOrderEnteryRegistrationId")));
				 
					if(organizationId!=null)
						criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				 
				 	switch(selectedValue){
					 	case "OrderId":
					 			criteria.add(Restrictions.like("outWardOrderEntryRegistration.orderId", searchText,MatchMode.START));break;
					 			
					 	case "OrderNo":
					 			criteria.add(Restrictions.like("outWardOrderEntryRegistration.orderNo", searchText,MatchMode.START));break;
				 	}
				 	
				 	outWardOrderEnteryRegIdList = criteria.list();
				 	Long totalRecordCount=0l;
					if(outWardOrderEnteryRegIdList.size()>0){
					 	criteria = session.createCriteria(OutWardOrderInvoiceEnteryRegistration.class,"InvoiceEntry")
								 .createCriteria("InvoiceEntry.outWardDispatchEnteryRegistration","outWardDispatchEnteryRegistration")
								 .createCriteria("InvoiceEntry.outWardOrderEnteryRegistration","outWardOrderEnteryRegistration")
							     .createAlias("InvoiceEntry.organization", "organization")
							     .add(Restrictions.eq("status", 1))
							     .add(Restrictions.in("outWardOrderEnteryRegistration.outWardOrderEnteryRegistrationId", outWardOrderEnteryRegIdList))
							     .setProjection(Projections.distinct(Projections.property("outWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId")));
						 
						if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
							   
					    List<Integer> outWardDispatchEnteryRegistrationIdList = criteria.list();
					   
					    if(outWardDispatchEnteryRegistrationIdList.size()>0){
							  criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
									     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
									     .add(Restrictions.eq("status", 1))
									     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegistrationIdList))
									     .addOrder(Order.desc("submitDate"))
									     .add(Restrictions.eq("printStickerStatus", 1))
									     .setProjection(Projections.rowCount());
								
									   if(organizationId!=null)
									     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
									   
							totalRecordCount = (Long)criteria.uniqueResult();
							
						  criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
								     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
								     .add(Restrictions.eq("status", 1))
								     .add(Restrictions.eq("printStickerStatus", 1))
								     .add(Restrictions.in("OutWardDispatchEnteryRegistration.outWardDispatchEnteryRegistrationId", outWardDispatchEnteryRegistrationIdList))
								     .addOrder(Order.desc("submitDate"));
							
						   if(organizationId!=null)
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
						   
						   	from = from - 1 ;
						    criteria.setFirstResult(from*Constant.PAGE_COUNT);
						    criteria.setMaxResults(Constant.PAGE_COUNT);
						   outWardDispatchEnteryRegistrationList = criteria.list();
					    }
				}
			   Integer totalPage = 0;
				if(totalRecordCount>0){
					totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPage = totalPage +1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardDispatchEnteryRegistration", outWardDispatchEnteryRegistrationList);
				map.put("totalPages", totalPage );
			}
			else
			{
				Criteria criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
					     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .add(Restrictions.eq("printStickerStatus", 1))
					     .addOrder(Order.desc("submitDate"))
					     .setProjection(Projections.rowCount());
					 
					if(organizationId!=null){
						     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					}
						   
					switch(selectedValue)
					 {
					 	case "Company" : 
							 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
							 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
							 break;
							 
						case "Organization" :
							 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
							 break;
							 
						case "Stockist" :
							  criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
							  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
							  break;
							  
						case "NoOfCases" :
							  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
							  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
							  break;
					 }
				
					Long totalRecordCount = (Long)criteria.uniqueResult();
				
				  criteria = session.createCriteria(OutWardDispatchEnteryRegistration.class,"OutWardDispatchEnteryRegistration")
				     .createAlias("OutWardDispatchEnteryRegistration.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("printStickerStatus", 1))
				     .addOrder(Order.desc("submitDate"));
				 
				if(organizationId!=null){
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				}
					   
				switch(selectedValue)
				 {
				 	case "Company" : 
						 criteria.createAlias("OutWardDispatchEnteryRegistration.company", "company");
						 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
						 break;
						 
					case "Organization" :
						 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
						 break;
						 
					case "Stockist" :
						  criteria.createAlias("OutWardDispatchEnteryRegistration.stockist", "stockist");
						  criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
						  break;
						  
					case "NoOfCases" :
						  criteria.add(Restrictions.ge("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((fromSpecialData))));
						  criteria.add(Restrictions.le("OutWardDispatchEnteryRegistration.noOfCases", Integer.parseInt((toSpecialData))));
						  break;
				 }
				from = from - 1 ;
			    criteria.setFirstResult(from*Constant.PAGE_COUNT);
			    criteria.setMaxResults(Constant.PAGE_COUNT);
				outWardDispatchEnteryRegistrationList = criteria.list();
				
				Integer totalPage = 0;
				if(totalRecordCount>0){
					totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPage = totalPage +1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("outWardDispatchEnteryRegistration", outWardDispatchEnteryRegistrationList);
				map.put("totalPages", totalPage );
			}
		}
			catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	
	}
  
}
