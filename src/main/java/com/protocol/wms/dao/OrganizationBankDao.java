/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.OrganizationBank;

/**
 * @author Sudhakar
 *
 */
public interface OrganizationBankDao {

	public Integer saveOrganizationBank(OrganizationBank organizationBank);
	
	public List<OrganizationBank> loadOrganizationBanksListUsingOrganizationId(Integer organizationId);
	
	public Map<String,Object> searchOrganiztionBankDetails(String searchOption,String searchText,Integer from, Integer organizationId);
	
	public Boolean deleteOrganizationBank(Integer organizationBankId);
	
	public OrganizationBank loadOrganizationBankObjectUsingOrganizationBankId(Integer organizationBankId);
	
	public Boolean updateOrganizationBankObj(OrganizationBank organizationBankObj);

	//public List<String> getOrganizationBankNameUsingBankIds(List<Integer> OrganizationBankIdList);
}
