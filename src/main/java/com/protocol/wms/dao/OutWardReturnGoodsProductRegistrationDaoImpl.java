/**
 * 
 */
package com.protocol.wms.dao;

import org.springframework.stereotype.Repository;


/**
 * @author admin
 *
 */
@Repository(value="OutWardReturnGoodsProductRegistrationDaoImpl")
public class OutWardReturnGoodsProductRegistrationDaoImpl implements
		OutWardReturnGoodsProductRegistrationDao {

	/*private Logger log = Logger.getLogger(OutWardReturnGoodsProductRegistrationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	 (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardReturnGoodsProductRegistrationDao#outWardReturnGoodsProductRegistrationList(java.lang.Integer)
	 
	@Override
	public List<OutWardReturnGoodsProductRegistration> outWardReturnGoodsProductRegistrationList(
			Integer outWardReturnGoodsLRDetailsId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardReturnGoodsProductRegistration> productList = null;
		try
		{
			Criteria criteria = session.createCriteria(OutWardReturnGoodsProductRegistration.class,"productRegDetals")
				     .createAlias("productRegDetals.outWardReturnGoodsLRDetails", "LR_Details")
				     .add(Restrictions.eq("LR_Details.outWardReturnGoodsLRDetailsId", outWardReturnGoodsLRDetailsId))
				     .add(Restrictions.eq("status", 1));
				   productList = criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return productList;
	}
	 (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardReturnGoodsProductRegistrationDao#deleteOutwardReturnGoodsRegLRProduct(java.lang.Integer)
	 
	@Override
	public Boolean deleteOutwardReturnGoodsRegLRProduct(Integer PRODUCT_ID) {
		Session session =sessionFactory.getCurrentSession();
		OutWardReturnGoodsProductRegistration outWardReturnGoodsProductRegistration= null;
		List<OutWardReturnGoodsProductRegistration>outwardReturnProductsList = null;
		try{
			outWardReturnGoodsProductRegistration = (OutWardReturnGoodsProductRegistration)session.get(OutWardReturnGoodsProductRegistration.class, PRODUCT_ID);
			outWardReturnGoodsProductRegistration.setStatus(0);
			session.update(outWardReturnGoodsProductRegistration);
			
			Criteria criteria = session.createCriteria(OutWardReturnGoodsProductRegistration.class,"outwardReturnProduct")
				     .createAlias("outwardReturnProduct.outWardReturnGoodsLRDetails", "LR_Details")
				     .add(Restrictions.eq("LR_Details.outWardReturnGoodsLRDetailsId",outWardReturnGoodsProductRegistration.getOutWardReturnGoodsLRDetails().getOutWardReturnGoodsLRDetailsId()))
				     .add(Restrictions.eq("status", 1));
			outwardReturnProductsList = criteria.list();
			if(!(outwardReturnProductsList.size()>0)){
				OutWardReturnGoodsLRDetails tempObj = (OutWardReturnGoodsLRDetails)session.load(OutWardReturnGoodsLRDetails.class, outWardReturnGoodsProductRegistration.getOutWardReturnGoodsLRDetails().getOutWardReturnGoodsLRDetailsId());
				tempObj.setStatus(0);
				session.update(tempObj);
				
				criteria = session.createCriteria(OutWardReturnGoodsLRDetails.class,"LRDetails")
					     .createAlias("LRDetails.outWardReturnGoodsRegistration", "outWardReturnGoodsRegistration")
					     .add(Restrictions.eq("outWardReturnGoodsRegistration.outWardReturnGoodsRegistrationId",tempObj.getOutWardReturnGoodsRegistration().getOutWardReturnGoodsRegistrationId()))
					     .add(Restrictions.eq("status", 1));
				List<OutWardReturnGoodsLRDetails> lrDetailsList = criteria.list();
				if(!(lrDetailsList.size()>0)){
					OutWardReturnGoodsRegistration tempObj1 = (OutWardReturnGoodsRegistration)session.load(OutWardReturnGoodsRegistration.class, tempObj.getOutWardReturnGoodsRegistration().getOutWardReturnGoodsRegistrationId());
					tempObj1.setStatus(0);
					session.update(tempObj1);
				}
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
		return null;
	}
*/
}
