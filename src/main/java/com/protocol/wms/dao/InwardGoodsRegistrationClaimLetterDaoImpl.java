/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.InwardGoodsRegistrationClaimLetter;
import com.protocol.wms.model.InwardGoodsRegistrationClaimLetterImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationClaimPicturesImagePath;

/**
 * @author Sudhakar
 *
 */
@Repository("InwardGoodsRegistrationClaimLetterDaoImpl")
public class InwardGoodsRegistrationClaimLetterDaoImpl implements InwardGoodsRegistrationClaimLetterDao{

	private Logger log = Logger.getLogger(InwardGoodsRegistrationClaimLetterDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationClaimLetterDao#saveInwardGoodsRegistrationClaimLetter(com.protocol.wms.model.InwardGoodsRegistrationClaimLetter)
	 */
	@Override
	public Integer saveInwardGoodsRegistrationClaimLetter(
			InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetterObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer inwardGoodsRegistrationClaimLetterId = null;
		try 
		{
			inwardGoodsRegistrationClaimLetterId=(Integer) session.save(inwardGoodsRegistrationClaimLetterObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardGoodsRegistrationClaimLetterId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationClaimLetterDao#getInwardGoodsRegistrationClaimLetterObj(java.lang.Integer)
	 */
	@Override
	public InwardGoodsRegistrationClaimLetter getInwardGoodsRegistrationClaimLetterObj(
			Integer inwardGoodsRegistrationClaimLetterId) {
		InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetter = null;
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			inwardGoodsRegistrationClaimLetter = (InwardGoodsRegistrationClaimLetter) session.get(InwardGoodsRegistrationClaimLetter.class, inwardGoodsRegistrationClaimLetterId);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardGoodsRegistrationClaimLetter;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationClaimLetterDao#editInwardGoodsRegistrationClaimLetter(com.protocol.wms.model.InwardGoodsRegistrationClaimLetter)
	 */
	@Override
	public Boolean editInwardGoodsRegistrationClaimLetter(InwardGoodsRegistrationClaimLetter claimObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(claimObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationClaimLetterDao#deleteInwardGoodsRegistrationClaimLetterImage(java.lang.Integer)
	 */
	@Override
	public Boolean deleteInwardGoodsRegistrationClaimLetterImage(
			Integer claimLetterImageId) {
		Session session =sessionFactory.getCurrentSession();
		InwardGoodsRegistrationClaimLetterImagePath inwardGoodsRegistrationClaimLetterImagePath= null;
		try{
			inwardGoodsRegistrationClaimLetterImagePath = (InwardGoodsRegistrationClaimLetterImagePath)session.load(InwardGoodsRegistrationClaimLetterImagePath.class, claimLetterImageId);
			session.delete(inwardGoodsRegistrationClaimLetterImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationClaimLetterDao#deleteInwardGoodsRegistrationClaimPictureImage(java.lang.Integer)
	 */
	@Override
	public Boolean deleteInwardGoodsRegistrationClaimPictureImage(
			Integer claimPictureImageId) {
		Session session =sessionFactory.getCurrentSession();
		InwardGoodsRegistrationClaimPicturesImagePath inwardGoodsRegistrationClaimPicturesImagePath= null;
		try{
			inwardGoodsRegistrationClaimPicturesImagePath = (InwardGoodsRegistrationClaimPicturesImagePath)session.load(InwardGoodsRegistrationClaimPicturesImagePath.class, claimPictureImageId);
			session.delete(inwardGoodsRegistrationClaimPicturesImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationClaimLetterDao#updateInwardGoodsRegClaimLetterImage(java.lang.Integer, java.util.List)
	 */
	@Override
	public List<InwardGoodsRegistrationClaimLetterImagePath> updateInwardGoodsRegClaimLetterImage(
			Integer inwardGoodsRegistrationClaimLetterId,
			List<InwardGoodsRegistrationClaimLetterImagePath> claimLetterImgList) {
		InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetter = null;
		List<InwardGoodsRegistrationClaimLetterImagePath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			inwardGoodsRegistrationClaimLetter = (InwardGoodsRegistrationClaimLetter) session.load(InwardGoodsRegistrationClaimLetter.class, inwardGoodsRegistrationClaimLetterId);
			for(InwardGoodsRegistrationClaimLetterImagePath tempObj : claimLetterImgList){
				tempObj.setInwardGoodsRegistrationClaimLetter(inwardGoodsRegistrationClaimLetter);
				session.save(tempObj);
			}
			lst = inwardGoodsRegistrationClaimLetter.getInwardGoodsRegistrationClaimLetterImagePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardGoodsRegistrationClaimLetterDao#updateInwardGoodsRegClaimPicturesImage(java.lang.Integer, java.util.List)
	 */
	@Override
	public List<InwardGoodsRegistrationClaimPicturesImagePath> updateInwardGoodsRegClaimPicturesImage(
			Integer inwardGoodsRegistrationClaimLetterId,
			List<InwardGoodsRegistrationClaimPicturesImagePath> claimPicturesImgList) {
		InwardGoodsRegistrationClaimLetter inwardGoodsRegistrationClaimLetter = null;
		List<InwardGoodsRegistrationClaimPicturesImagePath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			inwardGoodsRegistrationClaimLetter = (InwardGoodsRegistrationClaimLetter) session.load(InwardGoodsRegistrationClaimLetter.class, inwardGoodsRegistrationClaimLetterId);
			for(InwardGoodsRegistrationClaimPicturesImagePath tempObj : claimPicturesImgList){
				tempObj.setInwardGoodsRegistrationClaimLetter(inwardGoodsRegistrationClaimLetter);
				session.save(tempObj);
			}
			lst = inwardGoodsRegistrationClaimLetter.getInwardGoodsRegistrationClaimPicturesImagePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}


	
}
