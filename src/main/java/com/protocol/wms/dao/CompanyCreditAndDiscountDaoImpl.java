/**
 * 
 */
package com.protocol.wms.dao;

import groovy.util.OrderBy;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.Organization;

/**
 * @author Sudhakar
 *
 */
@Repository(value="CompanyCreditAndDiscountDaoImpl")
public class CompanyCreditAndDiscountDaoImpl implements CompanyCreditAndDiscountDao {
	
	private Logger log = Logger.getLogger(CompanyCreditAndDiscountDaoImpl.class.getClass());

	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyCreditAndDiscountDao#loadCompanyCreditAndDiscountObjectUsignComapnyId(java.lang.Integer)
	 */
	@Override
	public CompanyCreditAndDiscount loadCompanyCreditAndDiscountObjectUsignComapnyId(Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyCreditAndDiscount companyCreditAndDiscountObj=null;
		try 
		{
			Criteria criteria = session.createCriteria(CompanyCreditAndDiscount.class,"CompanyCreditAndDiscount")
					.createAlias("CompanyCreditAndDiscount.company", "company")
					.add(Restrictions.eq("company.companyId", companyId))
					.add(Restrictions.eq("status", 1));
				
			companyCreditAndDiscountObj=(CompanyCreditAndDiscount) criteria.uniqueResult();
			
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return companyCreditAndDiscountObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyCreditAndDiscountDao#getCompanyCreditAndDiscountDetailsById(java.lang.Integer)
	 */
	@Override
	public CompanyCreditAndDiscount getCompanyCreditAndDiscountDetailsById(Integer creditAndDiscountId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyCreditAndDiscount companyCreditAndDiscountObj = null;
		try 
		{
			companyCreditAndDiscountObj=(CompanyCreditAndDiscount) session.get(CompanyCreditAndDiscount.class, creditAndDiscountId);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return companyCreditAndDiscountObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyCreditAndDiscountDao#getCompanyDropDownForCredit_Discount(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getCompanyDropDownForCredit_Discount(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<Company> companyList = null;
		JSONObject companyJson = null;
		JSONArray  companyArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(Company.class,"comp")
					.createAlias("comp.organization", "org")
					.add(Restrictions.eq("org.organizationId", organizationId))
					.add(Restrictions.eq("org.organizationStatus", "1"))
					.add(Restrictions.not(Restrictions.eq("companyCreditAndDiscountStatus", 1)))
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.asc("comp.companyName"));
			companyList = criteria.list();
			Iterator<Company> companyIt = companyList.iterator();
			while(companyIt.hasNext()){
				Company temp = companyIt.next();
				companyJson = new JSONObject();
				companyJson.put("compId", temp.getCompanyId());
				companyJson.put("compName", temp.getCompanyName());
				companyArray.put(companyJson);
			}
			companyJson = new JSONObject();
			companyJson.put("companyArray",companyArray);
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return companyJson;	
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyCreditAndDiscountDao#getCompanyCreditAndDiscountList(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyCreditAndDiscount> getCompanyCreditAndDiscountList(Integer organizationId) {
		List<CompanyCreditAndDiscount> creditAndDiscountList = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(CompanyCreditAndDiscount.class,"creditAndDiscount")
					.createAlias("creditAndDiscount.organization", "org")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("org.organizationId", organizationId));
			creditAndDiscountList = criteria.list();
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return creditAndDiscountList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyCreditAndDiscountDao#deleteCompanyCreditAndDiscountDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCompanyCreditAndDiscountDetails(Integer companyCreditAndDiscountId) {
		Session session =sessionFactory.getCurrentSession();
		CompanyCreditAndDiscount companyCreditAndDiscount = null;
		Company company = null;
		try{
			companyCreditAndDiscount = (CompanyCreditAndDiscount)session.get(CompanyCreditAndDiscount.class, companyCreditAndDiscountId);
			companyCreditAndDiscount.setStatus(0);
			session.update(companyCreditAndDiscount);
			company = (Company)session.load(Company.class, companyCreditAndDiscount.getCompany().getCompanyId());
			company.setCompanyCreditAndDiscountStatus(0);
			session.update(company);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyCreditAndDiscountDao#updateCompanyCreditAndDiscount(java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CompanyCreditAndDiscount)
	 */
	@Override
	public Integer updateCompanyCreditAndDiscount(Integer organizationId,
			Integer companyId, Integer creditAndDiscountId,
			CompanyCreditAndDiscount companyCreditAndDiscount) {
		Company company=null;
		Organization organization = null;
		CompanyCreditAndDiscount CompanyCreditAndDiscountOld = null;
		Session session =sessionFactory.getCurrentSession();
		Integer id = null;
		try{
			organization = (Organization) session.get(Organization.class, organizationId);
			company = (Company) session.get(Company.class, companyId);
			companyCreditAndDiscount.setOrganization(organization);
			companyCreditAndDiscount.setCompany(company);
			id = 0;
			id= (Integer)session.save(companyCreditAndDiscount);
			if(id>0){
				CompanyCreditAndDiscountOld = (CompanyCreditAndDiscount) session.get(CompanyCreditAndDiscount.class, creditAndDiscountId);
				CompanyCreditAndDiscountOld.setStatus(0);
				company.setCompanyCreditAndDiscountStatus(1);
				session.update(company);
				return id;
			}
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
			return null;
		}
		return id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object>searchCreditAndDiscount(
			String searchOption, String searchText,Integer from,Integer organizationId) {
		// TODO Auto-generated method stub
		Session session =sessionFactory.getCurrentSession();
		List<CompanyCreditAndDiscount> companyCreditAndDiscountList = null;
		Map<String,Object>map=null;
		try{
			
			Criteria criteria = session.createCriteria(CompanyCreditAndDiscount.class,"companyCreditAndDiscount")
					.createAlias("companyCreditAndDiscount.organization", "organization")
					.createAlias("companyCreditAndDiscount.company", "company")
					.add(Restrictions.eq("company.status",1))
					.add(Restrictions.eq("organization.organizationStatus", "1"))
					.add(Restrictions.eq("companyCreditAndDiscount.status",1))
					.setProjection(Projections.rowCount());
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("CompanyName".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			
			Long totalRecordCount = (Long)criteria.uniqueResult();

			criteria = session.createCriteria(CompanyCreditAndDiscount.class,"companyCreditAndDiscount")
					.createAlias("companyCreditAndDiscount.organization", "organization")
					.createAlias("companyCreditAndDiscount.company", "company")
					.add(Restrictions.eq("company.status",1))
					.add(Restrictions.eq("organization.organizationStatus", "1"))
					.add(Restrictions.eq("companyCreditAndDiscount.status",1))
					.addOrder(Order.desc("companyCreditAndDiscount.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
			else if("CompanyName".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			
			from = from - 1 ;
		   criteria.setFirstResult(from*Constant.PAGE_COUNT);
		   criteria.setMaxResults(Constant.PAGE_COUNT);
		   companyCreditAndDiscountList=criteria.list();
			
			Integer totalPages = 0;
			if(totalRecordCount>0){
				totalPages = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPages=totalPages+1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("companyCreditAndDiscountList", companyCreditAndDiscountList);
			map.put("totalPages", totalPages);
			
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return map;
	
	}
}
