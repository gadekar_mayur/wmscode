/**
 * 
 */
package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.City;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyBank;
import com.protocol.wms.model.District;
import com.protocol.wms.model.InwardCourierRegistration;
import com.protocol.wms.model.InwardCourierRegistrationDocketFilePath;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.Particulars;
import com.protocol.wms.model.State;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistBankDetails;

/**
 * @author admin
 *
 */
@Repository("InwardCourierDaoImpl")
public class InwardCourierDaoImpl implements InwardCourierDao {

	private Logger log = Logger.getLogger(InwardCourierDaoImpl.class.getClass());
	
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardCourierDao#getParticularsDropdownList()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Particulars> getParticularsDropdownList() {
		List<Particulars> particularsList = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(Particulars.class)
					.add(Restrictions.eq("status",1));
			particularsList = criteria.list();
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return particularsList;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardCourierDao#addInwardCourierRegistration(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardCourierRegistration, com.protocol.wms.model.AdvancedChequeInformation, java.util.List)
	 */
	@Override
	public Boolean addInwardCourierRegistration(String companyStockistOption,Integer orgId,
			Integer companyId, Integer stateId, Integer districtId, Integer cityId,
			Integer stockistId, Integer particularsId, Integer companyBankId,
			InwardCourierRegistration inwardCourierRegistration,
			AdvancedChequeInformation advancedChequeInformation,
			List<ChequeNumberInfo> chequeNumberInfoList) {
		Organization organization = null;
		Company company = null;
		State state = null;
		District district = null;
		City city = null;
		Particulars particulars = null;
		Stockist stockist = null;
		CompanyBank companyBank = null;
		StockistBankDetails stockistBankDetails= null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization) session.load(Organization.class, orgId);
			if(companyId!=null)
			company = (Company) session.load(Company.class, companyId);
			
			particulars = (Particulars) session.load(Particulars.class, particularsId);
			
			if(stockistId!=null){
				stockist = (Stockist) session.load(Stockist.class, stockistId);
				inwardCourierRegistration.setStockist(stockist);
				state = stockist.getState();
				district = stockist.getDistrict();
				city = stockist.getCity();
			}else if(companyId!=null){
				state = company.getState();
				district = company.getDistrict();
				city = company.getCity();
			}
			else if(stateId!=null&&districtId!=null&&cityId!=null){
				state = (State) session.load(State.class, stateId);
				district = (District) session.load(District.class, districtId);
				city = (City) session.load(City.class, cityId);
			}
			if(particularsId==3){
				advancedChequeInformation.setOrganization(organization);
				advancedChequeInformation.setCompany(company);
			
				if("Company".equals(companyStockistOption)&&companyBankId!=null){
					companyBank = (CompanyBank) session.load(CompanyBank.class, companyBankId);
					advancedChequeInformation.setCompanyBank(companyBank);
					session.save(advancedChequeInformation);
				}else if("Stockist".equals(companyStockistOption)&&companyBankId!=null){
					stockistBankDetails = (StockistBankDetails) session.load(StockistBankDetails.class, companyBankId);
					advancedChequeInformation.setStockistBankDetails(stockistBankDetails);
					advancedChequeInformation.setStockist(stockist);
					session.save(advancedChequeInformation);
				}
				if(chequeNumberInfoList!=null){
					for(ChequeNumberInfo tempObj : chequeNumberInfoList){
						tempObj.setAdvancedChequeInformation(advancedChequeInformation);
						tempObj.setDepositStatus(0);
						session.save(tempObj);
					}
				}
				if(companyBankId!=null){
					advancedChequeInformation.setChequeNumberInfoList(chequeNumberInfoList);
					session.update(advancedChequeInformation);
				}
			}
			inwardCourierRegistration.setOrganization(organization);
			inwardCourierRegistration.setCompany(company);
			inwardCourierRegistration.setState(state);
			inwardCourierRegistration.setDistrict(district);inwardCourierRegistration.setCity(city);
			inwardCourierRegistration.setParticulars(particulars);
			
			Integer id = (Integer) session.save(inwardCourierRegistration);
			if(id!=null)
				return true;
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return false;
	}
	
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardCourierDao#editInwardCourierRegistration(java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.InwardCourierRegistration, com.protocol.wms.model.AdvancedChequeInformation, java.util.List)
	 */
	@Override
	public Boolean editInwardCourierRegistration(String companyStockistOption,
			Integer orgId, Integer companyId, Integer stateId, Integer districtId,
			Integer cityId, Integer stockistId, Integer particularsId,
			Integer companyBankId,
			InwardCourierRegistration inwardCourierRegistration,
			AdvancedChequeInformation advancedChequeInformation,
			List<ChequeNumberInfo> chequeNumberInfoList) {
		Organization organization = null;
		Company company = null;
		State state = null;
		District district = null;
		City city = null;
		Particulars particulars = null;
		Stockist stockist = null;
		CompanyBank companyBank = null;
		StockistBankDetails stockistBankDetails= null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization) session.load(Organization.class, orgId);
			if(companyId!=null)
			 company = (Company) session.load(Company.class, companyId);
			particulars = (Particulars) session.load(Particulars.class, particularsId);
			
			if(stockistId!=null)
				stockist = (Stockist) session.load(Stockist.class, stockistId);
			
			
			if(particularsId==3&&(!"CompanyHavingCheque".equals(companyStockistOption))&&(!"CompanyStockistHavingCheque".equals(companyStockistOption))){
				advancedChequeInformation.setOrganization(organization);
				advancedChequeInformation.setCompany(company);
			
				if("CompanyDontHaveCheque".equals(companyStockistOption)&&companyBankId!=null){
					companyBank = (CompanyBank) session.load(CompanyBank.class, companyBankId);
					advancedChequeInformation.setCompanyBank(companyBank);
					session.save(advancedChequeInformation);
				}else if("CompanyStockistDontHaveCheque".equals(companyStockistOption)&&companyBankId!=null){
					stockistBankDetails = (StockistBankDetails) session.load(StockistBankDetails.class, companyBankId);
					advancedChequeInformation.setStockistBankDetails(stockistBankDetails);
					advancedChequeInformation.setStockist(stockist);
					session.save(advancedChequeInformation);
				}
				if(chequeNumberInfoList!=null){
					for(ChequeNumberInfo tempObj : chequeNumberInfoList){
						tempObj.setAdvancedChequeInformation(advancedChequeInformation);
						tempObj.setDepositStatus(0);
						session.save(tempObj);
					}
				}
				if(companyBankId!=null){
					advancedChequeInformation.setChequeNumberInfoList(chequeNumberInfoList);
					session.update(advancedChequeInformation);
				}
			}
			inwardCourierRegistration.setOrganization(organization);
			inwardCourierRegistration.setCompany(company);
			inwardCourierRegistration.setStockist(stockist);
			if(stateId!=null&&districtId!=null&&cityId!=null){
				state = (State) session.load(State.class, stateId);
				district = (District) session.load(District.class, districtId);
				city = (City) session.load(City.class, cityId);
				inwardCourierRegistration.setState(state);
				inwardCourierRegistration.setDistrict(district);
				inwardCourierRegistration.setCity(city);
			}
			inwardCourierRegistration.setParticulars(particulars);
			
			session.update(inwardCourierRegistration);
			return true;
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return false;
	}
	
	
	@Override
	public List<InwardCourierRegistrationDocketFilePath> updateInwardCourierRegDocketFileImage(
			Integer inwardCourierRegId,
			List<InwardCourierRegistrationDocketFilePath> docketFileImgList) {

		InwardCourierRegistration inwardCourierRegistration = null;
		List<InwardCourierRegistrationDocketFilePath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			inwardCourierRegistration = (InwardCourierRegistration) session.load(InwardCourierRegistration.class, inwardCourierRegId);
			for(InwardCourierRegistrationDocketFilePath tempObj : docketFileImgList){
				tempObj.setInwardCourierRegistration(inwardCourierRegistration);
				session.save(tempObj);
			}
			lst = inwardCourierRegistration.getInwardCourierRegistrationDocketFilePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardCourierDao#inwardCourierRegistrationListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<InwardCourierRegistration> inwardCourierRegistrationListing(Integer organizationId) {
		List<InwardCourierRegistration> inwardCourierRegistrationList = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(InwardCourierRegistration.class,"courier")
					.createAlias("courier.organization", "organization")
					//.createAlias("courier.company", "company")
					.add(Restrictions.eq("courier.status",1))
					//.add(Restrictions.eq("company.status",1))
					//.add(Restrictions.eq("organization.organizationStatus","1"))
					.addOrder(Order.desc("courier.submitDate"));
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			
			inwardCourierRegistrationList = criteria.list();
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return inwardCourierRegistrationList;
	}
	
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardCourierDao#searchInwardCourierRegistrationDetails(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Object,Object> searchInwardCourierRegistrationDetails(String searchOption, String searchText, String fromSpecialData, String toSpecialData,
			Integer organizationId,Integer from) {
		
		List<InwardCourierRegistration> inwardCourierRegistrationList = null;
		Map<Object, Object>map = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(InwardCourierRegistration.class,"courier")
					.createAlias("courier.organization", "organization")
					.add(Restrictions.eq("courier.status",1))
					 .setProjection(Projections.rowCount())
					.addOrder(Order.desc("courier.submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
				  
			switch(searchOption){
			case "Company" : 
				 criteria.createAlias("courier.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
//				 criteria.add(Restrictions.like("organization.organization", searchText,MatchMode.START));
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
				 
			case "Stockist" :
				 criteria.createAlias("courier.stockist", "stockist");
				 criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				 break;
				 
			case "CourierName" :
				 criteria.add(Restrictions.like("courier.courierName", searchText,MatchMode.START));
				 break;
			case "DeliveryPerson" :
				  criteria.add(Restrictions.like("courier.deliveryPerson", searchText,MatchMode.START));
				  break;
			case "DocketNo" :
				 criteria.add(Restrictions.like("courier.docketNo", searchText,MatchMode.START));
				 break;
			case "State" :
				   criteria.createAlias("courier.state", "state");
				   criteria.add(Restrictions.like("state.stateName", searchText,MatchMode.START));
				   break;
			case "DistrictName" :
				 criteria.createAlias("courier.district", "district");
				 criteria.add(Restrictions.like("district.districtName", searchText,MatchMode.START));
				 break;
			case "Cityname" :
				  criteria.createAlias("courier.city", "city");
				  criteria.add(Restrictions.like("city.cityName", searchText,MatchMode.START));
				  break;
			
		case "Date" :
			if(fromSpecialData!=""&&toSpecialData!=""){
				   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
				   java.util.Date d1= null;
					d1 = dtf.parse(fromSpecialData);
					java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
					d1 = dtf.parse(toSpecialData);
					java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
					
				   criteria.add(Restrictions.between("courier.courierRegistrationDate", sqlFromDate, sqlToDate));
			   }
			}
			 Long totalRecordCount = (Long)criteria.uniqueResult();
		
			 criteria = session.createCriteria(InwardCourierRegistration.class,"courier")
					.createAlias("courier.organization", "organization")
					.add(Restrictions.eq("courier.status",1))
					.addOrder(Order.desc("courier.submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
				  
			switch(searchOption){
			case "Company" : 
				 criteria.createAlias("courier.company", "company");
				 criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
				 break;
			case "Organization" :
//				 criteria.add(Restrictions.like("organization.organization", searchText,MatchMode.START));
				criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
				 
			case "Stockist" :
				 criteria.createAlias("courier.stockist", "stockist");
				 criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				 break;
				 
			case "CourierName" :
				 criteria.add(Restrictions.like("courier.courierName", searchText,MatchMode.START));
				 break;
			case "DeliveryPerson" :
				  criteria.add(Restrictions.like("courier.deliveryPerson", searchText,MatchMode.START));
				  break;
			case "DocketNo" :
				 criteria.add(Restrictions.like("courier.docketNo", searchText,MatchMode.START));
				 break;
			case "State" :
				   criteria.createAlias("courier.state", "state");
				   criteria.add(Restrictions.like("state.stateName", searchText,MatchMode.START));
				   break;
			case "DistrictName" :
				 criteria.createAlias("courier.district", "district");
				 criteria.add(Restrictions.like("district.districtName", searchText,MatchMode.START));
				 break;
			case "Cityname" :
				  criteria.createAlias("courier.city", "city");
				  criteria.add(Restrictions.like("city.cityName", searchText,MatchMode.START));
				  break;
			
			case "Date" :
			if(fromSpecialData!=""&&toSpecialData!=""){
				   DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
				   java.util.Date d1= null;
					d1 = dtf.parse(fromSpecialData);
					java.sql.Date sqlFromDate = new java.sql.Date(d1.getTime());
					d1 = dtf.parse(toSpecialData);
					java.sql.Date sqlToDate = new java.sql.Date(d1.getTime());
					
				   criteria.add(Restrictions.between("courier.courierRegistrationDate", sqlFromDate, sqlToDate));
			   }
			}
			 from=from-1;
			   criteria.setFirstResult(from *Constant.PAGE_COUNT);
			   criteria.setMaxResults(Constant.PAGE_COUNT);
				 
			   inwardCourierRegistrationList=criteria.list();
			   
			   Integer tatalPage = 0;
				if(totalRecordCount>0)
				{
					 
					tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0)
					{
						tatalPage=tatalPage+1;
					}
				}
					map = new HashMap<Object, Object>();
					map.put("InwardCourierRegistration", inwardCourierRegistrationList);
					map.put("totalPages", tatalPage);
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
				
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.InwardCourierDao#deleteInwardCourierRegDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteInwardCourierRegDetails(Integer inwardCourierRegId) {
		Session session =sessionFactory.getCurrentSession();
		InwardCourierRegistration inwardCourierRegistration = null;
		try{
			inwardCourierRegistration = (InwardCourierRegistration)session.get(InwardCourierRegistration.class, inwardCourierRegId);
			inwardCourierRegistration.setStatus(0);
			session.update(inwardCourierRegistration);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
	@Override
	public InwardCourierRegistration loadInwardCourierRegistrationObjectUsignInwardCourierRegId(
			Integer inwardCourierRegId) {
		Session session =sessionFactory.getCurrentSession();
		InwardCourierRegistration inwardCourierRegistrationObj = null;
		try 
		{
			inwardCourierRegistrationObj=(InwardCourierRegistration)session.get(InwardCourierRegistration.class,inwardCourierRegId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return inwardCourierRegistrationObj;	
    }
	@Override
	public Boolean deleteInwardCourierRegistrationDocketFileImage(
			Integer docketFileImageId) {
		Session session =sessionFactory.getCurrentSession();
		InwardCourierRegistrationDocketFilePath inwardCourierRegistrationDocketFilePath= null;
		try{
			inwardCourierRegistrationDocketFilePath = (InwardCourierRegistrationDocketFilePath)session.load(InwardCourierRegistrationDocketFilePath.class, docketFileImageId);
			session.delete(inwardCourierRegistrationDocketFilePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
}
