/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentUploadedDocument;
import com.protocol.wms.model.CartingAgentUploadedDocumentImagePath;
import com.protocol.wms.model.DocumentListType;
import com.protocol.wms.model.Organization;

/**
 * @author admin
 *
 */
@Repository(value="CartingAgentUploadedDocumentDaoImpl")
public class CartingAgentUploadedDocumentDaoImpl implements CartingAgentUploadedDocumentDao {

	private Logger log = Logger.getLogger(CartingAgentUploadedDocumentDaoImpl.class.getClass());
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentDao#saveCartingAgentUploadDocument(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public Boolean saveCartingAgentUploadDocument(Integer organizationId,Integer cartingAgentId, Integer documentTypeId,CartingAgentUploadedDocument cartingAgentUploadedDocument) {
		Session session =sessionFactory.getCurrentSession();
		Organization organization = null;
		CartingAgent cartingAgent = null;
		DocumentListType documentListType = null;
		try{
			organization = (Organization)session.load(Organization.class, organizationId);
			cartingAgent = (CartingAgent)session.load(CartingAgent.class, cartingAgentId);
			documentListType = (DocumentListType)session.load(DocumentListType.class, documentTypeId);
			cartingAgentUploadedDocument.setOrganization(organization);
			cartingAgentUploadedDocument.setCartingAgent(cartingAgent);
			cartingAgentUploadedDocument.setDocumentListType(documentListType);
			Integer Id = (Integer) session.save(cartingAgentUploadedDocument);
			if(Id!=null)
				return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentDao#getCartingAgentUploadedDocList(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CartingAgentUploadedDocument> getCartingAgentUploadedDocList(Integer organizationId) {
		List<CartingAgentUploadedDocument> docList = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(CartingAgentUploadedDocument.class,"cartingDocument")
					.createAlias("cartingDocument.organization", "organization")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			docList = criteria.list();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return docList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentUploadedDocumentDao#deleteCartingAgentDocumentDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCartingAgentDocumentDetails(Integer docId) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgentUploadedDocument cartingAgentUploadedDocument= null;
		try{
			cartingAgentUploadedDocument = (CartingAgentUploadedDocument)session.load(CartingAgentUploadedDocument.class, docId);
			cartingAgentUploadedDocument.setStatus(0);
			session.update(cartingAgentUploadedDocument);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public CartingAgentUploadedDocument updateCartingAgentDocumentInfo(Integer documentTypeId, Integer cartingAgentId, Integer cartingAgentDocId) {
		CartingAgent cartingAgent = null;
		DocumentListType documentListType = null;
		CartingAgentUploadedDocument cartingAgentUploadedDocument = null;
		Session session =sessionFactory.getCurrentSession();
		{
			cartingAgent = (CartingAgent) session.load(CartingAgent.class, cartingAgentId);
			documentListType = (DocumentListType) session.load(DocumentListType.class, documentTypeId);
			cartingAgentUploadedDocument = (CartingAgentUploadedDocument) session.load(CartingAgentUploadedDocument.class, cartingAgentDocId);
			
			cartingAgentUploadedDocument.setCartingAgent(cartingAgent);
			cartingAgentUploadedDocument.setDocumentListType(documentListType);
			session.update(cartingAgentUploadedDocument);
		}
		return cartingAgentUploadedDocument;
	}

	@Override
	public List<CartingAgentUploadedDocumentImagePath> updateCartingAgentUploadDocumentImage(Integer cartingAgentDocumentId, 
			List<CartingAgentUploadedDocumentImagePath> documentList) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgentUploadedDocument cartingAgentUploadedDocument = null;
		{
			cartingAgentUploadedDocument = (CartingAgentUploadedDocument) session.load(CartingAgentUploadedDocument.class, cartingAgentDocumentId);
			for(CartingAgentUploadedDocumentImagePath tempObj : documentList){
				tempObj.setCartingAgentUploadedDocument(cartingAgentUploadedDocument);
				session.save(tempObj);
			}
		}
		return cartingAgentUploadedDocument.getCartingAgentUploadedDocumentImagePathList();
	}

	@Override
	public Boolean deleteCartingAgentDocumentFileImage(Integer cartingAgentUploadDocumentPathId) {
		CartingAgentUploadedDocumentImagePath cartingAgentUploadedDocumentImagePath = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			cartingAgentUploadedDocumentImagePath = (CartingAgentUploadedDocumentImagePath) session.load(CartingAgentUploadedDocumentImagePath.class, cartingAgentUploadDocumentPathId);
			session.delete(cartingAgentUploadedDocumentImagePath);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Object,Object> searchCartingAgentsUploadedDocumentDetails(
			String searchOptioin, String searchText, Integer organizationId, Integer from) {
		List<CartingAgentUploadedDocument> docList = null;
		Map<Object, Object>map = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(CartingAgentUploadedDocument.class,"cartingDocument")
					.createAlias("cartingDocument.organization", "organization")
					.add(Restrictions.eq("organization.organizationStatus", "1"))
					.add(Restrictions.eq("status", 1))
					.setProjection(Projections.rowCount());
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			switch (searchOptioin) {
			case "Organization":
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
			case "TransporterName":
				criteria.createAlias("cartingDocument.cartingAgent", "cartingAgent")
				 .add(Restrictions.like("cartingAgent.name", searchText,MatchMode.START));
				break;
			case "DocumentType":
				criteria.createAlias("cartingDocument.documentListType", "documentListType")
				 .add(Restrictions.like("documentListType.documentName", searchText,MatchMode.START));
				break;
	
			}
			 Long totalRecordCount = (Long)criteria.uniqueResult();
			
			 criteria = session.createCriteria(CartingAgentUploadedDocument.class,"cartingDocument")
					.createAlias("cartingDocument.organization", "organization")
					.add(Restrictions.eq("organization.organizationStatus", "1"))
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			switch (searchOptioin) {
			case "Organization":
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
			case "TransporterName":
				criteria.createAlias("cartingDocument.cartingAgent", "cartingAgent")
				 .add(Restrictions.like("cartingAgent.name", searchText,MatchMode.START));
				break;
			case "DocumentType":
				criteria.createAlias("cartingDocument.documentListType", "documentListType")
				 .add(Restrictions.like("documentListType.documentName", searchText,MatchMode.START));
				break;
	
			}
			
			 from=from-1;
			   criteria.setFirstResult(from *Constant.PAGE_COUNT);
			   criteria.setMaxResults(Constant.PAGE_COUNT);
				 
			   docList=criteria.list();
			   
			   Integer tatalPage = 0;
				if(totalRecordCount>0)
				{
					 
					tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0)
					{
						tatalPage=tatalPage+1;
					}
				}
					map = new HashMap<Object, Object>();
					map.put("cartingAgentUploadedDocument", docList);
					map.put("totalPages", tatalPage);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
	
}
