/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.json.JSONObject;

import com.protocol.wms.model.State;

/**
 * @author Sudhakar
 *
 */
public interface StateDao {
	
	public List<State> loadStateList(Integer orgId);
	
	public State loadSateObjectUsingStateId(Integer stateId);

	public JSONObject saveStateForOrganization(Integer orgId,String stateName);
	
	public Boolean stateAlreadyExistsOrNot(Integer orgId, String stateName);
	
	public List<State> getStateListByOrg(Integer organizationId);
	
	public Boolean deleteStateDetails(Integer stateId);

	public Boolean updateStateObj(State stateObj);

	public List<State> searchOptionForState(String searchOption,String searchText);
}
