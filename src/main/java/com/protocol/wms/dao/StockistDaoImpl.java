/**
 * 
 */
package com.protocol.wms.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.CommonMethods;
import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.City;
import com.protocol.wms.model.Company;
import com.protocol.wms.model.District;
import com.protocol.wms.model.DocumentListType;
import com.protocol.wms.model.DrugLicence;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.State;
import com.protocol.wms.model.Stockist;
import com.protocol.wms.model.StockistDocument;
import com.protocol.wms.model.StockistDocumentImagePath;
import com.protocol.wms.model.TransporterDetails;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.model.UserType;

/**
 * @author admin
 *
 */
@Repository(value="StockistDaoImpl")
public class StockistDaoImpl implements StockistDao{
	
	private SessionFactory sessionFactory;
	private Logger log = Logger.getLogger(StockistDaoImpl.class.getClass());
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public JSONObject displayStockistDetailsForm(Integer organizationId) {
		JSONObject stockistFormJson = null;
		JSONArray tempArray = null;
		try{
			stockistFormJson = new JSONObject();
			tempArray = getStateList(organizationId);
			stockistFormJson.put("stateArray", tempArray);
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistFormJson;
	}
	
	//get stockist list
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getStokistList(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<Stockist> stockistList = null;
		JSONObject stockistJson = null;
		JSONArray  stockistArray = new JSONArray();
		Organization organization = null;
		//Company company = null;
		try{
			//System.out.println("getStokistList org id="+organizationId);
			Criteria criteria = session.createCriteria(Stockist.class,"stockist")
					.createAlias("stockist.organization", "org")
					.add(Restrictions.eq("stockist.status", 1))
					.addOrder(Order.desc("stockist.submitDate"));
			if(organizationId!=null)
					criteria.add(Restrictions.eq("org.organizationId", organizationId));
			
			stockistList = criteria.list();
			Iterator<Stockist> stockistIt = stockistList.iterator();
			//System.out.println("Darrrrrrrr");
			while(stockistIt.hasNext()){
				Stockist temp = stockistIt.next();
				
				
				organization = (Organization) session.get(Organization.class, temp.getOrganization().getOrganizationId());
				//company = (Company) session.get(Company.class, temp.getCompany().getCompanyId());
				if("1".equals(organization.getOrganizationStatus())){
				stockistJson = new JSONObject();
				stockistJson.put("orgName", organization.getOrganizationName());
				//stockistJson.put("company", company.getCompanyName());
				stockistJson.put("stockistId", temp.getStockistId());
				stockistJson.put("stockistName", temp.getStockistName());
				if(temp.getStockistContactPerson().equalsIgnoreCase(""))
				{
				stockistJson.put("contactPerson", "-");
				}else{
					stockistJson.put("contactPerson", temp.getStockistContactPerson());
				}
				stockistJson.put("mobile",temp.getStockistMobileNo());
				if(temp.getStockistEmailId().equalsIgnoreCase(""))
				{
				stockistJson.put("email","-");
				}else{
					stockistJson.put("email",temp.getStockistEmailId());
				}
				stockistJson.put("location",temp.getStockistLocation());
				stockistArray.put(stockistJson);
				}
			}
			stockistJson = new JSONObject();
			stockistJson.put("stockistArray",stockistArray);
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistJson;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#searchStockistDetails(java.lang.String, java.lang.String, java.lang.Integer)
	//procedure definition
	 DELIMITER //
		CREATE PROCEDURE searchStockistByIdLikeQuery(IN stockistIdSr VARCHAR(255))
		 BEGIN
		 SELECT * 
		 FROM Stockist
		 WHERE stockistId LIKE stockistIdSr;
		 END //
	 DELIMITER ;
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public JSONObject searchStockistDetails(String searchOption,String searchText, Integer organizationId,Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<Stockist> stockistList = null;
		JSONObject stockistJson = null;
		JSONArray  stockistArray = new JSONArray();
		Organization organization = null;
		Iterator<Stockist> stockistIt = null;
		Map<String, Object>map = null;
		Integer totalPage = 0;
		try{
			if("Mobile".equals(searchOption)||"StockistID".equals(searchOption))
			{
				Query query = session.createSQLQuery("CALL searchStockistDetails(:searchOption,:searchText,:organizationId)")
						.addEntity(Stockist.class)
						.setParameter("searchText", searchText+"%")
						.setParameter("organizationId", organizationId);
				if("Mobile".equals(searchOption))
					query.setParameter("searchOption", "MOBILE");
				else if("StockistID".equals(searchOption))
					query.setParameter("searchOption", "STOCKIST_ID");
				List result = query.list();
				stockistIt = result.iterator();
			}
			else
			{
				Criteria criteria = session.createCriteria(Stockist.class,"stockist")
						.createAlias("stockist.organization", "organization")
						.add(Restrictions.eq("status", 1))
						.setProjection(Projections.rowCount())
						.addOrder(Order.desc("submitDate"));
				if(organizationId!=null)
						criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				if("Organization".equals(searchOption))
					criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				else if("StockistName".equals(searchOption))
					criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				else if("Transporter".equals(searchOption))
					criteria.createAlias("stockist.transporterDetails", "transporterDetails")
					.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
				else if("ContactPerson".equals(searchOption))
					criteria.add(Restrictions.like("stockist.stockistContactPerson", searchText,MatchMode.START));
				else if("Email".equals(searchOption))
					criteria.add(Restrictions.like("stockist.stockistEmailId", searchText,MatchMode.START));
				else if("Location".equals(searchOption))
					criteria.add(Restrictions.like("stockist.stockistLocation", searchText,MatchMode.START));
				Long totalRecordCount = (Long)criteria.uniqueResult();
				
				 criteria = session.createCriteria(Stockist.class,"stockist")
						.createAlias("stockist.organization", "organization")
						.add(Restrictions.eq("status", 1))
						.addOrder(Order.desc("submitDate"));
				 
				if(organizationId!=null)
						criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				
				if("Organization".equals(searchOption))
					criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				else if("StockistName".equals(searchOption))
					criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
				else if("Transporter".equals(searchOption))
					criteria.createAlias("stockist.transporterDetails", "transporterDetails")
					.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
				else if("ContactPerson".equals(searchOption))
					criteria.add(Restrictions.like("stockist.stockistContactPerson", searchText,MatchMode.START));
				else if("Email".equals(searchOption))
					criteria.add(Restrictions.like("stockist.stockistEmailId", searchText,MatchMode.START));
				else if("Location".equals(searchOption))
					criteria.add(Restrictions.like("stockist.stockistLocation", searchText,MatchMode.START));
				
				from = from - 1 ;
				criteria.setFirstResult(from*Constant.PAGE_COUNT);
				criteria.setMaxResults(Constant.PAGE_COUNT);
				stockistList = criteria.list();
				
			
				if(totalRecordCount>0){
					totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						totalPage=totalPage+1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("stockistList", stockistList);
				map.put("totalPages", totalPage);
				stockistIt = stockistList.iterator();
			}
			while(stockistIt.hasNext()){
				Stockist temp = stockistIt.next();
				organization = (Organization) session.get(Organization.class, temp.getOrganization().getOrganizationId());
				if("1".equals(organization.getOrganizationStatus())){
				stockistJson = new JSONObject();
				stockistJson.put("orgName", organization.getOrganizationName());
				stockistJson.put("stockistId", temp.getStockistId());
				stockistJson.put("stockistName", temp.getStockistName());
				stockistJson.put("contactPerson", temp.getStockistContactPerson());
				stockistJson.put("mobile",temp.getStockistMobileNo());
				stockistJson.put("email",temp.getStockistEmailId());
				stockistJson.put("location",temp.getStockistLocation());
				stockistArray.put(stockistJson);
				}
			}
			stockistJson = new JSONObject();
			stockistJson.put("paginationCount", totalPage);
			stockistJson.put("stockistArray",stockistArray);
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistJson;
	}
	@SuppressWarnings("unchecked")
	private JSONArray getStateList(Integer orgID){
		Session session =sessionFactory.getCurrentSession();
		List<State> stateList = null;
		JSONObject stateJson = null;
		JSONArray  stateArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(State.class,"state")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.asc("state.stateName"));
			if(orgID!=null)
			criteria.add(Restrictions.eq("organization.organizationId", orgID));
			stateList = criteria.list();
			Iterator<State> stateIt = stateList.iterator();
			while(stateIt.hasNext())
			{
				State temp = stateIt.next();
				stateJson = new JSONObject();
				stateJson.put("stateId", temp.getStateId());
				stateJson.put("stateName", temp.getStateName());
				stateArray.put(stateJson);
			}
		}
		catch(JSONException e){
			e.printStackTrace();
			log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return stateArray;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#organizationListDropdown(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject organizationListDropdown(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<Organization> orgList = null;
		JSONObject orgJson = null;
		JSONArray  orgArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(Organization.class)
					.add(Restrictions.eq("organizationStatus", "1"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organizationId", organizationId));
			
			orgList = criteria.list();
			Iterator<Organization> orgIt = orgList.iterator();
			//System.out.println("Darrrrrrrr");
			while(orgIt.hasNext())
			{
				Organization temp = orgIt.next();
				orgJson = new JSONObject();
				orgJson.put("orgId", temp.getOrganizationId());
				orgJson.put("orgName", temp.getOrganizationName());
				orgArray.put(orgJson);
			}
			orgJson = new JSONObject();
			orgJson.put("orgArray",orgArray);
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return orgJson;
	}
	/*@Override
	public JSONObject getCompanyList(Integer orgId) {
		Session session =sessionFactory.getCurrentSession();
		List<Company> companyList = null;
		JSONObject companyJson = null;
		JSONArray  companyArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(Company.class,"comp")
					.createAlias("comp.organization", "org")
					.add(Restrictions.eq("org.organizationId", orgId))
					.add(Restrictions.eq("status", 1));
			companyList = criteria.list();
			Iterator<Company> companyIt = companyList.iterator();
			//System.out.println("Darrrrrrrr");
			while(companyIt.hasNext()){
				Company temp = companyIt.next();
				companyJson = new JSONObject();
				companyJson.put("compId", temp.getCompanyId());
				companyJson.put("compName", temp.getCompanyName());
				companyArray.put(companyJson);
			}
			companyJson = new JSONObject();
			companyJson.put("companyArray",companyArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return companyJson;
	}*/
	/*@Override
	public JSONObject getDistrictList(Integer stateId) {
		Session session =sessionFactory.getCurrentSession();
		List<District> districtList = null;
		JSONObject districtJson = null;
		JSONArray  districtArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(District.class,"dist")
					.createAlias("dist.state", "state")
					.add(Restrictions.eq("state.stateId", stateId))
					.add(Restrictions.eq("status", 1));
			districtList = criteria.list();
			Iterator<District> distIt = districtList.iterator();
			//System.out.println("Darrrrrrrr");
			while(distIt.hasNext()){
				District temp = distIt.next();
				districtJson = new JSONObject();
				districtJson.put("distId", temp.getDistrictId());
				districtJson.put("distName", temp.getDistrictName());
				districtArray.put(districtJson);
			}
			districtJson = new JSONObject();
			districtJson.put("districtArray",districtArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return districtJson;
	}*/
	/*@Override
	public JSONObject getCityList(Integer districtId) {
		Session session =sessionFactory.getCurrentSession();
		List<City> cityList = null;
		JSONObject cityJson = null;
		JSONArray  cityArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(City.class,"city")
					.createAlias("city.district", "dist")
					.add(Restrictions.eq("dist.districtId", districtId))
					.add(Restrictions.eq("status", 1));
			cityList = criteria.list();
			Iterator<City> cityIt = cityList.iterator();
			//System.out.println("Darrrrrrrr");
			while(cityIt.hasNext()){
				City temp = cityIt.next();
				cityJson = new JSONObject();
				cityJson.put("cityId", temp.getCityId());
				cityJson.put("cityName", temp.getCityName());
				cityArray.put(cityJson);
			}
			cityJson = new JSONObject();
			cityJson.put("cityArray",cityArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return cityJson;
	}*/
	/*@Override
	public JSONObject getTransporterList(Integer orgId) {
		Session session =sessionFactory.getCurrentSession();
		List<TransporterDetails> transporterList = null;
		JSONObject transporterJson = null;
		JSONArray  transporterArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(TransporterDetails.class,"trans")
					.createAlias("trans.organization", "org")
					.add(Restrictions.eq("org.organizationId", orgId))
					.add(Restrictions.eq("status", 1));
			transporterList = criteria.list();
			Iterator<TransporterDetails> transporterIt = transporterList.iterator();
			//System.out.println("Darrrrrrrr");
			while(transporterIt.hasNext()){
				TransporterDetails temp = transporterIt.next();
				transporterJson = new JSONObject();
				transporterJson.put("transId", temp.getTransporterDetailsId());
				transporterJson.put("transName", temp.getTransporterName());
				transporterArray.put(transporterJson);
			}
			transporterJson = new JSONObject();
			transporterJson.put("transporterArray",transporterArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return transporterJson;
	}*/
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#addCompanyToStockist(java.lang.Integer[], java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public JSONObject addCompanyToStockist(Integer[] companyIdList,Integer stockistId, Integer orgId) {
		JSONObject result = new JSONObject();
		Company company=null;
		Stockist stockist = null; 
		List<Company>companyList = null ;
		Session session =sessionFactory.getCurrentSession();
		try{
			result.put("result",false);
			companyList = new ArrayList<Company>();
			if(companyIdList==null){
				stockist = (Stockist) session.get(Stockist.class, stockistId);
				stockist.setCompanyList(companyList);
				session.update(stockist);
				result.put("result",true);
			}else{
				for(int i=0 ; i<companyIdList.length;i++){
					company = (Company)session.load(Company.class, companyIdList[i]);
					companyList.add(company);
				}
				//if(companyList.size()>0){
					stockist = (Stockist) session.get(Stockist.class, stockistId);
					stockist.setCompanyList(companyList);
					session.update(stockist);
					result.put("result",true);
				//}
			}
		}
		catch(Exception e){
			try {
				result.put("result",false);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();log.error(e);
			}
			e.printStackTrace();log.error(e);
			return result;
			
		}
		return result;
	}
	@Override
	public JSONObject saveStockist(Stockist stockist, Integer orgId,Integer stateId, Integer cityId, Integer companyId,	Integer districtId, Integer transporterDetailsId,UserDetails userDetails) {
		Organization org=null;
		//List<Company> companyList=null;
		TransporterDetails transporter=null;
		State state=null;
		District district=null;
		City city = null;
		JSONObject result = new JSONObject();
		Session session =sessionFactory.getCurrentSession();
		try{
			org = (Organization) session.load(Organization.class, orgId);
			//company = (Company) session.load(Company.class, companyId);
			//companyList.add(company);
			transporter = (TransporterDetails) session.load(TransporterDetails.class, transporterDetailsId);
			state = (State) session.load(State.class, stateId);
			district = (District) session.load(District.class, districtId);
			city = (City) session.load(City.class, cityId);
			stockist.setOrganization(org);
			//stockist.setCompany(company);
			stockist.setTransporterDetails(transporter);
			stockist.setState(state);
			stockist.setDistrict(district);
			stockist.setCity(city);
			//
		       stockist.setSubmitDate(CommonMethods.setSubmitedDate());
			//
		       if(userDetails!=null){
		    	   UserType userType = (UserType) session.load(UserType.class,Constant.STOCKIEST);
		    	   userDetails.setUserType(userType);
		    	   stockist.setUserDetails(userDetails);
		       }
		       int i =(Integer) session.save(stockist);
		       if(i>0){
		    	   result.put("result", true);
		    	   //System.out.println("Iserted successfully id = "+stockist.getStockistId());
		       }
		       else{
		    	   result.put("result", false);
		    	   //System.out.println("Failed");
		       }
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return result;
	}
	/*@Override
	public JSONObject displayStockistBankDetailsForm() {
		JSONObject stockistFormJson = null;
		JSONArray tempArray = null;
		try{
			stockistFormJson = new JSONObject();
			tempArray = getOrganizationList();
			stockistFormJson.put("orgArray", tempArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return stockistFormJson;
	}*/
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getStockistByOrganization(Integer orgId){
		Session session =sessionFactory.getCurrentSession();
		List<Stockist> stockistList = null;
		JSONObject stockistJson = null;
		JSONArray  stockistArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(Stockist.class,"stockist")
					.createAlias("stockist.organization","organization")
					.add(Restrictions.eq("organization.organizationId", orgId))
					.addOrder(Order.asc("stockist.stockistName"))
					.add(Restrictions.eq("stockist.status", 1));
			/*if(orgId!=null||orgId!=0)
				criteria.add(Restrictions.eq("organization.organizationId", orgId));*/
			stockistList = criteria.list();
			Iterator<Stockist> stockistIt = stockistList.iterator();
			 if (stockistList.size() > 0) 
			 {
				    Collections.sort(stockistList, new Comparator<Stockist>() {
				        @Override
				        public int compare(final Stockist object1, final Stockist object2) 
				        {
				            return object1.getStockistName().toUpperCase().compareTo(object2.getStockistName().toUpperCase());// for ascending order
//				        	return object2.getStockistName().compareTo(object1.getStockistName()); //for descending order
				        }
				       });
			}
			while(stockistIt.hasNext()){
				Stockist temp = stockistIt.next();
				//System.out.println("Id = "+temp.getStockistId()+" Name = "+ temp.getStockistName());
				stockistJson = new JSONObject();
				stockistJson.put("stockistId", temp.getStockistId());
				stockistJson.put("stockistName", temp.getStockistName());
				stockistArray.put(stockistJson);
			}
			stockistJson = new JSONObject();
			stockistJson.put("stockistArray",stockistArray);
		}
		catch(JSONException e){
			log.error(e);
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();log.error(e);
		}
		return stockistJson;
	}
	@Override
	public JSONObject getStockistByOrgAndCompany(Integer orgId,	Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		List<Stockist> stockistList = null;
		JSONObject stockistJson = null;
		JSONArray  stockistArray = new JSONArray();
		try{
			if(companyId==0){
				stockistJson = new JSONObject();
				return stockistJson;
			}
			Company company = (Company) session.get(Company.class, companyId);
			stockistList = company.getStockistList();
			
			// sort questionList based on question Submit Date by descending order
			 if (stockistList.size() > 0) 
			 {
				    Collections.sort(stockistList, new Comparator<Stockist>() {
				        @Override
				        public int compare(final Stockist object1, final Stockist object2) 
				        {
				            return object1.getStockistName().toUpperCase().compareTo(object2.getStockistName().toUpperCase());// for ascending order
//				        	return object2.getStockistName().compareTo(object1.getStockistName()); //for descending order
				        }
				       });
			}
			
			Iterator<Stockist> stockistIt = stockistList.iterator();
			while(stockistIt.hasNext()){
				Stockist temp = stockistIt.next();
				if(temp.getStatus()==1){
					stockistJson = new JSONObject();
					stockistJson.put("stockistId", temp.getStockistId());
					stockistJson.put("stockistName", temp.getStockistName());
					stockistArray.put(stockistJson);
				}
			}
			stockistJson = new JSONObject();
			stockistJson.put("stockistArray",stockistArray);
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistJson;
	}
	/*@Override
	public JSONObject saveStockistBankDetails(Integer orgId,Integer stockistId, Integer companyId,StockistBankDetails bankDetatils) {
		Organization org=null;
		Company company=null;
		Stockist stockist = null;
		JSONObject result = new JSONObject();
		Session session =sessionFactory.getCurrentSession();
		try{
			org = (Organization) session.load(Organization.class, orgId);
			company = (Company) session.load(Company.class, companyId);
			stockist = (Stockist) session.load(Stockist.class, stockistId);
			bankDetatils.setOrganization(org);
			bankDetatils.setCompany(company);
			bankDetatils.setStockist(stockist);
			//
			Date today = new Date();
		    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		       df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		       String IST = df.format(today);
		       DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		       Date date = df2.parse(IST);
		       bankDetatils.setSubmitDate(date);
			//
		       int i =(Integer) session.save(bankDetatils);
		       if(i>0){
		    	   result.put("result", true);
		    	   System.out.println("Iserted successfully id = "+stockist.getStockistId());
		       }
		       else{
		    	   result.put("result", false);
		    	   System.out.println("Failed");
		       }
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}*/
	
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#stockistListingAccToOrg(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject stockistListingAccToOrg(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<Stockist> stockistList = null;
		JSONObject stockistJson = null;
		JSONArray  stockistArray = new JSONArray();
		Organization organization = null;
		//Company company = null;
		try{
			Criteria criteria = session.createCriteria(Stockist.class,"stockist")
					.createAlias("stockist.organization", "org")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
					criteria.add(Restrictions.eq("org.organizationId", organizationId));
			
			stockistList = criteria.list();
			Iterator<Stockist> stockistIt = stockistList.iterator();
			//System.out.println("Darrrrrrrr");
			while(stockistIt.hasNext()){
				Stockist temp = stockistIt.next();
				if("1".equals(temp.getOrganization().getOrganizationStatus())){
				stockistJson = new JSONObject();
				organization = (Organization) session.get(Organization.class, temp.getOrganization().getOrganizationId());
				stockistJson.put("ORG_NAME", organization.getOrganizationName());
				stockistJson.put("STOCKIST_ID", temp.getStockistId());
				stockistJson.put("STOCKIST_NAME", temp.getStockistName());
				stockistArray.put(stockistJson);
				}
			}
			stockistJson = new JSONObject();
			stockistJson.put("stockistArray",stockistArray);
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistJson;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#searchOrganizationStockist(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject searchOrganizationStockist(String searchOption,String searchText, Integer organizationId,Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<Stockist> stockistList = null;
		JSONObject stockistJson = null;
		JSONArray  stockistArray = new JSONArray();
		Organization organization = null;
		Integer totalPage = 0;
		try{
			Criteria criteria = session.createCriteria(Stockist.class,"stockist")
					.createAlias("stockist.organization", "org")
					.add(Restrictions.eq("status", 1))
					.setProjection(Projections.rowCount())
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null){
					criteria.add(Restrictions.eq("org.organizationId", organizationId));
			}
			if("Organization".equals(searchOption)){
				criteria.add(Restrictions.like("org.organizationName", searchText,MatchMode.START));
			}
			else if("StockistName".equals(searchOption))
				criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
			Long totalRecordCount = (Long)criteria.uniqueResult();
			
			criteria = session.createCriteria(Stockist.class,"stockist")
					.createAlias("stockist.organization", "org")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null){
					criteria.add(Restrictions.eq("org.organizationId", organizationId));
			}
			if("Organization".equals(searchOption)){
				criteria.add(Restrictions.like("org.organizationName", searchText,MatchMode.START));
			}
			else if("StockistName".equals(searchOption))
				criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
			
			from = from - 1 ;
			criteria.setFirstResult(from*Constant.PAGE_COUNT);
			criteria.setMaxResults(Constant.PAGE_COUNT);
			stockistList = criteria.list();
			
			if(totalRecordCount>0){
				totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPage=totalPage+1;
				}
			}
			
			Iterator<Stockist> stockistIt = stockistList.iterator();
			while(stockistIt.hasNext()){
				Stockist temp = stockistIt.next();
				if("1".equals(temp.getOrganization().getOrganizationStatus())){
				stockistJson = new JSONObject();
				organization = (Organization) session.get(Organization.class, temp.getOrganization().getOrganizationId());
				stockistJson.put("ORG_NAME", organization.getOrganizationName());
				stockistJson.put("STOCKIST_ID", temp.getStockistId());
				stockistJson.put("STOCKIST_NAME", temp.getStockistName());
				stockistArray.put(stockistJson);
				}
			}
			stockistJson = new JSONObject();
			stockistJson.put("paginationCount", totalPage);
			stockistJson.put("stockistArray",stockistArray);
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistJson;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#uploadStockistDocument(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.StockistDocument)
	 */
	@Override
	public JSONObject uploadStockistDocument(Integer orgId, Integer stockistId,Integer companyId, Integer documentTypeId,StockistDocument stockistDocument) {
		Organization organization=null;
		Company company=null;
		Stockist stockist = null;
		DocumentListType docType = null;
		JSONObject result = new JSONObject();
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization) session.load(Organization.class, orgId);
			company = (Company) session.load(Company.class, companyId);
			stockist = (Stockist) session.load(Stockist.class, stockistId);
			docType = (DocumentListType) session.load(DocumentListType.class, documentTypeId);
			
			stockistDocument.setOrganization(organization);
			stockistDocument.setCompany(company);
			stockistDocument.setStockist(stockist);
			stockistDocument.setDocumentListType(docType);
			
		       int i =(Integer) session.save(stockistDocument);
		       if(i>0){
		    	   result.put("result", true);
		    	   //System.out.println("Uploaded successfully id = "+stockist.getStockistId());
		       }
		       else{
		    	   result.put("result", false);
		    	//   System.out.println("Failed");
		       }
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return result;
	}
	//
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#uploadDocumentListing(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray uploadDocumentListing(Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<StockistDocument> stockistDocument = null;
		Organization organization=null;
		Company company=null;
		Stockist stockist = null;
		DocumentListType docType = null;
		JSONObject stockistDocJson = null;
		JSONArray  stockistDocArray = new JSONArray();
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		try{
			
			Criteria criteria = session.createCriteria(StockistDocument.class,"stockistDoc")
					.createAlias("stockistDoc.organization", "org")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
					criteria.add(Restrictions.eq("org.organizationId", organizationId));
			
			stockistDocument = criteria.list();
			Iterator<StockistDocument> stockistDocIt = stockistDocument.iterator();
			//System.out.println("Darrrrrrrr");
			while(stockistDocIt.hasNext()){
				StockistDocument temp = stockistDocIt.next();
				stockist = (Stockist) session.get(Stockist.class, temp.getStockist().getStockistId());
				if(stockist.getStatus()==1){
					organization = (Organization) session.get(Organization.class, temp.getOrganization().getOrganizationId());
					company = (Company) session.get(Company.class, temp.getCompany().getCompanyId());
					docType = (DocumentListType) session.get(DocumentListType.class, temp.getDocumentListType().getDocumentListTypeId());
					if("1".equals(organization.getOrganizationStatus())&&company.getStatus()==1){
						stockistDocJson = new JSONObject();
						stockistDocJson.put("STOCKIST_DOC_ID", temp.getStockistDocumentId());
						stockistDocJson.put("ORG_ID", organization.getOrganizationId());
						stockistDocJson.put("ORG_NAME", organization.getOrganizationName());
						stockistDocJson.put("COMPANY_ID", company.getCompanyId());
						stockistDocJson.put("COMPANY_NAME", company.getCompanyName());
						stockistDocJson.put("STOCKIST_ID", stockist.getStockistId());
						stockistDocJson.put("STOCKIST_NAME", stockist.getStockistName());
						stockistDocJson.put("DOCUMENT_TYPE_ID", docType.getDocumentListTypeId());
						stockistDocJson.put("DOCUMENT_TYPE", docType.getDocumentName());
						json_ImageArray=new JSONArray();
						for(StockistDocumentImagePath tempObj : temp.getStockistDocumentImagePathList()){
							tempJson = new JSONObject();
							tempJson.put("IMAGE_PATH", tempObj.getImagePath());
							tempJson.put("IMAGE_ID", tempObj.getStockistDocumentImagePathId());
							json_ImageArray.put(tempJson);
						}
						stockistDocJson.put("FILE_NAME", json_ImageArray);
						stockistDocArray.put(stockistDocJson);
					}
				}
			}
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistDocArray;
	}
	
	@Override
	public List<StockistDocumentImagePath> updateCompanyUploadDocumentImage(Integer stockistDocumentId,
			List<StockistDocumentImagePath> documentList) {
		StockistDocument stockistDocument = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			stockistDocument = (StockistDocument) session.load(StockistDocument.class, stockistDocumentId);
			for(StockistDocumentImagePath temp : documentList){
				temp.setStockistDocument(stockistDocument);
				session.save(temp);
			}
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistDocument.getStockistDocumentImagePathList();
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#searchUploadedDocumentsDetails(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray searchUploadedDocumentsDetails(String searchOption,String searchText, Integer organizationId, Integer from) {
		Session session =sessionFactory.getCurrentSession();
		List<StockistDocument> stockistDocument = null;
		Organization organization=null;
		Company company=null;
		Stockist stockist = null;
		DocumentListType docType = null;
		JSONObject stockistDocJson = null;
		JSONArray  stockistDocArray = new JSONArray();
		Map<Object, Object>map = null;
		JSONObject tempJson = null;
		JSONArray json_ImageArray = null;
		try{
			Criteria criteria = session.createCriteria(StockistDocument.class,"stockistDoc")
					.createAlias("stockistDoc.organization", "org")
					.createAlias("stockistDoc.company", "company")
					.createAlias("stockistDoc.stockist", "stockist")
					.createAlias("stockistDoc.documentListType", "documentListType")
					.add(Restrictions.eq("company.status", 1))
					.add(Restrictions.eq("stockist.status", 1))
					.add(Restrictions.eq("org.organizationStatus", "1"))
					.add(Restrictions.eq("status", 1))
					  .setProjection(Projections.rowCount());
					  	
			if(organizationId!=null)
					criteria.add(Restrictions.eq("org.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("org.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("StockistName".equals(searchOption))
				criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
			else if("DocumentType".equals(searchOption))
				criteria.add(Restrictions.like("documentListType.documentName", searchText,MatchMode.START));
			
			 Long totalRecordCount = (Long)criteria.uniqueResult();
			
			 criteria = session.createCriteria(StockistDocument.class,"stockistDoc")
					.createAlias("stockistDoc.organization", "org")
					.createAlias("stockistDoc.company", "company")
					.createAlias("stockistDoc.stockist", "stockist")
					.createAlias("stockistDoc.documentListType", "documentListType")
					.add(Restrictions.eq("status", 1))
					.add(Restrictions.eq("company.status", 1))
					.add(Restrictions.eq("stockist.status", 1))
					.add(Restrictions.eq("org.organizationStatus", "1"))
					.addOrder(Order.desc("submitDate"));
				
			if(organizationId!=null)
					criteria.add(Restrictions.eq("org.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("org.organizationName", searchText,MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText,MatchMode.START));
			else if("StockistName".equals(searchOption))
				criteria.add(Restrictions.like("stockist.stockistName", searchText,MatchMode.START));
			else if("DocumentType".equals(searchOption))
				criteria.add(Restrictions.like("documentListType.documentName", searchText,MatchMode.START));
			
		   from=from-1;
		   criteria.setFirstResult(from *Constant.PAGE_COUNT);
		   criteria.setMaxResults(Constant.PAGE_COUNT);
		   stockistDocument=criteria.list();
			   
		   Integer tatalPage = 0;
			if(totalRecordCount>0)
			{
				tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0)
				{
					tatalPage=tatalPage+1;
				}
			}
			
			Iterator<StockistDocument> stockistDocIt = stockistDocument.iterator();
			while(stockistDocIt.hasNext()){
				StockistDocument temp = stockistDocIt.next();
				stockist = (Stockist) session.get(Stockist.class, temp.getStockist().getStockistId());
				//if(stockist.getStatus()==1){
					organization = (Organization) session.get(Organization.class, temp.getOrganization().getOrganizationId());
					company = (Company) session.get(Company.class, temp.getCompany().getCompanyId());
					docType = (DocumentListType) session.get(DocumentListType.class, temp.getDocumentListType().getDocumentListTypeId());
					//if("1".equals(organization.getOrganizationStatus())&&company.getStatus()==1){
						stockistDocJson = new JSONObject();
						stockistDocJson.put("STOCKIST_DOC_ID", temp.getStockistDocumentId());
						stockistDocJson.put("ORG_NAME", organization.getOrganizationName());
						stockistDocJson.put("ORG_ID", organization.getOrganizationId());
						stockistDocJson.put("COMPANY_NAME", company.getCompanyName());
						stockistDocJson.put("COMPANY_ID", company.getCompanyId());
						stockistDocJson.put("STOCKIST_NAME", stockist.getStockistName());
						stockistDocJson.put("STOCKIST_ID", stockist.getStockistId());
						stockistDocJson.put("DOCUMENT_TYPE", docType.getDocumentName());
						stockistDocJson.put("DOCUMENT_TYPE_ID", docType.getDocumentListTypeId());
						json_ImageArray=new JSONArray();
						for(StockistDocumentImagePath tempObj : temp.getStockistDocumentImagePathList()){
							tempJson = new JSONObject();
							tempJson.put("IMAGE_PATH", tempObj.getImagePath());
							tempJson.put("IMAGE_ID", tempObj.getStockistDocumentImagePathId());
							json_ImageArray.put(tempJson);
						}
						stockistDocJson.put("FILE_NAME", json_ImageArray);
						stockistDocArray.put(stockistDocJson);
					//}
				//}
			}
			tempJson = new JSONObject();
			tempJson.put("paginationCount", tatalPage);
			stockistDocArray.put(tempJson);
		}
		catch(JSONException e){
			e.printStackTrace();log.error(e);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistDocArray;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#deleteStockistDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteStockistDetails(Integer stockistId) {
		Session session =sessionFactory.getCurrentSession();
		Stockist stockist = null;
		try{
			stockist = (Stockist)session.get(Stockist.class, stockistId);
			stockist.setStatus(0);
			session.update(stockist);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#deleteStockistDocumentDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteStockistDocumentDetails(Integer stockistDocId) {
		Session session =sessionFactory.getCurrentSession();
		StockistDocument stockistDocument = null;
		try{
			stockistDocument = (StockistDocument)session.get(StockistDocument.class, stockistDocId);
			stockistDocument.setStatus(0);
			session.update(stockistDocument);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#loadStockistObjUsigStokistId(java.lang.Integer)
	 */
	@Override
	public Stockist loadStockistObjUsigStokistId(Integer stokistId) {
		Session session =sessionFactory.getCurrentSession();
		Stockist stokistObj=null;
		try 
		{
			stokistObj=(Stockist)session.get(Stockist.class,stokistId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stokistObj;
	}
	@Override
	public Stockist loadStockistObjectByStockistId(Integer stockistId) {
		Session session =sessionFactory.getCurrentSession();
		Stockist stockistObj=null;
		try 
		{
			stockistObj=(Stockist)session.get(Stockist.class,stockistId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stockistObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#getStockistCompanyList(java.lang.Integer)
	 */
	@Override
	public JSONArray getStockistCompanyList(Integer stockistId) {
		List<Company> companyList = null;
		Session session =sessionFactory.getCurrentSession();
		Stockist stockistObj=null;
		JSONArray json_data_array= new JSONArray();
		JSONObject json=null;
		try 
		{
			stockistObj=(Stockist)session.get(Stockist.class,stockistId );
			companyList = stockistObj.getCompanyList();
			for(Company tempCompany:companyList){
				json=new JSONObject();
				json.put("COMPANY_NAME", tempCompany.getCompanyName());
				json_data_array.put(json);
			}
			
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return json_data_array;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#insertStockistDetilsAndDeleteOldStockistDetails(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.Stockist)
	 */
	@Override
	public Integer insertStockistDetilsAndDeleteOldStockistDetails(Integer stockistOldId, Integer transporterDetailsId, Integer stateId, Integer districtId,Integer cityId, Stockist stockist) {
		TransporterDetails transporter=null;
		State state=null;
		District district=null;
		City city = null;
		Stockist stockistOld = null;
		Integer id = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			transporter = (TransporterDetails) session.load(TransporterDetails.class, transporterDetailsId);
			state = (State) session.load(State.class, stateId);
			district = (District) session.load(District.class, districtId);
			city = (City) session.load(City.class, cityId);
			stockist.setTransporterDetails(transporter);
			stockist.setState(state);
			stockist.setDistrict(district);
			stockist.setCity(city);
	        session.save(stockist.getUserDetails());
			id =(Integer) session.save(stockist);
	        if(id!=null && id>0){
	        	stockistOld =  (Stockist) session.load(Stockist.class, stockistOldId);
	        	stockistOld.setStatus(0);
	        	stockistOld.getUserDetails().setUserName(UUID.randomUUID().toString());
	        	
	        	List<Company> companyList = stockistOld.getCompanyList();
	        	stockist.setCompanyList(companyList);
	        	stockistOld.setCompanyList(null);
	        	session.update(stockistOld);
	        	session.update(stockist);
	        	List<DrugLicence> drugLicenseList = stockistOld.getDrugLicenceList();
	        	for(DrugLicence drugLicence : drugLicenseList){
	        		drugLicence.setStockist(stockist);
	        		session.update(drugLicence);
	        	}
	        	
	        	/*Query query=session.createQuery("update STOCKIST_COMPANY_JOIN set stockistId=:stockistId where stockistId=:oldStockistId");
	        	query.setInteger("stockistId", id);
	        	query.setInteger("oldStockistId", stockistOldId);
	        	int modifications=query.executeUpdate();
	        	System.out.println("modifications in STOCKIST_COMPANY_JOIN = "+modifications);
	        	*/
	        	Query query=session.createQuery("update StockistBankDetails set stockistId=:stockistId where stockistId=:oldStockistId");
	        	query.setInteger("stockistId", id);
	        	query.setInteger("oldStockistId", stockistOldId);
	        	int modifications=query.executeUpdate();
	        	System.out.println("modifications in StockistBankDetails = "+modifications);
	        	
	        	return id;
	        }
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return id;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StockistDao#updateStockistDetails(com.protocol.wms.model.Stockist)
	 */
	@Override
	public Boolean updateStockistDetails(Stockist stockist) {
		Session session =sessionFactory.getCurrentSession();
		try{
		       session.update(stockist);
		       return true;
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return false;
	}
	@Override
	public StockistDocument loadStockistDocumentObjById(Integer stockistDocumentId) {
		StockistDocument stockistDocument = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			stockistDocument = (StockistDocument) session.get(StockistDocument.class, stockistDocumentId);
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return stockistDocument;
	}
	@Override
	public Boolean updateStockistDocumentObj(StockistDocument stockistDocument) {
		Session session =sessionFactory.getCurrentSession();
		try{
		       session.update(stockistDocument);
		       return true;
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return false;
	}
	
	@Override
	public Boolean deleteStockistDocumentFileImage(Integer stockistDocumentPathId) {
		StockistDocumentImagePath stockistDocumentImagePath = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			stockistDocumentImagePath = (StockistDocumentImagePath)session.load(StockistDocumentImagePath.class, stockistDocumentPathId);
		       session.delete(stockistDocumentImagePath);
		       return true;
		}
		catch(Exception e){
			e.printStackTrace();log.error(e);
		}
		return false;
	}
}
