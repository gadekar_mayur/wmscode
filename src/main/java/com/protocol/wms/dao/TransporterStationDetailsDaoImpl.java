/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.ClaimRates;
import com.protocol.wms.model.RatePerStation;
import com.protocol.wms.model.TransporterStationDetails;

/**
 * @author Sudhakar
 *
 */
@Repository(value="TransporterStationDetailsDaoImpl")
public class TransporterStationDetailsDaoImpl implements TransporterStationDetailsDao {
	
	private Logger log = Logger.getLogger(TransporterStationDetailsDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
		
		@Autowired
		@Qualifier("sessionFactory")
		public void setSessionFactory(SessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.TransporterStationDetailsDao#saveTransporterStationDetails(com.protocol.wms.model.TransporterStationDetails)
		 */
		@Override
		public Integer saveTransporterStationDetails(
				TransporterStationDetails tsdObj) {
			Session session =sessionFactory.getCurrentSession();
			Integer transporterStationDetailsId=null;
			try 
			{
				transporterStationDetailsId=(Integer) session.save(tsdObj);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return transporterStationDetailsId;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.TransporterStationDetailsDao#laodTransporterStationDetailsUsignTransporterStationDetailsId(java.lang.Integer)
		 */
		@Override
		public TransporterStationDetails laodTransporterStationDetailsUsignTransporterStationDetailsId(
				Integer transporterStationDetailsId) {
			Session session =sessionFactory.getCurrentSession();
			TransporterStationDetails transporterStationDetailsObj=null;
			try 
			{
				transporterStationDetailsObj=(TransporterStationDetails) session.get(TransporterStationDetails.class, transporterStationDetailsId);
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return transporterStationDetailsObj;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.TransporterStationDetailsDao#loadTransporterStationDetailsListUsignOrganizationId(java.lang.Integer)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public List<TransporterStationDetails> loadTransporterStationDetailsListUsignOrganizationId(
				Integer organizationId) {
			Session session =sessionFactory.getCurrentSession();
			List<TransporterStationDetails> transporterStationDetailsList=null;
			try 
			{
				Criteria criteria = session.createCriteria(TransporterStationDetails.class,"TransporterStationDetails")
					     .createAlias("TransporterStationDetails.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .addOrder(Order.desc("submitDate"));;
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   transporterStationDetailsList=criteria.list();
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return transporterStationDetailsList;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.TransporterStationDetailsDao#searchStationPersonContactDetails(java.lang.String, java.lang.String, java.lang.Integer)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public Map<String,Object> searchStationPersonContactDetails(String searchOption, String searchText, Integer from,Integer organizationId) {
			Session session =sessionFactory.getCurrentSession();
			List<TransporterStationDetails> transporterStationDetailsList=null;
			Map<String,Object>map=null;
			try
			{
				if("Mobile".equals(searchOption)){
					Query query = session.createSQLQuery("CALL searchStationPersonContactDetails(:searchText,:organizationId)")
							.addEntity(TransporterStationDetails.class)
							.setParameter("searchText", searchText+"%")
							.setParameter("organizationId", organizationId);
					transporterStationDetailsList = query.list();
				}
				else
				{
			Criteria criteria = session.createCriteria(TransporterStationDetails.class,"TransporterStationDetails")
					 .createAlias("TransporterStationDetails.transporterDetails", "transporterDetails")
				     .createAlias("TransporterStationDetails.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .addOrder(Order.desc("submitDate"))
				     .setProjection(Projections.rowCount());
				   if(organizationId!=null)
				     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				   
				   if("Organization".equals(searchOption))
					   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				   if("Transporter".equals(searchOption))
					   criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
				   if("StationName".equals(searchOption))
					   criteria.add(Restrictions.like("TransporterStationDetails.transporterStationName", searchText,MatchMode.START));
				   if("ContactName".equals(searchOption))
					   criteria.add(Restrictions.like("TransporterStationDetails.StationContactName", searchText,MatchMode.START));
				   if("Email".equals(searchOption))
					   criteria.add(Restrictions.like("TransporterStationDetails.transporterStationEmailId", searchText,MatchMode.START));
				  // transporterStationDetailsList=criteria.list();
				   Long totalRecordCount = (Long)criteria.uniqueResult();
	 //----------------------------------------------------------------------------
			    criteria = session.createCriteria(TransporterStationDetails.class,"TransporterStationDetails")
			    		 .createAlias("TransporterStationDetails.transporterDetails", "transporterDetails")
					     .createAlias("TransporterStationDetails.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .addOrder(Order.desc("submitDate"));
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   if("Organization".equals(searchOption))
						   criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					   if("Transporter".equals(searchOption))
						   criteria.add(Restrictions.like("transporterDetails.transporterName", searchText,MatchMode.START));
					   if("StationName".equals(searchOption))
						   criteria.add(Restrictions.like("TransporterStationDetails.transporterStationName", searchText,MatchMode.START));
					   if("ContactName".equals(searchOption))
						   criteria.add(Restrictions.like("TransporterStationDetails.StationContactName", searchText,MatchMode.START));
					   if("Email".equals(searchOption))
						   criteria.add(Restrictions.like("TransporterStationDetails.transporterStationEmailId", searchText,MatchMode.START));
					   transporterStationDetailsList=criteria.list();
	//----------------------------------------------------------------------------
				   from = from - 1 ;
				   criteria.setFirstResult(from*Constant.PAGE_COUNT);
				   criteria.setMaxResults(Constant.PAGE_COUNT);
				   transporterStationDetailsList=criteria.list();
					
					Integer totalPage = 0;
					if(totalRecordCount>0){
					totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPage=totalPage+1;
				}
			}
					map = new HashMap<String, Object>();
					map.put("transporterStationDetailsList", transporterStationDetailsList);
					map.put("totalPages", totalPage);
				}
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return map;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.TransporterStationDetailsDao#insertNewTransporterStationDetailsAndDeleteOldRecordDetails(java.lang.Integer, com.protocol.wms.model.TransporterStationDetails)
		 */
		@Override
		public Integer insertNewTransporterStationDetailsAndDeleteOldRecordDetails(Integer stationPersonContactDetailsId,
				TransporterStationDetails transporterStationDetails) {
			ClaimRates clainRate = null;
			RatePerStation ratePerStation = null;
			Session session =sessionFactory.getCurrentSession();
			TransporterStationDetails oldTransporterStationDetailsObj = null;
			Integer Id = null;
			try 
			{
				clainRate = transporterStationDetails.getClaimRates();
				ratePerStation = transporterStationDetails.getRatePerStation();
				
				Id = (Integer) session.save(transporterStationDetails);
				if(Id!=null&&Id>0){
					clainRate.setTransporterStationDetails(transporterStationDetails);
					session.save(clainRate);
					ratePerStation.setTransporterStationDetails(transporterStationDetails);
					session.save(ratePerStation);
					oldTransporterStationDetailsObj = (TransporterStationDetails) session.load(TransporterStationDetails.class, stationPersonContactDetailsId);
					oldTransporterStationDetailsObj.setStatus(0);
					session.update(oldTransporterStationDetailsObj);
					return Id;
				}
			}
			catch(DataAccessException e)
			{
				Id = null;
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				Id = null;
				e.printStackTrace();
				log.error(e);
			}
			return Id;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.TransporterStationDetailsDao#updateTransporterStationDetails(com.protocol.wms.model.TransporterStationDetails)
		 */
		@Override
		public Boolean updateTransporterStationDetails(TransporterStationDetails transporterStationDetails) {
			Session session =sessionFactory.getCurrentSession();
			try 
			{
				session.update(transporterStationDetails);
				return true;
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return false;
		}

}
