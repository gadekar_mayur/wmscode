/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;



import java.util.Map;

import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.ChequeNumberInfo;
import com.protocol.wms.model.InwardCourierRegistration;
import com.protocol.wms.model.InwardCourierRegistrationDocketFilePath;
import com.protocol.wms.model.Particulars;

/**
 * @author admin
 *
 */
public interface InwardCourierDao {

	public List<Particulars> getParticularsDropdownList();
	
	public Boolean addInwardCourierRegistration(String companyStockistOption,Integer orgId,Integer companyId ,Integer stateId, Integer districtId, Integer cityId,Integer stockistId ,Integer particular ,Integer companyBankId,InwardCourierRegistration inwardCourierRegistration, AdvancedChequeInformation advancedChequeInformation,List<ChequeNumberInfo>chequeNumberInfoList);
	
	public Boolean editInwardCourierRegistration(String companyStockistOption,Integer orgId,Integer companyId ,Integer state ,Integer district ,Integer city ,Integer stockistId ,Integer particular ,Integer companyBankId,InwardCourierRegistration inwardCourierRegistration, AdvancedChequeInformation advancedChequeInformation,List<ChequeNumberInfo>chequeNumberInfoList);
	
	public List<InwardCourierRegistration> inwardCourierRegistrationListing(Integer organizationId);
	
	public Map<Object,Object> searchInwardCourierRegistrationDetails(String searchOption,String searchText,String fromSpecialData, String toSpecialData,Integer organizationId, Integer from);
	
	public Boolean deleteInwardCourierRegDetails(Integer inwardCourierRegId);
	
	public InwardCourierRegistration loadInwardCourierRegistrationObjectUsignInwardCourierRegId(Integer inwardCourierRegId);
	
	public Boolean deleteInwardCourierRegistrationDocketFileImage(Integer docketFileImageId);
	
	public List<InwardCourierRegistrationDocketFilePath> updateInwardCourierRegDocketFileImage(Integer inwardCourierRegId, List<InwardCourierRegistrationDocketFilePath> docketFileImgList);
}
