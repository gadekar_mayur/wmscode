/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;


import com.protocol.wms.model.InwardGoodsRegistrationClaimLetter;
import com.protocol.wms.model.InwardGoodsRegistrationClaimLetterImagePath;
import com.protocol.wms.model.InwardGoodsRegistrationClaimPicturesImagePath;

/**
 * @author Sudhakar
 *
 */
public interface InwardGoodsRegistrationClaimLetterDao {
	
	public Integer saveInwardGoodsRegistrationClaimLetter(InwardGoodsRegistrationClaimLetter  inwardGoodsRegistrationClaimLetterObj);

	public InwardGoodsRegistrationClaimLetter getInwardGoodsRegistrationClaimLetterObj(Integer inwardGoodsRegistrationClaimLetterId);
	
	public Boolean editInwardGoodsRegistrationClaimLetter(InwardGoodsRegistrationClaimLetter claimObj);
	
	public Boolean deleteInwardGoodsRegistrationClaimLetterImage(Integer claimLetterImageId);
	
	public Boolean deleteInwardGoodsRegistrationClaimPictureImage(Integer claimPictureImageId);
	
	public List<InwardGoodsRegistrationClaimLetterImagePath> updateInwardGoodsRegClaimLetterImage(Integer inwardGoodsRegistrationClaimLetterId, List<InwardGoodsRegistrationClaimLetterImagePath> claimLetterImgList);
	
	public List<InwardGoodsRegistrationClaimPicturesImagePath> updateInwardGoodsRegClaimPicturesImage(Integer inwardGoodsRegistrationClaimLetterId,List<InwardGoodsRegistrationClaimPicturesImagePath> claimPicturesImgList);
}
