/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.OutWardReturnGoodsCreditNote;
import com.protocol.wms.model.OutWardReturnGoodsCreditNoteCreditNoteImagePath;

/**
 * @author admin
 *
 */
@Repository(value="OutWardReturnGoodsCreditNoteDaoImpl")
public class OutWardReturnGoodsCreditNoteDaoImpl implements	OutWardReturnGoodsCreditNoteDao {

private Logger log = Logger.getLogger(OutwardReturnGoodsLRDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardReturnGoodsCreditNoteDao#saveOutwardReturnGoodsRegCreditNote(java.lang.Integer, com.protocol.wms.model.OutWardReturnGoodsCreditNote)
	 */
	@Override
	public Boolean saveOutwardReturnGoodsRegCreditNote(Integer organiztionId,
			OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote) {
		Organization organization = null;
		Integer id = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization)session.load(Organization.class, organiztionId);
			outWardReturnGoodsCreditNote.setOrganization(organization);
			id = (Integer)session.save(outWardReturnGoodsCreditNote);
			if(id!=null)
				return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardReturnGoodsCreditNoteDao#updateOutwardReturnGoodsRegCreditNote(com.protocol.wms.model.OutWardReturnGoodsCreditNote)
	 */
	@Override
	public Boolean updateOutwardReturnGoodsRegCreditNote(
			OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.update(outWardReturnGoodsCreditNote);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardReturnGoodsCreditNoteDao#getOutwardReturnGoodsRegCreditNoteList(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardReturnGoodsCreditNote> getOutwardReturnGoodsRegCreditNoteList(
			Integer organizationId) {
		List<OutWardReturnGoodsCreditNote> creditNoteList=null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(OutWardReturnGoodsCreditNote.class,"creditNote")
					.createAlias("creditNote.organization", "organization")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			creditNoteList = criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return creditNoteList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardReturnGoodsCreditNoteDao#deleteOutwardReturnGoodsRegCreditNote(java.lang.Integer)
	 */
	@Override
	public Boolean deleteOutwardReturnGoodsRegCreditNote(Integer creditNoteId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardReturnGoodsCreditNote creditNote= null;
		try{
			creditNote = (OutWardReturnGoodsCreditNote)session.get(OutWardReturnGoodsCreditNote.class, creditNoteId);
			creditNote.setStatus(0);
			session.update(creditNote);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardReturnGoodsCreditNoteDao#loadOutWardReturnGoodsCreditNoteObjById(java.lang.Integer)
	 */
	@Override
	public OutWardReturnGoodsCreditNote loadOutWardReturnGoodsCreditNoteObjById(
			Integer outWardReturnGoodsCreditNoteId) {
		OutWardReturnGoodsCreditNote creditNote= null;
		Session session =sessionFactory.getCurrentSession();
		try{
			creditNote = (OutWardReturnGoodsCreditNote)session.get(OutWardReturnGoodsCreditNote.class, outWardReturnGoodsCreditNoteId);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return creditNote;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardReturnGoodsCreditNoteDao#editOutwardReturnGoodsRegCreditNote(java.lang.Integer, com.protocol.wms.model.OutWardReturnGoodsCreditNote)
	 */
	@Override
	public Boolean editOutwardReturnGoodsRegCreditNote(Integer organiztionId,
			OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote) {
		Organization organization = null;
		Session session =sessionFactory.getCurrentSession();
		
		try{
			organization = (Organization)session.load(Organization.class, organiztionId);
			outWardReturnGoodsCreditNote.setOrganization(organization);
			session.update(outWardReturnGoodsCreditNote);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Object,Object> searchOutwardReturnGoodsRegCreditNote(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from) {
		
		List<OutWardReturnGoodsCreditNote> OutReturnGoodsRegCreditNote = null;
		Map<Object, Object>map = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(OutWardReturnGoodsCreditNote.class,"outwardgoodreturnCreditNote")
					.createAlias("outwardgoodreturnCreditNote.organization", "organization")
					.add(Restrictions.eq("outwardgoodreturnCreditNote.status",1))
					 .setProjection(Projections.rowCount())
					.addOrder(Order.desc("outwardgoodreturnCreditNote.submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
		
			switch(selectedValue){
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
			case "CreditNoteNo" :
				 criteria.add(Restrictions.like("outwardgoodreturnCreditNote.creditNoteNo", searchText,MatchMode.START));
				 break;
			case "CreditNoteAmount" :
				 criteria.add(Restrictions.ge("outwardgoodreturnCreditNote.creditNoteAmount", Double.parseDouble(fromSpecialData)));
				  criteria.add(Restrictions.le("outwardgoodreturnCreditNote.creditNoteAmount",Double.parseDouble(toSpecialData)));
				  break;
			case "ClaimNo" :
				 criteria.add(Restrictions.like("outwardgoodreturnCreditNote.refNoOrClaimNo", searchText,MatchMode.START));
				 break;
			}
			
			 Long totalRecordCount = (Long)criteria.uniqueResult();
			 criteria = session.createCriteria(OutWardReturnGoodsCreditNote.class,"outwardgoodreturnCreditNote")
					.createAlias("outwardgoodreturnCreditNote.organization", "organization")
					.add(Restrictions.eq("outwardgoodreturnCreditNote.status",1))
					.addOrder(Order.desc("outwardgoodreturnCreditNote.submitDate"));
			
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
		
			switch(selectedValue){
			case "Organization" :
				 criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
				 break;
			case "CreditNoteNo" :
				 criteria.add(Restrictions.like("outwardgoodreturnCreditNote.creditNoteNo", searchText,MatchMode.START));
				 break;
			case "CreditNoteAmount" :
				 criteria.add(Restrictions.ge("outwardgoodreturnCreditNote.creditNoteAmount", Double.parseDouble(fromSpecialData)));
				  criteria.add(Restrictions.le("outwardgoodreturnCreditNote.creditNoteAmount",Double.parseDouble(toSpecialData)));
				  break;
			case "ClaimNo" :
				 criteria.add(Restrictions.like("outwardgoodreturnCreditNote.refNoOrClaimNo", searchText,MatchMode.START));
				 break;
			}
			
			 from=from-1;
			   criteria.setFirstResult(from *Constant.PAGE_COUNT);
			   criteria.setMaxResults(Constant.PAGE_COUNT);
				 
			   OutReturnGoodsRegCreditNote=criteria.list();
			   
			   Integer tatalPage = 0;
				if(totalRecordCount>0)
				{
					 
					tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0)
					{
						tatalPage=tatalPage+1;
					}
				}
//				System.out.println("totalRecordCount : "+totalRecordCount);
//				System.out.println("tatalPage : "+tatalPage);
			
					map = new HashMap<Object, Object>();
					map.put("paidInvoice", OutReturnGoodsRegCreditNote);
					map.put("totalPages", tatalPage);
			
		}
		catch(Exception e){
			log.error(e);
			e.printStackTrace();
		}
		return map;
	}

	@Override
	public List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> updateOutwardGoodsRegCreditNoteAttachImage(
			Integer creditNoteId,
			List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> creditNoteAttachList) {
		OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote = null;
		List<OutWardReturnGoodsCreditNoteCreditNoteImagePath> lst = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardReturnGoodsCreditNote = (OutWardReturnGoodsCreditNote) session.load(OutWardReturnGoodsCreditNote.class, creditNoteId);
			for(OutWardReturnGoodsCreditNoteCreditNoteImagePath tempObj : creditNoteAttachList){
				tempObj.setOutWardReturnGoodsCreditNote(outWardReturnGoodsCreditNote);
				session.save(tempObj);
			}
			lst = outWardReturnGoodsCreditNote.getOutWardReturnGoodsCreditNoteCreditNoteImagePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}

	@Override
	public Boolean deleteOutwardGoodsRegCreditNoteAttachImage(Integer creditNoteAttachImageId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardReturnGoodsCreditNoteCreditNoteImagePath outWardReturnGoodsCreditNoteCreditNoteImagePath= null;
		try{
			outWardReturnGoodsCreditNoteCreditNoteImagePath = (OutWardReturnGoodsCreditNoteCreditNoteImagePath)session.load(OutWardReturnGoodsCreditNoteCreditNoteImagePath.class, creditNoteAttachImageId);
			session.delete(outWardReturnGoodsCreditNoteCreditNoteImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardReturnGoodsCreditNoteDao#getOutWardReturnGoodsCreditNoteDetailsByLR_ID(java.lang.Integer)
	 
	@Override
	public OutWardReturnGoodsCreditNote getOutWardReturnGoodsCreditNoteDetailsByLR_ID(Integer outWardReturnGoodsLRDetailsId) {
		OutWardReturnGoodsCreditNote outWardReturnGoodsCreditNote = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria critera = session.createCriteria(OutWardReturnGoodsCreditNote.class,"creditNote")
					.createAlias("creditNote.outWardReturnGoodsLRDetails", "LR_Details")
					.add(Restrictions.eq("LR_Details.outWardReturnGoodsLRDetailsId", outWardReturnGoodsLRDetailsId));
			outWardReturnGoodsCreditNote = (OutWardReturnGoodsCreditNote) critera.uniqueResult();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardReturnGoodsCreditNote;
	}*/
}
