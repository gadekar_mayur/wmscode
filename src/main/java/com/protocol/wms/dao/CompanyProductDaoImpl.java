/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.CompanyProduct;

/**
 * @author nutan
 *
 */
@Repository(value="CompanyProductDaoImpl")
public class CompanyProductDaoImpl implements CompanyProductDao {

private Logger log = Logger.getLogger(CompanyDepoDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	//Add by nutan
	@Override
	public CompanyProduct loadCompanyProductObjectUsignCompanyProductId(
			Integer companyProductId) {
		
		Session session =sessionFactory.getCurrentSession();
		CompanyProduct companyProductObj=null;
		try 
		{
			companyProductObj=(CompanyProduct)session.get(CompanyProduct.class,companyProductId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return companyProductObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyProductDao#searchCompanyProductDetails(java.lang.String, java.lang.String, java.lang.Double, java.lang.Double, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String , Object> searchCompanyProductDetails(String searchOption, String searchText, Double fromValue,Double toValue, Integer from, Integer organizationId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyProduct> companyProductList = null;
		Map<String , Object> map = null;
		try{
			Criteria criteria = session.createCriteria(CompanyProduct.class,"companyProduct")
					.createAlias("companyProduct.organization", "organization")
					.createAlias("companyProduct.company", "company")
					.add(Restrictions.eq("organization.organizationStatus", "1"))
					.add(Restrictions.eq("company.status",1))
					.add(Restrictions.eq("companyProduct.status",1))
					.setProjection(Projections.rowCount());
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText, MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText, MatchMode.START));
			else if("ProductName".equals(searchOption))
				criteria.add(Restrictions.like("companyProduct.companyProductName", searchText, MatchMode.START));
			else if("PTD".equals(searchOption)){
				criteria.add(Restrictions.ge("companyProduct.companyProductPTD", fromValue));
				criteria.add(Restrictions.le("companyProduct.companyProductPTD", toValue));
				
			}
			else if("Type".equals(searchOption))
				criteria.add(Restrictions.like("companyProduct.companyProductType", searchText, MatchMode.START));
			else if("MRP".equals(searchOption)){
				criteria.add(Restrictions.ge("companyProduct.companyProductMRP", fromValue));
				criteria.add(Restrictions.le("companyProduct.companyProductMRP", toValue));
			}
			Long totalRecordCount = (Long)criteria.uniqueResult();
			//- - - - - - -  - - - - - - - - - - - - 
			criteria = session.createCriteria(CompanyProduct.class,"companyProduct")
					.createAlias("companyProduct.organization", "organization")
					.createAlias("companyProduct.company", "company")
					.add(Restrictions.eq("organization.organizationStatus", "1"))
					.add(Restrictions.eq("company.status",1))
					.add(Restrictions.eq("companyProduct.status",1))
					.addOrder(Order.desc("companyProduct.submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			
			if("Organization".equals(searchOption))
				criteria.add(Restrictions.like("organization.organizationName", searchText, MatchMode.START));
			else if("Company".equals(searchOption))
				criteria.add(Restrictions.like("company.companyName", searchText, MatchMode.START));
			else if("ProductName".equals(searchOption))
				criteria.add(Restrictions.like("companyProduct.companyProductName", searchText, MatchMode.START));
			else if("PTD".equals(searchOption)){
				criteria.add(Restrictions.ge("companyProduct.companyProductPTD", fromValue));
				criteria.add(Restrictions.le("companyProduct.companyProductPTD", toValue));
				
			}
			else if("Type".equals(searchOption))
				criteria.add(Restrictions.like("companyProduct.companyProductType", searchText, MatchMode.START));
			else if("MRP".equals(searchOption)){
				criteria.add(Restrictions.ge("companyProduct.companyProductMRP", fromValue));
				criteria.add(Restrictions.le("companyProduct.companyProductMRP", toValue));
			}
			from = from - 1;
		    criteria.setFirstResult(from*Constant.PAGE_COUNT);
		    criteria.setMaxResults(Constant.PAGE_COUNT);
			companyProductList = criteria.list();
			Integer tatalPage = 0;
			if(totalRecordCount>0){
				tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					tatalPage=tatalPage+1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("companyProductList", companyProductList);
			map.put("totalPages", tatalPage);
		}catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyProductDao#loadCompanyProductListUsignCompanyId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyProduct> loadCompanyProductListUsignCompanyId(
			Integer companyId) {
		Session session =sessionFactory.getCurrentSession();
		List<CompanyProduct> CompanyProductList=null;
		try 
		{
			Criteria criteria = session.createCriteria(CompanyProduct.class,"CompanyProduct")
					.createAlias("CompanyProduct.company", "company")
					.add(Restrictions.eq("company.companyId", companyId))
				     .add(Restrictions.eq("status", 1));
			CompanyProductList=criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return CompanyProductList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyProductDao#saveProduct(com.protocol.wms.model.CompanyProduct)
	 */
	@Override
	public Integer saveNewCompanyProductDetailsAndDeleteOldOne(Integer companyProductId,CompanyProduct companyProduct) {
		Integer productId = null;
		CompanyProduct oldCompanyProduct = null;
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			oldCompanyProduct = (CompanyProduct) session.load(CompanyProduct.class, companyProductId);
			oldCompanyProduct.setStatus(0);
			session.update(oldCompanyProduct);
			productId = (Integer) session.save(companyProduct);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return productId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CompanyProductDao#updateCompanyProductDetails(com.protocol.wms.model.CompanyProduct)
	 */
	@Override
	public Boolean updateCompanyProductDetails(CompanyProduct companyProduct) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(companyProduct);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
}
