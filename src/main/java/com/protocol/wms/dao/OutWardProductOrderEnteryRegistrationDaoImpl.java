/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.OutWardProductOrderEntryRegistration;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OutWardProductOrderEnteryRegistrationDaoImpl")
public class OutWardProductOrderEnteryRegistrationDaoImpl implements OutWardProductOrderEnteryRegistrationDao{

	private Logger log = Logger.getLogger(OutWardProductOrderEnteryRegistrationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardProductOrderEnteryRegistrationDao#saveOutWardProductOrderEnteryRegistration(com.protocol.wms.model.OutWardProductOrderEnteryRegistration)
	 */
	@Override
	public Integer saveOutWardProductOrderEnteryRegistration(
			OutWardProductOrderEntryRegistration outWardProductOrderEnteryRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer outWardProductOrderEnteryRegistrationId = null;
		try 
		{
			outWardProductOrderEnteryRegistrationId=(Integer) session.save(outWardProductOrderEnteryRegistrationObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardProductOrderEnteryRegistrationId;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<OutWardProductOrderEntryRegistration> getProductOrderEnteryRegistrationDetailsUsingOutWardOrderEntryRegistrationId(
			Integer OutWardOrderEntryRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutWardProductOrderEntryRegistration> outWardProductOrderEntryRegistrationList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutWardProductOrderEntryRegistration.class,"OutWardProductOrderEntryRegistration")
					.createAlias("OutWardProductOrderEntryRegistration.outWardOrderEntryRegistration", "outWardOrderEntryRegistration")
			    .add(Restrictions.eq("outWardOrderEntryRegistration.outWardOrderEnteryRegistrationId", OutWardOrderEntryRegistrationId));
			outWardProductOrderEntryRegistrationList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardProductOrderEntryRegistrationList;
		
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardProductOrderEnteryRegistrationDao#loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(java.lang.Integer)
	 */
	@Override
	public OutWardProductOrderEntryRegistration loadOutWardProductOrderEntryRegistrationUsingOutWardProductOrderEntryRegistrationId(
			Integer outWardProductOrderEntryRegistrationId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj=null;
		try 
		{
			outWardProductOrderEntryRegistrationObj=(OutWardProductOrderEntryRegistration)session.get(OutWardProductOrderEntryRegistration.class,outWardProductOrderEntryRegistrationId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardProductOrderEntryRegistrationObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardProductOrderEnteryRegistrationDao#updateOutWardProductOrderEntryRegistrationObj(com.protocol.wms.model.OutWardProductOrderEntryRegistration)
	 */
	@Override
	public boolean updateOutWardProductOrderEntryRegistrationObj(
			OutWardProductOrderEntryRegistration OutWardProductOrderEntryRegistrationObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(OutWardProductOrderEntryRegistrationObj);
			return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardProductOrderEnteryRegistrationDao#deleteOutWardProductOrderEntryRegistrationObj(com.protocol.wms.model.OutWardProductOrderEntryRegistration)
	 */
	@Override
	public boolean deleteOutWardProductOrderEntryRegistrationObj(
			OutWardProductOrderEntryRegistration outWardProductOrderEntryRegistrationObj) {
		Session session=sessionFactory.getCurrentSession();
		try 
		{
			session.delete(outWardProductOrderEntryRegistrationObj);
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
}
