package com.protocol.wms.dao;

import java.util.List;

public interface OrganizationBankCompanyJoinDAO {
	
	//Add by nutan
	public List<Integer> loadOrganizationBankIdListUsingCompanyId(Integer companyId);
}
