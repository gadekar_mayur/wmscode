/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;


import com.protocol.wms.model.InwardGoodsRegistrationInvoiceDetails;
import com.protocol.wms.model.InwardGoodsRegistrationInvoiceStnNoImagePath;

/**
 * @author Sudhakar
 *
 */
public interface InwardGoodsRegistrationInvoiceDetailsDao {

	public Integer saveInwardGoodsRegistrationInvoiceDetails(InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetailsObj);
	
	public List<InwardGoodsRegistrationInvoiceDetails> loadInwardGoodsRegistrationInvoiceDetailsListUsignInwardGoodsRegistrationId(Integer inwardGoodsRegistrationId);
	
	public InwardGoodsRegistrationInvoiceDetails  getInwardGoodsRegistrationInvoiceDetailsObjById(Integer inwardGoodsRegistrationInvoiceDetailsId);
	
	public Boolean editInwardGoodsRegistrationInvoiceDetails(InwardGoodsRegistrationInvoiceDetails inwardGoodsRegistrationInvoiceDetails);
	
	public Boolean deleteInwardGoodsRegInvoiceStnImage(Integer stnImageId);
	
	public List<InwardGoodsRegistrationInvoiceStnNoImagePath> updateInwardGoodsRegInvoiceStnImage(Integer inwardGoodsRegistrationInvoiceId, List<InwardGoodsRegistrationInvoiceStnNoImagePath> stnImgList);
}
