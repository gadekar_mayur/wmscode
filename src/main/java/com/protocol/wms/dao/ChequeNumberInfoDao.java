/**
 * 
 */
package com.protocol.wms.dao;

import com.protocol.wms.model.ChequeNumberInfo;

/**
 * @author Sudhakar
 *
 */
public interface ChequeNumberInfoDao {
	
	public ChequeNumberInfo loadChequeNumberInfoObjUsignChequeNumberInfoId(Integer ChequeNumberInfoId);
	
	public boolean updateChequeNumberInfoObj(ChequeNumberInfo chequeNumberInfoObj);
	

	public Integer returnNoOfChequeUsingAdvancedChequeInformationId(Integer AdvancedChequeInformationId);
	
	public boolean deleteChequeNumberInfoObj(ChequeNumberInfo chequeNumberInfoObj);
}
