/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.OrganizationDocument;
import com.protocol.wms.model.OrganizationDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
public interface OrganizationDocumentDao {
	
	public Integer saveOrganizationDocument(OrganizationDocument organizationDocumentObj);
	
	public List<OrganizationDocument> loadOrganizationDocumentsListUsingOrganiztionId(Integer organizationId);
	
	//public Map<String, Object> getOrganizationDocumentsListUsingOrganiztionId(Integer organizationId, Integer from);
	
	public Map<String , Object> searchOrgaiztionDocumentDetails(String searchOption, String searchText, Integer from,Integer organizationId);

	public Boolean  updateOrganizationDocumentObj(OrganizationDocument organizationDocument);
	
	public OrganizationDocument  loadOrganizationDocumentObjById(Integer organizationDocumentId);
	
	public List<OrganizationDocumentImagePath> updateOrganizationUploadDocumentImage(Integer organizationDocumentId,List<OrganizationDocumentImagePath> documentList);
	
	public Boolean deleteOrganizationDocumentFileImage(Integer orgDocumentPathId);
}
