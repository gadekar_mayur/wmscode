/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.OutWardGatePass;
import com.protocol.wms.model.OutWardGatePassLrImagePath;

/**
 * @author Sudhakar
 *
 */
public interface OutWardGatePassDao {
	
	public Integer saveOutWardGatePass(OutWardGatePass OutWardGatePassObj);
	
	public OutWardGatePass loadOutWardGatePassObjUsignOutWardGatePassId(Integer outWardGatePassId);
	
	public boolean updateOutWardGatePassObj(OutWardGatePass outWardGatePassObj);
	
	public boolean deleteLrDetails(OutWardGatePass outWardGatePassObj);
	
	public List<OutWardGatePassLrImagePath> updateOutwardGoodsGatePassLrImage(Integer outwardGatePassId, List<OutWardGatePassLrImagePath> lrImgList);

	public Boolean deleteOutwardGoodsGatePassLrImage(Integer lrImageId);
}
