/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.CompanyBank;

/**
 * @author admin
 *
 */
public interface CompanyBankDetailsDao {

	public Map<Object,Object> searchCompanyBankDetails(String searchOption,String searchText,Integer organizationId, Integer from);
}
