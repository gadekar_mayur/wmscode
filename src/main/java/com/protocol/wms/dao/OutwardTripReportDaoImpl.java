/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.OutwardTripReport;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OutwardTripReportDaoImpl")
public class OutwardTripReportDaoImpl implements OutwardTripReportDao{

	private Logger log = Logger.getLogger(OutwardTripReportDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardTripReportDao#saveOutwardTripReport(com.protocol.wms.model.OutwardTripReport)
	 */
	@Override
	public Integer saveOutwardTripReport(OutwardTripReport outwardTripReportObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer outWardTripReportId = null;
		try 
		{
			outWardTripReportId=(Integer) session.save(outwardTripReportObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardTripReportId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardTripReportDao#loadOutwardTripReportListUsignOutWardTripId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OutwardTripReport> loadOutwardTripReportListUsignOutWardTripId(
			Integer outWardTripId) {
		Session session =sessionFactory.getCurrentSession();
		List<OutwardTripReport> outwardTripReportList=null;
		try 
		{
			Criteria criteria = session.createCriteria(OutwardTripReport.class,"OutwardTripReport")
					.createAlias("OutwardTripReport.outWardTrip", "outWardTrip")
					.add(Restrictions.eq("outWardTrip.outWardTripId",outWardTripId));
			outwardTripReportList=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outwardTripReportList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutwardTripReportDao#deletePrintedOutwardTripReportObj(java.util.List)
	 */
	@Override
	public void deletePrintedOutwardTripReportObj(List<OutwardTripReport> printedOutwardTripReportObjList) 
	{
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			if(printedOutwardTripReportObjList.size()>0)
			{
				for(OutwardTripReport outwardTripReportObj : printedOutwardTripReportObjList)
				{
					session.delete(outwardTripReportObj);
					//session.delete(session.get(OutwardTripReport.class, outwardTripReportObj.getOutwardTripReportId()));
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
	}
}
