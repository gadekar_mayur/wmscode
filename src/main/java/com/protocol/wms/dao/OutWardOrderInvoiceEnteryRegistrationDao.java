/**
 * 
 */
package com.protocol.wms.dao;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;

/**
 * @author Sudhakar
 *
 */
public interface OutWardOrderInvoiceEnteryRegistrationDao {
	
	public Integer saveOutWardOrderInvoiceEnteryRegistrationObject(OutWardOrderInvoiceEnteryRegistration obj);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationWithDispatchEnteryRegistrationStatusZero(Integer organizationId);
	
	public OutWardOrderInvoiceEnteryRegistration loadOutWardOrderInvoiceEnteryRegistrationUsignOutWardOrderInvoiceEnteryRegistrationId(Integer OutWardOrderInvoiceEnteryRegistrationId);
	
	public void updateOutWardOrderInvoiceEnteryRegistrationObj(OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListEqualToCurrentDate(Date currentDate,Integer organizationId);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListLessThanToCurrentDate(Date currentDate,Integer organizationId);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListGreaterThanToCurrentDate(Date currentDate,Integer organizationId);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListUsignoutWardOrderEnteryRegistrationId(Integer outWardOrderEnteryRegistrationId);
	
	public List<OutWardOrderInvoiceEnteryRegistration> loadOutWardOrderInvoiceEnteryRegistrationListWithStatusOne(Integer orgnizationId);
	
	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistrationUsignoutWardOrderEnteryRegistrationId(Integer outWardOrderEnteryRegistrationId);

	public List<OutWardOrderInvoiceEnteryRegistration> getOutWardOrderInvoiceEnteryRegistrationListUsingoutWardDispatchEnteryRegistrationId(Integer outWardDispatchEnteryRegistrationId);
	
	public boolean updateInvoiceObj(OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj,String invoiceDate,String invoiceDiscountTakentOrNot,Double netAmount);

	public Map<String,Object> searchOutwardDispatchEntryRegistrationList(
			String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData,Integer from, Integer organizationId);
	
	public boolean deleteOutWardOrderInvoiceEnteryRegistrationObj(OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj);

	public Map<Object, Object> searchOutstandingTodaysDue(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from);

	public Map<Object, Object> searchOutstandingLapsed(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from);

	public Map<Object,Object> searchOutstandingUpcoming(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from);

	public Map<Object, Object> searchOutstandingPaidInvoice(
			Date ourJavaDateObject, String selectedValue, String searchText,
			String fromSpecialData, String toSpecialData, Integer organizationId, Integer from);
	
//	public boolean updateInvoiceObject(OutWardOrderInvoiceEnteryRegistration outWardOrderInvoiceEnteryRegistrationObj);
	
	
	
}
