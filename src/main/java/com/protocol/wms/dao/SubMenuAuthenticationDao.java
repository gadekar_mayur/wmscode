/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.SubMenuAuthentication;

/**
 * @author admin
 *
 */
public interface SubMenuAuthenticationDao {
	
	public List<SubMenuAuthentication> loadSubMenuUsingMenuAuthenticationId(Integer menuAuthenticationId);

}
