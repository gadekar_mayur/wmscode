/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.CartingAgent;
import com.protocol.wms.model.CartingAgentImagePath;
import com.protocol.wms.model.CartingAgentVehiclePhotoImagePath;
import com.protocol.wms.model.Organization;
import com.protocol.wms.model.UserDetails;
import com.protocol.wms.model.UserType;

/**
 * @author admin
 *
 */
@Repository(value="CartingAgentDaoImpl")
public class CartingAgentDaoImpl implements CartingAgentDao {
	private Logger log = Logger.getLogger(CartingAgentDaoImpl.class.getClass());
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentDao#saveCartingAgentDetails(org.springframework.web.multipart.MultipartFile, org.springframework.web.multipart.MultipartFile, java.lang.Integer, java.lang.Integer, com.protocol.wms.model.CartingAgent)
	 */
	@Override
	public Boolean saveCartingAgentDetails(Integer organizationId, CartingAgent cartingAgent,UserDetails userDetails) {
		Organization organization = null;
		//TransporterDetails transporterDetails = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			organization = (Organization)session.load(Organization.class, organizationId);
			//transporterDetails = (TransporterDetails)session.load(TransporterDetails.class, transporterId);
			cartingAgent.setOrganization(organization);
			//cartingAgent.setTransporterDetails(transporterDetails);
			if(userDetails!=null){
		    	   UserType userType = (UserType) session.load(UserType.class,Constant.CATERING_AGENT);
		    	   userDetails.setUserType(userType);
		    	   int j = (Integer) session.save(userDetails);
		    	   //System.out.println("UserDetails Id = "+j);
		    	   cartingAgent.setUserDetails(userDetails);
		       }
			Integer id = (Integer)session.save(cartingAgent);
			if(id!=null)
				return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentDao#updateCartingAgent(com.protocol.wms.model.CartingAgent)
	 */
	@Override
	public Boolean updateCartingAgent(CartingAgent cartingAgent) {
		Session session =sessionFactory.getCurrentSession();
		try{
			session.update(cartingAgent);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentDao#getCartingAgentList(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CartingAgent> getCartingAgentList(Integer organizationId) {
		List<CartingAgent> cartingAgentList = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(CartingAgent.class,"cartingAgent")
					.createAlias("cartingAgent.organization", "organization")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("submitDate"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			cartingAgentList = criteria.list();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return cartingAgentList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentDao#getCartingAgentDetailsById(java.lang.Integer)
	 */
	@Override
	public CartingAgent getCartingAgentDetailsById(Integer agentId) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgent cartingAgent = null;
		try{
			Criteria criteria = session.createCriteria(CartingAgent.class)
					.add(Restrictions.eq("status", 1))
					.add(Restrictions.eq("cartingAgentId", agentId));
			cartingAgent = (CartingAgent) criteria.uniqueResult();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return cartingAgent;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CartingAgentDao#deleteCartingAgentDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCartingAgentDetails(Integer agentId) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgent cartingAgent= null;
		try{
			cartingAgent = (CartingAgent)session.get(CartingAgent.class, agentId);
			cartingAgent.setStatus(0);
			session.update(cartingAgent);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object>searchCartingAgentsDetails(
			int searchOptioinINT,String searchText, Integer from, Integer organizationId)
	{
		Session session =sessionFactory.getCurrentSession();
		List<CartingAgent> cartingAgentList=null;
		Map<String,Object>map=null;
		
		String searchOptioin = null;
		if(searchOptioinINT==0)
		{
			searchOptioin="Organization";
		}
		else if(searchOptioinINT==1)
		{
			searchOptioin="AgentName";
		}
		else if(searchOptioinINT==2)
		{
			searchOptioin="VEHICLE_NO";
		}
		else if(searchOptioinINT==3)
		{
			searchOptioin="VEHICLE_MODEL";
		}
		else if(searchOptioinINT==4)
		{
			searchOptioin="MOBILE";
		}
		
		try 
		{
			Criteria criteria = session.createCriteria(CartingAgent.class,"CartingAgent")
				     .createAlias("CartingAgent.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("organization.organizationStatus", "1"))
				     .setProjection(Projections.rowCount());
		   if(organizationId!=null)
		     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
		   
		   if("Organization".equals(searchOptioin))
			     criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
		   else if("AgentName".equals(searchOptioin))
			     criteria.add(Restrictions.like("CartingAgent.name", searchText,MatchMode.START));
		   else if("VEHICLE_NO".equals(searchOptioin))
			     criteria.add(Restrictions.like("CartingAgent.vehicleNumber", searchText,MatchMode.START));
		   else if("VEHICLE_MODEL".equals(searchOptioin))
			     criteria.add(Restrictions.like("CartingAgent.model", searchText,MatchMode.START));
		   else if("MOBILE".equals(searchOptioin))
			     criteria.add(Restrictions.like("CartingAgent.mobileNo", searchText,MatchMode.START));
		   Long totalRecordCount = (Long)criteria.uniqueResult();
			
		   criteria = session.createCriteria(CartingAgent.class,"CartingAgent")
				     .createAlias("CartingAgent.organization", "organization")
				     .add(Restrictions.eq("status", 1))
				     .add(Restrictions.eq("organization.organizationStatus", "1"))
				     .addOrder(Order.desc("submitDate"));;
		   if(organizationId!=null)
		     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
		   
		   if("Organization".equals(searchOptioin))
			     criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
		   else if("AgentName".equals(searchOptioin))
			     criteria.add(Restrictions.like("CartingAgent.name", searchText,MatchMode.START));
		   else if("VEHICLE_NO".equals(searchOptioin))
			     criteria.add(Restrictions.like("CartingAgent.vehicleNumber", searchText,MatchMode.START));
		   else if("VEHICLE_MODEL".equals(searchOptioin))
			     criteria.add(Restrictions.like("CartingAgent.model", searchText,MatchMode.START));
		   else if("MOBILE".equals(searchOptioin))
			     criteria.add(Restrictions.like("CartingAgent.mobileNo", searchText,MatchMode.START));
		   cartingAgentList=criteria.list();
				   
		   from = from - 1 ;
		   criteria.setFirstResult(from*Constant.PAGE_COUNT);
		   criteria.setMaxResults(Constant.PAGE_COUNT);
		   cartingAgentList=criteria.list();
			
			Integer totalPage = 0;
			if(totalRecordCount>0){
				totalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
				if((totalRecordCount%Constant.PAGE_COUNT)!=0){
					totalPage=totalPage+1;
				}
			}
			map = new HashMap<String, Object>();
			map.put("cartingAgentList", cartingAgentList);
			map.put("totalPages", totalPage);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}
	
	@Override
	public CartingAgent loadCartingAgentObjectUsingAgentId(Integer agentId) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgent cartingAgentObj=null;
		try 
		{
			cartingAgentObj=(CartingAgent)session.get(CartingAgent.class,agentId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return cartingAgentObj;
	}

	@Override
	public Boolean updateCartingAgentObject(CartingAgent cartingAgent) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(cartingAgent);
			session.update(cartingAgent.getUserDetails());
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public Integer insertCartingAgentDetilsAndDeleteOldCartingAgentDetails(Integer oldCartingAgentId, CartingAgent cartingAgent) {
		Integer id = null;
		CartingAgent oldCartingAgent = null;
		Session session =sessionFactory.getCurrentSession();
		///try
		///{
			oldCartingAgent = (CartingAgent) session.load(CartingAgent.class, oldCartingAgentId);
			oldCartingAgent.setStatus(0);
			oldCartingAgent.setUserDetails(null);
			session.update(oldCartingAgent);
			id = (Integer) session.save(cartingAgent);
			cartingAgent = (CartingAgent) session.get(CartingAgent.class, id);
			if(cartingAgent.getCartingAgentTripCount()!=null){
				cartingAgent.getCartingAgentTripCount().setCartingAgent(cartingAgent);
				session.save(cartingAgent.getCartingAgentTripCount());
			}
			
			//System.out.println("Id = "+id);
		//} 
		/*catch(DataAccessException e)
		{id = null;
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e)
		{id = null;
			e.printStackTrace();
			log.error(e);
		}*/
		return id;
	}

	@Override
	public void updateCartingAgentImagesObjectByNewCartingAgentId(
			Integer oldCartingAgentId, Integer newCartingAgentId) {
		CartingAgent cartingAgent = null;
		Session session =sessionFactory.getCurrentSession();
		try
		{
			cartingAgent = (CartingAgent) session.load(CartingAgent.class, newCartingAgentId);
			Criteria criteria  = session.createCriteria(CartingAgentImagePath.class,"cartingAgentImagePath")
					.createAlias("cartingAgentImagePath.cartingAgent", "cartingAgent")
					.add(Restrictions.eq("cartingAgent.cartingAgentId", oldCartingAgentId));
			List<CartingAgentImagePath> imageList = criteria.list();
			for(CartingAgentImagePath tempObj : imageList){
				tempObj.setCartingAgent(cartingAgent);
				session.update(tempObj);
			}
			criteria  = session.createCriteria(CartingAgentVehiclePhotoImagePath.class,"cartingAgentImagePath")
					.createAlias("cartingAgentImagePath.cartingAgent", "cartingAgent")
					.add(Restrictions.eq("cartingAgent.cartingAgentId", oldCartingAgentId));
			List<CartingAgentVehiclePhotoImagePath> vehicleImageList = criteria.list();
			for(CartingAgentVehiclePhotoImagePath tempObj : vehicleImageList){
				tempObj.setCartingAgent(cartingAgent);
				session.update(tempObj);
			}
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error(e);
		}
	}

	@Override
	public List<CartingAgentImagePath> updateCartingAgentPhotoImage(Integer cartingAgentId, List<CartingAgentImagePath> cartingAgentPhotoImgList) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgent cartingAgent = null;
		{
			cartingAgent = (CartingAgent) session.load(CartingAgent.class, cartingAgentId);
			for(CartingAgentImagePath imageObj : cartingAgentPhotoImgList){
				imageObj.setCartingAgent(cartingAgent);
				session.save(imageObj);
			}
		}
		return cartingAgent.getCartingAgentImagePathList();
	}

	@Override
	public Boolean deleteCartingAgentPhotoImage(Integer cartingAgentPhotoImageId) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgentImagePath cartingAgentImagePath= null;
		try{
			cartingAgentImagePath = (CartingAgentImagePath)session.get(CartingAgentImagePath.class, cartingAgentPhotoImageId);
			session.delete(cartingAgentImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<CartingAgentVehiclePhotoImagePath> updateCartingAgentVehiclePhotoImage(
			Integer cartingAgentId, List<CartingAgentVehiclePhotoImagePath> cartingAgentVehiclePhotoImgList) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgent cartingAgent = null;
		{
			cartingAgent = (CartingAgent) session.load(CartingAgent.class, cartingAgentId);
			for(CartingAgentVehiclePhotoImagePath imageObj : cartingAgentVehiclePhotoImgList){
				imageObj.setCartingAgent(cartingAgent);
				session.save(imageObj);
			}
		}
		return cartingAgent.getCartingAgentVehiclePhotoImagePathList();
	}

	@Override
	public Boolean deleteCartingAgentVehiclePhotoImage(Integer cartingAgentVehiclePhotoImageId) {
		Session session =sessionFactory.getCurrentSession();
		CartingAgentVehiclePhotoImagePath cartingAgentVehiclePhotoImagePath= null;
		try{
			cartingAgentVehiclePhotoImagePath = (CartingAgentVehiclePhotoImagePath)session.get(CartingAgentVehiclePhotoImagePath.class, cartingAgentVehiclePhotoImageId);
			session.delete(cartingAgentVehiclePhotoImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
}
