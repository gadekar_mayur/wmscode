/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.DocumentListType;

/**
 * @author Sudhakar
 *
 */
public interface DocumentListTypeDao {
	
	public List<DocumentListType> loadDocumentListType();
	
	public DocumentListType loadDocumentListTypeUsingDocumentListTypeId(Integer documentListTypeId);

}
