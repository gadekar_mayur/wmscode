/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;


import com.protocol.wms.model.CreditNote;
import com.protocol.wms.model.CreditNoteCreditNoteImagePath;

/**
 * @author Sudhakar
 *
 */
public interface CreditNoteDao {

	public Integer saveCreditNoteObj(CreditNote creditNoteObj);
	
	public CreditNote loadCreditNoteObjectUsingCreditNoteId(Integer creditNoteId);
	
	public Boolean editInwardReturnGoodsRegCreditNoteDetails(CreditNote creditNote);
	
	public List<CreditNoteCreditNoteImagePath> updateCreditNoteRegCreditNoteAttachImage(Integer creditNoteId,List<CreditNoteCreditNoteImagePath> creditNoteImgList);
	
	public Boolean deleteCreditNoteRegCreditNoteAttachImage(Integer creditNoteAttachImageId);
}
