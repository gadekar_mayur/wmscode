package com.protocol.wms.dao;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.City;
import com.protocol.wms.model.District;

/**
 * @author admin
 *
 */
@Repository(value="CityDaoImpl")
public class CityDaoImpl implements CityDao {
	
	private Logger log = Logger.getLogger(CityDaoImpl.class.getClass());

	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getCityList(Integer districtId) {
		Session session =sessionFactory.getCurrentSession();
		List<City> cityList = null;
		JSONObject cityJson = null;
		JSONArray  cityArray = new JSONArray();
		try{
			Criteria criteria = session.createCriteria(City.class,"city")
					.createAlias("city.district", "dist")
					.add(Restrictions.eq("dist.districtId", districtId))
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.asc("city.cityName"))
					.addOrder(Order.desc("cityId"));
					
			cityList = criteria.list();
			Iterator<City> cityIt = cityList.iterator();
			//System.out.println("Darrrrrrrr");
			while(cityIt.hasNext()){
				City temp = cityIt.next();
				cityJson = new JSONObject();
				cityJson.put("cityId", temp.getCityId());
				cityJson.put("cityName", temp.getCityName());
				cityArray.put(cityJson);
			}
			cityJson = new JSONObject();
			cityJson.put("cityArray",cityArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return cityJson;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CityDao#loadCityObjectUsingCityId(java.lang.Integer)
	 */
	@Override
	public City loadCityObjectUsingCityId(Integer cityId) {
		Session session =sessionFactory.getCurrentSession();
		City cityObj=null;
		try 
		{
			cityObj=(City) session.get(City.class,cityId);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return cityObj;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CityDao#saveCity(java.lang.Integer, java.lang.String)
	 */
	@Override
	public JSONObject saveCity(Integer districtId,String cityName) {
		District district = null;
		City city = null;
		JSONObject result = new JSONObject();
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(City.class,"city")
					.createAlias("city.district", "district")
					.add(Restrictions.eq("cityName", cityName))
					.add(Restrictions.eq("status",1))
					.add(Restrictions.eq("district.districtId",districtId));
			city = (City)criteria.uniqueResult();
			
			if(city!=null){
				result.put("result", false);
				result.put("MSG", "City Already Exists...!");
				result.put("FLAG", true);
				return result;
			}
			district = (District) session.load(District.class, districtId);
			city = new City();
			city.setCityName(cityName.toUpperCase());
			city.setDistrict(district);
			city.setStatus(1);
			 int i =(Integer) session.save(city);
		       if(i>0){
		    	   result.put("result", true);
		    	   result.put("MSG", "City Added Succesfully...!");
		       }
		       else{
		    	   result.put("result", false);
		    	   result.put("MSG", "Add City failed...!");
		       }
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
			 /*try {
				result.put("result", false);
				result.put("MSG", "Operation failed...!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}*/
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.CityDao#deleteCityDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteCityDetails(Integer cityId) {
		Session session =sessionFactory.getCurrentSession();
		City city = null;
		try{
			city = (City)session.get(City.class, cityId);
			city.setStatus(0);
			session.update(city);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	@Override
	public boolean updateCityObject(City cityObj) {

		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(cityObj);
			return true;
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
	
	@Override
	public boolean cityAlreadyExistsOrNot(Integer districtId, String cityName) {

		City city=null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(City.class,"city")
					.createAlias("city.district", "district")
					.add(Restrictions.eq("cityName", cityName.toUpperCase()))
					.add(Restrictions.eq("status",1))
					.add(Restrictions.eq("district.districtId", districtId));
			city = (City)criteria.uniqueResult();
			
			if(city==null)
				return false;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return true;
	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<City> searchOptionForCity(String searchOption,
			String searchText, Integer organizationId) {
		// TODO Auto-generated method stub

			Session session =sessionFactory.getCurrentSession();
			List<City> cityList=null;
			
			try 
			{
				Criteria criteria = session.createCriteria(City.class,"city")
						.createAlias("city.district", "district")
						.createAlias("district.state", "state")
						.createAlias("state.organization", "organization")
						.add(Restrictions.eq("status", 1))
						.add(Restrictions.eq("city.status", 1))
						.add(Restrictions.eq("district.status", 1))
						.add(Restrictions.eq("state.status", 1))
						.add(Restrictions.eq("organization.organizationStatus", "1"))
						.addOrder(Order.desc("cityId"));
				
				if(organizationId!=null)
					criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				
				if("Organization".equals(searchOption)){
					criteria.add(Restrictions.like("organization.organizationName", searchText, MatchMode.START));
				}else if("District".equals(searchOption)){
					criteria.add(Restrictions.like("district.districtName", searchText, MatchMode.START));
				}
				else if("State".equals(searchOption)){
					criteria.add(Restrictions.like("state.stateName", searchText, MatchMode.START));
				}
				else if("City".equals(searchOption)){
					criteria.add(Restrictions.like("city.cityName", searchText, MatchMode.START));
				}
				
				cityList = criteria.list();
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return cityList;
		}
	@Override
	public String getCityName(Integer cityId) {
		Session session=sessionFactory.getCurrentSession();
		City city=(City) session.get(City.class,cityId);
		return city.getCityName();
	}
}
