/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.Organization;
import com.protocol.wms.model.State;

/**
 * @author Sudhakar
 *
 */
@Repository(value="StateDaoImpl")
public class StateDaoImpl implements StateDao{

	private Logger log = Logger.getLogger(StateDaoImpl.class.getClass());

	private SessionFactory sessionFactory;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StateDao#loadStateList()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<State> loadStateList(Integer orgId) 
	{
		Session session =sessionFactory.getCurrentSession();
		List<State> stateList=null;
		try 
		{
			Criteria criteria = session.createCriteria(State.class,"State")
					.add(Restrictions.eq("State.status", 1));
			if(orgId!=null)
			criteria.add(Restrictions.eq("organization.organizationId", orgId));
			stateList=criteria.list();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stateList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StateDao#loadSateObjectUsingStateId(java.lang.Integer)
	 */
	@Override
	public State loadSateObjectUsingStateId(Integer stateId) {
		Session session =sessionFactory.getCurrentSession();
		State stateObj=null;
		try 
		{
			stateObj=(State)session.get(State.class,stateId);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stateObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StateDao#saveStateForOrganization(java.lang.Integer, java.lang.String)
	 */
	@Override
	public JSONObject saveStateForOrganization(Integer orgId, String stateName) {
		Organization organization=null;
		State state=null;
		JSONObject result = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(State.class,"state")
					.createAlias("state.organization", "organization")
					.add(Restrictions.eq("stateName", stateName))
					.add(Restrictions.eq("status",1))
					.add(Restrictions.eq("organization.organizationId", orgId));
			state = (State)criteria.uniqueResult();
			
			if(state!=null){
				result = new JSONObject();
				result.put("result", false);
				result.put("MSG", "State Already Exists...!");
				result.put("FLAG", true);
				
				return result;
			}
			organization = (Organization) session.load(Organization.class, orgId);
			state = new State();
			state.setStateName(stateName.toUpperCase());
			state.setOrganization(organization);
			state.setStatus(1);
			 int i =(Integer) session.save(state);
		       if(i>0){
		    	   result = new JSONObject();
		    	   result.put("result", true);
		    	   result.put("MSG", "State Added Succesfully...!");
		       }
		       else{
		    	   result = new JSONObject();
		    	   result.put("result", false);
		    	   result.put("MSG", "Add State failed...!");
		       }
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
			 /*try {
				result.put("result", false);
				result.put("MSG", "Operation failed...!");
			} catch (JSONException e1) {
				e1.printStackTrace();log.error(e1);
			}*/
		}
		return result;
	}

	@Override
	public Boolean stateAlreadyExistsOrNot(Integer orgId, String stateName) {
		State state=null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(State.class,"state")
					.createAlias("state.organization", "organization")
					.add(Restrictions.eq("stateName", stateName.toUpperCase()))
					.add(Restrictions.eq("status",1))
					.add(Restrictions.eq("organization.organizationId", orgId));
			state = (State)criteria.uniqueResult();
			
			if(state==null)
				return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e);
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StateDao#getStateListByOrg(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<State> getStateListByOrg(Integer organizationId) {
		List<State> stateList = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			Criteria criteria = session.createCriteria(State.class,"state")
					.createAlias("state.organization", "organization")
					.add(Restrictions.eq("status",1))
					.addOrder(Order.desc("stateId"));
			if(organizationId!=null)
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				stateList = criteria.list();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return stateList;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.StateDao#deleteStateDetails(java.lang.Integer)
	 */
	@Override
	public Boolean deleteStateDetails(Integer stateId) {
		Session session =sessionFactory.getCurrentSession();
		State state = null;
		try{
			state = (State)session.get(State.class, stateId);
			state.setStatus(0);
			session.update(state);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public Boolean updateStateObj(State stateObj) {
		Session session =sessionFactory.getCurrentSession();
		try 
		{
			session.update(stateObj);
			return true;
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<State> searchOptionForState(String searchOption,String searchText) {
		
		Session session =sessionFactory.getCurrentSession();
		List<State> stateList=null;
		
		try 
		{
			Criteria criteria = session.createCriteria(State.class,"state")
					.createAlias("state.organization", "organization")
					.add(Restrictions.eq("status", 1))
					.addOrder(Order.desc("stateId"));
			
			Integer organizationId= (Integer)httpSession.getAttribute("ORGANIZATION_ID");
			if(organizationId!=null){
				criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			}
			if("Organization".equals(searchOption)){
				criteria.add(Restrictions.like("organization.organizationName", searchText, MatchMode.START));
			}else if("State".equals(searchOption)){
				criteria.add(Restrictions.like("state.stateName", searchText, MatchMode.START));
			}
			stateList = criteria.list();
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return stateList;
	}
}


