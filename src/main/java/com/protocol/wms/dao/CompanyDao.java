package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.protocol.wms.model.Company;
import com.protocol.wms.model.CompanyBank;
import com.protocol.wms.model.CompanyCreditAndDiscount;
import com.protocol.wms.model.CompanyDocument;
import com.protocol.wms.model.CompanyProduct;
import com.protocol.wms.model.UserDetails;

public interface CompanyDao {
	
	public JSONObject getCompanyList(Integer orgId);
	
	public Company loadCompanyObjectUsingCompanyId(Integer companyId);
	
	public JSONObject getCompanyListAccToStockist(Integer stockistId,	Integer orgId);
	
	public Boolean saveCompanyDetails(Integer orgId,Integer stateId,Integer districtId,Integer cityId,Integer roleTypeId,UserDetails userDetails, Company company);
	
	public List<Company> companyDetailsList(Integer organizationId);
	
	public Map<String,Object> searchAssignedBankDetailsOfOrgToCompany(String searchOption,String searchText,Integer from, Integer organizationId);
	
	public Map<String , Object> searchCompanyDetails(String searchOption,String searchText, Integer from, Integer orgId);
	
	public boolean saveCompanyBankDetails(Integer orgId,Integer companyId,CompanyBank companyBank);
	
	public List<CompanyBank> companyBankDetailsListing(Integer organizationId);
	
	public List<CompanyBank> companyBankDropdown(Integer companyId,Integer organizationId);
	
	/*public Boolean saveCompanyDepoDetails(Integer organizationId,Integer companyId,Integer stateId,Integer districtId,Integer cityId,CompanyDepo companyDepo);
	
	public List<CompanyDepo> getCompanyDepoListing(Integer organizationId);
	
	public List<CompanyDepo> getCompanyDepoListByCompanyId(Integer companyId);*/
	
	public Boolean saveCompanyProduct(Integer organizationId,Integer companyId,CompanyProduct companyProduct);
	
	public List<CompanyProduct> getCompanyProductListing(Integer organizationId);
	
	public JSONArray getCompanyOragainzationBankList(Integer companyId);
	
	public Boolean saveOrganizationBankToCompany(Integer organizationId,Integer companyId,Integer [] orgBanks);
	
	public Boolean uploadCompanyDocs(Integer orgId,Integer companyId,Integer documentTypeId,CompanyDocument companyDocument);
	
	public List<CompanyDocument> getCompanyDocsListing(Integer organizationId);
	
	public Integer saveCompanyCreditAndDiscount(Integer organizationId,Integer companyId,CompanyCreditAndDiscount companyCreditAndDiscount);
	
	public Boolean deleteCompanyDetails(Integer companyId);
	
	public Boolean deleteCompanyBankDetails(Integer companyBankId);
	
	//public Boolean deleteCompanyDepo(Integer depoId);
	
	public Boolean deleteCompanyProduct(Integer productId);
	
	public Boolean  deleteCompanyUploadedDocs(Integer companyDocId);

	public Boolean updateCompanyObj(Integer roleTypeId,Integer stateId,Integer districtId,Integer cityId,Company companyObj);

	/*public List<Company> searchCreditAndDiscount(String searchOption,
			String searchText, Integer orgId);*/
}
