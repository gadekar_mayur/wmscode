/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.OutWardReturnGoodsLRDetails;
import com.protocol.wms.model.OutWardReturnGoodsRegistration;

/**
 * @author admin
 *
 */
public interface OutwardReturnGoodsRegistrationDao {

	public OutWardReturnGoodsRegistration getOutWardReturnGoodsRegistrationDetails(Integer outWardReturnGoodsRegistrationId);
	
	public Boolean saveOutwardReturnGoodsRegistration(Integer orgId,Integer companyId,Integer stockistId,Integer transporterId,Integer employeeId,List<OutWardReturnGoodsLRDetails> lrDetailsList,OutWardReturnGoodsRegistration outWardReturnGoodsRegistration);
	
	public Boolean updateInwardReturnGoodsLrReg(List<OutWardReturnGoodsLRDetails> lrDetailsList);
	
	public List<OutWardReturnGoodsRegistration> outwardReturnGoodsRegListing(Integer organizationId);
	
	public Boolean deleteOutwardReturnGoodsRegDetails(Integer outwardRetGoodsRegId);
	
	public OutWardReturnGoodsRegistration loadOutWardReturnGoodsRegistrationObjByOutwardReturnGoodsRegId(Integer outWardReturnGoodsRegistrationId);
	
	public Boolean editOtwardReturnGoodsRegistration(Integer orgId,Integer companyId, Integer stockistId, Integer transporterId,Integer employeeId,OutWardReturnGoodsRegistration outwardReturnGoodsRegistration);

	public Map<Object,Object> searchOutReturnGoodsRegListing(String selectedValue, String searchText, String fromSpecialData,
			String toSpecialData, Integer organizationId, Integer from);
	
	public List<OutWardReturnGoodsLRDetails> getOutWardReturnGoodsLRDetailsHavingStatusOneUsingOutwardReturnGoodsRegId(Integer outWardReturnGoodsRegistrationId);
	}
