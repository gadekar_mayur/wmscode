/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.MenuAuthentication;

/**
 * @author admin
 *
 */
@Repository(value="MenuAuthenticationDaoImpl")
public class MenuAuthenticationDaoImpl implements MenuAuthenticationDao{
	private Logger log = Logger.getLogger(MenuAuthenticationDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.MenuAuthenticationDao#loadMainMenuUsingUserTypeId(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MenuAuthentication> loadMainMenuUsingUserTypeId(Integer userTypeId) {
		Session session =sessionFactory.getCurrentSession();
		List<MenuAuthentication> menuAuthenticationsList=null;
		try 
		{
			Criteria criteria = session.createCriteria(MenuAuthentication.class,"MenuAuthentication")
					.createAlias("MenuAuthentication.userType", "userType")
					.add(Restrictions.eq("userType.userTypeId", userTypeId));
			menuAuthenticationsList=criteria.list();
		} 
		catch (DataAccessException e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e);
		}
		return menuAuthenticationsList;
	}

}
