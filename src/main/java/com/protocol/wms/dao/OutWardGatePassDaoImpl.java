/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.OutWardGatePass;
import com.protocol.wms.model.OutWardGatePassLrImagePath;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OutWardGatePassDaoImpl")
public class OutWardGatePassDaoImpl implements OutWardGatePassDao{

	private Logger log = Logger.getLogger(OutWardGatePassDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardGatePassDao#saveOutWardGatePass(com.protocol.wms.model.OutWardGatePass)
	 */
	@Override
	public Integer saveOutWardGatePass(OutWardGatePass OutWardGatePassObj) {
		Session session =sessionFactory.getCurrentSession();
		Integer outWardGatePassId = null;
		try 
		{
			outWardGatePassId=(Integer) session.save(OutWardGatePassObj);
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardGatePassId;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardGatePassDao#loadOutWardGatePassObjUsignOutWardGatePassId(java.lang.Integer)
	 */
	@Override
	public OutWardGatePass loadOutWardGatePassObjUsignOutWardGatePassId(
			Integer outWardGatePassId) {
		Session session =sessionFactory.getCurrentSession();
		OutWardGatePass outWardGatePassObj=null;
		try 
		{
			outWardGatePassObj=(OutWardGatePass)session.get(OutWardGatePass.class,outWardGatePassId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return outWardGatePassObj;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardGatePassDao#updateOutWardGatePassObj(com.protocol.wms.model.OutWardGatePass)
	 */
	@Override
	public boolean updateOutWardGatePassObj(OutWardGatePass outWardGatePassObj) {
		Session session =sessionFactory.getCurrentSession();
		try{
			
			session.update(outWardGatePassObj);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	@Override
	public List<OutWardGatePassLrImagePath> updateOutwardGoodsGatePassLrImage(
			Integer outwardGatePassId,
			List<OutWardGatePassLrImagePath> lrImgList) {
		List<OutWardGatePassLrImagePath> lst = null;
		OutWardGatePass outWardGatePass = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardGatePass = (OutWardGatePass) session.load(OutWardGatePass.class, outwardGatePassId);
			for(OutWardGatePassLrImagePath tempObj : lrImgList){
				tempObj.setOutWardGatePass(outWardGatePass);
				session.save(tempObj);
			}
			lst = outWardGatePass.getOutWardGatePassLrImagePathList();
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return lst;
	}

	@Override
	public Boolean deleteOutwardGoodsGatePassLrImage(Integer lrImageId) {
		OutWardGatePassLrImagePath outWardGatePassLrImagePath = null;
		Session session =sessionFactory.getCurrentSession();
		try{
			outWardGatePassLrImagePath = (OutWardGatePassLrImagePath) session.load(OutWardGatePassLrImagePath.class, lrImageId);
			session.delete(outWardGatePassLrImagePath);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.OutWardGatePassDao#deleteLrDetails(com.protocol.wms.model.OutWardGatePass)
	 */
	@Override
	public boolean deleteLrDetails(OutWardGatePass outWardGatePassObj) {
		Session session =sessionFactory.getCurrentSession();
		try
		{
			
			session.delete(outWardGatePassObj);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e);
		}
		return false;
	}
}
