/**
 * 
 */
package com.protocol.wms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.constant.Constant;
import com.protocol.wms.model.OrganizationDocument;
import com.protocol.wms.model.OrganizationDocumentImagePath;

/**
 * @author Sudhakar
 *
 */
@Repository(value="OrganizationDocumentDaoImpl")
public class OrganizationDocumentDaoImpl implements OrganizationDocumentDao {
	
	private Logger log = Logger.getLogger(OrganizationDocumentDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
		@Autowired
		@Qualifier("sessionFactory")
		public void setSessionFactory(SessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.OrganizationDocumentDao#saveOrganizationDocument(com.protocol.wms.model.OrganizationDocument)
		 */
		@Override
		public Integer saveOrganizationDocument(OrganizationDocument organizationDocumentObj) {
			Session session =sessionFactory.getCurrentSession();
			Integer organizationDocumentId = null;
			try 
			{
				organizationDocumentId=(Integer) session.save(organizationDocumentObj);
			} 
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return organizationDocumentId;
		}

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.OrganizationDocumentDao#loadOrganizationDocumentsUsingOrganiztionId(java.lang.Integer)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public List<OrganizationDocument> loadOrganizationDocumentsListUsingOrganiztionId(
				Integer organizationId) {
			Session session =sessionFactory.getCurrentSession();
			List<OrganizationDocument> organizationDocumentsList=null;
			try 
			{
				Criteria criteria = session.createCriteria(OrganizationDocument.class,"OrganizationDocument")
					     .createAlias("OrganizationDocument.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .addOrder(Order.desc("submitDate"));;
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				organizationDocumentsList=criteria.list();
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return organizationDocumentsList;
		}

		/*@Override
		public Map getOrganizationDocumentsListUsingOrganiztionId(
				Integer organizationId, Integer from) {
			from = from-1;
			Integer PAGE_COUNT = 10;
			Session session =sessionFactory.getCurrentSession();
			List<OrganizationDocument> organizationDocumentsList=null;
			Map<String, Object> map = null;
			try 
			{
				Criteria criteria = session.createCriteria(OrganizationDocument.class,"OrganizationDocument")
					     .createAlias("OrganizationDocument.organization", "organization")
					     .setProjection(Projections.rowCount())
					     .add(Restrictions.eq("status", 1))
					     .addOrder(Order.desc("submitDate"));
			    if(organizationId!=null)
			      criteria.add(Restrictions.eq("organization.organizationId", organizationId));
				Long totalRecordCount = (Long)criteria.uniqueResult();
				System.out.println("totalRecordCount = "+totalRecordCount);
				criteria = session.createCriteria(OrganizationDocument.class,"OrganizationDocument")
					     .createAlias("OrganizationDocument.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					     .addOrder(Order.desc("submitDate"));
			    if(organizationId!=null)
			      criteria.add(Restrictions.eq("organization.organizationId", organizationId));
			    criteria.setFirstResult(from*PAGE_COUNT);
			    criteria.setMaxResults(PAGE_COUNT);
				
				organizationDocumentsList = criteria.list();
			    //Integer totalRecordCount = criteria.list().size();
				
				Integer tatalPage = (int) (totalRecordCount/PAGE_COUNT);
				if((totalRecordCount%PAGE_COUNT)!=0){
					tatalPage=tatalPage+1;
				}
				map = new HashMap<String, Object>();
				map.put("orgDocList", organizationDocumentsList);
				map.put("totalPages", tatalPage);
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return map;
		}*/

		/* (non-Javadoc)
		 * @see com.protocol.wms.dao.OrganizationDocumentDao#searchOrgaiztionDocumentDetails(java.lang.String, java.lang.String, java.lang.Integer)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public Map<String, Object> searchOrgaiztionDocumentDetails(
				String searchOption, String searchText, Integer organizationId, Integer from) {
			Session session =sessionFactory.getCurrentSession();
			List<OrganizationDocument> organizationDocumentsList=null;
			Map<String, Object>map = null;
			try 
			{
				Criteria criteria = session.createCriteria(OrganizationDocument.class,"OrganizationDocument")
					     .createAlias("OrganizationDocument.organization", "organization")
					     .add(Restrictions.eq("status", 1))
					      .add(Restrictions.eq("organization.organizationStatus", "1"))
					     .setProjection(Projections.rowCount());
					   if(organizationId!=null){
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   }
					   if("Organization".equals(searchOption))
						     criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					   else if("DocumentType".equals(searchOption)){
						   criteria.createAlias("OrganizationDocument.documentListType", "docType");
						   criteria.add(Restrictions.like("docType.documentName", searchText,MatchMode.START));
					   }
				Long totalRecordCount = (Long)criteria.uniqueResult();
				
				criteria = session.createCriteria(OrganizationDocument.class,"OrganizationDocument")
					     .createAlias("OrganizationDocument.organization", "organization")
					     .createAlias("OrganizationDocument.documentListType", "docType")
					     .add(Restrictions.eq("status", 1))
					      .add(Restrictions.eq("organization.organizationStatus", "1"))
					     .addOrder(Order.desc("submitDate"));
					   if(organizationId!=null)
					     criteria.add(Restrictions.eq("organization.organizationId", organizationId));
					   
					   if("Organization".equals(searchOption))
						     criteria.add(Restrictions.like("organization.organizationName", searchText,MatchMode.START));
					   else if("DocumentType".equals(searchOption))
						     criteria.add(Restrictions.like("docType.documentName", searchText,MatchMode.START));
				from = from - 1 ;
			    criteria.setFirstResult(from*Constant.PAGE_COUNT);
			    criteria.setMaxResults(Constant.PAGE_COUNT);
				organizationDocumentsList=criteria.list();
				
				Integer tatalPage = 0;
				if(totalRecordCount>0){
					tatalPage = (int) (totalRecordCount/Constant.PAGE_COUNT);
					if((totalRecordCount%Constant.PAGE_COUNT)!=0){
						tatalPage=tatalPage+1;
					}
				}
				map = new HashMap<String, Object>();
				map.put("orgDocList", organizationDocumentsList);
				map.put("totalPages", tatalPage);
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return map;
		}

		@Override
		public List<OrganizationDocumentImagePath> updateOrganizationUploadDocumentImage(Integer organizationDocumentId,
				List<OrganizationDocumentImagePath> documentList) {
			List<OrganizationDocumentImagePath> lst = null;
			OrganizationDocument organizationDocument = null;
			Session session =sessionFactory.getCurrentSession();
			try{
				organizationDocument = (OrganizationDocument) session.load(OrganizationDocument.class, organizationDocumentId);
				for(OrganizationDocumentImagePath tempObj : documentList){
					tempObj.setOrganizationDocument(organizationDocument);
					session.save(tempObj);
				}
				lst = organizationDocument.getOrganizationDocumentImagePathList();
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return lst;
		}

		@Override
		public Boolean deleteOrganizationDocumentFileImage(
				Integer orgDocumentPathId) {
			OrganizationDocumentImagePath organizationDocumentImagePath = null;
			Session session =sessionFactory.getCurrentSession();
			try{
				organizationDocumentImagePath = (OrganizationDocumentImagePath) session.load(OrganizationDocumentImagePath.class, orgDocumentPathId);
				session.delete(organizationDocumentImagePath);
				return true;
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return false;
		}

		@Override
		public Boolean updateOrganizationDocumentObj(OrganizationDocument organizationDocument) {
			Session session =sessionFactory.getCurrentSession();
			try 
			{
				session.update(organizationDocument);
				return true;
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return false;
		}

		@Override
		public OrganizationDocument loadOrganizationDocumentObjById(
				Integer organizationDocumentId) {
			Session session =sessionFactory.getCurrentSession();
			OrganizationDocument organizationDocument = null;
			try 
			{
				organizationDocument = (OrganizationDocument)session.load(OrganizationDocument.class,organizationDocumentId);
			}
			catch(DataAccessException e)
			{
				e.printStackTrace();
				log.error(e);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				log.error(e);
			}
			return organizationDocument;
		}

}
