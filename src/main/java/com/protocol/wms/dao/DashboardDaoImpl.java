package com.protocol.wms.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.protocol.wms.model.AdvancedChequeInformation;
import com.protocol.wms.model.InwardCourierRegistration;
import com.protocol.wms.model.InwardGoodsRegistration;
import com.protocol.wms.model.InwardReturnGoodsRegistration;
import com.protocol.wms.model.InwardReturnGoodsRegistrationLRDetails;
import com.protocol.wms.model.OutWardCourierRegistration;
import com.protocol.wms.model.OutWardDispatchEnteryRegistration;
import com.protocol.wms.model.OutWardOrderEntryRegistration;
import com.protocol.wms.model.OutWardOrderInvoiceEnteryRegistration;
import com.protocol.wms.model.StockistChequeDeposit;
import com.protocol.wms.model.StockistChequeReturn;

@Repository(value="DashboardDaoImpl")
public class DashboardDaoImpl implements DashboardDao {
	
	private Logger log = Logger.getLogger(DashboardDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;
	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public Map<String, Object> loadDashboardTable(Integer[] organizations,
			Integer[] companies, Integer[] stockists, Integer year,
			Integer organizationId) {
		Map<String, Object> map = new HashMap<String, Object>();
		;
		Session session = sessionFactory.getCurrentSession();
		try {
			// = = = common = = =//
			DateFormat dtf = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date d1 = null;
			Integer yr = year + 1;
			d1 = dtf.parse("04/01/" + year);
			java.sql.Date startDate = new java.sql.Date(d1.getTime());
			d1 = dtf.parse("03/31/" + yr);
			java.sql.Date endtDate = new java.sql.Date(d1.getTime());

			int jan = 0, feb = 0, mar = 0, april = 0, may = 0, june = 0, july = 0, august = 0, sept = 0, oct = 0, nov = 0, dec = 0;
			double djan = 0, dfeb = 0, dmar = 0, dapril = 0, dmay = 0, djune = 0, djuly = 0, daugust = 0, dsept = 0, doct = 0, dnov = 0, ddec = 0;
			// = = = common End= = =//

			// = = = = = cases outward = = = = = //
			Criteria criteria = session
					.createCriteria(OutWardDispatchEnteryRegistration.class,
							"OutWardDispatchEnteryRegistration")
					.createAlias(
							"OutWardDispatchEnteryRegistration.organization",
							"organization")
					.createAlias("OutWardDispatchEnteryRegistration.stockist",
							"stockist")
					.createAlias("OutWardDispatchEnteryRegistration.company",
							"company")
					// .createAlias("OutWardDispatchEnteryRegistration.outWardTrip",
					// "outWardTrip")
					.add(Restrictions.eq("status", 1));

			if (organizationId != null)
				criteria.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria.add(Restrictions.in("company.companyId", companies));
			}

			if (stockists.length != 0 && stockists != null) {
				criteria.add(Restrictions.in("stockist.stockistId", stockists));
			}

			criteria.add(Restrictions.between(
					"OutWardDispatchEnteryRegistration.submitDate", startDate,
					endtDate));

			List<OutWardDispatchEnteryRegistration> oderList = criteria.list();

			for (OutWardDispatchEnteryRegistration obj : oderList) {

				String month = obj.getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan += obj.getNoOfCases();
					break;

				case "02":
					feb += obj.getNoOfCases();
					break;

				case "03":
					mar += obj.getNoOfCases();
					break;

				case "04":
					april += obj.getNoOfCases();
					break;

				case "05":
					may += obj.getNoOfCases();
					break;

				case "06":
					june += obj.getNoOfCases();
					break;

				case "07":
					july += obj.getNoOfCases();
					break;

				case "08":
					august += obj.getNoOfCases();
					break;

				case "09":
					sept += obj.getNoOfCases();
					break;

				case "10":
					oct += obj.getNoOfCases();
					break;

				case "11":
					nov += obj.getNoOfCases();
					break;

				case "12":
					dec += obj.getNoOfCases();
					break;

				}
			}

			map.put("outward_cases_1", jan);
			map.put("outward_cases_2", feb);
			map.put("outward_cases_3", mar);
			map.put("outward_cases_4", april);
			map.put("outward_cases_5", may);
			map.put("outward_cases_6", june);
			map.put("outward_cases_7", july);
			map.put("outward_cases_8", august);
			map.put("outward_cases_9", sept);
			map.put("outward_cases_10", oct);
			map.put("outward_cases_11", nov);
			map.put("outward_cases_12", dec);
			map.put("outward_cases_13", jan + feb + mar + april + may + june
					+ july + august + sept + oct + nov + dec);

			// = = = = = cases outward End = = = = = //

			// = = = = = cases Inward = = = = = //
			Criteria criteria1 = session
					.createCriteria(InwardGoodsRegistration.class,
							"InwardGoodsRegistration")
					.createAlias("InwardGoodsRegistration.organization",
							"organization")
					.createAlias("InwardGoodsRegistration.company", "company")
					// .createAlias("OutWardDispatchEnteryRegistration.outWardTrip",
					// "outWardTrip")
					.add(Restrictions.eq("status", 1));

			if (organizationId != null)
				criteria1.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria1.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria1.add(Restrictions.in("company.companyId", companies));
			}

			criteria1.add(Restrictions.between(
					"InwardGoodsRegistration.submitDate", startDate, endtDate));
			List<InwardGoodsRegistration> igrList = criteria1.list();
			jan = 0;
			feb = 0;
			mar = 0;
			april = 0;
			may = 0;
			june = 0;
			july = 0;
			august = 0;
			sept = 0;
			oct = 0;
			nov = 0;
			dec = 0;
			for (InwardGoodsRegistration obj : igrList) {

				String month = obj.getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan += obj.getNoOfCases();
					break;

				case "02":
					feb += obj.getNoOfCases();
					break;

				case "03":
					mar += obj.getNoOfCases();
					break;

				case "04":
					april += obj.getNoOfCases();
					break;

				case "05":
					may += obj.getNoOfCases();
					break;

				case "06":
					june += obj.getNoOfCases();
					break;

				case "07":
					july += obj.getNoOfCases();
					break;

				case "08":
					august += obj.getNoOfCases();
					break;

				case "09":
					sept += obj.getNoOfCases();
					break;

				case "10":
					oct += obj.getNoOfCases();
					break;

				case "11":
					nov += obj.getNoOfCases();
					break;

				case "12":
					dec += obj.getNoOfCases();
					break;

				}
			}

			map.put("inward_cases_1", jan);
			map.put("inward_cases_2", feb);
			map.put("inward_cases_3", mar);
			map.put("inward_cases_4", april);
			map.put("inward_cases_5", may);
			map.put("inward_cases_6", june);
			map.put("inward_cases_7", july);
			map.put("inward_cases_8", august);
			map.put("inward_cases_9", sept);
			map.put("inward_cases_10", oct);
			map.put("inward_cases_11", nov);
			map.put("inward_cases_12", dec);
			map.put("inward_cases_13", jan + feb + mar + april + may + june
					+ july + august + sept + oct + nov + dec);

			// = = = = = cases inward End = = = = = //

			// = = = = = courier Inward = = = = = //
			Criteria criteria2 = session
					.createCriteria(InwardCourierRegistration.class,
							"InwardCourierRegistration")
					.createAlias("InwardCourierRegistration.organization",
							"organization")
					.createAlias("InwardCourierRegistration.company", "company")
					.createAlias("InwardCourierRegistration.stockist",
							"stockist").add(Restrictions.eq("status", 1));

			if (organizationId != null)
				criteria2.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria2.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria2.add(Restrictions.in("company.companyId", companies));
			}

			if (stockists.length != 0 && stockists != null) {
				criteria2
						.add(Restrictions.in("stockist.stockistId", stockists));
			}

			criteria2.add(Restrictions
					.between("InwardCourierRegistration.submitDate", startDate,
							endtDate));
			List<InwardCourierRegistration> icrList = criteria2.list();
			jan = 0;
			feb = 0;
			mar = 0;
			april = 0;
			may = 0;
			june = 0;
			july = 0;
			august = 0;
			sept = 0;
			oct = 0;
			nov = 0;
			dec = 0;
			for (InwardCourierRegistration obj : icrList) {

				String month = obj.getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan++;
					break;

				case "02":
					feb++;
					break;

				case "03":
					mar++;
					break;

				case "04":
					april++;
					break;

				case "05":
					may++;
					break;

				case "06":
					june++;
					break;

				case "07":
					july++;
					break;

				case "08":
					august++;
					break;

				case "09":
					sept++;
					break;

				case "10":
					oct++;
					break;

				case "11":
					nov++;
					break;

				case "12":
					dec++;
					break;

				}
			}

			map.put("inward_courier_1", jan);
			map.put("inward_courier_2", feb);
			map.put("inward_courier_3", mar);
			map.put("inward_courier_4", april);
			map.put("inward_courier_5", may);
			map.put("inward_courier_6", june);
			map.put("inward_courier_7", july);
			map.put("inward_courier_8", august);
			map.put("inward_courier_9", sept);
			map.put("inward_courier_10", oct);
			map.put("inward_courier_11", nov);
			map.put("inward_courier_12", dec);
			map.put("inward_courier_13", jan + feb + mar + april + may + june
					+ july + august + sept + oct + nov + dec);

			// = = = = = courier inward End = = = = = //

			// = = = = = courier outward = = = = = //
			Criteria criteria3 = session
					.createCriteria(OutWardCourierRegistration.class,"OutWardCourierRegistration")
					.createAlias("OutWardCourierRegistration.organization","organization")
					.createAlias("OutWardCourierRegistration.company","company")
					.add(Restrictions.eq("OutWardCourierRegistration.status", 1));
					
			if (organizationId != null)
				criteria3.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria3.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria3.add(Restrictions.in("company.companyId", companies));
			}

			if (stockists.length != 0 && stockists != null) {
				criteria3.createAlias("OutWardCourierRegistration.stockist","stockist")
						.add(Restrictions.in("stockist.stockistId", stockists));
			}

			criteria3.add(Restrictions.between(
					"OutWardCourierRegistration.submitDate", startDate,
					endtDate));
			List<OutWardCourierRegistration> ocrList = criteria3.list();
			jan = 0;
			feb = 0;
			mar = 0;
			april = 0;
			may = 0;
			june = 0;
			july = 0;
			august = 0;
			sept = 0;
			oct = 0;
			nov = 0;
			dec = 0;
			for (OutWardCourierRegistration obj : ocrList) {

				String month = obj.getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan++;
					break;

				case "02":
					feb++;
					break;

				case "03":
					mar++;
					break;

				case "04":
					april++;
					break;

				case "05":
					may++;
					break;

				case "06":
					june++;
					break;

				case "07":
					july++;
					break;

				case "08":
					august++;
					break;

				case "09":
					sept++;
					break;

				case "10":
					oct++;
					break;

				case "11":
					nov++;
					break;

				case "12":
					dec++;
					break;

				}
			}

			map.put("outward_courier_1", jan);
			map.put("outward_courier_2", feb);
			map.put("outward_courier_3", mar);
			map.put("outward_courier_4", april);
			map.put("outward_courier_5", may);
			map.put("outward_courier_6", june);
			map.put("outward_courier_7", july);
			map.put("outward_courier_8", august);
			map.put("outward_courier_9", sept);
			map.put("outward_courier_10", oct);
			map.put("outward_courier_11", nov);
			map.put("outward_courier_12", dec);
			map.put("outward_courier_13", jan + feb + mar + april + may + june
					+ july + august + sept + oct + nov + dec);

			// = = = = = courier outward End = = = = = //

			// = = = = = order received = = = = = //
			Criteria criteria4 = session
					.createCriteria(OutWardOrderEntryRegistration.class,
							"OutWardOrderEntryRegistration")
					.createAlias("OutWardOrderEntryRegistration.organization",
							"organization")
					.createAlias("OutWardOrderEntryRegistration.company",
							"company")
					.createAlias("OutWardOrderEntryRegistration.stockist",
							"stockist").add(Restrictions.eq("status", 1));

			if (organizationId != null)
				criteria4.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria4.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria4.add(Restrictions.in("company.companyId", companies));
			}

			if (stockists.length != 0 && stockists != null) {
				criteria4
						.add(Restrictions.in("stockist.stockistId", stockists));
			}

			criteria4.add(Restrictions.between(
					"OutWardOrderEntryRegistration.submitDate", startDate,
					endtDate));
			List<OutWardOrderEntryRegistration> ooerList = criteria4.list();
			jan = 0;
			feb = 0;
			mar = 0;
			april = 0;
			may = 0;
			june = 0;
			july = 0;
			august = 0;
			sept = 0;
			oct = 0;
			nov = 0;
			dec = 0;
			for (OutWardOrderEntryRegistration obj : ooerList) {

				String month = obj.getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan++;
					break;

				case "02":
					feb++;
					break;

				case "03":
					mar++;
					break;

				case "04":
					april++;
					break;

				case "05":
					may++;
					break;

				case "06":
					june++;
					break;

				case "07":
					july++;
					break;

				case "08":
					august++;
					break;

				case "09":
					sept++;
					break;

				case "10":
					oct++;
					break;

				case "11":
					nov++;
					break;

				case "12":
					dec++;
					break;

				}
			}

			map.put("order_received_1", jan);
			map.put("order_received_2", feb);
			map.put("order_received_3", mar);
			map.put("order_received_4", april);
			map.put("order_received_5", may);
			map.put("order_received_6", june);
			map.put("order_received_7", july);
			map.put("order_received_8", august);
			map.put("order_received_9", sept);
			map.put("order_received_10", oct);
			map.put("order_received_11", nov);
			map.put("order_received_12", dec);
			map.put("order_received_13", jan + feb + mar + april + may + june
					+ july + august + sept + oct + nov + dec);

			// = = = = = order received End = = = = = //

			// = = = = = Invoice generated & sales Amount= = = = = //
			Criteria criteria5 = session
					.createCriteria(
							OutWardOrderInvoiceEnteryRegistration.class,
							"OutWardOrderInvoiceEnteryRegistration")
					.createAlias(
							"OutWardOrderInvoiceEnteryRegistration.organization",
							"organization")
					.createAlias(
							"OutWardOrderInvoiceEnteryRegistration.outWardOrderEnteryRegistration",
							"OutWardOrderEntryRegistration")
					.createAlias("OutWardOrderEntryRegistration.company",
							"company")
					.createAlias("OutWardOrderEntryRegistration.stockist",
							"stockist").add(Restrictions.eq("status", 1));

			if (organizationId != null)
				criteria5.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria5.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria5.add(Restrictions.in("company.companyId", companies));
			}

			if (stockists.length != 0 && stockists != null) {
				criteria5
						.add(Restrictions.in("stockist.stockistId", stockists));
			}

			criteria5.add(Restrictions.between(
					"OutWardOrderEntryRegistration.submitDate", startDate,
					endtDate));
			List<OutWardOrderInvoiceEnteryRegistration> ooierList = criteria5.list();
			jan = 0;
			feb = 0;
			mar = 0;
			april = 0;
			may = 0;
			june = 0;
			july = 0;
			august = 0;
			sept = 0;
			oct = 0;
			nov = 0;
			dec = 0;
			
			
			for (OutWardOrderInvoiceEnteryRegistration obj : ooierList) {

				String month = obj.getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan++;
					djan+=obj.getNetAmount();
					break;

				case "02":
					feb++;
					dfeb+=obj.getNetAmount();
					break;

				case "03":
					mar++;
					dmar+=obj.getNetAmount();
					break;

				case "04":
					april++;
					dapril+=obj.getNetAmount();
					break;

				case "05":
					may++;
					dmay+=obj.getNetAmount();
					break;

				case "06":
					june++;
					djune+=obj.getNetAmount();
					break;

				case "07":
					july++;
					djuly+=obj.getNetAmount();
					break;

				case "08":
					august++;
					daugust+=obj.getNetAmount();
					break;

				case "09":
					sept++;
					dsept+=obj.getNetAmount();
					break;

				case "10":
					oct++;
					doct+=obj.getNetAmount();
					break;

				case "11":
					nov++;
					dnov+=obj.getNetAmount();
					break;

				case "12":
					dec++;
					ddec+=obj.getNetAmount();
					break;

				}
			}

			map.put("invoice_generated_1", jan);
			map.put("invoice_generated_2", feb);
			map.put("invoice_generated_3", mar);
			map.put("invoice_generated_4", april);
			map.put("invoice_generated_5", may);
			map.put("invoice_generated_6", june);
			map.put("invoice_generated_7", july);
			map.put("invoice_generated_8", august);
			map.put("invoice_generated_9", sept);
			map.put("invoice_generated_10", oct);
			map.put("invoice_generated_11", nov);
			map.put("invoice_generated_12", dec);
			map.put("invoice_generated_13", jan + feb + mar + april + may
					+ june + july + august + sept + oct + nov + dec);
			
			map.put("sales_amount_1", Math.round(djan));
			map.put("sales_amount_2", Math.round(dfeb));
			map.put("sales_amount_3", Math.round(dmar));
			map.put("sales_amount_4", Math.round(dapril));
			map.put("sales_amount_5", Math.round(dmay));
			map.put("sales_amount_6", Math.round(djune));
			map.put("sales_amount_7", Math.round(djuly));
			map.put("sales_amount_8", Math.round(daugust));
			map.put("sales_amount_9", Math.round(dsept));
			map.put("sales_amount_10", Math.round(doct));
			map.put("sales_amount_11", Math.round(dnov));
			map.put("sales_amount_12", Math.round(ddec));
			map.put("sales_amount_13",
					Math.round(djan + dfeb + dmar + dapril + dmay + djune
							+ djuly + daugust + dsept + doct + dnov + ddec));

			// = = = = = Invoice generated sales Amount End = = = = = //

			// = = = = = cheque received = = = = = //
			Criteria criteria6 = session
					.createCriteria(AdvancedChequeInformation.class,
							"AdvancedChequeInformation")
					.createAlias("AdvancedChequeInformation.organization",
							"organization")
					.createAlias("AdvancedChequeInformation.company", "company")
					.createAlias("AdvancedChequeInformation.stockist",
							"stockist").add(Restrictions.eq("status", 1));

			if (organizationId != null)
				criteria6.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria6.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria6.add(Restrictions.in("company.companyId", companies));
			}

			if (stockists.length != 0 && stockists != null) {
				criteria6
						.add(Restrictions.in("stockist.stockistId", stockists));
			}

			criteria6.add(Restrictions
					.between("AdvancedChequeInformation.submitDate", startDate,
							endtDate));
			List<AdvancedChequeInformation> aciList = criteria6.list();
			jan = 0;
			feb = 0;
			mar = 0;
			april = 0;
			may = 0;
			june = 0;
			july = 0;
			august = 0;
			sept = 0;
			oct = 0;
			nov = 0;
			dec = 0;
			for (AdvancedChequeInformation obj : aciList) {

				String month = obj.getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan += obj.getNoOfCheque();
					break;

				case "02":
					feb += obj.getNoOfCheque();
					break;

				case "03":
					mar += obj.getNoOfCheque();
					break;

				case "04":
					april += obj.getNoOfCheque();
					break;

				case "05":
					may += obj.getNoOfCheque();
					break;

				case "06":
					june += obj.getNoOfCheque();
					break;

				case "07":
					july += obj.getNoOfCheque();
					break;

				case "08":
					august += obj.getNoOfCheque();
					break;

				case "09":
					sept += obj.getNoOfCheque();
					break;

				case "10":
					oct += obj.getNoOfCheque();
					break;

				case "11":
					nov += obj.getNoOfCheque();
					break;

				case "12":
					dec += obj.getNoOfCheque();
					break;

				}
			}

			map.put("cheque_received_1", jan);
			map.put("cheque_received_2", feb);
			map.put("cheque_received_3", mar);
			map.put("cheque_received_4", april);
			map.put("cheque_received_5", may);
			map.put("cheque_received_6", june);
			map.put("cheque_received_7", july);
			map.put("cheque_received_8", august);
			map.put("cheque_received_9", sept);
			map.put("cheque_received_10", oct);
			map.put("cheque_received_11", nov);
			map.put("cheque_received_12", dec);
			map.put("cheque_received_13", jan + feb + mar + april + may + june
					+ july + august + sept + oct + nov + dec);

			// = = = = = cheque received End = = = = = //

			// = = = = = cheque deposited = = = = = //
			Criteria criteria7 = session
					.createCriteria(StockistChequeDeposit.class,
							"StockistChequeDeposit")
					.createAlias("StockistChequeDeposit.organization",
							"organization")
					.createAlias("StockistChequeDeposit.company", "company")
					.createAlias("StockistChequeDeposit.stockist", "stockist")
					.add(Restrictions.eq("status", 1));

			if (organizationId != null)
				criteria7.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria7.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria7.add(Restrictions.in("company.companyId", companies));
			}

			if (stockists.length != 0 && stockists != null) {
				criteria7
						.add(Restrictions.in("stockist.stockistId", stockists));
			}

			criteria7.add(Restrictions.between(
					"StockistChequeDeposit.submitDate", startDate, endtDate));
			List<StockistChequeDeposit> scdList = criteria7.list();
			jan = 0;
			feb = 0;
			mar = 0;
			april = 0;
			may = 0;
			june = 0;
			july = 0;
			august = 0;
			sept = 0;
			oct = 0;
			nov = 0;
			dec = 0;
			for (StockistChequeDeposit obj : scdList) {

				String month = obj.getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan++;
					break;

				case "02":
					feb++;
					break;

				case "03":
					mar++;
					break;

				case "04":
					april++;
					break;

				case "05":
					may++;
					break;

				case "06":
					june++;
					break;

				case "07":
					july++;
					break;

				case "08":
					august++;
					break;

				case "09":
					sept++;
					break;

				case "10":
					oct++;
					break;

				case "11":
					nov++;
					break;

				case "12":
					dec++;
					break;

				}
			}

			map.put("cheque_deposited_1", jan);
			map.put("cheque_deposited_2", feb);
			map.put("cheque_deposited_3", mar);
			map.put("cheque_deposited_4", april);
			map.put("cheque_deposited_5", may);
			map.put("cheque_deposited_6", june);
			map.put("cheque_deposited_7", july);
			map.put("cheque_deposited_8", august);
			map.put("cheque_deposited_9", sept);
			map.put("cheque_deposited_10", oct);
			map.put("cheque_deposited_11", nov);
			map.put("cheque_deposited_12", dec);
			map.put("cheque_deposited_13", jan + feb + mar + april + may + june
					+ july + august + sept + oct + nov + dec);

			// = = = = = cheque deposited End = = = = = //

			// = = = = = cheque return = = = = = //
			Criteria criteria8 = session
					.createCriteria(StockistChequeReturn.class,
							"StockistChequeReturn")
					.createAlias("StockistChequeReturn.organization",
							"organization")
					.createAlias("StockistChequeReturn.stockistChequeDeposit",
							"StockistChequeDeposit")
					.createAlias("StockistChequeDeposit.company", "company")
					.createAlias("StockistChequeDeposit.stockist", "stockist");

			if (organizationId != null)
				criteria8.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria8.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria8.add(Restrictions.in("company.companyId", companies));
			}

			if (stockists.length != 0 && stockists != null) {
				criteria8
						.add(Restrictions.in("stockist.stockistId", stockists));
			}

			criteria8.add(Restrictions.between(
					"StockistChequeReturn.submitDate", startDate, endtDate));
			List<StockistChequeReturn> scrList = criteria8.list();
			jan = 0;
			feb = 0;
			mar = 0;
			april = 0;
			may = 0;
			june = 0;
			july = 0;
			august = 0;
			sept = 0;
			oct = 0;
			nov = 0;
			dec = 0;
			for (StockistChequeReturn obj : scrList) {

				String month = obj.getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan++;
					break;

				case "02":
					feb++;
					break;

				case "03":
					mar++;
					break;

				case "04":
					april++;
					break;

				case "05":
					may++;
					break;

				case "06":
					june++;
					break;

				case "07":
					july++;
					break;

				case "08":
					august++;
					break;

				case "09":
					sept++;
					break;

				case "10":
					oct++;
					break;

				case "11":
					nov++;
					break;

				case "12":
					dec++;
					break;

				}

			}

			map.put("cheque_returned_1", jan);
			map.put("cheque_returned_2", feb);
			map.put("cheque_returned_3", mar);
			map.put("cheque_returned_4", april);
			map.put("cheque_returned_5", may);
			map.put("cheque_returned_6", june);
			map.put("cheque_returned_7", july);
			map.put("cheque_returned_8", august);
			map.put("cheque_returned_9", sept);
			map.put("cheque_returned_10", oct);
			map.put("cheque_returned_11", nov);
			map.put("cheque_returned_12", dec);
			map.put("cheque_returned_13", jan + feb + mar + april + may + june
					+ july + august + sept + oct + nov + dec);

			// = = = = = cheque return End = = = = = //
	
			// = = = = = No. of return claim received & claim amount = = = = = //
			Criteria criteria9 = session
					.createCriteria(InwardReturnGoodsRegistrationLRDetails.class,
							"InwardReturnGoodsRegistrationLRDetails")
							.createAlias("InwardReturnGoodsRegistrationLRDetails.inwardReturnGoodsRegistration",
							"inwardReturnGoodsRegistration")
					.createAlias("inwardReturnGoodsRegistration.organization",
							"organization")
					.createAlias("inwardReturnGoodsRegistration.company", "company")
					.createAlias("inwardReturnGoodsRegistration.stockist", "stockist");
					criteria9.add(Restrictions.eq("InwardReturnGoodsRegistrationLRDetails.status",1));
					criteria9.add(Restrictions.eq("inwardReturnGoodsRegistration.status",1));
			if (organizationId != null)
				criteria9.add(Restrictions.eq("organization.organizationId",
						organizationId));

			if (organizations.length != 0 && organizations != null) {
				criteria9.add(Restrictions.in("organization.organizationId",
						organizations));
			}

			if (companies.length != 0 && companies != null) {
				criteria9.add(Restrictions.in("company.companyId", companies));
			}

			if (stockists.length != 0 && stockists != null) {
				criteria9
						.add(Restrictions.in("stockist.stockistId", stockists));
			}

			criteria9.add(Restrictions.between(
					"inwardReturnGoodsRegistration.submitDate", startDate, endtDate));
			List<InwardReturnGoodsRegistrationLRDetails> irgrdList = criteria9.list();
			jan = 0;
			feb = 0;
			mar = 0;
			april = 0;
			may = 0;
			june = 0;
			july = 0;
			august = 0;
			sept = 0;
			oct = 0;
			nov = 0;
			dec = 0;
			djan = 0; dfeb = 0; dmar = 0; dapril = 0; dmay = 0; djune = 0; djuly = 0; daugust = 0; dsept = 0; doct = 0; dnov = 0; ddec = 0;
			for (InwardReturnGoodsRegistrationLRDetails obj : irgrdList) {

				String month = obj.getInwardReturnGoodsRegistration().getSubmitDate().toString().substring(5, 7);

				switch (month) {

				case "01":
					jan++;
					djan+=obj.getClaimAmount();
					break;

				case "02":
					feb++;
					dfeb+=obj.getClaimAmount();
					break;

				case "03":
					mar++;
					dmar+=obj.getClaimAmount();
					break;

				case "04":
					april++;
					dapril+=obj.getClaimAmount();
					break;

				case "05":
					may++;
					dmay+=obj.getClaimAmount();
					break;

				case "06":
					june++;
					djune+=obj.getClaimAmount();
					break;

				case "07":
					july++;
					djuly+=obj.getClaimAmount();
					break;

				case "08":
					august++;
					daugust+=obj.getClaimAmount();
					break;

				case "09":
					sept++;
					dsept+=obj.getClaimAmount();
					break;

				case "10":
					oct++;
					doct+=obj.getClaimAmount();
					break;

				case "11":
					nov++;
					dnov+=obj.getClaimAmount();
					break;

				case "12":
					dec++;
					ddec+=obj.getClaimAmount();
					break;

				}

			}

			map.put("return_claim_1", jan);
			map.put("return_claim_2", feb);
			map.put("return_claim_3", mar);
			map.put("return_claim_4", april);
			map.put("return_claim_5", may);
			map.put("return_claim_6", june);
			map.put("return_claim_7", july);
			map.put("return_claim_8", august);
			map.put("return_claim_9", sept);
			map.put("return_claim_10", oct);
			map.put("return_claim_11", nov);
			map.put("return_claim_12", dec);
			map.put("return_claim_13", jan + feb + mar + april + may + june
					+ july + august + sept + oct + nov + dec);
			
			map.put("claim_amount_1", Math.round(djan));
			map.put("claim_amount_2", Math.round(dfeb));
			map.put("claim_amount_3", Math.round(dmar));
			map.put("claim_amount_4", Math.round(dapril));
			map.put("claim_amount_5", Math.round(dmay));
			map.put("claim_amount_6", Math.round(djune));
			map.put("claim_amount_7", Math.round(djuly));
			map.put("claim_amount_8", Math.round(daugust));
			map.put("claim_amount_9", Math.round(dsept));
			map.put("claim_amount_10", Math.round(doct));
			map.put("claim_amount_11", Math.round(dnov));
			map.put("claim_amount_12", Math.round(ddec));
			map.put("claim_amount_13",
					Math.round(djan + dfeb + dmar + dapril + dmay + djune
							+ djuly + daugust + dsept + doct + dnov + ddec));

			// = = = = = No. of return claim received & claim amount End = = = = = //
			
			// = = = = = No. of credit notes & credit notes amount = = = = = //
						Criteria criteria10 = session
								.createCriteria(InwardReturnGoodsRegistration.class, "inwardReturnGoodsRegistration")
										//.createAlias("inwardReturnGoodsRegistration.creditNote", "creditNote")
								.createAlias("inwardReturnGoodsRegistration.organization", "organization")
								.createAlias("inwardReturnGoodsRegistration.company", "company")
								.createAlias("inwardReturnGoodsRegistration.stockist", "stockist")
								.add(Restrictions.eq("inwardReturnGoodsRegistration.status",1));
						criteria10.add(Restrictions.eq("inwardReturnGoodsRegistration.checkingDoneStatus",1));
						criteria10.add(Restrictions.eq("inwardReturnGoodsRegistration.creditNoteStatus",1));

						if (organizationId != null)
							criteria10.add(Restrictions.eq("organization.organizationId",
									organizationId));

						if (organizations.length != 0 && organizations != null) {
							criteria10.add(Restrictions.in("organization.organizationId",
									organizations));
						}

						if (companies.length != 0 && companies != null) {
							criteria10.add(Restrictions.in("company.companyId", companies));
						}

						if (stockists.length != 0 && stockists != null) {
							criteria10
									.add(Restrictions.in("stockist.stockistId", stockists));
						}

						criteria10.add(Restrictions.between(
								"inwardReturnGoodsRegistration.submitDate", startDate, endtDate));
						List<InwardReturnGoodsRegistration> irgrList = criteria10.list();
						jan = 0;
						feb = 0;
						mar = 0;
						april = 0;
						may = 0;
						june = 0;
						july = 0;
						august = 0;
						sept = 0;
						oct = 0;
						nov = 0;
						dec = 0;
						djan = 0; dfeb = 0; dmar = 0; dapril = 0; dmay = 0; djune = 0; djuly = 0; daugust = 0; dsept = 0; doct = 0; dnov = 0; ddec = 0;
						for (InwardReturnGoodsRegistration obj : irgrList) {

							String month = obj.getSubmitDate().toString().substring(5, 7);

							switch (month) {

							case "01":
								jan++;
								djan+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "02":
								feb++;
								dfeb+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "03":
								mar++;
								dmar+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "04":
								april++;
								if(obj.getCreditNote().getCreditNoteAmount()==null)
								{}
								else
								{
								dapril+=obj.getCreditNote().getCreditNoteAmount();
								}
								break;

							case "05":
								may++;
								dmay+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "06":
								june++;
								djune+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "07":
								july++;
								djuly+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "08":
								august++;
								daugust+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "09":
								sept++;
								dsept+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "10":
								oct++;
								doct+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "11":
								nov++;
								dnov+=obj.getCreditNote().getCreditNoteAmount();
								break;

							case "12":
								dec++;
								ddec+=obj.getCreditNote().getCreditNoteAmount();
								break;

							}

						}

						map.put("credit_notes_1", jan);
						map.put("credit_notes_2", feb);
						map.put("credit_notes_3", mar);
						map.put("credit_notes_4", april);
						map.put("credit_notes_5", may);
						map.put("credit_notes_6", june);
						map.put("credit_notes_7", july);
						map.put("credit_notes_8", august);
						map.put("credit_notes_9", sept);
						map.put("credit_notes_10", oct);
						map.put("credit_notes_11", nov);
						map.put("credit_notes_12", dec);
						map.put("credit_notes_13", jan + feb + mar + april + may + june
								+ july + august + sept + oct + nov + dec);
						
						map.put("credit_amount_1", Math.round(djan));
						map.put("credit_amount_2", Math.round(dfeb));
						map.put("credit_amount_3", Math.round(dmar));
						map.put("credit_amount_4", Math.round(dapril));
						map.put("credit_amount_5", Math.round(dmay));
						map.put("credit_amount_6", Math.round(djune));
						map.put("credit_amount_7", Math.round(djuly));
						map.put("credit_amount_8", Math.round(daugust));
						map.put("credit_amount_9", Math.round(dsept));
						map.put("credit_amount_10", Math.round(doct));
						map.put("credit_amount_11", Math.round(dnov));
						map.put("credit_amount_12", Math.round(ddec));
						map.put("credit_amount_13",
								Math.round(djan + dfeb + dmar + dapril + dmay + djune
										+ djuly + daugust + dsept + doct + dnov + ddec));

						// = = = = = No. of credit notes & credit notes amount End = = = = = //
			
						// = = = = = Checking Pending = = = = = //
						Criteria criteria11 = session
								.createCriteria(InwardReturnGoodsRegistration.class, "inwardReturnGoodsRegistration")
								.createAlias("inwardReturnGoodsRegistration.organization", "organization")
								.createAlias("inwardReturnGoodsRegistration.company", "company")
								.createAlias("inwardReturnGoodsRegistration.stockist", "stockist")
								.add(Restrictions.eq("inwardReturnGoodsRegistration.status",1));
						criteria11.add(Restrictions.eq("inwardReturnGoodsRegistration.checkingDoneStatus",0));


						if (organizationId != null)
							criteria11.add(Restrictions.eq("organization.organizationId",
									organizationId));

						if (organizations.length != 0 && organizations != null) {
							criteria11.add(Restrictions.in("organization.organizationId",
									organizations));
						}

						if (companies.length != 0 && companies != null) {
							criteria11.add(Restrictions.in("company.companyId", companies));
						}

						if (stockists.length != 0 && stockists != null) {
							criteria11
									.add(Restrictions.in("stockist.stockistId", stockists));
						}

						criteria11.add(Restrictions.between(
								"inwardReturnGoodsRegistration.submitDate", startDate, endtDate));
						List<InwardReturnGoodsRegistration> irgrList1 = criteria11.list();
						jan = 0;
						feb = 0;
						mar = 0;
						april = 0;
						may = 0;
						june = 0;
						july = 0;
						august = 0;
						sept = 0;
						oct = 0;
						nov = 0;
						dec = 0;
						
						for (InwardReturnGoodsRegistration obj : irgrList1) {

							String month = obj.getSubmitDate().toString().substring(5, 7);

							switch (month) {

							case "01":
								jan++;
								break;

							case "02":
								feb++;
								break;

							case "03":
								mar++;
								break;

							case "04":
								april++;
								break;

							case "05":
								may++;
								break;

							case "06":
								june++;
								break;

							case "07":
								july++;
								break;

							case "08":
								august++;
								break;

							case "09":
								sept++;
								break;

							case "10":
								oct++;
								break;

							case "11":
								nov++;
								break;

							case "12":
								dec++;
								break;
							}

						}

						map.put("checking_pendings_1", jan);
						map.put("checking_pendings_2", feb);
						map.put("checking_pendings_3", mar);
						map.put("checking_pendings_4", april);
						map.put("checking_pendings_5", may);
						map.put("checking_pendings_6", june);
						map.put("checking_pendings_7", july);
						map.put("checking_pendings_8", august);
						map.put("checking_pendings_9", sept);
						map.put("checking_pendings_10", oct);
						map.put("checking_pendings_11", nov);
						map.put("checking_pendings_12", dec);
						map.put("checking_pendings_13", jan + feb + mar + april + may + june
								+ july + august + sept + oct + nov + dec);
						

						// = = = = = Checking Pending End = = = = = //
						
						// = = = = = C N Pending = = = = = //
						Criteria criteria12 = session
								.createCriteria(InwardReturnGoodsRegistration.class, "inwardReturnGoodsRegistration")
								.createAlias("inwardReturnGoodsRegistration.organization", "organization")
								.createAlias("inwardReturnGoodsRegistration.company", "company")
								.createAlias("inwardReturnGoodsRegistration.stockist", "stockist")
								.add(Restrictions.eq("inwardReturnGoodsRegistration.status",1));
						criteria12.add(Restrictions.eq("inwardReturnGoodsRegistration.checkingDoneStatus",1));
						criteria12.add(Restrictions.eq("inwardReturnGoodsRegistration.creditNoteStatus",0));


						if (organizationId != null)
							criteria12.add(Restrictions.eq("organization.organizationId",
									organizationId));

						if (organizations.length != 0 && organizations != null) {
							criteria12.add(Restrictions.in("organization.organizationId",
									organizations));
						}

						if (companies.length != 0 && companies != null) {
							criteria12.add(Restrictions.in("company.companyId", companies));
						}

						if (stockists.length != 0 && stockists != null) {
							criteria12
									.add(Restrictions.in("stockist.stockistId", stockists));
						}

						criteria12.add(Restrictions.between(
								"inwardReturnGoodsRegistration.submitDate", startDate, endtDate));
						List<InwardReturnGoodsRegistration> irgrList2 = criteria12.list();
						jan = 0;
						feb = 0;
						mar = 0;
						april = 0;
						may = 0;
						june = 0;
						july = 0;
						august = 0;
						sept = 0;
						oct = 0;
						nov = 0;
						dec = 0;
						
						for (InwardReturnGoodsRegistration obj : irgrList2) {

							String month = obj.getSubmitDate().toString().substring(5, 7);

							switch (month) {

							case "01":
								jan++;
								break;

							case "02":
								feb++;
								break;

							case "03":
								mar++;
								break;

							case "04":
								april++;
								break;

							case "05":
								may++;
								break;

							case "06":
								june++;
								break;

							case "07":
								july++;
								break;

							case "08":
								august++;
								break;

							case "09":
								sept++;
								break;

							case "10":
								oct++;
								break;

							case "11":
								nov++;
								break;

							case "12":
								dec++;
								break;
							}

						}

						map.put("cn_pendings_1", jan);
						map.put("cn_pendings_2", feb);
						map.put("cn_pendings_3", mar);
						map.put("cn_pendings_4", april);
						map.put("cn_pendings_5", may);
						map.put("cn_pendings_6", june);
						map.put("cn_pendings_7", july);
						map.put("cn_pendings_8", august);
						map.put("cn_pendings_9", sept);
						map.put("cn_pendings_10", oct);
						map.put("cn_pendings_11", nov);
						map.put("cn_pendings_12", dec);
						map.put("cn_pendings_13", jan + feb + mar + april + may + june
								+ july + august + sept + oct + nov + dec);
						

						// = = = = = C N Pending End = = = = = //
			
		

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return map;
	}

}
