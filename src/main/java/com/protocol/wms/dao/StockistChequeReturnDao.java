/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.StockistChequeReturn;

/**
 * @author Sudhakar
 *
 */
public interface StockistChequeReturnDao {
	
	public Integer saveStockistChequeReturnObj(StockistChequeReturn stockistChequeReturnObj);
	
	public List<StockistChequeReturn> loadStockistChequeReturnListUsignOrgnizationId(Integer organizationId);
	
	public StockistChequeReturn loadStockistChequeReturnObjUsignStockistChequeReturnId(Integer stockistChequeReturnId);

	public void deleteStockistChequeReturnObj(StockistChequeReturn stockistChequeReturnObj);
}
