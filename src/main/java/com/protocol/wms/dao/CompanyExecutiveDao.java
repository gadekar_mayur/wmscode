/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;
import java.util.Map;

import com.protocol.wms.model.CompanyExecutive;

/**
 * @author admin
 *
 */
public interface CompanyExecutiveDao {

	public Boolean saveExecutivesToCompany(Integer orgId,Integer companyId,CompanyExecutive companyExecutive);
	
	public CompanyExecutive getCompanyExecutiveObjById(Integer companyExecutiveId);
	
	public Boolean updateCompanyExecutivesDetails(CompanyExecutive companyExecutive);
	
	public Boolean deleteCompanyExecutive(Integer companyExeId);
	
	public List<CompanyExecutive> getCompanyExecutivesListing(Integer organizationId);
	
	public Map<String,Object> searchCompanyExecutiveDetails(String searchOption,String searchText,Integer from, Integer organizationId);
}
