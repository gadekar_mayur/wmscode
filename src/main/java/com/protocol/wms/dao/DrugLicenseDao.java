package com.protocol.wms.dao;

import java.util.List;

import com.protocol.wms.model.DrugLicence;

public interface DrugLicenseDao {
	public List<DrugLicence> loadDrugLicenseListUsingStockistId(Integer stockistId);
	
	public DrugLicence getDrugLicenceObjectByDrugLicenceId(Integer drugLicenceId);
	
	public Boolean updateDrugLicenceDetails(DrugLicence drugLicence);
}
