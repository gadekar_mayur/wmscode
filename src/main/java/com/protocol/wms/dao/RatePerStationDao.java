/**
 * 
 */
package com.protocol.wms.dao;

import com.protocol.wms.model.RatePerStation;

/**
 * @author Sudhakar
 *
 */
public interface RatePerStationDao {
	
	public Integer saveRatePerStationObject(RatePerStation ratePerStationObj);

}
