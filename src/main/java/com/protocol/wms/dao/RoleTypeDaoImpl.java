/**
 * 
 */
package com.protocol.wms.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.protocol.wms.model.RoleType;

/**
 * @author Sudhakar
 *
 */
@Repository(value="RoleTypeDaoImpl")
public class RoleTypeDaoImpl implements RoleTypeDao{

	private Logger log = Logger.getLogger(RoleTypeDaoImpl.class.getClass());
	
	private SessionFactory sessionFactory;

	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RoleTypeDao#loadRoleTypeList()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RoleType> loadRoleTypeList() {
		Session session =sessionFactory.getCurrentSession();
		List<RoleType> roleTypes=null;
		try 
		{
			Criteria criteria = session.createCriteria(RoleType.class,"RoleType")
					.add(Restrictions.eq("RoleType.status", 1))
					.addOrder(Order.asc("RoleType.roleName"));
			roleTypes=criteria.list();
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return roleTypes;
	}

	/* (non-Javadoc)
	 * @see com.protocol.wms.dao.RoleTypeDao#loadRoleTypeUsingRoleTypeId(java.lang.Integer)
	 */
	@Override
	public RoleType loadRoleTypeUsingRoleTypeId(Integer roleTypeId) {
		Session session =sessionFactory.getCurrentSession();
		RoleType roleType=null;
		try 
		{
			roleType=(RoleType)session.get(RoleType.class,roleTypeId );
		} 
		catch(DataAccessException e)
		{
			e.printStackTrace();
			log.error(e);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			log.error(e);
		}
		return roleType;
	}

}
