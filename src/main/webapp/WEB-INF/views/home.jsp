<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>CFA</title>
<!-- 	css -->
	
	<link href="<c:url value="/resources/css/icons.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/css/plugins.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/css/style.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/css/animate-custom.css" />" rel="stylesheet">
	
	
<!-- 	javascript -->
<%-- 	<script src="<c:url value="/resources/javascript/jquery-1.9.1.js"/>" type="text/javascript"></script> --%>
<%-- 	<script src="<c:url value="/resources/javascript/jquery-ui.js"/>" type="text/javascript"></script> --%>
<%-- 	<script src="<c:url value="/resources/javascript/jquery.form.js"/>" type="text/javascript"></script> --%>
	
</head>
<body class="login fade-in" data-page="login">
 <div class="container" id="login-block">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                <div class="login-box clearfix animated flipInY">
                    <div class="page-icon animated bounceInDown">
                        <img src="${pageContext.request.contextPath}/resources/img/user-icon.png" alt="Key icon">
                    </div>
                    <div class="login-logo">
                        <a href="#?login-theme-3">
                            <img src="${pageContext.request.contextPath}/resources/img/logo.png" alt="Company Logo">
                        </a>
                    </div>
                    <hr>
                    <div class="login-form">
                        <!-- BEGIN ERROR BOX -->
                        <div class="alert alert-danger hide">
                            <button type="button" class="close" data-dismiss="alert">�</button>
                            <h4>Error!</h4>
                            Your Error Message goes here
                        </div>
                        <!-- END ERROR BOX -->
                        <form action="${pageContext.request.contextPath}/LoginController/login" method="post">
                            <input type="text" placeholder="Username" class="input-field form-control user" name="uname"/>
                            <input type="password" placeholder="Password" class="input-field form-control password" name="password"/>
                            <button type="submit" class="btn btn-login">Login</button>
                        </form>
                        <!-- <div class="login-links">
                            <a href="#">Forgot password?</a>
                            </br>
                            <a href="#"> Don't have an account? <strong>Sign Up</strong></a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
