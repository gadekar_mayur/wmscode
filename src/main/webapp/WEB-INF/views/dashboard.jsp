<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CFA</title>
<!-- 	css -->
<%-- <script src="<c:url value="/resources/assets/plugins/jquery-validation/jquery.validate.min.js"/>" type="text/javascript"></script> --%>

	<script src="<c:url value="/resources/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"/>" type="text/javascript"></script>	

	<script src="<c:url value="/resources/assets/plugins/jquery-1.11.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/assets/plugins/jquery-migrate-1.2.1.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/assets/plugins/jquery-ui/jquery-ui-1.10.4.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/assets/plugins/parsley/parsley.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/assets/plugins/parsley/parsley.extend.js"/>" type="text/javascript"></script>
    <link href="<c:url value="/resources/assets/css/icons/icons.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/assets/css/plugins.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/assets/css/bootstrap.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/assets/css/style.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/assets/css/jquery.multiselect.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/css/pagination.css" />" rel="stylesheet">

	<!-- 	javascript -->
<%-- 	<script src="<c:url value="/resources/javascript/jquery-1.9.1.js"/>" type="text/javascript"></script> --%>
	<script src="<c:url value="/resources/javascript/jquery-ui.js"/>" type="text/javascript"></script>
	<link href="<c:url value="/resources/css/jquery-ui.css" />" rel="stylesheet">
	<script src="<c:url value="/resources/javascript/jquery.form.js"/>" type="text/javascript"></script>
	<script	src='<c:url value="/resources/javascript/jquery.multiselect.js"/>'type="text/javascript"></script>
<%-- 	<script src="<c:url value="/resources/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"/>" type="text/javascript"></script> --%>

	<!-- 	 Custom JavaScript	 -->
	<script src="<c:url value="/resources/customjavascript/menu.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/organization.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/employee.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/stockist.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/transporter.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/company.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/transporter.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/uploadDoc.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/inward.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/organizationView.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/transporterView.js"/>" type="text/javascript"></script> 
	<script src="<c:url value="/resources/customjavascript/viewStationPersonContactPersonDetails.js"/>" type="text/javascript"></script>  
	<script src="<c:url value="/resources/customjavascript/viewCompany.js"/>" type="text/javascript"></script>  
	<script src="<c:url value="/resources/customjavascript/viewStockist.js"/>" type="text/javascript"></script>  
	<script src="<c:url value="/resources/customjavascript/courier.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/inwardReturnGoods.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/inwardReturnGoodsChecking.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/courierView.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outwardGoods.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/globalVariable.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outward_courier.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outwardReturnGoods.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/bank.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/employeeView.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outwardCourierView.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/stockistChequeDepositEntry.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outstandingChequeDepositEntry.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/cartingAgent.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/maintenance.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outwardGoodsView.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/bankView.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outstandingChequeDepositEntryView.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editInward.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editDropDown.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editDropDown.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outstandingChequeDepositEntryView.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/organizationEdit.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editInwardReturnGoodsReg.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/maintainanceEdit.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outstandingChequeDepositEntryView.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/organizationEdit.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editInwardReturnGoodsReg.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editOutwardReturnGoods.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/cartingAgentEdit.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editCompanyDetails.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/outwardGoodsEdit.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editStockistDetails.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editTransporterDetails.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/editEmployeeDetails.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/bankEdit.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/commonFunctions.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/validation.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/CommonDropdown.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/dashboard.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/customjavascript/ApprovalRequest.js"/>" type="text/javascript"></script>
<%-- 	<script src="<c:url value="/resources/customjavascript/jquery.dataTables.min.js"/>" type="text/javascript"></script> --%>
	
	
	<%--<script	src='<c:url value="/resources/javascript/jquery.multiselect.min.js"/>'type="text/javascript"></script>--%>
	<link href='<c:url value="/resources/css/jquery-ui-1.11.2.smooth.css"/>'rel="stylesheet">
	<link href='<c:url value="/resources/css/jquery.multiselect.css"/>'	rel="stylesheet">
	
	
	<!-- For Time Picker -->
	<link href="<c:url value="/resources/css/timepicki.css" />" rel="stylesheet">
	<script	src='<c:url value="/resources/javascript/timepicki.js"/>'type="text/javascript"></script>
	
<%-- 	<script src="<c:url value="/resources/javascript/picker.js"/>" type="text/javascript"></script> --%>
<%-- 	<script src="<c:url value="/resources/javascript/picker.time.js"/>" type="text/javascript"></script> --%>
<%-- 	<script src="<c:url value="/resources/javascript/picker.date.js"/>" type="text/javascript"></script> --%>
<%-- 	<script src="<c:url value="/resources/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"/>" type="text/javascript"></script>	 --%>

	<script type="text/javascript">
	 contextApplicationPath = "${pageContext.request.contextPath}";
	</script>

</head>
<body onload="menu()" data-page="forms" >
   <!-- BEGIN TOP MENU -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
<!--                 <div class="navbar-header"> -->
<!--                     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar"> -->
<!--                         <span class="sr-only">Toggle navigation</span> -->
<!--                         <span class="icon-bar"></span> -->
<!--                         <span class="icon-bar"></span> -->
<!--                         <span class="icon-bar"></span> -->
<!--                     </button> -->
<!--                     <a id="menu-medium" class="sidebar-toggle tooltips"> -->
<!--                         <i class="fa fa-outdent"></i> -->
<!--                     </a> -->
<!--                                        <a class="navbar-brand" href="index.html">
<!--                                             <img src="resources/assets/img/logo.png" alt="logo" width="79" height="26"> -->
<!--                                         </a>--> -->
<!--                 </div> -->
                <div class="navbar-center" ><label onclick="addDashboardTable()">DashBoard</label></div>
            <!--     <div class="navbar-right">
                <div class='icon-wrapper'> <i class='fa fa-envelope fa-5x fa-border icon-red'></i> <span class='badge' id="approvalPendingId" ></span>
		</div>--> 
		</div>
            </div>
        </nav>
        <!-- END TOP MENU -->
        <!-- BEGIN WRAPPER -->
        <div id="wrapper">
            <!-- BEGIN MAIN SIDEBAR -->
            <nav id="sidebar" >
                <div id="main-menu">
                    <ul class="sidebar-nav" id="Menu">
<!--                        	 <li> -->
<!-- 					     <a href=""><i class="glyph-icon flaticon-charts2"></i><span class="sidebar-text">Master</span><span class="fa arrow"></span></a> -->
<!-- 					     <ul class="submenu collapse"> -->
<!-- 					         <li> -->
<!-- 					             <a href="orgreg.html"><span class="sidebar-text">Add Organization</span></a> -->
<!-- 					         </li> -->
<!-- 					         <li> -->
<!-- 					             <a href="compreg.html"><span class="sidebar-text">Add Company</span></a> -->
<!-- 					         </li> -->
<!-- 					         <li> -->
<!-- 					             <a href="storeg.html"><span class="sidebar-text">Add Stockist</span></a> -->
<!-- 					         </li> -->
<!-- 					         <li> -->
<!-- 					             <a href="transreg.html"><span class="sidebar-text">Add Transporter</span></a> -->
<!-- 					         </li> -->
<!-- 					         <li> -->
<!-- 					             <a href="empreg.html"><span class="sidebar-text">Add Employee</span></a> -->
<!-- 					         </li> -->
<!-- 					     </ul> -->
<!--  						</li> -->
                    </ul>

                </div>
                  
                <div class="footer-widget">
                    <div class="sidebar-footer clearfix">
                        <a class="pull-left toggle_fullscreen" href="#" rel="tooltip" data-placement="top" data-original-title="Fullscreen"><i class="glyph-icon flaticon-fullscreen3"></i></a>
                        <a class="pull-left" href="${pageContext.request.contextPath}" rel="tooltip" data-placement="top" data-original-title="Logout"><i class="fa fa-power-off"></i></a>
                    </div>
                </div>
            </nav>
            <!-- END MAIN SIDEBAR -->
            <!-- BEGIN MAIN CONTENT -->
            <div id="dashboardPopup"></div>
            <div id="popup"></div>
            <div id="popup1"></div>
            <div id="showEditImagesPopup"></div>
            <div id="showImagePopup"></div>
            <div id="main-content">
                <div ng-view>               
                   
                </div>
            </div>
            <div id="dialog-confirm"></div>
            <!-- END MAIN CONTENT -->
        </div>
        
<div id="loader" style="display: none; background: none repeat scroll 0 0 rgba(0, 0, 0, 0.59); height: 100%; left: 0; padding: 15% 0; position: fixed; text-align: center; top: 0; width: 100% !important; z-index: 10000;">
	<img src="${pageContext.request.contextPath}/resources/images/a.gif"/>
</div>       
        
	<%-- <script src="<c:url value="/resources/assets/plugins/jquery-1.11.js"/>" type="text/javascript"></script>	 --%>
	<%-- <script src="<c:url value="/resources/assets/plugins/jquery-migrate-1.2.1.jss"/>" type="text/javascript"></script> --%>
	<%-- <script src="<c:url value="/resources/plugins/jquery-ui/jquery-ui-1.10.4.min.js"/>" type="text/javascript"></script>		 --%>
	<script src="<c:url value="/resources/assets/plugins/bootstrap/bootstrap.min.js"/>" type="text/javascript"></script>	
	<script src="<c:url value="/resources/assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/assets/plugins/bootstrap-select/bootstrap-select.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/assets/plugins/icheck/icheck.js"/>" type="text/javascript"></script>	
	<script src="<c:url value="/resources/assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"/>" type="text/javascript"></script>	
	<script src="<c:url value="/resources/assets/plugins/mmenu/js/jquery.mmenu.min.all.js"/>" type="text/javascript"></script>	
	<script src="<c:url value="/resources/assets/plugins/nprogress/nprogress.js"/>" type="text/javascript"></script>	
	<script src="<c:url value="/resources/assets/plugins/charts-sparkline/sparkline.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/assets/plugins/breakpoints/breakpoints.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/assets/plugins/numerator/jquery-numerator.js"/>" type="text/javascript"></script>
	
<%-- 	<script src="<c:url value="/resources/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js"/>" type="text/javascript"></script> --%>
<%-- 	<script src="<c:url value="/resources/assets/plugins/pickadate/picker.js"/>" type="text/javascript"></script> --%>
<%-- 	<script src="<c:url value="/resources/assets/plugins/pickadate/picker.date.js"/>" type="text/javascript"></script> --%>
<%-- 	<script src="<c:url value="/resources/assets/plugins/pickadate/picker.time.js"/>" type="text/javascript"></script> --%>
<%-- 	<script src="<c:url value="/resources/assets/plugins/icheck/custom.js"/>" type="text/javascript"></script> --%>

	<script src="<c:url value="/resources/assets/plugins/bootstrap-switch/bootstrap-switch.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resources/assets/plugins/bootstrap-progressbar/bootstrap-progressbar.js"/>" type="text/javascript"></script>
<%-- 	<script src="<c:url value="/resources/assets/js/form.js"/>" type="text/javascript"></script> --%>
	<script src="<c:url value="/resources/assets/js/application.js"/>" type="text/javascript"></script>
	
</body>
</html>