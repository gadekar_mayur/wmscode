function AddTransporter()
{
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>Transporter Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class='tabcordion'>"
		+"                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#1_1' data-toggle='tab' onclick=\"transporterDetails()\">Transporter Details</a></li>"
		+"                                <li class=''><a href='#' data-toggle='tab' onclick=\"StationPersonContactDetails()\">Station Person Contact Details</a></li>"
		+"                                <li class=''><a href='#tab1_3' data-toggle='tab' onclick=\"TransporterUploadDocument()\">Upload Document</a></li>"
		+"                                <li class=''><a href='#tab1_4' data-toggle='tab' onclick=\"MergeTransporter()\">Merge Transporter</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	transporterDetails();
}
function subTabsForAddTransporterDetails(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addTransporterDetailsTabEntry1' class=''><a href='#tab1_1' data-toggle='tab' onclick='transporterDetails()'><h5><strong>Add Transporter Details</strong></h5></a></li>"
			+ "                                            <li id='viewTransporterDetailsTabEntry2' class=''><a href='#tab1_1_1' data-toggle='tab' onclick='transporterDetailsList()'><h5><strong>View Transporter Details </strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddTransporterDetails').html(html);
	if (index == 1) {
		$('#addTransporterDetailsTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewTransporterDetailsTabEntry2').addClass("active");
	}
}


function transporterDetails()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Transporter Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddTransporterDetails'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"<form id='addTransporterDetails' action='"+contextApplicationPath+"/TransporterDetailsController/saveTransporter' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"<div id='organiztionDropDown'></div>";
		
		html+="                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select State*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"<div id='stateList'></div>";
		
		
		
		html+="                                                            </div>        "   
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select City*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='city' id='cityName' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select City</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>        "    
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Address*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='textarea' class='form-control' placeholder='Address' name='address' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Password*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='Password' class='form-control' placeholder='Password' name='pwd' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Transporter Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='Transporter Name' name='transporterName' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select District*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='districtName' name='district' onchange='getCities()' class='form-control' required>"
		+"                                                                        <option disabled selected> Select District</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>        "    
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>PAN No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='PAN No' class='form-control' name='panNo' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>User Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='textarea' class='form-control' placeholder='User Name' name='userName' required>"
		+"																	  <div id='UserName_Error' class='validationError'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>" 
		+"                                                    <div class='col-md-12 col-sm-12 col-xs-12 table-responsive' style=' margin-top: 10PX;'>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addTransporterDetails\")'>Save</button>"
		+"                                                                <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='transporterDetailsList'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddTransporterDetails(1);
	loadOrganiztionDropDown();
	loadState();
	ajaxAddTransporterDetails('addTransporterDetails');
//	transporterDetailsList();
}


function ajaxAddTransporterDetails(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					if(element.MSG=='User Name already exists')
						{
						$('#UserName_Error').html(element.MSG);
						ajaxAddOrganization('addOrganizationDetails');
						$('#loader').hide();
						}
					else
						{
						$('#'+ FormId)[0].reset();
						alert(element.MSG);
//						transporterDetailsList();
						$('#loader').hide();
						}
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function  loadState()
{
	var html="<select class='form-control' class='form-control' id='stateName' name='state' onchange='getDistrict();' required>"
		+"<option selected disabled>Select State</option>";
		
		$.post(contextApplicationPath+'/TransporterDetailsController/stateList', function(data) {
			$(data).each(function(index, element) {
				html+="<option value='"+element.STATE_ID+"'>"+element.STATE_NAME+"</option>";
			}); 
			html+="</select>";
			$('#stateList').html(html);
		}, 'json');
}


function transporterDetailsList()
{
	var i=1;
	$('#loader').show();
	//$.post(contextApplicationPath+'/TransporterDetailsController/transporterDetailsList', function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab1_1_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Transporter Details</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddTransporterDetails'></div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption'class='form-control' class='form-control'>"
			+"                                                                        <option value='Organization' >Organization Name</option>"
			+"                                                                        <option value='TransporterName'>Transporter Name</option>"
			+"                                                                        <option value='Address'>Address</option>"
			+"                                                                        <option value='State'>State</option>"
			+"                                                                        <option value='District'>District</option>"
			+"                                                                        <option value='City'>City</option>"
			
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"//searchOption searchText
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls' id='searchControl'>"
			+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchTransporterDetails()'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+"                                                    </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html);
		commonTransporterDetailsController("","",1);
		subTabsForAddTransporterDetails(2);
		$('#transporterDetailsList').html(html);
		//transporterDetailsListView(data);
	//$('#loader').hide();
	//}, 'json');
}

function transporterDetailsListView(data, from, url){
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i = 0;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Transporter Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Address</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>State</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>District</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>City</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
		
		$(data).each(function(index, element) {
			if((data.length-2)<i)
				return false;
			html+="<tr style='text-align: center;'>"
				+"<td>"+ SR_NO +"</td>"
				+"<td>"+element.ORG_NAME+"</td>"
				+"<td id='TRAN_NAME"+i+"'>"+element.TRAN_NAME+"</td>"
				+"<td id='ADDRESS"+i+"'>"+element.ADDRESS+"</td>"
				+"<td id='STATA"+i+"'>"+element.STATE+"</td>"
				+"<td id='DISTRICT"+i+"'>"+element.DISTRICT+"</td>"
				+"<td id='CITY"+i+"'>"+element.CITY+"</td>"
				+"<td id='LINKS"+i+"'><a class='edit btn btn-blue' href='#' onclick=\"viewTransporterDetails("+element.TRAN_DETAILS_ID+")\"><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' href='#' onclick=\"editTransporterDetails("+element.TRAN_DETAILS_ID+","+i+")\"><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteTransporterDetails("+element.TRAN_DETAILS_ID+")\"><i class='fa fa-times-circle'></i> </a>"
				+"</tr>";
			i++;SR_NO++;
		}); 
	 +"                                                                </tbody>"
	 +"                                                            </table>";
  $('#searchResult').html(html);
  $('#loader').hide();
	paginationView(from,data[data.length-1].paginationCount,url);
}
function searchTransporterDetails(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commonTransporterDetailsController(searchOption,searchText,1);
}
	
function commonTransporterDetailsController(searchOption,searchText,from ){
	$.post(contextApplicationPath+'/TransporterDetailsController/searchTransporterDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonTransporterDetailsController(\""+searchOption+"\",\""+searchText+"\",";
		transporterDetailsListView(data, from, url);
	}, 'json');
}
function viewTransporterDetails(transporterDetailsId)
{
	ViewTransporterDetailsPopup(transporterDetailsId);
}
function viewStationPersonContactDetails(transporterStationDetailsId)
{
	ViewTransporterStationDetailsPopup(transporterStationDetailsId);
}

function deleteTransporterDetails(transporterDetailsId)
{
	//alert("in delete Transporter Details=="+transporterDetailsId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/TransporterDetailsController/deleteTransporter', {
			transporterDetailsId : transporterDetailsId
		}, function(data) {
			transporterDetailsList();
			alert(data.MSG);
		}, 'json');
    }
}
function subTabsForAddStationPersonContactDetails(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addStationPersonContactDetailsTabEntry1' class=''><a href='#tab1_2' data-toggle='tab' onclick='StationPersonContactDetails()'><h5><strong>Station PersonContact Details</strong></h5></a></li>"
			+ "                                            <li id='viewStationPersonContactDetailsTabEntry2' class=''><a href='#tab1_2_1' data-toggle='tab' onclick='StationPersonContactDetailsList()'><h5><strong>View Station PersonContact Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddStationPersonContactDetails').html(html);
	if (index == 1) {
		$('#addStationPersonContactDetailsTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewStationPersonContactDetailsTabEntry2').addClass("active");
	}
}






function StationPersonContactDetails()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_2'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Station Person Contact Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddStationPersonContactDetails'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"<form id='addStationPersonContactDetails' action='"+contextApplicationPath+"/TransporterDetailsController/saveStationPersonContactDetails' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"<div id='OrganiztionDropDownList'></div>";
		
		
		
		
		html+="                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Transporter*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"<div id='TransporterDropDown'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                                <h4><strong>Station Person Contact Details</strong></h4>"
		+"                                                            </div>"
		+"                                                            <div class='row'>"
		+"                                                                <div class='col-md-12 col-sm-12 col-xs-12'>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Contact Name</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Contact Name' class='form-control' name='stationPersonContactName'>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Email ID</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Email ID' class='form-control' name='emailId'>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                                <h4><strong>Station Details</strong></h4>"
		+"                                                            </div>"
		+"                                                            <div class='row'>"
		+"                                                                <div class='col-md-12 col-sm-12 col-xs-12'>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Station State*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                    <select  id='stateName' name='stateName' class='form-control' onchange='getDistrict()' required>"
		+"                                                                        <option disabled selected> Select State</option>"
		+"                                                                    </select>"
	//	+"                                                                            <input type='text' placeholder='Station Name' class='form-control' name='StationName' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Station District*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                    <select  id='districtName' name='districtName' class='form-control' onchange='getCities()' required>"
		+"                                                                        <option disabled selected> Select District</option>"
		+"                                                                    </select>"
	//	+"                                                                            <input type='text' placeholder='Station Name' class='form-control' name='StationName' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Station City Name*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                    <select  id='cityName' name='cityName' class='form-control' required>"
		+"                                                                        <option disabled selected> Select City</option>"
		+"                                                                    </select>"
	//	+"                                                                            <input type='text' placeholder='Station Name' class='form-control' name='StationName' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Address*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='textarea' placeholder='Address' class='form-control' name='address' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Telephone</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Telephone' class='form-control' name='telephone' parsley-type='onlynumber'>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Mobile</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Mobile' class='form-control' name='mobile' parsley-type='onlynumber'>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='panel panel-default'>"
		+"                                                            <div class='panel-body'>"
		+"                                                                <div class='col-md-4'>"
		+"                                                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                                        <h4><strong>Claim Rates</strong></h4>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Claim Rates*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Claim Rates' class='form-control' name='claimRate' parsley-type='number' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Rate Per LR*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Rate Per LR' class='form-control' name='cRatePerLR' parsley-type='number' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>DD Per Case*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='DD Per Case' class='form-control' name='cDDPerCase' parsley-type='number' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Labour Per Case*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Labour Per Case' class='form-control' name='cLaburPerCase' parsley-type='number' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='col-md-4'>"
		+"                                                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                                        <h4><strong>Rate Per Station :</strong> Rate Per Case</h4>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Rate Per Case*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Rate Per Case' class='form-control' name='rRatePerCase' parsley-type='number' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Rate Per LR*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Rate Per LR' class='form-control' name='rRatePerLR' parsley-type='number' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>DD Per Case*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='DD Per Case' class='form-control' name='rDDPerCase' parsley-type='number' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group'>"
		+"                                                                        <label class='form-label'><strong>Labour Per Case*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' placeholder='Labour Per Case' class='form-control' name='rLabourPerCase' parsley-type='number' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='col-md-12 col-sm-12 col-xs-12 table-responsive' style=' margin-top: 10PX;'>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addStationPersonContactDetails\")'>Add</button>"
		+"                                                                <button type='reset' class='btn btn-danger m-b-10'>Reset</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='StationPersonContactDetailsList'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddStationPersonContactDetails(1);
	loadTransporterOrganiztionDropDown();
	loadStationState();
	ajaxStationPersonContactDetails('addStationPersonContactDetails');
//	StationPersonContactDetailsList();
}
function  loadStationState()
{
	var optionHtml = "<option disabled selected> Select State</option>";
		$.post(contextApplicationPath+'/TransporterDetailsController/stateList', function(data) {
			$(data).each(function(index, element) {
				optionHtml+="<option value='"+element.STATE_ID+"'>"+element.STATE_NAME+"</option>";
			}); 
			$('#stateName').append(optionHtml);
		}, 'json');
}

function ajaxStationPersonContactDetails(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
//					StationPersonContactDetailsList();
					$('#loader').hide();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}
function loadTransporterOrganiztionDropDown()
{
	var userType='';
	var html="<div class='form-group'>"
	+"<label class='form-label'><strong>Select Organization*</strong>"
	+"</label>"
	+"<span class='tips'></span>"
	+"<div class='controls'>"
	+"<select class='form-control' class='form-control' id='TransOrgId' name='TransOrgId' onchange=\"changeTransporterOrganization()\" required >"   //parsley-mincheck='1' parsley-maxcheck='1'
	+"<option selected disabled >Select Organization</option>";
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
		$(data).each(function(index, element) {
			userType=element.USER_TYPE;
			if(element.USER_TYPE=='SuperAdmin')
				{
				html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
				}
			else
				{
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='TransOrgId' id='TransOrgId' >" +
						""+element.ORG_NAME+"";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
		content+="</div>"
		+"</div>";
		if(userType=='SuperAdmin')
			{
			$('#OrganiztionDropDownList').html(html);
			}
		else
			{
			$('#OrganiztionDropDownList').html(content);
			loadTransporterList($("#TransOrgId").val());
			}
		
	}, 'json');
}


function changeTransporterOrganization()
{
	loadTransporterList($("#TransOrgId").val());
}

function loadTransporterList(TransOrgId)
{
	var html="<select class='form-control' class='form-control' name='transporterId' parsley-required='true'>"  // parsley-mincheck='1' parsley-maxcheck='1'
		+"<option selected disabled>Select Transporter</option>";
			$.post(contextApplicationPath+'/TransporterDetailsController/transporterDetailsListUsingOrgId',{organizationId : TransOrgId} ,function(data) {
				$(data).each(function(index, element) {
					html+="<option value='"+element.TRAN_DETAILS_ID+"'>"+element.TRAN_NAME+"</option>";
				}); 
				html+="</select>";
				$('#TransporterDropDown').html(html);
			}, 'json');
}

function StationPersonContactDetailsList()
{
	var i=1;
	$('#loader').show();
	//$.post(contextApplicationPath+'/TransporterDetailsController/stationPersonContactDetailsList', function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab1_2_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Station PersonContact Details</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddStationPersonContactDetails'></div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
			+"                                                                        <option value='Organization'>Organization Name</option>"
			+"                                                                        <option value='Transporter'>Transporter Name</option>"
			+"                                                                        <option value='StationName'>Station Name</option>"
			+"                                                                        <option value='ContactName'>Contact Name</option>"
		//	+"                                                                        <option value='Mobile'>Mobile No.</option>"
			+"                                                                        <option value='Email'>Email ID</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchStationPersonContactDetails()'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			
			+"                                                    </div>"
			+"                                                              <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>";
		$('#myTabContent').html(html);
		subTabsForAddStationPersonContactDetails(2);	
		$('#StationPersonContactDetailsList').html(html);
		//StationPersonContactDetailsListView(data);

		commonsearchStationPersonContactDetailsController(-1,"",1);
			$('#loader').hide();
	//}, 'json');
	
}
function StationPersonContactDetailsListView(data,from,url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i =0;
	var html = ""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Transporter Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Station Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Contact Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Mobile No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Email ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
			html+="<tr style='text-align: center;'>"
			+"<td>"+ SR_NO +"</td>"
			+"<td>"+element.ORG_NAME+"</td>"
			+"<td>"+element.TRANSPORTER_NAME+"</td>"
			+"<td id='STATION_NAME"+i+"'>"+element.STATION_NAME+"</td>"
			+"<td id='CONTACT_NAME"+i+"'>"+element.CONTACT_NAME+"</td>"
			+"<td id='MOBILE_NO"+i+"'>"+element.MOBILE_NO+"</td>"
			+"<td id='EMAIL"+i+"'>"+element.EMAIL+"</td>"
			+"<td id='LINKS"+i+"'><a class='edit btn btn-blue' href='#' onclick=\"viewStationPersonContactDetails("+element.TRAN_STATION_DETAILS_ID+")\"><i class='fa fa-external-link'></i></a><a class='edit btn btn-dark' href='#' onclick=\"editStationPersonContactDetails("+element.TRAN_STATION_DETAILS_ID+","+i+")\"><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteStationPersonContactDetails("+element.TRAN_STATION_DETAILS_ID+")\"><i class='fa fa-times-circle'></i> </a>"
			+"</tr>";i++;
			SR_NO++;
		}); 
	html+="                                                                </tbody>"
		+"                                                            </table>";
  $('#searchResult').html(html);
  paginationView(from,data[data.length-1].paginationCount,url);
}

function searchStationPersonContactDetails(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText.trim()))){
		alert("Please Enter the search text");
		return false
	}
	commonsearchStationPersonContactDetailsController(searchOption,searchText,1);
}

function commonsearchStationPersonContactDetailsController(searchOption,searchText,from)
{
	$.post(contextApplicationPath+'/TransporterDetailsController/searchStationPersonContactDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from:from
	}, function(data) {
		var url = "commonsearchStationPersonContactDetailsController(\""+searchOption+"\",\""+searchText+"\",";
		StationPersonContactDetailsListView(data,from,url);
	}, 'json');
}
/*function editStationPersonContactDetails(StationPersonContactDetailsId)
{
	alert("in edit Station Person Contact Details="+StationPersonContactDetailsId);
}*/

function deleteStationPersonContactDetails(stationPersonContactDetailsId)
{
	//alert("in delete Station Person Contact Details="+stationPersonContactDetailsId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/TransporterDetailsController/deleteStationPersonContactDetails', {
			stationPersonContactDetailsId : stationPersonContactDetailsId
		}, function(data) {
			StationPersonContactDetailsList();
			alert(data.MSG);
		}, 'json');
    }
}

function subTabsForAddTransporterUploadDocument(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addTransporterUploadDocumentTabEntry1' class=''><a href='#tab1_3' data-toggle='tab' onclick='TransporterUploadDocument()'><h5><strong>Transporter Upload Document</strong></h5></a></li>"
			+ "                                            <li id='viewTransporterUploadDocumentTabEntry2' class=''><a href='#tab1_3_1' data-toggle='tab' onclick='TransporterUploadDocumentListing()'><h5><strong>View Transporter Upload Document</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddTransporterUploadDocument').html(html);
	if (index == 1) {
		$('#addTransporterUploadDocumentTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewTransporterUploadDocumentTabEntry2').addClass("active");
	}
}
function TransporterUploadDocument()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_3'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Transporter Upload Document</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddTransporterUploadDocument'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"<form id='addTransporterUploadDocument' action='"+contextApplicationPath+"/TransporterDetailsController/saveTransporterUploadDocument' method='post' enctype='multipart/form-data'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"<div id='TransporterUploadDocumentOrganiztionDropDown'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Browse File*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' id='field-2' name='imageFile'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='transporterDocumentImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='transporterDocumentImageAddMoreButtonId'><button onclick=\"addFileConrol('transporterDocumentImageDivId','transporterDocumentImageAddMoreButtonId','imageFile',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Transporter*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='transporter' id='transName' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Transporter</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Document Type*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"<div id='documentList'></div>";
		
		
		
		html+="                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addTransporterUploadDocument\")'>Submit</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='TransporterUploadDocumentListing'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddTransporterUploadDocument(1);
	loadTransporterDocumentOrganiztionDropDown();
	loadDocumentTypeList();
	ajaxAddTransporterUploadDocument('addTransporterUploadDocument');
	$('#loader').hide();
//	TransporterUploadDocumentListing();
}

function ajaxAddTransporterUploadDocument(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#'+FormId)[0].reset();
					alert(element.MSG);
//					TransporterUploadDocumentListing();
					$('#loader').hide();
				});
			}
		};
$('#'+FormId).ajaxForm(options);
}
function loadTransporterDocumentOrganiztionDropDown()
{
	var userType='';
	var html="<div class='form-group' style='height: 58px;'>"
	+"<label class='form-label'><strong>Select Organization</strong>"
	+"</label>"
	+"<span class='tips'></span>"
	+"<div class='controls'>"
	+"<select class='form-control' class='form-control' id='TranUploadDocumetOrgId' name='TranUploadDocumetOrgId' onchange='changeTransporterUploadDocumentOrgnaiation()'>" 
	+"<option value='-1'>Select Organiztion</option>";
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
		$(data).each(function(index, element) {
			userType=element.USER_TYPE;
			if(element.USER_TYPE=='SuperAdmin')
				{
				html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
				}
			else
				{
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='TranUploadDocumetOrgId' id='TranUploadDocumetOrgId'>" +
						""+element.ORG_NAME+"";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
		content+="</div>"
		+"</div>";
		if(userType=='SuperAdmin')
			{
			$('#TransporterUploadDocumentOrganiztionDropDown').html(html);
			}
		else
			{
			$('#TransporterUploadDocumentOrganiztionDropDown').html(content);
			loadTransporter($("#TranUploadDocumetOrgId").val());
			}
		
	}, 'json');
}

function changeTransporterUploadDocumentOrgnaiation()
{
	loadTransporter($("#TranUploadDocumetOrgId").val());
}

function loadTransporter(orgId)
{
	$.post(contextApplicationPath+'/TransporterDetailsController/getTransporter', {
		orgId : orgId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Transporter</option>";
		for (counter = 0; counter < data.transporterArray.length; counter++) {
			optionHtml += "<option value='" + data.transporterArray[counter].transId + "'>"
			+ data.transporterArray[counter].transName + "</option>";
		}
		$("#transName").html(optionHtml);
	}, 'json');
}

function TransporterUploadDocumentListing()
{
	var i=1; 
	$('#loader').show();
	//$.post(contextApplicationPath+'/TransporterDetailsController/loadTransporterDocument',function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab1_3_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Transporter Upload Document</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddTransporterUploadDocument'></div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
			+"                                                                        <option value='Organization'>Organization Name</option>"
			+"                                                                        <option value='TransporterName'>Transporter Name</option>"
			+"                                                                        <option value='DocumentType'>Document Type</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input id='searchText' type='text' class='form-control'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchTransporterUploadDocument()'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+"                                                    </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+"                                                </div>"
			+"                                            </div>";
		$('#myTabContent').html(html);
		subTabsForAddTransporterUploadDocument(2);
		$('#TransporterUploadDocumentListing').html(html);
		commonTransporterUploadDocumentController("", "", 1);
		//TransporterUploadDocumentListingView(data);
		$('#loader').hide();
	//}, 'json');
}

function commonTransporterUploadDocumentController(searchOption, searchText, from)
{
	$.post(contextApplicationPath+'/TransporterDetailsController/searchTransporterUploadDocument', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonTransporterUploadDocumentController(\""+searchOption+"\",\""+searchText+"\",";
		TransporterUploadDocumentListingView(data,from,url);
	}, 'json');
}

function TransporterUploadDocumentListingView(data,from,url)
{	//global var
	commonData = data;
	urlForEdit = url;
	var i = 0; 
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html = ""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Transporter Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Document Type</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>File Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="<tr style='text-align: center;'>"
			+"<td>"+SR_NO+"</td>"
			+"<td>"+element.ORG_NAME+"</td>"
			+"<td>"+element.TRAN_NAME+"</td>"
			+"<td>"+element.DOC_TYPE+"</td>"
			+"<td><img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showTransporterDocumentFileListPopup("+i+")'></td>"//+element.FILE_NAME+"</td>"
			+"<td><a class='edit btn btn-dark' href='#' onclick=\"editTransporterDocument("+i+","+from+")\"><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteTransporterDocument("+element.TRAN_DOCUMENT_ID+")\"><i class='fa fa-times-circle'></i> </a></td>"
			+"</tr>";
		i++;
		SR_NO++;
	}); 
	html+="                                                                </tbody>"
		+"                                                            </table>";
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);
}

function showTransporterDocumentFileListPopup(index){
	showListOfImagesPopup(commonData[index].FILE_NAME);
}
function searchTransporterUploadDocument(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText.trim()))){
		alert("Please Enter the search text");
		return false
	}
	commonTransporterUploadDocumentController(searchOption,searchText,1);
}

function commonTransporterUploadDocumentController(searchOption,searchText,from)
{
	$.post(contextApplicationPath+'/TransporterDetailsController/searchTransporterUploadDocument', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonTransporterUploadDocumentController(\""+searchOption+"\",\""+searchText+"\",";
		TransporterUploadDocumentListingView(data,from,url);
	}, 'json');
}

function editTransporterDocument(index, from)
{
	editTransporterDocumentForm(index, from);
}

function deleteTransporterDocument(transporterDocumentId)
{
	//alert("in delete Transporter Document===="+transporterDocumentId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/TransporterDetailsController/deleteTransporterDocument', {
			transporterDocumentId : transporterDocumentId
		}, function(data) {
			TransporterUploadDocumentListing();
			alert(data.MSG);
		}, 'json');
    }
}
 function MergeTransporter()
 {
	 var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_3'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>Merge Transporter</strong></h3>"
			+"                                    </div>"
			+"<div id='subTabsForMergeTransporter'></div>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-12'>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
		//	+"                                                            <div class='form-group'>"
		//	+"                                                            <div class='form-group'>"
			+"                                                            	  <div id='organiztionTransporterDropDown'>"
		//	+"                                                            	  </div>"
		    +"                                                            </div>"
		    +"                                                            </div>"
		    +"<div class='col-md-4'>"
			+"																<div id='companyDropDown'></div>"
			+"                                                        </div>"

		    +"                                                            </div>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong> From Transporter</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select  id='StockistName' name='stockistId' class='form-control' required>"
			+"                                                                        <option disabled selected> Select Transporter</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong> To Transporter</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select  id='TransporterName' name='transporterId' class='form-control' required>"
			+"                                                                        <option disabled selected> Select Transporter</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"</div>"
			+"</div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchStockistDetails()'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+"                                                                <button class='btn btn-danger m-b-10' data-toggle='modal' data-target='#modal-responsive21' onclick='transferTooTransporter()'>Transfer To</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='pull-right'>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                                <div class='row'>"
			+"                                                    <div class='col-md-6'>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                            <table class='table table-striped table-hover'>"
			+"                                                                <thead class='no-bd' id='a'>"
			+"                                                                    <tr>"
			+"                                                            <th style='text-align: center;'><strong>Select Stockist</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Stockist City</strong>"
			+"                                                            </th>"
			//+"                                                            <th style='text-align: center;'><strong>Order No</strong>"
			//+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Actual Rate</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Claiming Rate</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Actual LR</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Claiming LR</strong>"
			+"                                                            </th>"
			//+"                                                            <th style='text-align: center;'><strong>Company</strong>"
			//+"                                                            </th>"
			
			+"                                                            </tr>"
			+"                                                            </thead>"
			+"                                                                <tbody class='no-bd-y' id='transporterStockistSearch'>"
			+"                                                                </tbody>"
			+"                                                            </table>"
			+"                                                        </div>"
			+"                                                     <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
	 $('#myTabContent').html(html);
	 loadOrganiztionTransporterDropDown();
	 subTabsForMergeTransporter(1);
	
	// OutWardLoadAllCompanyDetails('organiztionId');
	 
 }
 function subTabsForMergeTransporter(index) {
		var html = ""
				+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
				+ "                                            <li id='transferStockistTabEntry1' class=''><a href='#tab1_2' data-toggle='tab' onclick='MergeTransporter()'><h5><strong>Merge Transporter</strong></h5></a></li>"
				+ "                                            <li id='viewTransferForApprovalTabEntry2' class=''><a href='#tab1_2_1' data-toggle='tab' onclick='AdminApprovalPendingList()'><h5><strong>Admin Approval Status</strong></h5></a></li>"
			//	+"                                             <li id='TransferApprovedTabEntry3' class=''><a href='#tab1_2_2' data-toggle='tab' onclick='StationPersonContactDetailsList()'><h5><strong>Approved</strong></h5></a></li>"
				                                     
				+ "                                        </ul>"; 	
		$('#subTabsForMergeTransporter').html(html);
		if (index == 1) {
			$('#transferStockistTabEntry1').addClass("active");
		} else if (index == 2) 
		{
			$('#viewTransferForApprovalTabEntry2').addClass("active");
		}
		else
			{
			$('#TransferApprovedTabEntry3').addClass("active");
			}
	}
 function searchStockistDetails(){
	
		var OrgId = $('#orgId').val();
		var CompanyId = $('#comapnyId').val();
		var TransporterId=$('#StockistName').val();
		var from=1;
		if(!(Boolean(CompanyId))){
			alert("Please Select Company");
			return false
		}
		else if(!(Boolean(TransporterId))){
			alert("Please Select Transporter");
			return false
		}
		commonTransporterStockistDetailsController(OrgId,CompanyId,TransporterId);
	}
		
	function commonTransporterStockistDetailsController(OrgId,CompanyId,TransporterId){
		$('#loader').show();
		$.post(contextApplicationPath+'/TransporterDetailsController/searchTransporterStockistDetails', {
			OrgId : OrgId,
			CompanyId : CompanyId,
			TransporterId : TransporterId
			
		}, function(data) {
			var url = "commonTransporterStockistDetailsController(\""+OrgId+"\",\""+CompanyId+"\",\""+TransporterId+"\",";
			
			transporterStokistDetailsListView(data,url);
		}, 'json');
	}
	function OutWardLoadAllCompanyDetails(orgId)
	{
		$.post(contextApplicationPath+'/CompanyController/getCompanies',{orgId : orgId }, function(data) {
		var	html="<div class='form-group'>"
				+"<label class='form-label'><strong>Select Company</strong>"
				+"</label>"
				+"<span class='tips'></span>"
				+"<div class='controls'>"
				+"<select class='form-control' class='form-control' name='comapnyId' id='comapnyId' onchange='getTransporterDropDown()' required>"
				+"<option selected disabled>Select Company</option>";
			for (var i = 0; i < data.companyArray.length; i++)
			{
				html += "<option value='" + data.companyArray[i].compId + "'>"+ data.companyArray[i].compName + "</option>";
			}
			html+="</select>"
			+"</div>"
			+"</div>";
			$('#companyDropDown').html(html);
		}, 'json');
	}
	
	function getTransporterDropDown()
	{
		var orgId=$('#orgId').val();
		var compId = $('#comapnyId').val();
	
			$.post(contextApplicationPath+'/TransporterDetailsController/getTransporter', {
				orgId : orgId,
				compId : compId
			}, function(data) {
				//$('#loader').hide();
				var counter = 0;
				var optionHtml = "<option disabled selected> Select Transporter</option>";
				for (counter = 0; counter < data.transporterArray.length; counter++) {
					optionHtml += "<option value='" + data.transporterArray[counter].transporterId + "'>"
					+ data.transporterArray[counter].transporterName + "</option>";
					
				}
				$("#StockistName").empty();
				$("#TransporterName").empty();
				$("#StockistName").append(optionHtml);
				$("#TransporterName").append(optionHtml);
			}, 'json');
			
	}
	function loadOrganiztionTransporterDropDown() {
		
		var userType='';
	var html="<div class='form-group'>"
		+"<label class='form-label'><strong>Select Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>"
		+"<select class='form-control' class='form-control' id='orgId' name='selectedOrganiztionNameId' onchange='OutGoodsRegistrationOrganizationChange()' required>" 
		+"<option selected disabled>Select Organiztion</option>";
		var content="<div class='form-group' style='height: 58px;'>"
			+"<label class='form-label'><strong>Your Organization</strong>"
			+"</label>"
			+"<span class='tips'></span>"
			+"<div class='controls'>";
		$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
			$(data).each(function(index, element) {
				userType=element.USER_TYPE;
				if(element.USER_TYPE=='SuperAdmin')
					{
					html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
					}
				else
					{
					content+="<input type='hidden' value='"+element.ORG_ID+"' name='selectedOrganiztionNameId' id='orgId'>" +
							""+element.ORG_NAME+"";
					}
			}); 
			html+=" </select>"
				+"</div>"
				+"</div>";
			content+="</div>"
			+"</div>";
			if(userType=='SuperAdmin')
				{
				$('#organiztionTransporterDropDown').html(html);
				}
			else
				{
				$('#organiztionTransporterDropDown').html(content);
				OutWardLoadAllCompanyDetails($("#orgId").val());
//				getStockistByOrg();
				}
		}, 'json');}

	function transporterStokistDetailsListView(data,url)
	{
		var i=0;
		var html1;
		 if(data=="")
			 {
			 alert("Sorry No Data Available");
			 }
		$(data).each(function(index,element){
			//console.log(element.STOCKIST_NAME);
			
			
				
			
				if(element.PENDING_STATUS==1)
					{
					console.log(element.STOCKIST_NAME);
					html1+="<tr style='text-align: center;'>"
					+"<td>"
					+"<div class='div_checkbox'>"
				+"<input type='checkbox' id='checkbox_"+element.STOCKIST_ID+"' onclick='AddCasesCheckboxEvent("+element.STOCKIST_ID+")' style='opacity: 1 !important;' name='TransporterStockistId' value='"+element.STOCKIST_ID+"'>"
				+"<input type='hidden' id='stockist_"+element.STOCKIST_ID+"' value='"+element.STOCKIST_ID+"'>"
				+"<input type='hidden' id='transporter_"+element.STOCKIST_ID+"' value='"+element.TRANSPORTER_ID+"'>"
				+"<input type='hidden' id='transporter_station_"+element.STOCKIST_ID+"' value='"+element.TRANSPORTER_STATION_ID+"'>"
				+"<input type='hidden' id='STOCKIST_NAME_"+element.STOCKIST_ID+"' value='"+element.STOCKIST_NAME+"'>"
				+"<input type='hidden' id='CITY_"+element.STOCKIST_ID+"' value='"+element.CITY+"'>"
				+"<input type='hidden' id='ACTUAL_RATE_"+element.STOCKIST_ID+"' value='"+element.ACTUAL_RATE+"'>"
				+"<input type='hidden' id='CLAIM_RATE_"+element.STOCKIST_ID+"' value='"+element.CLAIM_RATE+"'>"
				+"<input type='hidden' id='ACTUAL_LR_"+element.STOCKIST_ID+"' value='"+element.ACTUAL_LR+"'>"
				+"<input type='hidden' id='CLAIM_LR_"+element.STOCKIST_ID+"' value='"+element.CLAIM_LR+"'>"
				+"</div>"
				+"</td>"
				+"<td id='STOCKIST_NAME"+i+"'>"+element.STOCKIST_NAME+"</td>"
				+"<td id='CITY"+i+"'>"+element.CITY+"</td>"
				+"<td id='ACTUAL_RATE"+i+"'>"+element.ACTUAL_RATE+"</td>"
				+"<td id='CLAIM_RATE"+i+"'>"+element.CLAIM_RATE+"</td>"
				+"<td id='ACTUAL_LR"+i+"'>"+element.ACTUAL_LR+"</td>"
				+"<td id='CLAIM_LR"+i+"'>"+element.CLAIM_LR+"</td>"
				+"</tr>";
					}
				else
					{
					console.log("pending"+element.STOCKIST_NAME);
					html1+="<tr style='text-align: center;'>"
					+"<td> </td>"
					
				+"<td id='STOCKIST_NAME"+i+"'>"+element.STOCKIST_NAME+"</td>"
				+"<td id='CITY"+i+"'>"+element.CITY+"</td>"
				+"<td id='ACTUAL_RATE"+i+"'>"+element.ACTUAL_RATE+"</td>"
				+"<td id='CLAIM_RATE"+i+"'>"+element.CLAIM_RATE+"</td>"
				+"<td id='ACTUAL_LR"+i+"'>"+element.ACTUAL_LR+"</td>"
				+"<td id='CLAIM_LR"+i+"'>"+element.CLAIM_LR+"</td>"
				+"</tr>";
					}
				
				
			
			i++;
		});
		 $('#transporterStockistSearch').empty();
		$('#transporterStockistSearch').html(html1);
		$('#loader').hide();
	}
	function AdminApprovalPendingList()
	{
		
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_3'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>Admin Approval Status List</strong></h3>"
			+"                                    </div>"
			+"<div id='subTabsForMergeTransporter'></div>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-12'>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			/*+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
		//	+"                                                            <div class='form-group'>"
		//	+"                                                            <div class='form-group'>"
			+"                                                            	  <div id='organiztionTransporterDropDown'>"
		//	+"                                                            	  </div>"
		    +"                                                            </div>"
		    +"                                                            </div>"
		    +"                                                            </div>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"																<div id='companyDropDown'></div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Transporter</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select  id='StockistName' name='stockistId' class='form-control' required>"
			+"                                                                        <option disabled selected> Select Transporter</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"</div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchStockistDetails()'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+"                                                                <button class='btn btn-danger m-b-10' onclick='searchStockistDetails()'>Transfer Too</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='pull-right'>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>"*/
			+"                                                <div class='row'>"
			+"                                                    <div class='col-md-6'>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                            <table class='table table-striped table-hover'>"
			+"                                                                <thead class='no-bd' id='a'>"
			+"                                                                    <tr>"
			+"                                                            <th style='text-align: center;'><strong>Sr No</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
			+"                                                            </th>"
			//+"                                                            <th style='text-align: center;'><strong>Stockist City</strong>"
			//+"                                                            </th>"
			//+"                                                            <th style='text-align: center;'><strong>Order No</strong>"
			//+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>From Transporter</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Too Transporter</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Approval Status</strong>"
			+"                                                            </th>"
			/*+"                                                            <th style='text-align: center;'><strong>Claiming LR</strong>"
			+"                                                            </th>"*/
			//+"                                                            <th style='text-align: center;'><strong>Company</strong>"
			//+"                                                            </th>"
			
			+"                                                            </tr>"
			+"                                                            </thead>"
			+"                                                                <tbody class='no-bd-y' id='transporterStockistAdminApprovaStatus'>"
			+"                                                                </tbody>"
			+"                                                            </table>"
			+"                                                        </div>"
			+"                                                     <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html)
		subTabsForMergeTransporter(2);
		loadAdminApprovalStatusList(1);
	}
	
	function transferTooTransporter()
	{
		var i=1;
		var fromTransporterID=$('#StockistName').val();
		var toTransporterId=$('#TransporterName').val();
		var fromtransporterName= $("#StockistName option:selected").text();
		var totransporterName= $("#TransporterName option:selected").text();
		
		//alert(transporterName)
		var checkboxesValues = document.getElementsByName('TransporterStockistId');
		var vals = "";
		var transporterValues="";
		var stockistValue="";
		var transporterStationValue="";
		var stockistName="";
		var stockistCity="";
		var actualRate="";
		var claimRate="";
		var actualLR="";
		var claimLR="";
		for (var i=0, n=checkboxesValues.length;i<n;i++) {
		  if (checkboxesValues[i].checked) 
		  {
			  transporterValues +=","+$("#transporter_"+checkboxesValues[i].value+"").val();
		  stockistValue +=","+$("#stockist_"+checkboxesValues[i].value+"").val();
		  transporterStationValue +=","+$("#transporter_station_"+checkboxesValues[i].value+"").val();
		  stockistName +=","+$("#STOCKIST_NAME_"+checkboxesValues[i].value+"").val();
		  stockistCity +=","+$("#CITY_"+checkboxesValues[i].value+"").val();
		  actualRate +=","+$("#ACTUAL_RATE_"+checkboxesValues[i].value+"").val();
		  claimRate +=","+$("#CLAIM_RATE_"+checkboxesValues[i].value+"").val();
		  actualLR +=","+$("#ACTUAL_LR_"+checkboxesValues[i].value+"").val();
		  claimLR +=","+$("#CLAIM_LR_"+checkboxesValues[i].value+"").val();
		  vals += ","+checkboxesValues[i].value;
		  }
		}
		
		
		if (vals) vals = vals.substring(1);
		if(transporterValues) transporterValues=transporterValues.substring(1);
		if(stockistValue) stockistValue=stockistValue.substring(1);
		if(transporterStationValue) transporterStationValue=transporterStationValue.substring(1);
		if(stockistName) stockistName=stockistName.substring(1);
		if(stockistCity) stockistCity=stockistCity.substring(1);
		if(actualRate) actualRate=actualRate.substring(1);
		if(claimRate) claimRate=claimRate.substring(1);
		if(actualLR) actualLR=actualLR.substring(1);
		if(claimLR) claimLR=claimLR.substring(1);
		if(vals=="")
		{
		alert("Please select at least one check box");
		}
	else
		{
		var transporterArray = new Array();
		var stockistArray=new Array();
		var transporterStationArray=new Array();
		var stockistNameArray=new Array();
		var stockistCityArray=new Array();
		var actualRateArray=new Array();
		var claimRateArray=new Array();
		var actualLRArray=new Array();
		var claimLRArray=new Array();
		// this will return an array with strings "1", "2", etc.
		transporterArray = transporterValues.split(",");
		stockistArray=stockistValue.split(",");
		transporterStationArray=transporterStationValue.split(",");
		 stockistNameArray=stockistName.split(",");
		 stockistCityArray=stockistCity.split(",");
		 actualRateArray=actualRate.split(",");
		 claimRateArray=claimRate.split(",");
		 actualLRArray=actualLR.split(",");
		 claimLRArray=claimLR.split(",");
		
		
		
		console.log("stockist Array"+stockistArray)
		console.log("transporter Array"+transporterArray)
		console.log("transporterStation Array"+transporterStationArray)
		var html="                                                                            <div class='modal fade' id='modal-responsive' aria-hidden='true'>"
			+"                                                                                <div class='modal-dialog modal-lg'>"
			+"<form id='transferSelectedStockist' action='"+contextApplicationPath+"/TransporterDetailsController/saveTranfereSelectedStockist' method='post'>"
			+"<input type='hidden' name='stockistId' value='"+stockistArray+"'>"
			+"<input type='hidden' name='transporterStationArrayId' value='"+transporterStationArray+"'>"
			+"<input type='hidden' name='fromTransporterId' value='"+fromTransporterID+"'>"
			+"<input type='hidden' name='toTransporterId' value='"+toTransporterId+"'>"
			+"                                                                                    <div class='modal-content'>"
			+"                                                                                        <div class='modal-header'>"
			+"                                                                                            <button type='button' class='close' data-dismiss='modal' aria-hidden='true' onclick='hidePopUp()'>X</button>"
			+"                                                                                            <h4 class='modal-title' id='myModalLabel'><strong>Selected Transporter List</strong></h4>"
			+"                                                                                            <h4 class='modal-title' id='myModalLabel'><strong>Do you want to transfer stockist from "+fromtransporterName+" to "+totransporterName+" </strong></h4>"
			
			+"                                                                                        </div>"
			+"                                                                                        <div class='modal-body' style=' text-align: left;'>"
			+"                                                                                            <div class='row'>"
			+"                                                                                                <div class='col-md-12'>"
			+"                                                                                                    <div class='panel panel-default'>"
			+"                                                                                                        <div class='panel-body'>"
			+"                                                                                                            <div class='row'>    "                                
			+"                                                                                                                <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                                                                                    <table class='table table-striped table-hover'>"
			+"                                                                                                                        <thead class='no-bd'>"
			+"                                                                    <tr>"
			+"                                                                        <th style='text-align: center;'><strong>Sr No</strong>"
			+"                                                                        </th>"
			+"                                                                        <th style='text-align: center;'><strong>Stockist Name</strong>"
			+"                                                                        </th>"
			+"                                                                        <th style='text-align: center;'><strong>Stockist City</strong>"
			+"                                                                        </th>"
			+"                                                                        <th style='text-align: center;'><strong>Actual Rate</strong>"
			+"                                                                        </th>"
			+"                                                                        <th style='text-align: center;'><strong>Claim Rate</strong>"
			+"                                                                        </th>"
			+"                                                                        <th style='text-align: center;'><strong>Actual LR</strong>"
			+"                                                                        </th>"
			+"                                                                        <th style='text-align: center;'><strong>Claim LR</strong>"
			+"                                                                        </th>"
			+"                                                                    </tr>"
			+"                                                                                                                        </thead>"
			+"                                                                                                                        <tbody class='no-bd-y'>";
		for(var i=0;i<stockistArray.length;i++)
			{
			html+="<tr style='text-align: center;'>"
				+"<td>"+[i+1]+"</td>"
				+"<td>"+stockistNameArray[i]+"</td>"
				+"<td>"+stockistCityArray[i]+"</td>"
				+"<td>"+actualRateArray[i]+"</td>"
				+"<td>"+claimRateArray[i]+"</td>"
				+"<td>"+actualLRArray[i]+"</td>"
				+"<td>"+claimLRArray[i]+"</td>"
				+"</tr>";	
			}
			html+="                                                                                                                        </tbody>"
				+"                                                                                                                    </table>"
				+"                                                                                                                </div>"
				+"                                                                                                            </div>"
				+"                                                                                                        </div>"
				+"                                                                                                    </div>"
				+"                                                                                                </div> "
				+"                                                                                            </div>"
				+"                                                                                        </div>"
				+"                                                                                        <div class='modal-footer text-center'>"
				+"                                                                                            <button type='submit' class='btn btn-primary' onclick='validateFormDetails(\"#transferSelectedStockist\")'>Transfer</button>"
				+"                                                                                            <button type='button' class='btn btn-danger' data-dismiss='modal' onclick='hidePopUp()'>Cancel</button>"
				+"                                                                                        </div>"
				+"                                                                                    </div>"
				+"</form>"
				+"                                                                                </div>"
				+"                                                                            </div>";
		$('#popup').html(html);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block");
		ajaxAddCases('transferSelectedStockist');
	}
		function ajaxAddCases(FormId)
		{
			
			var options = {
					success : function(json) {
						$(json).each(function(index, element) {
							$('#loader').hide();
							$('#'+ FormId)[0].reset();
							alert(element.MSG);
							MergeTransporter();
							$("#modal-responsive").remove();
							
						});
					}
				};
		$('#' + FormId).ajaxForm(options);
		}
}
	function loadAdminApprovalStatusList(rateId)
	{
		$.post(contextApplicationPath+'/TransporterDetailsController/loadAdminApprovalStatus',{
			rateId:rateId	
		
		},function(data)
		{
			var url = "commonTransporterStockistDetailsController(\""+rateId+"\",";
			
			transporterStokistAdminApprovalListListView(data,url);
		
				
			
				},'json');
	}
	
	function transporterStokistAdminApprovalListListView(data,url)
	{
		
		var i=0;
		var html1;
		 if(data=="")
			 {
			 alert("Sorry No Data Available");
			 }
		$(data).each(function(index,element){
			console.log(element.STOCKIST_NAME);
			
				if(element.status==0)
					{
					
					html1+="<tr style='text-align: center;'>"
						+"<td>"+(i+1)+"</td>"
						+"<td id='StockistName"+i+"'>"+element.StockistName+"</td>"
						+"<td id='FromTransporterName"+i+"'>"+element.FromTransporterName+"</td>"
						+"<td id='ToTransferName"+i+"'>"+element.ToTransferName+"</td>"
				+"<td id='Status"+i+"'> PENDING </td>"
				+"</tr>";
					}
				else if(element.status==1)
					{
					
					html1+="<tr style='text-align: center;'>"
						+"<td>"+(i+1)+"</td>"
						+"<td id='StockistName"+i+"'>"+element.StockistName+"</td>"
						+"<td id='FromTransporterName"+i+"'>"+element.FromTransporterName+"</td>"
						+"<td id='ToTransferName"+i+"'>"+element.ToTransferName+"</td>"
						+"<td id='Status"+i+"'> APPROVED </td>"
				+"</tr>";
					
					}
				else 
					{
					
					html1+="<tr style='text-align: center;'>"
						+"<td>"+(i+1)+"</td>"
						+"<td id='StockistName"+i+"'>"+element.StockistName+"</td>"
						+"<td id='FromTransporterName"+i+"'>"+element.FromTransporterName+"</td>"
						+"<td id='ToTransferName"+i+"'>"+element.ToTransferName+"</td>"
						+"<td id='Status"+i+"'> REJECTED </td>"
				+"</tr>";
					}
				
			i++;
		});
		 $('#transporterStockistAdminApprovaStatus').empty();
		$('#transporterStockistAdminApprovaStatus').html(html1);
		$('#loader').hide();
	}