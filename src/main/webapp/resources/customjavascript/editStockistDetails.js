function editStockistRegDetails(stockistId,mainIndex){
	$.post(contextApplicationPath+'/StockistController/ViewStockistDetails', {stockistId : stockistId , editStatus : true}, function(data) {
		$(data).each(function(index,element){
				var i =0;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Update View Stockist Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"
			        + "				 <form id='editStockistRegDetails' action='"+contextApplicationPath+"/StockistController/updateStockistRegistrationDetails' method='post'>"
			        +"                                                    <div class='row'>"
			        +"														  <input  type='hidden' value='"+stockistId+"' name='stockistId' class='form-control'>"
					+"                                                        <div class='col-md-4'>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Organization Name*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input  type='text' value='"+element.ORG_NAME+"' disabled class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Stockist Name*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input name='stockistName' id='stockistNameId' value='"+element.STOCKIST_NAME+"' type='text' placeholder='Stockist Name' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>State*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control' required>"
					+"																		<option disabled selected> Select State</option>"
				    +"																	</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group' style='height: 58px;'>"
					+"                                                                <label class='form-label'><strong>City*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <select name='city' id='cityName' class='form-control' class='form-control' required>"
					+"                                                                        <option disabled selected> Select City</option>"
					+"                                                                    </select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Email ID</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input type='text' name='email' id='emailId' value='"+element.EMAIL_ID+"' placeholder='Email ID' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Contact Person</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input type='text' name='contactPerson' id='contactPersonId' value='"+element.CONTACT_PERSON+"' placeholder='Contact Person' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group' style='height: 58px;'>"
					+"                                                                <label class='col-lg-12 control-label' style='padding-left: 0px;'><strong>Stockist Location*</strong></label>"
					+"                                                                <div class='col-lg-12'>"
					+"                                                                    <div class='pos-rel'>"
					+"                                                                        <input tabindex='13' id='ctest' type='radio' name='location' value='Local' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;'>"
					+"                                                                        <label class='p-l-40' for='flat-checkbox-1'>Local</label>"
					+"                                                                    </div>"
					+"                                                                    <div class='pos-rel'>"
					+"                                                                        <input tabindex='14' id='ctest' type='radio' name='location' value='Outstation' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;'>"
					+"                                                                        <label class='p-l-40' for='flat-checkbox-2'>Outstation</label>"
					+"                                                                    </div>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>VAT*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input type='text' name='vat'  value='"+element.VAT+"' placeholder='VAT' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>PAN No*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input type='text' name='panNo'  value='"+element.PAN_NO+"' placeholder='PAN No' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Address*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input name='address' type='textarea'  value='"+element.ADDRESS+"' placeholder='Address' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Mobile No*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input name='mobile' id='mobileId' type='text'  value='"+element.MOBILE+"' placeholder='Mobile No' class='form-control' parsley-type='onlynumber' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group' style='height: 58px;'>"
					+"                                                                <label class='form-label'><strong>District*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control' required>"
					+"                                                                        <option disabled selected> Select District</option>"
					+"                                                                    </select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Fax No</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input name='fax' type='text'  value='"+element.FAX_NO+"' placeholder='Fax No' class='form-control' parsley-type='onlynumber'>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Proprietor/Partner*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input type='text' name='pro_part'  value='"+element.PARTNER+"' placeholder='Proprietor/Partner' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group' style='height: 58px;'>"
					+"                                                                <label class='form-label'><strong>Transporter*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <select name='transName' id='transName' class='form-control' class='form-control' required>"
					+"                                                                        <option disabled selected> Select Transporter*</option>"
					+"                                                                    </select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CST</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input name='cst' type='text'  value='"+element.CTS+"' placeholder='CST' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>User Name*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input name='uname' type='text'  value='"+element.USER_NAME+"' placeholder='User Name' class='form-control' required>"
					+"                                                                </div>"
					+"																  <div id='error' class='validationError' ></div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Password*</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                    <input name='password' type='password'  value='"+element.PASSWORD+"' placeholder='Password' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                    </div>"
					+"                                                    <div class='row'>"
					+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
					+"                                                                <div class='pull-right'>"
					+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#editStockistRegDetails\")'>Save</button>"
					+"                                                                    <button type='reset' onClick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
					+"                                                                </div>"
					+"</form>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                    </div>"
					//+"				</form>"
					 +"                                                    <div class='row'>"
		             +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		             +"                                                            <table class='table table-striped table-hover'>"
		             +"                                                                <thead class='no-bd'>"
		              +"                                                                    <tr>"
		              +"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>Drug License Number</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>From Date</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>To Date</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>Validity</strong>"
		              +"                                                                        </th> "
		              +"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		              +"                                                                        </th> "
		              +"                                                                    </tr>"
		              +"                                                                </thead>"
		              +"                                                                <tbody class='no-bd-y'>";
					commonData = element.DRUG_ARRAY;
		             $(commonData).each(function(index,element){
		            html+="                                                                    <tr style='text-align: center;'>"
		            +"                                                                        <td>"+ (i+1) +"</td>"
		            +"                                                                        <td id='DRUG_LICENSE_NO"+i+"'>"+element.DRUG_LICENSE_NO+"</td>"
		            +"                                                                        <td id='FROM_DATE"+i+"'>"+element.FROM_DATE+"</td>"
		            +"                                                                        <td id='TO_DATE"+i+"'>"+element.TO_DATE+"</td>"
		            +"                                                                        <td id='VALIDITY"+i+"'>"+element.VALIDITY+"</td>"
		            +" <td><a class='edit btn btn-dark' onClick='editDrugLicenseRegDetails("+element.DRUG_LICENSE_Id+","+i+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteDrugLicenseDetails()' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
		            +"                                                                    </tr>";
		            i++;
		             });
		    html+="                                                                </tbody>"
		    +"                                                            </table>"
		    +"                                                        </div>"
		    +"<div id='subPopUp'></div>"
		    +"                                                    </div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $('input:radio[name="location"]').filter('[value="'+element.LOCATION+'"]').attr('checked', true);
				 ajaxEditStockistRegDetails("editStockistRegDetails",mainIndex)
				 getTransporterDropdownForEdit(element.ORG_ID,element.TRANSPORTER_ID);
				 stateDropDownListForEdit(element.STATE_ID);
				 getDistrictForEdit(element.DISTRICT_ID,element.STATE_ID);
				 getCitiesForEdit(element.DISTRICT_ID,element.CITY_ID);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
		});
	}, 'json');
}
function ajaxEditStockistRegDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						//alert($('input:radio[name=location]:checked').val());
						$('#stockistName'+mainIndex).html($('#stockistNameId').val());
						$('#contactPerson'+mainIndex).html($('#contactPersonId').val());
						$('#mobile'+mainIndex).html($('#mobileId').val());
						$('#email'+mainIndex).html($('#emailId').val());
						if(data.STOCKIST_ID!=null||data.STOCKIST_ID!=undefined){
							//alert("NEW ID = "+data.STOCKIST_ID);
							var htmlText = "<a class='edit btn btn-blue' onClick='viewStockist("+data.STOCKIST_ID+")'  href='javascript:;'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' onClick='editStockistRegDetails("+data.STOCKIST_ID+","+mainIndex+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteStockist("+data.STOCKIST_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> ";
							$('#LINKS'+mainIndex).html(htmlText);
							$('#stockistId'+mainIndex).html(data.STOCKIST_ID);
							$('#location'+mainIndex).html($('input:radio[name=location]:checked').val());
						}
						//$('#modal-responsive').remove();
						
					}else if(data.RESULT==false){
						$('#error').html(data.MSG);
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
//
function editDrugLicenseRegDetails(drugLicenseId,index){
	var html=""
		+ "<div class='modal fade ' id='modal-responsive777' >"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive777\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Outward Return Goods Registration LR Details</strong></h4>"
        + "            </div>"
        + "			       <div class='panel-body'>"
        + "				 <form id='editDrugLicenseRegDetails' action='"+contextApplicationPath+"/StockistController/updateStockistDrugLicenseRegDetails' method='post'>"
        + "    			       <div class='row'>"
        + "       				   <div class='col-md-12'>"
        + "							<input  name='drugLicenseId' type='hidden' value='"+drugLicenseId+"'>"
        +"                                                                    <div class='form-group' style='width: 34%; float: left;'>"
		+"                                                                        <label class='form-label'><strong>Drug License No*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"//DRUG_LICENSE_NO FROM_DATE TO_DATE VALIDITY
		+"                                                                            <input type='text' id='licenseNoId' name='licenseNo' value='"+commonData[index].DRUG_LICENSE_NO+"' placeholder='Drug License No' class='form-control' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"                                                            
		+"                                                                    <div class='form-group' style='width: 33%; float: left;'>"
		+"                                                                        <label class='form-label'><strong>From Date*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input  id='frmDate' name='frmDate' value='"+commonData[index].FROM_DATE+"' class='dateClassCommon form-control' onchange=\"onDateChangeFunction('frm')\" type='text' placeholder='From Date' style='height: 36px; padding-left: 10px;' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group' style='width: 33%; float: left;'>"
		+"                                                                        <label class='form-label'><strong>To Date*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input  id='toDate' name='toDate'  value='"+commonData[index].TO_DATE+"' onchange=\"onDateChangeFunction('to')\" class='dateClassCommon form-control' type='text' placeholder='To Date' style='height: 36px; padding-left: 10px;' required>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group' style='width: 33%; float: left;'>"
		+"                                                                        <label class='form-label'><strong>Validity*</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                          <label id='validitylbl' class='form-label'><strong>"+commonData[index].VALIDITY+"</strong><input id='validitytxt' name='validity' type='hidden' value='"+commonData[index].VALIDITY+"'>"
		+"                                                                          </label>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+ " 				   <input type='hidden' name='LR_REG_ID' value='123'>"
		+"														</div>"
		+"													</div>"
		+"             <div class='modal-footer text-center'>"
		+"                  <button type='Submit' class='btn btn-primary'  onclick='validateFormDetails(\"#editDrugLicenseRegDetails\")'>Update</button>"
        + "                <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "            </div>"
		+" 					</form>"
		+"												</div>";
		 $("#subPopUp").html(html);
		 $(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
		 ajaxEditDrugLicenseRegDetails("editDrugLicenseRegDetails",index);
		 $("#modal-responsive777").addClass("in");
		 $("#modal-responsive777").attr("aria-hidden","false");
		 $("#modal-responsive777").css("display","block");
		
}
function ajaxEditDrugLicenseRegDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						//   
						commonData[mainIndex].DRUG_LICENSE_NO = $('#licenseNoId').val();
						commonData[mainIndex].FROM_DATE = $('#frmDate').val();
						commonData[mainIndex].TO_DATE = $('#toDate').val();
						commonData[mainIndex].VALIDITY = $('#validitytxt').val();
						//alert($('input:radio[name=location]:checked').val());
						$('#DRUG_LICENSE_NO'+mainIndex).html($('#licenseNoId').val());
						$('#FROM_DATE'+mainIndex).html($('#frmDate').val());
						$('#TO_DATE'+mainIndex).html($('#toDate').val());
						$('#VALIDITY'+mainIndex).html($('#validitytxt').val());
						$('#modal-responsive777').remove();
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
function onDateChangeFunction(position) {
	var fromDate;
	var toDate;
	//alert("dateChange");
	fromDate = $("#frmDate").val();
	toDate =  $("#toDate").val();
    if(fromDate!=""&&toDate!=""&&fromDate!=undefined&&toDate!=undefined){
    	if(fromDate>toDate){
    		alert("from date should be less");
    		if(position=='frm')
        		$('#frmDate').val('');
            else if(position=='to')
            	$('#toDate').val('');
    		var html = "<strong>0 year(s) 0 month(s) 0 day(s)</strong><input id='validitytxt' name='validity' type='hidden' value='0 year(s) 0 month(s) 0 day(s)'>";
    		$('#validitylbl').empty();
    		$('#validitylbl').html(html);
    	}else{
    		CalculateDiffOfTwoDates();
    	}
    }
}
//To calulate difference b/w two dates
function CalculateDiffOfTwoDates() {
		var From_date = new Date($("#frmDate").val());
		var To_date = new Date($("#toDate").val());
		var diff_date =  To_date - From_date;
		
		var years = Math.floor(diff_date/31536000000);
		var months = Math.floor((diff_date % 31536000000)/2628000000);
		var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
		var txt = years+" year(s) "+months+" month(s) "+days+" day(s)";	
		var html = "<strong>"+txt+"</strong><input id='validitytxt' name='validity' type='hidden' value='"+txt+"'>";
			
		$('#validitylbl').empty();
		$('#validitylbl').html(html);
		$('#validitytxt').val(txt);
		//alert( years+" year(s) "+months+" month(s) "+days+" and day(s)");
}
//Edit Stockist bank details
function editStockistBankDetails(stockistBankId,index)
{
	var html=""
		+ "<div class='modal fade in' id='modal-responsive' >"
	    + "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Organization</strong></h4>"
		+ "            </div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"													<form id='editStockistBankDetails' action='"+contextApplicationPath+"/StockistBankDetailsController/updateStockistBankDetails' method='post'>"
		+"													  <input type='hidden' name='stockistBankId' value='"+stockistBankId+"'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Organization Name :-</strong>"//ORG_NAME COMPANY STOCKIST_NAME BANK_NAME BRANCH ACC_NO IFSC
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' value='"+ commonData[index].ORG_NAME +"' class='form-control' disabled>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Stockist Name :-</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' value='"+ commonData[index].STOCKIST_NAME +"' class='form-control' disabled>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Branch*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id='bankBranchNameId' value='"+ commonData[index].BRANCH +"' class='form-control' name='branchName' required>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>IFSC Code*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text'  id='bankIfscCodeId' value='"+ commonData[index].IFSC +"' class='form-control' name='ifscCode' required>"
		+"                                                                </div>"
		+"                                                            </div>      "                                                 
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Company Name :-</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' value='"+ commonData[index].COMPANY +"' class='form-control' disabled>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Bank Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id='bankNameId'  value='"+ commonData[index].BANK_NAME +"' class='form-control' name='bankName' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Account No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id='bankAccoutNoId' value='"+ commonData[index].ACC_NO +"' class='form-control' name='accountNo' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+ "            										  <div class='modal-footer text-center'>"
		+ "                										<button type='submit'  class='btn btn-danger'  onclick='validateFormDetails(\"#editStockistBankDetails\")'>Update</button>"
        + "                										<button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "            										  </div>"
		+"													  </form>"
		+ "                                                </div>"
	    + "                                            </div>"
        + "        </div>"
        + "    </div>"
        + "</div>";
		$('#popup').html(html);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
			//loadOrganiztionDropDownForEdit(element.ORG_ID);
		ajaxEditStockistBankDetails('editStockistBankDetails',index);
}
function ajaxEditStockistBankDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						commonData[mainIndex].BANK_NAME = $('#bankNameId').val();
						commonData[mainIndex].BRANCH = $('#bankBranchNameId').val();
						commonData[mainIndex].ACC_NO = $('#bankAccoutNoId').val();
						commonData[mainIndex].IFSC =  $('#bankIfscCodeId').val();
					
						$('#BANK_NAME'+mainIndex).html($('#bankNameId').val());
						$('#BRANCH'+mainIndex).html($('#bankBranchNameId').val());
						$('#ACC_NO'+mainIndex).html($('#bankAccoutNoId').val());
						$('#IFSC'+mainIndex).html($('#bankIfscCodeId').val());
						$('#modal-responsive').remove();
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
	$('#' + FormId).ajaxForm(options);
}
//
function editStockistUploadDocumentForm(index, from){
	var html = ""
		+ "<div class='modal fade in' id='modal-responsive' >"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Update Stockist Upload Document Details</strong></h4>"
        + "            </div>"
        + "												<form id='editStockistDocumentForm' action='"+contextApplicationPath+"/StockistController/updateStockistDocumentInfo' method='post'>"
		+ "                                                <div class='panel-body'>"
		+ "                                                    <div class='row'>"
		+ "                                                        <div class='col-md-4'>"
		+ "															   <input type='hidden' name='stockistDocId' value='"+commonData[index].STOCKIST_DOC_ID+"'>"
		+ "                                                            <div class='form-group'>"
		+ "                                                                <label class='form-label'><strong>Organization Name</strong>"
		+ "                                                                </label>"
		+ "                                                                <span class='tips'></span>"
		+ "                                                                <div class='controls'>"
		+ "                                                                    <input disabled type='text' value='"+commonData[index].ORG_NAME+"' class='form-control'>"
		+ "                                                                </div>"
		+ "                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Stockist*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select  id='StockistName' name='stockistId' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Stockist</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
        +"                                                            <div class='form-group'>"
        +"                                                     		    <button type='button' onclick='editStockistDocImagesListPopup("+commonData[index].STOCKIST_DOC_ID+","+index+")' class='btn btn-success'>Edit Stockist Document Images</button>"
	    +"                                                            </div>"
		+ "                                                        </div>"
		+ "                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Company*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='companyName' name='company' onchange='getStockist()' class='form-control' class='form-control'>"
		+"                                                                        <option disabled selected> Select Company</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+ "                                                            <div class='form-group'>"
		+ "                                                                <label class='form-label'><strong>Document Type*</strong>"
		+ "                                                                </label>"
		+ "                                                                <span class='tips'></span>"
		+ "                                                                <div class='controls'>"
		+ "																		<div id='documentList'></div>"
        + "                                                                </div>"
		+ "                                                            </div>"
		+ "                                                        </div>"
		+ "                                                    </div>"
		+ "                                                    <div class='row'>"
		+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                                                <div class='pull-right'>"
		+ "                                                                    <button class='btn btn-primary m-b-10' onclick='showLoader()'>Submit</button>"
		+ "                                                                    <button type='reset' onclick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
		+ "                                                                </div>"
		+ "                                                            </div>"
		+ "                                                        </div>"
		+ "                                                    </div>"
		+ "                                                </div>"
		+ "												</form>"
		+ "                            </div>"
		+ "                        </div>" 
		+ "                    </div>";
	$('#popup').html(html);
	loadDocumentTypeListForEdit(commonData[index].DOCUMENT_TYPE_ID);
	getCompaniesDropdownForEdit(commonData[index].ORG_ID,commonData[index].COMPANY_ID);
	getStockistForEdit(commonData[index].ORG_ID,commonData[index].COMPANY_ID,commonData[index].STOCKIST_ID);
	ajaxEditStockistDocumentForm("editStockistDocumentForm",index, from);
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block");
}
function ajaxEditStockistDocumentForm(FormId,index, from) {
	var options = {
		success : function(data) {
				alert(data.MSG);
				if(data.RESULT==true){
					commonData[index].DOCUMENT_TYPE_ID = data.DOCUMENT_TYPE_ID;
					commonData[index].DOCUMENT_TYPE = data.DOCUMENT_TYPE;
					commonData[index].COMPANY_NAME = data.COMPANY_NAME;
					commonData[index].COMPANY_ID = data.COMPANY_ID;
					commonData[index].STOCKIST_ID = data.STOCKIST_ID;
					commonData[index].STOCKIST_NAME = data.STOCKIST_NAME;
					displayUploadDocumentListingView(commonData, from, urlForEdit);
					//displayUploadDocumentListing();
				}
				hidePopUp();
				$('#loader').hide();
		}
	};
	$('#' + FormId).ajaxForm(options);
}
//
function editStockistDocImagesListPopup(stockistDocId,index){
	//commonData2 is global var
	commonData2 = index ;
	editListOfImagesPopup(stockistDocId,"documentImage",commonData[index].FILE_NAME,"STOCKIST_UPLOAD_DOCUMENT_IMAGE" , "/StockistController/updatesStockistUploadDocumentImage");
}
//Image updation Ajax call after getting result from server
function ajaxStockistDocumentImageUpdate(data){
	if(data.RESULT==true) {
		commonData[commonData2].FILE_NAME = data.FILE_NAME;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteStockistDocumentImage(stockistDocumentPathId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/StockistController/deleteStockistDocumentFileImage', {
			stockistDocumentPathId : stockistDocumentPathId
		}, function(data) {
			if(data.RESULT==true){
				commonData[commonData2].FILE_NAME[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}