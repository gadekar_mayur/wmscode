function viewOrderEntryRegistrationPopUp(OutWardOrderEntryRegistrationId)
{
		var i=1;
		var j =1;
		var html="" 
						+ "<div class='modal fade in' id='modal-responsive' >"
			            + "    <div class='col-md-13'>"
			            + "        <div class='modal-content'>"
			            + "            <div class='modal-header'>"
			            + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			            + "                <h4 class='modal-title' id='myModalLabel'><strong>View Outward Order Entry Registration Details</strong></h4>"
			            + "            </div>"
			            + "  <div class='panel panel-default'>"
			            + "<div class='panel-body'>"
			            + "    <div class='row'>"
			            + "       <div class='col-md-4'>";
		$.post(contextApplicationPath+'/OutWardGoodsController/ViewAllDetailsOfOutwardGoodsRegistration',{OutWardOrderEntryRegistrationId : OutWardOrderEntryRegistrationId}, function(data) {
			$(data).each(function(index,element){
				//commonData is global var
				commonData = element;
				   html+= "            <div class='form-group'>"
			            + "                <label class='form-label'><strong>Organization Name : - </strong>"
			            + "               </label>"
			            + "				  <label class='form-label'><strong>"+element.ORG_NAME+"</strong>"
			            + "                </label>"
			            + "           </div>"
			            + "           <div class='form-group'>"
			            + "               <label class='form-label'><strong>Company Name : - </strong>"
			            + "               </label>"
			            + "				  <label class='form-label'><strong>"+element.COMP_NAME+"</strong>"
			            + "               </label>"
			            + "                <span class='tips'></span>"
			            + "           </div>"
			            + "           <div class='form-group'>"
			            + "                <label class='form-label'><strong>Mode : - </strong>"
			            + "                </label>"
			            + "				 <label class='form-label'><strong>"+element.MODE+"</strong>"
			            + "                </label>"
			            + "                <span class='tips'></span>"
			            + "           </div>  "
			            + "           <div class='form-group'>"
			            + "               <label class='form-label'><strong>Remark : - </strong>"
			            + "               </label>"
			            + "				   <label class='form-label'><strong>"+element.REMARK+"</strong>"
			            + "               </label>"
			            + "               <span class='tips'></span>"
			            + "            </div>"
			            +"            <div class='form-group'>"
						+"            <label class='form-label'><strong>Order Copy : - </strong>"
						+"				<span class='tips'></span>"
						+"              <div id='imgDiv'>"
						+"              <img onClick='showOutwardOrderCopyImagesListPopup()' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"//<img onClick='showImagePopup(\""+element.ORDER_COPY+"\")' src='"+element.ORDER_COPY+"' alt='Smiley face' height='100' width='100'>"
						+"              </div>"
						+"              </div>";
			          html+= "       </div>"
			            + "       <div class='col-md-4'>"
			            + "            <div class='form-group'>"
			            + "                <label class='form-label'><strong>Stockist : - </strong>"
			            + "               </label>"
			            + "				  <label class='form-label'><strong>"+element.STOCKIST+"</strong>"
			            + "                </label>"
			            + "           </div>"
			            + "          <div class='form-group'>"
			            + "               <label class='form-label'><strong>Date : - </strong>"
			            + "               </label>"
			            + "				 <label class='form-label'><strong>"+element.DATE+"</strong>"
			            + "                </label>"
			            + "               <span class='tips'></span>"
			            + "            </div>"
			            + "            <div class='form-group'>"
			            + "                <label class='form-label'><strong>Order No. : - </strong>"
			            + "               </label>"
			            + "				 <label class='form-label'><strong>"+element.ORDER_NUM+"</strong>"
			            + "                </label>"
			            + "                <span class='tips'></span>"
			            + "           </div>"
			            + "           <div class='form-group'>"
			            + "                <label class='form-label'><strong>Submitted Date : - </strong>"
			            + "                </label>"
			            + "				 <label class='form-label'><strong>"+element.SUBMITTED_DATE+"</strong>"
			            + "               </label>"
			            + "               <span class='tips'></span>"
			            + "           </div>"
			            + " </div>"
			            + " </div>"
			            + "</div>";
			      	var productInfo=element.PRODUCT_ARRAY;
			      	if(productInfo.length > 0)
			      		{
			         html +="                                                <div class='panel-body'>"
					   +"                                                    <div class='row'>"
					   + "                                                      <h5 class='modal-title' id='myModalLabel'><strong>Product Information</strong>"
			            + "            									        </h5>"
					   +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					   +"                                                            <table class='table table-striped table-hover'>"
					   +"                                                                <thead class='no-bd'>"
						+"                                                                    <tr>"
						+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Product Name</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Quantity</strong>"
						+"                                                                        </th>"
						+"                                                                    </tr>"
						+"                                                                </thead>"
						+"                                                                <tbody class='no-bd-y'>";
			      
					 $(productInfo).each(function(index,element){
					html+="                                                                    <tr style='text-align: center;'>"
					+"                                                                        <td>"+ i++ +"</td>"
					+"                                                                        <td>"+element.PRODUCT_NAME+"</td>"
					+"                                                                        <td>"+element.QUANTITY+"</td>"
					+"                                                                    </tr>"
					 });
			html+="                                                                </tbody>"
			+"                                                            </table>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>";
			      		}
	
			      	
			     	var invoiceInfo=element.INVOICE_ARRAY;
			      	if(invoiceInfo.length > 0)
			      		{
			         html +="                                                <div class='panel-body'>"
					   +"                                                    <div class='row'>"
					    + "                                                      <h5 class='modal-title' id='myModalLabel'><strong>Invoice Information</strong>"
			            + "            									        </h5>"
					   +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					   +"                                                            <table class='table table-striped table-hover'>"
					   +"                                                                <thead class='no-bd'>"
						+"                                                                    <tr>"
						+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Invoice Number</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Invoice Date</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Gross Amount</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Tax Amount</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Vat Amount</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Discount</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Net Amount</strong>"
						+"                                                                        </th>"
						+"                                                                    </tr>"
						+"                                                                </thead>"
						+"                                                                <tbody class='no-bd-y'>";
			      
					 $(invoiceInfo).each(function(index,element){
					html+="                                                                    <tr style='text-align: center;'>"
					+"                                                                        <td>"+ j++ +"</td>"
					+"                                                                        <td>"+element.INVOICE_NO+"</td>"
					+"                                                                        <td>"+element.INVOICE_DATE+"</td>"
					+"                                                                        <td>"+element.GROSS_AMOUNT+"</td>"
					+"                                                                        <td>"+element.TAX_AMOUNT+"</td>"
					+"                                                                        <td>"+element.VAT_AMOUNT+"</td>"
					+"                                                                        <td>"+element.DISCOUNT+"</td>"
					+"                                                                        <td>"+element.NET_AMOUNT+"</td>"
					+"                                                                    </tr>"
					 });
			html+="                                                                </tbody>"
			+"                                                            </table>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>";
			      		}
	
		});
			html+= "            <div class='modal-footer text-center'>"
	            + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	            + "            </div>"
	            + "        </div>"
	            + "    </div>"
	            + "</div>";
		$('#popup').html(html);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
	}, 'json');	
}
//
function showOutwardOrderCopyImagesListPopup(){
	showListOfImagesPopup(commonData.ORDER_COPY);
}
function viewDispatchCasesPopUp(OutWardDispatchEnteryRegistrationId)
{
	var j =1;
	var html="" 
					+ "<div class='modal fade in' id='modal-responsive' >"
		            + "    <div class='col-md-13'>"
		            + "        <div class='modal-content'>"
		            + "            <div class='modal-header'>"
		            + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		            + "                <h4 class='modal-title' id='myModalLabel'><strong>View Outward Order Dispatch Entry Registration Details</strong></h4>"
		            + "            </div>"
		            + "  <div class='panel panel-default'>"
		            + "<div class='panel-body'>"
		            + "    <div class='row'>"
		            + "       <div class='col-md-4'>";
	$.post(contextApplicationPath+'/OutWardGoodsController/ViewAllCasesOfDispatchEntryRegistration',{OutWardDispatchEnteryRegistrationId : OutWardDispatchEnteryRegistrationId}, function(data) {
		$(data).each(function(index,element){
			
			   html+= "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Organization Name : - </strong>"
		            + "               </label>"
		            + "				  <label class='form-label'><strong>"+element.ORG_NAME+"</strong>"
		            + "                </label>"
		            + "           </div>"
		            + "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Company Name : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.COMP_NAME+"</strong>"
		            + "                </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>";
		          html+= "       </div>"
		            + "       <div class='col-md-4'>"
		            + "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Stockist : - </strong>"
		            + "               </label>"
		            + "				  <label class='form-label'><strong>"+element.STOCKIST+"</strong>"
		            + "                </label>"
		            + "           </div>"
		            + "         <div class='form-group'>"
		            + "            <label class='form-label'><strong> <font size='4' weight='bold'>Number Of Cases : - </font></strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong> <font size='4' weight='bold'>"+element.NO_OF_CASES+"</font></strong>"
		            + "                </label>"
		            + "               <span class='tips'></span>"
		            + "            </div>"
		            + "           <div class='form-group'>"
		            + "                <label class='form-label'><strong>Submitted Date : - </strong>"
		            + "                </label>"
		            + "				 <label class='form-label'><strong>"+element.SUBMITTED_DATE+"</strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "           </div>"
		            + " </div>"
		            + " </div>"
		            + "</div>";
		      	
		     	var invoiceInfo=element.INVOICE_ARRAY;
		      	if(invoiceInfo.length > 0)
		      		{
		         html +="                                                <div class='panel-body'>"
				   +"                                                    <div class='row'>"
				    + "                                                      <h5 class='modal-title' id='myModalLabel'><strong>Invoice Information</strong>"
		            + "            									        </h5>"
				   +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
				   +"                                                            <table class='table table-striped table-hover'>"
				   +"                                                                <thead class='no-bd'>"
					+"                                                                    <tr>"
					+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Invoice Number</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Order No</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Order Id</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Net Amount</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Order Date</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Invoice Date</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Day's Count</strong>"
					+"                                                                        </th>"
					+"                                                                    </tr>"
					+"                                                                </thead>"
					+"                                                                <tbody class='no-bd-y'>";
		      
				 $(invoiceInfo).each(function(index,element){
				html+="                                                                    <tr style='text-align: center;'>"
				+"                                                                        <td>"+ j++ +"</td>"
				+"                                                                        <td>"+element.INVOICE_NO+"</td>"
				+"                                                                        <td>"+element.ORDER_NO+"</td>"
				+"                                                                        <td>"+element.ORDER_ID+"</td>"
				+"                                                                        <td>"+element.NET_AMOUNT+"</td>"
				+"                                                                        <td>"+element.ORDER_DATE+"</td>"
				+"                                                                        <td>"+element.INVOICE_DATE+"</td>"
				+"                                                                        <td>"+element.DAYS_COUNT+"</td>"
				+"                                                                    </tr>"
				 });
		html+="                                                                </tbody>"
		+"                                                            </table>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>";
		      		}

	});
		html+= "            <div class='modal-footer text-center'>"
            + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
            + "            </div>"
            + "        </div>"
            + "    </div>"
            + "</div>";
	$('#popup').html(html);
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block"); 
}, 'json');	

}