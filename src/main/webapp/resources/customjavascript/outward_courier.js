function AddOutwardCourier() {
	var html = ""
			+ "<div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                    <h3><strong>Outward Courier Registration</strong></h3>"
			+ "                </div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-6'>"
			+ "                        <div class='tabcordion'>"
			+ "                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
			+ "                                <li class='active'><a href='#tab1_1' data-toggle='tab' onclick='outwardCourierRegistration()'>Outward Courier Registration</a></li>"
			+ "                                <li class=''><a href='#tab1_2' data-toggle='tab' onclick='outwardCourierRegistrationListing()'>Outward Courier Registration View</a></li>"
			+ "                            </ul>"
			+ "                            <div id='myTabContent' class='tab-content'>"
			+ "                            </div>"
			+ "                        </div>" + "                    </div>"
			+ "                </div>";
	$('#main-content').html(html);
	$('#subMenu' + lastClickId).hide();
	outwardCourierRegistration();
}

function outwardCourierRegistration(){
	var html=""
		+"<form id='outwardCourierReg' action='"+contextApplicationPath+"/OutwardCourierController/saveOutwardCourierRegistration' method='post'>"
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>Outward Courier Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-12'>"
		+"                            <div class='panel panel-default'>"
		+"                                <div class='panel-body'>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-4'>"
		+ "                                           <div id='selectOrganization' class='form-group' required></div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Date</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input  type='text' name='courierRegDate' id='date' class='dateClassCommon form-control'  placeholder='Select Date' style='height: 36px; padding-left: 10px;' onchange=\"errorMsgEmpty('date','dateError_msg')\">"
		+"												 			 <div id='dateError_msg' class='validationError' style='color:red'></div>"
		+"                                                </div>"
		+"                                            </div>"
		+ "                                           <div class='form-group'>"
		+ "                                                <label class='col-lg-12 control-label' style='padding-left: 0px;'><strong></strong></label>"
		+ "                                                <div class='col-lg-12'>"
		+ "                                                    <div class='pos-rel'>"
		+ "                                                        <input tabindex='13' id='ctest' type='radio' checked='checked' onchange='showStockistDropdownOnRadioButton(1)' name='companyStockistOption' value='Company' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;'>"
		+ "                                                        <label class='p-l-40' for='flat-checkbox-1'>Company</label>"
		+ "                                                    </div>"
		+ "                                                    <div class='pos-rel'>"
		+ "                                                        <input tabindex='14' id='ctest' type='radio' onchange='showStockistDropdownOnRadioButton(2)' name='companyStockistOption' value='Stockist' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;'>"
		+ "                                                        <label class='p-l-40' for='flat-checkbox-2'>Stockist</label>"
		+ "                                                    </div>"
		+ "                                                </div>"
		+ "                                           </div>"
		+ "                                           <div class='form-group'>"
		+ "                                                <label class='form-label'><strong>Select Company</strong>"
		+ "                                                </label>"
		+ "                                                <span class='tips'></span>"
		+ "                                                <div class='controls'>"
		+ "                                                    <select id='companyName' name='companyName' onchange='onCompanyChange()' class='form-control' class='form-control' required>"
		+ "                                                        <option disabled selected> Select Company</option>"
		+ "                                                    </select>"
		+ "                                                </div>"
		+ "                                           </div>"
		+ "                                          <div id='selectStockist' class='form-group'></div>"
		+ "                                          <div id='stockistError_msg' class='validationError' style='color:red'></div>"
		+"                                           <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Sending to Depo</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <select name='depoId' id='companyDepo' class='form-control' class='form-control'>"
		+"                                                        <option disabled selected>Select Depo</option>"
		+"                                                    </select>"
		+"                                                </div>"
		+"                                           </div>"
		/*+ "                                          <div class='form-group'>"
		+ "                                               <label class='form-label'><strong>State</strong>"
		+ "                                               </label>"
		+ "                                               <span class='tips'></span>"
		+ "                                               <div class='controls'>"
		+ "                                               	<select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control' required>"
		+ "														<option disabled selected> Select State</option>"
		+ "													</select>"
		+ "                                                </div>"
		+ "                                           </div>"
		+ "                                           <div class='form-group' style='height: 70px;'>"
		+ "                                            	<label class='form-label'><strong>District</strong>"
		+ "                                              </label>"
		+ "                                              <span class='tips'></span>"
		+ "                                              <div class='controls'>"
		+ "                                              	<select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control' required>"
		+ "                                                  	<option disabled selected> Select District</option>"
		+ "                                                  </select>"
		+ "                                              </div>"
		+ "                                           </div>"
		+ "                                           <div class='form-group'>"
		+ "                                                <label class='form-label'><strong>City</strong>"
		+ "                                                </label>"
		+ "                                                <span class='tips'></span>"
		+ "                                                <div class='controls'>"
		+ "                                                    <select name='city' id='cityName' class='form-control' required>"
		+ "                                                        <option disabled selected> Select City</option>"
		+ "                                                    </select>"
		+ "                                                </div>"
		+ "                                           </div>"*/
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Particulars</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+ "                                                    <select name='particular' id='selectParticular' class='form-control' class='form-control' required>"//onchange='showPopUpDiv()' for cheque
		+ "                                                        <option disabled selected>Select Particulars</option>"
		+ "                                                    </select>"
		+"                                                </div>"
		+"                                            </div>"
		//pop up
		+ "                                           <div id='showPopUp' class='form-group'></div>"
		// /pop up
		+"                                        </div>"
		+"                                        <div class='col-md-4'>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Courier Name</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='courierName' placeholder='Courier Name' class='form-control' required>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Docket No</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                <input type='text' name='docketNo'  placeholder='Docket No' class='form-control'>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Weight</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='weight' placeholder='Weight' class='form-control'>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Quantity (BOX)</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='quantity' placeholder='Quantity Per Box' class='form-control' parsley-type='onlynumber' required>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Delivery Person</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='deliveryPerson' placeholder='Delivery Person' class='form-control' required>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Courier Person</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='courierPerson' placeholder='Courier Person' class='form-control' required>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Time</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='time' id='timePicker' placeholder='Time' class='form-control' focusout=\"errorMsgEmpty('timePicker','timeError_msg')\">"
		+"                                                </div><div id='timeError_msg' class='validationError' style='color:red'></div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Remark</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='textarea' name='remark' placeholder='Remark' class='form-control'>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                <div class='pull-right'>"
		+"                                                    <button class='btn btn-primary m-b-10' onclick='return validateOutwardCourierForm(\"#outwardCourierReg\")'>Save</button>"
		/*+"                                                    <button class='btn btn-success m-b-10' onclick='validateFormDetails(\"#outwardCourierReg\")'>Save & Mail</button>"*/
		//+"                                                    <button class='btn btn-success m-b-10' onclick='javascript:$('#form1').parsley('validate');'>Save & Mail</button>"
		+"                                                    <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>    "
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>"
		+"</form>"
		+"				  <div id='outwardCourierListing'>"
		+"				  </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	$(".dateClassCommon").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	$('#timePicker').timepicki();
	displayOrganizationDropDown(1);
	//stateDropDownList();
	perticularsDropDownList();
	ajaxAddOutwardCourierRegistration('outwardCourierReg');
}
//ajax call
function ajaxAddOutwardCourierRegistration(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.result==true){
					$('#'+ formId)[0].reset();
					$("#stockistError_msg").html("");
				}
				alert(data.MSG);
			}
	};
	$('#outwardCourierReg').ajaxForm(options);
}
var stockistDropDownExistStatus = false;
function showStockistDropdownOnRadioButton(index){
	stockistDropDownExistStatus = false
	if(index==1)
		stockistDropDownExistStatus = false;
	else if(index==2)
		stockistDropDownExistStatus = true;
	showStockistDropdownList(index);
	$("#stockistError_msg").html("");
}
function validateOutwardCourierForm(formId){
	var z = $(formId).parsley('validate');
	if($("#timePicker").val()==""||$("#timePicker").val()==undefined||$("#date").val()==""||$("#date").val()==undefined){
		var flag =  0;
		var flag=false;
		if($("#timePicker").val()==""||$("#timePicker").val()==undefined){
			$("#timeError_msg").html("This value is required.");flag=true;
		}
		if($("#date").val()==""||$("#date").val()==undefined){
			$("#dateError_msg").html("This value is required.");flag=true;
		}
		if(flag){
			$('#loader').hide();
			return false;
		}
		return true;
	}

	if(z==true)
		{showLoader();
		if(stockistDropDownExistStatus){
			if($("#StockistName").val()=="Select Stockist"||$("#StockistName").val()==""||$("#StockistName").val()==undefined){
				$("#stockistError_msg").html("This value is required.");
				$('#loader').hide();
				return false;
			}
		}
		}
	
}
//
function onCompanyChange(){
	$('#selectParticular').prop('selectedIndex',0);
	hideDiv(2);
	getStockist();
	getCompanyDepo();
}
//
function onCompanyChangeForEdit(flag){
	$('#selectParticular').prop('selectedIndex',0);
	hideDiv(2);
	if(flag)
		getStockist();
	getCompanyDepo();
}
//get company depo
function getCompanyDepo(){
	//alert("getStockist");
	var companyId = $('#companyName').val();
	if(companyId==null||companyId==undefined)
		companyId=0;
	$.post(contextApplicationPath+'/CompanyController/companyDepoDropdown', {
		companyId : companyId
	}, function(data) {
		//$('#loader').hide();
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Depo</option>";
		for (counter = 0; counter < data.companyDepoArr.length; counter++) {
			optionHtml += "<option value='" + data.companyDepoArr[counter].DEPO_ID + "'>"
			+ data.companyDepoArr[counter].DEPO_NAME + "</option>";
		}
		$("#companyDepo").empty();
		$("#companyDepo").append(optionHtml);
	}, 'json');
}
function showPopUpDiv() {
	var selectId = $('#selectParticular').val();
	
	 if(selectId==3){
		 showPopUpForCourierParticulars();
		 var flag = organizationBankDropDownList();
		 if(flag){
			 $("#modal-responsive250").addClass("in");
			 $("#modal-responsive250").attr("aria-hidden","false");
			 $("#modal-responsive250").css("display","block");
		 }else{
			 //$('#selectParticular').prop('selectedIndex',0);
			 hideDiv(2);
		 }
	 }else{
		 hideDiv(2);
	 }
	 
}
function organizationBankDropDownList(){
	var orgId = $('#OrgName').val();

	if(orgId==null){
		alert("Please Select Organization...!");
		return false;
	}else{
			//alert("orgId="+orgId);
			$.post(contextApplicationPath+ '/OrganizationController/organiztionBankDropdown',{
				orgId : orgId,
			},function(data) {
						var optionHtml = "<option disabled selected> Select Bank</option>";
						for (var i = 0; i < data.orgBankArray.length; i++) {
							optionHtml += "<option value='"+ data.orgBankArray[i].ORG_BANK_ID+ "'>"
									+ data.orgBankArray[i].BANK_NAME
									+ "</option>";
						}
						$("#selectCompanyBank").empty();
						$("#selectCompanyBank").append(optionHtml);
					}, 'json');
			return true;
	}
}
//Outward Courier Registration Listing
function outwardCourierRegistrationListing(){
	$('#loader').show();
	var i=1;
//	$.post(contextApplicationPath+'/OutwardCourierController/outwardCourierRegistrationListing', function(data) {
	var html=""
		+"                <div class='row'>"
		+"                    <div class='col-md-12'>"
		+"                        <div class='panel panel-default'>"
		+"                            <div class='panel-body'>"
		+"                                <div class='row'>"
		+"                                    <div class='col-md-4'>"
		+"                                        <div class='form-group'>"
		+"                                            <label class='form-label'><strong>Search By</strong>"
		+"                                            </label>"
		+"                                            <span class='tips'></span>"
		+"                                            <div class='controls'>"
		+"													<select id='searchOption' class='form-control' class='form-control' onchange='changeFuncOutward();'>"
		+"														  <option value='Organization'>Select Organization</option>"
		+ "                                                       <option value='Company'>Comapny Name</option>"
		+ "                                                       <option value='Date'>Search By Registration Date</option>"
		+ "                                                       <option value='Stockist'>Stockist</option>"
		+ "                                                       <option value='SendingToDepot'>Sending To Depot</option>"
		+"														  <option value='CourierName'> Courier Name  </option>"
		+ "                                                       <option value='DocketNo'> Docket No.</option>"
		+ "                                                       <option value='DeliveryPerson'>Delivery Person  </option>"
		+ "                                                       <option value='CourierPerson'>Courier Person  </option>"
		//+ "                                                       <option value='Weight'>Weight  </option>"
		+ "                                                       <option value='Quantity'>Quantity  </option>"
		//+ "                                                       <option value='CHEQUENO'>CHEQUE NO </option>"
		//+ "                                                       <option value='BANKNAME'>BANK NAME </option>"
		+ "                                                       <option value='State'>State </option>"
		+ "                                                       <option value='district'>District</option>"
		+ "                                                       <option value='City'>City </option>"
		+ "                                                     </select>"		
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div class='col-md-4'>"
		+"                                        <div class='form-group'>"
		+"                                            <label class='form-label'><strong>Search For</strong>"
		+"                                            </label>"
		+"                                            <span class='tips'></span>"
		+"                                            <div class='controls' id='searchControl'>"
		+"                                                <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div class='col-sm-9 col-sm-offset-3'>"
		+"                                        <div class='pull-right'>"
		+"                                            <button class='btn btn-success m-b-10' onclick='searchoutwardCourierRegistration()'>Show</button>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>"
		+"                            </div>"
		+"                            <div class='row'>"
		+"                                <div class='col-md-6'>"
		+"                                    <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                    </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                                </div>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html);
		commonOutwardCourierRegistrationController("", "", "", "", 1);
//		showoutwardCourierRegistrationListing(data);//to show dynamic data by another function 
//		}, 'json');
}

function commonOutwardCourierRegistrationController(searchOption, searchText, fromSpecialData, toSpecialData, from)
{
	showLoader();
	$.post(contextApplicationPath+'/OutwardCourierController/searchoutwardCourierRegistration',
			{
		searchOption:searchOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		from : from
		}, function(data) {
			var url = "commonOutwardCourierRegistrationController(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showoutwardCourierRegistrationListing(data, from, url); // to call the function which shows the searched item
			$('#loader').hide();
//		$('#searchResult').html(html);
	}, 'json');
}


//search Funcationality 
//hello
function changeFuncOutward()
{
	var selectedValue=$('#searchOption').val();
	var html="";
	
		if(selectedValue=="Weight" || selectedValue=="Quantity")
		{
    		html+="<input id='inputFromData3' type='text' class='form-control' placeholder='From Amount' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTOData3' type='text' class='form-control' placeholder='To Amount' style='width: 49%;'>";
		}
    	else if(selectedValue=="Date")
    	{
    		html+="<input id='inputFromDate' type='text' class='dateClassCommon form-control' placeholder='From Date' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTODate' type='text' class='dateClassCommon form-control' placeholder='To Date' style='width: 49%;'>";
    	}
    	else
    	{
    		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control'>";
    		
    		
    		
    		
    		
    		
    	} 	
		$("#searchControl").html(html);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
}

function searchoutwardCourierRegistration()
{
	var searchOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	if(searchOption=="Weight" || searchOption=="Quantity")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (searchOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonOutwardCourierRegistrationController(searchOption, searchText, fromSpecialData, toSpecialData, 1);
//	$.post(contextApplicationPath+'/OutwardCourierController/searchoutwardCourierRegistration',
//		{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData
//	}, function(data) {
//		showoutwardCourierRegistrationListing(data) // to call the function which shows the searched item
//	$('#searchResult').html(html);
//}, 'json');
}
//using this function to show dynamic data at first time and after search
function showoutwardCourierRegistrationListing(data, from, url)
{
	var i=0;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html=""
		+"                                        <table class='table table-striped table-hover'>"
		+"                                            <thead class='no-bd'>"
		+"                                                <tr>"
		+"                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>COMPANY</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>STOCKIST</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>COURIER NAME</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>CITY</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>DATE</strong>"
		+"                                                    </th>"
	    +"                                                    <th style='text-align: center;'><strong>DOCKET NO</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>Action</strong>"
		+"                                                    </th>"
		+"                                                </tr>"
		+"                                            </thead>"
		+"                                            <tbody class='no-bd-y'>"
		$(data).each(function(index, element) {
			if((data.length-2)<i)
				return false;
			html+="                                                <tr style='text-align: center;'>"
				+"                                                    <td>"+SR_NO+"</td>"
				+"                                                    <td id='REG_DATE_"+i+"'>"+element.ORG+"</td>"
				+"                                                    <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
				+"                                                    <td id='STOCKIST_NAME_"+i+"'>"+element.STOCKIST_NAME+"</td>"
				+"                                                    <td id='COURIER_NAME_"+i+"'>"+element.COURIER_NAME+"</td>"
				+"                                                    <td id='CITY_"+i+"'>"+element.CITY+"</td>"
				+"                                                    <td id='REG_DATE_"+i+"'>"+element.REG_DATE+"</td>"
				+"                                                    <td id='DOCKET_NO_"+i+"'>"+element.DOCKET_NO+"</td>"
				+"                                                    <td><a class='edit btn btn-blue' onClick='viewOutwardCourierRegistration("+element.COURIER_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' onClick='editOutwardCourierRegistration("+i+","+element.COURIER_ID+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteOutwardCourierRegistration("+element.COURIER_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
				+"                                                </tr>";
			i++;
			SR_NO++;
			});
		+"                                            </tbody>"
		+"                                        </table>";
		$('#searchResult').html(html);
		 paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
		$('#loader').hide();
}

//VIEW Outward Courier Registration
function viewOutwardCourierRegistration(outwardCourierRegId){
	 viewOutwardCourierRegistrationPopUp(outwardCourierRegId);
}
//Edit Outward Courier Registration
function editOutwardCourierRegistration(mainIndex,outwardCourierRegId){
	//alert(outwardCourierRegId);
	$.post(contextApplicationPath+'/OutwardCourierController/ViewAllDetailsOfOutwardCorierRegistration',{outwardCourierRegId : outwardCourierRegId, editStatus : true}, function(data) {
		$(data).each(function(index,element){
		var html = ""
			+"<form id='outwardCourierEdit' action='"+contextApplicationPath+"/OutwardCourierController/editOutwardCourierRegistration' method='post' enctype='multipart/form-data'>"
			+ "<div class='modal fade in' id='modal-responsive' >"
	        + "    <div class='col-md-13'>"
	        + "        <div class='modal-content'>"
	        + "            <div class='modal-header'>"
	        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	        + "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Outward Courier Goods</strong></h4>"
	        + "            </div>"
			+ "                                <div class='panel-body'>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-4'>"
			+"											  <input type='hidden' name='courierId' value='"+outwardCourierRegId+"'>"
			//+"											  <input type='hidden' name='option' value='"+element.OPTION+"'>"
			+"                                            <div id='selectOrganization' class='form-group'>"
			+"                                                <label class='form-label'><strong>Organization Name: -</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                   <label class='form-label'><strong>"+element.ORG_NAME+"</strong><input type='hidden' id='OrgName' name='orgName' value='"+element.ORG_ID+"'>"
			+"                                                   </label>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Date</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                    <input  type='text' name='courierRegDate' value='"+element.DATE+"' class='dateClassCommon form-control'  placeholder='Select Date' style='height: 36px; padding-left: 10px;' required>"
			+"                                                </div>"
			+"                                            </div>";
			//if(element.OPTION=="NoCheque"){
			html+="                                           <div class='form-group'>"
				+ "                                                <label class='form-label'><strong>Select Company</strong>"
				+ "                                                </label>"
				+ "                                                <span class='tips'></span>"
				+ "                                                <div class='controls'>"
				if(element.STOCKIST_ID!=null||element.STOCKIST_ID!=undefined)
					html+="                                                    <select id='companyName' name='companyName' onchange='onCompanyChangeForEdit(true)' class='form-control' class='form-control'>";
				else
					html+="                                                    <select id='companyName' name='companyName' onchange='onCompanyChangeForEdit(false)' class='form-control' class='form-control'>";
			html+="                                                        <option disabled selected> Select Company</option>"
				+ "                                                    </select>"
				+ "                                                </div>"
				+ "                                           </div>";
			/*}else{
				html+="                                           <div class='form-group'>"
					+ "                                                <label class='form-label'><strong>Company Name :- </strong>"
					+ "                                                </label>"
					+ "                                                <span class='tips'></span>"
					+ "                                                <div class='controls'>"
					+ "                                                   <label class='form-label'><strong>"+element.COMP_NAME+"</strong><input type='hidden' name='companyName' value='"+element.COMPANY_ID+"'>"
					+ "                                                   </label>"
					+ "                                                </div>"
					+ "                                           </div>";
			}*/
			//if(element.OPTION=="NoCheque"&&(element.STOCKIST_ID!=null||element.STOCKIST_ID!=undefined)){
			if(element.STOCKIST_ID!=null||element.STOCKIST_ID!=undefined){
				html+="                                           <div class='form-group'>"
					+ "                                                <label class='form-label'><strong>List of Company Stockist</strong>"
					+ "                                                </label>"
					+ "                                                <span class='tips'></span>"
					+ "                                                <div class='controls'>"
					+ "                                                    <select id='StockistName' name='stockistId' onchange='onStockistChange()'  class='form-control' class='form-control' required>"
					+ "                                                        <option disabled selected> Select Stockist</option>"
					+ "                                                    </select>"
					+ "                                                </div>"
					+ "                                           </div>";
				}/*else if(element.STOCKIST_ID!=null||element.STOCKIST_ID!=undefined){
					html+="                                           <div class='form-group'>"
						+ "                                                <label class='form-label'><strong>Stockist Name :- </strong>"
						+ "                                                </label>"
						+ "                                                <span class='tips'></span>"
						+ "                                                <div class='controls'>"
						+ "                                                   <label class='form-label'><strong>"+element.STOCKIST+"</strong><input type='hidden' name='stockistId' value='"+element.STOCKIST_ID+"' required>"
						+ "                                                   </label>"
						+ "                                                </div>"
						+ "                                           </div>";
				}*/
		html+="                                           <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Sending to Depo</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                    <select name='depoId' class='form-control' id='companyDepo' class='form-control'>"
			+"                                                        <option disabled selected>Select Depo</option>"
			+"                                                    </select>"
			+"                                                </div>"
			+"                                           </div>"
			/*+ "                                          <div class='form-group'>"
			+ "                                               <label class='form-label'><strong>State</strong>"
			+ "                                               </label>"
			+ "                                               <span class='tips'></span>"
			+ "                                               <div class='controls'>"
			+ "                                               	<select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control' required>"
			+ "														<option disabled selected> Select State</option>"
			+ "													</select>"
			+ "                                                </div>"
			+ "                                           </div>"
			+ "                                           <div class='form-group' style='height: 58px;'>"
			+ "                                            	<label class='form-label'><strong>District</strong>"
			+ "                                              </label>"
			+ "                                              <span class='tips'></span>"
			+ "                                              <div class='controls'>"
			+ "                                              	<select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control' required>"
			+ "                                                  	<option disabled selected> Select District</option>"
			+ "                                                  </select>"
			+ "                                              </div>"
			+ "                                           </div>"
			+ "                                           <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>City</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <select name='city' id='cityName' class='form-control' required>"
			+ "                                                        <option disabled selected> Select City</option>"
			+ "                                                    </select>"
			+ "                                                </div>"
			+ "                                           </div>"*/;
			//if(element.OPTION== "NoCheque"){
			html+="                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Particulars</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+ "                                                    <select name='particular' id='selectParticular' class='form-control' class='form-control' required>"//onchange='showPopUpDiv()'
			+ "                                                        <option disabled selected>Select Particulars</option>"
			+ "                                                    </select>"
			+"                                                </div>"
			+"                                            </div>"
			//pop up
			+ "                                           <div id='showPopUp' class='form-group'></div>";
			// /pop up
			/*}else{
			html+="                                            <div class='form-group'>"
				+"                                                <label class='form-label'><strong>Particulars</strong>"
				+"                                                </label>"
				+"                                                <span class='tips'></span>"
				+"                                                <div class='controls'>"
				+ "                                                   <label class='form-label'><strong>"+element.PRTICULARS+"</strong><input type='hidden'  name='particular' value='"+element.PRTICULARS_ID+"'>"
				+ "                                                   </label>"
				+"                                                </div>"
				+"                                            </div>";
			}*/
		html+="                                        </div>"
			+"                                        <div class='col-md-4'>"
			+"                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Courier Name</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                    <input type='text' name='courierName' value='"+element.COURIER_NAME+"' placeholder='Courier Name' class='form-control' required>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Docket No</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                <input type='text' name='docketNo' value='"+element.DOCKET_NUM+"' placeholder='Docket No' class='form-control'>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Weight</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                    <input type='text' name='weight' value='"+element.WEIGHT+"' placeholder='Weight' class='form-control' parsley-type='onlynumber'>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Quantity (BOX)</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                    <input type='text' name='quantity' value='"+element.QUANTITY+"' placeholder='Quantity Per Box' class='form-control' parsley-type='onlynumber' required>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Delivery Person</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                    <input type='text' name='deliveryPerson' value='"+element.DELIVERY_PERSON+"' placeholder='Delivery Person' class='form-control' required>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Courier Person</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                    <input type='text' name='courierPerson' value='"+element.CORIER_PERSON+"' placeholder='Courier Person' class='form-control' required>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Time</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                    <input type='text' name='time' value='"+element.TIME+"' id='timePicker' placeholder='Time' class='form-control' required>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                            <div class='form-group'>"
			+"                                                <label class='form-label'><strong>Remark</strong>"
			+"                                                </label>"
			+"                                                <span class='tips'></span>"
			+"                                                <div class='controls'>"
			+"                                                    <input type='textarea' value='"+element.REMARK+"' name='remark' placeholder='Remark' class='form-control'>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                        </div>"
			+"                                    </div>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                            <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                <div class='pull-right'>"
			+"                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#outwardCourierEdit\")'>Save</button>"
			+"                                                    <button type='reset' onclick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                        </div>"
			+"                                    </div>"
			+ "                                </div>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>"
			+ "</form>";
		$('#popup').html(html);
		ajaxEditOutwardCourierRegistration(mainIndex,"outwardCourierEdit");
		$(".dateClassCommon").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
		$('#timePicker').timepicki();
		getCompanyDepoDropdownForEdit(element.COMPANY_ID,element.DEPOT_ID);
		//if(element.OPTION== "NoCheque"||element.OPTION== "NoCheque"){
			getCompaniesDropdownForEdit(element.ORG_ID,element.COMPANY_ID);
			perticularsDropDownListForEdit(element.PRTICULARS_ID);
		//}
		//if(element.OPTION== "NoCheque"&&(element.STOCKIST_ID!=null||element.STOCKIST_ID!=undefined)){
			if(element.STOCKIST_ID!=null||element.STOCKIST_ID!=undefined){
			getStockistForEdit(element.ORG_ID,element.COMPANY_ID,element.STOCKIST_ID);
		}
		//stateDropDownListForEdit(element.STATE_ID);
		//getDistrictForEdit(element.DISTRICT_ID,element.STATE_ID);
		//getCitiesForEdit(element.DISTRICT_ID,element.CITY_ID);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
		});
	}, 'json');
}
//ajax call
function ajaxEditOutwardCourierRegistration(mainIndex,formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.RESULT==true){
					$('#REG_DATE_'+mainIndex).html(data.REG_DATE);
					$('#COURIER_NAME_'+mainIndex).html(data.COURIER_NAME);
					$('#COMPANY_'+mainIndex).html(data.COMPANY);
					$('#STOCKIST_NAME_'+mainIndex).html(data.STOCKIST_NAME);
					$('#CITY_'+mainIndex).html(data.CITY);
					$('#DOCKET_NO_'+mainIndex).html(data.DOCKET_NO);
					alert(data.MSG);
					hidePopUp();
				}
				else{
					alert(data.MSG);
				}
			}
	};
	$('#'+formId).ajaxForm(options);
}
//Delete Outward Courier Registration
function deleteOutwardCourierRegistration(outwardCourierRegId){
	//alert("Delete = "+outwardCourierRegId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OutwardCourierController/deleteOutwardCourierRegistration', {
			outwardCourierRegId : outwardCourierRegId
		}, function(data) {
			outwardCourierRegistrationListing();
			alert(data.MSG);
		}, 'json');
    }
}