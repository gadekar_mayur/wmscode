function validateFormDetails(formId)
{
	var z = $(formId).parsley('validate');
//	var z =  new parsley$(formId);
	
	if(commonData==true)
		{
		alert("Order No. Already Exist");
		//return false;
		}
	else
		{
		//alert("success");
		return true;
		}
	
var flag =  0;
	
	if($("#timePicker").val()==""||$("#timePicker").val()==undefined||$("#date").val()==""||$("#date").val()==undefined){
		var flag=false;
		if($("#timePicker").val()==""||$("#timePicker").val()==undefined){
			$("#timeError_msg").html("This value is required.");flag=true;
		}
		if($("#date").val()==""||$("#date").val()==undefined){
			$("#dateError_msg").html("This value is required.");flag=true;
		}
		if(flag){
			$('#loader').hide();
			return false;
		}
		return true;
	}

	if(z==true)
		{
		showLoader();
		}
}
//
function showImagePopup(path){
	var ext = path.split('.').pop().toLowerCase();
	var html="" 
		+ "<div class='modal fade ' id='ProductImagePopup7123' style='z-index:10000;'>"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hideSubPopUp(\"ProductImagePopup7123\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>File Details</strong></h4>"
        + "            </div>"
        + "			       <div class='panel-body'>"
        + "    			        <div class='row'>"
        +" 							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"								<table class='table table-striped table-hover'>"
		+"									<tbody class='no-bd-y'>"
		+"                                      <tr style='text-align: center;'>"
		+"                                         	<td>";
													if(ext=="pdf"||ext=="PDF")
														html+="<object data='"+path+"' alt='Smiley face' height='480px' width='100%'></object>";
													else
														html+="<span style='height:480px; width:100%'><img style='max-height:480px;max-width:100%;' src='"+path+"' alt='Smiley face' ></span>";
	html+="											</td>"
		+"										</tr>"		
		+ "									</tbody>"
		+"								</table>"
		+"							</div>"
		+ "						</div>"
        + "            		</div>"
        + "        </div>"
        + "    </div>"
        + "</div>";
	 $("#showImagePopup").html(html);
	 $("#ProductImagePopup7123").addClass("in");
	 $("#ProductImagePopup7123").attr("aria-hidden","false");
	 $("#ProductImagePopup7123").css("display","block");
}
//
function showListOfImagesPopup(pathList){
	
	var html="" 
		+ "<div class='modal fade ' id='ProductImagePopup7' style='z-index:10000;'>"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hideSubPopUp(\"ProductImagePopup7\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>List of Files Details</strong></h4>"
        + "            </div>"
        + "			       <div class='panel-body'>"
        + "    			        <div class='row'>"
        +" 							<div class='col-md-12 col-sm-12 col-xs-12'>"
		+"								<table class='table table-striped table-hover'>"
		+"									<tbody class='no-bd-y'>"
		+"                                      <tr>"
		+"                                         	<td>"
		+"                                          	<span style='height:480px; width:100%'>";
														$(pathList).each(function(index, element) {
															if(element.IMAGE_PATH!=""){
																html+="<img style='float: left; width: 150px; height: 150px;padding: 6px;' src='"+element.IMAGE_PATH+"' onclick='showImagePopup(\""+element.IMAGE_PATH+"\")' alt='Smiley face' >" ;
															}
														});
	html+="  											</span>"
		+"											</td>"
		+"										</tr>"		
		+ "									</tbody>"
		+"								</table>"
		+"							</div>"
		+ "						</div>"
        + "            		</div>"
        + "        </div>"
        + "    </div>"
        + "</div>";
	 $("#showEditImagesPopup").html(html);
	 $("#ProductImagePopup7").addClass("in");
	 $("#ProductImagePopup7").attr("aria-hidden","false");
	 $("#ProductImagePopup7").css("display","block");
	
}
//this function is used to show list  of uploaded documents , delete that document and update or upload new docs (this function is common and used at many places)
function editListOfImagesPopup(registrationId,fileInputName,pathList,imageCatagory,updateImgController){
	//registrationId==>To which record we are attaching the multiple images (ex.InwardGoodsRegId,OutwardGoodsRegId...etc..)
	var i = 0;
	var html="" 
		+ "<div class='modal fade ' id='editImagePopup7' style='z-index:10000;'>"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='X' onClick='hideSubPopUp(\"editImagePopup7\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>List of Files Details</strong></h4>"
        + "            </div>"
        + "			       <div class='panel-body'>"
        + "					  <form id='commonImageUpdationForm' action='"+contextApplicationPath + updateImgController +"' method='post' enctype='multipart/form-data'>"
        + "    			        <div class='row'>"
        + "						  <input type='hidden' name='registrationId' value='"+registrationId+"'>"
        +" 							<div class='col-md-12 col-sm-12 col-xs-12'>";
		if(pathList.length>0){
	html+="								<table class='table table-striped table-hover'>"
		+"									<tbody class='no-bd-y'>"
		+"                                      <tr>"
		+"                                         	<td>"
		+"                                          	<span style='height:480px; width:100%'> ";
														$(pathList).each(function(index, element) {
															if(element.IMAGE_PATH!=""){
															html+="<span id='spanId_"+i+"' style='float: left;position: relative;width: 150px;' ><img style='float: left; width: 150px; height: 150px;padding: 6px;' src='"+element.IMAGE_PATH+"' onclick='showImagePopup(\""+element.IMAGE_PATH+"\")' alt='Smiley face' ><a class='delete btn btn-danger' onclick='deleteDocumentFile(\""+imageCatagory+"\","+i+","+element.IMAGE_ID+")' style='height: 39px;left: 103px;margin-left: 3px;margin-top: 10px;position: relative;right: 0;text-align: right;top: -162px;width: 55px;z-index: 1;background-color: transparent !important; background-image: none !important; border-color: transparent !important;' title='Delete Document'><i class='fa fa-times-circle' style='margin-left: -3px; margin-top: 3px;'></i> </a>"
																+"<input type='hidden' name='imageId' value='"+element.IMAGE_ID+"'></span>";
															}
													i++;});
	html+="  											</span>"
		+"											</td>"
		+"										</tr>"		
		+ "									</tbody>"
		+"								</table>";
		}
	html+="                          <div id='"+fileInputName+"DivId'></div>"
		+"                          <div class='form-group'>"
		+"                          	<div id='"+fileInputName+"AddMoreButtonId'><button onclick=\"addFileConrol('"+fileInputName+"DivId','"+fileInputName+"AddMoreButtonId','"+fileInputName+"',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                          </div>"
		+"							</div>"
		+ "						</div>"
		+"                      <div class='modal-footer'>"
		+"                      	<button type='Submit' class='btn btn-primary'>Upload Images</button>"
		+"                          <button type='button' onClick='hideSubPopUp(\"editImagePopup7\")' class='btn btn-success'>Cancel</button>"                                                                               
		+"                      </div>"
		+" 					  </form>"
        + "            		</div>"
        + "        </div>"
        + "    </div>"
        + "</div>";
	 $("#showEditImagesPopup").html(html);
	 ajaxCommonImageUpdationForm(imageCatagory,"commonImageUpdationForm");
	 $("#editImagePopup7").addClass("in");
	 $("#editImagePopup7").attr("aria-hidden","false");
	 $("#editImagePopup7").css("display","block");
}
//common function
function ajaxCommonImageUpdationForm(imageCatagory,formId){
	var options = {
			success : function(data) {
				switch(imageCatagory){
					case "INWARD_GOODS_LR_IMAGE" : ajaxInwardGoodsLrImageUpdate(data); break;//editInward.js
					case "INWARD_GOODS_STN_IMAGE" : ajaxInwardGoodsInvoiceRegStnImageUpdate(data);break;
					case "INWARD_GOODS_CLAIM_LETTER_IMAGE" :  ajaxInwardGoodsRegClaimLetterImageUpdate(data);break;
					case "INWARD_GOODS_CLAIM_PICTURE_IMAGE" : ajaxInwardGoodsRegClaimPicturesImageUpdate(data); break;
					case "INWARD_COURIER_DOCKET_FILE_IMAGE" : ajaxInwardCourierDocketFileImageUpdate(data); break;
					case "INWARD_RETURN_GOODS_LR_DETAILS_LR_IMAGE" : ajaxInwardReturnGoodsLrRegLrImageUpdate(data); break;
					case "INWARD_RETURN_GOODS_LR_DETAILS_CLAIM_COPY_IMG" : ajaxInwardReturnGoodsLrRegClaimCopyImageUpdate(data); break;
					case "INWARD_RETURN_GOODS_CHECKING_DONE_CHECKING_SLIP" : ajaxCheckingDoneCheckingSlipImageUpdate(data); break;
					case "INWARD_RETURN_GOODS_CREDIT_NOTE_ATTACH_IMG" : ajaxCreditNoteRegCreditNoteAttachImageUpdate(data); break;
					case "OUTWARD_RETURN_GOODS_LR_DETAILS_LR_IMAGE" : ajaxOutwardReturnGoodsLrRegLrImageUpdate(data); break;
					case "OUTWARD_RETURN_GOODS_LR_DETAILS_CLAIM_COPY_IMG" : ajaxOutwardReturnGoodsLrRegClaimCopyImageUpdate(data); break;
					case "OUTWARD_RETURN_GOODS_CREDIT_NOTE_ATTACH_IMGAGE" : ajaxOutwardReturnGoodsCreditNoteAttachImageUpdate(data); break;
					case "OUTWARD_GOODS_ORDER_COPY_IMAGE" : ajaxOutwardOrderCopyImageUpdate(data); break;
					case "OUTWARD_GATE_PASS_LR_IMAGE" : ajaxOutwardAddLrImageUpdate(data); break;
					case "ORGANIZATON_UPLOAD_DOCUMENT_IMAGE" : ajaxOrganizationDocImageUpdate(data); break;
					case "COMPANY_UPLOAD_DOCUMENT_IMAGE" : ajaxCompanyDocumentImageUpdate(data); break;
					case "STOCKIST_UPLOAD_DOCUMENT_IMAGE" : ajaxStockistDocumentImageUpdate(data); break;
					case "TRANSPORTER_UPLOAD_DOCUMENT_IMAGE" : ajaxTransporterDocumentImageUpdate(data); break;
					case "EMPLOYEE_IMAGE" : ajaxEmployeeImageUpdate(data); break;
					case "EMPLOYEE_ADDRESS_PROOF_IMAGE" : ajaxEmployeeAddressProofImageUpdate(data); break;
					case "EMPLOYEE_UPLOAD_DOCUMENT_IMAGE" : ajaxEmployeeDocumentImageUpdate(data); break;
					case "CARTING_AGENT_PHOTO_IMAGE" : ajaxCartingAgentPhotoImageUpdate(data); break;
					case "CARTING_AGENT_VEHICLE_PHOTO_IMAGE" : ajaxCartingAgentVehiclePhotoImageUpdate(data); break;
					case "CARTING_AGENT_UPLOAD_DOCUMENT_IMAGE" : ajaxCartingAgentDocumentImageUpdate(data); break;
				}
			}
		};
$('#' + formId).ajaxForm(options);
}
//common function
function deleteDocumentFile( imageCatagory, image_arrayIndex, imageId){
	//alert("Delete Function Call ,imageCatagory = "+imageCatagory+" imageId="+imageId);
	switch(imageCatagory){
		case "INWARD_GOODS_LR_IMAGE" : deleteInwardGoodsRegLrImage(imageId,image_arrayIndex); break;//editInward.js
		case "INWARD_GOODS_STN_IMAGE" : deleteInwardGoodsRegInvoiceStnImage(imageId,image_arrayIndex); break;
		case "INWARD_GOODS_CLAIM_LETTER_IMAGE" :  deleteInwardGoodsRegClaimLetterImage(imageId,image_arrayIndex); break;
		case "INWARD_GOODS_CLAIM_PICTURE_IMAGE" : deleteInwardGoodsRegClaimPictureImage(imageId,image_arrayIndex); break;
		case "INWARD_COURIER_DOCKET_FILE_IMAGE" : alert("Delete INWARD_COURIER_DOCKET_FILE_IMAGE");deleteInwardCourierDocketFileImage(imageId,image_arrayIndex); break;
		case "INWARD_RETURN_GOODS_LR_DETAILS_LR_IMAGE" : deleteInwardReturnGoodsLrRegLrImage(imageId,image_arrayIndex); break;
		case "INWARD_RETURN_GOODS_LR_DETAILS_CLAIM_COPY_IMG" : deleteInwardReturnGoodsLrRegClaimCopyImage(imageId,image_arrayIndex); break;
		case "INWARD_RETURN_GOODS_CHECKING_DONE_CHECKING_SLIP" : deleteCheckingDoneCheckingSlipImage(imageId,image_arrayIndex); break;
		case "INWARD_RETURN_GOODS_CREDIT_NOTE_ATTACH_IMG" : deleteCreditNoteRegCreditNoteAttachImage(imageId,image_arrayIndex); break;
		case "OUTWARD_RETURN_GOODS_LR_DETAILS_LR_IMAGE" : deleteOutwardReturnGoodsLrRegLrImage(imageId,image_arrayIndex); break;
		case "OUTWARD_RETURN_GOODS_LR_DETAILS_CLAIM_COPY_IMG" : deleteOutwardReturnGoodsLrRegClaimCopyImage(imageId,image_arrayIndex); break;
		case "OUTWARD_RETURN_GOODS_CREDIT_NOTE_ATTACH_IMGAGE" : deleteOutwardReturnGoodsCreditNoteAttachImage(imageId,image_arrayIndex); break;
		case "OUTWARD_GOODS_ORDER_COPY_IMAGE" : deleteOutwardOrderCopyImage(imageId,image_arrayIndex); break;
		case "OUTWARD_GATE_PASS_LR_IMAGE" : deleteOutwardGoodsGatePassLrImage(imageId,image_arrayIndex); break;
		case "ORGANIZATON_UPLOAD_DOCUMENT_IMAGE" : deleteOrganizationDocImage(imageId,image_arrayIndex); break;
		case "COMPANY_UPLOAD_DOCUMENT_IMAGE" : deleteCompanyDocumentImage(imageId,image_arrayIndex); break;
		case "STOCKIST_UPLOAD_DOCUMENT_IMAGE" : deleteStockistDocumentImage(imageId,image_arrayIndex); break;
		case "TRANSPORTER_UPLOAD_DOCUMENT_IMAGE" : deleteTransporterDocumentImage(imageId,image_arrayIndex); break;
		case "EMPLOYEE_IMAGE" : deleteEmployeeImage(imageId,image_arrayIndex); break;
		case "EMPLOYEE_ADDRESS_PROOF_IMAGE" : deleteEmployeeAddressProofImage(imageId,image_arrayIndex); break;
		case "EMPLOYEE_UPLOAD_DOCUMENT_IMAGE" : deleteEmployeeDocumentImage(imageId,image_arrayIndex); break;
		case "CARTING_AGENT_PHOTO_IMAGE" : deleteCartingAgentPhotoImage(imageId,image_arrayIndex); break;
		case "CARTING_AGENT_VEHICLE_PHOTO_IMAGE" : deleteCartingAgentVehiclePhotoImage(imageId,image_arrayIndex); break;
		case "CARTING_AGENT_UPLOAD_DOCUMENT_IMAGE" : deleteCartingAgentDocumentImage(imageId,image_arrayIndex); break;
	}
}

var RECORD_COUNT = 100;
function paginationView(selectedPage, pageCount,url) {
	if(pageCount>0){
		var pageCounter = selectedPage;
		var html = "<ul>";
		if(selectedPage>1)
			html += "<li><a href='#' onclick='"+url+(selectedPage-1)+")'>Prev</a></li>";
		if (selectedPage < 7) {
			for (var i = 1; i <= pageCount; i++) {
				if (selectedPage == i)
					html += "<li class='active'><a>" + i + "</a></li>";
				else
					html += "<li><a href='#' onclick='"+url+i+")'>" + i + "</a></li>";
				if (i == 10)
					break;
			}
		} else {
			if (pageCount >= (selectedPage + 4)) {
				for (var i = selectedPage - 5; i <= pageCount; i++) {
					if (selectedPage == i)
						html += "<li class='active'><a>" + i + "</a></li>";
					else
						html += "<li><a href='#' onclick='"+url+i+")'>" + i + "</a></li>";
					if (i > (selectedPage + 3))
						break;
				}
			} else {
				var i;
				if(pageCount>10)
					i = pageCount+1-10;
				else
					i = 1;
				for (; i <= pageCount; i++) {
					if (selectedPage == i)
						html += "<li class='active'><a>" + i + "</a></li>";
					else
						html += "<li><a href='#' onclick='"+url+i+")'>" + i + "</a></li>";
				}
			}
		}
		if(selectedPage<pageCount)
			html += "<li><a href='#' onclick='"+url+(selectedPage+1)+")'>Next</a></li>";
		html += "</ul>";
		$('#pagination').html(html);
	}
	else
		$('#pagination').html("");
}
