function organizationEditForm(organizationId,mainIndex)
{
	$.post(contextApplicationPath+'/OrganizationController/ViewAllDetailsOfOrganization', {organizationId : organizationId , editStatus : true}, function(data) {
		$(data).each(function(index,element){
		var html = ""
			+ "<div class='modal fade in' id='modal-responsive' >"
	        + "    <div class='col-md-13'>"
	        + "        <div class='modal-content'>"
	        + "            <div class='modal-header'>"
	        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	        + "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Organization Details</strong></h4>"
	        + "            </div>"
	        + "												<form id='editOrganizationDetails' action='"+contextApplicationPath+"/OrganizationController/editOrganization' method='post'>"
			+ "                                                <div class='panel-body'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "															   <input type='hidden' name='organizationId' value='"+organizationId+"'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Organization Name</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' value='"+element.ORG_NAME+"' placeholder='Organization Name' name='organizationName' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Location</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' value='"+element.LOCATION_NAME+"' placeholder='Location' name='location' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Telephone</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' value='"+element.TELEPHONE+"' placeholder='Telephone' name='telephone' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>  "
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Email ID</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' value='"+element.EMAIL_ID+"' placeholder='Email ID' name='emailId' class='form-control' required>"
			+ "                                                               </div>"
			+ "                                                          </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>User Name</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' value='"+element.USER_NAME+"' placeholder='User Name' name='userName' class='form-control' required>"
			+ "																	  <div id='UserName_Error' class='validationError'></div>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Address</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='textarea' value='"+element.ADDRESS+"' placeholder='Address' name='address' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Contact Person</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Contact Person' value='"+element.CONTACT_PERSON+"' name='contactPerson' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Mobile No</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' value='"+element.MOBILE_NO+"' placeholder='Mobile No' name='mobileNo' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Website</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' value='"+element.WEB_SITE+"' placeholder='Website' name='website' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Password</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='password' value='"+element.PWD+"' placeholder='Password' name='password' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                                <div class='pull-right'>"
			+ "                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#editOrganizationDetails\")'>Submit</button>"
			+ "                                                                    <button type='reset' onclick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "												</form>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>";
		$('#popup').html(html);
		ajaxOrganizationEditForm("editOrganizationDetails",mainIndex);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
		});
	}, 'json');
}
//ajax call
function ajaxOrganizationEditForm(formId,mainIndex){
	var options = {
			dataType : 'json',//     
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.RESULT==true){
					$('#ORG_NAME_'+mainIndex).html(data.ORG_NAME);
					$('#CONTACT_PERSON_'+mainIndex).html(data.CONTACT_PERSON);
					$('#MOBILE_NO_'+mainIndex).html(data.MOBILE_NO);
					$('#EMAIL_ID_'+mainIndex).html(data.EMAIL_ID);
					alert(data.MSG);
					hidePopUp();
				}
				else if(data.RESULT==false){
					$('#UserName_Error').html(data.MSG);
					alert(data.MSG);
				}else{
					alert(data.MSG);
				}
			}
	};
	$('#'+formId).ajaxForm(options);
}		
function organizationBankDetailsEdit(organizationBankId,index)
{
	var html="" 
		+ "<div class='modal fade in' id='modal-responsive' >"
	    + "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Organization</strong></h4>"
		+ "            </div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>";
	/*$.post(contextApplicationPath+'/OrganizationController/EditOranizationBankDetails', {organizationBankId : organizationBankId}, function(data) {
			$(data).each(function(index,element){*/
			html+="<form id='editOrganizationBankDetails' action='"+contextApplicationPath+"/OrganizationController/UpdateOranizationBankDetails' method='post'>"
			+"<input type='hidden' name='organizationBankId' value='"+organizationBankId+"'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Organization</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' value='"+ commonData[index].ORG_NAME +"' class='form-control' disabled>"
			//+"                                                                    <input type='hidden' value='"+ commonData[index].ORG_ID+"' name='organiztionNameId'>"
			+"                                                                </div>"
			+"                                                            </div> "
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Branch*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' id='bankBranchNameId' value='"+ commonData[index].BRANCH_NAME +"' class='form-control' name='branchName' required>"
			+"                                                                </div>"
			+"                                                            </div> "
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>IFSC Code*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text'  id='bankIfscCodeId' value='"+ commonData[index].IFSC_CODE +"' class='form-control' name='ifscCode' required>"
			+"                                                                </div>"
			+"                                                            </div>      "                                                 
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Bank Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' id='bankNameId'  value='"+ commonData[index].BANK_NAME +"' class='form-control' name='bankName' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Account No*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' id='bankAccoutNoId' value='"+ commonData[index].ACCOUNT_NO +"' class='form-control' name='accountNo' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+ "            <div class='modal-footer text-center'>"
			+ "                <button type='submit'  class='btn btn-danger' onclick='validateFormDetails(\"#editOrganizationBankDetails\")'>Update</button>"
	        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	        + "            </div>"
			+"</form>";
			
			html+="                                                </div>"
			    +"                                            </div>"
	            + "        </div>"
	            + "    </div>"
	            + "</div>";
			$('#popup').html(html);
			$("#modal-responsive").addClass("in");
			$("#modal-responsive").attr("aria-hidden","false");
			$("#modal-responsive").css("display","block"); 
			//loadOrganiztionDropDownForEdit(element.ORG_ID);
			ajaxEditOrganizationBankDetails('editOrganizationBankDetails',index);
		/*});
	}, 'json');*/	
	
}
		
function ajaxEditOrganizationBankDetails(FormId,mainIndex)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						commonData[mainIndex].BRANCH_NAME = $('#bankBranchNameId').val();
						commonData[mainIndex].IFSC_CODE = $('#bankIfscCodeId').val();
						commonData[mainIndex].BANK_NAME = $('#bankNameId').val();
						commonData[mainIndex].ACCOUNT_NO = $('#bankAccoutNoId').val();
						
						$('#BANK_NAME'+mainIndex).html($('#bankNameId').val());
						$('#BRANCH_NAME'+mainIndex).html($('#bankBranchNameId').val());
						$('#ACCOUNT_NO'+mainIndex).html($('#bankAccoutNoId').val());
						$('#IFSC_CODE'+mainIndex).html($('#bankIfscCodeId').val());
						$('#modal-responsive').remove();
					}
					alert(data.MSG);
					$('#loader').hide();
					//organiztionBankList();
			}
		};
$('#' + FormId).ajaxForm(options);
}

//for edit org drop down
function loadOrganiztionDropDownForEdit(orgId)
{
	var userType='';
	var html="<div class='form-group' style='height: 58px;'>"
	+"<label class='form-label'><strong>Select Organization</strong>"
	+"</label>"
	+"<span class='tips'></span>"
	+"<div class='controls'>"
	+"<select class='form-control' class='form-control' id='organiztionId' name='organiztionNameId'>" 
	+"<option value='-1'>Select Organiztion</option>";
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
		$(data).each(function(index, element) {
			userType=element.USER_TYPE;
			if(element.USER_TYPE=='SuperAdmin')
				{
					if(orgId==element.ORG_ID)
						html+="<option selected='selected' value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
					else
						html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
				}
			else
				{
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='organiztionNameId'>" +
						""+element.ORG_NAME+"";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
		content+="</div>"
		+"</div>";
		if(userType=='SuperAdmin')
			{
			$('#organiztionDropDown').html(html);
			}
		else
			{
			$('#organiztionDropDown').html(content);
			}
		
	}, 'json');
}

function editOrganizationDocumentForm(index, from)
{
		var html = ""
			+ "<div class='modal fade in' id='modal-responsive' >"
	        + "    <div class='col-md-13'>"
	        + "        <div class='modal-content'>"
	        + "            <div class='modal-header'>"
	        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	        + "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Organization Details</strong></h4>"
	        + "            </div>"
	        + "												<form id='editOrganizationDocumentForm' action='"+contextApplicationPath+"/OrganizationController/updateOrganizationDocumentInfo' method='post'>"
			+ "                                                <div class='panel-body'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "															   <input type='hidden' name='organiztionDocId' value='"+commonData[index].ORG_DOCUMENT_ID+"'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Organization Name</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input disabled type='text' value='"+commonData[index].ORG_NAME+"' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
            +"                                                            <div class='form-group'>"
            +"                                                     		    <button type='button' onclick='editOrganizationDocImagesListPopup("+commonData[index].ORG_DOCUMENT_ID+","+index+")' class='btn btn-success'>Edit Organization Document Images</button>"
		    +"                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Document Type*</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "																		<div id='documentList'></div>"
	        + "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                                <div class='pull-right'>"
			+ "                                                                    <button class='btn btn-primary m-b-10' onclick='showLoader()'>Submit</button>"
			+ "                                                                    <button type='reset' onclick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "												</form>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>";
		$('#popup').html(html);
		loadDocumentTypeListForEdit(commonData[index].DOCUMENT_TYPE_ID);
		ajaxEditOrganizationDocumentForm("editOrganizationDocumentForm",index, from);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
}
function ajaxEditOrganizationDocumentForm(FormId,index, from) {
	var options = {
		success : function(data) {
				alert(data.MSG);
				if(data.RESULT==true){
					commonData[index].DOCUMENT_TYPE_ID = data.DOCUMENT_TYPE_ID;
					commonData[index].DOCUMENT_TYPE = data.DOCUMENT_TYPE;
					orgaiztionDocumentListingView(commonData, from, urlForEdit);
				}
				hidePopUp();
				$('#loader').hide();
		}
	};
	$('#' + FormId).ajaxForm(options);
}
//
function editOrganizationDocImagesListPopup(documentId, index){
	//commonData2 is global var
	commonData2 = index ;
	editListOfImagesPopup(documentId,"documentImage",commonData[index].FILE_NAME,"ORGANIZATON_UPLOAD_DOCUMENT_IMAGE" , "/OrganizationController/updateOrganizationUploadDocumentImage");
}
//Image updation Ajax call after getting result from server
function ajaxOrganizationDocImageUpdate(data){
	if(data.RESULT==true) {
		commonData[commonData2].FILE_NAME = data.FILE_NAME;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteOrganizationDocImage(orgDocumentPathId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OrganizationController/deleteOrganizationDocumentFileImage', {
			orgDocumentPathId : orgDocumentPathId
		}, function(data) {
			if(data.RESULT==true){
				commonData[commonData2].FILE_NAME[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}