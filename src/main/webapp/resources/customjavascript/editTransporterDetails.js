function editTransporterDetails(transporterDetailsId,mainIndex)
{
	//alert("in edit Transporter Details=="+transporterDetailsId);
	$.post(contextApplicationPath+'/TransporterDetailsController/ViewAllDetailsOfTransporterDetails',{TransporterDetailsId : transporterDetailsId, editStatus : true}, function(data) {
		$(data).each(function(index,element){
		var html=""
			+ "<div class='modal fade in' id='modal-responsive' >"
		    + "    <div class='col-md-13'>"
			+ "        <div class='modal-content'>"
			+ "            <div class='modal-header'>"
			+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Organization</strong></h4>"
			+ "            </div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"													<form id='editTransporterDetails' action='"+contextApplicationPath+"/TransporterDetailsController/updateTransporterDetails' method='post'>"
			+"													  <input type='hidden' name='transporterDetailsId' value='"+transporterDetailsId+"'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Organization Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' class='form-control' value='"+element.ORG_NAME+"' disabled >"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>State*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control'>"
			+"																		<option disabled selected> Select State</option>"
		    +"																	</select>"
			+"                                                                </div>"
			+"                                                            </div>"  
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select City*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select name='city' id='cityName' class='form-control' class='form-control'>"
			+"                                                                        <option disabled selected> Select City</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>        "    
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Address*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='textarea' id='addressId' value='"+element.ADDRESS+"' class='form-control' placeholder='Address' name='address' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Password*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='Password' value='"+element.PASSWORD+"' class='form-control' placeholder='Password' name='pwd' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Transporter Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' class='form-control' id='transporterNameId' value='"+element.TRAN_NAME+"' placeholder='Transporter Name' name='transporterName' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select District*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='districtName' name='district' onchange='getCities()' class='form-control'>"
			+"                                                                        <option disabled selected> Select District</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>        "    
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>PAN No*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' placeholder='PAN No' value='"+element.PAN_NUM+"' class='form-control' name='panNo' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>User Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='textarea' class='form-control' value='"+element.USER_NAME+"' placeholder='User Name' name='userName' required>"
			+"																	  <div id='UserName_Error' class='validationError'></div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>" 
			+"                                                    <div class='col-md-12 col-sm-12 col-xs-12 table-responsive' style=' margin-top: 10PX;'>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#editTransporterDetails\")'>Save</button>"
			+"                                                                <button type='reset'  onClick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"													  </form>"
			+ "                                                </div>"
		    + "                                            </div>"
	        + "        </div>"
	        + "    </div>"
	        + "</div>";
			$('#popup').html(html);
			$("#modal-responsive").addClass("in");
			$("#modal-responsive").attr("aria-hidden","false");
			$("#modal-responsive").css("display","block");
			 stateDropDownListForEdit(element.STATE_ID);
			 getDistrictForEdit(element.DISTRICT_ID,element.STATE_ID);
			 getCitiesForEdit(element.DISTRICT_ID,element.CITY_ID);
			 ajaxEditTransporterDetails('editTransporterDetails',index);
		});
	}, 'json');	
}
function ajaxEditTransporterDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						$('#TRAN_NAME'+mainIndex).html($('#transporterNameId').val());
						$('#ADDRESS'+mainIndex).html($('#addressId').val());
						if(data.TRANSPORTER_ID!=null||data.TRANSPORTER_ID!=undefined){
							$('#STATA'+mainIndex).html(data.STATE_NAME);
							$('#DISTRICT'+mainIndex).html(data.DISTRICT_NAME);
							$('#CITY'+mainIndex).html(data.CITY_NAME);
							//alert("NEW ID = "+data.TRANSPORTER_ID);
							var htmlText = "<a class='edit btn btn-blue' href='#' onclick=\"viewTransporterDetails("+data.TRANSPORTER_ID+")\"><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' href='#' onclick=\"editTransporterDetails("+data.TRANSPORTER_ID+","+mainIndex+")\"><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteTransporterDetails("+data.TRANSPORTER_ID+")\"><i class='fa fa-times-circle'></i> </a>";
							$('#LINKS'+mainIndex).html(htmlText);
						}
						$('#loader').hide();
						$('#modal-responsive').remove();
					}else if(data.RESULT==false){
						$('#error').html(data.MSG);
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
function editStationPersonContactDetails(StationPersonContactDetailsId,mainIndex)
{
	//alert("in edit Transporter Details=="+transporterDetailsId);
	$.post(contextApplicationPath+'/TransporterDetailsController/ViewAllDetailsOfTransporterStationDetailsId',{transporterStationDetailsId : StationPersonContactDetailsId}, function(data) {
		$(data).each(function(index,element){
		var html=""
			+ "<div class='modal fade in' id='modal-responsive' >"
		    + "    <div class='col-md-13'>"
			+ "        <div class='modal-content'>"
			+ "            <div class='modal-header'>"
			+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Organization</strong></h4>"
			+ "            </div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"													<form id='editStationPersonContactDetails' action='"+contextApplicationPath+"/TransporterDetailsController/updateTransporterStationPersonContactDetails' method='post'>"
			+"													  <input type='hidden' name='stationPersonContactDetailsId' value='"+StationPersonContactDetailsId+"'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Organization Name*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' disabled class='form-control' value='"+element.ORG_NAME+"'>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Transporter Name*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' disabled class='form-control' value='"+element.TRAN_NAME+"'>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                            <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                                                <h4><strong>Station Person Contact Details</strong></h4>"
			+"                                                            </div>"
			+"                                                            <div class='row'>"
			+"                                                                <div class='col-md-12 col-sm-12 col-xs-12'>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Contact Name</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Contact Name' value='"+element.CONTACT_NAME+"' class='form-control' id='stationPersonContactNameId' name='stationPersonContactName'>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Email ID</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' name='emailId' value='"+element.EMAIL_ID+"' class='form-control' placeholder='Email ID' >"      // id='email_Id' 
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                                                <h4><strong>Station Details</strong></h4>"
			+"                                                            </div>"
			+"                                                            <div class='row'>"
			+"                                                                <div class='col-md-12 col-sm-12 col-xs-12'>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Station Name*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Station Name' value='"+element.STATION_NAME+"' class='form-control' id='StationNameId' name='StationName' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Address*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='textarea' placeholder='Address' value='"+element.ADDRESS+"' class='form-control' name='address' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Telephone</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Telephone' value='"+element.TELEPHONE+"' class='form-control' name='telephone' parsley-type='onlynumber'>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Mobile</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Mobile' value='"+element.MOBILE+"' class='form-control' id='mobileId' name='mobile' parsley-type='onlynumber'>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='panel panel-default'>"
			+"                                                            <div class='panel-body'>"
			+"                                                                <div class='col-md-4'>"
			+"                                                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                                                        <h4><strong>Claim Rates</strong></h4>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Claim Rates*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Claim Rates' value='"+element.CLAIM_RATE+"' class='form-control' name='claimRate' parsley-type='number' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Rate Per LR*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Rate Per LR' value='"+element.RATE_PER_LR+"' class='form-control' name='cRatePerLR' parsley-type='number' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>DD Per Case*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='DD Per Case' value='"+element.DD_PER_CASE+"' class='form-control' name='cDDPerCase' parsley-type='number' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Labour Per Case*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Labour Per Case' value='"+element.LABOUR_PER_CASE+"' class='form-control' name='cLaburPerCase' parsley-type='number' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                </div>"
			+"                                                                <div class='col-md-4'>"
			+"                                                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                                                        <h4><strong>Rate Per Station :</strong> Rate Per Case</h4>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Rate Per Case*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Rate Per Case' value='"+element.RPC_RATE_PER_CASE+"' class='form-control' name='rRatePerCase' parsley-type='number' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Rate Per LR*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Rate Per LR' value='"+element.RPC_RATE_PER_LR+"' class='form-control' name='rRatePerLR' parsley-type='number' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>DD Per Case*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='DD Per Case' value='"+element.RPC_DD_PER_CASE+"' class='form-control' name='rDDPerCase' parsley-type='number' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='form-group'>"
			+"                                                                        <label class='form-label'><strong>Labour Per Case*</strong>"
			+"                                                                        </label>"
			+"                                                                        <span class='tips'></span>"
			+"                                                                        <div class='controls'>"
			+"                                                                            <input type='text' placeholder='Labour Per Case' value='"+element.RPC_LABOUR_PER_CASE+"' class='form-control' name='rLabourPerCase' parsley-type='number' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                    <div class='col-md-12 col-sm-12 col-xs-12 table-responsive' style=' margin-top: 10PX;'>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button type='submit' class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#editStationPersonContactDetails\")'>Update</button>"
			+"                                                                <button type='reset' onClick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"													  </form>"
			+ "                                                </div>"
		    + "                                            </div>"
	        + "        </div>"
	        + "    </div>"
	        + "</div>";
			$('#popup').html(html);
			$("#modal-responsive").addClass("in");
			$("#modal-responsive").attr("aria-hidden","false");
			$("#modal-responsive").css("display","block");
			ajaxEditStationPersonContactDetails("editStationPersonContactDetails",mainIndex);
		});
	}, 'json');	
}
function ajaxEditStationPersonContactDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						$('#STATION_NAME'+mainIndex).html($('#StationNameId').val());
						$('#CONTACT_NAME'+mainIndex).html($('#stationPersonContactNameId').val());
						$('#MOBILE_NO'+mainIndex).html($('#mobileId').val());
						$('#EMAIL'+mainIndex).html($('#email_Id').val());
						if(data.STAION_CONTACT_PER_ID!=null||data.STAION_CONTACT_PER_ID!=undefined){
							alert("NEW ID = "+data.STAION_CONTACT_PER_ID);
							var htmlText = "<a class='edit btn btn-blue' href='#' onclick=\"viewStationPersonContactDetails("+data.STAION_CONTACT_PER_ID+")\"><i class='fa fa-external-link'></i></a><a class='edit btn btn-dark' href='#' onclick=\"editStationPersonContactDetails("+data.STAION_CONTACT_PER_ID+","+mainIndex+")\"><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteStationPersonContactDetails("+data.STAION_CONTACT_PER_ID+")\"><i class='fa fa-times-circle'></i> </a>";
							$('#LINKS'+mainIndex).html(htmlText);
						}
						$('#modal-responsive').remove();
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}

function editTransporterDocumentForm(index, from){
		var html = ""
			+ "<div class='modal fade in' id='modal-responsive' >"
	        + "    <div class='col-md-13'>"
	        + "        <div class='modal-content'>"
	        + "            <div class='modal-header'>"
	        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	        + "                <h4 class='modal-title' id='myModalLabel'><strong>Update Transporter Upload Document Details</strong></h4>"
	        + "            </div>"
	        + "												<form id='editCompanyDocumentForm' action='"+contextApplicationPath+"/TransporterDetailsController/updateTransporterDocumentInfo' method='post'>"
			+ "                                                <div class='panel-body'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "															   <input type='hidden' name='transporterDocId' value='"+commonData[index].TRAN_DOCUMENT_ID+"'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Organization Name</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input disabled type='text' value='"+commonData[index].ORG_NAME+"' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
            +"                                                            <div class='form-group'>"
            +"                                                     		    <button type='button' onclick='editTransporterDocImagesListPopup("+commonData[index].TRAN_DOCUMENT_ID+","+index+")' class='btn btn-success'>Edit Transporter Document Images</button>"
		    +"                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Transporter*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select name='transporter' id='transName' class='form-control' class='form-control'>"
			+"                                                                        <option disabled selected> Select Transporter</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Document Type*</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "																		<div id='documentList'></div>"
	        + "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                                <div class='pull-right'>"
			+ "                                                                    <button class='btn btn-primary m-b-10' onclick='showLoader()'>Submit</button>"
			+ "                                                                    <button type='reset' onclick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "												</form>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>";
		$('#popup').html(html);
		loadDocumentTypeListForEdit(commonData[index].DOC_TYPE_ID);
		getTransporterDropdownForEdit(commonData[index].ORG_ID,commonData[index].TRAN_ID);
		ajaxEditTransporterDocumentForm("editCompanyDocumentForm",index, from);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
}
function ajaxEditTransporterDocumentForm(FormId,index, from) {
	var options = {
		success : function(data) {
				alert(data.MSG);
				if(data.RESULT==true){
					commonData[index].DOC_TYPE_ID = data.DOC_TYPE_ID;
					commonData[index].DOC_TYPE = data.DOC_TYPE;
					commonData[index].TRAN_ID = data.TRAN_ID;
					commonData[index].TRAN_NAME = data.TRAN_NAME;
					TransporterUploadDocumentListingView(commonData, from, urlForEdit);
					//TransporterUploadDocumentListing();
				}
				hidePopUp();
				$('#loader').hide();
		}
	};
	$('#' + FormId).ajaxForm(options);
}
//
function editTransporterDocImagesListPopup(documentId, index){
	//commonData2 is global var
	commonData2 = index ;
	editListOfImagesPopup(documentId,"documentImage",commonData[index].FILE_NAME,"TRANSPORTER_UPLOAD_DOCUMENT_IMAGE" , "/TransporterDetailsController/updateTransportUploadDocumentImage");
}
//Image updation Ajax call after getting result from server
function ajaxTransporterDocumentImageUpdate(data){
	if(data.RESULT==true) {
		commonData[commonData2].FILE_NAME = data.FILE_NAME;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteTransporterDocumentImage(transporterDocumentPathId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/TransporterDetailsController/deleteTrnsporterDocumentFileImage', {
			transporterDocumentPathId : transporterDocumentPathId
		}, function(data) {
			if(data.RESULT==true){
				commonData[commonData2].FILE_NAME[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}