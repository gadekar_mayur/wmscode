function subTabsForOutstandingChequeDepositEntry(index)
{
	 var html = ""
	  +"                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
	  +"                                            <li id='OutstandingChequeDepositEntryTabEnetry1' class=''><a href='#tab2_6' data-toggle='tab' onclick='outstandingChequeDepositEntryDue()'><h5><strong>Today's Due</strong></h5></a></li>"
	  +"                                            <li id='OutstandingChequeDepositEntryTabEnetry2' class=''><a href='#tab2_7' data-toggle='tab' onclick='outstandingChequeDepositEntryLapsed()'><h5><strong>Lapsed</strong></h5></a></li>"
	  +"                                            <li id='OutstandingChequeDepositEntryTabEnetry3' class=''><a href='#tab2_7' data-toggle='tab' onclick='outstandingChequeDepositEntryUpcoming()'><h5><strong>Upcoming</strong></h5></a></li>"
	  +"                                            <li id='OutstandingChequeDepositEntryTabEnetry4' class=''><a href='#tab2_7' data-toggle='tab' onclick='outstandingChequeDepositEntryPadiInvoice()'><h5><strong>Paid Invoice</strong></h5></a></li>"
	  +"                                        </ul>";
	 $('#subTabsForOutstandingChequeDepositEntry').html(html);
	 		if(index==1)
	 			{
	 			$('#OutstandingChequeDepositEntryTabEnetry1').addClass("active");
	 			}
	 		if(index==2)
	 			{
	 			$('#OutstandingChequeDepositEntryTabEnetry2').addClass("active");
	 			}
	 		else if(index==3)
	 			{
				$('#OutstandingChequeDepositEntryTabEnetry3').addClass("active");
	 			}
	 		else if(index==4)
	 			{
	 			$('#OutstandingChequeDepositEntryTabEnetry4').addClass("active");
	 			}
}


function outstandingChequeDepositEntryDue()
{

	$('#loader').show();
	var i=1;
	//$.post(contextApplicationPath+'/BankController/listOutstandingChequeDepositEntryDue', function(data) {
		var html="                                <div class='tab-pane fade active in' id='tab2_5'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Outstanding Cheque Deposit Entry</strong></h3>"
		+"                                    </div>"
		+"                                    <div class=''>"
		+"<div id='subTabsForOutstandingChequeDepositEntry'></div>"
		+"                                            <div class='tab-pane fade active in' id='tab2_6'>"
		+"                                                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                    <h3><strong>Due</strong></h3>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-12'>"
		+"                                                        <div class='panel panel-default'>"
		+"                                                            <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+"																	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+"																	<option value='OrderNo'> Order Number</option>"
		+ "                                                    			   	<option value='InvoiceNumber'> Invoice Number</option>"
		+ "                                                    			   	<option value='InvoiceDate'> Invoice Date</option>"
		+ "                                                    			   	<option value='GrossAmount'> Gross Amount</option>"
		+ "                                                    			   	<option value='NetAmount'> Invoice Amount</option>"
		+"																</select>"	
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                   <input id='searchText' type='text' placeholder='Search Text' class='form-control' onkeyup =\"organizationKeyUp()\" >"
	    + "																		 <div id='searchText_Error_Msg' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
//		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutstandingChequeDepositEntryDueList()'>Show</button>"
		+"                                                                <button class='btn btn-success m-b-10' onclick=\" return validateSearchOutstandingChequeDepositEntryDueList($('#searchOption').val())\">Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForOutWardInvoiceEntryRegistration(false);
	commonOutstandingChequeDepositEntryDue("", "", "", "", 1);
//	searchOutstandingChequeDepositEntryDue(data);
//	}, 'json');

}


function commonOutstandingChequeDepositEntryDue(searchOption, searchText, fromSpecialData, toSpecialData, from)
{
//	alert("searchOption : "+searchOption);
//	alert("searchText : "+searchText);
$.post(contextApplicationPath+'/BankController/searchOutstandingChequeDepositEntry',
		{
	searchOption:searchOption,
	searchText:searchText,
	fromSpecialData:fromSpecialData,
	toSpecialData:toSpecialData,
	from : from
	}, function(data) {
		var url = "commonOutstandingChequeDepositEntryDue(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
		searchOutstandingChequeDepositEntryDue(data,from,url); // to call the function which shows the searched item
}, 'json');

}

function validateSearchOutstandingChequeDepositEntryDueList(searchOption)
{
	var status=true;
	if(searchOption=="InvoiceDate" || searchOption=="GrossAmount" || searchOption=="NetAmount")
		{
		if(!invoiceDate())
			status=false;
		if(!GrossAmountAndNetAmount())
			status=false;
		}else
		{
			if(!organization())
				status=false;
		}
		if(status)
		{
		searchOutstandingChequeDepositEntryDueList();
		}
	else
		return false;
}


function searchOutstandingChequeDepositEntryDue(data,from,url)
{
	$('#loader').show();
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=0;
	var html=""
		+"                                                                        <table class='table table-striped table-hover'>"
		+"                                                                            <thead class='no-bd'>"
		+"                                                                                <tr>"
		+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Order ID</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                                                    </th> "
		+"                                                                                    <th style='text-align: center;'><strong>Oranization</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                                    </th>   "
		+"                                                                                    <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Invoice No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Invoice Amount</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Remaining Amount</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                    </th>"
		+"                                                                                </tr>"
		+"                                                                            </thead>"
		+"                                                                            <tbody class='no-bd-y'>";
		$(data).each(function(index, element) {
			if((data.length-2)<i)
				return false;
			html+="<tr style='text-align: center;'>"
				+"<td>"+ SR_NO +"</td>"
				+"<td>"+element.ORDER_ID+"</td>"
				+"<td>"+element.ORDER_NO+"</td>"
				+"<td>"+element.ORG+"</td>"
				+"<td>"+element.COMPANY+"</td>"
				+"<td>"+element.STOCKIST+"</td>"
				+"<td>"+element.INVOICE_NO+"</td>"
				+"<td>"+element.INVOICE_AMOUNT+"</td>"
				+"<td>"+element.REMAINING_AMOUNT+"</td>"
				+"<td><a class='edit btn btn-blue' href='#' onclick='viewInvoiceDetails("+element.OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID+")'><i class='fa fa-external-link'></i></a></td>"
				+"</tr>";
			i++;
			SR_NO++;
		});
		html+="                                                                            </tbody>"
			+"                                                                        </table>";
		subTabsForOutstandingChequeDepositEntry(1);
		$('#searchResult').html(html);
		 paginationView(from,data[data.length-1].paginationCount,url);
		$('#loader').hide();
}

function searchOutstandingChequeDepositEntryDueList()
{
	var searchOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if(searchOption=="GrossAmount" || searchOption=="NetAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (searchOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	
	else if (searchOption=="InvoiceDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonOutstandingChequeDepositEntryDue(searchOption, searchText, fromSpecialData, toSpecialData, 1);
//	$.post(contextApplicationPath+'/BankController/searchOutstandingChequeDepositEntry',
//		{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData
//	}, function(data) {
//		searchOutstandingChequeDepositEntryDue(data) // to call the function which shows the searched item
//}, 'json');
}


function viewInvoiceDetails(OutWardOrderInvoiceEnteryRegistrationId)
{
	viewBankOutstandingChequeDepositEntry(OutWardOrderInvoiceEnteryRegistrationId);
	
}

function outstandingChequeDepositEntryLapsed()
{
	$('#loader').show();
	var i=1;
//	$.post(contextApplicationPath+'/BankController/listOutstandingChequeDepositEntryLapsed', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_5'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Outstanding Cheque Deposit Entry</strong></h3>"
		+"                                    </div>"
		+"                                    <div class=''>"
		+"<div id='subTabsForOutstandingChequeDepositEntry'></div>"
		+"                                            <div class='tab-pane fade active in' id='tab2_6'>"
		+"                                                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                    <h3><strong>Lapsed</strong></h3>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-12'>"
		+"                                                        <div class='panel panel-default'>"
		+"                                                            <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+"																	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+"																	<option value='OrderNo'> Order Number</option>"
		+ "                                                    			   	<option value='InvoiceNumber'> Invoice Number</option>"
		+ "                                                    			   	<option value='InvoiceDate'> Invoice Date</option>"
		+ "                                                    			   	<option value='GrossAmount'> Gross Amount</option>"
		+ "                                                    			   	<option value='NetAmount'> Invoice Amount</option>"
		+"																</select>"	
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                   <input id='searchText' type='text' placeholder='Search Text' class='form-control' onkeyup =\"organizationKeyUp()\" >"
	    + "																		 <div id='searchText_Error_Msg' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick=\" return validateSearchOutstandingChequeDepositEntryLapsList($('#searchOption').val())\">Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";

	$('#myTabContent').empty();
	$('#myTabContent').html(html);
		subTabsForOutstandingChequeDepositEntry(2);
		commonStockistDepositChequeLapsController("", "", "", "", 1);
//		searchOutstandingChequeDepositEntryLaps(data);
		$('#loader').hide();
//	}, 'json');
}

function commonStockistDepositChequeLapsController(searchOption, searchText, fromSpecialData, toSpecialData, from)
{
	$.post(contextApplicationPath+'/BankController/searchOutstandingChequeDepositEntryLaps',
			{
		searchOption:searchOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		from : from
		}, function(data) {
			var url = "commonStockistDepositChequeLapsController(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			searchOutstandingChequeDepositEntryLaps(data, from, url) // to call the function which shows the searched item
	}, 'json');
	
}
function validateSearchOutstandingChequeDepositEntryLapsList(searchOption)
{
	var status=true;
	if(searchOption=="InvoiceDate" || searchOption=="GrossAmount" || searchOption=="NetAmount")
		{
		if(!invoiceDate())
			status=false;
		if(!GrossAmountAndNetAmount())
			status=false;
		}else
		{
			if(!organization())
				status=false;
		}
		if(status)
		{
			searchOutstandingChequeDepositEntryLapsList();
		}
	else
		return false;
}
function searchOutstandingChequeDepositEntryLaps(data, from, url)
{
$('#loader').show();
var i=0;
SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
var html=""
	+"                                                                        <table class='table table-striped table-hover'>"
	+"                                                                            <thead class='no-bd'>"
	+"                                                                                <tr>"
	+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Order ID</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Order No</strong>"
	+"                                                                                    </th> "
	+"                                                                                    <th style='text-align: center;'><strong>Oranization</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Company</strong>"
	+"                                                                                    </th>   "
	+"                                                                                    <th style='text-align: center;'><strong>Stockist</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Invoice No</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Invoice Amount</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Remaining Amount</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Action</strong>"
	+"                                                                                    </th>"
	+"                                                                                </tr>"
	+"                                                                            </thead>"
	+"                                                                            <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="<tr style='text-align: center;'>"
			+"<td>"+ SR_NO +"</td>"
			+"<td>"+element.ORDER_ID+"</td>"
			+"<td>"+element.ORDER_NO+"</td>"
			+"<td>"+element.ORG+"</td>"
			+"<td>"+element.COMPANY+"</td>"
			+"<td>"+element.STOCKIST+"</td>"
			+"<td>"+element.INVOICE_NO+"</td>"
			+"<td>"+element.INVOICE_AMOUNT+"</td>"
			+"<td>"+element.REMAINING_AMOUNT+"</td>"
			+"<td><a class='edit btn btn-blue' href='#' onclick='viewInvoiceDetails("+element.OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID+")'><i class='fa fa-external-link'></i></a></td>"
			+"</tr>";
		i++;
		SR_NO++;
	});
	html+="                                                                            </tbody>"
		+"                                                                        </table>";
	subTabsForOutstandingChequeDepositEntry(2);
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);
	$('#loader').hide();
}

function searchOutstandingChequeDepositEntryLapsList()
{
	
	var searchOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if(searchOption=="GrossAmount" || searchOption=="NetAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	

	}
	else if (searchOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	
	else if (searchOption=="InvoiceDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	
	commonStockistDepositChequeLapsController(searchOption, searchText, fromSpecialData, toSpecialData, 1);
//	$.post(contextApplicationPath+'/BankController/searchOutstandingChequeDepositEntryLaps',
//		{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData
//	}, function(data) {
//		searchOutstandingChequeDepositEntryLaps(data) // to call the function which shows the searched item
//}, 'json');

}
function outstandingChequeDepositEntryUpcoming()
{
	$('#loader').show();
	var i=1;
//	$.post(contextApplicationPath+'/BankController/listOutstandingChequeDepositEntryUpcoming', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_5'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Outstanding Cheque Deposit Entry</strong></h3>"
		+"                                    </div>"
		+"                                    <div class=''>"
		+"<div id='subTabsForOutstandingChequeDepositEntry'></div>"
		+"                                            <div class='tab-pane fade active in' id='tab2_6'>"
		+"                                                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                    <h3><strong>Upcoming</strong></h3>"
		+"                                                </div>"
		
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-12'>"
		+"                                                        <div class='panel panel-default'>"
		+"                                                            <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+"																	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+"																	<option value='OrderNo'> Order Number</option>"
		+ "                                                    			   	<option value='InvoiceNumber'> Invoice Number</option>"
		+ "                                                    			   	<option value='InvoiceDate'> Invoice Date</option>"
		+ "                                                    			   	<option value='GrossAmount'> Gross Amount</option>"
		+ "                                                    			   	<option value='NetAmount'> Invoice Amount</option>"
		+"																</select>"	
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                   <input id='searchText' type='text' placeholder='Search Text' class='form-control' onkeyup =\"organizationKeyUp()\" >"
	    + "																		 <div id='searchText_Error_Msg' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick=\" return validateSearchOutstandingChequeDepositEntryUpcomingList($('#searchOption').val())\">Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+ "                                                  <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();	
	$('#myTabContent').html(html);
//	searchOutstandingChequeDepositEntryDue(dat
	commonStockistDepositChequeUpcomingController("", "", "", "", 1);
	subTabsForOutstandingChequeDepositEntry(3);
		$('#loader').hide();
//	}, 'json');
	
}
function validateSearchOutstandingChequeDepositEntryUpcomingList(searchOption)
{
	var status=true;
	if(searchOption=="InvoiceDate" || searchOption=="GrossAmount" || searchOption=="NetAmount")
		{
		if(!invoiceDate())
			status=false;
		if(!GrossAmountAndNetAmount())
			status=false;
		}else
		{
			if(!organization())
				status=false;
		}
		if(status)
		{
			searchOutstandingChequeDepositEntryUpcomingList();
		}
	else
		return false;
}

function searchOutstandingChequeDepositEntryUpcoming(data, from, url)
{
$('#loader').show();
var i=0;
SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
var html=""
	+"                                                                        <table class='table table-striped table-hover'>"
	+"                                                                            <thead class='no-bd'>"
	+"                                                                                <tr>"
	+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Order ID</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Order No</strong>"
	+"                                                                                    </th> "
	+"                                                                                    <th style='text-align: center;'><strong>Oranization</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Company</strong>"
	+"                                                                                    </th>   "
	+"                                                                                    <th style='text-align: center;'><strong>Stockist</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Invoice No</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Invoice Amount</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Remaining Amount</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Action</strong>"
	+"                                                                                    </th>"
	+"                                                                                </tr>"
	+"                                                                            </thead>"
	+"                                                                            <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="<tr style='text-align: center;'>"
			+"<td>"+ SR_NO +"</td>"
			+"<td>"+element.ORDER_ID+"</td>"
			+"<td>"+element.ORDER_NO+"</td>"
			+"<td>"+element.ORG+"</td>"
			+"<td>"+element.COMPANY+"</td>"
			+"<td>"+element.STOCKIST+"</td>"
			+"<td>"+element.INVOICE_NO+"</td>"
			+"<td>"+element.INVOICE_AMOUNT+"</td>"
			+"<td>"+element.REMAINING_AMOUNT+"</td>"
			+"<td><a class='edit btn btn-blue' href='#' onclick='viewInvoiceDetails("+element.OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID+")'><i class='fa fa-external-link'></i></a></td>"
			+"</tr>";
		i++;
		SR_NO++;
	});
	html+="                                                                            </tbody>"
		+"                                                                        </table>";
	subTabsForOutstandingChequeDepositEntry(3);
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
	$('#loader').hide();
}
function commonStockistDepositChequeUpcomingController(searchOption, searchText, fromSpecialData, toSpecialData, from)
{
	$.post(contextApplicationPath+'/BankController/searchOutstandingChequeDepositEntryUpcoming',
			{
		searchOption:searchOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		from : from
	
}, function(data) {
	var url = "commonStockistDepositChequeUpcomingController(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
	searchOutstandingChequeDepositEntryUpcoming(data, from, url); // to call the function which shows the searched item
}, 'json');
}
function searchOutstandingChequeDepositEntryUpcomingList()
{

	var searchOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if(searchOption=="GrossAmount" || searchOption=="NetAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
		/* if($('#inputTOData3').val()<=$('#inputFromData3').val())
			{
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
			}
			else
			{
				alert('Seleted TO Amount should be greater than from Amount');
				return false;
			}*/
	}
	else if (searchOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	
	else if (searchOption=="InvoiceDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	
	commonStockistDepositChequeUpcomingController(searchOption, searchText, fromSpecialData, toSpecialData, 1);
	
}




function outstandingChequeDepositEntryPadiInvoice()
{
	$('#loader').show();
	var i=1;
//	$.post(contextApplicationPath+'/BankController/listOutstandingChequeDepositEntryPaidInvoice', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_5'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Outstanding Cheque Deposit Entry</strong></h3>"
		+"                                    </div>"
		+"                                    <div class=''>"
		+"<div id='subTabsForOutstandingChequeDepositEntry'></div>"
		+"                                            <div class='tab-pane fade active in' id='tab2_6'>"
		+"                                                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                    <h3><strong>Paid Invoice</strong></h3>"
		+"                                                </div>"


		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-12'>"
		+"                                                        <div class='panel panel-default'>"
		+"                                                            <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+"																	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+"																	<option value='OrderNo'> Order Number</option>"
		+ "                                                    			   	<option value='InvoiceNumber'> Invoice Number</option>"
		+ "                                                    			   	<option value='InvoiceDate'> Invoice Date</option>"
		+ "                                                    			   	<option value='GrossAmount'> Gross Amount</option>"
		+ "                                                    			   	<option value='NetAmount'> Invoice Amount</option>"
		+"																</select>"	
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                   <input id='searchText' type='text' placeholder='Search Text' class='form-control' onkeyup =\"organizationKeyUp()\" >"
	    + "																		 <div id='searchText_Error_Msg' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick=\" return validateSearchOutstandingChequeDepositEntryPaidInvoiceList($('#searchOption').val())\">Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";

	
	$('#myTabContent').empty();	
	$('#myTabContent').html(html);
	commonStockistDepositChequePaidInvoiceController("", "", "", "", 1);
//	searchOutstandingChequeDepositEntryDue(data);
	subTabsForOutstandingChequeDepositEntry(4);
		
		$('#loader').hide();
//	}, 'json');
}
function commonStockistDepositChequePaidInvoiceController(searchOption, searchText, fromSpecialData, toSpecialData, from)
{
	$.post(contextApplicationPath
			+ '/BankController/searchOutstandingChequeDepositEntryPaidInvoice',
			{
		searchOption:searchOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		from : from
		}, function(data) {
			var url = "commonStockistDepositChequePaidInvoiceController(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			searchOutstandingChequeDepositEntryPaidInvoice(data, from, url) // to call the function which shows the searched item
	}, 'json');
	
}
function validateSearchOutstandingChequeDepositEntryPaidInvoiceList(searchOption)
{
	var status=true;
	if(searchOption=="InvoiceDate" || searchOption=="GrossAmount" || searchOption=="NetAmount")
		{
		if(!invoiceDate())
			status=false;
		if(!GrossAmountAndNetAmount())
			status=false;
		}else
		{
			if(!organization())
				status=false;
		}
		if(status)
		{
		searchOutstandingChequeDepositEntryPaidInvoiceList();
		}
	else
		return false;
}


function searchOutstandingChequeDepositEntryPaidInvoice(data, from, url)
{
$('#loader').show();
var i=0;
SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
var html=""
	+"                                                                        <table class='table table-striped table-hover'>"
	+"                                                                            <thead class='no-bd'>"
	+"                                                                                <tr>"
	+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Order ID</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Order No</strong>"
	+"                                                                                    </th> "
	+"                                                                                    <th style='text-align: center;'><strong>Oranization</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Company</strong>"
	+"                                                                                    </th>   "
	+"                                                                                    <th style='text-align: center;'><strong>Stockist</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Invoice No</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Invoice Amount</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Remaining Amount</strong>"
	+"                                                                                    </th>"
	+"                                                                                    <th style='text-align: center;'><strong>Action</strong>"
	+"                                                                                    </th>"
	+"                                                                                </tr>"
	+"                                                                            </thead>"
	+"                                                                            <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="<tr style='text-align: center;'>"
			+"<td>"+ SR_NO +"</td>"
			+"<td>"+element.ORDER_ID+"</td>"
			+"<td>"+element.ORDER_NO+"</td>"
			+"<td>"+element.ORG+"</td>"
			+"<td>"+element.COMPANY+"</td>"
			+"<td>"+element.STOCKIST+"</td>"
			+"<td>"+element.INVOICE_NO+"</td>"
			+"<td>"+element.INVOICE_AMOUNT+"</td>"
			+"<td>"+element.REMAINING_AMOUNT+"</td>"
			+"<td><a class='edit btn btn-blue' href='#' onclick='viewInvoiceDetails("+element.OUTWARD_ORDER_INVOICE_ENTRY_REGISTRATION_ID+")'><i class='fa fa-external-link'></i></a></td>"
			+"</tr>";
		i++;
		SR_NO++;
	});
	html+="                                                                            </tbody>"
		+"                                                                        </table>";
	subTabsForOutstandingChequeDepositEntry(4);
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
	$('#loader').hide();
}

function searchOutstandingChequeDepositEntryPaidInvoiceList()
{

	var searchOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if(searchOption=="GrossAmount" || searchOption=="NetAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (searchOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	
	else if (searchOption=="InvoiceDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonStockistDepositChequePaidInvoiceController(searchOption, searchText, fromSpecialData, toSpecialData, 1);
//	$.post(contextApplicationPath+'/BankController/searchOutstandingChequeDepositEntryPaidInvoice',
//		{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData
//	}, function(data) {
//		searchOutstandingChequeDepositEntryPaidInvoice(data) // to call the function which shows the searched item
//}, 'json');

}