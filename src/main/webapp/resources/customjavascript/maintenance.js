function AddState(){
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>State Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class='tabcordion'>"
		+"                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#' data-toggle='tab' onclick='AddStateRegistration()'>State Details</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	AddStateRegistration();
}
function subTabsForAddStateRegistration(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addStateRegistrationTabEntry1' class=''><a href='#tab1_1' data-toggle='tab' onclick='AddStateRegistration()'><h5><strong>Add States </strong></h5></a></li>"
			+ "                                            <li id='viewStateRegistrationTabEntry2' class=''><a href='#tab1_1_1' data-toggle='tab' onclick='stateList(true,true)'><h5><strong>View States</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddStateRegistration').html(html);
	if (index == 1) {
		$('#addStateRegistrationTabEntry1').addClass("active");
	} 
	else if (index == 2) 
	{
		$('#viewStateRegistrationTabEntry2').addClass("active");
	}
}


function AddStateRegistration()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add States</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddStateRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"<form id='addState' action='"+contextApplicationPath+"/MaintenanceController/addState' method='post'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"															  <div id='selectOrganization' required></div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>State Name</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='State Name' name='stateName' required>"
		+"                                                                </div>"
		+"																  <div id='error' class='validationError' ></div>"
		+"                                                            </div>"
		+"                                                       </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addState\")'>Submit</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='stateList'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddStateRegistration(1);
	displayOrganizationDropDown(0);
	ajaxAddState('addState');
//	stateList(true,true);
}

function ajaxAddState(FormId)
{
	var options = {
			success : function(data) {
				if(data.result==true){
					$('#'+FormId)[0].reset();
					$('#error').html("");
					$('#loader').hide();
//					stateList(true,true);
					alert(data.MSG);
				}
				else{
					if(data.FLAG==true)
						$('#error').html(data.MSG);
					$('#loader').hide();
					alert(data.MSG);
				}
				
			}
		};
$('#'+FormId).ajaxForm(options);
}
//flag1=only state
//flag2=only district
//flag1 and flag2==>false---- only city
function stateList(FLAG1,FLAG2){
	//var i=1; 
	$('#loader').show();
		var html="";
//		alert(FLAG1 +"&&"+ FLAG2);
			if(FLAG1==false && FLAG2 == false)
				{
				html += "                                <div class='tab-pane fade active in' id='tab1_1_1'>"
	           	+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
				+ "                                        <h3><strong>View Cities </strong></h3>"
				+ "                                    </div>"
			    + "                                 <div id='subTabsForAddCityRegistration'></div>";
	          }
			if(FLAG1==true && FLAG2 == true)
			{
//				alert(FLAG1 +"&&"+ FLAG2+"InState");
				html+="                                <div class='tab-pane fade active in' id='tab1_2_1'>"
				+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
				+ "                                        <h3><strong>View States</strong></h3>"
				+ "                                    </div>"
				+ "<div id='subTabsForAddStateRegistration'></div>";
			}
			if(FLAG1==false && FLAG2 == true)
			{
			html += "                                <div class='tab-pane fade active in' id='tab1_3_1'>"
           	+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Districts</strong></h3>"
			+ "                                    </div>"
		    + "                                 <div id='subTabsForAddDistrictRegistration'></div>";
          }
			$.post(contextApplicationPath+'/MaintenanceController/getStateList',function(data) {
			html+="                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
		
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control' class='form-control'>";
			if(FLAG1){
			                                                                      html+=" <option value='Organization'>Organization Name</option>"
			+"                                                                       	 <option value='State'>State Name</option>";
			}else if(!FLAG1 && FLAG2){
																			      html+=" <option value='Organization'>Organization Name</option>"
			+"                                                                       	 <option value='District'>District Name</option>"
			+"                                                                       	 <option value='State'>State Name</option>";
			}
			else{
																				  html+=" <option value='Organization'>Organization Name</option>"
																					  +"  <option value='District'>District Name</option>"
																					  +"  <option value='State'>State Name</option>"
																				  	  +"  <option value='City'>City Name</option>";
				}
			html+="                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input id='searchText' type='text' class='form-control'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>";
				if (FLAG1) {
							html += "                                                        <div class='col-sm-9 col-sm-offset-3'>"
									+ "                                                            <div class='pull-right'>"
									+ "                                                                <button class='btn btn-success m-b-10' onclick='searchStateList()'>Show</button>"
									+ "                                                            </div>"
									+ "                                                        </div>";
				} else if (!FLAG1 && FLAG2) {
							html += "                                                        <div class='col-sm-9 col-sm-offset-3'>"
									+ "                                                            <div class='pull-right'>"
									+ "                                                                <button class='btn btn-success m-b-10' onclick='searchDistrictList()'>Show</button>"
									+ "                                                            </div>"
									+ "                                                        </div>";
						}
    		   else {
							html += "                                                        <div class='col-sm-9 col-sm-offset-3'>"
									+ "                                                            <div class='pull-right'>"
									+ "                                                                <button class='btn btn-success m-b-10' onclick='searchCityList()'>Show</button>"
									+ "                                                            </div>"
									+ "                                                        </div>";

						}
			//list of state
			html+="                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			//
			+"                                                    </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>";
		$('#myTabContent').html(html);
		if(FLAG1==false && FLAG2 == false)
		{
		subTabsForAddCityRegistration(2);
		}
		if(FLAG1==true && FLAG2 == true)
		{
			subTabsForAddStateRegistration(2);
		}
		if(FLAG1==false && FLAG2 == true)
		{
			subTabsForAddDistrictRegistration(2);
		}
		stateListView(FLAG1,FLAG2,data);
		
		$('#loader').hide();
		$('#loader').hide();
	}, 'json');
}
//
function stateListView(FLAG1,FLAG2,data){
	
	var i=0; 
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>State Name</strong>"
		+"                                                                        </th>";
																				if(FLAG1)
																					html+="<th style='text-align: center;'><strong>Action</strong></th>"
																				else
																					html+="<th style='text-align: center;'><strong>View District</strong></th>";
	html+="                                                                   </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
		
	$(data).each(function(index, element) {
		html+="<tr style='text-align: center;'>"
			+"<td>"+(i+1)+"</td>"
			+"<td>"+element.ORG_ID+"</td>"
			+"<td>"+element.STATE_NAME+"</td>";
		if(FLAG1)
			html+="<td><a class='edit btn btn-dark' href='#' onclick='editState("+element.STATE_ID+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick='deleteState("+element.STATE_ID+")'><i class='fa fa-times-circle'></i> </a></td>";
		else if(FLAG2)
			html+="<td><a class='edit btn btn-blue' onClick='viewDistrictList("+true+",\""+element.ORG_ID+"\",\""+element.STATE_NAME+"\","+element.STATE_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a></td>";
		else
			html+="<td><a class='edit btn btn-blue' onClick='viewDistrictList("+false+",\""+element.ORG_ID+"\",\""+element.STATE_NAME+"\","+element.STATE_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a></td>";
		html+="</tr>";
		i++;
	}); 
	html+="                                                                </tbody>"
		+"                                                            </table>";
	$("#searchResult").html(html);
	
}
function editState(stateId){
	stateEditPopUp(stateId);
}
function deleteState(ID){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/MaintenanceController/deleteStateDetails', {
			stateId : ID
		}, function(data) {
			stateList(true,true);
			alert(data.MSG);
		}, 'json');
    }
}
//District
function AddDistrict(){
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>District Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class='tabcordion'>"
		+"                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#' data-toggle='tab' onclick='AddDistrictRegistration()'>District Details</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	AddDistrictRegistration();
}

function subTabsForAddDistrictRegistration(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addDistrictRegistrationTabEntry1' class=''><a href='#tab1_3' data-toggle='tab' onclick='AddDistrictRegistration()'><h5><strong>Add District's </strong></h5></a></li>"
			+ "                                            <li id='viewDistrictRegistrationTabEntry2' class=''><a href='#tab1_3_1' data-toggle='tab' onclick='stateList(false,true)'><h5><strong>View District's </strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddDistrictRegistration').html(html);
	if (index == 1) {
		$('#addDistrictRegistrationTabEntry1').addClass("active");
	} 
	else if (index == 2) 
	{
		$('#viewDistrictRegistrationTabEntry2').addClass("active");
	}
}
function AddDistrictRegistration()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_3'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Districts</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddDistrictRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"<form id='addState' action='"+contextApplicationPath+"/MaintenanceController/addDistrict' method='post'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"															  <div id='selectOrganization' required></div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>District Name</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='District Name' name='districtName' required>"
		+"                                                                </div>"
		+"																  <div id='error' class='validationError' ></div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>State</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='state' id='stateName' class='form-control' required>"
		+"																		<option disabled selected> Select State</option>"
	    +"																	</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                       </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addState\")'>Submit</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='stateList'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddDistrictRegistration(1);
	displayOrganizationDropDown(0);
	ajaxAddDistrict('addState');
	stateDropDownList();
//	stateList(false,true);
}

function ajaxAddDistrict(FormId)
{
	var options = {
			success : function(data) {
				if(data.result==true){
					$('#'+FormId)[0].reset();
					$('#error').html("");
					$('#loader').hide();
					alert(data.MSG);
				}
				else{
					if(data.FLAG==true)
						$('#error').html(data.MSG);
					$('#loader').hide();
					alert(data.MSG);
				}
				
			}
		};
$('#'+FormId).ajaxForm(options);
}

function viewDistrictList(FLAG,orgName,stateName,stateId){
	$.post(contextApplicationPath+'/MaintenanceController/getDistrictList', {
		stateId : stateId 
			} , function(data) {
				var districtArray = data.districtArray;
				if(districtArray.length==0){
					alert("No District Available....!");
					return false;
				}
				var i =1;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>District List</strong></h4>"
			        + "            </div>"
			        + "  		   <div class='panel panel-default'>"
			        + "			       <div class='panel-body'>"
			        
					
			
					+"						<div class='row'>"
					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Organization</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>State</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>District</strong>"
					+"											</th>";
															  if(FLAG)
																html+="<th style='text-align: center;'><strong>Action</strong></th>";
															  else
																html+="<th style='text-align: center;'><strong>View City</strong></th>";
				html+="										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
					$(districtArray).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ i++ +"</td>"
							+"                                         <td >"+orgName+"</td>"
							+"                                         <td >"+stateName+"</td>"
							+"                                         <td>"+element.distName+"</td>"
							+"											<td style='width: 214px;'>";
							if(FLAG)
								html+="<a class='edit btn btn-dark' href='javascript:;' onClick='editDistrict("+element.distId+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteDistrict("+element.distId+",\""+orgName+"\",\""+stateName+"\","+stateId+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>";
							else
								html+="<a class='edit btn btn-blue' onClick='viewCityList(\""+orgName+"\",\""+stateName+"\",\""+element.distName+"\","+element.distId+")' href='javascript:;'><i class='fa fa-external-link'></i></a>";
						html+="											</td>"
							+"										</tr>";
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"<div id='subPopUp'></div>"
					+"						</div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
					+ "			   </div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
			}, 'json');
}
function editDistrict(districtId){
	
	districtEditPopUp(districtId);
}
function deleteDistrict(districtId,orgName,stateName,stateId){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/MaintenanceController/deleteDistrictDetails', {
			districtId : districtId
		}, function(data) {
			alert(data.MSG);
			hidePopUp();
			viewDistrictList(true,orgName,stateName,stateId);
		}, 'json');
    }
}

function viewCityList(orgName,stateName,distName,districtId){

	$.post(contextApplicationPath+'/MaintenanceController/getCityList', {
		districtId : districtId 
			} , function(data) {
				var cityArray = data.cityArray;
				if(cityArray.length==0){
					alert("No City Available....!");
					return false;
				}
				var i =1;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive123' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive123\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>City List</strong></h4>"
			        + "            </div>"
			        + "  		   <div class='panel panel-default'>"
			        + "			       <div class='panel-body'>"
					+"						<div class='row'>"
					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Organization</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>State</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>District</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>City</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Action</strong></th>"
					+"										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
					$(cityArray).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ i++ +"</td>"
							+"                                         <td >"+orgName+"</td>"
							+"                                         <td >"+stateName+"</td>"
							+"                                         <td>"+distName+"</td>"
							+"                                         <td>"+element.cityName+"</td>"
							+"											<td style='width: 214px;'>"
							+"											<a class='edit btn btn-dark' href='javascript:;' onClick='editCity(\""+orgName+"\",\""+stateName+"\",\""+distName+"\","+districtId+","+element.cityId+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteCity("+element.cityId+",\""+orgName+"\",\""+stateName+"\",\""+distName+"\","+districtId+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
							+"											</td>"
							+"										</tr>";
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"<div id='cityEditPopup'></div>"
					+"						</div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hideSubPopUp(\"modal-responsive123\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
					+ "			   </div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#subPopUp").html(html);
				 $("#modal-responsive123").addClass("in");
				 $("#modal-responsive123").attr("aria-hidden","false");
				 $("#modal-responsive123").css("display","block");
			}, 'json');
}
function editCity(orgName,stateName,distName,districtId,cityId){
	
	cityEditPopUp(orgName,stateName,distName,districtId,cityId);
}
function deleteCity(cityId,orgName,stateName,distName,districtId){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/MaintenanceController/deleteCityDetails', {
			cityId : cityId
		}, function(data) {
			alert(data.MSG);
			hideSubPopUp("modal-responsive123");
			viewCityList(orgName,stateName,distName,districtId);
		}, 'json');
    }
}
//City
function AddCity(){
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>City Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class='tabcordion'>"
		+"                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#' data-toggle='tab' onclick='AddCityRegistration()'>City Details</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	AddCityRegistration();
}

function subTabsForAddCityRegistration(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addCityRegistrationTabEntry1' class=''><a href='#tab1_3' data-toggle='tab' onclick='AddCityRegistration()'><h5><strong>Add Cities </strong></h5></a></li>"
			+ "                                            <li id='viewCityRegistrationTabEntry2' class=''><a href='#tab1_3_1' data-toggle='tab' onclick='stateList(false,false)'><h5><strong>View Cities </strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCityRegistration').html(html);
	if (index == 1) {
		$('#addCityRegistrationTabEntry1').addClass("active");
	} 
	else if (index == 2) 
	{
		$('#viewCityRegistrationTabEntry2').addClass("active");
	}
}

function AddCityRegistration()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_3'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Cities</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCityRegistration'></div>"

		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"<form id='addState' action='"+contextApplicationPath+"/MaintenanceController/addCity' method='post'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"															  <div id='selectOrganization' required></div>"
		+"                                                            <div class='form-group' style='height: 58px;'>"
		+"                                                                <label class='form-label'><strong>District</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='districtName' name='district'  class='form-control' required>"
		+"                                                                        <option disabled selected> Select District</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>State</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control' required>"
		+"																		<option disabled selected> Select State</option>"
	    +"																	</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>City Name</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='City Name' name='cityName' required>"
		+"                                                                </div>"
		+"																  <div id='error' class='validationError' ></div>"
		+"                                                            </div>"
		+"                                                       </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addState\")'>Submit</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='stateList'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddCityRegistration(1);
	displayOrganizationDropDown(0);
	ajaxAddCity('addState');
	stateDropDownList();
//	stateList(false,false);
}

function ajaxAddCity(FormId)
{
	var options = {
			success : function(data) {
				if(data.result==true){
					$('#'+FormId)[0].reset();
					$('#error').html("");
					$('#loader').hide();
					alert(data.MSG);
				}
				else{
					if(data.FLAG==true)
						$('#error').html(data.MSG);
					$('#loader').hide();
					alert(data.MSG);
				}
				
			}
		};
$('#'+FormId).ajaxForm(options);
}

//////////////////////////////// Search option for state by Vijay/////

function searchStateList()
{
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if (!(Boolean(searchText))) {
		alert("Please Enter the search text");
		return false
	}
	
	$.post(contextApplicationPath+ '/MaintenanceController/searchOptionForState',{
		searchOption : searchOption,
		searchText : searchText
		},
		function(data) {
			stateListView(true,true,data);
		}, 'json');
}
function searchDistrictList()
{
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if (!(Boolean(searchText))) {
		alert("Please Enter the search text");
		return false
	}
	$.post(contextApplicationPath+ '/MaintenanceController/searchDistrictDetails',{
		searchOption : searchOption,
		searchText : searchText
		},
		function(data) {
			
			districtSearchResultList(false,true,data);
			
		}, 'json');
}
function districtSearchResultList(FLAG1,FLAG2,data){
	var i = 1;
	var html=""
		+"								<table class='table table-striped table-hover'>"
		+"									<thead class='no-bd'>"
		+"										<tr>"
		+"											<th style='text-align: center;'><strong>Sr No.</strong>"
		+"											</th>"
		+"											<th style='text-align: center;'><strong>Organization</strong>"
		+"											</th>"
		+"											<th style='text-align: center;'><strong>State</strong>"
		+"											</th>"
		+"											<th style='text-align: center;'><strong>District</strong>"
		+"											</th>"
		+"											<th style='text-align: center;'><strong>Action</strong></th>"
	html+="										</tr>"
		+"									</thead>"
		+"									<tbody class='no-bd-y'>";
		$(data).each(function(index, element) {
			html+="                                    <tr style='text-align: center;'>"
				+"                                         <td>"+ i++ +"</td>"
				+"                                         <td >"+element.ORG+"</td>"
				+"                                         <td >"+element.STATE_NAME+"</td>"
				+"                                         <td>"+element.distName+"</td>"
				+"											<td style='width: 214px;'>"
			    +"<a class='edit btn btn-dark' href='javascript:;' onClick='editDistrict("+element.distId+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteDistrict("+element.distId+",\""+element.ORG+"\",\""+element.STATE_NAME+"\","+element.STATE_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
			    +"											</td>"
				+"										</tr>";
		 });
		
	html+="									</tbody>"
		+"								</table>"
		+"<div id='subPopUp'></div>";
	$("#searchResult").html(html);
}


function searchCityList()
{
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if (!(Boolean(searchText))) {
		alert("Please Enter the search text");
		return false
	}
	$.post(contextApplicationPath+ '/MaintenanceController/searchCityDetails',{
		searchOption : searchOption,
		searchText : searchText
		},
		function(data) {
			
			citySearchResultList(false,false,data);
			
		}, 'json');

}

function citySearchResultList(FLAG1,FLAG2,data){
	
	var i = 1;
	var html=""
		+"								<table class='table table-striped table-hover'>"
		+"									<thead class='no-bd'>"
		+"										<tr>"
		+"											<th style='text-align: center;'><strong>Sr No.</strong>"
		+"											</th>"
		+"											<th style='text-align: center;'><strong>Organization</strong>"
		+"											</th>"
		+"											<th style='text-align: center;'><strong>State</strong>"
		+"											</th>"
		+"											<th style='text-align: center;'><strong>District</strong>"
		+"											</th>"
		+"											<th style='text-align: center;'><strong>City</strong></th>"
		+"											<th style='text-align: center;'><strong>Action</strong></th>"
	html+="										</tr>"
		+"									</thead>"
		+"									<tbody class='no-bd-y'>";
		$(data).each(function(index, element) {
			html+="                                    <tr style='text-align: center;'>"
				+"                                         <td>"+ i++ +"</td>"
				+"                                         <td >"+element.ORG+"</td>"
				+"                                         <td >"+element.STATE_NAME+"</td>"
				+"                                         <td>"+element.distName+"</td>"
				+"                                         <td>"+element.cityName+"</td>"
				+"											<td style='width: 214px;'>"
			    +" <a class='edit btn btn-dark' href='#' onClick=\"editCity("+element.cityId+")\" href='javascript:;'><i class='fa fa-external-link'></i></a>  <a class='delete btn btn-danger' onClick='deleteCity("+element.cityId+",\""+element.ORG+"\",\""+element.STATE_NAME+"\",\""+element.distName+"\","+element.distId+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
			    +"											</td>"
				+"										</tr>";
		 });
		
	html+="									</tbody>"
		+"								</table>"
		+"<div id='cityEditPopup'></div>";
	$("#searchResult").html(html);
}

