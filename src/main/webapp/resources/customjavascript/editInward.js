//global variables
var invoiceDetails;
function editInwardGoodsRegistration(mainIndex,inwardGoodsRegistrationId)
{
	var i=1;
	var html="                                                        <div class='modal fade' id='modal-responsive' tabindex='-1' role='dialog' aria-labelledby='myLargeModalLabel' aria-hidden='true'>"
		+"                                                            <div class='modal-dialog modal-lg'>"
		+"                                                                <div class='modal-content'>"
		+"                                                                    <div class='modal-header'>"
		+"                                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true' onclick='hidePopUp()'>&times;</button>"
		+"                                                                        <h4 class='modal-title' id='myModalLabel'>Edit Inward Goods</h4>"
		+"                                                                    </div>";
	$.post(contextApplicationPath+'/InwardGoodsRegistrationController/loadInwardGoodsRegistrationUsignId',{inwardGoodsRegistrationId : inwardGoodsRegistrationId}, function(data) {
		$(data).each(function(index,element){
			commonData = element;
			 html+="<form id='editInwardGoodsRegistration' action='"+contextApplicationPath+"/InwardGoodsRegistrationController/editInwardGoodsRegistration' method='post' enctype='multipart/form-data'>"
			   +"                                                                    <div class='modal-body'>"
			   +"    																	  <div class='row'>"
	           +"       												  <div class='col-md-4'>"
			   +"            												 <div id='selectOrganization' class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Select Organization : - </strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div id='companyDropDown' class='form-group' required>"
			   +"                                                                <label class='form-label'><strong>Select Company* : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <select id='companyName' name='comapnyId' onchange=\"showCompanyDepoDropDown()\" class='form-control' required>"//+element.COMP+"</strong>"
			   +"                                                                   <option disabled selected> Select Company</option>"
			   +"                                                                </select>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Select Sending Depo* : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <div id='depoList' class='controls'>"
			   +"                                                                 <select id='companyDepo' name='sendingDepo' class='form-control' required>"//+element.DEPO+"</strong>"
			   +"                                                                    <option disabled selected> Select Depo</option>"
			   +"                                                                 </select>"
			   +"                                                                </div>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Select Transporter* : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <div id='TransporterDropDown' class='controls'>"
			   +"                                                                 <select id='transName' name='transporterId' class='form-control' required>"//+element.TRAN+"</strong>"
			   +"                                                                   <option disabled selected> Select Transporter</option>"
			   +"                                                                 </select>"
			   +"                                                                </div>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>No of Cases : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.NO_OF_CASES+"' name='noOfCases' class='form-control' id='field-1' placeholder='No of Cases' parsley-type='onlynumber' required>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Registration Date : -</strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.REG_DATE+"' name='regDate' class='dateClassCommon form-control' id='field-1' placeholder='Registration Date' required>"//+element.REG_DATE+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>LR No : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.LR_NO+"' name='LR_NO' class='form-control' id='field-1' placeholder='LR No' required>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			  /* +"            												 <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>LR File : - </strong>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <div id='imgDiv'>"
			   +"                                                                <img onClick='editLrImagesListPopup()' src='' alt='Smiley face' height='100' width='100'>"
			   +"                                                                </div>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='file' id='LR_FILE'  name='LR_File' class='form-control' id='field-1'>"
			   +"                                                                </label>"
			   +"                                                            </div>"*/
	           +"                                                            <div class='form-group'>"
	           +"                                                     		    <button type='button' onclick='editLrImagesListPopup("+inwardGoodsRegistrationId+")' class='btn btn-success'>Edit LR Images</button>"
			   +"                                                            </div>"
			   +"                                                        </div>"
	           +"       												 <div class='col-md-4'>"
	           +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Driver Name : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.DRIVER_NAME+"' name='driverName' class='form-control' id='field-1' placeholder='Driver Name'>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Unloading Charges : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.UNLOADING_CHARGES+"' name='unloadingCharges' class='form-control' id='field-1' placeholder='Uploading Charges' parsley-type='number'>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Unloader  Name : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.UNLOADER_NAME+"' name='unloaderName' class='form-control' id='field-1' placeholder='Unloader  Name'>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Time : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='timePicker' value='"+element.TIME+"' name='time' class='form-control' id='field-1' placeholder='Time' required>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Received By : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.RECEIVED_BY+"' name='receivedBy' class='form-control' id='field-1' placeholder='Received By' required>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Transporter Charges : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.TRAN_CHARGES+"' name='transPorterCharges' class='form-control' id='field-1' placeholder='Transporter Charges' parsley-type='number'>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Hamali Charges : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.HAMALI_CHAR+"' name='hamaliCharges' class='form-control' id='field-1' placeholder='Hamali Charges' parsley-type='number'>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Remark : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <input type='text' id='' value='"+element.REMARK+"' name='remark' class='form-control' id='field-1' placeholder='Remark'>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                        </div>"
			   +"													  </div>"
			   +"                                                   <div class='modal-footer'>"
			   +"                                                     <button type='Submit' class='btn btn-primary' data-dismiss='modal' onclick='validateFormDetails(\"#editInwardGoodsRegistration\")'>Update</button>"
			   +"                                                     <button type='button' class='btn btn-success' onclick='hidePopUp()'>Cancel</button>"                                                                               
			   +"                                                   </div>"
			   +"                                                   </div>"
			   +"													<input type='hidden' name='id' value='"+element.INWARD_REG_ID+"'>"
			  +"</form>"
			   +"                                                <div class='panel-body'>"
			   +"                                                    <div class='row'>"
			   +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			   +"                                                            <table class='table table-striped table-hover'>"
			   +"                                                                <thead class='no-bd'>"
				+"                                                                    <tr>"
				+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
				+"                                                                        </th>"
				+"                                                                        <th style='text-align: center;'><strong>STN No</strong>"
				+"                                                                        </th>"
				+"                                                                        <th style='text-align: center;'><strong>STN Image</strong>"
				+"                                                                        </th>"
				+"                                                                        <th style='text-align: center;'><strong>STN Date</strong>"
				+"                                                                        </th>"
				+"                                                                        <th style='text-align: center;'><strong>Gross Amount</strong>"
				+"                                                                        </th> "
				+"                                                                        <th style='text-align: center;'><strong>Vat Amount</strong>"
				+"                                                                        </th> "
				+"                                                                        <th style='text-align: center;'><strong>Discount Amount</strong>"
				+"                                                                        </th> "
				+"                                                                        <th style='text-align: center;'><strong>Net Amount</strong>"
				+"                                                                        </th> "
				+"                                                    					  <th style='text-align: center;'><strong>Action</strong></th>"
				+"                                                                    </tr>"
				+"                                                                </thead>"
				+"                                                                <tbody class='no-bd-y'>";
						 invoiceDetails=element.INVOICE_DETAILS;
						 $(invoiceDetails).each(function(index,element){
						html+="                                                                    <tr style='text-align: center;'>"
						+"                                                                        <td>"+ i +"</td>"
						+"                                                                        <td><label id='stnNo_"+i+"'>"+element.STN_NO+"</label></td>"
						+"                                                                        <td id='stnImg_"+i+"'><img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='editStnImagesListPopup("+element.INVOICE_ID+","+(i-1)+")'></td>"
						+"                                                                        <td><label id='stnDate_"+i+"'>"+element.STN_Date+"</label></td>"
						+"                                                                        <td><label id='stnGrossAmt_"+i+"'>"+element.Gross_Amount+" Rs </label></td>"
						+"                                                                        <td><label id='stnVatAmt_"+i+"'>"+element.Vat_Amount+" Rs</label></td>"
						+"                                                                        <td><label id='stnDiscountAmt_"+i+"'>"+element.Discount_Amount+" Rs</label></td>"
						+"                                                                        <td><label id='stnNetAmt_"+i+"'>"+element.Net_Amount+" Rs</label></td>"
						+"                                                       				  <td> <a  style='float: left; margin-right: 4px;' class='edit btn btn-dark' href='#' onclick='editInvoiceDetails("+i+")'><i class='fa fa-pencil-square-o'></i></a> </td>"
						+"                                                                    </tr>";
						i++;
						 });
				html+="                                                                </tbody>"
				+"                                                            </table>"
				+"                                                        </div>"
				+"<div id='subPopUp'></div>"
				+"                                                    </div>"
				+"                                                </div>";
			   
		
				//CLAIM_LETTER_STATUS
		if(element.CLAIM_LETTER_STATUS==1)
			{
			html+="<form id='editClaimLetter' action='"+contextApplicationPath+"/InwardGoodsRegistrationController/editClaimLetter' method='post' enctype='multipart/form-data'>"
				+"                                                                    <div class='modal-body'>"
				+"                                                                        <div class='row'>"
				+"                                                                            <div class='col-md-4'>"
				+"                                                                                <div class='form-group'>"
				+"                                                                                    <label for='field-1' class='control-label'><b style='font-size: medium;'>Edit Claim Letter Details :-</b></label>"
				+"                                                                                </div>"
				+"                                                                            </div>"
				+"																		  </div>"
				+"                                                                        <div class='row'>"
				+"                                                                            <div class='col-md-4'>"
	            +"                                                           					 <div class='form-group'>"
	            +"                                                     		    					<button type='button' onclick='editClaimLetterImagesListPopup("+element.CLAIM_LETTER_ID+")' class='btn btn-success'>Edit Claim Letter Images</button>"
			    +"                                                           					 </div>"
				/*+"                                                                                <div class='form-group'>"
				+"                                                                                    <label for='field-1' class='control-label'>Claim Letter</label>"
				+"                                                                                    <span class='tips'></span>"
				+"                                                                                    <div id='claimLetterImgDiv'>"
				+"                                                                					  <img onClick='updateClaimLetterDocument("+element.CLAIM_LETTER_ID+")' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"
				+"                                                                                    </div>"
				 +"                                                                                   <span class='tips'></span>"
				    +"                                                               				  <input type='file' id='claimLetter' name='claimLetter' class='form-control' id='field-1'>"
				+"                                                                                </div>"*/
				+"                                                                            </div>"
				+"                                                                            <div class='col-md-4'>"
	            +"                                                           					 <div class='form-group'>"
	            +"                                                     		    					<button type='button' onclick='editClaimPicturesImagesListPopup("+element.CLAIM_LETTER_ID+")' class='btn btn-success'>Edit Claim Pictures Images</button>"
			    +"                                                           					 </div>"
				/*+"                                                                                <div class='form-group'>"
				+"                                                                                    <label for='field-1' class='control-label'>Claim Picture</label>"
				+"                                                                                    <span class='tips'></span>"
				+"                                                                                    <div id='claimPicturesImgDiv'>"
				+"                                                                					  <img onClick='updateClaimPicturesDocument("+element.CLAIM_LETTER_ID+")' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"
				+"                                                                                    </div>"
				+"                                                                                   <span class='tips'></span>"
			    +"                                                               				  <input type='file' id='claimPictures' name='uploadPictures' class='form-control' id='field-1'>"
				+"                                                                                </div>"*/
				+"                                                                            </div>"
				+"                                                                        </div>"
				+"                                                                        <div class='row'>"
				+"                                                                            <div class='col-md-6'>"
				+"                                                                                <div class='form-group'>"
				+"                                                                                    <label for='field-2' class='control-label'>Add Description</label>"
				+"                                                                                    <span class='tips'></span>"
				+"                                                                                    <textarea class='form-control' id='field-2'  placeholder='Add Description' name='description' required>"+element.CLAIM_DESC+"</textarea>"
				+"                                                                                </div>"
				+"                                                                            </div>"
				+"                                                                        </div>"
				+"													 <input type='hidden' name='claimLetterId' value='"+element.CLAIM_LETTER_ID+"'>"
				+"                                                   <div class='modal-footer'>"
				+"                                                     <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editClaimLetter\")'>Update</button>"
				+"                                                     <button type='button' class='btn btn-success' onclick='hidePopUp()'>Cancel</button>"                                                                               
				+"                                                   </div>"
				+"                                                                    </div>"
				+"</form>";
		}
		html+="                                                                    <div class='modal-footer'>"
			+"                                                                        <button type='button' class='btn btn-danger' data-dismiss='modal' onclick='hidePopUp()'>Close</button>"                                                                                   
			+"                                                                    </div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>";
			$('#popup').html(html);
			$(".dateClassCommon").datepicker({
				changeMonth : true,
				changeYear : true,
				yearRange : "-100:+0"
			});
			$('#timePicker').timepicki();
			displayEditOrganizationDropDown(1,element.ORG_ID);
			getCompaniesDropdownForEdit(element.ORG_ID,element.COMP_ID);
			getTransporterDropdownForEdit(element.ORG_ID,element.TRAN_ID);
			getCompanyDepoDropdownForEdit(element.COMP_ID,element.DEPO_ID);
			ajaxeditInwardGoodsRegistration("editInwardGoodsRegistration",mainIndex);
			
			if(element.CLAIM_LETTER_STATUS==1)
			{
				ajaxEditClaimLetter("editClaimLetter");
			}
			$("#modal-responsive").addClass("in");
			$("#modal-responsive").attr("aria-hidden","false");
			$("#modal-responsive").css("display","block");
		});
	}, 'json');
}
function ajaxEditClaimLetter(FormId)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){//claimLetterImgDiv claimPicturesImgDiv//claimLetter claimPictures
						/*var htmlText = "<img onClick='showImagePopup(\""+data.CLAIM_LETTER+"\")' src='"+data.CLAIM_LETTER+"' alt='Smiley face' height='100' width='100'>";
						$('#claimLetterImgDiv').html(htmlText);
						$('#claimLetter').val('');
						
						var htmlText = "<img onClick='showImagePopup(\""+data.CLAIM_UPLOAD_PICTURE+"\")' src='"+data.CLAIM_UPLOAD_PICTURE+"' alt='Smiley face' height='100' width='100'>";
						$('#claimPicturesImgDiv').html(htmlText);
						$('#claimPictures').val('');*/
						alert(data.MSG);
					}else
					alert(data.MSG);
					$('#loader').hide();
			}
		};
$('#' + FormId).ajaxForm(options);
}
function ajaxeditInwardGoodsRegistration(FormId,mainIndex)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					if(element.RESULT==true){
						var htmlText = "<img onClick='showImagePopup(\""+element.LR_IMAGE_PATH+"\")' src='"+element.LR_IMAGE_PATH+"' alt='Smiley face' height='100' width='100'>";
						$('#imgDiv').html(htmlText);
						$('#LR_FILE').val('');
						
						$('#org_'+mainIndex).html(element.ORG_NAME);
						$('#trans_'+mainIndex).html(element.TRANS);
						$('#company_'+mainIndex).html(element.COMPANY);
						$('#lrNo_'+mainIndex).html(element.LR_NO);
						$('#depo_'+mainIndex).html(element.DEPO);
						$('#driver_'+mainIndex).html(element.DRIVER_NAME);
						$('#regDate_'+mainIndex).html(element.REG_DATE);
					}
					alert(element.MSG);
					$('#loader').hide();
				});
				$('#loader').hide();
			}
		};
$('#' + FormId).ajaxForm(options);
}
function editLrImagesListPopup(registrationId){
	editListOfImagesPopup(registrationId,"LR_File",commonData.LR_IMAGE_PATH,"INWARD_GOODS_LR_IMAGE" , "/InwardGoodsRegistrationController/updateInwardGoodsRegLrImage");
}
//Image updation Ajax call after getting result from server
function ajaxInwardGoodsLrImageUpdate(data){
	if(data.RESULT==true) {
		commonData.LR_IMAGE_PATH = data.LR_IMAGE_PATH;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteInwardGoodsRegLrImage(lrImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardGoodsRegistrationController/deleteInwardGoodsRegistrationLrImage', {
			lrImageId : lrImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.LR_IMAGE_PATH[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//global var
var invoiceArrayIndex;
function editStnImagesListPopup(registrationId,arrayIndex){
	//initialize invoiceArrayIndex(This is useful when deltio of stn image happen)
	invoiceArrayIndex = arrayIndex;
	editListOfImagesPopup(registrationId,"stnImagePath",invoiceDetails[arrayIndex].STN_Image,"INWARD_GOODS_STN_IMAGE" , "/InwardGoodsRegistrationController/updateInwardGoodsRegInvoiceStnImage");
}
//Image updation Ajax call after getting result from server
function ajaxInwardGoodsInvoiceRegStnImageUpdate(data){
	if(data.RESULT==true) {
		invoiceDetails[invoiceArrayIndex].STN_Image = data.STN_Image;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Stn Image of Inward Goods Reg Invoice details
function deleteInwardGoodsRegInvoiceStnImage(stnImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardGoodsRegistrationController/deleteInwardGoodsRegInvoiceStnImage', {
			stnImageId : stnImageId
		}, function(data) {
			if(data.RESULT==true){
				invoiceDetails[invoiceArrayIndex].STN_Image[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//
function editClaimLetterImagesListPopup(claimLetterId){
	editListOfImagesPopup(claimLetterId,"claimLetter",commonData.CLAIM_LETTER,"INWARD_GOODS_CLAIM_LETTER_IMAGE" , "/InwardGoodsRegistrationController/updateInwardGoodsRegClaimLetterImage");
}
//
function ajaxInwardGoodsRegClaimLetterImageUpdate(data){
	if(data.RESULT==true) {
		commonData.CLAIM_LETTER = data.CLAIM_LETTER;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Claim Letter Image of Inward Goods Reg
function deleteInwardGoodsRegClaimLetterImage(claimLetterImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardGoodsRegistrationController/deleteInwardGoodsRegistrationClaimLetterImage', {
			claimLetterImageId : claimLetterImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.CLAIM_LETTER[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//
function editClaimPicturesImagesListPopup(claimLetterId){
	editListOfImagesPopup(claimLetterId,"uploadPictures",commonData.CLAIM_UPLOAD_PICTURE,"INWARD_GOODS_CLAIM_PICTURE_IMAGE" , "/InwardGoodsRegistrationController/updateInwardGoodsRegClaimPicturesImage");
}
//
function ajaxInwardGoodsRegClaimPicturesImageUpdate(data){
	if(data.RESULT==true) {
		commonData.CLAIM_UPLOAD_PICTURE = data.CLAIM_UPLOAD_PICTURE;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Claim Picture Image of Inward Goods Reg
function deleteInwardGoodsRegClaimPictureImage(claimPictureImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardGoodsRegistrationController/deleteInwardGoodsRegistrationClaimPictureImage', {
			claimPictureImageId : claimPictureImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.CLAIM_UPLOAD_PICTURE[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}

function editInvoiceDetails(index){
	var i = 0;
	var html="" 
		+ "<div class='modal fade ' id='modal-responsive777' >"
	    + "    <div class='col-md-13'>"
	    + "<form id='editInvoiceDetails' action='"+contextApplicationPath+"/InwardGoodsRegistrationController/editInwardGoodsRegistrationInvoiceDetails' method='post' enctype='multipart/form-data'>"
	    + "        <div class='modal-content'>"
	    + "            <div class='modal-header'>"
	    + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive777\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	    + "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Invoice Details</strong></h4>"
	    + "            </div>"
	    + "  		   <div class='panel panel-default'>"
	    + "			       <div class='panel-body'>"
	    + "    			       <div class='row'>"
	    + "       				   <div class='col-md-4'>"
		+"                                        					<div class='form-group'>"
		+"                                                                <label class='form-label'><strong>STN NO : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <input type='text' id='' value='"+invoiceDetails[(index-1)].STN_NO+"' name='stnNo' class='form-control' id='field-1' placeholder='STN NO' required>"
		+"                                                            </div>"   
		+"                                                             <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>GROSS AMOUNT : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <input type='text' id='' value='"+invoiceDetails[(index-1)].Gross_Amount+"' name='grossAmount' class='form-control' id='field-1' placeholder='GROSS AMOUNT' parsley-type='number' required>"
		+"                                                            </div>  "   
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>NET AMOUNT : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <input type='text' id='' value='"+invoiceDetails[(index-1)].Net_Amount+"' name='netAmount' class='form-control' id='field-1' placeholder='NET AMOUNT' parsley-type='number' required>"
		+"                                                            </div>"                                                      
		/*+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>STN IMAGE : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div id='stnImgDiv'>"
	    +"                                                                <img onClick='showImagePopup(\""+invoiceDetails[(index-1)].STN_Image+"\")' src='"+invoiceDetails[(index-1)].STN_Image+"' alt='Smiley face' height='100' width='100'>"
	    +"                                                                </div>"
	    +"                                                                </label>"
	    +"                                                                <span class='tips'></span>"
	    +"                                                                <input type='file' id='STN_FILE'  name='stnImagePath' class='form-control' id='field-1'>"
		+"                                                                </label>"
		+"                                                            </div>"*/
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>STN DATE: - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <input type='text' id='' value='"+invoiceDetails[(index-1)].STN_Date+"' name='stnDate' class='dateClassCommon form-control' id='field-1' placeholder='STN DATE' required>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>VAT AMOUNT : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <input type='text' id='' value='"+invoiceDetails[(index-1)].Vat_Amount+"' name='vatAmount' class='form-control' id='field-1' placeholder='VAT AMOUNT' parsley-type='number' required>"
		+"                                                            </div>    "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>DISCOUNT AMOUNT : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <input type='text' id='' value='"+invoiceDetails[(index-1)].Discount_Amount+"' name='discountAmount' class='form-control' id='field-1' placeholder='DISCOUNT AMOUNT' parsley-type='number' required>"
		+"                                                            </div>"
		+ "						 </div>"
		+ "  					<input type='hidden' value='"+invoiceDetails[(index-1)].INVOICE_ID+"' name='invoiceId'>" 
		+ " 			    </div>"
		+"             <div class='modal-footer text-center'>"
		+"                                                     <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editInvoiceDetails\")'>Update</button>"
		+"                                                     <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-success'>Cancel</button>"
	    + "            </div>"
		+"															</div>"
		+"														</div>"
		+"			</form>"
		+"													</div>"
		+"												</div>";
	$("#subPopUp").html(html);
	$(".dateClassCommon").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	ajaxEditInvoiceDetails("editInvoiceDetails",index);
	$("#modal-responsive777").addClass("in");
	$("#modal-responsive777").attr("aria-hidden","false");
	$("#modal-responsive777").css("display","block");
}
function ajaxEditInvoiceDetails(FormId,mainIndex)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						$('#stnNo_'+mainIndex).html(data.STN_NO);
						$('#stnDate_'+mainIndex).html(data.STN_Date);
						$('#stnGrossAmt_'+mainIndex).html(data.Gross_Amount);
						$('#stnVatAmt_'+mainIndex).html(data.Vat_Amount);
						$('#stnDiscountAmt_'+mainIndex).html(data.Discount_Amount);
						/*$('#stnNetAmt_'+mainIndex).html(data.Net_Amount);
						var txtHtml = "<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showImagePopup(\""+data.STN_Image+"\")'>";
						$('#stnImg_'+mainIndex).html(txtHtml);*/
						hideSubPopUp("modal-responsive777");
						alert(data.MSG);
						invoiceDetails[(mainIndex-1)].STN_NO=data.STN_NO;
						invoiceDetails[(mainIndex-1)].STN_Date=data.STN_Date;
						invoiceDetails[(mainIndex-1)].Gross_Amount=data.Gross_Amount;
						invoiceDetails[(mainIndex-1)].Vat_Amount=data.Vat_Amount;
						invoiceDetails[(mainIndex-1)].Discount_Amount=data.Discount_Amount;
						//invoiceDetails[(mainIndex-1)].STN_Image=data.STN_Image;
						invoiceDetails[(mainIndex-1)].Net_Amount=data.Net_Amount;
					}else
					alert(element.MSG);
					$('#loader').hide();
			}
		};
$('#' + FormId).ajaxForm(options);
}
function showCompanyDepoDropDown()
{
	
	var html="<select class='form-control' class='form-control' name='sendingDepo' required>"
	+"<option value='-1'>Selcet Depo</option>";
	
	$.post(contextApplicationPath+'/InwardGoodsRegistrationController/loadCompanyDepo',{orgId : $("#orgId").val() ,companyId : $("#companyName").val()}, function(data) {
		$(data).each(function(index,element){
			html+="<option value='"+element.COMP_DEPO_ID+"'>"+element.COMP_DEPO_NAME+"</option>"
		});
		html+="</select>";
		$('#depoList').html(html);
	}, 'json');
}