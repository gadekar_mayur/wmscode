function AddOrganization() {
	var html = ""
			+ "<div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                   <h3><strong>Organization Registration</strong></h3>"
			+ "              </div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-6'>"
			+ "                        <div class='tabcordion'>"
			+ "                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
			+ "                                <li id='orgDetails' class='active'><a href='#' data-toggle='tab' onclick=\"organizationDetails()\">Organization Details</a></li>"
			+ "                                <li id='orgBank' class=''><a href='#' data-toggle='tab' onclick=\"addOrgnizationBankDetails()\">Add Bank Details</a></li>"
			+ "                                <li class=''><a href='#' data-toggle='tab' onclick=\"addOrganizationDocument()\">Upload Document</a></li>"
			+ "                            </ul>"
			+ "                            <div id='myTabContent' class='tab-content'>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>"
			+ "                </div>";
	$('#main-content').html(html);
	$('#subMenu' + lastClickId).hide();
	checkLoginUser();
}

function checkLoginUser() {
	$.post(contextApplicationPath + '/OrganizationController/checkLoginUser',
			function(data) {
				$(data).each(function(index, element) {
					var userType = element.LOGIN_USER;
					if (userType == 'SuperAdmin') {
						organizationDetails();
					} else {
						$('#orgDetails').hide();
						$('#orgBank').addClass('active');
						addOrgnizationBankDetails();
					}
				});
			}, 'json');

}

function subTabsForOrganizationReg(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addOrgTabEntry1' class=''><a href='#tab2_6' data-toggle='tab' onclick='organizationDetails()'><h5><strong>Add Organization</strong></h5></a></li>"
			+ "                                            <li id='viewOrgTabEntry2' class=''><a href='#tab2_7' data-toggle='tab' onclick='organizationDetailsList(1)'><h5><strong>View Organization</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForOrganizationReg').html(html);
	if (index == 1) {
		$('#addOrgTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewOrgTabEntry2').addClass("active");
	}
}

function organizationDetails() {
	$('#loader').show();
	var html = ""
			+ "                                <div class='tab-pane fade active in' id='tab1_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>Add Organization</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForOrganizationReg'></div>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12'>"
			+ "                                            <div class='panel panel-default'>"
			+ "													<form id='addOrganizationDetails' action='"+contextApplicationPath+"/OrganizationController/saveOrganization' method='post'>"
			+ "                                                <div class='panel-body'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Organization Name</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Organization Name' name='organizationName' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Location</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Location' name='location' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Telephone</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Telephone' name='telephone' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>  "
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Email ID</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Email ID' name='emailId' class='form-control' required>"
			+ "                                                               </div>"
			+ "                                                          </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Website</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Website' name='website' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Address</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='textarea' placeholder='Address' name='address' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Contact Person</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Contact Person' name='contactPerson' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Mobile No</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Mobile No' name='mobileNo' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>User Name</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='User Name' name='userName' class='form-control' required>"
			+ "																	  <div id='UserName_Error' class='validationError'></div>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Password</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='password' placeholder='Password' name='password' class='form-control' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                                <div class='pull-right'>"
			+ "                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addOrganizationDetails\")'>Submit</button>"
			+ "                                                                    <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "												</form>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                </div>";
	$('#myTabContent').html(html);
	subTabsForOrganizationReg(1);
	ajaxAddOrganization('addOrganizationDetails');
	$('#loader').hide();

}
function ajaxAddOrganization(FormId) {
	var options = {
		success : function(json) {
			$(json).each(function(index, element) {
				if (element.MSG == 'User Name already exists') {
					$('#UserName_Error').html(element.MSG);
					ajaxAddOrganization('addOrganizationDetails');
					$('#loader').hide();
				} else {
					$('#' + FormId)[0].reset();
					alert(element.MSG);
					$('#loader').hide();
				}
			});
		}
	};
	$('#' + FormId).ajaxForm(options);
}

function organizationDetailsList(from){
	var i = 1;
	$('#loader').show();
	//$.post(contextApplicationPath+ '/OrganizationController/listOrganization',function(data) {
	var html = ""
			+ "                                         <div class='tab-pane fade active in' id='tab2_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Organization</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForOrganizationReg'></div>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12'>"
			+ "                                            <div class='panel panel-default'>"
			+ "                                                <div class='panel-body'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group' style='height: 58px;'>"
			+ "                                                                <label class='form-label'><strong>Search By</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <select id='orgSearchSelect' class='form-control' class='form-control'>"
			+ "                                                                        <option value='Organization'>Organization Name</option>"
			+ "                                                                        <option value='ContactPerson'>Contact Person</option>"
			//+ "                                                                        <option value='Mobile'>Mobile No.</option>"
			+ "                                                                        <option value='Email'>Email ID</option>"
			+ "                                                                    </select>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search For</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input id='searchText' type='text' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right'>"
			+ "                                                                <button class='btn btn-success m-b-10' onclick='searchOrganizationDetails(1)'>Show</button>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div id='orgSearchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                        </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+ "                                                    </div>"
			// +" </div>"
			+ "                                            </div>";
			$('#myTabContent').html(html);
			subTabsForOrganizationReg(2);
			commonOrganizationDetailsController("", "", from);
			//organizationDetailsListView(data);
			$('#loader').hide();
		//}, 'json');
}

function organizationDetailsListView(data, from, url) {
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i = 1;
	var html = ""
		+ "                                                            <table class='table table-striped table-hover'>"
		+ "                                                                <thead class='no-bd'>"
		+ "                                                                    <tr>"
		+ "                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Contact Person</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Mobile No.</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Email ID</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+ "                                                                    </tr>"
		+ "                                                                </thead>"
		+ "                                                                <tbody class='no-bd-y'>";

										$(data).each(function(index, element) {
										   if((data.length-1)<i)
											 return false;
										   html += " <tr style='text-align: center;'>"
												+ "  <td>"+ SR_NO + "</td>"
												+ "  <td id='ORG_NAME_"+i+"'>" + element.ORG_NAME + "</td>"
												+ "  <td id='CONTACT_PERSON_"+i+"'>"+ element.CONTACT_PERSON + "</td>"
												+ "  <td id='MOBILE_NO_"+i+"'>" + element.MOBILE_NO + "</td>"
												+ "  <td id='EMAIL_ID_"+i+"'>" + element.EMAIL_ID + "</td>"
												+ "  <td><a class='edit btn btn-blue' href='#' onclick=\"viewOrganiztion("+ element.ORG_ID+ ")\"><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' href='#' onclick='editOrganiztion("
												+ element.ORG_ID+","+i
												+ ")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteOrganiztion("
												+ element.ORG_ID
												+ ")\"><i class='fa fa-times-circle'></i> </a>"
												+ "  </tr>";
										i++;SR_NO++;
									});
					html += "                                                                </tbody>"
						 + "                                                            </table>";
						 
	$('#orgSearchResult').html(html);
	paginationView(from,data[data.length-1].paginationCount,url);
}

function viewOrganiztion(organizationId) {
	ViewOrganizationDetails(organizationId);
	// $("#modal-responsive").addClass("in");
	// $("#modal-responsive").attr("aria-hidden","false");
	// $("#modal-responsive").css("display","block");
}

function deleteOrganiztion(organizationId) {
	var r = confirm("Do you want to Delete Entry...!");
	if (r == true) {
		$.post(contextApplicationPath
				+ '/OrganizationController/deleteOrganiztion', {
			organizationId : organizationId
		}, function(data) {
			organizationDetailsList(1);
			alert(data.MSG);
		}, 'json');
	}
}

function editOrganiztion(organizationId,index) {
	 organizationEditForm(organizationId,index);
	// organizationDetails();
}
function searchOrganizationDetails(from) {
	var searchOption = $('#orgSearchSelect').val();
	var searchText = $('#searchText').val();
	if (!(Boolean(searchText))) {
		alert("Please Enter the search text");
		return false
	}
	commonOrganizationDetailsController(searchOption, searchText, from);
}

function commonOrganizationDetailsController(searchOption, searchText, from){
	$.post(contextApplicationPath+ '/OrganizationController/searchOrganizationDetails',{
		searchOption : searchOption,
		searchText : searchText,
		from : from
		},
		function(data) {
			var url = "commonOrganizationDetailsController(\""+searchOption+"\",\""+searchText+"\",";
			organizationDetailsListView(data, from, url);
		}, 'json');
}

function subTabsForOrganizationBank(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addOrgBankTabEntry1' class=''><a href='#tab2_2_1' data-toggle='tab' onclick='addOrgnizationBankDetails()'><h5><strong>Add Organization Bank</strong></h5></a></li>"
			+ "                                            <li id='viewOrgBankTabEntry2' class=''><a href='#tab2_2_2' data-toggle='tab' onclick='organiztionBankList()'><h5><strong>View Organization Bank </strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForOrganizationBank').html(html);
	if (index == 1) {
		$('#addOrgBankTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewOrgBankTabEntry2').addClass("active");
	}
}


function addOrgnizationBankDetails() {
	$('#loader').show();
	var html = ""
		+ "                                         <div class='tab-pane fade active in' id='tab2_2_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Organization Bank</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForOrganizationBank'></div>"
		+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12'>"
			+ "                                            <div class='panel panel-default'>"
			+ "                                                <div class='panel-body'>"
			+ "<form id='addOrganizationBank' action='"+ contextApplicationPath+ "/OrganizationController/saveOrganizationBank' method='post'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "<div id='organiztionDropDown'></div>";

	html += "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Branch*</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Branch' class='form-control' name='branchName' required>"
			+ "                                                                </div>"
			+ "                                                            </div> "
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>IFSC Code*</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='IFSC Code' class='form-control' name='ifscCode' required>"
			+ "                                                                </div>"
			+ "                                                            </div>      "
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Bank Name*</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Bank Name' class='form-control' name='bankName' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Account No*</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='text' placeholder='Account No' class='form-control' name='accountNo' required>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                                <div class='pull-right'>"
			+ "                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addOrganizationBank\")' >Add</button>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>   "
			+ "</form>"
			+ "                                                </div>"
			+ "                                            </div>"
//			+ "											  <div id='organiztionBankList'>"
//			+ "											  </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                </div>";
	$('#myTabContent').html(html);
	subTabsForOrganizationBank(1);
	loadOrganiztionDropDown();
	$('#loader').hide();
	ajaxAddOrganizationBank('addOrganizationBank');
//	organiztionBankList();
}
function ajaxAddOrganizationBank(FormId) {
	var options = {
		success : function(json) {
			$(json).each(function(index, element) {
				$('#loader').hide();
				$('#' + FormId)[0].reset();
				alert(element.MSG);
//				organiztionBankList();
				
			});
		}
	};
	$('#' + FormId).ajaxForm(options);
}
function loadOrganiztionDropDown() {
	var userType = '';
	var html = "<div class='form-group' style='height: 70px;'>"
			+ "<label class='form-label'><strong>Select Organization*</strong>"
			+ "</label>"
			+ "<span class='tips'></span>"
			+ "<div class='controls'>"
			+ "<select class='form-control' class='form-control' id='organiztionId' name='organiztionNameId' required>"
			+ "<option selected disabled>Select Organiztion</option>";
	var content = "<div class='form-group' style='height: 58px;'>"
			+ "<label class='form-label'><strong>Your Organization</strong>"
			+ "</label>" + "<span class='tips'></span>"
			+ "<div class='controls'>";
	$.post(contextApplicationPath+ '/OrganizationController/organizationDropDownList',function(data) { $(data).each(function(index, element) {
											userType = element.USER_TYPE;
											if (element.USER_TYPE == 'SuperAdmin') {
												html += "<option value='"
														+ element.ORG_ID + "'>"
														+ element.ORG_NAME
														+ "</option>";
											} else {
												content += "<input type='hidden' value='"
														+ element.ORG_ID
														+ "' name='organiztionNameId'>"
														+ ""
														+ element.ORG_NAME
														+ "";
											}
										});
						html += " </select>" + "</div>" + "</div>";
						content += "</div>" + "</div>";
						if (userType == 'SuperAdmin') {
							$('#organiztionDropDown').html(html);
						} else {
							$('#organiztionDropDown').html(content);
						}

					}, 'json');
}

function organiztionBankList() {
	
	$('#loader').show();
	//$.post(contextApplicationPath+'/OrganizationController/organiztionBankList',function(data) {
		var i = 0;
		var html  = ""
			+"<div class='tab-pane fade active in' id='tab2_2_2'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Organization Bank</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForOrganizationBank'></div>"
		+ "                                    <div class='row'>"
		+ "                                        <div class='col-md-12'>"
		+ "                                            <div class='panel panel-default'>"
		+ "                                                <div class='panel-body'>"
		+ "                                                    <div class='row'>"
	    + "                                                        <div class='col-md-4'>"
		+ "                                                            <div class='form-group' style='height: 58px;'>"
		+ "                                                                <label class='form-label'><strong>Search By</strong>"
		+ "                                                                </label>"
		+ "                                                                <span class='tips'></span>"
		+ "                                                                <div class='controls'>"
		+ "                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+ "                                                                        <option value='Organization' >Organization Name</option>"
		+ "                                                                        <option value='BankName'>Bank Name</option>"
		+ "                                                                        <option value='Branch'>Branch</option>"
		+ "                                                                        <option value='AccountNo'>Account No</option>"
		+ "                                                                        <option value='IFSC'>IFSC Code</option>"
		+ "                                                                    </select>"
		+ "                                                                </div>"
		+ "                                                            </div>"
		+ "                                                        </div>"
		+ "                                                        <div class='col-md-4'>"
		+ "                                                            <div class='form-group'>"
		+ "                                                                <label class='form-label'><strong>Search For</strong>"
		+ "                                                                </label>"
		+ "                                                                <span class='tips'></span>"
		+ "                                                                <div class='controls' id='searchControl'>"
		+ "                                                                    <input id='searchText' type='text' class='form-control'>"
		+ "                                                                </div>"
		+ "                                                            </div>"
		+ "                                                        </div>"
		+ "                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                                            <div class='pull-right'>"
		+ "                                                                <button class='btn btn-success m-b-10' onclick='searchOrganiztionBankDetails()'>Show</button>"
		+ "                                                            </div>"
		+ "                                                        </div>"
		+ "                                                        <div id='bankDetails' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+ "                                                        </div>"
		+"                                                     <div id='pagination' class='pagination'></div>"
		+ "                                                    </div>"
		+ "                                                </div>"
		+ "                                                </div>"
	//								+ "                                                        </div>"
	//								+ "                                                    </div>"
	//								+ "                                                </div>"
		+ "                                                </div>"
		+ "                                            </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html);
		commonOrganiztionBankDetailsController("","",1);
		subTabsForOrganizationBank(2);
		$('#organiztionBankList').html(html);
		//organiztionBankListView(data);
	//	$('#loader').hide();
//	}, 'json');
}
function organiztionBankListView(data, from, url) {
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	commonData = data;
	var i = 0;
	var html = ""
		+ "                                                            <table class='table table-striped table-hover'>"
		+ "                                                                <thead class='no-bd'>"
		+ "                                                                    <tr>"
		+ "                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Bank Name</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Branch</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Account No</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>IFSC Code</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+ "                                                                        </th>"
		+ "                                                                        </div>"
		+ "                                                                        </th>"
		+ "                                                                    </tr>"
		+ "                                                                </thead>"
		+ "                                                                <tbody class='no-bd-y'>";
		$(data).each(function(index, element) {
			
			if((data.length-2)<i)
				return false;
						html += "<tr style='text-align: center;'>"
								+ "<td>"
								+ SR_NO
								+ "</td>"
								+ "<td id='ORG_NAME"+i+"'>"
								+ element.ORG_NAME
								+ "</td>"
								+ "<td id='BANK_NAME"+i+"'>"
								+ element.BANK_NAME
								+ "</td>"
								+ "<td id='BRANCH_NAME"+i+"'>"
								+ element.BRANCH_NAME
								+ "</td>"
								+ "<td id='ACCOUNT_NO"+i+"'>"
								+ element.ACCOUNT_NO
								+ "</td>"
								+ "<td id='IFSC_CODE"+i+"'>"
								+ element.IFSC_CODE
								+ "</td>"
								+ "<td><a class='edit btn btn-dark' href='#' onclick=\"editOrganizationBank("
								+ element.ORG_BANK_ID+","+i
								+ ")\"><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteOrganizationBank("
								+ element.ORG_BANK_ID
								+ ")\"><i class='fa fa-times-circle'></i> </a>"
								+ "</tr>";
						i++;SR_NO++;
		});
			+"                                                                </tbody>"
			+ "                                                            </table>";
		$('#bankDetails').html(html);
		$('#loader').hide();
		paginationView(from,data[data.length-1].paginationCount,url);
}
function searchOrganiztionBankDetails() {
	
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if (!(Boolean(searchText))) {
		alert("Please Enter the search text");
		return false
	}
	commonOrganiztionBankDetailsController(searchOption,searchText,1);
}
	
function commonOrganiztionBankDetailsController(searchOption,searchText,from){

	$.post(
					contextApplicationPath
							+ '/OrganizationController/searchOrganiztionBankDetails',
					{
						searchOption : searchOption,
						searchText : searchText,
						from : from
					},
					function(data) {
						var url = "commonOrganiztionBankDetailsController(\""+searchOption+"\",\""+searchText+"\",";
						$('#loader').show();
						organiztionBankListView(data, from, url);
						$('#loader').hide();
					}, 'json');
}

function editOrganizationBank(organizationBankId,index) {

	organizationBankDetailsEdit(organizationBankId,index);
}
function deleteOrganizationBank(organizationBankId) {
	var r = confirm("Do you want to Delete Entry...!");
	if (r == true) {
		$.post(contextApplicationPath
				+ '/OrganizationController/deleteOrganizationBank', {
			organizationBankId : organizationBankId
		}, function(data) {
			organiztionBankList();
			alert(data.MSG);
		}, 'json');
	}
}

function addOrganizationDocument() {
	var html = ""
			+ "                                <div class='tab-pane fade active in' id='tab1_3'>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12'>"
			+ "                                            <div class='panel panel-default'>"
			+ "                                                <div class='panel-body'>"
			+ "<form id='addOrganizationDocument' action='"
			+ contextApplicationPath
			+ "/OrganizationController/saveOrganizationDocument' method='post' enctype='multipart/form-data'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "<div id='organiztionDropDown'></div>";

	html += "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Browse File</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input type='file' class='form-control' id='field-2' name='documentImage'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+"                                                            <div id='orgDocumentImageDivId'></div>"
			+"                                                            <div class='form-group'>"
			+"                                                				  <div id='orgDocumentImageAddMoreButtonId'><button onclick=\"addFileConrol('orgDocumentImageDivId','orgDocumentImageAddMoreButtonId','documentImage',1)\" class='btn btn-primary'>Add More Files</button></div>"
			+"                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Document Type*</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "<div id='documentList'></div>";

	html += "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                                <div class='pull-right'>"
			+ "                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addOrganizationDocument\")'>Submit</button>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "</form>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "											  <div id='organizationDocumentList'>"
			+ "											  </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                </div>";
	$('#myTabContent').html(html);
	loadOrganiztionDropDown();
	loadDocumentTypeList();
	ajaxAddOrganizationDocument('addOrganizationDocument');
	orgaiztionDocumentListing();
}
function ajaxAddOrganizationDocument(FormId) {
	var options = {
		success : function(json) {
			$(json).each(function(index, element) {
				$('#' + FormId)[0].reset();
				orgaiztionDocumentListing();
				alert(element.MSG);
				$('#loader').hide();
			});
		}
	};
	$('#' + FormId).ajaxForm(options);
}

function loadDocumentTypeList() {
	var html = "<select class='form-control' class='form-control' id='field-1' name='documentTypeId' parsley-required>"
			+ "<option selected disabled>Select Document Type</option>";

	$.post(contextApplicationPath + '/DocumentListTypController/documentList',
			function(data) {
				$(data).each(
						function(index, element) {
							html += "<option value='" + element.DOCUMENT_ID
									+ "'>" + element.DOCUMENT_NAME
									+ "</option>";
						});
				html += "</select>";
				$('#documentList').html(html);
			}, 'json');

}

function orgaiztionDocumentListing() {
	$('#loader').show();
	var html = ""
			+ "                                            <div class='panel panel-default'>"
			+ "                                                <div class='panel-body'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group' style='height: 58px;'>"
			+ "                                                                <label class='form-label'><strong>Search By</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <select id='searchOption' class='form-control' class='form-control'>"
			+ "                                                                        <option value='Organization'>Organization Name</option>"
			+ "                                                                        <option value='DocumentType'>Document Type</option>"
			+ "                                                                    </select>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search For</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input id='searchText' type='text' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right'>"
			+ "                                                                <button class='btn btn-success m-b-10' onclick='searchOrgaiztionDocumentDetails()'>Show</button>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+ "                                                </div>"
			+ "                                            </div>";
	$('#organizationDocumentList').html(html);
	commonOrgaiztionDocumentDetails("","",1);
	$('#loader').hide();
}

function commonOrgaiztionDocumentDetails(searchOption,searchText,from){
	$.post(contextApplicationPath+ '/OrganizationController/searchOrgaiztionDocumentDetails',
			{
				searchOption : searchOption,
				searchText : searchText,
				from : from
			},
			function(data) {
				var url = "commonOrgaiztionDocumentDetails(\""+searchOption+"\",\""+searchText+"\",";
				orgaiztionDocumentListingView(data,from,url);
			}, 'json');
}

function searchOrgaiztionDocumentDetails() {
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if (!(Boolean(searchText))) {
		alert("Please Enter the search text");
		return false
	}
	commonOrgaiztionDocumentDetails(searchOption,searchText,1);
}

function orgaiztionDocumentListingView(data,from,url) {
	//urlForEdit & commonData is global var
	urlForEdit = url;
	commonData = data;
	var i = 0;
    SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html = ""
		+ "                                                            <table class='table table-striped table-hover'>"
		+ "                                                                <thead class='no-bd'>"
		+ "                                                                    <tr>"
		+ "                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Document Type</strong>"
		+ "                                                                        </th>"
		// +" <th style='text-align:
		// center;'><strong>Document Code</strong>"
		// +" </th>"
		+ "                                                                        <th style='text-align: center;'><strong>File Name</strong>"
		+ "                                                                        </th>"
		+ "                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+ "                                                                        </th>"
		+ "                                                                        </div>"
		+ "                                                                        </th>"
		+ "                                                                    </tr>"
		+ "                                                                </thead>"
		+ "                                                                <tbody class='no-bd-y'>"
		$(data).each(function(index, element) {
			if((data.length-2)<i)
				return false;
			
					html += " <tr style='text-align: center;'>"
							+ " <td>"+ SR_NO + "</td>"
							+ " <td>"
							+ element.ORG_NAME
							+ "</td>"
							+ " <td>"
							+ element.DOCUMENT_TYPE
							+ "</td>"
							+ " <td><img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showorgDocumentFileListPopup("+i+")'></td>"// +element.FILE_NAME+"</td>"
							+ " <td><a class='edit btn btn-dark' href='#' onclick='editOrganizationDocument("+ i + ","+from+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteOrganizationDocument("
							+ element.ORG_DOCUMENT_ID+")\"><i class='fa fa-times-circle'></i> </a></td>"
							+ " </tr>";
					i++;SR_NO++;
		});
   html +="                                                                </tbody>"
		+ "                                                            </table>";
   $('#searchResult').html(html);
   
   paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
}

function showorgDocumentFileListPopup(index){
	showListOfImagesPopup(commonData[index].FILE_NAME);
}

function editOrganizationDocument(index,from) {
	editOrganizationDocumentForm(index, from);
}

function deleteOrganizationDocument(organizationDocumentId) {
	var r = confirm("Do you want to Delete Entry...!");
	if (r == true) {
		$.post(contextApplicationPath
				+ '/OrganizationController/deleteOrganizationDoc', {
			organizationDocumentId : organizationDocumentId
		}, function(data) {
			orgaiztionDocumentListing();
			alert(data.MSG);
		}, 'json');
	}
}