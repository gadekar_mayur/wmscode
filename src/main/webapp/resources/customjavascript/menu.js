function menu()
{
	var i=1;
	var content='';
	$.post(contextApplicationPath+'/MenuController/loadMenu', function(data) {
		$(data).each(function(index, element) {
			content+="<li>" +
			"<a href='#' id='"+i+"' onclick='subMenuToggle(this.id)'><i class='glyph-icon flaticon-charts2'></i><span class='sidebar-text'>"+element.MENU+"</span><span class='fa arrow'></span></a>" +
			"<ul style='display: none;' id='subMenu"+i+"' class='submenu collapse'>" ;
			var subMenu=element.SUBMENUARRAY;
			$(subMenu).each(function(index,element){
				content+="<li>" +
				"<a href='#' onclick=\""+element.SUBMENU_FUNCTION+"\"><span class='sidebar-text'>"+element.SUBMENU+"</span></a>" +
				"</li>";
			});
			content+="</ul>"+
				"</li>" ;
			i++;
		});
		$('#Menu').html(content);
		//addDashboardTable();
	}, 'json');
}

var lastClickId=null;
function subMenuToggle(id)
{
	prevClicked = lastClickId;
	$('#subMenu'+id).toggle();
	if(lastClickId!=id)
		{
		$('#subMenu'+prevClicked).hide();
		}
	lastClickId=id;
}