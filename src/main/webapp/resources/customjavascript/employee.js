function AddEmployee()
{
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>Employee Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class='tabcordion'>"
		+"                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#tab1_1' data-toggle='tab' onclick=\"employeeDetails()\">Employee Details</a></li>"
		+"                                <li class=''><a href='#tab1_2' data-toggle='tab' onclick=\"employeeUploadDocument()\">Upload Document</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	employeeDetails();
}

function subTabsForAddEmployeeDetails(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addEmployeeDetailsTabEntry1' class=''><a href='#tab1_1' data-toggle='tab' onclick='employeeDetails()'><h5><strong>Add Employee Details</strong></h5></a></li>"
			+ "                                            <li id='viewEmployeeDetailsTabEntry2' class=''><a href='#tab1_1_1' data-toggle='tab' onclick='employeeDetailsList()'><h5><strong>View Employee Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddEmployeeDetails').html(html);
	if (index == 1) {
		$('#addEmployeeDetailsTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewEmployeeDetailsTabEntry2').addClass("active");
	}
}
function employeeDetails()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Employee Details</strong></h3>"
		+ "                                    </div>"
		+" <div id='subTabsForAddEmployeeDetails'></div> "
		 +"		                                 <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"<form id='addEmployee' action='"+contextApplicationPath+"/EmployeeController/saveEmployee' method='post'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"<div id='organiztionDropDown'></div>";
		
		html+="                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Employee Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Employee Name' class='form-control' name='eName' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Blood Group*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"<div id='bloodGroupList'></div>";
		
		
		
		html+="                                                                </div>"
		+"                                                            </div>  "                                              
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Role*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"<div id='roleTypeDropDown'></div>";
		
		
		
		html+="                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Marital Status*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select class='form-control' class='form-control' name='MaritalStatus' required>"
		+"                                                                        <option selected disabled>Select Marital Status</option>"
		+"                                                                        <option value='Married'>Married</option>"
		+"                                                                        <option value='Unmarried'>Unmarried</option>  +"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"                                                      
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Mobile No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Mobile No' class='form-control' name='mobileNo' parsley-type='onlynumber' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Current Address*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='textarea' placeholder='Current Address' class='form-control' id = 'currentAddress' name='currentAddress' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='from-group' >"
		+"                                                                <div class='div_checkbox'>"
		+"                                                                    <input type='checkbox'  parsley-group='mygroup3'  class='parsley-validated' onclick='PermanantAddress()' style='opacity: 1 !important;'>  Same as Current"
		+"                                                                </div>"
		+"                                                                <label class='form-label'><strong>Permanent Address*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Permanent Address' class='form-control' id = 'PermanentAddress' name='PermanentAddress' required>"
		+"                                                                </div>"
		+"                                                            </div>"               
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Date of Joining*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='datepicker form-control' type='text' placeholder='Date of Joining' style='height: 36px; padding-left: 10px;' id='doj' name='doj' required>"
		+"                                                                </div>"
		+"                                                            </div>"                                                         
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Image*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' name='empImage'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='employeeImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='employeeImageAddMoreButtonId'><button onclick=\"addFileConrol('employeeImageDivId','employeeImageAddMoreButtonId','empImage',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"<div id='companyDropDown'></div>";
		html+="                                                            <div class='form-group'>"					//style='height: 58px;'
		+"                                                                <label class='col-lg-12 control-label' style='padding-left: 0px;'><strong>Gender*</strong></label>"
		+"                                                                <div class='col-lg-12'>"
		+"                                                                    <div class='pos-rel'>"
		+"                                                                        <input tabindex='13' id='ctest' type='radio' name='sex' value='Male' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;'>"
		+"                                                                        <label class='p-l-40' for='flat-checkbox-1'>Male</label>"
		+"                                                                    </div>"
		+"                                                                    <div class='pos-rel'>"
		+"                                                                        <input tabindex='14' id='ctest' type='radio' name='sex' value='Female' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;'>"
		+"                                                                        <label class='p-l-40' for='flat-checkbox-2'>Female</label>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>DOB*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='datepicker form-control' type='text' placeholder='Select DOB' style='height: 36px; padding-left: 10px;' id='dob' name='dob' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Qualification*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Qulification' class='form-control' name='qulification' required>"
		+"                                                                </div>"
		+"                                                            </div>"  
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Telephone No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Telephone No' class='form-control' name='telephoneNo' parsley-type='onlynumber'>"
		+"                                                                </div>"
		+"                                                            </div>"                  
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Email ID</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Email ID' class='form-control' name='emailId' >"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Pincode*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Pincode' class='form-control' name='pincode' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>PAN No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='PAN Nos' class='form-control' name='panno' >"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Basic Salary</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Basic Salary' class='form-control' name='basicSalary' parsley-type='number'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Address Proof*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' name='AddressProofImage'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='addressProofImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='addressProofImageAddMoreButtonId'><button onclick=\"addFileConrol('addressProofImageDivId','addressProofImageAddMoreButtonId','AddressProofImage',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>UserName*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' name='uname' placeholder='User Name' required>"
		+"																	  <div id='UserName_Error' class='validationError'  style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Password*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='password' class='form-control' name='pwd' placeholder='Password' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick=\"validateFormDetails('#addEmployee')\">Save</button>"
		+"                                                                    <button type='reset' class='btn btn-danger m-b-10' data-dismiss='modal'>Cancel</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"</form>"
//		+"											  <div id='employeeDetailsList'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddEmployeeDetails(1);
	loadEmployeeOrganiztionDropDown();
	loadRoleTypeDropDown();
	loadBloodGroupDropDown();
	$("#dob").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	$("#doj").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	ajaxAddEmployee('addEmployee');
//	employeeDetailsList();
}

function ajaxAddEmployee(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					if(element.MSG=='User Name already exists')
						{
						$('#UserName_Error').html(element.MSG);
						ajaxAddEmployee('addOrganizationDetails');
						$('#loader').hide();
						}
					else
						{
						$('#'+ FormId)[0].reset();
						alert(element.MSG);
//						employeeDetailsList();
						$('#loader').hide();
						}
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function PermanantAddress()
{
	if($('#PermanentAddress').val())
		{
		  $('#PermanentAddress').val('');
		}
	else{
	var addr = $('#currentAddress').val()
   $('#PermanentAddress').val(addr);
	}
}
function loadRoleTypeDropDown()
{
	var html="<select class='form-control' class='form-control' name='roleType' required>"
	+"<option disabled selected>Select Employee Role</option>";
	$.post(contextApplicationPath+'/EmployeeController/loadRoleType', function(data) {
		$(data).each(function(index,element){
			html+= "<option value='" + element.ROLE_TYPE_ID+ "'>"+ element.ROLE_TYPE + "</option>";
		});
		html+=" </select>";
		$('#roleTypeDropDown').html(html);
	}, 'json');
}

function loadBloodGroupDropDown()
{
	var html=" <select class='form-control' class='form-control' name='bloodroupName' required>"
	+" <option disabled selected>Select Blood Group</option>";
	$.post(contextApplicationPath+'/EmployeeController/bloodGroupList', function(data) {
		$(data).each(function(index,element){
			html += "<option value='" + element.BLOOD_GROUP_ID+ "'>"+ element.BLOOD_GROUP + "</option>";
		});
		html+=" </select>";
		$('#bloodGroupList').html(html);
	}, 'json');
}


function loadEmployeeOrganiztionDropDown()
{
	var userType='';
	var html="<div class='form-group'>"					//style='height: 58px;'
	+"<label class='form-label'><strong>Select Organization</strong>"
	+"</label>"
	+"<span class='tips'></span>"
	+"<div class='controls'>"
	+"<select class='form-control' class='form-control' id='dropDownorganiztionId' name='selectedOrganiztionNameId' onchange='organizationChange()' required>" 
	+"<option selected disabled>Select Organization</option>";
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
		$(data).each(function(index, element) {
			userType=element.USER_TYPE;
			if(element.USER_TYPE=='SuperAdmin')
				{
				html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
				}
			else
				{
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='selectedOrganiztionNameId' id='orgId'>" +
						""+element.ORG_NAME+"";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
		content+="</div>"
		+"</div>";
		if(userType=='SuperAdmin')
			{
			$('#organiztionDropDown').html(html);
			}
		else
			{
			$('#organiztionDropDown').html(content);
			loadCompany($("#orgId").val());
			}
		
	}, 'json');
}

function organizationChange()
{
	loadCompany($("#dropDownorganiztionId").val());
}

function loadCompany(orgId)
{
	$.post(contextApplicationPath+'/CompanyController/getCompanies',{orgId : orgId }, function(data) {
		html="<div class='form-group'>"
			+"<label class='form-label'><strong>Select Company*</strong>"
			+"</label>"
			+"<span class='tips'></span>"
			+"<div class='controls'>"
			+"<select class='form-control' class='form-control' name='comapnyId'  parsley-required='true'>"
			+"<option selected disabled>Select Company</option>";
		for (var i = 0; i < data.companyArray.length; i++)
		{
			html += "<option value='" + data.companyArray[i].compId + "'>"+ data.companyArray[i].compName + "</option>";
		}
		html+="</select>"
		+"</div>"
		+"</div>";
		$('#companyDropDown').html(html);
	}, 'json');
}

function employeeDetailsList()
{
	var i=1;
	var html='';
	$('#loader').show();
//	$.post(contextApplicationPath+'/EmployeeController/listEmployeeDetails', function(data) {
		 html+=""
				+"                                <div class='tab-pane fade active in' id='tab1_1_1'>"
				+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
				+ "                                        <h3><strong>View Employee Details</strong></h3>"
				+ "                                    </div>"
				+" <div id='subTabsForAddEmployeeDetails'></div> "
			 +"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
			+"                                                                        <option value='Organization'>Organization Name</option>"
			//+"                                                                        <option value='Company'>Company Name</option>"
			+"                                                                        <option value='EmployeeName'>Employee Name</option>"
			+"                                                                        <option value='Gender'>Gender</option>"
			+"                                                                        <option value='Qualification'>Qualification</option>"
//			+"                                                                        <option value='Mobile'>Mobile No</option>"
			+"                                                                        <option value='Email'>Email ID</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchEmployeeDetails(1)'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>";
			$('#myTabContent').html(html);
			subTabsForAddEmployeeDetails(2);
			$('#employeeDetailsList').html(html);
			commonEmployeeDetails("","",1);
//			employeeDetailsListView(data);
			$('#loader').hide();
//	}, 'json');	
}
function commonEmployeeDetails(searchOption,searchText,from){
	$.post(contextApplicationPath+'/EmployeeController/searchEmployeeDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, 	function(data) {
		var url = "commonEmployeeDetails(\""+searchOption+"\",\""+searchText+"\",";
		employeeDetailsListView(data,from,url);
	}, 'json');
}
//
function employeeDetailsListView(data,from,url){
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	//alert("employeeDetailsListView");
	var i=0;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Employee Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Gender</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Qualification</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Mobile No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Email ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
			
			html+="<tr style='text-align: center;'>"
				+" <td>"+ SR_NO +"</td>"
				+" <td>"+element.ORGANIZATION+"</td>"
				+" <td id='EMP_NAME"+i+"'>"+element.EMP_NAME+"</td>"
				+" <td id='GENDER"+i+"'>"+element.GENDER+"</td>"
				+" <td id='Qualification"+i+"'>"+element.Qualification+"</td>"
				+" <td id='Mobile_No"+i+"'>"+element.Mobile_No+"</td>"
				+" <td id='Email_ID"+i+"'>"+element.Email_ID+"</td>"
				+" <td><a class='edit btn btn-blue' href='#' onclick=\"viewEmployeeDetails("+element.EMP_DETAILS_ID+")\"><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' href='#' onclick=\"editEmployeeDetails("+element.EMP_DETAILS_ID+","+i+")\"><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteEmployeeDetails("+element.EMP_DETAILS_ID+")\"><i class='fa fa-times-circle'></i> </a>"
				+" </tr>";
			i++;SR_NO++;
		});
		html+="                                                                </tbody>"
			+"                                                            </table>";
		$('#searchResult').html(html);
		   paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
}
function searchEmployeeDetails(from){
	var searchOption = $('#searchOption').val();//searchOption searchText
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText.trim()))){
		alert("Please Enter the search text");
		return false
	}
	commonEmployeeDetails(searchOption,searchText,from);
//	$.post(contextApplicationPath+'/EmployeeController/searchEmployeeDetails', {
//		searchOption : searchOption,
//		searchText : searchText
//	}, function(data) {
//		employeeDetailsListView(data);
//	}, 'json');	
}
function viewEmployeeDetails(empDetailsId)
{
	employeeViewDetailsPopUp(empDetailsId);
}

function deleteEmployeeDetails(empDetailsId)
{
	//alert("in delete Employee Details ="+empDetailsId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/EmployeeController/deleteEmployeeDetails', {
			empDetailsId : empDetailsId
		}, function(data) {
			employeeDetailsList(1);
			alert(data.MSG);
		}, 'json');
    }
}

function subTabsForAddemployeeUploadDocument(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addemployeeUploadDocumentTabEntry1' class=''><a href='#tab1_2' data-toggle='tab' onclick='employeeUploadDocument()'><h5><strong>Employee Upload Document</strong></h5></a></li>"
			+ "                                            <li id='viewemployeeUploadDocumentTabEntry2' class=''><a href='#tab1_2_1' data-toggle='tab' onclick='employeeDocumetList()'><h5><strong>View Employee Uploaded Document</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddemployeeUploadDocument').html(html);
	if (index == 1) {
		$('#addemployeeUploadDocumentTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewemployeeUploadDocumentTabEntry2').addClass("active");
	}
}
function employeeUploadDocument()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_2'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Employee Upload Document</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddemployeeUploadDocument'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"<form id='addEmployeeDocument' action='"+contextApplicationPath+"/EmployeeController/saveEmployeeDocument' method='post' enctype='multipart/form-data'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"<div id='employeeUploadDocumentOrganiztionDropDown'></div>";
		
		
		html+="															  <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Employee*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"<div id='employeeDropDown'></div>";
		
		
		
		html+="</div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Document Type*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"<div id='documentList'></div>";
		
		
		
		html+="                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Browse File*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' id='field-2' name='documentName'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='employeeDocumentImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='employeeDocumentImageAddMoreButtonId'><button onclick=\"addFileConrol('employeeDocumentImageDivId','employeeDocumentImageAddMoreButtonId','documentName',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>" 
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addEmployeeDocument\")'>Submit</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='employeeDocumentListing'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddemployeeUploadDocument(1);
	loadEmployeeDocumentOrganiztionDropDown();
	loadDocumentTypeList();
	ajaxAddEmployeeDocument('addEmployeeDocument');
//	employeeDocumetList();
}

function ajaxAddEmployeeDocument(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#'+FormId)[0].reset();
					alert(element.MSG);
//					employeeDocumetList();
					$('#loader').hide();
				});
			}
		};
$('#'+FormId).ajaxForm(options);
}
function loadEmployeeDocumentOrganiztionDropDown()
{
	var userType='';
	var html="<div class='form-group'>"
	+"<label class='form-label'><strong>Select Organization</strong>"
	+"</label>"
	+"<span class='tips'></span>"
	+"<div class='controls'>"
	+"<select class='form-control' class='form-control' id='empUploadDocumetOrgId' name='empUploadDocumetOrgId' onchange='changeEmployeeUploadDocumentOrgnaiation()' required>" 
	+"<option selected disabled>Select Organiztion</option>";
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
		$(data).each(function(index, element) {
			userType=element.USER_TYPE;
			if(element.USER_TYPE=='SuperAdmin')
				{
				html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
				}
			else
				{
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='empUploadDocumetOrgId' id='empUploadDocumetOrgId'>" +
						""+element.ORG_NAME+"";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
		content+="</div>"
		+"</div>";
		if(userType=='SuperAdmin')
			{
			$('#employeeUploadDocumentOrganiztionDropDown').html(html);
			}
		else
			{
			$('#employeeUploadDocumentOrganiztionDropDown').html(content);
			loadEmployee($("#empUploadDocumetOrgId").val());
			}
		
	}, 'json');
}




function changeEmployeeUploadDocumentOrgnaiation()
{
	loadEmployee($("#empUploadDocumetOrgId").val());
}


function loadEmployee(organizationId)
{
var html="<select class='form-control' class='form-control' name='employeeDetailsId' required>"
	+"<option selected disabled>Select Employee</option>";
		$.post(contextApplicationPath+'/EmployeeController/loadEmployeeList',{organizationId : organizationId} ,function(data) {
			$(data).each(function(index, element) {
				html+="<option value='"+element.EMP_ID+"'>"+element.EMP_NAME+"</option>";
			}); 
			html+="</select>";
			$('#employeeDropDown').html(html);
		}, 'json');
}




function employeeDocumetList()
{
	//$('#loader').show();
	//$.post(contextApplicationPath+'/EmployeeController/employeeDocumentList' ,function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab1_2'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Employee Uploaded Document</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddemployeeUploadDocument'></div>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-12'>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control'>"
			+"                                                                        <option value='Organization'>Organization Name</option>"
			+"                                                                        <option value='EmployeeName'>Employee Name</option>"
			+"                                                                        <option value='DocumentType'>Document Type</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls' id='searchControl'>"
			+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchEmployeeDocumet()'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                       		 <div class='pull-right'>"
			+"                                                       		 </div>"
			+"                                                       	 </div>"
			+"                                                        </div>"
			+"                                                <div class='row'>"
			+"                                                    <div class='col-md-6'>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+"                                                     <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                	</div>"
		+"                                                    	</div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html);
		$('#employeeDocumentList').html(html);
		commonEmployeeDocumetController("","",1);
		subTabsForAddemployeeUploadDocument(2);
		//employeeDocumetListView(data);
	//$('#loader').hide();
	//}, 'json');
}

function employeeDocumetListView(data,from,url){
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	urlForEdit = url;
	commonData = data;
	var i=0;
	var html = ""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Employee Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Document Type</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>File Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="<tr style='text-align: center;'>"
		+"<td>"+SR_NO+"</td>"
		+"<td id='ORG_NAME_"+i+"'>"+element.ORG_NAME+"</td>"
		+"<td id='EMP_NAME_"+i+"'>"+element.EMP_NAME+"</td>"
		+"<td id='DOCUMENT_TYPE_"+i+"'>"+element.DOCUMENT_TYPE+"</td>"
		+"<td><img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showEmployeeDocumentFileListPopup("+i+")'></td>"//+element.FILE_NAME+"</td>"
		+"<td><a class='edit btn btn-dark' href='#' onclick=\"editEmployeeDocumet("+i+","+from+")\"><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteEmployeeDocumet("+element.EMP_DOCUMENT_ID+")\"><i class='fa fa-times-circle'></i> </a></td>"
		+"</tr>";
		i++;SR_NO++;
	});
	+"                                                                </tbody>"
	+"                                                            </table>";
	$('#searchResult').html(html);
	//$('#loader').hide();
	paginationView(from,data[data.length-1].paginationCount,url);
}
//
function showEmployeeDocumentFileListPopup(index){
	showListOfImagesPopup(commonData[index].FILE_NAME);
}
function searchEmployeeDocumet(){
	var searchOption = $('#searchOption').val();//searchOption searchText
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText.trim()))){
		alert("Please Enter the search text");
		return false
	}
	commonEmployeeDocumetController(searchOption, searchText, 1);
}	
	
function commonEmployeeDocumetController(searchOption, searchText, from){
	$.post(contextApplicationPath+'/EmployeeController/searchEmployeeDocumet', {
		searchOption : searchOption,
		searchText : searchText,
		from :from
	}, function(data) {
		var url = "commonEmployeeDocumetController(\""+searchOption+"\",\""+searchText+"\",";
		employeeDocumetListView(data,from,url);
	}, 'json');
}
function deleteEmployeeDocumet(empDocumentId)
{
	//alert("in delete Employee Documet="+empDocumentId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/EmployeeController/deleteEmployeeDocumet', {
			empDocumentId : empDocumentId
		}, function(data) {
			employeeDocumetList();
			alert(data.MSG);
		}, 'json');
    }
}

function editEmployeeDocumet(index, from)
{
	editEmployeeDocumetForm(index, from);
}