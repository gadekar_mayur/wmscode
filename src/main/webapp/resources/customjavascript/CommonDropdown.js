function transporterMultiSelectDropDown(){

	var html = "<select name='transporterMultiSelect[]' multiple id='transporterId'>";
	$.post(contextApplicationPath+'/OutWardGoodsController/getTransporterDropDownList',// function(data) {
		function(data) {
			$(data).each(
					function(index, element) {
						html += "<option value='" + element.TRANSPORTER_ID + "'>"
						+ element.TRANSPORTER + "</option>";
					});
			html += "</select>";
		$("#TransporterSelectDiv").empty();
		$("#TransporterSelectDiv").html(html);
		$('#transporterId').multiselect();
	}, 'json');
}

//common dropdown loader for organization multiple select
function loadOrganizatonsMultiSelectDropDown() {
	var html = "<select name='organizationSelected[]' multiple id='organizationSelect'>";

	$.post(contextApplicationPath + '/OrganizationController/organizationDropDownList',
			function(data) {
				$(data).each(
						function(index, element) {
							html += "<option value='" + element.ORG_ID + "'>"
									+ element.ORG_NAME + "</option>";
						});
				html += "</select>";
				$("#organizationSelectDiv").empty();
				$("#organizationSelectDiv").html(html);
				$('#organizationSelect').multiselect({
					 onControlClose: function( element ){
						// loadStockistsMultiSelectDropDown();
					 }
				});
			}, 'json');

}

//common dropdown loader for company multiple select
function loadCompaniesMultiSelectDropDown() {
	var html = "<select name='companySelected[]' multiple id='companySelect'>";

	$.post(contextApplicationPath + '/CompanyController/getCompaniesList',
			function(data) {
				$(data).each(
						function(index, element) {
							html += "<option value='" + element.compId + "'>"
									+ element.compName + "</option>";
						});
				html += "</select>";
				$("#companySelectDiv").empty();
				$("#companySelectDiv").html(html);
				$('#companySelect').multiselect({
					 onControlClose: function( element ){
						 loadStockistsMultiSelectDropDown();
					 }
				});
			}, 'json');

}
//common dropdown loader for stockist multiple select (called on company multiselect close event)
function loadStockistsMultiSelectDropDown() {
	$("#loader").show();
	var selectedValues = $('#companySelect').val();
	var html = "<select name='stockistSelected[]' multiple id='stockistSelect'>";
	if(selectedValues != null)
		selectedValues=selectedValues.toString()
	$.post(contextApplicationPath + '/StockistController/getStockistOfCompany',
			{
				company : selectedValues
			}, function(data) {
				$(data).each(
						function(index, element) {
							html += "<option value='" + element.STOCKIST_ID
									+ "'>" + element.STOCKIST_NAME
									+ "</option>";
						});
				html += "</select>";
				$("#stockistSelectDiv").empty();
				$("#stockistSelectDiv").html(html);
				$('#stockistSelect').multiselect();
				$("#loader").hide();
			}, 'json');
	
}

function toggleFilter() {

	var el = document.getElementById("filterToggle");

	if ( el.style.display != 'none' ) {

		el.style.display = 'none';

	}

	else {

		el.style.display = 'block';

	}

}

function setYears() {
	var i,yr,yr2,now = new Date();
	for (i=0; i>= -20 ; i--) {
	    yr = (now.getFullYear()+i).toString(); 
	    yr2= (now.getFullYear()+i+1).toString().substring(2,4);
	    $('#yearSelect').append($('<option/>').val(yr).text(yr+"-"+yr2));
	};
}

function filterViewLoader() {
	var html=""
		+ " <div class='col-sm-9 col-sm-offset-3'>"
		+ "       <div class='pull-right' style='padding-left: 10px;'>"
		+ "           <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
		+ "       </div>"
		+ "  </div>"
		+ "  <div id='filterToggle'  style='display:none'>"	
		+ "      <div class='row' id='companyWise'>"
		+ "           <div class='col-md-4'>"
		+ "               <div class='form-group'>"
		+ "                    <label class='form-label'><strong>Search By</strong>"
		+ "                    </label>"
		+ "                    <span class='tips'></span>"
		+ "                    <div class='controls'>"
		+ "							<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		// + " 							<option value='Stockist'>Stockist</option>"
		+ "                         	<option value='TripNo'> Trip Number</option>"
		// +" 							<option value='Organization'>Organization</option>"
		// + " 							<option value='Company'>Company Name</option>"
		// + " 							<option value='Stockist'>Stockist</option>"
		+ "                         	<option value='Transporter'>Transporter</option>"
		+ "                         	<option value='InvoiceNo'>Invoice Number</option>"
		+ "                         	<option value='NoOfCases'>No. of Cases</option>"
		+ "                         	<option value='LRNo'>LR Number</option>"
		+ "							</select>"
		+ "                     </div>"
		+ "                 </div>"
		+ "             </div>"
		+ "             <div class='col-md-4'>"
		+ "                 <div class='form-group'>"
		+ "                     <label class='form-label'><strong>Search For</strong>"
		+ "                     </label>"
		+ "                     <span class='tips'></span>"
		+ "                     <div class='controls' id='searchControl'>"
		+ "                     	<input id='searchText' type='text' class='form-control'>"
		+ "                 	</div>"
		+ "             	</div>"
		+ "         	</div>"
		+ "    	   </div>"
		+ "        <div class='row'>"
		+ "            <div class='col-md-4'>"
		+ "                 <div class='form-group'>"
		+ "                     <label class='form-label'><strong>Organization</strong>"
		+ "                     </label>"
		+ "                     <span class='tips'></span>"
		+ "                     <div class='controls' id='searchControl'>"
		+ "                          <div id='organizationSelectDiv'>"
		+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
		+ "                               </select>"
		+ "							 </div>"
		+ "                     </div>"
		+ "                  </div>"
		+ "              </div>"
		
		+ "            <div class='col-md-4'>"
		+ "                 <div class='form-group'>"
		+ "                     <label class='form-label'><strong>Company</strong>"
		+ "                     </label>"
		+ "                     <span class='tips'></span>"
		+ "                     <div class='controls' id='searchControl'>"
		+ "                          <div id='companySelectDiv'>"
		+ "                               <select name='companySelected[]' multiple id='companySelect'>"
		+ "                               </select>"
		+ "							 </div>"
		+ "                     </div>"
		+ "                  </div>"
		+ "              </div>"
		+ "              <div class='col-md-4'>"
		+ "                   <div class='form-group'>"
		+ "                        <label class='form-label'><strong>Stockist</strong>"
		+ "                        </label>"
		+ "                        <span class='tips'></span>"
		+ "                        <div class='controls' id='searchControl'>"
		+ "                              <div id='stockistSelectDiv'>"
		+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
		+ "                                   </select>"
		+ "								 </div>"
		+ "                        </div>"
		+ "                    </div>"
		+ "              </div>"
		+ "              <div class='col-md-4'>"
		+ "                   <div class='form-group'>"
		+ "                       <label class='form-label'><strong>Date</strong>"
		+ "                       </label>"
		+ "                       <span class='tips'></span>"
		+ "                       <div class='controls'>"
		+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
		+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
		+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
		+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
		+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
		+ "                        </div>"
		+ "                   </div>"
		+ "                </div>"
		+ "         </div>"
		+ "         <div class='col-sm-9 col-sm-offset-3'>"
		+ "                <div class='pull-right' style='padding-left: 10px;'>"
		+ "                      <button class='btn btn-success m-b-10' onclick='searchViewGetPassLRList()'>Show</button>"
		+ "						 <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onclick='addGetPassLR1()'>Report </a>"
		+ "                 </div>"
		+ "         </div>"
		+ "			<div class='pull-right'>"
		+ "         </div>"
		+ "    </div>";
}