//Add by Nutan
function inwardCourierGoodsPopUp(inwardCourierRegId)
{
	var html="" 
		+ "<div class='modal fade in' id='modal-responsive' >"
		            + "    <div class='col-md-13'>"
		            + "        <div class='modal-content'>"
		            + "            <div class='modal-header'>"
		            + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		            + "                <h4 class='modal-title' id='myModalLabel'><strong>View Inward Courier Goods</strong></h4>"
		            + "            </div>"
		            + "<div class='panel-body'>"
		            + "    <div class='row'>"
		            + "       <div class='col-md-4'>";
	
	$.post(contextApplicationPath+'/InwardCourierController/viewInwardCourierRegistration', {inwardCourierRegId : inwardCourierRegId}, function(data) {
		$(data).each(function(index,element){
			//commonData is used for showing docket file image list 
			commonData = element;
			   html+= "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Organization Name : - </strong>"
		            + "               </label>"
		            + "				  <label class='form-label'><strong>"+element.ORG_NAME+"</strong>"
		            + "                </label>"
		            + "           </div>"
		            + "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Date (MM/DD/YYYY) : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.DATE+"</strong>"
		            + "                </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>";
			   if(element.COMPANY_NAME != "")
        	   {
                    html+=  "           <div class='form-group'>"
		            + "                <label class='form-label'><strong>Company Name : - </strong>"
		            + "                </label>"
		            + "				 <label class='form-label'><strong>"+element.COMPANY_NAME+"</strong>"
		            + "                </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>  ";
        	   
	               if(element.STOCKIST_NAME != "")
	            	 {
		           html+= "           <div class='form-group'>"
		            + "               <label class='form-label'><strong>Stockist Name : - </strong>"
		            + "               </label>"
		            + "				   <label class='form-label'><strong>"+element.STOCKIST_NAME+"</strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "            </div>";
			       }
        	   }else{
        		   html+= "           <div class='form-group'>"
   		            + "               <label class='form-label'><strong>Other Name : - </strong>"
   		            + "               </label>"
   		            + "				   <label class='form-label'><strong>"+element.OTHER+"</strong>"
   		            + "               </label>"
   		            + "               <span class='tips'></span>"
   		            + "            </div>";
        	   }
			        html+= "            <div class='form-group'>"
		            + "               <label class='form-label'><strong>State : - </strong>"
		            + "               </label>"
		            + "				<label class='form-label'><strong>"+element.STATE+"</strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "           </div>  "                            
		            + "            <div class='form-group'>"
		            + "               <label class='form-label'><strong>District : - </strong>"
		            + "               </label>"
		            + "				<label class='form-label'><strong>"+element.DISTRICT+"</strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "           </div>  "                            
		            + "            <div class='form-group'>"
		            + "               <label class='form-label'><strong>City : - </strong>"
		            + "               </label>"
		            + "				<label class='form-label'><strong>"+element.CITY+"</strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "           </div>  "   
		            + "            <div class='form-group'>"
		            + "               <label class='form-label'><strong>Submitted Date : - </strong>"
		            + "               </label>"
		            + "				<label class='form-label'><strong>"+element.SUBMITTED_DATE+"</strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "           </div>  "   
		            + "       </div>"
		            + "       <div class='col-md-4'>"
		            + "          <div class='form-group'>"
		            + "               <label class='form-label'><strong>Courier Name : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.COURIER_NAME+"</strong>"
		            + "                </label>"
		            + "               <span class='tips'></span>"
		            + "            </div>"
		            + "           <div class='form-group'>"
		            + "               <label class='form-label'><strong>Delivery Person : - </strong>"
		            + "               </label>"
		            + "				  <label class='form-label'><strong>"+element.DELIVERY_PERSON+"</strong>"
		            + "               </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>"
		            + "           <div class='form-group'>"
		            + "                <label class='form-label'><strong>Time : - </strong>"
		            + "                </label>"
		            + "				 <label class='form-label'><strong>"+element.TIME+"</strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "           </div>"
		            +"             <div class='form-group'>"
		            + "                <label class='form-label'><strong>Docket Number : - </strong>"
		            + "                </label>"
		            + "				 <label class='form-label'><strong>"+element.DOCKET_NUMBER+"</strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "           </div>"
		            + "            <div class='form-group'>"
		            + "               <label class='form-label'><strong>Docket File : - </strong>"
		            + "               </label>"
		            +"                <img onClick='showDocketFileImagesListPopup()' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"
		            + "               </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>"
		            + "            <div class='form-group'>"
		            + "               <label class='form-label'><strong>Remark : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.REMARK+"</strong>"
		            + "               </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>"
		            + "            <div class='form-group'>"
		            + "               <label class='form-label'><strong>Particular : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.PARTICULAR+"</strong>"
		            + "               </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>"
		            + "       </div>"
		            + "   </div>"
		            + "</div>";
	});
		html+= "            <div class='modal-footer text-center'>"
            + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
            + "            </div>"
            + "        </div>"
            + "    </div>"
            + "</div>";
	$('#popup').html(html);
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block"); 
  }, 'json');	
}
//
function showDocketFileImagesListPopup(){
	showListOfImagesPopup(commonData.DOCKET_FILE);
}
function editInwardCourierRegDetails(mainIndex,inwardCourierRegId)
{
	$.post(contextApplicationPath+'/InwardCourierController/viewInwardCourierRegistration', {inwardCourierRegId : inwardCourierRegId , editStatus : true}, function(data) {
		$(data).each(function(index,element){
		commonData = element;
		var html = ""
			+"<form id='courierEdit' action='"+contextApplicationPath+"/InwardCourierController/editInwardCourierRegistration' method='post' enctype='multipart/form-data'>"
			+ "<div class='modal fade in' id='modal-responsive' >"
	        + "    <div class='col-md-13'>"
	        + "        <div class='modal-content'>"
	        + "            <div class='modal-header'>"
	        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	        + "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Inward Courier Goods</strong></h4>"
	        + "            </div>"
			+ "                                <div class='panel-body'>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-4'>"
			+"											   <input type='hidden' name='courierId' value='"+inwardCourierRegId+"'>"
			+"											   <input type='hidden' name='option' value='"+element.OPTION+"'>"
			+ "                                            <div id='selectOrganization' class='form-group'>"
			+ "                                                <label class='form-label'><strong>Organization Name: -</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                   <label class='form-label'><strong>"+element.ORG_NAME+"</strong><input type='hidden' id='OrgName' name='orgName' value='"+element.ORG_ID+"' required>"
			+ "                                                   </label>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Date</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input name='courierRegDate' class='dateClassCommon form-control' value='"+element.DATE+"' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' required>"
			+ "                                                </div>"
			+ "                                            </div>";
			if(element.OPTION=="CompanyDontHaveCheque"){
			html+="                                            <div id='companyDropDown' class='form-group'>"
				+ "                                                <label class='form-label'><strong>Select Company</strong>"
				+ "                                                </label>"
				+ "                                                <span class='tips'></span>"
				+ "                                                <div class='controls'>"
				+ "                                                    <select id='companyName' name='companyName'  onchange='onChangeCompany(false)' class='form-control' class='form-control' required>"
				+ "                                                        <option disabled selected> Select Company</option>"
				+ "                                                    </select>"
				+ "                                                </div>"
				+ "                                            </div>";
			}else if(element.OPTION=="CompanyHavingCheque"||element.OPTION== "CompanyStockistHavingCheque"){
			html+= "                                            <div id='companyDropDown' class='form-group'>"
				+ "                                                <label class='form-label'><strong>Company Name :- </strong>"
				+ "                                                </label>"
				+ "                                                <span class='tips'></span>"
				+ "                                                <div class='controls'>"
				+ "                                                 <label class='form-label'><strong>"+element.COMPANY_NAME+"</strong>"
				+ "                                                </label>"
				+ " 											    <input type='hidden' name='companyName' value='"+element.COMPANY_ID+"'>"	
				+ "                                                </div>"
				+ "                                            </div>";
			}
			if(element.OPTION== "CompanyStockistHavingCheque"){
				html+= "                                            <div class='form-group'>"
					+ "                                                <label class='form-label'><strong>Stockist Name :- </strong>"
					+ "                                                </label>"
					+ "                                                <span class='tips'></span>"
					+ "                                                <div class='controls'>"
					+ "                                                <label class='form-label'><strong>"+element.STOCKIST_NAME+"</strong>"
					+ " 											   <input type='hidden' name='stockistId' value='"+element.STOCKIST_ID+"' required>"	
					+ "                                                </label>"
					+ "                                                </div>"
					+ "                                            </div>";
			}else if(element.OPTION=="CompanyStockistDontHaveCheque"){
				html+="                                            <div id='companyDropDown' class='form-group'>"
					+ "                                                <label class='form-label'><strong>Select Company</strong>"
					+ "                                                </label>"
					+ "                                                <span class='tips'></span>"
					+ "                                                <div class='controls'>"
					+ "                                                    <select id='companyName' name='companyName' onchange='onChangeCompany(true)' class='form-control' required>"
					+ "                                                        <option disabled selected> Select Company</option>"
					+ "                                                    </select>"
					+ "                                                </div>"
					+ "                                            </div>"
				    + "                                            <div class='form-group'>"
					+ "                                                <label class='form-label'><strong>Select Stockist</strong>"
					+ "                                                </label>"
					+ "                                                <span class='tips'></span>"
					+ "                                                <div class='controls'>"
					+ "                                                    <select id='StockistName' name='stockistId' onchange='onStockistChange()' class='form-control' class='form-control' required>"
					+ "                                                        <option disabled selected> Select Stockist</option>"
					+ "                                                    </select>"
					+ "                                                </div>"
					+ "                                            </div>";
			}
			if(element.OPTION=="OTHER"){
				html+= "                                            <div class='form-group'>"
					+ "                                                <label class='form-label'><strong>Other :- </strong>"
					+ "                                                </label>"
					+ "                                                <span class='tips'></span>"
					+ "                                                <div class='controls'>"
					+ "                                                <input type='text' name='other' value='"+element.OTHER+"' placeholder='Other' class='form-control' required>"
					+ "                                                </div>"
					+ "                                            </div>"
					+ "                                            <div class='form-group'>"
					+ "                                               <label class='form-label'><strong>State</strong>"
					+ "                                               </label>"
					+ "                                               <span class='tips'></span>"
					+ "                                               <div class='controls'>"
					+ "                                               	<select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control' required>"
					+ "														<option disabled selected> Select State</option>"
					+ "													</select>"
					+ "                                                </div>"
					+ "                                             </div>"
					+ "                                            <div class='form-group' style='height: 58px;'>"
					+ "                                            	<label class='form-label'><strong>District</strong>"
					+ "                                              </label>"
					+ "                                              <span class='tips'></span>"
					+ "                                              <div class='controls'>"
					+ "                                              	<select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control' required>"
					+ "                                                  	<option disabled selected> Select District</option>"
					+ "                                                  </select>"
					+ "                                              </div>"
					+ "                                            </div>"
					+ "                                            <div class='form-group'>"
					+ "                                                <label class='form-label'><strong>City</strong>"
					+ "                                                </label>"
					+ "                                                <span class='tips'></span>"
					+ "                                                <div class='controls'>"
					+ "                                                    <select name='city' id='cityName' class='form-control' required>"
					+ "                                                        <option disabled selected> Select City</option>"
					+ "                                                    </select>"
					+ "                                                </div>"
					+ "                                            </div>";
			}
		html+="                                            <div id='selectStockist' class='form-group'></div>"
		    + "                                             <div class='form-group'>"
            +"                                             	  <button type='button' onclick='editDocketFileImagesListPopup("+inwardCourierRegId+")' class='btn btn-success'>Edit Docket File Document</button>"
		    +"                                             </div>"
		    + "                                        </div>"
			+ "                                        <div class='col-md-4'>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Courier Name</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='text' name='courierName' value='"+element.COURIER_NAME+"' placeholder='Courier Name' class='form-control' required >"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Delivery Person</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='text' name='deliveryPerson' value='"+element.DELIVERY_PERSON+"' placeholder='Delivery Person' class='form-control' required>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Time</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input name='time' id='timePicker' value='"+element.TIME+"' type='text' placeholder='Time' class='form-control' required>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Docket No</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='text' name='docketNo' value='"+element.DOCKET_NUMBER+"' placeholder='Docket No' class='form-control' required>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Remark</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='text' name='remark' value='"+element.REMARK+"' placeholder='Remark' class='form-control'>"
			+ "                                                </div>"
			+ "                                            </div>";/*
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Docket File</strong>"//DOCKET_FILE
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
		    + "                                                <div id='imgDiv'>"
		    + "                                                    <img onClick='showImagePopup(\""+element.DOCKET_FILE+"\")' src='"+element.DOCKET_FILE+"' alt='Smiley face' height='100' width='100'>"
		    + "                                                </div>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='file' name='docketFile' class='form-control'>"
			+ "                                                </div>"
			+ "                                            </div>"*/
			if(element.OPTION== "CompanyHavingCheque"||element.OPTION== "CompanyStockistHavingCheque"){
				html+="                                            <div class='form-group'>"
					+ "                                                <label class='form-label'><strong>Particulars Name :- </strong>"
					+ "                                                </label>"
					+ "                                                <span class='tips'></span>"
					+ "                                                <div class='controls'>"
					+ "                                                <label class='form-label'><strong>"+element.PARTICULAR+"</strong>"
					+ "												   <input type='hidden' name='particular' value='"+element.PARTICULAR_ID+"' required>"
					+ "                                                </label>"
					+ "                                                </div>"
					+ "                                            </div>";
				}else{
				html+="                                            <div class='form-group'>"
					+ "                                                <label class='form-label'><strong>Particulars</strong>"
					+ "                                                </label>"
					+ "                                                <span class='tips'></span>"
					+ "                                                <div class='controls'>"
					+ "                                                    <select name='particular' id='selectParticular' onchange='showDiv()' class='form-control' class='form-control' required>"
					+ "                                                        <option disabled selected>Select Particulars</option>"
					+ "                                                    </select>"
					+ "                                                </div>"
					+ "                                            </div>"
					//pop up
					+ "                                            <div id='showPopUp' class='form-group'></div>";
					//pop up
				}
        html+="                                        </div>"
			+ "                                    </div>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                <div class='pull-right'>"
			+ "                                                    <button type='submit' class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#courierEdit\")'>Save</button>"
			+ "                                                    <button type='reset' onClick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                </div>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>"
			+ "</form>";
		$('#popup').html(html);
		ajaxEditCourierRegistration(mainIndex,"courierEdit");
		$(".dateClassCommon").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
		$('#timePicker').timepicki();
		/*stateDropDownList();
		perticularsDropDownList();*/
		
		if(element.OPTION== "CompanyDontHaveCheque"||element.OPTION== "CompanyStockistDontHaveCheque"){
			getCompaniesDropdownForEdit(element.ORG_ID,element.COMPANY_ID);
			perticularsDropDownListForEdit(element.PARTICULAR_ID);
		}
		if(element.OPTION== "CompanyStockistDontHaveCheque"){
			getStockistForEdit(element.ORG_ID,element.COMPANY_ID,element.STOCKIST_ID);
		}
		if(element.OPTION== "OTHER")
			particularsDropDownListForOtherRadioForEdit(element.PARTICULAR_ID);
		stateDropDownListForEdit(element.STATE_ID);
		getDistrictForEdit(element.DISTRICT_ID,element.STATE_ID);
		getCitiesForEdit(element.DISTRICT_ID,element.CITY_ID);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
		
		});
	}, 'json');
}
//ajax call
function ajaxEditCourierRegistration(mainIndex,formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.result==true){
					$('#REG_DATE_'+mainIndex).html(data.REG_DATE);
					$('#COURIER_NAME_'+mainIndex).html(data.COURIER_NAME);
					$('#COMPANY_'+mainIndex).html(data.COMPANY);
					$('#STOCKIST_NAME_'+mainIndex).html(data.STOCKIST_NAME);
					$('#CITY_'+mainIndex).html(data.CITY);
					$('#DOCKET_NO_'+mainIndex).html(data.DOCKET_NO);
					alert(data.MSG);
					hidePopUp();
				}
				else{
					alert(data.MSG);
				}
			}
	};
	$('#'+formId).ajaxForm(options);
}
//
function editDocketFileImagesListPopup(registrationId){
	editListOfImagesPopup(registrationId,"docketFile",commonData.DOCKET_FILE,"INWARD_COURIER_DOCKET_FILE_IMAGE" , "/InwardCourierController/updateInwardCourierRegDocketFileImage");
}
//Image updation Ajax call after getting result from server
function ajaxInwardCourierDocketFileImageUpdate(data){
	if(data.RESULT==true) {
		commonData.DOCKET_FILE = data.DOCKET_FILE;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Docket File Image of Inward Courier Reg
function deleteInwardCourierDocketFileImage(docketFileImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardCourierController/deleteInwardCourierRegistrationDocketFileImage', {
			docketFileImageId : docketFileImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.DOCKET_FILE[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}