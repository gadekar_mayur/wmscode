function subTabsForStockistChequeDepositEntry(flag)
{
	 var html = ""
	  +"                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
	  +"                                            <li id='StockistChequeDepositEntryTabEnetry1' class=''><a href='#tab2_6' data-toggle='tab' onclick='stockistChequeDepositEntry()'><h5><strong>Deposit</strong></h5></a></li>"
	  +"                                            <li id='StockistChequeDepositEntryTabEnetry2' class=''><a href='#tab2_7' data-toggle='tab' onclick='stockistChequeReturnEntry()'><h5><strong>Return</strong></h5></a></li>"
	  +"                                        </ul>";
	 $('#subTabsForStockistChequeDepositEntry').html(html);
	 		if(flag)
	 			$('#StockistChequeDepositEntryTabEnetry1').addClass("active");
	 		else
				$('#StockistChequeDepositEntryTabEnetry2').addClass("active");
}


function subTabsForDeposit(flag)
{
	var html=	""
	+"											<ul id='myTab3' class='nav nav-tabs nav-dark'>"
	+"												<li id='DepositChequeEntryTab1' class=''><a href='#tab3_1' data-toggle='tab' onclick='stockistChequeDepositEntry()'><h5><strong>Add Cheque Deposit Entry</strong></h5></a></li>"	
	+"												<li id='DepositChequeEntryTab2' class=''><a href='#tab3_2' data-toggle='tab' onclick='StockistChequeDepositEntryList()'><h5><strong>View Deposited Cheque Entries</strong></h5></a></li>"
	+"                                        	</ul>";
	 $('#subTabsForDeposit').html(html);
	 		if(flag)
	 			$('#DepositChequeEntryTab1').addClass("active");
	 		else
				$('#DepositChequeEntryTab2').addClass("active");

}


function stockistChequeDepositEntry()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab3_1'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Stockist Cheque Deposit Entry</strong></h3>"
		+"                                    </div>"
		+"                                    <div class=''>"
		+"<div id='subTabsForStockistChequeDepositEntry'></div>"
		+"                                            <div class='tab-pane fade active in' id='tab2_6'>"
		+"                                                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                    <h3><strong>Deposit</strong></h3>"
		+"                                                </div>"
		+"                                    <div class=''>"
		+"<div id='subTabsForDeposit'></div>"		
		+"                                                <div id='chequeDepositEntryListing' class='row'>"
		+"                                                    <div class='col-md-12'>"
		+"                                                        <div class='panel panel-default'>"
		+"<form id='addStockistChequeDepositEntry' action='"+contextApplicationPath+"/BankController/saveStockistChequeDepositEntry' method='post'>"
		+"                                                            <div class='panel-body'>"
		+"                                                                <div class='row'>"
		+"                                                                    <div class='col-md-4'>"
		+"<div id='organiztionDropDown'></div>"
		+"                                            			  	  <div class='form-group'>"
		+"                                                			      <label class='col-lg-12 control-label' style='padding-left: 0px;'><strong></strong></label>"
		+"                                                				  <div class='col-lg-12'>"
		+"                                                    				  <div class='pos-rel'>"
		+"                                                        				  <input tabindex='13'  type='radio' value='Company' onchange='changeCompanyRadio(3)'  name='company_stokist_crit'   class='parsley-validated classname' required>"
		+"                                                        				  <label class='p-l-40' for='flat-checkbox-1'>Company</label>"
		+"                                                    				  </div>"
		+"                                                    				  <div class='pos-rel'>"
		+"                                                        				  <input tabindex='14'  type='radio' value='Stockist' name='company_stokist_crit' onchange='showStockistDropdownHtml(4)'   class='parsley-validated classname' required>"
		+"                                                          			  <label class='p-l-40' for='flat-checkbox-2'>Stockist</label>"
		+"                                                    				  </div>"
		+"                                                				  </div>"
		+"                                            				  </div>"
		+															"  <div id='selectStockist' class='form-group'></div>"
		+"                                                                        <div class='form-group'>"
		+"                                                                            <label class='form-label'><strong>Remark</strong>"
		+"                                                                            </label>"
		+"                                                                            <span class='tips'></span>"
		+"                                                                            <div class='controls'>"
		+"                                                                                <input class='form-control' type='text' placeholder='Remark' name='remark'/>"
		+"                                                                            </div>"
		+"                                                                        </div>"
		+"                                                                        <div class='form-group'>"
		+"                                                                            <label class='form-label'><strong>Date</strong>"
		+"                                                                            </label>"
		+"                                                                            <span class='tips'></span>"
		+"                                                                            <div class='controls'>"
		+"                                                                                <input class='datepicker form-control' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' id='date' name='depositDate' parsley-required='true'>"
		+"                                                                            </div>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='col-md-4'>  "
			
		+ "      <div id='companyDropDown'></div>                                   "

		+"                                                                        <div class='form-group'>"
		+"                                                                            <label class='form-label'><strong>Bank</strong>"
		+"                                                                            </label>"
		+"                                                                            <span class='tips'></span>"
		+"<div id='bankList'>"
		+"                                                                <select class='form-control' class='form-control'  name='bankID' required>"
		+"                                                                    <option selected disabled >Select Bank</option>"
		+"                                                                </select>"
		+																"</div>"
		+"                                                                        </div>"
		+"                                                                        <div class='form-group'>"
		+"                                                                            <label class='form-label'><strong>Cheque No</strong>"
		+"                                                                            </label>"
		+"                                                                            <span class='tips'></span>"
		+"<div id='ChequeNoDiv'>"
		+"<select class='form-control' class='form-control' required>"
		+"<option selected disabled >Select Cheque No</option>"
		+"</select>"
		+"</div>"
		+"                                                                        </div>"
		+"                                                                        <div class='form-group'>"
		+"                                                                            <label class='form-label'><strong>Cheque Amount</strong>"
		+"                                                                            </label>"
		+"                                                                            <span class='tips'></span>"
		+"                                                                            <div class='controls'>"
		+"                                                                                <input class='form-control' type='text' placeholder='Cheque Amount' name='ChequeAmount' parsley-type='number' required/>"
		+"                                                                            </div>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                        <div class='pull-right'>"
		+"                                                                            <button type='submit' class='btn btn-success m-b-10' onclick='validateFormDetails(\"#addStockistChequeDepositEntry\")'>Deposited</button>"
		/*+"                                                                            <button class='btn btn-success m-b-10' onclick='javascript:$('#form1').parsley('validate');'>Deposit & Mail</button>"*/
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"</form>"
		+"<div id='stockistChequeDepositeEntry'>"
		+"</div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	BankStockistAdvancedChequeOrganiztionDropDown();
	$("#date").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	subTabsForStockistChequeDepositEntry(true);
	ajaxAddStockistChequeDepositEntry('addStockistChequeDepositEntry');
	//StockistChequeDepositEntryList();
	subTabsForDeposit(true);
}


function ajaxAddStockistChequeDepositEntry(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					stockistChequeDepositEntry();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}


function CompanyBankChange()
{
	var orgId=$('#orgId').val();
	var companyId=$('#comapnyId').val();
	var companyBankId=$('#selectedbankId').val();
	
//	alert("orgId="+orgId+"companyId="+companyId+"companyBankId="+companyBankId);
	var html="<select class='form-control' class='form-control' name='ChequeNumberInfoId' required>"
		+"<option value='-1'>Select Cheque No</option>";
	
	$.post(contextApplicationPath+'/BankController/getCompanyChequeList',{orgId:orgId,companyId:companyId,companyBankId:companyBankId}, function(data) {
		$(data).each(function(index, element) {
			html+="<option value='"+element.CHEQUE_NUMBER_INFO_ID+"'>"+element.CHEQUE_NO+"</option>";
		});
		html+="</select>";
		$('#ChequeNoDiv').html(html);
	}, 'json');
	
}


function stockistBankChange()
{
	var orgId=$('#orgId').val();
	var companyId=$('#comapnyId').val();
	var stockistId=$('#StockistName').val();
	var stockistBankId=$('#selectedbankId').val();
//	alert("orgId="+orgId+"companyId="+companyId+"stockistId="+stockistId+"stockistBankId="+stockistBankId);
	
	var html="<select class='form-control' class='form-control' name='ChequeNumberInfoId'>"
		+"<option value='-1'>Select Cheque No</option>";
	
	$.post(contextApplicationPath+'/BankController/getStockistChequeList',{orgId:orgId,companyId:companyId,stockistId:stockistId,stockistBankId:stockistBankId}, function(data) {
		$(data).each(function(index, element) {
			html+="<option value='"+element.CHEQUE_NUMBER_INFO_ID+"'>"+element.CHEQUE_NO+"</option>";
		});
		html+="</select>";
		$('#ChequeNoDiv').html(html);
	}, 'json');
}


function StockistChequeDepositEntryList()
{
	$('#loader').show();
	var i=1;
	
	//$.post(contextApplicationPath+'/BankController/listStockistChequeDepositEntry', function(data) {
	var html=""
		+"<div class='panel panel-default'>"
        +"                                                <div class='panel-body'>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'  onchange='changeFunctionOutwardInvoiceEntry();'>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
		+"                                                                        <option value='Stockist'>Stockist</option>"
		+"                                                                        <option value='Company'>Company</option>"
		+"                                                                        <option value='CompanyBank'>Company Bank</option>"
		+"                                                                        <option value='StockistBank'>Stockist Bank</option>"
		+"                                                                        <option value='ChequeNo'>Cheque Number</option>"
		+"                                                                        <option value='ChequeAmount'>Cheque Amount</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchStockistDepositCheque()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+"                                                                    </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                                                                </div>"
		+"                                                                </div>"
		+"                                                            </div>";
		$('#chequeDepositEntryListing').html(html);
			//viewTable(data);
	//}, 'json');
	commonStockistDepositChequeController("", "", "", "", 1);
}

function viewTable(data, from, url)
{
	var i=0;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html="                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
	+"                                                            <table class='table table-striped table-hover'>"
	+"                                                                <thead class='no-bd'>"
	+"                                                                    <tr>"
	+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
	+"                                                                        </th>"
	+"                                                                        <th style='text-align: center;'><strong>Orgnization</strong>"
	+"                                                                        </th>"
	+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
	+"                                                                        </th>"
	+"                                                                        <th style='text-align: center;'><strong>Company Bank</strong>"
	+"                                                                        </th>"
	+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
	+"                                                                        </th>                                                    "
	+"                                                                        <th style='text-align: center;'><strong>Stockist Bank</strong>"
	+"                                                                        </th>  "
	+"                                                                        <th style='text-align: center;'><strong>Cheque Number</strong>"
	+"                                                                        </th>  "
	+"                                                                        <th style='text-align: center;'><strong>Amount</strong>"
	+"                                                                        </th>  "
	+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
	+"                                                                        </th>"
	+"                                                                    </tr>"
	+"                                                                </thead>"
	+"                                                                <tbody class='no-bd-y'>";

	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="<tr style='text-align: center;'>"
			+"<td>"+SR_NO+"</td>"
			+"<td>"+element.ORG+"</td>"
			+"<td>"+element.COMPANY+"</td>"
			+"<td>"+element.COMPANY_BANK+"</td>"
			+"<td>"+element.STOCKIST+"</td>"
			+"<td>"+element.STOCKIST_BANK+"</td>"
			+"<td>"+element.CHEQUE_NUMBER+"</td>"
			+"<td>"+element.CHEQUE_AMOUNT+"</td>"
			+"<td style='width: 180px;'>"
			+"<a class='btn btn-danger m-b-10' data-toggle='modal' data-target='#modal-responsive3' onclick=\"sendReturnCheque('"+element.STOCKIST_CHEQUE_DEPOSIT_ID+"','"+element.ORG_ID+"')\">Return</a>"
			+"</td>"
//			+"<a class='delete btn btn-danger' href='javascript:;'><i class='fa fa-times-circle'></i> </a></td>"
//			+"<a class='view btn btn-danger' href='#' onclick='deleteAdvanceCheque("+element.ADV_CHEQUE_INFORMATION_ID+")'><i class='fa fa-times-circle'></i> </a></td>"
			+"</tr>";
		i++;
		SR_NO++;
	});
	html+="                                                                            </tbody>"
		+"                                                                        </table>"
		+"                                                                    </div>";
	 $('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);
	 $('#loader').hide();

}


function searchStockistDepositCheque() {
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	var fromSpecialData;
	var toSpecialData;
	if(searchOption=="ChequeAmount" )
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else{
	if (!(Boolean(searchText))) {
		alert("Please Enter the search text");
		return false
	}
	searchText=$('#searchText').val();
	}
	commonStockistDepositChequeController(searchOption, searchText, fromSpecialData, toSpecialData, 1);
}

function commonStockistDepositChequeController(searchOption, searchText, fromSpecialData, toSpecialData, from) {
	$.post(contextApplicationPath+ '/BankController/searchStockistDepositedChequeEntries',{
		selectedValue:searchOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		searchOption:'ChequeAmount', 
		from : from
		},
		function(data) {
			var url = "commonStockistDepositChequeController(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			viewTable(data, from, url);
		}, 'json');
}

function sendReturnCheque(STOCKIST_CHEQUE_DEPOSIT_ID,orgnizationId)
{
	var html="                                                                                        <div class='modal fade' id='modal-responsive' aria-hidden='true'>"
	+"                                                                                            <div class='col-md-6'>"
	+"<form id='saveStockistReturnChequeDepositEntry' action='"+contextApplicationPath+"/BankController/saveStockistReturnChequeDepositEntry' method='post'>"
	+"<input type='hidden' name='stockistChequeDepositId' value='"+STOCKIST_CHEQUE_DEPOSIT_ID+"'>"
	+"<input type='hidden' name='orgnizationId' value='"+orgnizationId+"'>"
	+"                                                                                                <div class='modal-content'>"
	+"                                                                                                    <div class='modal-header'>"
	+"                                                                                                        <button type='button' onclick='hidePopUp()' class='close' data-dismiss='modal' area-hidden='true'>X</button>"
	+"                                                                                                        <h4 class='modal-title' id='myModalLabel'><strong>Return Stockist Cheque</strong></h4>"
	+"                                                                                                    </div>"
	+"                                                                                                    <div class='modal-body'>"
	+"                                                                                                        <div class='row'>"
	+"                                                                                                            <div class='col-md-12'>"
	+"                                                                                                                <div class='form-group'>"
	+"                                                                                                                    <label class='form-label'><strong>Reason</strong>"
	+"                                                                                                                    </label>"
	+"                                                                                                                    <span class='tips'></span>"
	+"                                                                                                                    <div class='controls'>"
	+"                                                                                                                        <input class='form-control' type='text' placeholder='Reason' name='reason' required/>"
	+"                                                                                                                    </div>"
	+"                                                                                                                </div>"
	+"                                                                                                            </div>"
	+"                                                                                                        </div>"
	+"                                                                                                    </div>  "
	+"                                                                                                    <div class='modal-footer text-center'>"
	+"                                                                                                        <button type='submit' class='btn btn-primary' onclick='validateFormDetails(\"#saveStockistReturnChequeDepositEntry\")'>Submit</button>"
	+"                                                                                                        <button type='button' class='btn btn-danger' onClick='hidePopUp()' data-dismiss='modal'>Cancel</button>"
	+"                                                                                                    </div>"
	+"                                                                                                </div>"
	+"</form>"
	+"                                                                                            </div>"
	+"                                                                                        </div>";
	$('#popup').html(html);
	ajaxReturnStockistChequeDepositEntry('saveStockistReturnChequeDepositEntry');
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block");
}
function ajaxReturnStockistChequeDepositEntry(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					$('#modal-responsive').remove();
					StockistChequeDepositEntryList();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}


function stockistChequeReturnEntry()
{
	$('#loader').show();
	var i=1;
	$('#loader').show();
	var html="                                <div class='tab-pane fade active in' id='tab3_1'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Stockist Cheque Deposit Entry</strong></h3>"
		+"                                    </div>"
		+"                                    <div class=''>"
		+"<div id='subTabsForStockistChequeDepositEntry'></div>"
		+"                                            <div class='tab-pane fade active in' id='tab2_7'>"
		+"                                                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                                    <h3><strong>Return</strong></h3>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-12'>"
		+"                                                        <div class='panel panel-default'>"
		+"                                                            <div class='row'>"
		+"                                                                    <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                        <div class='pull-right' style='margin: 10px 10px 0px 0px;'>"
		+"                                                                            <button type='button' class='btn btn-success m-b-10' onclick='reDeposit()'>Deposited</button>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                        <table class='table table-striped table-hover'>"
		+"                                                                            <thead class='no-bd'>"
		+"                                                                                <tr>"
		+"                                                                                  <th style='text-align: center;'><strong>Select</strong>"
		+"                                                                                  </th>"
		+"                                                                                  <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                  </th>"
		+"                                                                                  <th style='text-align: center;'><strong>Orgnization</strong>"
		+"                                                                                  </th>"
		+"                                                                                  <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                                  </th>"
		+"                                                                                  <th style='text-align: center;'><strong>Company Bank</strong>"
		+"                                                                                  </th>"
		+"                                                                                  <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                                   </th>                                                    "
		+"                                                                                  <th style='text-align: center;'><strong>Stockist Bank</strong>"
		+"                                                                                  </th>  "
		+"                                                                        			<th style='text-align: center;'><strong>Cheque Number</strong>"
		+"                                                                        			</th>  "
		+"                                                                       			 <th style='text-align: center;'><strong>Amount</strong>"
		+"                                                                       			 </th>  "
		+"                                                                       			 <th style='text-align: center;'><strong>Return Reason</strong>"
		+"                                                                       			 </th>  "
		+"                                                                                </tr>"
		+"                                                                            </thead>"
		+"                                                                            <tbody class='no-bd-y'>";
	$.post(contextApplicationPath+'/BankController/listStockistReturnChequeEntry', function(data) {
		$(data).each(function(index, element) {
			html+="<tr style='text-align: center;'>"
				+"<td>"
				+"<div class='div_checkbox'>"
				+"<input type='checkbox' id='ctest' style='opacity: 1 !important;' name='stockistChequeReturnId' value='"+element.STOCKIST_CHEQUE_RETURN_ID+"'>"
				+"<input type='hidden' id='org_"+element.STOCKIST_CHEQUE_RETURN_ID+"' value='"+element.ORG_ID+"'>"
				+"<input type='hidden' id='stockistDepositChequeId_"+element.STOCKIST_CHEQUE_RETURN_ID+"' value='"+element.STOCKIST_CHEQUE_DEPOSIT_ID+"'>"
				+"</div>"
				+"</td>"
				+"<td>"+ i++ +"</td>"
				+"<td>"+element.ORG+"</td>"
				+"<td>"+element.COMPANY+"</td>"
				+"<td>"+element.COMPANY_BANK+"</td>"
				+"<td>"+element.STOCKIST+"</td>"
				+"<td>"+element.STOCKIST_BANK+"</td>"
				+"<td>"+element.CHEQUE_NUMBER+"</td>"
				+"<td>"+element.CHEQUE_AMOUNT+"</td>"
				+"<td>"+element.REASON+"</td>"
				+"</tr>";
		});
		html+="                                                                            </tbody>"
			+"                                                                        </table>"
			+"                                                                    </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                                </div>"
			+"                                                </div>"
			+"                                            </div>";
			$('#myTabContent').html(html);
			subTabsForStockistChequeDepositEntry(false);
			$('#stockistChequeDepositEntry').html(html);
			$('#loader').hide();
	}, 'json');
	
}

function reDeposit()
{
	
	var i=1;
	var checkboxesValues = document.getElementsByName('stockistChequeReturnId');
	var stockistReturnChequeId = "";
	var orgValues="";
	var stockistDepositChequeId="";
	for (var i=0, n=checkboxesValues.length;i<n;i++) {
	  if (checkboxesValues[i].checked) 
	  {
	  orgValues +=","+$("#org_"+checkboxesValues[i].value+"").val();
	  stockistDepositChequeId +=","+$("#stockistDepositChequeId_"+checkboxesValues[i].value+"").val();
	  stockistReturnChequeId += ","+checkboxesValues[i].value;
	  }
	}
	if (stockistReturnChequeId) stockistReturnChequeId = stockistReturnChequeId.substring(1);
	if(orgValues) orgValues=orgValues.substring(1);
	if(stockistDepositChequeId) stockistDepositChequeId=stockistDepositChequeId.substring(1);
	if(stockistReturnChequeId=="")
	{
	alert("Please select at least one check box");
	}
	else
	{
		var temp = new Array();
		// this will return an array with strings "1", "2", etc.
		temp = orgValues.split(",");
			if(temp.allValuesSame())
				{
				$.ajax({
					type: "POST",
		            url : contextApplicationPath+'/BankController/reDepositCheque',
		            data : {
		            	stockistReturnChequeId : stockistReturnChequeId,
		            	orgValues : orgValues,
		            	stockistDepositChequeId : stockistDepositChequeId
		            },
		            success : function(json) {
		            	$(json).each(function(index, element) {
							alert(element.MSG);
							stockistChequeReturnEntry()
						});
		            }
		        });
				}
			else
				{
				alert("Please select same orgnization Cheque");
				}
		}
}