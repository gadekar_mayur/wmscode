//Add by Nutan
function viewStockistDetailsPopUp(stockistId)
{
	var i=1;
	var html="" 
		+ "<div class='modal fade in' id='modal-responsive' >"
		            + "    <div class='col-md-13'>"
		            + "        <div class='modal-content'>"
		            + "            <div class='modal-header'>"
		            + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		            + "                <h4 class='modal-title' id='myModalLabel'><strong>View Stockist Details</strong></h4>"
		            + "            </div>"
		            + "  <div class='panel panel-default'>"
		            + "<div class='panel-body'>"
		            + "    <div class='row'>";
	
	$.post(contextApplicationPath+'/StockistController/ViewStockistDetails', {stockistId : stockistId}, function(data) {
		$(data).each(function(index,element){
			 html+= ""
			     +"       													<div class='col-md-4'>"
				 +"                                      					  <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Organization Name : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.ORG_NAME+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Stockist Name : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.STOCKIST_NAME+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>City : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.CITY+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                             <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>State : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                               <label class='form-label'><strong>"+element.STATE+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Email ID : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.EMAIL_ID+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Contact Person : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                 <label class='form-label'><strong>"+element.CONTACT_PERSON+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				  +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Stockist Location : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                 <label class='form-label'><strong>"+element.LOCATION+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>VAT : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.VAT+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>PAN No : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.PAN_NO+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                        </div>"
				 +"                                                        <div class='col-md-4'>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Address : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                               <label class='form-label'><strong>"+element.ADDRESS+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>District : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.DISTRICT+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Mobile No : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.MOBILE+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Fax No : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                 <label class='form-label'><strong>"+element.FAX_NO+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Proprietor/Partner : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.PARTNER+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>Transporter : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.TRANSPORTER+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group'>"
				 +"                                                                <label class='form-label'><strong>CST : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                                <label class='form-label'><strong>"+element.CTS+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				 +"                                                            <div class='form-group>"
				 +"                                                                <label class='form-label'><strong>User Name : - </strong>"
				 +"                                                                </label>"
				 +"                                                                <span class='tips'></span>"
				 +"                                                              <label class='form-label'><strong>"+element.USER_NAME+"</strong>"
				 +"                                                                </label>"
				 +"                                                            </div>"
				+ "</div>"
				+ " </div>"
				+ "</div>";
			 
			 var drugData=element.DRUG_ARRAY;
			 if(drugData.length > 0)
				 {
			html+="                                                <div class='panel-body'>"
             +"                                                    <div class='row'>"
             +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
             +"                                                            <table class='table table-striped table-hover'>"
             +"                                                                <thead class='no-bd'>"
              +"                                                                    <tr>"
              +"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
              +"                                                                        </th>"
              +"                                                                        <th style='text-align: center;'><strong>Drug License Number</strong>"
              +"                                                                        </th>"
              +"                                                                        <th style='text-align: center;'><strong>From Date</strong>"
              +"                                                                        </th>"
              +"                                                                        <th style='text-align: center;'><strong>To Date</strong>"
              +"                                                                        </th>"
              +"                                                                        <th style='text-align: center;'><strong>Validity</strong>"
              +"                                                                        </th> "
              +"                                                                    </tr>"
              +"                                                                </thead>"
              +"                                                                <tbody class='no-bd-y'>";
			
             $(drugData).each(function(index,element){
            html+="                                                                    <tr style='text-align: center;'>"
            +"                                                                        <td>"+ i++ +"</td>"
            +"                                                                        <td>"+element.DRUG_LICENSE_NO+"</td>"
            +"                                                                        <td>"+element.FROM_DATE+"</td>"
            +"                                                                        <td>"+element.TO_DATE+"</td>"
            +"                                                                        <td>"+element.VALIDITY+"</td>"
            +"                                                                    </tr>"
             });
				
    html+="                                                                </tbody>"
    +"                                                            </table>"
    +"                                                        </div>"
    +"                                                    </div>"
    +"                                                </div>";
				 }
	
				 });
		html+= "            <div class='modal-footer text-center'>"
            + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
            + "            </div>"
            + "        </div>"
            + "    </div>"
            + "</div>";
	$('#popup').html(html);
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block"); 
		
		}, 'json');	
}