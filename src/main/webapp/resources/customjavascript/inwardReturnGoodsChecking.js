//
function subTabsForInwardReturnGoodsRegCheckingDone(flag){
 var html = ""
  +"                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
  +"                                            <li id='checkingDoneTab1' class=''><a href='#tab2_6' data-toggle='tab' onclick='AddCreditNote()'><h5><strong>Checking Done</strong></h5></a></li>"
  +"                                            <li id='checkingDoneTab2' class=''><a href='#tab2_7' data-toggle='tab' onclick='viewCheckingDone()'><h5><strong>Checking Done View</strong></h5></a></li>"
  +"                                        </ul>";
 $('#subTabsForCheckingDone').html(html);
 		if(flag)
 			$('#checkingDoneTab1').addClass("active");
 		else
			$('#checkingDoneTab2').addClass("active");
}
//USED FUNCTION FOR SHOWING INWARD CHECKING RETURN GOOD REGISTRATION (CHECKING PENDING) 
function checkingDone()
{
	$('#loader').show();
//	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationCheckingController/listInwardReturnGoodsRegistration', function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_2'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>Checking Pending</strong></h3>"
			+"                                    </div>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-6'>"
			+ "                        <div class='panel panel-default'>"
			+ "                            <div class='panel-body'>"
			+ "                                <div class='row'>"
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+ "                                            <label class='form-label'><strong>Search By</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls'>"
			+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFunc();'>"
			+ "                                                       <option value='stockist'>Stockist</option>"
			+"														  <option value='Organization'>Select Organization</option>"
			+ "                                                       <option value='Company'>Comapny Name</option>"
			+ "                                                       <option value='Date'>Search By Registration Date</option>"
			+ "                                                       <option value='Transporter'>Transporter </option>"
			+"														  <option value='LrNo' >LR No </option>"
			+ "                                                       <option value='ClaimNo'>Claim No </option>"
			+ "                                                       <option value='ClaimAmount'>Claim Amount </option>"
			+ "                                                       <option value='DriverName'>Driver Name</option>"
			+ "                                                       <option value='DeliveredEmployee'>Delivered Employee Name</option>"
			+ "                                                     </select>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"//fromDate toDate searchOption searchText
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group' id='searchForDiv'>"
			+ "                                            <label class='form-label'><strong>Search For</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div id='searchControl'>"
			+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search For'>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-success m-b-10' onclick='searchInwardCourierRegistrationDetailscheckingDone()'>Show</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+"                                            <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                            </div>"
			+"                                        </div>"
			+ "                                       <div id='pagination' class='pagination'></div>"
			+"                                    </div>"
			+"                                </div>";
		$('#myTabContent').html(html);
		commanShowCheckingDone("","","","",1);
//		showcheckingDoneNew(data);
//	}, 'json');
}

function commanShowCheckingDone(selectedOption, searchText, fromSpecialData, toSpecialData,from)
{
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/searchInwardReturnGoodsRegListing',
			{
		selectedValue:selectedOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		searchOption:'checkingPending',
		from : from
		}, function(data) {
			var url = "commanShowCheckingDone(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showcheckingDoneNew(data,from, url); // to call the function which shows the searched item
//		$('#searchResult').html(html);
	}, 'json');
}

// Searxhing Functionality of INWARD CHECKING RETURN GOOD REGISTRATION 
//hello
function searchInwardCourierRegistrationDetailscheckingDone(){
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	if(selectedOption=="ClaimAmount" ||selectedOption=="CreditNoteAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (selectedOption=="Date"||selectedOption=="CheckingDoneDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commanShowCheckingDone(selectedOption, searchText, fromSpecialData, toSpecialData, 1);
}

//USED COMMON FUCTION FOR SHOWING INWARD CHECKING RETURN GOOD REGISTRATION (CHECKING PENDING) 
function showcheckingDoneNew(data,from, url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=0;
	var  html=""
		+"													<table class='table table-striped table-hover'>"
		+"                                                    <thead class='no-bd'>"
		+"                                                        <tr>"
		+"                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Company</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Transporter</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>View LR Details</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Check</strong>"
		+"                                                            </th>"
		+"                                                        </tr>"
		+"                                                    </thead>"
		+"                                                    <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			 return false;
		html+="                                                        <tr style='text-align: center;'>"//ID_NO_  ORG_ COMPANY_ STOCKIST_ TRAN_
			+"                                                            <td>"+ SR_NO +"</td>"
			+"                                                            <td id='ORG_"+i+"'>"+element.ORG+"</td>"
			+"                                                            <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
			+"                                                            <td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
			+"                                                            <td id='TRAN_"+i+"'>"+element.TRAN+"</td>"
			+"                                                            <td>"
			//<a class='edit btn btn-dark m-b-10' href='#' onclick='editInwardReturnGoodsReg("+i+","+element.ID_NO+")'><i class='fa fa-pencil-square-o'></i></a>
			+"                                                                        <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive' onclick='viewInwardReturnGoodsRegDetailsForCheckingPending("+element.Inward_Return_Goods_Registration_id+")'><i class='fa fa-external-link'></i></a>"
			+"                                                                        <a class='edit btn btn-dark m-b-10' data-toggle='modal' data-target='#modal-responsive' onclick='editInwardReturnGoodsReg("+i+","+element.Inward_Return_Goods_Registration_id+")'><i class='fa fa-pencil-square-o'></i></a>"
			+"<div id='LRDetailList'></div>"
			+"                                                            </td>"
			+"                                                            <td>"
			+"                                                                        <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive3' onclick=\"checkingDoneForm('"+element.Inward_Return_Goods_Registration_id+"','"+element.ORG_ID+"','"+element.ORG+"','"+element.STOCKIST_ID+"','"+element.STOCKIST+"')\">Check</a>"
			+"<div id='checkingDoneHtml'></div>"
			+"                                                                </div>"
			+"                                                            </td>"
			+"                                                        </tr>";
		i++;
		SR_NO++;
	}); 
	html+="                                                    </tbody>"
		+"                                                </table>";
		$('#searchResult').html(html);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
		$('#loader').hide();
		paginationView(from,data[data.length-1].paginationCount,url);
}


function viewInwardReturnGoodsRegistrationLRDetails(Inward_Return_Goods_Registration_id)
{
	var html="                                                                <div class='modal fade' id='modal-responsive6' aria-hidden='true'>"
	+"                                                                    <div class='col-md-13'>"
	+"                                                                        <div class='modal-content'>"
	+"                                                                            <div class='modal-header'>"
	+"                                                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>"
	+"                                                                                <h4 class='modal-title' id='myModalLabel'><strong>Checking Pending</strong></h4>"
	+"                                                                            </div>"
	+"                                                                            <div class='modal-body'>"
	+"                                                                                <div class='row'>"
	+"                                                                                    <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
	+"                                                                                        <table class='table table-striped table-hover'>"
	+"                                                                                            <thead class='no-bd'>"
	+"                                                                                                <tr>"
	+"                                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
	+"                                                                                                    </th>"
	+"                                                                                                    <th style='text-align: center;'><strong>Stockist Name</strong>"
	+"                                                                                                    </th>"
	+"                                                                                                    <th style='text-align: center;'><strong>LR No</strong>"
	+"                                                                                                    </th>"
	+"                                                                                                    <th style='text-align: center;'><strong>No of Cases</strong>"
	+"                                                                                                    </th>"
	+"                                                                                                    <th style='text-align: center;'><strong>Claim No</strong>"
	+"                                                                                                    </th>"
	+"                                                                                                    <th style='text-align: center;'><strong>Claim Amount</strong>"
	+"                                                                                                    </th>      "
	+"                                                                                                    <th style='text-align: center;'><strong>LR Copy</strong>"
	+"                                                                                                    </th>"
	+"                                                                                                    <th style='text-align: center;'><strong>Claim Copy</strong>"
	+"                                                                                                    </th>                                           "
	+"                                                                                                    <th style='text-align: center;'><strong>Action</strong>"
	+"                                                                                                    </th>"
	+"                                                                                                </tr>"
	+"                                                                                            </thead>"
	+"                                                                                            <tbody class='no-bd-y'>"
	+"                                                                                                <tr style='text-align: center;'>"
	+"                                                                                                    <td>1</td>"
	+"                                                                                                    <td>Shekhar</td>"
	+"                                                                                                    <td>LN23989</td>"
	+"                                                                                                    <td>81</td>"
	+"                                                                                                    <td>CN27972</td>"
	+"                                                                                                    <td>12,987 Rs</td>"
	+"                                                                                                    <td>lrfile.jpeg</td>"
	+"                                                                                                    <td>claimfile.jpeg</td>"
	+"                                                                                                    <td style='width: 214px;'>"
	+"                                                                                                        <div class='row' style='float: left;'>"
	+"                                                                                                            <div class='col-md-3'>"
	+"                                                                                                                <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive7'><i class='fa fa-external-link'></i></button>"
	+"                                                                                                            </div>"
	+"                                                                                                        </div>"
	+"                                                                                                        <div class='modal fade' id='modal-responsive7' aria-hidden='true'>"
	+"                                                                                                            <div class='col-md-6'>"
	+"                                                                                                                <div class='modal-content'>"
	+"                                                                                                                    <div class='modal-header'>"
	+"                                                                                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>"
	+"                                                                                                                        <h4 class='modal-title' id='myModalLabel'><strong>Product List</strong></h4>"
	+"                                                                                                                    </div>"
	+"                                                                                                                    <div class='modal-body'>"
	+"                                                                                                                        <div class='row'>"
	+"                                                                                                                            <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
	+"                                                                                                                                <table class='table table-striped table-hover'>"
	+"                                                                                                                                    <thead class='no-bd'>"
	+"                                                                                                                                        <tr>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>Product Name</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>Claim Quantity</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>Received Quantity</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>Batch</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>MFG Date</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>Expiry Date</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>MFG Company</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>Quantity Variance</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                            <th style='text-align: center;'><strong>Action</strong>"
	+"                                                                                                                                            </th>"
	+"                                                                                                                                        </tr>"
	+"                                                                                                                                    </thead>"
	+"                                                                                                                                    <tbody class='no-bd-y'>"
	+"                                                                                                                                        <tr style='text-align: center;'>"
	+"                                                                                                                                            <td>1</td>"
	+"                                                                                                                                            <td>Ommy</td>"
	+"                                                                                                                                            <td>10,000</td>"
	+"                                                                                                                                            <td>8,000</td>"
	+"                                                                                                                                            <td>BN3983</td>"
	+"                                                                                                                                            <td>10/03/2014</td>"
	+"                                                                                                                                            <td>10/03/2016</td>"
	+"                                                                                                                                            <td>Sipla</td>"
	+"                                                                                                                                            <td>Na</td>"
	+"                                                                                                                                            <td><a class='edit btn btn-dark' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
	+"                                                                                                                                        </tr>"
	+"                                                                                                                                    </tbody>"
	+"                                                                                                                                </table>"
	+"                                                                                                                            </div>"
	+"                                                                                                                        </div>"
	+"                                                                                                                    </div>"
	+"                                                                                                                </div>"
	+"                                                                                                            </div>"
	+"                                                                                                        </div>"
	+"                                                                                                        <a class='edit btn btn-dark' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
	+"                                                                                                    </td>"
	+"                                                                                                </tr>"
	+"                                                                                            </tbody>"
	+"                                                                                        </table>"
	+"                                                                                    </div>"
	+"                                                                                </div>"
	+"                                                                            </div>"
	+"                                                                        </div>"
	+"                                                                    </div>"
	+"                                                                </div>";
	$('#LRDetailList').html('');
	$('#LRDetailList').html(html);
}

function checkingDoneForm(Inward_Return_Goods_Registration_id,orgId,orgName,stockistId,stockistName)
{
	var html="<div class='modal fade' id='modal-responsive3' aria-hidden='true'>"
	+"<form id='addCheckingDone' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationCheckingController/saveCheckingDone' method='post' enctype='multipart/form-data'>"
	+"<input type='hidden' name='Inward_Return_Goods_Registration_id' value='"+Inward_Return_Goods_Registration_id+"'>"
	+"<input type='hidden' name='selectedOrganiztionNameId' value='"+orgId+"'>"
	+"<input type='hidden' name='stockistId' value='"+stockistId+"'>"
	+"                                                                    <div class='col-md-13'>"
	+"                                                                        <div class='modal-content'>"
	+"                                                                            <div class='modal-header'>"
	+"                                                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>X</button>"
	+"                                                                                <h4 class='modal-title' id='myModalLabel'><strong>Checking Done</strong></h4>"
	+"                                                                            </div>"
	+"                                                                            <div class='modal-body'>"
	+"                                                                                <div class='row'>"
	+"                                                                                    <div class='col-md-4'>"
	+"                                                                                        <div class='form-group'>";
	html+="                                                                                   	  <label for='field-3' class='control-label'>Orgnization</label></br>"
	+"                                                                                        <input type='text' class='form-control' disabled id='field-2' value='"+orgName+"' parsley-required>"                                                                                
	+"                                                                                    </div>"
	+"                                                                                    </div>"
	+"                                                                                    <div class='col-md-4'>"
	+"                                                                                        <div class='form-group'>"
	+"                                                                                            <label for='field-3' class='control-label'>Stockist</label></br>"
	+"                                                                                            <input type='text' class='form-control' disabled id='field-2' value='"+stockistName+"' parsley-required>"
	+"                                                                                        </div>"
	+"                                                                                    </div>"
	+"                                                                                    <div class='col-md-4'style='height: 73px;'>"
	+"                                                                                        <div class='form-group'>"
	+"                                                                                            <label for='field-3' class='control-label'>Select Employee</label>"
	+"                                                                                            <div class='controls'>"
	+"																								  <div id='employeeDropDown'></div>"
//	+"                                                                                                <select class='form-control' class='form-control'>"
//	+"                                                                                                    <option>Swapnil</option>"
//	+"                                                                                                    <option>Pratul</option>"
//	+"                                                                                                </select>"
	+"                                                                                            </div>"
	+"                                                                                        </div>"
	+"                                                                                    </div>"
	+"                                                                                    <div class='col-md-4'>"
	+"                                                                                        <div class='form-group'>"
	+"                                                                                            <label for='field-3' class='control-label'>Date</label></br>"
	+"                                                                                            <input class='datepicker form-control'  type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' id='date' name='date' onchange=\"errorMsgEmpty('date','dateError_msg')\">"
	+"																			 <div id='dateError_msg' class='validationError' style='color:red'></div>"
	+"                                                                                        </div>"
	+"                                                                                    </div>"
	+"                                                                                    <div class='col-md-4'>"
	+"                                                                                        <div class='form-group'>"
	+"                                                                                            <label for='field-5' class='control-label'>Time</label></br>"
	+"                                                                                            <input type='texareat' class='form-control'  placeholder='Time' id='timePicker' name='time'  focusout=\"errorMsgEmpty('timePicker','timeError_msg')\">"
	+"                                               												 <div id='timeError_msg' class='validationError' style='color:red'></div>"
	+"                                                                                        </div>"
	+"                                                                                    </div>"
	+"                                                                                    <div class='col-md-4'>"
	+"                                                                                        <div class='form-group'>"
	+"                                                                                            <label for='field-6' class='control-label'>Remark</label></br>"
	+"                                                                                            <input type='text' class='form-control' id='field-6' placeholder='Remark' name='remark' required>"
	+"                                                                                        </div>"
	+"                                                                                    </div>"
	+"                                                                                    <div class='col-md-12'>"
	+"                                                                                        <div class='form-group'>"
	+"                                                                                            <label for='field-2' class='control-label'>Attach Checking Slip</label>"
	+"                                                                                            <input type='file' class='form-control'  name='CheckingSlip' parsley-required>"
	+"                                                                                        </div>"
	+"                                                            							  <div id='checkingSlipImageDivId'></div>"
	+"                                                            							  <div class='form-group'>"
	+"                                                				 							 <div id='checkingSlipImageAddMoreButtonId'><button onclick=\"addFileConrol('checkingSlipImageDivId','checkingSlipImageAddMoreButtonId','CheckingSlip',1)\" class='btn btn-primary'>Add More Files</button></div>"
	+"                                                            							  </div>"
	+"                                                                                    </div>"
	+"                                                                                </div>"
	+"                                                                            </div>  "
	+"                                                                            <div class='modal-footer text-center'>"
	+"                                                                                <input type='Submit' value='Submit' class='btn btn-primary' onClick='validateFormDetails(\"#addCheckingDone\")'>"
	+"                                                                                <button type='button' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	+"                                                                            </div>"
	+"                                                                        </div>"
	+"                                                                    </div>"
	+"</form>"
	+"                                                                </div>";
	$('#checkingDoneHtml').html('');
	$('#checkingDoneHtml').html(html);
	$("#date").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	$('#timePicker').timepicki();
	loadEmployee(orgId);
//	 loadInward_Return_Goods_RegistrationOrgnizationDropDown();
	ajaxAddCheckingDone('addCheckingDone');
	
}

function ajaxAddCheckingDone(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					checkingDone();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

//function loadInward_Return_Goods_RegistrationOrgnizationDropDown()
//{
//	var userType='';
//	var html="<div class='form-group' style='height: 58px;'>"
//	+"<label class='form-label'><strong>Select Organization</strong>"
//	+"</label>"
//	+"<span class='tips'></span>"
//	+"<div class='controls'>"
//	+"<select class='form-control' class='form-control' id='OrgName' name='selectedOrganiztionNameId' onchange='inwardRetrunGoodsOrgChange()'>" 
//	+"<option value='-1'>Select Organiztion</option>";
//	var content="<div class='form-group' style='height: 58px;'>"
//		+"<label class='form-label'><strong>Your Organization</strong>"
//		+"</label>"
//		+"<span class='tips'></span>"
//		+"<div class='controls'>";
//	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
//		$(data).each(function(index, element) {
//			userType=element.USER_TYPE;
//			if(element.USER_TYPE=='SuperAdmin')
//				{
//				html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
//				}
//			else
//				{
//				content+="<input type='hidden' value='"+element.ORG_ID+"' name='selectedOrganiztionNameId' id='OrgName'>" +
//						""+element.ORG_NAME+"";
//				}
//		}); 
//		html+=" </select>"
//			+"</div>"
//			+"</div>";
//		content+="</div>"
//		+"</div>";
//		if(userType=='SuperAdmin')
//			{
//			$('#Inward_Return_Goods_Registration_DropDown').html(html);
//			}
//		else
//			{
//			$('#Inward_Return_Goods_Registration_DropDown').html(content);
//			getStockistByOrg();
//			loadEmployee($("#OrgName").val())
//			}
//		
//	}, 'json');
//}

//function inwardRetrunGoodsOrgChange()
//{
//	getStockistByOrg();
//	loadEmployee($("#OrgName").val())
//}

//hello1
function AddCreditNote()
{
	$('#loader').show();
//	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationCheckingController/listInwardReturnGoodsRegistrationWithCheckingDoneStausOne', function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_3'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>Checking Done</strong></h3>"
			+"                                    </div>"
			+"             <div id='subTabsForCheckingDone'></div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-12'>"
			+ "                        <div class='panel panel-default'>"
			+ "                            <div class='panel-body'>"
			+ "                                <div class='row'>"
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+ "                                            <label class='form-label'><strong>Search By</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls'>"
			+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFunc();'>"
			+ "                                                       <option value='stockist'>Stockist</option>"
			+"														  <option value='Organization'>Select Organization</option>"
			+ "                                                       <option value='Company'>Comapny Name</option>"
			+ "                                                       <option value='Date'>Search By Registration Date</option>"
			+ "                                                       <option value='CheckingDoneDate'>Search By Checking Done Date</option>"
			+ "                                                       <option value='Transporter'>Transporter </option>"
			+"														  <option value='LrNo' >LR No </option>"
			+ "                                                       <option value='ClaimNo'>Claim No </option>"
			+ "                                                       <option value='ClaimAmount'>Claim Amount </option>"
			+ "                                                       <option value='DriverName'>Driver Name</option>"
			+ "                                                       <option value='DeliveredEmployee'>Delivered Employee Name</option>"
			+ "                                                     </select>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"//fromDate toDate searchOption searchText
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group' id='searchForDiv'>"
			+ "                                            <label class='form-label'><strong>Search For</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div id='searchControl'>"
			+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search For'>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-success m-b-10' onclick='searchInwardCourierRegistrationDetailsAddCreditNote()'>Show</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+"                                                        </div>"
			+"                                                        <div class='pull-right'>"
			+"                                                                                            <div class='row'>"
			+"                                                                                                <div class='col-md-3'>"
			+"                                                                                                    <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive5' onclick='addCreditNoteForm()'>Add Credit Note</button>"
			+"																										<div id='creditNoteForm'></div>"
			+"                                                                                                </div>"
			+"                                                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                                <div class='row'>"
			+"                                                    <div class='col-md-6'>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+ "                                       		      <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
		$('#myTabContent').html(html);
//		showAddCreditNote(data);//calling commaon function 
		commanCheckingDone("","","","",1);
//	}, 'json');
}

function commanCheckingDone(selectedOption, searchText, fromSpecialData, toSpecialData,from)
{
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/searchInwardReturnGoodsRegListing',
			{
		selectedValue:selectedOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		searchOption:'CheckingDoneSearch',
		from : from
		}, function(data) {
			var url = "commanCheckingDone(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showAddCreditNote(data,from, url);//calling commaon function  // to call the function which shows the searched item
	}, 'json');
}

function searchInwardCourierRegistrationDetailsAddCreditNote(){
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	if(selectedOption=="ClaimAmount" ||selectedOption=="CreditNoteAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (selectedOption=="Date"||selectedOption=="CheckingDoneDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	
	commanCheckingDone(selectedOption, searchText, fromSpecialData, toSpecialData, 1);
}

function showAddCreditNote(data,from, url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=0;
	var html=""
        +"                           <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                            <th style='text-align: center;'><strong>Select</strong>"
		+"                                                            </th>"   
		+"                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>ID No</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Company</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Transporter</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                            </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			 return false;
		html+="                                                                    <tr style='text-align: center;'>"
			+"                                                                        <td>"
			+"                                                                            <div class='div_checkbox'>"
			+"                                                                                <input type='checkbox' id='ctest' style='opacity: 1 !important;' name='Inward_Return_Goods_Registration_id' value='"+element.Inward_Return_Goods_Registration_id+"'>"
			+"                                                                            </div>"
			+"                                                                        </td>"
			+"                                                                        <td>"+ SR_NO +"</td>"//ID_NO_ ORG_  COMPANY_ STOCKIST_ TRAN_
			+"                                                                        <td id='ID_NO_"+i+"'>"+element.ID_NO+"</td>"
			+"                                                                        <td id='ORG_"+i+"'>"+element.ORG+"</td>"
			+"                                                                        <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
			+"                                                                        <td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
			+"                                                                        <td id='TRAN_"+i+"'>"+element.TRAN+"</td>"
			+"                                                                        <td><a class='edit btn btn-blue' onClick='viewInwardReturnGoodsRegDetailsCheckingDoneView("+element.ID_NO+")'  href='javascript:;'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' onClick='editInwardReturnGoodsRegDetailsAndCheckingDoneDetails("+i+","+element.ID_NO+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegWithoutCreditNote("+element.ID_NO+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
			+"                                                                    </tr>";
		i++;
		SR_NO++;
	});
	html+="                                                                </tbody>"
	+"                                                            </table>";
$('#searchResult').html(html);
$('#loader').hide();
$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
subTabsForInwardReturnGoodsRegCheckingDone(true);

paginationView(from,data[data.length-1].paginationCount,url);
}

function deleteInwardReturnGoodsRegWithoutCreditNote(ID_NO){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/deleteInwardReturnGoodsReg', {
			ID_NO : ID_NO
		}, function(data) {
			AddCreditNote();
			alert(data.MSG);
		}, 'json');
    }
}
function addCreditNoteForm()
{
	
	var checkboxes = document.getElementsByName('Inward_Return_Goods_Registration_id');
	var vals = "";
	for (var i=0, n=checkboxes.length;i<n;i++) {
	  if (checkboxes[i].checked) 
	  {
	  vals += ","+checkboxes[i].value;
	  }
	}
	if (vals) vals = vals.substring(1);
	if(vals=="")
		{
		alert("Please select at least one check box");
		}
	else
		{
		var html="                                                                                            <div class='modal fade' id='modal-responsive5' aria-hidden='true'>"
			+"<form id='addCreditNote' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationCheckingController/saveCreditNote' method='post' enctype='multipart/form-data'>"
			+"<input type='hidden' name='Selected_Inward_Return_Goods_Registration_id' value='"+vals+"'>"
			+"                                                                                                <div class='col-md-13'>"
			+"                                                                                                    <div class='modal-content'>"
			+"                                                                                                        <div class='modal-header'>"
			+"                                                                                                            <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>"
			+"                                                                                                            <h4 class='modal-title' id='myModalLabel'><strong>Add Credit Note</strong></h4>"
			+"                                                                                                        </div>"
			+"                                                                                                        <div class='modal-body'>"
			+"                                                                                                            <div class='row'>"
			+"                                                                                                                <div class='col-md-4'>"
			+"                                                                                                                    <div class='form-group'>"
			+"                                                                                                                        <label for='field-1' class='control-label'>Credit Note No</label></br>"
			+"                                                                                                                        <input type='text' class='form-control' id='field-1' placeholder='Credit Note No' name='creditNoteNo' parsley-type='onlynumber'>"
			+"                                                                                                                    </div>"
			+"                                                                                                                </div>"
			+"                                                                                                                <div class='col-md-4'>"
			+"                                                                                                                    <div class='form-group'>"
			+"                                                                                                                        <label for='field-2' class='control-label'>Credit Note Amount</label></br>"
			+"                                                                                                                        <input type='text' class='form-control' id='field-2' placeholder='Credit Note Amount' name='creditNoteAmount' parsley-type='onlynumber'>"
			+"                                                                                                                    </div>"
			+"                                                                                                                </div>"
			+"                                                                                                                <div class='col-md-4'>"
			+"                                                                                                                    <div class='form-group'>"
			+"                                                                                                                        <label for='field-3' class='control-label'>Remark</label></br>"
			+"                                                                                                                        <input type='text' class='form-control' id='field-3' placeholder='Remark' name='remark' required>"
			+"                                                                                                                    </div>"
			+"                                                                                                                </div>"
			+"                                                                                                                <div class='col-md-4'>"
			+"                                                                                                                    <div class='form-group'>"
			+"                                                                                                                        <label for='field-3' class='control-label'>Attach Credit Note</label>"
			+"                                                                                                                        <input type='file' class='form-control' id='field-3' name='creditNoteImage'>"
			+"                                                                                                                    </div>"
			+"                                                            														  <div id='creditNoteImageDivId'></div>"
			+"                                                            														  <div class='form-group'>"
			+"                                                				 														 <div id='creditNoteImageAddMoreButtonId'><button onclick=\"addFileConrol('creditNoteImageDivId','creditNoteImageAddMoreButtonId','creditNoteImage',1)\" class='btn btn-primary'>Add More Files</button></div>"
			+"                                                            														  </div>"
			+"                                                                                                                </div>"
			+"                                                                                                            </div>"
			+"                                                                                                        </div>"
			+"                                                                                                        <div class='modal-footer text-center'>"
			+"                                                                                                            <input type='submit' class='btn btn-primary' value='Submit' onclick='validateFormDetails(\"#addCreditNote\")'>"
			+"                                                                                                            <button type='button' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			+"                                                                                                        </div>"
			+"                                                                                                    </div>"
			+"                                                                                                </div>"
			+"</form>"
			+"                                                                                            </div>";
			$('#creditNoteForm').html(html);
			ajaxAddCreditNote('addCreditNote');
		}
}
function ajaxAddCreditNote(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					AddCreditNote();
					$('#loader').hide();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}


//hello2
function viewCheckingDone()
{
	$('#loader').show();
    var i=1;
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_3'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Checking Done</strong></h3>"
		+"                                    </div>"
		  +"             <div id='subTabsForCheckingDone'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>From Date</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='datepicker form-control' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>To Date</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='datepicker form-control' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='javascript:$('#form1').parsley('validate');'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>ID No</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Company</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Transporter</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                            </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
	//$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationCheckingController/loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne', function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_3'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>Checking Done</strong></h3>"
			+"                                    </div>"
			  +"             <div id='subTabsForCheckingDone'></div>"
				+ "                <div class='row'>"
				+ "                    <div class='col-md-12'>"
				+ "                        <div class='panel panel-default'>"
				+ "                            <div class='panel-body'>"
				+ "                                <div class='row'>"
				+ "                                    <div class='col-md-4'>"
				+ "                                        <div class='form-group'>"
				+ "                                            <label class='form-label'><strong>Search By</strong>"
				+ "                                            </label>"
				+ "                                            <span class='tips'></span>"
				+ "                                            <div class='controls'>"
				+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFunction();'>"
				+ "                                                       <option value='stockist'>Stockist</option>"
				+"														  <option value='Organization'>Select Organization</option>"
				+ "                                                       <option value='Company'>Comapny Name</option>"
				+ "                                                       <option value='Date'>Search By Registration Date</option>"
				+ "                                                       <option value='CheckingDoneDate'>Search By Checking Done Date</option>"
				+ "                                                       <option value='Transporter'>Transporter </option>"
				+"														  <option value='LrNo' >LR No </option>"
				+ "                                                       <option value='ClaimNo'>Claim No </option>"
				+ "                                                       <option value='ClaimAmount'>Claim Amount </option>"
				+ "                                                       <option value='DriverName'>Driver Name</option>"
				+ "                                                       <option value='DeliveredEmployee'>Delivered Employee Name</option>"
				+ "														  <option value='CreditNoteNo'>Credit Note No</option>"
				+ "														  <option value='CreditNoteAmount'>Credit Note Amount</option>"
				+ "                                                     </select>"
				+ "                                            </div>"
				+ "                                        </div>"
				+ "                                    </div>"//fromDate toDate searchOption searchText
				+ "                                    <div class='col-md-4'>"
				+ "                                        <div class='form-group' id='searchForDiv'>"
				+ "                                            <label class='form-label'><strong>Search For</strong>"
				+ "                                            </label>"
				+ "                                            <span class='tips'></span>"
				+ "                                            <div id='searchControl'>"
				+ "                                                <input id='searchText' type='text' class='form-control'>"
				+ "                                            </div>"
				+ "                                        </div>"
				+ "                                    </div>"
				+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
				+ "                                        <div class='pull-right'>"
				+ "                                            <button class='btn btn-success m-b-10' onclick='searchInwardCourierRegistrationDetailsviewCheckingDone()'>Show</button>"
				+ "                                        </div>"
				+ "                                    </div>"
			+"                                                <div class='row'>"
			+"                                                    <div class='col-md-6'>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+ "                                       <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			
			+"                                                </div>"
			+"                                            </div>"
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
		$('#myTabContent').html(html);
		commonInwardCourierRegistrationDetailsviewCheckingDoneController("","","","",1)
		//showviewCheckingDoneVivew(data);
	//}, 'json');
}
function changeFunction()
{
	var selectedValue=$('#searchOption').val();
	var html="";
	
		if(selectedValue=="ClaimAmount"||selectedValue=="CreditNoteAmount")
		{
    		html+="<input id='inputFromData3' type='text' class='form-control' placeholder='From Amount' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTOData3' type='text' class='form-control' placeholder='To Amount' style='width: 49%;'>";
		}
    	else if(selectedValue=="Date" || selectedValue=="CheckingDoneDate")
    	{
    		html+="<input id='inputFromDate' type='text' class='dateClassCommon form-control' placeholder='From Date' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTODate' type='text' class='dateClassCommon form-control' placeholder='To Date' style='width: 49%;'>";
    	}
    	else
    	{
    		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control'>";
    	} 	
		$("#searchControl").empty();
		$("#searchControl").html(html);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
}

function searchInwardCourierRegistrationDetailsviewCheckingDone(){
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;

	if(selectedOption=="ClaimAmount" ||selectedOption=="CreditNoteAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (selectedOption=="Date"||selectedOption=="CheckingDoneDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonInwardCourierRegistrationDetailsviewCheckingDoneController(selectedOption,searchText,fromSpecialData,toSpecialData,1);
}

function commonInwardCourierRegistrationDetailsviewCheckingDoneController(selectedOption,searchText,fromSpecialData,toSpecialData,from){
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/searchInwardReturnGoodsRegListing',
			{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData,searchOption:'CreditNoteSearch', from : from
			
			}, function(data) {
				var url = "commonInwardCourierRegistrationDetailsviewCheckingDoneController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
				showviewCheckingDoneVivew(data,from,url);//calling commaon function  // to call the function which shows the searched item
	}, 'json');
}

function showviewCheckingDoneVivew(data, from, url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=0;
		var html=""
			+"                                                            <table class='table table-striped table-hover'>"
			+"                                                                <thead class='no-bd'>"
			+"                                                                    <tr>"
			+"                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>ID No</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Organization</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Company</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Transporter</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Action</strong>"
			+"                                                            </th>"
			+"                                                                    </tr>"
			+"                                                                </thead>"
			+"                                                                <tbody class='no-bd-y'>";
		$(data).each(function(index, element) {
			if((data.length-2)<i)
				 return false;
			html+="                                                                    <tr style='text-align: center;'>"
				+"                                                                        <td>"+ SR_NO +"</td>"
				+"                                                                        <td id='ID_NO_"+i+"'>"+element.ID_NO+"</td>"
				+"                                                                        <td id='ORG_"+i+"'>"+element.ORG+"</td>"
				+"                                                                        <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
				+"                                                                        <td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
				+"                                                                        <td id='TRAN_"+i+"'>"+element.TRAN+"</td>"
				+"                                                                        <td><a class='edit btn btn-blue' onClick='viewInwardReturnGoodsRegDetailsCheckingDoneView("+element.ID_NO+")' href='javascript:;'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' onClick='editInwardReturnGoodsRegDetailsAndCheckingDoneDetails("+i+","+element.ID_NO+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegCheckingDone("+element.ID_NO+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
				+"                                                                    </tr>";
			i++;SR_NO++;
		}); 
		
		html+="                                                                </tbody>"
		+"                                                            </table>";
	$('#searchResult').html(html);
	$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	subTabsForInwardReturnGoodsRegCheckingDone(false);
	$('#loader').hide();
	paginationView(from,data[data.length-1].paginationCount,url);
}

function deleteInwardReturnGoodsRegCheckingDone(ID_NO){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/deleteInwardReturnGoodsReg', {
			ID_NO : ID_NO
		}, function(data) {
			viewCheckingDone();
			alert(data.MSG);
		}, 'json');
    }
}