function SeeApprovalRequest()
{
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>Admin Request Approval</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class='tabcordion'>"
		+"                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#1_1' data-toggle='tab'  onclick=\"approvalDetailList()\">Approval Pending Request   " 
		//+"<span class=badge badge-notify id='approvalPendingId'> </span></a></li>"
		+"                                <li class=''><a href='#' data-toggle='tab' onclick=\"approvalStatus()\">Approved Request</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	
	approvalDetailList();
}
function approvalDetailList()
{
	
	//$('#approvalPendingId').html(100);
	 var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_3'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		//	+"                                        <h3><strong>Merge Transporter</strong></h3>"
			+"                                    </div>"
			+"<div id='subTabsForMergeTransporter'></div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+"                                                                <button class='btn btn-danger m-b-10' onclick='rejectStockistTransfer()'>Reject</button>"
			+"                                                            </div>"
			+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='approveStockistTransfer()'>Approve</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='pull-right'>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                                <div class='row'>"
			+"                                                    <div class='col-md-6'>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                            <table class='table table-striped table-hover'>"
			+"                                                                <thead class='no-bd' id='a'>"
			+"                                                                    <tr>"
			+"                                                            <th style='text-align: center;'><strong>Select Stockist</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Stockist Name</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>From Transporter</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>TO Transporter</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Station Name</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>From Transporter Rate</strong>"
			
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>To Transporter Rate</strong>"
			+"                                                            </th>"
            +"                                                            <th style='text-align: center;'><strong>From Transporter LR Rate</strong>"
			
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>To Transporter LR Rate</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>Rate Difference</strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong>LR Rate Difference</strong>"
			+"                                                            </th>"
			+"                                                            </tr>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                                    <select class='form-control' id='RateId' name='RateName' class='form-control' onChange='loadStockistList()' required>"
			+"                                                                        <option disabled selected> Select Transporter Rates</option>"
			+"                                                                    </select>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                                    <select  id='ToRateId' name='ToRateName' class='form-control' required>"
			+"                                                                        <option disabled selected> Select Transporter Rates</option>"
			+"                                                                    </select>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                                    <select class='form-control' id='LRRateId' name='LRRateName' class='form-control' onChange='loadStockistList()' required>"
			+"                                                                        <option disabled selected> Select Transporter Rates</option>"
			+"                                                                    </select>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                                    <select  id='LRToRateId' name='LRRateName' class='form-control' required>"
			+"                                                                        <option disabled selected> Select Transporter Rates</option>"
			+"                                                                    </select>"
			+"                                                            </th>"
			+"                                                            <th style='text-align: center;'><strong></strong>"
			+"                                                            </th>"
			
			+"                                                            </tr>"
			
			+"                                                            </thead>"
			
			+"                                                                <tbody class='no-bd-y' id='transporterStockistSearch'>"
			+"                                                                </tbody>"
			+"                                                            </table>"
			+"                                                        </div>"
			+"                                                     <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
	 $('#myTabContent').html(html);
	 loadOptionList();
	loadStockistList(); 
}
function loadOptionList()
{
	
	var html=""
		+"<option value='"+1+"'>Actual Rate</option>"
		+"<option value='"+2+"'>Claim  Rate</option>"
		//+"<option value='"+3+"'>Actual LR Rate</option>"
		//+"<option value='"+4+"'>Claim LR Rate</option>"
		$('#RateId').html(html);
	//$('#ToRateId').html(html);
	}
function loadStockistList()
{
	var rateId=$("#RateId").val();
	var name=$("#RateId option:selected").text();
	if(rateId==1)
		{
		var html=""
			+"<option value='"+1+"'>Actual LR Rate</option>"
			$('#LRRateId').html(html);
		$('#LRToRateId').html(html);
		}
	else
		{
		var html=""
			+"<option value='"+1+"'>Claim LR Rate</option>"
			$('#LRRateId').html(html);
		$('#LRToRateId').html(html);
		}
	 var html=""
		 +"<option value='"+1+"'>"+name+"</option>"
		 $('#ToRateId').html(html);
		 
	$.post(contextApplicationPath+'/TransporterDetailsController/loadPendingStockist',{
		rateId:rateId	
	
	},function(data)
	{
		var url = "commonTransporterStockistDetailsController(\""+rateId+"\",";
		
		transporterStokistPendingListView(data,url);
	
			
		
			},'json');
	
	}
function transporterStokistPendingListView(data,url)
{
	var i=0;
	var html1;
	 if(data=="")
		 {
		 alert("Sorry No Data Available");
		 }
	$(data).each(function(index,element){
		//console.log(element.STOCKIST_NAME);
		html1+="<tr style='text-align: center;'>"
			+"<td>"
			+"<div class='div_checkbox'>"
			+"<input type='checkbox' id='checkbox_"+element.StockistId+"' onclick='AddCasesCheckboxEvent("+element.StockistId+")' style='opacity: 1 !important;' name='TransporterStockistTransferId' value='"+element.StockistId+"'>"
			+"<input type='hidden' id='stockist_"+element.StockistId+"' value='"+element.StockistId+"'>"
			+"<input type='hidden' id='totransporter_"+element.StockistId+"' value='"+element.ToTransporterId+"'>"
			+"<input type='hidden' id='transporter_Stockist_Id_"+element.StockistId+"' value='"+element.TransporterStockistTransferId+"'>"
			
			+"</div>"
			+"</td>"
			+"<td id='StockistName"+i+"'>"+element.StockistName+"</td>"
			+"<td id='FromTransporterName"+i+"'>"+element.FromTransporterName+"</td>"
			+"<td id='ToTransferName"+i+"'>"+element.ToTransferName+"</td>"
			+"<td id='StationName"+i+"'>"+element.StationName+"</td>"
			+"<td id='FromRate"+i+"'>"+element.FromRate+"</td>"
			+"<td id='ToRate"+i+"'>"+element.ToRate+"</td>"
			+"<td id='FromLRRate"+i+"'>"+element.FromLRRate+"</td>"
			+"<td id='ToLRRate"+i+"'>"+element.ToLRRate+"</td>"
			+"<td id='Difference"+i+"'>"+element.Difference+"</td>"
			+"<td id='LRDifference"+i+"'>"+element.LRDifference+"</td>"
			+"</tr>";
		i++;
	});
	 $('#transporterStockistSearch').empty();
	$('#transporterStockistSearch').html(html1);
	$('#loader').hide();

	
	}
function approvalStatus()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_3'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Admin Approval Status List</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForMergeTransporter'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		/*+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
	//	+"                                                            <div class='form-group'>"
	//	+"                                                            <div class='form-group'>"
		+"                                                            	  <div id='organiztionTransporterDropDown'>"
	//	+"                                                            	  </div>"
	    +"                                                            </div>"
	    +"                                                            </div>"
	    +"                                                            </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"																<div id='companyDropDown'></div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Transporter</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select  id='StockistName' name='stockistId' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Transporter</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"</div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchStockistDetails()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-danger m-b-10' onclick='searchStockistDetails()'>Transfer Too</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"*/
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd' id='a'>"
		+"                                                                    <tr>"
		+"                                                            <th style='text-align: center;'><strong>Sr No</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                            </th>"
		//+"                                                            <th style='text-align: center;'><strong>Stockist City</strong>"
		//+"                                                            </th>"
		//+"                                                            <th style='text-align: center;'><strong>Order No</strong>"
		//+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>From Transporter</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Too Transporter</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Approval Status</strong>"
		+"                                                            </th>"
		/*+"                                                            <th style='text-align: center;'><strong>Claiming LR</strong>"
		+"                                                            </th>"*/
		//+"                                                            <th style='text-align: center;'><strong>Company</strong>"
		//+"                                                            </th>"
		
		+"                                                            </tr>"
		+"                                                            </thead>"
		+"                                                                <tbody class='no-bd-y' id='transporterStockistAdminApprovalStatus'>"
		+"                                                                </tbody>"
		+"                                                            </table>"
		+"                                                        </div>"
		+"                                                     <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html)
	
	loadAdminApprovalStatus(1);
	}
function loadAdminApprovalStatus(rateId)
{
	$.post(contextApplicationPath+'/TransporterDetailsController/loadAdminApprovalStatus',{
		rateId:rateId	
	
	},function(data)
	{
		var url = "commonTransporterStockistDetailsController(\""+rateId+"\",";
		
		transporterStokistAdminApprovalListView(data,url);
	
			
		
			},'json');
}

function transporterStokistAdminApprovalListView(data,url)
{
	var i=0;
	var html1;
	 if(data=="")
		 {
		 alert("Sorry No Data Available");
		 }
	$(data).each(function(index,element){
		console.log(element.STOCKIST_NAME);
		
			if(element.status==0)
				{
				
				html1+="<tr style='text-align: center;'>"
					+"<td>"+(i+1)+"</td>"
					+"<td id='StockistName"+i+"'>"+element.StockistName+"</td>"
					+"<td id='FromTransporterName"+i+"'>"+element.FromTransporterName+"</td>"
					+"<td id='ToTransferName"+i+"'>"+element.ToTransferName+"</td>"
			+"<td id='Status"+i+"'> PENDING </td>"
			+"</tr>";
				}
			else if(element.status==1)
				{
				
				html1+="<tr style='text-align: center;'>"
					+"<td>"+(i+1)+"</td>"
					+"<td id='StockistName"+i+"'>"+element.StockistName+"</td>"
					+"<td id='FromTransporterName"+i+"'>"+element.FromTransporterName+"</td>"
					+"<td id='ToTransferName"+i+"'>"+element.ToTransferName+"</td>"
					+"<td id='Status"+i+"'> APPROVED </td>"
			+"</tr>";
				
				}
			else 
				{
				
				html1+="<tr style='text-align: center;'>"
					+"<td>"+(i+1)+"</td>"
					+"<td id='StockistName"+i+"'>"+element.StockistName+"</td>"
					+"<td id='FromTransporterName"+i+"'>"+element.FromTransporterName+"</td>"
					+"<td id='ToTransferName"+i+"'>"+element.ToTransferName+"</td>"
					+"<td id='Status"+i+"'> REJECTED </td>"
			+"</tr>";
				}
			
		i++;
	});
	 $('#transporterStockistAdminApprovalStatus').empty();
	$('#transporterStockistAdminApprovalStatus').html(html1);
	$('#loader').hide();
}
function approveStockistTransfer()
{
	var totransporterArray = new Array();
	var stockistArray=new Array();
	var transporterStockistTransferArray=new Array();
	var i=1;
	
	
	//alert(transporterName)
	var checkboxesValues = document.getElementsByName('TransporterStockistTransferId');
	var vals = "";
	var totransporterValues="";
	var stockistValue="";
	var transporterStockistTransferIdValues="";
	
	for (var i=0, n=checkboxesValues.length;i<n;i++) {
	  if (checkboxesValues[i].checked) 
	  {
		  totransporterValues +=","+$("#totransporter_"+checkboxesValues[i].value+"").val();
	  stockistValue +=","+$("#stockist_"+checkboxesValues[i].value+"").val();
	  transporterStockistTransferIdValues +=","+$("#transporter_Stockist_Id_"+checkboxesValues[i].value+"").val();
	  vals += ","+checkboxesValues[i].value;
	  }
	}
	
	
	if (vals) vals = vals.substring(1);
	if(totransporterValues) totransporterValues=totransporterValues.substring(1);
	if(stockistValue) stockistValue=stockistValue.substring(1);
	if(transporterStockistTransferIdValues) transporterStockistTransferIdValues=transporterStockistTransferIdValues.substring(1);
	
	if(vals=="")
	{
	alert("Please select at least one check box");
	}
else
	{
	
	$.post(contextApplicationPath+'/TransporterDetailsController/saveApprovedData',{
		"totransporterArray":totransporterValues,
		"stockistArray":stockistValue,
		"transporterStockistTransferArray":transporterStockistTransferIdValues
	},function(data)
	{
		var url = "commonTransporterStockistDetailsController(\""+totransporterValues+"\",\""+stockistValue+"\",\""+transporterStockistTransferIdValues+"\"";
		
		transporterStokistAdminApprovalList(data,url);
			},'json');
	
	
	//saveSelectedStockistTransfer(totransporterArray,stockistArray,transporterStockistTransferArray);
	
	
	}
	
	
}
function transporterStokistAdminApprovalList(data,url)
{
	$(data).each(function(index,element){
		alert(element.msg);
	});	
	approvalDetailList();
}
function rejectStockistTransfer()
{
	var totransporterArray = new Array();
	var stockistArray=new Array();
	var transporterStockistTransferArray=new Array();
	var i=1;
	
	
	//alert(transporterName)
	var checkboxesValues = document.getElementsByName('TransporterStockistTransferId');
	var vals = "";
	var totransporterValues="";
	var stockistValue="";
	var transporterStockistTransferIdValues="";
	
	for (var i=0, n=checkboxesValues.length;i<n;i++) {
	  if (checkboxesValues[i].checked) 
	  {
		  totransporterValues +=","+$("#totransporter_"+checkboxesValues[i].value+"").val();
	  stockistValue +=","+$("#stockist_"+checkboxesValues[i].value+"").val();
	  transporterStockistTransferIdValues +=","+$("#transporter_Stockist_Id_"+checkboxesValues[i].value+"").val();
	  vals += ","+checkboxesValues[i].value;
	  }
	}
	
	
	if (vals) vals = vals.substring(1);
	if(totransporterValues) totransporterValues=totransporterValues.substring(1);
	if(stockistValue) stockistValue=stockistValue.substring(1);
	if(transporterStockistTransferIdValues) transporterStockistTransferIdValues=transporterStockistTransferIdValues.substring(1);
	
	if(vals=="")
	{
	alert("Please select at least one check box");
	}
else
	{
	
	$.post(contextApplicationPath+'/TransporterDetailsController/saveRejectedData',{
		"transporterStockistTransferArray":transporterStockistTransferIdValues
	},function(data)
	{
		var url = "commonTransporterStockistDetailsController(\""+transporterStockistTransferIdValues+"\"";
		
		transporterStokistAdminRejectedList(data,url);
			},'json');
	
	
	//saveSelectedStockistTransfer(totransporterArray,stockistArray,transporterStockistTransferArray);
	
	
	}
	
	
	}
function transporterStokistAdminRejectedList(data,url)
{
	$(data).each(function(index,element){
		alert(element.msg);
	});	
	approvalDetailList();
	}