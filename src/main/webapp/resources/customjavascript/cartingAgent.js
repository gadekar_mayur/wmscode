//by Darshan
function AddCartingAgent()
{
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>Carting Agent Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class='tabcordion'>"
		+"                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#' data-toggle='tab' onclick='AddCartingAgentRegistration()'>Carting Agent Details</a></li>"
		+"                                <li class=''><a href='#tab1_3' data-toggle='tab' onclick='cartingAgentUploadDocument()'>Upload Document</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	AddCartingAgentRegistration();
}
function subTabsForAddCartingAgentRegistration(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='AddCartingAgentRegistrationTabEntry1' class=''><a href='#tab1_1' data-toggle='tab' onclick='AddCartingAgentRegistration()'><h5><strong>Add CartingAgent Registration</strong></h5></a></li>"
			+ "                                            <li id='ViewCartingAgentRegistrationTabEntry2' class=''><a href='#tab1_1_1' data-toggle='tab' onclick='cartingAgentListing()'><h5><strong>View CartingAgent Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCartingAgentRegistration').html(html);
	if (index == 1) {
		$('#AddCartingAgentRegistrationTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#ViewCartingAgentRegistrationTabEntry2').addClass("active");
	}
}

function AddCartingAgentRegistration()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add CartingAgent Registration</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCartingAgentRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"<form id='addCartingAgent' action='"+contextApplicationPath+"/CartingAgentController/saveCartingAgentDetails' method='post' enctype='multipart/form-data'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"															  <div id='selectOrganization'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Agent Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='Agent Name' name='agentName' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Mobile No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='Mobile No.' name='mobile' parsley-type='onlynumber' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Agent Photo</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' id='field2342' name='agentPhoto'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='cartingAgentImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='cartingAgentImageAddMoreButtonId'><button onclick=\"addFileConrol('cartingAgentImageDivId','cartingAgentImageAddMoreButtonId','agentPhoto',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Address*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='textarea' class='form-control' placeholder='Address' id='field-2' name='address' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Vehicle No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='Vehicle No.' name='vehicleNo' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Vehicle Model*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='Vehicle Model' name='vehicleModel' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Vehicle Photo</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' id='field65' name='vehiclePhoto'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='cartingAgentVehicleImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='cartingAgentVehicleImageAddMoreButtonId'><button onclick=\"addFileConrol('cartingAgentVehicleImageDivId','cartingAgentVehicleImageAddMoreButtonId','vehiclePhoto',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>User Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input name='username' type='text' placeholder='User Name' class='form-control' required>"
		+"                                                                </div>"
		+"																  <div id='error' class='validationError' ></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Password*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input name='password' type='password' placeholder='Password' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addCartingAgent\")()'>Submit</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='cartingAgentList'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddCartingAgentRegistration(1);
	displayOrganizationDropDown(0);
	ajaxAddCartingAgent('addCartingAgent');
//	cartingAgentListing();
}

function ajaxAddCartingAgent(FormId)
{
	
	var options = {
			success : function(data) {
				if(data.result==true){
					$('#loader').hide();
					$('#'+FormId)[0].reset();
//					cartingAgentListing();
				}
				else{
					if(data.FLAG==true)
						$('#error').html(data.MSG);
					$('#loader').hide();
				}
				alert(data.MSG);
			}
		};
$('#'+FormId).ajaxForm(options);
}

function showLoader()
{
	$('#loader').show();
	return true;
}

var CURRENT_PAGE_INDEX=0;
function cartingAgentListing()
{
	$('#loader').show();
	//$.post(contextApplicationPath+'/CartingAgentController/cartingAgentListing',function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab1_1_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                    <h3><strong>View CartingAgent Registration</strong></h3>"
			+ "                                    </div>"
			+ "										<div id='subTabsForAddCartingAgentRegistration'></div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
			+"                                                                        <option value='0'>Organization Name</option>"
			+"                                                                        <option value='1'>Agent Name</option>"
			+"                                                                        <option value='2'>VEHICLE_NO</option>"
			+"																		  <option value='3'>VEHICLE_MODEL</option>"
			+"                                                                        <option value='4'>MOBILE</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search For'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchCartingAgentDetails(1)'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div id='CartingAgentSearchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+"														<div id='ViewCartingAgent'></div>"
			+"                                                    </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>";	
		$('#myTabContent').html(html);
		subTabsForAddCartingAgentRegistration(2);
		$('#cartingAgentList').html(html);
		$('#loader').hide();
		commonCartingAgentDetailsController(-1 , "" , 1);
		//}, 'json');
}

function searchCartingAgentDetails(from) {
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if (!(Boolean(searchText))) {
		alert("Please Enter the search text");
		return false
	}
	commonCartingAgentDetailsController(searchOption,searchText,from);
}

function commonCartingAgentDetailsController(searchOption,searchText,from){
	$.post(contextApplicationPath+ '/CartingAgentController/searchCartingAgentsDetails',{searchOption : searchOption,searchText : searchText, from : from
	},function(data) {
		var url = "commonCartingAgentDetailsController(\""+searchOption+"\",\""+searchText+"\",";
		cartingAgentListingView(data, from, url);//calling the same function to searching carting agent list
		}, 'json');
}

//common function to show the CartingAgentList && For Searching Functionality view
function cartingAgentListingView(data,from,url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=0; 
	var html = ""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		/*+"                                                                        <th style='text-align: center;'><strong>Transporter Name</strong>"
		+"                                                                        </th>"*/
		+"                                                                        <th style='text-align: center;'><strong>Agent Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>VEHICLE_NO</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>VEHICLE_MODEL</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>MOBILE</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
		
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="<tr style='text-align: center;'>"
			+"<td>"+SR_NO+"</td>"
			+"<td>"+element.ORG_NAME+"</td>"
			//+"<td>"+element.TRAN_NAME+"</td>"
			+"<td>"+element.AGENT_NAME+"</td>"
			+"<td>"+element.VEHICLE_NO+"</td>"
			+"<td>"+element.VEHICLE_MODEL+"</td>"
			+"<td>"+element.MOBILE+"</td>"
			+"<td><a class='edit btn btn-blue' onClick='viewCartingAgentDetails("+element.AGENT_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark' href='#' onclick='editCartingAgentDetails("+element.AGENT_ID+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick='deleteCartingAgentDetails("+element.AGENT_ID+")'><i class='fa fa-times-circle'></i> </a></td>"
			+"</tr>";
		i++;
		SR_NO++;
	});
	html+="                                                                </tbody>"
		+"                                                            </table>";
	$('#CartingAgentSearchResult').html(html);
	paginationView(from,data[data.length-1].paginationCount,url);
}

function viewCartingAgentDetails(agentId){
	//alert("View :"+agentId);
	$.post(contextApplicationPath+'/CartingAgentController/viewCartingAgentDetails', {
		agentId : agentId 
			} , function(data) {
				commonData = data;
				if(data.ORG_NAME!=null){
					var html="" 
						+ "<div class='modal fade ' id='modal-responsive'>"
				        + "    <div class='col-md-13'>"
				        + "        <div class='modal-content'>"
				        + "            <div class='modal-header'>"
				        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
				        + "                <h4 class='modal-title' id='myModalLabel'><strong>Carting Agent Registration Details</strong></h4>"
				        + "            </div>"
				        + "  		   <div class='panel panel-default'>"
				        + "			       <div class='panel-body'>"
				        + "    			       <div class='row'>"
				        + "       				   <div class='col-md-4'>"
						+"                                        					<div class='form-group'>"
						+"                                                                <label class='form-label'><strong>AGENT NAME : - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                                <label class='form-label'><strong>"+data.AGENT_NAME+"</strong>"
						+"                                                                </label>"
						+"                                                            </div>"                                                            
						+"                                                            <div class='form-group'>"
						+"                                                                <label class='form-label'><strong>ORGANIZATION NAME: - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                                <label class='form-label'><strong>"+data.ORG_NAME+"</strong>"
						+"                                                                </label>"
						+"                                                            </div>"
						+"                                                            <div class='form-group'>"
						+"                                                                <label class='form-label'><strong>MOBILE: - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                               <label class='form-label'><strong>"+data.MOBILE+"</strong>" 
						+"                                                                </label>"
						+"                                                            </div>"
						+"                                                            <div class='form-group'>"
						+"                                                                <label class='form-label'><strong>AGENT PHOTO: - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                                <img src='../resources/images/fileIcon.png' style='width:100px;height:100px;' onClick='showCartingAgentImageFileListPopup()'>"
						+"                                                            </div>  "
						+"                                                            <div class='form-group'>"
						+"                                                                <label class='form-label'><strong>ADDRESS: - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                                <label class='form-label'><strong>"+data.ADDRESS+"</strong>"
						+"                                                                </label>"
						+"                                                            </div>"
						+"                                                        </div>"
						+"                                                        <div class='col-md-4'>"
						/*+"                                                             <div class='form-group'>"
						+"                                                                <label class='form-label'><strong>TRANSPORTER NAME : - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                                <label class='form-label'><strong>"+data.TRANSPORTER+"</strong>"
						+"                                                                </label>"
						+"                                                            </div>  "*/
						+"                                                            <div class='form-group'>"
						+"                                                                <label class='form-label'><strong>VEHICLE NO. : - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                                 <label class='form-label'><strong>"+data.VEHICLE_NO+"</strong>"
						+"                                                                </label>"
						+"                                                            </div>    "
						+"                                                            <div class='form-group'>"
						+"                                                                <label class='form-label'><strong>VEHICLE MODEL : - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                                 <label class='form-label'><strong>"+data.VEHICLE_MODEL+"</strong>"
						+"                                                                </label>"
						+"                                                            </div>                                             "            
						+"                                                            <div class='form-group'>"
						+"                                                                <label class='form-label'><strong>VEHICLE PHOTO : - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                                <img src='../resources/images/fileIcon.png' style='width:100px;height:100px;' onClick='showCartingAgentVehicleImageFileListPopup()'>"
						+"                                                            </div>"
						+"                                                            <div class='form-group'>"
						+"                                                                <label class='form-label'><strong>USER NAME : - </strong>"
						+"                                                                </label>"
						+"                                                                <span class='tips'></span>"
						+"                                                                 <label class='form-label'><strong>"+data.USER_NAME+"</strong>"
						+"                                                                </label>"
						+"                                                            </div>"
						+ "						 								 </div>"
						+ " 			    </div>"
						+"             <div class='modal-footer text-center'>"
				        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
				        + "            </div>"
						+ "			   </div>"
				        + "        </div>"
				        + "    </div>"
				        + "</div>";
					 $("#ViewCartingAgent").html(html);
					 $("#modal-responsive").addClass("in");
					 $("#modal-responsive").attr("aria-hidden","false");
					 $("#modal-responsive").css("display","block");
				}
				else{
					alert(data.MSG);
				}
			}, 'json'); 
			
}

function showCartingAgentImageFileListPopup(){
	showListOfImagesPopup(commonData.AGENT_PHOTO);
}

function showCartingAgentVehicleImageFileListPopup(){
	showListOfImagesPopup(commonData.VEHICLE_PHOTO);
}

function editCartingAgentDetails(agentId){
	//alert("Edit :"+agentId);
	editCartingAgentDetailsPopUp(agentId);
}
function deleteCartingAgentDetails(agentId){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CartingAgentController/deleteCartingAgentDetails', {
			agentId : agentId
		}, function(data) {
			cartingAgentListing();
			alert(data.MSG);
		}, 'json');
    }
}

//
function displyaCateringAgentDropDown(){
	var orgId = $('#OrgName').val();
	if(orgId==null||orgId==undefined)
		return false;
	//alert("org = "+orgId);
	$.post(contextApplicationPath+'/CartingAgentController/getCateringAgentDropDownByOrg', {
		orgId : orgId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Carting Agent</option>";
		$(data).each(function(index, element) {
			optionHtml += "<option value='" + element.AGENT_ID + "'>"+ element.AGENT_NAME + "</option>";
		});
		$("#selectCartingAgent").empty();
		$("#selectCartingAgent").append(optionHtml);
	}, 'json');
}

function subTabsForAddCartingAgentUploadDocument(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addCartingAgentUploadDocumentTabEntry1' class=''><a href='#tab1_3' data-toggle='tab' onclick='cartingAgentUploadDocument()'><h5><strong>CartingAgent Upload Document</strong></h5></a></li>"
			+ "                                            <li id='viewCartingAgentUploadDocumentTabEntry2' class=''><a href='#tab1_3_1' data-toggle='tab' onclick='cartingAgentUploadedDocListing()'><h5><strong>View CartingAgent Uploaded Document</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCartingAgentUploadDocument').html(html);
	if (index == 1) {
		$('#addCartingAgentUploadDocumentTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewCartingAgentUploadDocumentTabEntry2').addClass("active");
	}
}
function cartingAgentUploadDocument()
{
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_3'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>CartingAgent Upload Document</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCartingAgentUploadDocument'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"<form id='cateringAgentUploadDocument' action='"+contextApplicationPath+"/CartingAgentController/cartingAgentUploadDocument' method='post' enctype='multipart/form-data'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"															  <div id='selectOrganization' class='form-group' required></div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Document Type*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																	<div id='documentList'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Carting Agent*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='cartingAgent' id='selectCartingAgent' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Carting Agent</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                           <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Browse File*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' id='field2132' name='uploadFile'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='cartingAgentDocumentImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='cartingAgentDocumentImageAddMoreButtonId'><button onclick=\"addFileConrol('cartingAgentDocumentImageDivId','cartingAgentDocumentImageAddMoreButtonId','uploadFile',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>" 
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#cateringAgentUploadDocument\")'>Submit</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='cartingAgentUploadedDocList'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForAddCartingAgentUploadDocument(1);
	displayOrganizationDropDown(4);
	loadDocumentTypeList();
	ajaxAddCateringAgentUploadDocument('cateringAgentUploadDocument');
//	cartingAgentUploadedDocListing();
}
function ajaxAddCateringAgentUploadDocument(FormId)
{
	var options = {
			success : function(json) {
				$('#'+FormId)[0].reset();
				alert(json.MSG);
//				cartingAgentUploadedDocListing();
				$('#loader').hide();
			}
		};
$('#'+FormId).ajaxForm(options);
}

function cartingAgentUploadedDocListing()
{
	var i=1; 
	$('#loader').show();
//	$.post(contextApplicationPath+'/CartingAgentController/cartingAgentUploadedDocListing',function(data) {
	var html=""
			+"                                <div class='tab-pane fade active in' id='tab1_3_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View CartingAgent Upload Document</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddCartingAgentUploadDocument'></div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
			+"                                                                        <option value='Organization'>Organization Name</option>"
			+"                                                                        <option value='TransporterName'>Carting Agent Name</option>"
			+"                                                                        <option value='DocumentType'>Document Type</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search For'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchCartingAgentUploadedDocDetails()'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+ "                                                     <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>";
		$('#myTabContent').html(html);
		subTabsForAddCartingAgentUploadDocument(2);
		$('#cartingAgentUploadedDocList').html(html);
		commonCartingAgentUploadedDocController("", "", 1);
//		cartingAgentUploadedDocListingView(data);
		
		$('#loader').hide();
//	}, 'json');
}

function commonCartingAgentUploadedDocController(searchOption, searchText,  from)
{
	$.post(contextApplicationPath+'/CartingAgentController/searchCartingAgentUploadedDocumentDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonCartingAgentUploadedDocController(\""+searchOption+"\",\""+searchText+"\",";
		cartingAgentUploadedDocListingView(data, from, url);
	}, 'json');

}
function searchCartingAgentUploadedDocDetails(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commonCartingAgentUploadedDocController(searchOption, searchText, 1);
//	$.post(contextApplicationPath+'/CartingAgentController/searchCartingAgentUploadedDocumentDetails', {
//		searchOption : searchOption,
//		searchText : searchText
//	}, function(data) {
//		cartingAgentUploadedDocListingView(data);
//	}, 'json');
}

function cartingAgentUploadedDocListingView(data, from, url)
{
	urlForEdit=url;
	commonData = data;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i = 0; 
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Carting Agent Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Document Type</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Document</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="<tr style='text-align: center;'>"
			+"<td>"+ SR_NO +"</td>"
			+"<td>"+element.ORG_NAME+"</td>"
			+"<td>"+element.AGENT_NAME+"</td>"
			+"<td>"+element.DOC_TYPE+"</td>"
			+"<td>"
			+"<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showCatringAgentDocumentFileListPopup("+i+")'>"
			+"</td>"
			+"<td><a class='edit btn btn-dark' href='#' onclick='editCartingAgentDocumentDetails("+i+","+from+")'><i class='fa fa-pencil-square-o'></i></a> <a class='delete btn btn-danger' href='#' onclick='deleteCartingAgentDocumentDetails("+element.DOC_ID+")'><i class='fa fa-times-circle'></i> </a></td>"
			+"</tr>";
		i++;SR_NO++;
	});
	html+="                                                                </tbody>"
		+"                                                            </table>";
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
}

function showCatringAgentDocumentFileListPopup(index){
	showListOfImagesPopup(commonData[index].DOCUMENT);
}
function editCartingAgentDocumentDetails(index,from){
	editCartingAgentDocumentDetailsForm(index,from);
}
function deleteCartingAgentDocumentDetails(docId){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CartingAgentController/deleteCartingAgentDocumentDetails', {
			docId : docId
		}, function(data) {
			cartingAgentUploadedDocListing();
			alert(data.MSG);
		}, 'json');
    }
}