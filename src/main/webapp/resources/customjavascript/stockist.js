var rowCounter = 1;
//main tabs stockist reg
function AddStockist(){
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>Stockist Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class='tabcordion'>"
		+"                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#tab1_1' onclick='stockistRegForm()' data-toggle='tab'>Stockist Details</a></li>"
		+"                                <li class=''><a href='#tab1_3' onClick='displayAddStockistCompany()' data-toggle='tab'>Add Stockist Company</a></li>"
		+"                                <li class=''><a href='#tab1_2' onClick='addStockistBank()' data-toggle='tab'>Add Stockist Bank Details</a></li>"
		+"                                <li class=''><a href='#tab1_4' onClick='stockistUploadDocumentForm()' data-toggle='tab'>Upload Document</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').empty();
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	stockistRegForm();
}

function subTabsForAddStockistRegistration(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addStockistRegistrationTabEntry1' class=''><a href='#tab1_1' data-toggle='tab' onclick='stockistRegForm()'><h5><strong> Stockist Registration</strong></h5></a></li>"
			+ "                                            <li id='viewStockistRegistrationTabEntry2' class=''><a href='#tab1_1_1' data-toggle='tab' onclick='stockistListing()'><h5><strong>View Stockist </strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddStockistRegistration').html(html);
	if (index == 1) {
		$('#addStockistRegistrationTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewStockistRegistrationTabEntry2').addClass("active");
	}
}

//function validateStockistDetails()
//{
//	var z=$('#saveStockist').parsley('validate');
//}
//
function stockistRegForm(){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Stockist Registration</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddStockistRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"<form id='saveStockist' action='"+contextApplicationPath+"/StockistController/saveStockist' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div id='selectOrganization' class='form-group'>"
		/*+"                                                                <label class='form-label'><strong>Organization Name</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='orgName' id='OrgName' onchange='getTransporter()' class='form-control' class='form-control'>"//onchange='getCompanies()'
		+"																		<option disabled selected> Select Organization</option>";
																				for (var i = 0; i < data.orgArray.length; i++) {
																					html += "<option value='" + data.orgArray[i].orgId + "'>"+ data.orgArray[i].orgName + "</option>";
																				}
   html +="																	  </select>"
		+"                                                                </div>"*/
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Stockist Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input name='stockistName' type='text' placeholder='Stockist Name' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>State*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control' required>"
		+"																		<option disabled selected> Select State</option>"
	    +"																	</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>City*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='city' id='cityName' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select City</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Email ID</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='email' placeholder='Email ID' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Contact Person</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='contactPerson' placeholder='Contact Person' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group' style='height: 58px;'>"
		+"                                                                <label class='col-lg-12 control-label' style='padding-left: 0px;'><strong>Stockist Location*</strong></label>"
		+"                                                                <div class='col-lg-12'>"
		+"                                                                    <div class='pos-rel'>"
		+"                                                                        <input tabindex='13' id='ctest' type='radio' name='location' value='Local' ' required>"
		+"                                                                        <label class='p-l-40' for='flat-checkbox-1'>Local</label>"
		+"                                                                    </div>"
		+"                                                                    <div class='pos-rel'>"
		+"                                                                        <input tabindex='14' id='ctest' type='radio' name='location' value='Outstation'  required>"
		+"                                                                        <label class='p-l-40' for='flat-checkbox-2'>Outstation</label>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='from-group' >"
		
		//-------------------									
		+"															<div id='licenseForm'>"							
		+"                                                                <div class='col-md-12' style='padding-left:0px;'>"
		+"                                                                    <div class='form-group' style='width: 30%; float: left;'>"
		+"                                                                        <label class='form-label'><strong>Drug License No</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input type='text' name='license[]' placeholder='Drug License No' class='form-control'>"
		+"                                                                        </div>"
		+"                                                                    </div>"                                                            
		+"                                                                    <div class='form-group' style='width: 28%; float: left;'>"
		+"                                                                        <label class='form-label'><strong>From Date</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input  id='frmDate0' name='frmDate[]' class='dateClassCommon form-control' onchange='dataChange(\""+"frm"+"\","+0+")' type='text' placeholder='From Date' style='height: 36px; padding-left: 10px;'>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                    <div class='form-group' style='width: 28%; float: left;'>"
		+"                                                                        <label class='form-label'><strong>To Date</strong>"
		+"                                                                        </label>"
		+"                                                                        <span class='tips'></span>"
		+"                                                                        <div class='controls'>"
		+"                                                                            <input  id='toDate0' name='toDate[]' onchange='dataChange(\""+"to"+"\","+0+")' class='dateClassCommon form-control' type='text' placeholder='To Date' style='height: 36px; padding-left: 10px;'>"
		+"                                                                        </div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"  												</div>"			
		//-------------------
		+"                                                                <div class='col-md-12'>"
		+"                                                                    <div class='pull-left'>"
		+"                                                                        <input type='button' class='btn btn-info' onClick='addInput()' value='Add More'>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            </br>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>VAT*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='vat' placeholder='VAT' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>PAN No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='panNo' placeholder='PAN No' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
	/*	+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Company Name</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='companyName'  name='company' class='form-control' class='form-control'>"
		+"																		<option disabled selected> Select Company</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"*/
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Address*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input name='address' type='textarea' placeholder='Address' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Mobile No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input name='mobile' type='text' placeholder='Mobile No' class='form-control' parsley-type='onlynumber' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>District*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select District</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Fax No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input name='fax' type='text' placeholder='Fax No' class='form-control' parsley-type='onlynumber'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Proprietor/Partner*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='pro_part' placeholder='Proprietor/Partner' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Transporter*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='transporter' id='transName' name='transName' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Transporter*</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>CST</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input name='cst' type='text' placeholder='CST' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		//-----------------validity
		+"									<div id='validityDiv'>"
		+"                                                            <div class='form-group' style='height: 58px;'>"
		+"                                                                <label class='form-label'><strong>Validity</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div id='lbl0'>"
		+"                                                                   <label id='validitylbl0' class='form-label'><strong>0 year(s) 0 month(s) 0 day(s)</strong></label><input id='validitytxt0' name='validities[]' type='hidden'>"
		+"                                                                </label>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"</div>"
		//-----------------/validity
		+"                                                                <div class='col-md-12' style='height:37px;'>"
		+"                                                                </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>User Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input name='uname' type='text' placeholder='User Name' class='form-control' required>"
		+"                                                                </div>"
		+"																  <div id='error' class='validationError' ></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Password*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input name='password' type='password' placeholder='Password' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#saveStockist\")'>Save</button>"
		+"                                                                    <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                                </div>"
		+"</form>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='stockistListing'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForAddStockistRegistration(1);
	$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	ajaxStockistRegForm("saveStockist");
	displayOrganizationDropDown(3);
	stateDropDownList();
//	stockistListing();
}
//
function ajaxStockistRegForm(formId){
	var options = {
			dataType : 'json',
			success : function(data) {
				$('#loader').hide();
				if(data.result==true){
					$('#'+ formId)[0].reset();
//					stockistListing();
					alert("Stockist Added Successfully");
					$('#loader').hide();
				}
				else{
					if(data.FLAG==true){
						alert("User Name already exists");
						$('#error').html(data.MSG);
						$('#loader').hide();
					}
					else{
						alert("Add Stockist Failed");
						$('#loader').hide();
					}
				}
			}
	};
	$('#saveStockist').ajaxForm(options);
}


//state dropdown 
function stateDropDownList(){
	$.post(contextApplicationPath+'/StockistController/displayStockistDetailsForm',function(data){
		var optionHtml="<option disabled selected>Select State</option>";
		for (var i = 0; i < data.stateArray.length; i++) {
			optionHtml += "<option value='" + data.stateArray[i].stateId + "'>"+ data.stateArray[i].stateName + "</option>";
		}
		$("#stateName").empty();
		$("#stateName").append(optionHtml);
	},'json');
}
//stockist listing
function stockistListing(){
	$('#loader').show();
//	$.post(contextApplicationPath+'/StockistController/getStokistList',function(data){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Stockist Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddStockistRegistration'></div>"
		+"<div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+"                                                                        <option value='StockistName'>Stockist Name</option>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
		//+"                                                                        <option>Company Name</option>"
//		+"                                                                        <option value='StockistID'>Stockist ID</option>"
		+"																		  <option value='Transporter'>Transporter Name</option>"
		+"                                                                        <option value='ContactPerson'>Contact Person</option>"
//		+"                                                                        <option value='Mobile'>Mobile No.</option>"
		+"                                                                        <option value='Email'>Email ID</option>"
		+"                                                                        <option value='Location'>Location</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick=\"searchStockistDetails(1)\">Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+ "                                                        <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>";
//	$('#stockistListing').empty();
	$('#myTabContent').html(html);
	subTabsForAddStockistRegistration(2);
//    $('#stockistListing').html(html);
//    stockistListingView(data);
	commonStockistDetails("","",1);
	$('#loader').hide();
//	}, 'json');
}


function stockistListingView(data,from,url){
	//alert("stockistListingView");
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html = ""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		//+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		//+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Contact Person</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Mobile No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Email ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Location</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
																		for (counter = 0; counter < data.stockistArray.length; counter++) {
																			html+="<tr style='text-align: center;'>"
																				+" <td>"+SR_NO+"</td>"
																				+" <td >"+data.stockistArray[counter].orgName+"</td>"
																				+" <td id='stockistId"+counter+"'>"+data.stockistArray[counter].stockistId+"</td>"
																				+" <td id='stockistName"+counter+"'>"+data.stockistArray[counter].stockistName+"</td>"
																				+" <td id='contactPerson"+counter+"'>"+data.stockistArray[counter].contactPerson+"</td>"
																				+" <td id='mobile"+counter+"'>"+data.stockistArray[counter].mobile+"</td>"
																				+" <td id='email"+counter+"'>"+data.stockistArray[counter].email+"</td>"
																				+" <td id='location"+counter+"'>"+data.stockistArray[counter].location+"</td>"
																				+" <td id='LINKS"+counter+"'><a class='edit btn btn-blue' onClick='viewStockist("+data.stockistArray[counter].stockistId+")'  href='javascript:;'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' onClick='editStockistRegDetails("+data.stockistArray[counter].stockistId+","+counter+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteStockist("+data.stockistArray[counter].stockistId+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
																				+"</tr>"
																				SR_NO++;
																		}
    html+="                                                                </tbody>"
		+"                                                            </table>";
    $('#searchResult').html(html);
    
    paginationView(from,data.paginationCount,url);
}

function commonStockistDetails(searchOption,searchText,from){
	$.post(contextApplicationPath+'/StockistController/searchStockistDetails', {
				searchOption : searchOption,
				searchText : searchText,
				from : from
			},
			function(data) {
				var url = "commonStockistDetails(\""+searchOption+"\",\""+searchText+"\",";
				stockistListingView(data,from,url);
			}, 'json');
}

function searchStockistDetails(from){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commonStockistDetails(searchOption,searchText,from);
}
function viewStockist(stockistId){
	viewStockistDetailsPopUp(stockistId)
}
/*function editStockist(stockistId){
	alert("editStockist= "+stockistId);
}*/
function deleteStockist(stockistId){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
	$.post(contextApplicationPath+'/StockistController/deleteStockistDetails', {
		stockistId : stockistId
	}, function(data) {
			stockistListing();
			alert(data.MSG);
	}, 'json');
    }
}
function getOrganization(){
	$.post(contextApplicationPath+'/StockistController/addStockistBankDetailsForm',function(data){
		//alert("org array="+data.orgArray.length);
		optionHtml="<option disabled selected> Select Organization</option>";
		for (var i = 0; i < data.orgArray.length; i++) {
			optionHtml += "<option value='" + data.orgArray[i].orgId + "'>"+ data.orgArray[i].orgName + "</option>";
		}
		$("#orgNameId").empty();
		$("#orgNameId").append(optionHtml);
	}, 'json');
}


var fromDateArr=[];
var toDateArr=[];
function dataChange(position,i) {
	//alert(position+","+i);
    if(position=='frm') 
    	fromDateArr[i] = $("#frmDate"+i).val();
    else if(position=='to')
    	toDateArr[i] =  $("#toDate"+i).val();
    if(fromDateArr[i]!=""&&toDateArr[i]!=""&&fromDateArr[i]!=undefined&&toDateArr[i]!=undefined&&fromDateArr.length>0&&toDateArr.length>0){
    	/*var From_date = new Date($("#frmDate").val());
    	var To_date = new Date($("#toDate").val());*/
    	if(fromDateArr[i]>toDateArr[i]){
    		alert("from date should be less");
    		if(position=='frm')
        		$('#frmDate'+i).val('');
            else if(position=='to')
            	$('#toDate'+i).val('');
    		var html = "<label id='validitylbl"+i+"'' class='form-label'><strong>0 year(s) 0 month(s) 0 day(s)</strong></label><input id='validitytxt"+i+"' name='validities[]' type='hidden' value='0 year(s) 0 month(s) 0 day(s)'>";
    		$('#lbl'+i).empty();
    		$('#lbl'+i).html(html);
    	}else{
    		CalculateDiff(i);
    	}
    	
    }
}
function addInput() {
	 var newdiv = document.createElement('div');
	  newdiv.innerHTML = "<div id='frm"+rowCounter+"' class='col-md-12' style='padding-left:0px;'><div class='form-group' style='width: 30%; float: left;'>"
						+"<span class='tips'></span><div class='controls'><input type='text' name='license[]' placeholder='Drug License No' class='form-control'></div></div>"                                           
						+"<div class='form-group' style='width: 28%; float: left;'>"
						+"<span class='tips'></span><div class='controls'><input id='frmDate"+rowCounter+"' onchange='dataChange(\""+"frm"+"\","+rowCounter+")' name='frmDate[]'class='dateClassCommon form-control'  type='text' placeholder='From Date'></div></div>"
						+"<div class='form-group' style='width: 28%; float: left;'><span class='tips'></span>"
						+"<div class='controls'><input class='dateClassCommon form-control' id='toDate"+rowCounter+"' name='toDate[]' onchange='dataChange(\""+"to"+"\","+rowCounter+")' type='text' placeholder='To Date'></div></div>"
						+"<div onclick='removerow("+ rowCounter +")'><a class='delete btn btn-danger' style=' margin-left: 8px; width: 55px;height: 36px;'><i class='fa fa-times-circle' style='margin-left: -3px; margin-top: 3px;'></i> </a></div>";

	document.getElementById('licenseForm').appendChild(newdiv);

	var newdiv1 = document.createElement('div');
	newdiv1.innerHTML = "<div id='validity"+rowCounter+"' class='form-group' style='height: 37px;'><div id='lbl"+rowCounter+"'><label id='validitylbl"+rowCounter+"'' class='form-label'><strong>0 year(s) 0 month(s) 0 day(s)</strong></label><input id='validitytxt"+rowCounter+"' name='validities[]' type='hidden' value='0 year(s) 0 month(s) 0 day(s)'></div></div>";
	document.getElementById('validityDiv').appendChild(newdiv1);
  
	$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+20" });
	rowCounter++;
}
function removerow(rowid){
  $("#frm"+rowid).remove();
  $("#validity"+rowid).remove();
  
  fromDateArr[rowid] = "";
  toDateArr[rowid] ="";
}
//To get company list according to organization
//This function used at many places so dont change its code...Thank you
function getCompanies(){
	var orgId = $('#OrgName').val();
	if(orgId==null||orgId==undefined)
		return false;
	$.post(contextApplicationPath+'/CompanyController/getCompanies', {
		orgId : orgId
	}, function(data) {
		//$('#loader').hide();
		var counter = 0;
		var optionHtml = "<option disabled selected>Select Company</option>";
		for (counter = 0; counter < data.companyArray.length; counter++) {
			optionHtml += "<option value='" + data.companyArray[counter].compId + "'>"
			+ data.companyArray[counter].compName + "</option>";
		}
		$("#companyName").empty();
		$("#companyName").append(optionHtml);
	}, 'json');
	//used in outward and inward courier reg
	hideDiv(2);
}
//To get Transporter drop down values list
function getTransporter(){
	var orgId = $('#OrgName').val();
	console.log("organization id-"+orgId);
	if(orgId==null||orgId==undefined)
		return false;
	$.post(contextApplicationPath+'/TransporterDetailsController/getTransporterList', {
		orgId : orgId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Transporter</option>";
		for (counter = 0; counter < data.transporterArray.length; counter++) {
			optionHtml += "<option value='" + data.transporterArray[counter].transId + "'>"
			+ data.transporterArray[counter].transName + "</option>";
			console.log("transporter name stockist 614-"+data.transporterArray[counter].transName);
		}
		$("#transName").empty();
		$("#transName").append(optionHtml);
	}, 'json');
}

//To get District Dropdown values list
function getDistrict(){
	var stateId = $('#stateName').val();
	$.post(contextApplicationPath+'/DistrictController/getDistrict', {
		stateId : stateId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected>Select District</option>";
		for (counter = 0; counter < data.districtArray.length; counter++) {
			optionHtml += "<option value='" + data.districtArray[counter].distId + "'>"
			+ data.districtArray[counter].distName + "</option>";
		}
		$("#districtName").empty();
		$("#districtName").append(optionHtml);
	}, 'json');
}
//To get city dropdown values list
function getCities(){
	var distId = $('#districtName').val();
	$.post(contextApplicationPath+'/CityController/getCities', {
		distId : distId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select City</option>";
		for (counter = 0; counter < data.cityArray.length; counter++) {
			optionHtml += "<option value='" + data.cityArray[counter].cityId + "'>"
			+ data.cityArray[counter].cityName + "</option>";
		}
		$("#cityName").empty();
		$("#cityName").append(optionHtml);
	}, 'json');
}

function subTabsForAddStockistBankDetails(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addStockistBankDetailsTabEntry1' class=''><a href='#tab1_2' data-toggle='tab' onclick='addStockistBank()'><h5><strong>Add Stockist Bank Details</strong></h5></a></li>"
			+ "                                            <li id='viewStockistBankDetailsTabEntry2' class=''><a href='#tab1_2_1' data-toggle='tab' onclick=\"stockistBankList()\"><h5><strong>View Stockist Bank Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddStockistBankDetails').html(html);
	if (index == 1) {
		$('#addStockistBankDetailsTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewStockistBankDetailsTabEntry2').addClass("active");
	}
}

//function validateStockistBankDetails()
//{
//	var z=$('#saveStockistBank').parsley('validate');
//}

function addStockistBank(){
		//alert("addStockistBank");
		var html=""
			
			+"                                <div class='tab-pane fade active in' id='tab1_2'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>Add Stockist Bank Details</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddStockistBankDetails'></div>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-12'>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"<form id='saveStockistBank' action='"+contextApplicationPath+"/StockistBankDetailsController/addStockistBankDetails' method='post'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div id='selectOrganization' class='form-group'>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Stockist*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='StockistName' name='stockistName' class='form-control' class='form-control' required>"
			+"                                                                        <option disabled selected> Select Stockist</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Branch*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='branch' placeholder='Branch' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>IFSC Code*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='ifsc' placeholder='IFSC Code' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Company*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='companyName' name='companyName' onchange='getStockist()' class='form-control' class='form-control' required>"
			+"                                                                        <option disabled selected> Select Company</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Bank Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='bankName' placeholder='Bank Name' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Account No*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='accNo' placeholder='Account No' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                                <div class='pull-right'>"
			+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#saveStockistBank\")'>Add</button>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>"
			+"											  </form>"
//			+"											  <div id='stockistBankListing'>"
//			+"											  </div>"	
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html);
		subTabsForAddStockistBankDetails(1);
		ajaxAddStockistBank('saveStockistBank');
		displayOrganizationDropDown(1);
//		stockistBankList();
}
//ajax call
function ajaxAddStockistBank(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.result=true){
					$('#'+ formId)[0].reset();
//					stockistBankList();
					alert("Stockist Bank Details Added Successfully");
					$('#loader').hide();
				}
				else{
					alert("Add Stockist Bank Details Failed");
					displayAddStockist();
					$('#loader').hide();
				}
			}
	};
	$('#saveStockistBank').ajaxForm(options);
}

//
function getStockist(){
	//alert("getStockist");
	var orgId = $('#OrgName').val();
	var compId = $('#companyName').val();
	if(compId==null||compId==undefined){
		compId=0;return false;
	}
	if(orgId==null||orgId==undefined)
		orgId=0;
	//$('#loader').show(100);
	$.post(contextApplicationPath+'/StockistController/getStockist', {
		orgId : orgId,
		compId : compId
	}, function(data) {
		//$('#loader').hide();
		var counter = 0;
		var optionHtml = "<option disabled selected>Select Stockist</option>";
		for (counter = 0; counter < data.stockistArray.length; counter++) {
			optionHtml += "<option value='" + data.stockistArray[counter].stockistId + "'>"
			+ data.stockistArray[counter].stockistName + "</option>";
		}
		$("#StockistName").empty();
		$("#StockistName").append(optionHtml);
		$("#StockistName").html($("#StockistName option").sort(function (a, b) {
		    return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
		}))
	}, 'json');
}

/*function getOrganization(){
	$.post(contextApplicationPath+'/StockistController/addStockistBankDetailsForm',function(data){
		//alert("org array="+data.orgArray.length);
		optionHtml="<option disabled selected> Select Organization</option>";
		for (var i = 0; i < data.orgArray.length; i++) {
			optionHtml += "<option value='" + data.orgArray[i].orgId + "'>"+ data.orgArray[i].orgName + "</option>";
		}
		$("#orgNameId").empty();
		$("#orgNameId").append(optionHtml);
	}, 'json');
}*/


//stockist bank Listing
function stockistBankList(){
	//alert("stockist Bank List");
	$('#loader').show();
//	$.post(contextApplicationPath+'/StockistBankDetailsController/stockistBankDetailsList',function(data){
		//commonData is use to edit bank info
//		commonData = data.stockistBankArray;
		//
		var html=""
			
			+"                                <div class='tab-pane fade active in' id='tab1_2_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Stockist Bank Details</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddStockistBankDetails'></div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
			+"                                                                        <option value='StockistName'>Stockist Name</option>"
			+"                                                                        <option value='Organization'>Organization Name</option>"
			+"                                                                        <option value='Company'>Company Name</option>"
			+"                                                                        <option value='BankName'>Bank Name</option>"
			+"                                                                        <option value='Branch'>Branch</option>"
			+"                                                                        <option value='AccountNo'>Account No</option>"
			+"                                                                        <option value='IFSC'>IFSC Code</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input id='searchText' type='text' class='form-control'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick=\"searchStokistBankDetails(1)\">Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+"                                                    </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+"                                                </div>"
			+"                                            </div>";
		$('#myTabContent').html(html);
		subTabsForAddStockistBankDetails(2);
		$('#stockistBankListing').empty();
		$('#stockistBankListing').html(html);
		commanStockistBankList("","",1);
		$('#loader').hide();
//	}, 'json');
}
//
function stockistBankListView(data,from,url){
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Bank Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Branch</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Account No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>IFSC Code</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
																		for (counter = 0; counter < data.stockistBankArray.length; counter++) {
																			html+="<tr style='text-align: center;'>"
																				+" <td>"+SR_NO+"</td>"
																				+" <td>"+data.stockistBankArray[counter].ORG_NAME+"</td>"
																				+" <td>"+data.stockistBankArray[counter].COMPANY+"</td>"
																				+" <td>"+data.stockistBankArray[counter].STOCKIST_NAME+"</td>"
																				+" <td id='BANK_NAME"+counter+"'>"+data.stockistBankArray[counter].BANK_NAME+"</td>"
																				+" <td id='BRANCH"+counter+"'>"+data.stockistBankArray[counter].BRANCH+"</td>"
																				+" <td id='ACC_NO"+counter+"'>"+data.stockistBankArray[counter].ACC_NO+"</td>"
																				+" <td id='IFSC"+counter+"'>"+data.stockistBankArray[counter].IFSC+"</td>"
																				+" <td><a class='edit btn btn-dark' onClick='editStockistBankDetails("+data.stockistBankArray[counter].STOCKIST_BANK_ID+","+counter+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteStockistBank("+data.stockistBankArray[counter].STOCKIST_BANK_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
																				+"</tr>";
																			SR_NO++;
																		}
	html+="                                                                </tbody>"
		+"                                                            </table>";
	$('#searchResult').html(html);
	paginationView(from,data.paginationCount,url);
}
//

function commanStockistBankList(searchOption,searchText,from)
{
	$.post(contextApplicationPath+'/StockistBankDetailsController/searchStokistBankDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		commonData = data.stockistBankArray;
		var url = "commanStockistBankList(\""+searchOption+"\",\""+searchText+"\",";
		stockistBankListView(data,from,url);
	}, 'json');
}


function searchStokistBankDetails(from){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commanStockistBankList(searchOption,searchText,from);
}



//Edit Stockist Bank
function editStockistBank(stockistBankId){
	alert("Edit : "+stockistBankId);
}
//Delete Stockist Bank
function deleteStockistBank(stockistBankId){
	//alert("Delete : "+stockistBankId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/StockistBankDetailsController/deleteStockistBankDetails', {
			stockistBankId : stockistBankId
		}, function(data) {
			stockistBankList();
				alert(data.MSG);
		}, 'json');
    }
}

function subTabsForAddStockistCompany(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addStockistCompanyTabEntry1' class=''><a href='#tab1_3' data-toggle='tab' onclick='displayAddStockistCompany()'><h5><strong>Add Stockist Company</strong></h5></a></li>"
			+ "                                            <li id='viewStockistCompanyTabEntry2' class=''><a href='#tab1_3_1' data-toggle='tab' onclick='addCompanyToStockistList()'><h5><strong>View Stockist Company </strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddStockistCompany').html(html);
	if (index == 1) {
		$('#addStockistCompanyTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewStockistCompanyTabEntry2').addClass("active");
	}
}


function displayAddStockistCompany(){
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab1_3'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>Add Stockist Company</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddStockistCompany'></div>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-12'>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-heading'>"
			+"                                                    <h3 class='panel-title'><strong>Add Stockist Company</strong></h3>"
			+"                                                </div>"
			+"											<form id='addCompanyToStockist' action='"+contextApplicationPath+"/StockistController/addCompanyToStockist' method='post'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div  id='selectOrganization' class='form-group'>"
			+"                                                            </div>"
			+"																<div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Company*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"<div id='selectCompanyList'><select id='selectCompany' name='companies' class='form-control' multiple='multiple' style='display: none;' required></select></div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Stockist*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='selectStockist' name='stockist' onchange='getCompanyList()' class='form-control' required>"
			 +"																		<option disabled selected> Select Stockist</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"                                                            
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                                <div class='pull-right'>"
			+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addCompanyToStockist\")'>Add</button>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+" 												</form>"
			+"                                            </div>"
//			+"											  <div id='CompanyToStockistListing'>"
//			+"											  </div>"	
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html);
		subTabsForAddStockistCompany(1);
		$('#selectCompany').multiselect({
			header: "Select Company"
		});
		ajaxAddCompanyToStockistForm('addCompanyToStockist');
		displayOrganizationDropDown(2);
//		addCompanyToStockistList();
}
// ajax call to add companies to stockist
function ajaxAddCompanyToStockistForm(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				if(data.result==true){
					$('#'+ formId)[0].reset();
//					addCompanyToStockistList();
					$('#selectCompany').html('');
					alert("Company Added to Stockist Successfully....!");
					$('#loader').hide();
				}
				else{
					alert("Operation Failed......!");
					$('#loader').hide();
				}
			}
	};
	$('#addCompanyToStockist').ajaxForm(options);
}

//get all companies accoding to org an
function getCompanyList(){
	var stockistId = $('#selectStockist').val();
	var orgId = $('#OrgName').val();
	var optionHtml =" <select id='selectCompany' name='companies' class='form-control' multiple='multiple' style='display: none;'>";
	
	//alert("stockistId = "+stockistId+" org = "+orgId);
	//$('#loader').show(100);
	$.post(contextApplicationPath+'/CompanyController/getCompanyList', {
		stockistId : stockistId,
		orgId      : orgId
	}, function(data) {
		//$('#loader').hide();
		var counter = 0;
		for (counter = 0; counter < data.companyArray.length; counter++) {
			var stat=false;
			for (var j = 0; j < data.stockistCompanyArray.length; j++) {
				if(data.stockistCompanyArray[j].COMPANY_ID==data.companyArray[counter].COMPANY_ID)
					stat=true;
			}
			if(stat)
				optionHtml += "<option selected='selected' value='" + data.companyArray[counter].COMPANY_ID + "'>"+ data.companyArray[counter].COMPANY_NAME + "</option>";
			else
				optionHtml += "<option value='" + data.companyArray[counter].COMPANY_ID + "'>"+ data.companyArray[counter].COMPANY_NAME + "</option>";
		}
		optionHtml +="</select>";
		
		$("#selectCompanyList").empty();
		$("#selectCompanyList").html(optionHtml);
		$('#selectCompany').multiselect({
			header: "Select Company"
		});
	}, 'json');
}

//get stockist according to organization 
function getStockistByOrg(){
	
	var orgId = $('#OrgName').val();
	//alert("org = "+orgId);
	$.post(contextApplicationPath+'/StockistController/getStockistByOrg', {
		orgId : orgId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Stockist</option>";
		for (counter = 0; counter < data.stockistArray.length; counter++) {
			optionHtml += "<option value='" + data.stockistArray[counter].stockistId + "'>"
			+ data.stockistArray[counter].stockistName + "</option>";
		}
		$("#selectStockist").empty();
		$("#selectStockist").append(optionHtml);
	}, 'json');
}

//According to Organization Stockist list
function addCompanyToStockistList(){
	$('#loader').show();
//	$.post(contextApplicationPath+'/StockistController/addCompanyToStockistList',function(data){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_3'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Stockist Company</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddStockistCompany'></div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+"                                                                        <option value='StockistName'>Stockist Name</option>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
	//	+"                                                                        <option value=''>Company Name</option>"
		
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick=\"searchOrganizationStockist(1)\">Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+ "                                                        <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>";
	$('#myTabContent').html(html);
	subTabsForAddStockistCompany(2);
    $("#CompanyToStockistListing").empty();
	$("#CompanyToStockistListing").html(html);
//	showStockistCompany(data);
	commanStockistCompany("","",1);
	$('#loader').hide();
//	}, 'json');
}

function showStockistCompany(data,from,url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
var html="                                                            <table class='table table-striped table-hover'>"
	+"                                                                <thead class='no-bd'>"
	+"                                                                    <tr>"
	+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
	+"                                                                        </th>"
	+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
	+"                                                                        </th>"
	+"                                                                        <th style='text-align: center;'><strong>Stockist Name</strong>"
	+"                                                                        </th>"
	+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
	+"                                                                        </th>"
	+"                                                                    </tr>"
	+"                                                                </thead>";
															for (var counter = 0; counter < data.stockistArray.length; counter++) {
																html+="<tbody class='no-bd-y'>"
																+" <tr style='text-align: center;'>"
																+" <td>"+SR_NO+"</td>"
																+" <td>"+data.stockistArray[counter].ORG_NAME+"</td>"
																+" <td>"+data.stockistArray[counter].STOCKIST_NAME+"</td>"
																+" <td><a class='edit btn btn-blue' onClick='viewStockistCompanies("+data.stockistArray[counter].STOCKIST_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a></td>"
																+" </tr>"
																SR_NO++;
															}
html+=" </tbody>"
+"                                                            </table>";
$("#searchResult").html(html);

paginationView(from,data.paginationCount,url);
}


function commanStockistCompany(searchOption,searchText,from)
{
	$.post(contextApplicationPath+'/StockistController/searchOrganizationStockist', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	},
	function(data) {
		var url = "commanStockistCompany(\""+searchOption+"\",\""+searchText+"\",";
		showStockistCompany(data,from,url);
	}, 'json');
}

function searchOrganizationStockist(from){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();//searchOption searchText
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commanStockistCompany(searchOption,searchText,from);
}
//View Stockist Company
function viewStockistCompanies(stockistId){
	//alert(stockistId);
	var html="" 
		+ "<div class='modal fade in' id='modal-responsive' >"
		            + "    <div class='col-md-13'>"
		            + "        <div class='modal-content'>"
		            + "            <div class='modal-header'>"
		            + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		            + "                <h4 class='modal-title' id='myModalLabel'><strong>View Stockist Companies</strong></h4>"
		            + "            </div>"
		            + "  <div class='panel panel-default'>"
		            + "<div class='panel-body'>"
		            + "    <div class='row'>";
	
	$.post(contextApplicationPath+'/StockistController/viewStockistCompanies', {stockistId : stockistId}, function(data) {
			   html+= "							         <div class='col-md-4'>"
					/*+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Organization Name : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>asdadas</strong>"
					+"                                                                </label>"
					+"                                                            </div>"*/
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Company List : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					/*+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>Stockist Name : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>dsaaad</strong>"
					+"                                                                </label>"
					+"                                                            </div>"*/
					+"															</div>"
					+" 						   </div>"
					//---
					+"                                                <div class='panel-body'>"
					+"                                                    <div class='row'>"
					+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"                                                            <table class='table table-striped table-hover'>"
					+"                                                                <thead class='no-bd'>"
					+"                                                                    <tr>"
					+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
					+"                                                                        </th>"
					+"                                                                    </tr>"
					+"                                                                </thead>"
					+"                                                                <tbody class='no-bd-y'>";
			   var count =1;
			 $(data).each(function(index,element){
			html+="<tr style='text-align: center;'>"
				+"   <td>"+count+"</td>"
				+"   <td>"+element.COMPANY_NAME+"</td>"
				+" </tr>";
			count++;
			 });
html+="                                                           </tbody>"
	+"                                                        </table>"
	+"                                                     </div>"
	+"                                                 </div>"
	+"                                          </div>"
//===
+"  </div>";
	//});
		html+= "            <div class='modal-footer text-center'>"
            + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
            + "            </div>"
            + "        </div>"
            + "    </div>"
            + "</div>";
	$('#popup').html(html);
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block"); 
}, 'json');
}

//This function used at many places so dont change its code...Thank you
function displayOrganizationDropDown(index){
	$.post(contextApplicationPath+'/StockistController/organizationListDropdown',function(data){
		var html ="";
		if(data.orgArray.length>1){
			html+="<div class='form-group'><label class='form-label'><strong>Select Organization*</strong>"
				+" </label>"
				+" <span class='tips'></span>"
				html+=" <div class='controls'>";
			
			if(index==4)
				html+=" <select id='OrgName' name='orgName' onchange='displyaCateringAgentDropDown()' class='form-control' class='form-control' required>";
			else if(index==3)
				html+=" <select id='OrgName' name='orgName' onchange='getTransporter()' class='form-control' class='form-control' required>";
			else if(index==2)
				html+=" <select id='OrgName' name='orgName' onchange='getStockistByOrg()' class='form-control' class='form-control' required>";
			else if(index==0)
				html+=" <select id='OrgName' name='orgName' class='form-control' class='form-control' required>";
			else 
				html+=" <select id='OrgName' name='orgName' onchange='getCompanies()' class='form-control' class='form-control' required>";
			html+="<option disabled selected> Select Organization</option>";
					for (var i = 0; i < data.orgArray.length; i++) {
					html += "<option value='" + data.orgArray[i].orgId + "'>"+ data.orgArray[i].orgName + "</option>";
					}
			html+="</select>"
				+"</div></div>";	
		}else if(data.orgArray.length>0){
			html+="<div class='form-group'><label class='form-label'><strong>Your Organization</strong>"
				+"</label>"
				+"<span class='tips'></span>"
				+"<div class='controls'>"
				+"<label class='form-label'><strong></strong>"+data.orgArray[0].orgName+"</label>"
				+"<input type='hidden' value='"+data.orgArray[0].orgId+"' name='orgName' id='OrgName'>"
				+"</div></div>";	
		}
		//$('#selectOrganization').empty();
		$('#selectOrganization').html(html);
		switch(index){
		case 1 : getCompanies();break;
		case 2 : getStockistByOrg();break;
		case 3 : getTransporter();break;
		case 4 : displyaCateringAgentDropDown();break;
		}
		return 0;
	}, 'json');
	
}

function subTabsForAddStockistUploadDocument(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addStockistUploadDocumentTabEntry1' class=''><a href='#tab1_4' data-toggle='tab' onclick='stockistUploadDocumentForm()'><h5><strong>Stockist Upload Document</strong></h5></a></li>"
			+ "                                            <li id='viewStockistUploadDocumentTabEntry2' class=''><a href='#tab1_4_1' data-toggle='tab' onclick='displayUploadDocumentListing()'><h5><strong>View Stockist Uploaded Document</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddStockistUploadDocument').html(html);
	if (index == 1) {
		$('#addStockistUploadDocumentTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewStockistUploadDocumentTabEntry2').addClass("active");
	}
}


//stockist upload document form
function stockistUploadDocumentForm(){
	
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab1_4'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>Stockist Upload Document</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddStockistUploadDocument'></div>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-12'>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"													<form id='saveStockistUploadDocument' action='"+contextApplicationPath+"/StockistController/stockistUploadDocument' method='post' enctype='multipart/form-data'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div id='selectOrganization' class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Organization</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='OrgName' name='orgName' onchange='getCompanies()' class='form-control' class='form-control' required>"
			 +"																		<option disabled selected> Select Organization</option>";
																				    /*for (var i = 0; i < data.orgArray.length; i++) {
																				    	html += "<option value='" + data.orgArray[i].orgId + "'>"+ data.orgArray[i].orgName + "</option>";
																					}*/
		html+="                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Stockist*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select  id='StockistName' name='stockistId' class='form-control' class='form-control' required>"
			+"                                                                        <option disabled selected> Select Stockist</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Browse File*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='file' name='documentImage' class='form-control'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Company*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='companyName' name='company' onchange='getStockist()' class='form-control' class='form-control' required>"
			+"                                                                        <option disabled selected> Select Company</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Document Type*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"<div id='documentList'></div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div id='stockistDocumentImageDivId'>"
			+"                                                                <label class='form-label'><strong>Add More Files</strong>"
			+"                                                                </label>"
			+"															  </div>"
			+"                                                            <div class='form-group'>"
			+"                                                				  <div id='stockistDocumentImageAddMoreButtonId'><button onclick=\"addFileConrol('stockistDocumentImageDivId','stockistDocumentImageAddMoreButtonId','documentImage',1)\" class='btn btn-primary'>Add More Files</button></div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                                <div class='pull-right'>"
			+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#saveStockistUploadDocument\")'>Submit</button>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"													</form>"
			+"                                            </div>"
//			+"											  <div id='uploadDocListing'>"
//			+"											  </div>"	
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html);
		subTabsForAddStockistUploadDocument(1);
		//use function in organization.js for loading document type
		displayOrganizationDropDown(1);
		loadDocumentTypeList();
		ajaxStockistUploadDocumentForm("saveStockistUploadDocument");
//		displayUploadDocumentListing();
}
//ajax call for Stockist Upload Document Form
function ajaxStockistUploadDocumentForm(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				if(data.result==true){
					
					$('#'+ formId)[0].reset();
//					displayUploadDocumentListing();
					alert("Stockist document uploaded successfully....!");
					$('#loader').hide();
				}
				else{
					if(data.MSG!=""||data.MSG!=undefined)
						alert(data.MSG);
					else
						alert("Operation Failed......!");
					$('#loader').hide();
				}
			}
	};
	$('#saveStockistUploadDocument').ajaxForm(options);
}
//Stockist Uploaded Doc List
function displayUploadDocumentListing(){
	$('#loader').show();
//	$.post(contextApplicationPath+'/StockistController/uploadDocumentListing',function(data){
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab1_4'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Stockist Upload Document</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddStockistUploadDocument'></div>"
			+"<div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
			+"                                                                        <option value='StockistName'>Stockist Name</option>"
			+"                                                                        <option value='Organization'>Organization Name</option>"
			+"                                                                        <option value='Company'>Company Name</option>"
			+"                                                                        <option value='DocumentType'>Document Type</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchUploadedDocumentsDetails()'>Show</button>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+"                                                    </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>";
		$('#myTabContent').html(html);
		subTabsForAddStockistUploadDocument(2);
		$('#uploadDocListing').empty();
		$('#uploadDocListing').html(html);
		commonUploadedStockisDocumentController("", "", 1);
//		displayUploadDocumentListingView(data);t
		$('#loader').hide();
//	}, 'json');
}

function commonUploadedStockisDocumentController(searchOption, searchText, from)
{
	$.post(contextApplicationPath+'/StockistController/searchUploadedDocumentsDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonUploadedStockisDocumentController(\""+searchOption+"\",\""+searchText+"\",";
		displayUploadDocumentListingView(data,from,url);
	}, 'json');
}
//
function displayUploadDocumentListingView(data,from,url){//alert("displayUploadDocumentListingView");
	//urlForEdit & commonData is global var
	urlForEdit = url;
	commonData = data;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Document Type</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>File Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
																	for (var i = 0; i < data.length; i++) {
//																		alert("data"+data);
																		if((data.length-2)<i)
																			break;
																		html+=" <tr style='text-align: center;'>"
																		+"   <td>"+SR_NO+"</td>"
																		+"   <td>"+data[i].ORG_NAME+"</td>"
																		+"   <td>"+data[i].COMPANY_NAME+"</td>"
																		+"   <td>"+data[i].STOCKIST_NAME+"</td>"
																		+"   <td>"+data[i].DOCUMENT_TYPE+"</td>"
																		+"   <td><img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showStockistDocumentFileListPopup("+i+")'></td>"//+data.stockistDocsArray[i].FILE_NAME+"</td>"
																		+"   <td><a class='edit btn btn-dark' onClick='editStockistUploadDocument("+i+","+from+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteStockistUploadDocument("+data[i].STOCKIST_DOC_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a></td>"
																		+"   </tr>";
																		SR_NO++;
																	}
	html+="                                                                </tbody>"
		+"                                                            </table>";
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);
}
//
function showStockistDocumentFileListPopup(index){
	showListOfImagesPopup(commonData[index].FILE_NAME);
}
//
function searchUploadedDocumentsDetails(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commonUploadedStockisDocumentController(searchOption, searchText, 1);
//	$.post(contextApplicationPath+'/StockistController/searchUploadedDocumentsDetails', {
//		searchOption : searchOption,
//		searchText : searchText
//	}, function(data) {
//		displayUploadDocumentListingView(data);
//	}, 'json');
}
//edit Stockist Upload Document acc to Doc ID
function editStockistUploadDocument(index, from){
	editStockistUploadDocumentForm(index, from);
}
//delete Stockist Upload Document by Doc ID
function deleteStockistUploadDocument(stockistDocId){
	//alert(stockistDocId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/StockistController/deleteStockistDocDetails', {
			stockistDocId : stockistDocId
		}, function(data) {
			displayUploadDocumentListing();
				alert(data.MSG);
		}, 'json');
    }
}

//To calulate difference b/w two dates
function CalculateDiff(i) {
		var From_date = new Date($("#frmDate"+i).val());
		var To_date = new Date($("#toDate"+i).val());
		var diff_date =  To_date - From_date;
		
		var years = Math.floor(diff_date/31536000000);
		var months = Math.floor((diff_date % 31536000000)/2628000000);
		var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
		var txt = years+" year(s) "+months+" month(s) "+days+" day(s)";	
		var html = "<label id='validitylbl"+i+"'' class='form-label'><strong>"+txt+"</strong></label><input id='validitytxt"+i+"' name='validities[]' type='hidden' value='"+txt+"'>";
			
		$('#lbl'+i).empty();
		$('#lbl'+i).html(html);
		$('#validitytxt'+i).val(txt);
		//alert( years+" year(s) "+months+" month(s) "+days+" and day(s)");
}