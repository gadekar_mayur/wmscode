function editCartingAgentDetailsPopUp(agentId)
{
	var html="" 
		+ "<div class='modal fade in' id='modal-responsive' >"
	    + "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Carting Agent Details</strong></h4>"
		+ "            </div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>";
	$.post(contextApplicationPath+'/CartingAgentController/viewCartingAgentDetails', {agentId : agentId}, function(data) {
			$(data).each(function(index,element){
				//commonData is global data var
				commonData = element;
			html+="<form id='editCartingAgentDetails' action='"+contextApplicationPath+"/CartingAgentController/UpdateCartingAgentDetails' method='post'>"
			+"<input type='hidden' name='cartingAgentId' value='"+agentId+"'>"
			+"<input type='hidden' name='orgId' value='"+element.ORG_ID+"'>";
			html+="                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Organization Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' class='form-control' value='"+element.ORG_NAME+"' name='organization_name' readOnly>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Agent Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' class='form-control' value='"+element.AGENT_NAME+"' name='agentName' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Mobile No*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' class='form-control' value='"+element.MOBILE+"' name='mobile' parsley-type='onlynumber' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			/*+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Agent Photo*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                          <img src='"+element.AGENT_PHOTO+"' style='width:150px;height:150px;' >"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='file' class='form-control' id='field-2' name='agentPhoto'>"
			+"                                                                </div>"
			+"                                                            </div>"*/
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Address*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>" 
			+"                                                                    <input type='textarea' class='form-control' value='"+element.ADDRESS+"' id='field-2' name='address' required>"
			+"                                                                </div>"
			+"                                                            </div>"
            +"                                                            <div class='form-group'>"
            +"                                                     		    <button type='button' onclick='editCartingAgentPhotosListPopup("+agentId+")' class='btn btn-success'>Update Carting Agent Photo</button>"
		    +"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Vehicle No*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' class='form-control' value='"+element.VEHICLE_NO+"' name='vehicleNo' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Vehicle Model*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' class='form-control' value='"+element.VEHICLE_MODEL+"' name='vehicleModel' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			/*+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Vehicle Photo*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                          <img src='"+element.VEHICLE_PHOTO+"' style='width:150px;height:150px;' >"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='file' class='form-control'  id='field-2' name='vehiclePhoto'>"
			+"                                                                </div>"
			+"                                                            </div>"*/
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>User Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input name='username' type='text' value='"+element.USER_NAME+"' class='form-control' required>"
			+"                                                                </div>"
			+"																  <div id='error' class='validationError' ></div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Password*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input name='password' type='password' value='"+element.PASSWORD+"' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
            +"                                                            <div class='form-group'>"
            +"                                                     		    <button type='button' onclick='editCartingAgentVehiclePhotosListPopup("+agentId+")' class='btn btn-success'>Update Carting Agent Vehicle Photo</button>"
		    +"                                                            </div>"
			+"                                                    </div>"
			+"                                                    </div>"
			+ "            <div class='modal-footer text-center'>"
			+ "                <button type='submit'  class='btn btn-danger' onclick='validateFormDetails(\"#editCartingAgentDetails\")'>Update</button>"
	        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	        + "            </div>"
			+"</form>";
			
			html+="                                                </div>"
			    +"                                            </div>"
	            + "        </div>"
	            + "    </div>"
	            + "</div>";
			$('#popup').html(html);
			$("#modal-responsive").addClass("in");
			$("#modal-responsive").attr("aria-hidden","false");
			$("#modal-responsive").css("display","block"); 
			ajaxEditCartingAgent('editCartingAgentDetails');
		});
	}, 'json');	
}
			
function ajaxEditCartingAgent(FormId)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						$('#modal-responsive').remove();
						cartingAgentListing();
						//alert($('input:radio[name=location]:checked').val());
						/*$('#stockistName'+mainIndex).html($('#stockistNameId').val());
						$('#contactPerson'+mainIndex).html($('#contactPersonId').val());
						$('#mobile'+mainIndex).html($('#mobileId').val());
						$('#email'+mainIndex).html($('#emailId').val());
						if(data.STOCKIST_ID!=null||data.STOCKIST_ID!=undefined){
							//alert("NEW ID = "+data.STOCKIST_ID);
							var htmlText = "<a class='edit btn btn-blue' onClick='viewStockist("+data.STOCKIST_ID+")'  href='javascript:;'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' onClick='editStockistRegDetails("+data.STOCKIST_ID+","+mainIndex+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteStockist("+data.STOCKIST_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> ";
							$('#LINKS'+mainIndex).html(htmlText);
							$('#stockistId'+mainIndex).html(data.STOCKIST_ID);
							$('#location'+mainIndex).html($('input:radio[name=location]:checked').val());
						}*/
						//$('#modal-responsive').remove();
						
					}else if(data.RESULT==false){
						$('#error').html(data.MSG);
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
 $('#' + FormId).ajaxForm(options);
}

function editCartingAgentPhotosListPopup(cartingAgentId){
	editListOfImagesPopup(cartingAgentId,"documentImage",commonData.AGENT_PHOTO,"CARTING_AGENT_PHOTO_IMAGE" , "/CartingAgentController/updateCartingAgentPhotoImage");
}
//Image updation Ajax call after getting result from server
function ajaxCartingAgentPhotoImageUpdate(data){
	if(data.RESULT==true) {
		commonData.AGENT_PHOTO = data.AGENT_PHOTO;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Image
function deleteCartingAgentPhotoImage(cartingAgentPhotoImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CartingAgentController/deleteCartingAgentPhotoImage', {
			cartingAgentPhotoImageId : cartingAgentPhotoImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.AGENT_PHOTO[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
function editCartingAgentVehiclePhotosListPopup(cartingAgentId){
	editListOfImagesPopup(cartingAgentId,"documentImage",commonData.VEHICLE_PHOTO,"CARTING_AGENT_VEHICLE_PHOTO_IMAGE" , "/CartingAgentController/updateCartingAgentVehiclePhotoImage");
}
//Image updation Ajax call after getting result from server
function ajaxCartingAgentVehiclePhotoImageUpdate(data){
	if(data.RESULT==true) {
		commonData.VEHICLE_PHOTO = data.VEHICLE_PHOTO;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Image
function deleteCartingAgentVehiclePhotoImage(cartingAgentVehiclePhotoImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CartingAgentController/deleteCartingAgentVehiclePhotoImage', {
			cartingAgentVehiclePhotoImageId : cartingAgentVehiclePhotoImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.VEHICLE_PHOTO[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}

function editCartingAgentDocumentDetailsForm(index,from){
	var html = ""
		+ "<div class='modal fade in' id='modal-responsive' >"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Update Carting Agent Upload Document Details</strong></h4>"
        + "            </div>"
        + "												<form id='editCartingAgentDocumentForm' action='"+contextApplicationPath+"/CartingAgentController/updateCartingAgentDocumentInfo' method='post'>"
		+ "                                                <div class='panel-body'>"
		+ "                                                    <div class='row'>"
		+ "                                                        <div class='col-md-4'>"
		+ "															   <input type='hidden' name='cartingAgentDocId' value='"+commonData[index].DOC_ID+"'>"
		+ "                                                            <div class='form-group'>"
		+ "                                                                <label class='form-label'><strong>Organization Name</strong>"
		+ "                                                                </label>"
		+ "                                                                <span class='tips'></span>"
		+ "                                                                <div class='controls'>"
		+ "                                                                    <input disabled type='text' value='"+commonData[index].ORG_NAME+"' class='form-control'>"
		+ "                                                                </div>"
		+ "                                                            </div>"
        +"                                                            <div class='form-group'>"
        +"                                                     		    <button type='button' onclick='editCartingAgentDocImagesListPopup("+commonData[index].DOC_ID+","+index+")' class='btn btn-success'>Edit Catring Agent Document Images</button>"
	    +"                                                            </div>"
		+ "                                                        </div>"
		+ "                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Carting Agent*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='cartingAgent' id='selectCartingAgent' class='form-control' class='form-control'>"
		+"                                                                        <option disabled selected> Select Carting Agent</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+ "                                                            <div class='form-group'>"
		+ "                                                                <label class='form-label'><strong>Document Type*</strong>"
		+ "                                                                </label>"
		+ "                                                                <span class='tips'></span>"
		+ "                                                                <div class='controls'>"
		+ "																		<div id='documentList'></div>"
        + "                                                                </div>"
		+ "                                                            </div>"
		+ "                                                        </div>"
		+ "                                                    </div>"
		+ "                                                    <div class='row'>"
		+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                                                <div class='pull-right'>"
		+ "                                                                    <button class='btn btn-primary m-b-10' >Submit</button>"//onclick='showLoader()'
		+ "                                                                    <button type='reset' onclick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
		+ "                                                                </div>"
		+ "                                                            </div>"
		+ "                                                        </div>"
		+ "                                                    </div>"
		+ "                                                </div>"
		+ "												</form>"
		+ "                            </div>"
		+ "                        </div>" 
		+ "                    </div>";
	$('#popup').html(html);
	loadDocumentTypeListForEdit(commonData[index].DOC_TYPE_ID);
	displyaCateringAgentDropDownForEdit(commonData[index].ORG_ID, commonData[index].AGENT_ID);
	ajaxEditCartingAgentDocumentForm("editCartingAgentDocumentForm",index,from);
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block"); 
}

function ajaxEditCartingAgentDocumentForm(FormId,index,from) {
	var options = {
		success : function(data) {
				alert(data.MSG);
				if(data.RESULT==true){
					commonData[index].DOC_TYPE_ID = data.DOC_TYPE_ID;
					commonData[index].DOC_TYPE = data.DOC_TYPE;
					commonData[index].AGENT_NAME = data.AGENT_NAME;
					commonData[index].AGENT_ID = data.AGENT_ID;
					cartingAgentUploadedDocListingView(commonData,from,urlForEdit);
				}
				hidePopUp();
				$('#loader').hide();
		}
	};
	$('#' + FormId).ajaxForm(options);
}

function editCartingAgentDocImagesListPopup(cartingAgentDocID, index){
	//commonData2 is global var
	commonData2 = index;
	editListOfImagesPopup(cartingAgentDocID,"documentImage",commonData[index].DOCUMENT,"CARTING_AGENT_UPLOAD_DOCUMENT_IMAGE" , "/CartingAgentController/updateCartingAgentUploadDocumentImage");
}
//Image updation Ajax call after getting result from server
function ajaxCartingAgentDocumentImageUpdate(data){
	if(data.RESULT==true) {
		commonData[commonData2].DOCUMENT = data.DOCUMENT;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}

function deleteCartingAgentDocumentImage(cartingAgentDocumentPathId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CartingAgentController/deleteCartingAgentDocumentFileImage', {
			cartingAgentDocumentPathId : cartingAgentDocumentPathId
		}, function(data) {
			if(data.RESULT==true){
				commonData[commonData2].DOCUMENT[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}