function AddInwardCourier() {
	var html = ""
			+ "<div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                    <h3><strong>Inward Courier Registration</strong></h3>"
			+ "                </div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-6'>"
			+ "                        <div class='tabcordion'>"
			+ "                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
			+ "                                <li class='active'><a href='#tab1_1' data-toggle='tab' onclick='inwardCourierRegistration()'>Inward Courier Registration</a></li>"
			+ "                                <li class=''><a href='#tab1_2' data-toggle='tab' onclick='inwardCourierRegistrationListing()'>Inward Courier Registration View</a></li>"
			+ "                            </ul>"
			+ "                            <div id='myTabContent' class='tab-content'>"
			+ "                            </div>"
			+ "                        </div>" + "                    </div>"
			+ "                </div>";
	$('#main-content').html(html);
	$('#subMenu' + lastClickId).hide();
	inwardCourierRegistration();
}
var selectOption;
function onchangeCourierRadioButton(option) {
	selectOption = option;
	var html = "";
	if(option == 1) {
		html += ""
			+ "   <label class='form-label'><strong>Select Company</strong>"
			+ "   </label>"
			+ "   <span class='tips'></span>"
			+ "   <div class='controls'>"
			+ "     <select id='companyName' name='companyName' onchange='onChangeCompany(false)' class='form-control' class='form-control' required >"
			+ "       <option disabled selected> Select Company</option>"
			+ "     </select>"
			+ "   </div>"
			+ "   <div id='companyError_msg' class='validationError' style='color:red'></div>";
		$('#companyDropDown').html(html);
		$('#selectStockist').empty();
		$("#stateForOther").html('');
		$("#companyError_msg").html('');$("#stockistError_msg").html('');$("#otherTxtError_msg").html('');$("#stateTxtError_msg").html('');$("#districtTxtError_msg").html('');$("#cityTxtError_msg").html('');
		getCompanies();
		perticularsDropDownList();
	}
	else if(option == 2){
			html +="                                           <label class='form-label'><strong>Select Company</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <select id='companyName' name='companyName' onchange='onChangeCompany(true)' class='form-control' required>"
			+ "                                                        <option disabled selected>Select Company</option>"
			+ "                                                    </select>"
			+ "                                                </div>"
			+ "   <div id='companyError_msg' class='validationError' style='color:red'></div>";
			$('#companyDropDown').html(html);
			getCompanies();
			html="";
		
		html += "<label class='form-label'><strong>List of Company Stockist</strong>"
				+ "</label>"
				+ "<span class='tips'></span>"
				+ "<div class='controls'>"
				+ " <select id='StockistName' name='stockistId' onchange='onStockistChange()' class='form-control' class='form-control' required>"
				+ "  <option>Select Stockist</option>"
				+ " </select>"
				+ "</div>"
				+ "<div id='stockistError_msg' class='validationError' style='color:red'></div>";
		$('#selectStockist').html(html);
		 $('#selectParticular').prop('selectedIndex',0);
		 $("#stateForOther").html('');
		 $("#companyError_msg").html('');$("#stockistError_msg").html('');$("#otherTxtError_msg").html('');$("#stateTxtError_msg").html('');$("#districtTxtError_msg").html('');$("#cityTxtError_msg").html('');
		getStockist();
		perticularsDropDownList();
	}
	else if(option == 3){
		html +=""
			+"<label class='form-label'><strong>Other</strong>"
			+ "</label>"
			+ "<span class='tips'></span>"
			+ "<div class='controls'>"
			+ " <input type='text' id='otherTxtId' name='other' placeholder='Other' class='form-control' required>"
			+ "</div>"
			+ "<div id='otherTxtError_msg' class='validationError' style='color:red'></div>";
		$('#companyDropDown').html(html);
		$('#selectStockist').empty();
		html=""
			+ "                                            <div class='form-group'>"
			+ "                                               <label class='form-label'><strong>State</strong>"
			+ "                                               </label>"
			+ "                                               <span class='tips'></span>"
			+ "                                               <div class='controls'>"
			+ "                                               	<select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control'>"
			+ "														<option disabled selected>Select State</option>"
			+ "													</select>"
			+ "                                                </div>"
			+ "												   <div id='stateTxtError_msg' class='validationError' style='color:red'></div>"
			+ "                                             </div>"
			+ "                                            <div class='form-group' style='height: 70px;'>"
			+ "                                            	<label class='form-label'><strong>District</strong>"
			+ "                                              </label>"
			+ "                                              <span class='tips'></span>"
			+ "                                              <div class='controls'>"
			+ "                                              	<select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control'>"
			+ "                                                  	<option disabled selected>Select District</option>"
			+ "                                                  </select>"
			+ "                                              </div>"
			+ "												 <div id='districtTxtError_msg' class='validationError' style='color:red'></div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>City</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <select name='city' id='cityName' class='form-control'>"
			+ "                                                        <option disabled selected>Select City</option>"
			+ "                                                    </select>"
			+ "                                                </div>"
			+ "												 <div id='cityTxtError_msg' class='validationError' style='color:red'></div>"
			+ "                                            </div>";
		$("#stateForOther").html(html);
		$("#companyError_msg").html('');$("#stockistError_msg").html('');$("#otherTxtError_msg").html('');$("#stateTxtError_msg").html('');$("#districtTxtError_msg").html('');$("#cityTxtError_msg").html('');
		stateDropDownList();
		particularsDropDownListForOtherRadio();
	}
}
//particulars dropdown [Cheque(id==3) option will be disabled for other radio button]
function particularsDropDownListForOtherRadio() {
	$.post(contextApplicationPath+ '/InwardCourierController/getParticularsDropdownList',
					function(data) {
						var optionHtml = "<option disabled selected> Select Particulars</option>";
						for (var i = 0; i < data.particularsArr.length; i++) {
							if(data.particularsArr[i].PARTICULAR_ID!=3)
								optionHtml += "<option value='"+ data.particularsArr[i].PARTICULAR_ID+ "'>"+ data.particularsArr[i].PARTICULAR_NAME+ "</option>";
						}
						$("#selectParticular").empty();
						$("#selectParticular").append(optionHtml);
					}, 'json');
}
function inwardCourierRegistration() {
	var html = ""
			+"<form id='courierReg' action='"+contextApplicationPath+"/InwardCourierController/addInwardCourierRegistration' method='post' enctype='multipart/form-data'>"
			+ "<div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                    <h3><strong>Inward Courier Registration</strong></h3>"
			+ "                </div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-12'>"
			+ "                            <div class='panel panel-default'>"
			+ "                                <div class='panel-body'>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-4'>"
			+ "                                            <div id='selectOrganization' class='form-group' required></div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Date</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input name='courierRegDate' id='date' class='dateClassCommon form-control' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' onchange=\"errorMsgEmpty('date','dateError_msg')\">"
			+ "                                                </div>"
			+"												  <div id='dateError_msg' class='validationError' style='color:red'></div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='col-lg-12 control-label' style='padding-left: 0px;'><strong></strong></label>"
			+ "                                                <div class='col-lg-12'>"
			+ "                                                    <div class='pos-rel'>"
			+ "                                                        <input tabindex='13' id='ctest' type='radio' checked='checked' onchange='onchangeCourierRadioButton(1)' name='companyStockistOption' value='Company' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;'>"
			+ "                                                        <label class='p-l-40' for='flat-checkbox-1'>Company</label>"
			+ "                                                    </div>"
			+ "                                                    <div class='pos-rel'>"
			+ "                                                        <input tabindex='14' id='ctest' type='radio' onchange='onchangeCourierRadioButton(2)' name='companyStockistOption' value='Stockist' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;' >"
			+ "                                                        <label class='p-l-40' for='flat-checkbox-2'>Stockist</label>"
			+ "                                                    </div>"
			+ "                                                    <div class='pos-rel'>"
			+ "                                                        <input tabindex='14' id='ctest' type='radio' onchange='onchangeCourierRadioButton(3)' name='companyStockistOption' value='Other' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;' >"
			+ "                                                        <label class='p-l-40' for='flat-checkbox-2'>Other</label>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div id='companyDropDown' class='form-group'>"
			+ "                                                <label class='form-label'><strong>Select Company</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <select id='companyName' name='companyName' onchange='onChangeCompany(false)' class='form-control'  required>"
			+ "                                                        <option disabled selected> Select Company</option>"
			+ "                                                    </select>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div id='selectStockist' class='form-group'></div>"
			+ "											   <div id='stateForOther'></div>"
			+ "                                        </div>"
			+ "                                        <div class='col-md-4'>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Courier Name</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='text' name='courierName' placeholder='Courier Name' class='form-control' required>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Delivery Person</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='text' name='deliveryPerson' placeholder='Delivery Person' class='form-control' required>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Time</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input name='time' id='timePicker' type='text' placeholder='Time' class='form-control' focusout=\"errorMsgEmpty('timePicker','timeError_msg')\">"
			+"                                                <div id='timeError_msg' class='validationError' style='color:red'></div>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Docket No</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='text' name='docketNo' placeholder='Docket No' class='form-control' required>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Docket File</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='file' name='docketFile' placeholder='Remark' class='form-control'>"
			+ "                                                </div>"
			+ "                                            </div>"
			+"                                             <div id='docketFileDivId'></div>"
			+"                                             <div class='form-group'>"
			+"                                                <div id='docketFileAddMoreButtonId'><button onclick=\"addFileConrol('docketFileDivId','docketFileAddMoreButtonId','docketFile',1)\" class='btn btn-primary'>Add More Files</button></div>"
			+"                                             </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Remark</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <input type='text' name='remark' placeholder='Remark' class='form-control'>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                            <div class='form-group'>"
			+ "                                                <label class='form-label'><strong>Particulars</strong>"
			+ "                                                </label>"
			+ "                                                <span class='tips'></span>"
			+ "                                                <div class='controls'>"
			+ "                                                    <select name='particular' id='selectParticular' onchange='showDiv()' class='form-control' class='form-control' required>"
			+ "                                                        <option disabled selected>Select Particulars</option>"
			+ "                                                    </select>"
			+ "                                                </div>"
			+ "                                            </div>"
			//pop up
			+ "                                            <div id='showPopUp' class='form-group'></div>"
			// /pop up
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                <div class='pull-right'>"
			+ "                                                    <button type='submit' class='btn btn-primary m-b-10' onclick='validateInwardCourierForm(\"#courierReg\")'>Save</button>"
			/*+ "                                                    <button class='btn btn-success m-b-10' onclick='validateFormDetails(\"#courierReg\")'>Save & Mail</button>"*/
			+ "                                                    <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                </div>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>"
			+ "                </div>" 
			+ "</form>"
			+ "				  <div id='Listing'>"
			+ "				  </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	$(".dateClassCommon").datepicker({changeMonth : true,changeYear : true,yearRange : "-100:+0"});
	$('#timePicker').timepicki();
	displayOrganizationDropDown(1);
	perticularsDropDownList();
	ajaxAddCourierRegistration('courierReg');
}

//ajax call
function ajaxAddCourierRegistration(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.result==true){
					alert("Inward Courier Details Added Successfully");
					inwardCourierRegistration();
				}
				else{
					alert("Add Inward Courier Details Failed");
				}
			}
	};
	$('#'+formId).ajaxForm(options);
}
function validateInwardCourierForm(formId){
	var z = $(formId).parsley('validate');
	var flag =  0;
	if($("#timePicker").val()==""||$("#timePicker").val()==undefined||$("#date").val()==""||$("#date").val()==undefined){
		var flag=false;
		if($("#timePicker").val()==""||$("#timePicker").val()==undefined){
			$("#timeError_msg").html("This value is required.");flag=true;
		}
		if($("#date").val()==""||$("#date").val()==undefined){
			$("#dateError_msg").html("This value is required.");flag=true;
		}
		if(flag){
			$('#loader').hide();
			return false;
		}
		return true;
	}
	if(z==true)
	{
			showLoader();
	}else{
		if(selectOption==1){
			if($("#companyName").val()=="Select Company"||$("#companyName").val()==""||$("#companyName").val()==undefined){
				$("#companyError_msg").html("This value is required.");
				$('#loader').hide();
				return false;
			}
		}else if(selectOption==2){
			if($("#companyName").val()=="Select Company"||$("#companyName").val()==""||$("#companyName").val()==undefined){
				$("#companyError_msg").html("This value is required.");
				$('#loader').hide();
			}
			if($("#StockistName").val()=="Select Stockist"||$("#StockistName").val()==""||$("#StockistName").val()==undefined){
				$("#stockistError_msg").html("This value is required.");
				$('#loader').hide();
			}
			return false;
		}else if(selectOption==3){
			if($("#otherTxtId").val()==""||$("#otherTxtId").val()==undefined){
				$("#otherTxtError_msg").html("This value is required.");
				$('#loader').hide();
			}
			if($("#stateName").val()=="Select State"||$("#stateName").val()==""||$("#stateName").val()==undefined){
				$("#stateTxtError_msg").html("This value is required.");
				$('#loader').hide();
			}
			if($("#districtName").val()=="Select District"||$("#districtName").val()==""||$("#districtName").val()==undefined){
				$("#districtTxtError_msg").html("This value is required.");
				$('#loader').hide();
			}
			if($("#cityName").val()=="Select City"||$("#cityName").val()==""||$("#cityName").val()==undefined){
				$("#cityTxtError_msg").html("This value is required.");
				$('#loader').hide();
			}
			return false;
		}
	}
	
}

function onChangeCompany(stat){
	 $('#selectParticular').prop('selectedIndex',0);
	 hideDiv(2);
	 if(stat)
		 getStockist();
}

//This function is used at many places so dont change it (In Outward_Courier registration and In Inward Corier Registration)
function onStockistChange(){
	 $('#selectParticular').prop('selectedIndex',0);
	 hideDiv(2);
}
//This function is used at many places so dont change it (In Outward_Courier registration and In Inward Corier Registration)
function showPopUpForCourierParticulars(){
	var html = ""
		+ "<div class='modal fade' id='modal-responsive250' aria-hidden='true'>"
		+ "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hideDiv(1)' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>Advanced Cheque Info</strong></h4>"
		+ "            </div>"
		+ "            <div class='modal-body'>"
		+ "                <div class='row'>"
		+ "                    <div class='col-md-4'>"
		+ "                        <div class='form-group' style='height: 58px;'>"
		+ "                            <label class='form-label'><strong>Bank</strong>"
		+ "                            </label>"
		+ "                            <span class='tips'></span>"
		+ "                            <select id='selectCompanyBank' name='companyBank' class='form-control' class='form-control' required>"
		+ "                                <option disabled selected>Select Bank</option>"
		+ "                            </select>"
		+ "                        </div>"
		+ "                    </div>"
		+ "                    <div class='col-md-4'>"
		+ "                        <div class='form-group'>"
		+ "                            <label class='form-label'><strong>Date</strong>"
		+ "                            </label>"
		+ "                            <span class='tips'></span>"
		+ "                            <div class='controls'>"
		+ "                                <input id='recDateId' name='receivedDate' class='dateClassCommon form-control' type='text' placeholder='Select Date' style='cursor:auto; height: 36px; padding-left: 10px;' required>"
		+ "                            </div>"
		+ "                        </div>"
		+ "                    </div>"
		+ "                    <div class='col-md-4'>"
		+ "                        <div class='form-group'>"
		+ "                            <label class='form-label'><strong>Cheque No</strong>"
		+ "                            </label>"
		+ "                            <span class='tips'></span>"
		+ "                            <div class='controls'>"
		+ "                                <input id='chequeNoId' name='chequeNo' type='text' class='form-control' required>"
		+ "                            </div>"
		+ "                        </div>"
		+ "	<div id='chequeDiv'>"
		+ "</diV>"
		+ "            <div class='modal-footer text-center'>"
		+ "                <button type='button' onclick='addChequeNoFields()' class='btn btn-primary'>Add More</button>"
		+ "            </div>"
		+ "</div>"
		+ "                </div>"
		+ "            </div>"
		+ "            <div class='modal-footer text-center'>"
		+ "                <button type='button' onClick='validateInwardCourierChequeInfo(3)'class='btn btn-primary' data-dismiss='modal'>Submit</button>"
		+ "                <button type='button' onClick='hideDiv(1)' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
		+ "            </div>"
		+ "        </div>"
		+ "    </div>"
		+ "</div>";
		$('#showPopUp').empty();
		$('#showPopUp').html(html);
		$(".dateClassCommon").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
}

function validateInwardCourierChequeInfo(index){
	if($("#selectCompanyBank").val()==""||$("#selectCompanyBank").val()==undefined||$("#selectCompanyBank").val()==null|| $("#recDateId").val()==""||$("#chequeNoId").val()==""){
		alert("Please fill all the fields.");
	}else{
		hideDiv(index);
	}
}
function organizationCompanyBankDropDownList(){
	var orgId = $('#OrgName').val();
	var companyId = $('#companyName').val();
	var stockistId = $('#StockistName').val();
	if(orgId==null||companyId==null){
		alert("Please Select Both Oraganization and Company...!");
		return false;
	}else{
		if(stockistId!=null){
			//alert("stockistId="+stockistId);
			$.post(contextApplicationPath+ '/StockistBankDetailsController/stockistBankDropdownList',{
				orgId : orgId,
				companyId : companyId,
				stockistId : stockistId
			},function(data) {
						var optionHtml = "<option disabled selected> Select Bank</option>";
						for (var i = 0; i < data.bankArray.length; i++) {
							optionHtml += "<option value='"+ data.bankArray[i].BANK_ID+ "'>"
									+ data.bankArray[i].BANK_NAME
									+ "</option>";
						}
						$("#selectCompanyBank").empty();
						$("#selectCompanyBank").append(optionHtml);
					}, 'json');
			return true;
		}
		else{
			//alert("CompanyId = "+companyId);
			$.post(contextApplicationPath+ '/CompanyController/companyBankDropdown',{
				orgId : orgId,
				companyId : companyId
			},function(data) {
						var optionHtml = "<option disabled selected> Select Bank</option>";
						for (var i = 0; i < data.companyBankArray.length; i++) {
							optionHtml += "<option value='"+ data.companyBankArray[i].COMPANY_BANK_ID+ "'>"
									+ data.companyBankArray[i].BANK_NAME
									+ "</option>";
						}
						$("#selectCompanyBank").empty();
						$("#selectCompanyBank").append(optionHtml);
					}, 'json');
			return true;
		}
		
	}
	
}
//This function is used at many places so dont change it (In Outward_Courier registration and In Inward Corier Registration)
var chequeCounter = 1;
function addChequeNoFields(){
	var newdiv = document.createElement('div');
	newdiv.innerHTML ="<div id='chequeNo"+chequeCounter+"' class='form-group'><div class='controls'><input id='chequeNoId' name='chequeNo' type='text' class='form-control'><a href='#' class='delete btn btn-danger' style=' margin-left: 8px; width: 55px;height: 36px;' onClick='removeChequeNo("+chequeCounter+")'>X</a></div></div>";
	document.getElementById('chequeDiv').appendChild(newdiv);
	$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+20" });
	chequeCounter++;
}
//This function is used at many places so dont change it (In Outward_Courier registration and In Inward Corier Registration)
function removeChequeNo(fieldId){
	$("#chequeNo"+fieldId).remove();
}

function showDiv() {
	var selectId = $('#selectParticular').val();
	 showPopUpForCourierParticulars();
	 if(selectId==3){
		 var flag = organizationCompanyBankDropDownList();
		 if(flag){
			 $("#modal-responsive250").addClass("in");
			 $("#modal-responsive250").attr("aria-hidden","false");
			 $("#modal-responsive250").css("display","block");
		 }else{
			 $('#selectParticular').prop('selectedIndex',0);
			 hideDiv(2);
		 }
	 }else{
		 hideDiv(2);
	 }
	 
}
//This function is used at many places so dont change it (In Outward_Courier registration and In Inward Corier Registration)
function hideDiv(id) {
	if(id==1){
		$('#showPopUp').empty();
		 $('#selectParticular').prop('selectedIndex',0);
	}
	else if(id==2) {
		 $('#showPopUp').empty();
	 }else{
		 $("#modal-responsive250").removeClass("in");
		 $("#modal-responsive250").attr("aria-hidden","true");
		 $("#modal-responsive250").css("display","none");
	 }
}
function showStockistDropdownList(option) {
	
	var html = "";
	if (option == 2) {
		html += "<label class='form-label'><strong>List of Company Stockist</strong>"
				+ "</label>"
				+ "<span class='tips'></span>"
				+ "<div class='controls'>"
				+ " <select name='stockistId' id='StockistName' onchange='onStockistChange()' class='form-control' class='form-control' required>"
				+ "  <option disabled selected>Select Stockist</option>"
				+ " </select>"
				+ "</div>";
	}
	$('#selectStockist').empty();
	$('#selectStockist').html(html);
	 $('#selectParticular').prop('selectedIndex',0);
	getStockist();
}
// particulars dropdown
function perticularsDropDownList() {
	$.post(contextApplicationPath+ '/InwardCourierController/getParticularsDropdownList',
					function(data) {
						var optionHtml = "<option disabled selected> Select Particulars</option>";
						for (var i = 0; i < data.particularsArr.length; i++) {
							optionHtml += "<option value='"+ data.particularsArr[i].PARTICULAR_ID+ "'>"
									+ data.particularsArr[i].PARTICULAR_NAME
									+ "</option>";
						}
						$("#selectParticular").empty();
						$("#selectParticular").append(optionHtml);
					}, 'json');
}

//function to show the inwardDetails 
function inwardCourierRegistrationListing() {
	
	$('#loader').show();
//	$.post(contextApplicationPath+'/InwardCourierController/inwardCourierRegistrationListing',function(data){
		var html=""
			+ "                <div class='row'>"
			+ "                    <div class='col-md-12'>"
			+ "                        <div class='panel panel-default'>"
			+ "                            <div class='panel-body'>"
			+ "                                <div class='row'>"
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+ "                                            <label class='form-label'><strong>Search By</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls'>"
			+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFuncCourier();'>"
			+ "                                                       <option value='Stockist'>Stockist</option>"
			+"														  <option value='Organization'>Select Organization</option>"
			+ "                                                       <option value='Company'>Comapny Name</option>"
			+ "                                                       <option value='Date'>Search By Date</option>"
		//	+ "                                                       <option value='Stockist'>Stockist</option>"
			+ "                                                       <option value='CourierName'>Courier Name</option>"
			+ "                                                       <option value='DeliveryPerson' >Delivery Person</option>"
			+"														  <option value='DocketNo' >Docket Number</option>"
			+ "                                                       <option value='State'>State Name</option>"
			+ "                                                       <option value='DistrictName'>District Name</option>"
			+ "                                                       <option value='Cityname'>City Name</option>"
			+ "                                                     </select>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"//fromDate toDate searchOption searchText
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group' id='searchForDiv'>"
			+ "                                            <label class='form-label'><strong>Search For</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls' id='searchControl'>"
			+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-success m-b-10' onclick='searchInwardCourierRegistrationDetails()'>Show</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                    <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                    </div>"
			+ "                                </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>"
			+ "                </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html);
		commonInwardCourierRegistrationController("", "", "", "", 1);
//		showinwardCourierRegistrationListing(data);// to call the function which shows inwardDetails
		$('#loader').hide();
//	}, 'json');
}

function commonInwardCourierRegistrationController(searchOption, searchText, fromSpecialData, toSpecialData, from)
{
	$.post(contextApplicationPath+'/InwardCourierController/searchInwardCourierRegistrationDetails',
			{
		searchOption : searchOption,
		searchText : searchText,
		fromSpecialData :fromSpecialData,
		toSpecialData : toSpecialData,
		from : from
		}, function(data) {
			var url = "commonInwardCourierRegistrationController(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showinwardCourierRegistrationListing(data, from, url); // to call the function which shows the searched item
//		$('#searchResult').html(html);
	}, 'json');
}
//function to Searching for the  inwardDetails 
function changeFuncCourier() {
	var selectedValue=$('#searchOption').val();
	var html="";
		if(selectedValue=="Date")
    	{
    		html+="<input id='inputFromDate' type='text' class='dateClassCommon form-control' placeholder='From Date' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTODate' type='text' class='dateClassCommon form-control' placeholder='To Date' style='width: 49%;'>";
    	}
    	else
    	{
    		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control'>";
    	} 	
/*		$("#searchControl").empty();
*/		$("#searchControl").html(html);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
 }

function searchInwardCourierRegistrationDetails(){
	var searchOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
   if (searchOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
   commonInwardCourierRegistrationController(searchOption, searchText, fromSpecialData, toSpecialData, 1);
//	$.post(contextApplicationPath+'/InwardCourierController/searchInwardCourierRegistrationDetails',
//		{selectedValue : selectedOption,searchText : searchText,fromSpecialData :fromSpecialData,toSpecialData : toSpecialData	
//	}, function(data) {
//		showinwardCourierRegistrationListing(data) // to call the function which shows the searched item
////	$('#searchResult').html(html);
//}, 'json');
}

//common funcationality to show inwarddetails && for Searching 
function showinwardCourierRegistrationListing(data, from, url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html = ""
		+ "                                        <table class='table table-striped table-hover'>"
		+ "                                            <thead class='no-bd'>"
		+ "                                                <tr>"
		+ "                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
		+ "                                                    </th>"
		+ "                                                    <th style='text-align: center;'><strong>Organization</strong>"
		+ "                                                    </th>"
		+ "                                                    <th style='text-align: center;'><strong>Company</strong>"
		+ "                                                    </th>"
		+ "                                                    <th style='text-align: center;'><strong>Stockist</strong>"
		+ "                                                    </th>"
		+ "                                                    <th style='text-align: center;'><strong>Courier Name</strong>"
		+ "                                                    </th>"
		+ "                                                    <th style='text-align: center;'><strong>City</strong>"
		+ "                                                    </th>"
		+ "                                                    <th style='text-align: center;'><strong>Date</strong>"
		+ "                                                    </th>"
		+ "                                                    <th style='text-align: center;'><strong>Docket No</strong>"
		+ "                                                    </th>"
		+ "                                                    <th style='text-align: center;'><strong>Action</strong>"
		+ "                                                </tr>"
		+ "                                            </thead>"
		+ "                                            <tbody class='no-bd-y'>";
														for (var counter = 0; counter < data.inwardCourierRegArr.length; counter++) {
															html+="<tr style='text-align: center;'>"
																+" <td>"+SR_NO+"</td>"
																+" <td id='ORG_NAME"+counter+"'>"+data.inwardCourierRegArr[counter].ORG_NAME+"</td>"
																+" <td id='COMPANY_"+counter+"'>"+data.inwardCourierRegArr[counter].COMPANY+"</td>"
																+" <td id='STOCKIST_NAME_"+counter+"'>"+data.inwardCourierRegArr[counter].STOCKIST_NAME+"</td>"
																+" <td id='COURIER_NAME_"+counter+"'>"+data.inwardCourierRegArr[counter].COURIER_NAME+"</td>"
																+" <td id='CITY_"+counter+"'>"+data.inwardCourierRegArr[counter].CITY+"</td>"
																+" <td id='REG_DATE_"+counter+"'>"+data.inwardCourierRegArr[counter].REG_DATE+"</td>"
																+" <td id='DOCKET_NO_"+counter+"'>"+data.inwardCourierRegArr[counter].DOCKET_NO+"</td>"
																+" <td><a class='edit btn btn-blue' onClick=\"viewInwardCourierReg("+data.inwardCourierRegArr[counter].COURIER_ID+")\"  href='#'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' onClick='editInwardCourierRegDetails("+counter+","+data.inwardCourierRegArr[counter].COURIER_ID+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteInwardCourierReg("+data.inwardCourierRegArr[counter].COURIER_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
																+"</tr>";
															SR_NO++;
														}
    html+= "                                            </tbody>"
		+ "                                        </table>";
    $('#searchResult').html(html);
   paginationView(from,data.paginationCount,url);//from, totalPages, url
    //paginationView(from,data[data.length-1].paginationCount,url);
	$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
}
//view Inward Courier Reg
function viewInwardCourierReg(inwardCourierRegId){
    inwardCourierGoodsPopUp(inwardCourierRegId);
}
//Edit Inward Courier Reg
function editInwardCourierReg(inwardCourierRegId){
	alert("edit = "+inwardCourierRegId);
}
//Delete Inward Courier Reg
function deleteInwardCourierReg(inwardCourierRegId){
	//alert("Delete = "+inwardCourierRegId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardCourierController/deleteInwardCourierReg', {
			inwardCourierRegId : inwardCourierRegId
		}, function(data) {
			inwardCourierRegistrationListing();
			alert(data.MSG);
		}, 'json');
    }
}

