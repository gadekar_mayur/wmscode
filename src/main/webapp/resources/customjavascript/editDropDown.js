//This function used at many places so dont change its code...Thank you
function displayEditOrganizationDropDown(index,orgId){
	$.post(contextApplicationPath+'/StockistController/organizationListDropdown',function(data){
		var html ="";
		if(data.orgArray.length>1){
			html+="<div class='form-group'><label class='form-label'><strong>Select Organization</strong>"
				+" </label>"
				+" <span class='tips'></span>"
				html+=" <div class='controls'>";
			if(index==0)
				html+="<select id='orgId' name='orgName' class='form-control' class='form-control' required>";//onchange function is in inward.js
			else if(index==1)
				html+="<select id='orgId' name='orgName' onchange='InwardGoodsRegistrationOrganizationChange()' class='form-control' class='form-control' required>";//onchange function is in inward.js
			else if(index==2)
				html+="<select id='OrgName' name='orgName' onchange='getCredential()' class='form-control' class='form-control'>";//onchange function is in inwardReturnGoods.js
			
			html+="<option disabled selected> Select Organization</option>";
					for (var i = 0; i < data.orgArray.length; i++) {
						if(orgId==data.orgArray[i].orgId)
							html += "<option selected='selected' value='" + data.orgArray[i].orgId + "'>"+ data.orgArray[i].orgName + "</option>";
						else
							html += "<option value='" + data.orgArray[i].orgId + "'>"+ data.orgArray[i].orgName + "</option>";
					}
			html+="</select>"
				+"</div></div>";	
		}else if(data.orgArray.length>0){
			html+="<div class='form-group'><label class='form-label'><strong>Your Organization</strong>"
				+"</label>"
				+"<span class='tips'></span>"
				+"<div class='controls'>"
				+"<label class='form-label'><strong></strong>"+data.orgArray[0].orgName+"</label>";
			if(index==1)
				html+="<input type='hidden' value='"+data.orgArray[0].orgId+"' name='orgName' id='orgId' required>";
			else if(index==2)
				html+="<input type='hidden' value='"+data.orgArray[0].orgId+"' name='orgName' id='OrgName' required>";
			html+="</div></div>";	
		}
		$('#selectOrganization').html(html);
		return 0;
	}, 'json');
	
}
//To get company list according to organization
//This function used at many places so dont change its code...Thank you
function getCompaniesDropdownForEdit(orgId,companyId){
	//var orgId = $('#OrgName').val();
	$.post(contextApplicationPath+'/CompanyController/getCompanies', {
		orgId : orgId
	}, function(data) {
		//$('#loader').hide();
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Company</option>";
		for (counter = 0; counter < data.companyArray.length; counter++) {
			if(companyId==data.companyArray[counter].compId)
				optionHtml += "<option selected='selected' value='" + data.companyArray[counter].compId + "'>"+ data.companyArray[counter].compName + "</option>";
			else
				optionHtml += "<option value='" + data.companyArray[counter].compId + "'>"+ data.companyArray[counter].compName + "</option>";
		}
		$("#companyName").empty();
		$("#companyName").append(optionHtml);
	}, 'json');
}

//get company depo
function getCompanyDepoDropdownForEdit(companyId,depoId){
	//alert("getStockist");
	//var companyId = $('#companyName').val();
	if(companyId==null||companyId==undefined)
		companyId=0;
	$.post(contextApplicationPath+'/CompanyController/companyDepoDropdown', {
		companyId : companyId
	}, function(data) {
		//$('#loader').hide();
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Depo</option>";
		for (counter = 0; counter < data.companyDepoArr.length; counter++) {
			if(depoId==data.companyDepoArr[counter].DEPO_ID)
				optionHtml += "<option selected='selected' value='" + data.companyDepoArr[counter].DEPO_ID + "'>"+ data.companyDepoArr[counter].DEPO_NAME + "</option>";
			else
				optionHtml += "<option value='" + data.companyDepoArr[counter].DEPO_ID + "'>"+ data.companyDepoArr[counter].DEPO_NAME + "</option>";
		}
		$("#companyDepo").empty();
		$("#companyDepo").append(optionHtml);
	}, 'json');
}
//To get Transporter drop down values list
function getTransporterDropdownForEdit(orgId,transporterId){
	//var orgId = $('#OrgName').val();
	$.post(contextApplicationPath+'/TransporterDetailsController/getTransporterList', {
		orgId : orgId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Transporter</option>";
		for (counter = 0; counter < data.transporterArray.length; counter++) {
			if(transporterId==data.transporterArray[counter].transId)
				optionHtml += "<option selected='selected' value='" + data.transporterArray[counter].transId + "'>"+ data.transporterArray[counter].transName + "</option>";
			else
				optionHtml += "<option value='" + data.transporterArray[counter].transId + "'>"+ data.transporterArray[counter].transName + "</option>";
		}
		$("#transName").empty();
		$("#transName").append(optionHtml);
	}, 'json');
}
function loadEmployeeDropDownForEdit(organizationId,empId)
{
var html="<select class='form-control' class='form-control' name='employeeDetailsId' required>"
	+"<option disabled selected>Select Employee</option>";
		$.post(contextApplicationPath+'/EmployeeController/loadEmployeeList',{organizationId : organizationId} ,function(data) {
			$(data).each(function(index, element) {
				if(empId==element.EMP_ID)
					html+="<option selected='selected' value='"+element.EMP_ID+"'>"+element.EMP_NAME+"</option>";
				else
					html+="<option value='"+element.EMP_ID+"'>"+element.EMP_NAME+"</option>";
			}); 
			html+="</select>";
			$('#employeeDropDown').html(html);
		}, 'json');

}
function loadEmployeeDropDownForEditCheckingDone(organizationId,empId)
{
var html="<select class='form-control' class='form-control' name='employeeDetailsId'>"
	+"<option disabled selected>Select Employee</option>";
		$.post(contextApplicationPath+'/EmployeeController/loadEmployeeList',{organizationId : organizationId} ,function(data) {
			$(data).each(function(index, element) {
				if(empId==element.EMP_ID)
					html+="<option selected='selected' value='"+element.EMP_ID+"'>"+element.EMP_NAME+"</option>";
				else
					html+="<option value='"+element.EMP_ID+"'>"+element.EMP_NAME+"</option>";
			}); 
			html+="</select>";
			$('#employeeDropDownForCheckingDone').html(html);
		}, 'json');

}
//
function getStockistForEdit(orgId,compId,stockistId){
	
	if(compId==null||compId==undefined)
		compId=0;
	if(orgId==null||orgId==undefined)
		orgId=0;
	$.post(contextApplicationPath+'/StockistController/getStockist', {
		orgId : orgId,
		compId : compId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Stockist</option>";
		for (counter = 0; counter < data.stockistArray.length; counter++) {
			if(stockistId==data.stockistArray[counter].stockistId)
				optionHtml += "<option selected='selected' value='" + data.stockistArray[counter].stockistId + "'>"+ data.stockistArray[counter].stockistName + "</option>";
			else
				optionHtml += "<option value='" + data.stockistArray[counter].stockistId + "'>"+ data.stockistArray[counter].stockistName + "</option>";
		}
		$("#StockistName").empty();
		$("#StockistName").append(optionHtml);
	}, 'json');
}

//state dropdown 
function stateDropDownListForEdit(stateId){
	$.post(contextApplicationPath+'/StockistController/displayStockistDetailsForm',function(data){
		var optionHtml="<option disabled selected> Select State</option>";
		for (var i = 0; i < data.stateArray.length; i++) {
			if(stateId==data.stateArray[i].stateId)
				optionHtml += "<option selected='selected' value='" + data.stateArray[i].stateId + "'>"+ data.stateArray[i].stateName + "</option>";
			else
				optionHtml += "<option value='" + data.stateArray[i].stateId + "'>"+ data.stateArray[i].stateName + "</option>";
		}
		$("#stateName").empty();
		$("#stateName").append(optionHtml);
	},'json');
}

function getDistrictForEdit(districtId,stateId){
	$.post(contextApplicationPath+'/DistrictController/getDistrict', {
		stateId : stateId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select District</option>";
		for (counter = 0; counter < data.districtArray.length; counter++) {
			if(districtId==data.districtArray[counter].distId)
				optionHtml += "<option selected='selected' value='" + data.districtArray[counter].distId + "'>"+ data.districtArray[counter].distName + "</option>";
			else
				optionHtml += "<option value='" + data.districtArray[counter].distId + "'>"+ data.districtArray[counter].distName + "</option>";
		}
		$("#districtName").empty();
		$("#districtName").append(optionHtml);
	}, 'json');
}

function getCitiesForEdit(distId,cityId){
	$.post(contextApplicationPath+'/CityController/getCities', {
		distId : distId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select City</option>";
		for (counter = 0; counter < data.cityArray.length; counter++) {
			if(cityId==data.cityArray[counter].cityId)
				optionHtml += "<option selected='selected' value='" + data.cityArray[counter].cityId + "'>"+ data.cityArray[counter].cityName + "</option>";
			else
				optionHtml += "<option value='" + data.cityArray[counter].cityId + "'>"+ data.cityArray[counter].cityName + "</option>";
		}
		$("#cityName").empty();
		$("#cityName").append(optionHtml);
	}, 'json');
}
//if 'other'
function particularsDropDownListForOtherRadioForEdit(PARTICULAR_ID) {
	$.post(contextApplicationPath+ '/InwardCourierController/getParticularsDropdownList',
					function(data) {
						var optionHtml = "<option disabled selected> Select Particulars</option>";
						for (var i = 0; i < data.particularsArr.length; i++) {
							if(data.particularsArr[i].PARTICULAR_ID!=3){
								if(PARTICULAR_ID==data.particularsArr[i].PARTICULAR_ID)
									optionHtml += "<option selected='selected' value='"+ data.particularsArr[i].PARTICULAR_ID+ "'>"+ data.particularsArr[i].PARTICULAR_NAME+ "</option>";
								else
									optionHtml += "<option value='"+ data.particularsArr[i].PARTICULAR_ID+ "'>"+ data.particularsArr[i].PARTICULAR_NAME+ "</option>";
							}
						}
						$("#selectParticular").empty();
						$("#selectParticular").append(optionHtml);
					}, 'json');
}

//particulars dropdown
function perticularsDropDownListForEdit(PARTICULAR_ID) {
	$.post(contextApplicationPath+ '/InwardCourierController/getParticularsDropdownList',
					function(data) {
						var optionHtml = "<option disabled selected> Select Particulars</option>";
						for (var i = 0; i < data.particularsArr.length; i++) {
							if(PARTICULAR_ID==data.particularsArr[i].PARTICULAR_ID)
								optionHtml += "<option selected='selected' value='"+ data.particularsArr[i].PARTICULAR_ID+ "'>"+ data.particularsArr[i].PARTICULAR_NAME+ "</option>";
							else
								optionHtml += "<option value='"+ data.particularsArr[i].PARTICULAR_ID+ "'>"+ data.particularsArr[i].PARTICULAR_NAME+ "</option>";
						}
						$("#selectParticular").empty();
						$("#selectParticular").append(optionHtml);
					}, 'json');
}
//
function loadBloodGroupDropDownForEdit1(BLOOD_GROUP_ID)
{
	var html=" <select class='form-control' class='form-control' name='bloodroupName'>"
	+" <option disabled selected>Select Blood Group</option>";
	$.post(contextApplicationPath+'/EmployeeController/bloodGroupList', function(data) {
		$(data).each(function(index,element){
			if(BLOOD_GROUP_ID==element.BLOOD_GROUP_ID)
				html += "<option selected='selected' value='" + element.BLOOD_GROUP_ID+ "'>"+ element.BLOOD_GROUP + "</option>";
			else
				html += "<option value='" + element.BLOOD_GROUP_ID+ "'>"+ element.BLOOD_GROUP + "</option>";
		});
		html+=" </select>";
		$('#bloodGroupList').html(html);
	}, 'json');
}
//
function loadRoleTypeDropDownForEdit1(ROLE_TYPE_ID)
{
	var html="<select class='form-control' class='form-control' name='roleType'>"
	+"<option disabled selected>Select Employee Role</option>";
	$.post(contextApplicationPath+'/EmployeeController/loadRoleType', function(data) {
		$(data).each(function(index,element){
			if(ROLE_TYPE_ID==element.ROLE_TYPE_ID)
				html+= "<option selected='selected' value='" + element.ROLE_TYPE_ID+ "'>"+ element.ROLE_TYPE + "</option>";
			else
				html+= "<option value='" + element.ROLE_TYPE_ID+ "'>"+ element.ROLE_TYPE + "</option>";
		});
		html+=" </select>";
		$('#roleTypeDropDown').html(html);
	}, 'json');
}
//
function loadDocumentTypeListForEdit(docTypeId) {
	var html = "<select class='form-control' class='form-control' id='field-1' name='documentTypeId' parsley-required>"
			+ "<option selected disabled>Select Document Type</option>";

	$.post(contextApplicationPath + '/DocumentListTypController/documentList',
			function(data) {
				$(data).each(
						function(index, element) {
							if(docTypeId==element.DOCUMENT_ID)
								html += "<option selected='selected' value='" + element.DOCUMENT_ID+ "'>" + element.DOCUMENT_NAME+ "</option>";
							else
								html += "<option value='" + element.DOCUMENT_ID+ "'>" + element.DOCUMENT_NAME+ "</option>";
						});
				html += "</select>";
				$('#documentList').html(html);
			}, 'json');
}

function displyaCateringAgentDropDownForEdit(orgId, cartingAgentId){
	if(orgId==null||orgId==undefined)
		return false;
	$.post(contextApplicationPath+'/CartingAgentController/getCateringAgentDropDownByOrg', {
		orgId : orgId
	}, function(data) {
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Carting Agent</option>";
		$(data).each(function(index, element) {
			if(cartingAgentId == element.AGENT_ID)
				optionHtml += "<option selected='selected' value='" + element.AGENT_ID + "'>"+ element.AGENT_NAME + "</option>";
			else
				optionHtml += "<option value='" + element.AGENT_ID + "'>"+ element.AGENT_NAME + "</option>";
		});
		$("#selectCartingAgent").empty();
		$("#selectCartingAgent").append(optionHtml);
	}, 'json');
}