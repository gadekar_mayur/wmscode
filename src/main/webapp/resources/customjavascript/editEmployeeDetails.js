function editEmployeeDetails(empDetailsId,mainIndex)
{
	//alert("in edit Employee Details Darshan ="+empDetailsId+","+mainIndex);
	$.post(contextApplicationPath+'/EmployeeController/ViewAllDetailsOfEmployeeDetails',{empDetailsId : empDetailsId, editStatus : true}, function(data) {
		$(data).each(function(index,element){
			//commonData is global var
			commonData = element;
		var html=""
			+ "<div class='modal fade in' id='modal-responsive' >"
		    + "    <div class='col-md-13'>"
			+ "        <div class='modal-content'>"
			+ "            <div class='modal-header'>"
			+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Employee Details</strong></h4>"
			+ "            </div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"													<form id='editEmployeeDetails' action='"+contextApplicationPath+"/EmployeeController/updateEmployeeDetails' method='post'>"
			+"													  <input type='hidden' name='empDetailsId' value='"+empDetailsId+"'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Organization Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' disabled class='form-control' value='"+element.ORG_NAME+"' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Employee Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' placeholder='Employee Name' value='"+element.EMP_NAME+"' class='form-control'  id='eNameId' name='eName' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Blood Group*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"<div id='bloodGroupList'></div>";
			html+="                                                                </div>"
			+"                                                            </div>  "                                              
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Role*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"<div id='roleTypeDropDown'></div>";
			html+="                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Marital Status*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select class='form-control' class='form-control' name='MaritalStatus'>"
			+"                                                                        <option value='-1'>Select Marital Status</option>";
			if("Married"==element.MARRITAL_STATUS){
			   html+="                                                                        <option  selected='selected' value='Married'>Married</option>"
				   +"                                                                        <option value='Unmarried'>Unmarried</option>";
			}
			else if("Unmarried"==element.MARRITAL_STATUS){
				 html+="                                                                        <option value='Married'>Married</option>"
					   +"                                                                        <option  selected='selected' value='Unmarried'>Unmarried</option>";
			}
		html+="                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"                                                      
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Mobile No*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' value='"+element.MOBILE+"' placeholder='Mobile No' class='form-control' id='mobileNoId' name='mobileNo' parsley-type='onlynumber' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Current Address*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='textarea' value='"+element.CURR_ADDR+"' placeholder='Current Address' class='form-control' name='currentAddress' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='from-group' style='height: 90px;'>"/*
			+"                                                                <div class='div_checkbox' style='padding-left: 20px;'>"
			+"                                                                    <input type='checkbox' id='ctest' style='opacity: 1 !important;'>Same as Current+"
			+"                                                                </div>"*/
			+"                                                                <label class='form-label'><strong>Permanent Address*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' value='"+element.PER_ADDR+"' placeholder='Permanent Address' class='form-control' name='PermanentAddress' required>"
			+"                                                                </div>"
			+"                                                            </div>"               
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Date of Joining*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input class='datepicker form-control' value='"+element.DOJ+"' type='text' placeholder='Date of Joining' style='height: 36px; padding-left: 10px;' id='doj' name='doj' required>"
			+"                                                                </div>"
			+"                                                            </div>"/*                                                         
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Image*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"  															  <div id='employeeImgId'>"
			+"                                                               	 <img id='employeeImgId1' onClick='showImagePopup(\""+element.IMAGE+"\")' src='"+element.IMAGE+"' alt='Smiley face' height='100' width='100'>"
			+"  															  </div>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='file' class='form-control' name='empImage'>"
			+"                                                                </div>"
			+"                                                            </div>"*/
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>UserName*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' value='"+element.USER_NAME+"' class='form-control' name='uname' required>"
			+"																	  <div id='UserName_Error' class='validationError'></div>"
			+"                                                                </div>"
			+"                                                            </div>"
            +"                                                            <div class='form-group'>"
            +"                                                     		    <button type='button' onclick='editEmployeeImagesListPopup("+empDetailsId+")' class='btn btn-success'>Edit Employee Image</button>"
		    +"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Company Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' disabled value='"+element.COMP_NAME+"' class='form-control'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='col-lg-12 control-label' style='padding-left: 0px;'><strong>Gender*</strong></label>"
			+"                                                                <div class='col-lg-12'>"
			+"                                                                    <div class='pos-rel'>"
			+"                                                                        <input tabindex='13' id='ctest' type='radio' name='sex' value='Male' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;'>"
			+"                                                                        <label class='p-l-40' for='flat-checkbox-1'>Male</label>"
			+"                                                                    </div>"
			+"                                                                    <div class='pos-rel'>"
			+"                                                                        <input tabindex='14' id='ctest' type='radio' name='sex' value='Female' parsley-group='mygroup2' parsley-required='true' class='parsley-validated' style='opacity: 1 ! important;'>"
			+"                                                                        <label class='p-l-40' for='flat-checkbox-2'>Female</label>"
			+"                                                                    </div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>DOB*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input class='datepicker form-control' value='"+element.DOB+"' type='text' placeholder='Select DOB' style='height: 36px; padding-left: 10px;' id='dob' name='dob' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Qualification*</strong>" 
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' placeholder='Qulification' value='"+element.QUALIFICATION+"' class='form-control' id='qulificationId' name='qulification' required>"
			+"                                                                </div>"
			+"                                                            </div>"  
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Telephone No</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' placeholder='Telephone No' value='"+element.TELEPHONE+"' class='form-control' name='telephoneNo' parsley-type='onlynumber'>"
			+"                                                                </div>"
			+"                                                            </div>"                  
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Email ID</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' placeholder='Email ID' value='"+element.EMAIL+"' class='form-control' id='email_Id' name='emailId'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Pincode*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' placeholder='Pincode' value='"+element.PIN_CODE+"' class='form-control' name='pincode' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>PAN No</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' placeholder='PAN No' value='"+element.PAN_NUM+"' class='form-control' name='panno'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Basic Salary</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' placeholder='Basic Salary' value='"+element.BASIC_SALARY+"' class='form-control' name='basicSalary' parsley-type='number'>"
			+"                                                                </div>"
			+"                                                            </div>"/*
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Address Proof* : - </strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"  															  <div id='addressProofImgId'>"
			+"                                                               	 <img id='addressProofImgId1' onClick='showImagePopup(\""+element.ADDR_PROOF+"\")' src='"+element.ADDR_PROOF+"' alt='Smiley face' height='100' width='100'>"
			+"  															  </div>"
			+"                                                                <div class='controls'>"
			+"                                                                  <input type='file' class='form-control'  name='AddressProofImage'>"
			+"                                                                </div>"
			+"                                                            </div>"*/
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Password*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='password' value='"+element.PASSWORD+"' class='form-control' name='pwd' required>"
			+"                                                                </div>"
			+"                                                            </div>"
            +"                                                            <div class='form-group'>"
            +"                                                     		    <button type='button' onclick='editEmployeeAddressProofImagesListPopup("+empDetailsId+")' class='btn btn-success'>Edit Employee Address Proof Images</button>"
		    +"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                                <div class='pull-right'>"
			+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#editEmployeeDetails\")'>Update</button>"
			+"                                                                    <button type='reset' class='btn btn-danger m-b-10'onClick='hidePopUp()' >Cancel</button>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"													  </form>"
			+ "                                                </div>"
		    + "                                            </div>"
	        + "        </div>"
	        + "    </div>"
	        + "</div>";
			$('#popup').html(html);
			$("#dob").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
			$("#doj").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
			 $('input:radio[name="sex"]').filter('[value="'+element.GENDER+'"]').attr('checked', true);
			$("#modal-responsive").addClass("in");
			$("#modal-responsive").attr("aria-hidden","false");
			$("#modal-responsive").css("display","block");
			loadRoleTypeDropDownForEdit1(element.ROLE_ID);
			loadBloodGroupDropDownForEdit1(element.BLOOD_GROUP_ID);
			ajaxEditEmployeeDetails('editEmployeeDetails',mainIndex);
		});
	}, 'json');	
}
//
function ajaxEditEmployeeDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						$('#EMP_NAME'+mainIndex).html($('#eNameId').val());
						$('#GENDER'+mainIndex).html($('input:radio[name=sex]:checked').val());
						$('#Qualification'+mainIndex).html($('#qulificationId').val());
						$('#Mobile_No'+mainIndex).html($('#mobileNoId').val());
						$('#Email_ID'+mainIndex).html($('#email_Id').val());
						$('#modal-responsive').remove();
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
//
function editEmployeeImagesListPopup(employeeRegistrationId){
	editListOfImagesPopup(employeeRegistrationId,"documentImage",commonData.IMAGE,"EMPLOYEE_IMAGE" , "/EmployeeController/updateEmployeeImage");
}
//Image updation Ajax call after getting result from server
function ajaxEmployeeImageUpdate(data){
	if(data.RESULT==true) {
		commonData.IMAGE = data.IMAGE;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Image
function deleteEmployeeImage(employeeImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/EmployeeController/deleteEmployeeImage', {
			employeeImageId : employeeImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.IMAGE[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//
function editEmployeeAddressProofImagesListPopup(employeeRegistrationId){
	editListOfImagesPopup(employeeRegistrationId,"documentImage",commonData.ADDR_PROOF,"EMPLOYEE_ADDRESS_PROOF_IMAGE" , "/EmployeeController/updateEmployeeAddressProofImage");
}
//Image updation Ajax call after getting result from server
function ajaxEmployeeAddressProofImageUpdate(data){
	if(data.RESULT==true) {
		commonData.ADDR_PROOF = data.ADDR_PROOF;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteEmployeeAddressProofImage(employeeAddressProofImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/EmployeeController/deleteEmployeeAddressProofImage', {
			employeeAddressProofImageId : employeeAddressProofImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.ADDR_PROOF[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//Ducument
function editEmployeeDocumetForm(index, from)
{
		var html = ""
			+ "<div class='modal fade in' id='modal-responsive' >"
	        + "    <div class='col-md-13'>"
	        + "        <div class='modal-content'>"
	        + "            <div class='modal-header'>"
	        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	        + "                <h4 class='modal-title' id='myModalLabel'><strong>Update Employee Upload Document Details</strong></h4>"
	        + "            </div>"
	        + "												<form id='editEmployeeDocumentForm' action='"+contextApplicationPath+"/EmployeeController/updateEmployeeDocumentInfo' method='post'>"
			+ "                                                <div class='panel-body'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "															   <input type='hidden' name='employeeDocId' value='"+commonData[index].EMP_DOCUMENT_ID+"'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Organization Name</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input disabled type='text' value='"+commonData[index].ORG_NAME+"' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
            +"                                                            <div class='form-group'>"
            +"                                                     		    <button type='button' onclick='editEmployeeDocImagesListPopup("+commonData[index].EMP_DOCUMENT_ID+","+index+")' class='btn btn-success'>Edit Employee Document Images</button>"
		    +"                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Employee*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='employeeDropDown' name='emplyeeDetailsId' class='form-control' class='form-control'>"
			+"                                                                        <option disabled selected> Select Employee</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Document Type*</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "																		<div id='documentList'></div>"
	        + "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                                <div class='pull-right'>"
			+ "                                                                    <button class='btn btn-primary m-b-10' >Submit</button>"//onclick='showLoader()'
			+ "                                                                    <button type='reset' onclick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "												</form>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>";
		$('#popup').html(html);
		loadDocumentTypeListForEdit(commonData[index].DOCUMENT_TYPE_ID);
		loadEmployeeDropDownForEdit(commonData[index].ORG_ID,commonData[index].EMP_ID) 
		ajaxEditEmployeeDocumentForm("editEmployeeDocumentForm",index, from);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block");
}
function ajaxEditEmployeeDocumentForm(FormId,index, from) {
	var options = {
		success : function(data) {
				alert(data.MSG);
				if(data.RESULT==true){
					commonData[index].DOCUMENT_TYPE_ID = data.DOCUMENT_TYPE_ID;
					commonData[index].DOCUMENT_TYPE = data.DOCUMENT_TYPE;
					commonData[index].EMP_NAME = data.EMP_NAME;
					commonData[index].EMP_ID = data.EMP_ID;
					employeeDocumetListView(commonData, from, urlForEdit);
					//commonEmployeeDocumetController("","",1);
				}
				hidePopUp();
				$('#loader').hide();
		}
	};
	$('#' + FormId).ajaxForm(options);
}
//
function editEmployeeDocImagesListPopup(documentId, index){
	//commonData2 is global var
	commonData2 = index;
	editListOfImagesPopup(documentId,"documentImage",commonData[index].FILE_NAME,"EMPLOYEE_UPLOAD_DOCUMENT_IMAGE" , "/EmployeeController/updateEmployeeUploadDocumentImage");
}
//Image updation Ajax call after getting result from server
function ajaxEmployeeDocumentImageUpdate(data){
	if(data.RESULT==true) {
		commonData[commonData2].FILE_NAME = data.FILE_NAME;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}

function deleteEmployeeDocumentImage(employeeDocumentPathId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/EmployeeController/deleteEmployeeDocumentFileImage', {
			employeeDocumentPathId : employeeDocumentPathId
		}, function(data) {
			if(data.RESULT==true){
				commonData[commonData2].FILE_NAME[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}