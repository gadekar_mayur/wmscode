// = = = = = = = = Main Dashboard Start = = = = = = = = // 
//function loads dashboard table view
function addDashboardTable() {
	var html= ""
		+ "	<div class='tab-content' id='dashboardMain'>"
		+ "		<div class='page-title'> <i class='icon-custom-left'></i>"
		+ "			<h3><strong>Glance Report</strong></h3>"
		+ "		</div>"
		+ " 	<div class='col-sm-9 col-sm-offset-3'>"
		+ "       <div class='pull-right' style='padding-left: 10px;'>"
		+ "           <button class='btn btn-success m-b-10' onclick='addDashboardTable()'>Refresh</button>"	
		+ "           <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
		+ "       </div>"
		+ "  	</div>"
		+ "  	<div id='filterToggle'  style='display:none'>"	
		+ "      <div class='row'>"
		+ "            <div class='col-md-4'>"
		+ "                 <div class='form-group'>"
		+ "                     <label class='form-label'><strong>Organization</strong>"
		+ "                     </label>"
		+ "                     <span class='tips'></span>"
		+ "                     <div class='controls' id='searchControl'>"
		+ "                          <div id='organizationSelectDiv'>"
		+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
		+ "                               </select>"
		+ "							 </div>"
		+ "                     </div>"
		+ "                  </div>"
		+ "              </div>"	
		+ "            <div class='col-md-4'>"
		+ "                 <div class='form-group'>"
		+ "                     <label class='form-label'><strong>Company</strong>"
		+ "                     </label>"
		+ "                     <span class='tips'></span>"
		+ "                     <div class='controls' id='searchControl'>"
		+ "                          <div id='companySelectDiv'>"
		+ "                               <select name='companySelected[]' multiple id='companySelect'>"
		+ "                               </select>"
		+ "							 </div>"
		+ "                     </div>"
		+ "                  </div>"
		+ "              </div>"
		+ "              <div class='col-md-4'>"
		+ "                   <div class='form-group'>"
		+ "                        <label class='form-label'><strong>Stockist</strong>"
		+ "                        </label>"
		+ "                        <span class='tips'></span>"
		+ "                        <div class='controls' id='searchControl'>"
		+ "                              <div id='stockistSelectDiv'>"
		+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
		+ "                                   </select>"
		+ "								 </div>"
		+ "                        </div>"
		+ "                    </div>"
		+ "              </div>"
		+ "              <div class='col-md-4'>"
		+ "                   <div class='form-group'>"
		+ "                       <label class='form-label'><strong>Year</strong>"
		+ "                       </label>"
		+ "                       <span class='tips'></span>"
		+ "                       <div class='controls'>"
		+ "                           <div id='yearSelectDiv'>"
		+ "							  	 <select id='yearSelect' class='form-control'>"
		+ "								 </select>"                                                              
		+ "							  </div>"
		+ "                       </div>"
		+ "                   </div>"
		+ "                </div>"
		+ "         </div>"
		+ "         <div class='col-sm-9 col-sm-offset-3'>"
		+ "                <div class='pull-right' style='padding-left: 10px;'>"
		+ "                      <button class='btn btn-success m-b-10' onclick=' dashboardFilterValidator()'>Show</button>"
		+ "                 </div>"
		+ "         </div>"
		+ "		</div>"
		+ "		<div class='row'>"
		+ "			<div class='col-md-6'>"
		+ "				<div id='dashboardTable' class='col-md-12 col-sm-12 col-xs-12 table-responsive' style='padding: 0 !important;'>"
		+ "					<table class='table table-striped table-hover' id='example'>"
		+ "						<thead class='no-bd' id='a'>"
		+ "							<tr>"
		+ "                         	<th style='text-align: center;'><strong>Month</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Cases Out- wards</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Cases In- wards</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Courier In- wards</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Courier Out- wards</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Orders Received</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Invoice Gene- rated</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Cheque Received</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Cheque Depo- sited</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Cheque Returned</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Sales Amount</strong>"
		+ "                             </th>"	
		+ "                             <th style='text-align: center;'><strong>NO. of Return Claim Received</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Total Claim Amount</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>NO. of Credit Notes</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Credit Notes Amount</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>Checking pendings</strong>"
		+ "                             </th>"
		+ "                             <th style='text-align: center;'><strong>C N Pendings</strong>"
		+ "                             </th>"
		+ "							</tr>"
		+ "						</thead>"
		+ "						<tbody class='no-bd-y' id='dashboardTableBody'>"
		+ "                     </tbody>"
		+ "					</table>"
		+ "				</div>"
		+ "			</div>"
		+ "		</div>"
		+ "	</div>";
	$('#main-content').html(html);
	$('#organizationSelect').multiselect();
	$('#companySelect').multiselect();
	$('#stockistSelect').multiselect();
	setYears();
	loadOrganizatonsMultiSelectDropDown();
	loadCompaniesMultiSelectDropDown();
	var year = $("#yearSelect").val();
	loadDashboardTableData("", "", "", year);	
}

//function set dashboard filter values
function dashboardFilterValidator() {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var year = null;
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	year = $("#yearSelect").val();
	
	loadDashboardTableData(organizations,companies, stockists, year)
}

//function loads dashboard table data 
function loadDashboardTableData(organizations,companies, stockists, year) {
	$("#loader").show();
	
	$.post(contextApplicationPath + '/DashboardController/loadDashboardTable', {
		"organizations" : organizations,
		"companies" : companies,
		"stockists" : stockists,
		"year" : year
	}, function(data) {
        var i=4;
        var html;
		$(data).each(
				function(index, element) {
					
					html += "<tr style='text-align: center;'>" 
						 + "<td>" 
						 + element.month
						 + "</td>"
						 + "<td onclick='outwardCasesCall("+i+")'> " 
						 + element.outward_cases
						 + "</td>" 
						 + "<td onclick='inwardCasesCall("+i+")'>" 
						 + element.inward_cases
						 + "</td>" 
						 + "<td onclick='inwardCourierCall("+i+")'>" 
						 + element.inward_courier
						 + "</td>" 
						 + "<td onclick='outwardCourierCall("+i+")'>" 
						 + element.outward_courier
						 + "</td>" 
						 + "<td onclick='ordersReceivedCall("+i+")'>" 
						 + element.orders_received
						 + "</td>" 
						 + "<td onclick='invoiceGeneratedCall("+i+")'>" 
						 + element.invoice_generated
						 + "</td>" 
						 + "<td onclick='chequesReceivedCall("+i+")'>" 
						 + element.cheque_received
						 + "</td>" 
						 + "<td onclick='chequesDepositedCall("+i+")'>" 
						 + element.cheque_deposited
						 + "</td>" 
						 + "<td onclick='chequesReturnedCall("+i+")'>" 
						 + element.cheque_returned
						 + "</td>" 
						 + "<td onclick='outwardCasesCall("+i+")'>" 
						 + element.sales_amount
						 + "</td>" 
						 + "<td onclick='returnCliamsCall("+i+")'>" 
						 + element.return_claim
						 + "</td>" 
						 + "<td onclick='returnCliamsCall("+i+")'>" 
						 + element.claim_amount
						 + "</td>" 
						 + "<td onclick='creditNotesCall("+i+")'>" 
						 + element.credit_notes
						 + "</td>" 
						 + "<td onclick='creditNotesCall("+i+")'>" 
						 + element.credit_amount
						 + "</td>" 
						 + "<td onclick='checkingPendingsCall("+i+")'>"
						 + element.checking_pendings
						 + "</td>" 
						 + "<td onclick='cnPendingsCall("+i+")'>" 
						 + element.cn_pendings
						 + "</td>" 
						 + "</tr>";
					
						if(i>=12)
							i=0;
						if(i==3)
							i=12;
						
						i++;
				});
		$('#dashboardTableBody').html(html);
		$('#loader').hide();
	}, 'json');	
}
//= = = = = = = = Main Dashboard End = = = = = = = = // 


//= = = = = = = = Outward cases Start = = = = = = = = // 
//fuction call outward cases view on dashboard with filter values
function outwardCasesCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	viewGetPassLRListOnDashboard(organizations,companies,stockists,fromDate,toDate);
}

//function load outward cases view on dashboard
function viewGetPassLRListOnDashboard(organizations,companies,stockists,fromDate,toDate) {
	$('#loader').show();
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""
	+ temp + "\",\"" + organizations + "\",\"" + companies + "\",\"" + stockists
	+ "\",\"" + temp + "\",\"" + fromDate + "\",\"" + toDate
	+ "\"";
	
	var html = ""
			+ "                                <div class='tab-pane fade active in' id='tab2_5'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>Outward Cases</strong></h3>"
			+ "                                    </div>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12'>"
			+ "                                            <div class='panel panel-default'>"
			+ "                                                <div class='panel-body'>"
		/*	+ "                                                  <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "                                                                <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
			+ "                                                            </div>"
			+ "                                                   </div>"*/
			+ " 												<div id='filterToggle'  style='display:none'>"	
			+ "                                                     <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "                                                                <input type='checkbox' name='filterSelection' id='glanceReport'  onclick='glanceReport()'> Glance Report"	
		                                                               
			+ "                                                            </div>"
			+ "                                                       </div>"
			+ "                                                  <div id='companyWise'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search By</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "																	<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
			// + " <option value='Stockist'>Stockist</option>"
			+ "                                                    				   	<option value='TripNo'> Trip Number</option>"
			// +" <option value='Organization'>Organization</option>"
			// + " <option value='Company'>Company Name</option>"
			// + " <option value='Stockist'>Stockist</option>"
			+ "                                                    			  	 	<option value='Transporter'>Transporter</option>"
			+ "                                                    			  	 	<option value='InvoiceNo'>Invoice Number</option>"
			+ "                                                     		 	 	<option value='NoOfCases'>No. of Cases</option>"
			+ "                                                     		 	 	<option value='LRNo'>LR Number</option>"
			+ "																	</select>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search For</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <input id='searchText' type='text' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                    <div class='row'>"
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Organization</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='organizationSelectDiv'>"
			+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Company</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <div id='companySelectDiv'>"
			+ "                                                                    <select name='companySelected[]' multiple id='companySelect'>"
			+ "                                                                    </select>"
			+ "																		</div>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Stockist</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <div id='stockistSelectDiv'>"
			+ "                                                                    <select name='stockistSelected[]' multiple id='stockistSelect' >"
			+ "                                                                    </select>"
			+ "																	</div>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>City</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <div id='citySelectDiv'>"
			+ "                                                                    <select name='citySelected[]' multiple id='citySelect'>"
			+ "                                                                    </select>"
			+ "																	   </div>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"

			+ "                                                       <div class='col-md-4'>"
			+ "                                                         <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Date</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                            <div class='controls'>"
			+ "                                                                <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                                                                <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "							                                	   <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
			+ "							                                	   <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
			+ "							                                	   <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
			+ "                                                             </div>"
			+ "                                                         </div>"
			+ "                                                       </div>"
			+ "                                                    </div>"
			+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "                                                                <button class='btn btn-success m-b-10' onclick='searchViewGetPassLRList()'>Show</button>"
			+ "																   <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onclick='generateExcel()'>Report </a>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "																   <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onClick='generateExcelConfirm("+url+")' >Generate Excel </a>"//onClick='generateExcel()'
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "															  <div class='pull-right'>"
			+ "                                                      	  </div>"
			+ "                                                  </div>"
			+ "                                              </div>"
			+ "<div id='transportWise' style='display:none'>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Transporter</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <div id='TransporterSelectDiv'>"
		                                                                  
			+ "																	</div>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Year</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <div id='yearSelectDiv'>"
			+ "																	<select id='yearSelect' class='form-control'>"

			+ "																	</select>"                                                              
			+ "																	</div>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "                                                                <button class='btn btn-success m-b-10' onclick='getGlanceReportData()'>Show</button>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "												   </div>"
			+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "																   <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onClick='generateExcelConfirm("+url+")' >Export to Excel </a>"//onClick='generateExcel()'
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                <div class='row' id='copanyWiseTable' >"
			+ "                                                    <div class='col-md-6'>"
			+ "                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                                        <table class='table table-striped table-hover'>"
			+ "                                                                            <thead class='no-bd' id='a'>"
			+ "                                                                                <tr>"
			+ "                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>Trip No</strong>"
			+ "                                                                                    </th>"
			//+ "                                                                                    <th style='text-align: center;'><strong>Organization</strong>"
			//+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>Company</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>Stockist</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>City</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>Transporter</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>No of Invo</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>No of Cases</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>Dispatch Date</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>LR NO</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>Total Amount</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>LR Image</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>Action</strong></th>"
			// +" <th style='text-align: center;'><strong>Print</strong></th>"
			+ "                                                                                </tr>"
			+ "                                                                            </thead>"
			+ "                                                                <tbody class='no-bd-y' id='lrViewSearch'>"
		//	+ "                                                                <tbody class='no-bd-y' id='test'>"
			+ "                                                                </tbody>"
			+ "                                                            </table>"
			+ "                                                    </div>"
			+ "                                                        <div id='pagination' class='pagination'></div>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "  <div class='row' id='TransporterWiseTable' style='display:none'>"
			+ "									 <div class='col-md-6'>"
			+ "                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                                        <table class='table table-striped table-hover'>"
			+ "                                                                            <thead class='no-bd' id='a'>"
			+ "                                                                                <tr>"
			+ "                                                                                    <th style='text-align: center;'><strong>Transporter</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>APRIL</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>MAY</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>JUNE</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>JULY</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>AUG</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>SEPT</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>OCT</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>NOV</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>DEC</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>JAN</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>FEB</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>MARCH</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>TOTAL CASES</strong>"
			+ "                                                                                    </th>"
			+ "                                                                                    <th style='text-align: center;'><strong>TOTAL AMOUNT</strong></th>"
			// +" <th style='text-align: center;'><strong>Print</strong></th>"
			+ "                                                                                </tr>"
			+ "                                                                            </thead>"
			+ "                                                                <tbody class='no-bd-y' id='transportWiseTableBody'>"
			+ "                                                                </tbody>"
			+ "                                                            </table>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "  </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                </div>";
	$('#main-content').empty();
	$('#main-content').html(html);
	$("#main-content").addClass("in");
	$(".commonDate").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	$('#organizationSelect').multiselect();
	$('#companySelect').multiselect();
	$('#stockistSelect').multiselect();
	$('#citySelect').multiselect();

	loadCompanies();
	loadCities();
	 loadOrganizatonsMultiSelectDropDown();
	transporterMultiSelectDropDown();
	commonsearchViewGetPassLRListController("", "", "", "", organizations, companies, stockists, "", fromDate, toDate, 1);
	subTabsForOutWardGetPass(false);
	setYears();
	// }, 'json');
}
//= = = = = = = = Outward cases End = = = = = = = = //


//= = = = = = = = Inward cases Start = = = = = = = = //
//fuction call inward cases view on dashboard with filter values
function inwardCasesCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	InwardGoodsRegistrationListOnDashboard(organizations,companies,stockists,fromDate,toDate);
}

//function load inward cases view on dashboard
function InwardGoodsRegistrationListOnDashboard(organizations,companies,stockists,fromDate,toDate)
{
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + temp + "\",\"" + fromDate + "\",\"" + toDate+ "\"";
	
	$('#loader').show();
	var html=""
		+"            <div  id='tab2_2'>"
		+"                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                	  <h3><strong>Inward Goods Registration View</strong></h3>"
		+"				  </div>"
		+"                <div class='row'>"
		+"                   <div class='col-md-12'>"
		+"                        <div class='panel panel-default'>"
		+"                            <div class='panel-body'>"
		/*+ "                                                  <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+ "                                                                <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
		+ "                                                            </div>"
		+ "                                                   </div>"*/	
		+ " 												<div id='filterToggle'  style='display:none'>"	
		+"                                <div class='row'>"
		+"                                    <div class='col-md-4'>"
		+"                                        <div class='form-group'>"
		+"                                            <label class='form-label'><strong>Search By</strong>"
		+"                                            </label>"
		+"                                            <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' onchange='showDatePickerField()'>"
		//+"                                                                        <option value='Organization' >Organization Name</option>"
		//+"                                                                        <option value='Transporter'>Transporter</option>"
	//	+"                                                                        <option value='Company'>Company Name</option>"
		+"                                                                        <option value='LRNo'>LR No.</option>"
		+"                                                                        <option value='SendingDepo'>Sending Depo</option>"
		+"                                                                        <option value='DriverName'>Driver Name</option>"
		+"                                                                        <option value='RegDate'>Inward Registration Date</option>"
		+"                                                                        <option value='STNNo'>STN No</option>"
		+"                                                                        <option value='STNNetAmount'>STN Net Amount</option>"
	//	+"                                                                        <option value='STNDate'>STN Date</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div class='col-md-4'>"
		+"                                        <div class='form-group'>"
		+"                                            <label class='form-label'><strong>Search For</strong>"
		+"                                            </label>"
		+"                                            <span class='tips'></span>"
		+"                                            <div id='searchControl' class='controls'>"
		+"                                                <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    </div>"
		+ "        <div class='row'>"
		+ "          							  <div class='col-md-4'>"
		+ "                 							<div class='form-group'>"
		+ "                     							<label class='form-label'><strong>Organization</strong>"
		+ "                     							</label>"
		+ "                     							<span class='tips'></span>"
		+ "                     							<div class='controls' id='searchControl'>"
		+ "                          							<div id='organizationSelectDiv'>"
		+ "                               							<select name='organizationSelected[]' multiple id='organizationSelect'>"
		+ "                               							</select>"
		+ "							 							</div>"
		+ "                     							</div>"
		+ "                  							</div>"
		+ "              </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Company</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <div id='companySelectDiv'>"
		+"                                                                    <select name='companySelected[]' multiple id='companySelect1'>"
		+"                                                                    </select>"
		+"																		</div>"	
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Transporter</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <div id='TransporterSelectDiv'>"
		+"                                                                    <select name='transporterMultiSelect[]' multiple id='transporterId'>"
		+"                                                                    </select>"
		+"																		</div>"	
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+ "                    <div class='col-md-4'>"
		+ "                        <div class='form-group'>"
		+ "                            <label class='form-label'><strong>Select Dates</strong>"
		+ "                            </label>"
		+ "                            <span class='tips'></span>"
		+ "                            <div class='controls'>"
		+ "                                <input type='text' id='fromDate' class='dateClassCommon form-control' type='text' placeholder='From (mm/dd/yyyy)' style='width: 49%; height: 36px; padding-left: 10px;float: left' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
		+ "                                <input type='text' id='toDate' class='dateClassCommon form-control' type='text' placeholder='To (mm/dd/yyyy)' style='width: 49%; height: 36px; padding-left: 10px;float: left' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
		//+"								<div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
		//+"								<div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
		//+"								<div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
		+ "                            </div>"
		+ "                        </div>"
		+ "                    </div>"
		+"                                    <div class='col-sm-9 col-sm-offset-3'>"
		+"                                        <div class='pull-right'>"
		+"                                            <button class='btn btn-success m-b-10' onclick='searchInwardGoodsRegDetails(1)'>Show</button>"
		+"                                        </div>"
		+"                                    </div>"
		+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+ "																   <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onClick='generateExcelForInward()' >Generate Excel </a>"//onClick='generateExcel()'
		+ "                                                            </div>"
		+ "                                                        </div>"
		+"                    </div>"
		+"                    </div>"
		+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+ "																   <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onClick='generateExcelForInwardController("+url+")' >Generate Excel </a>"//onClick='generateExcel()'
		+ "                                                            </div>"
		+ "                                                        </div>"
		+"                                    <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                    </div>"
		+"                                </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>"
		+"		 	  </div>";
		$('#main-content').html(html);
		$('#organizationSelect').multiselect();
		loadOrganizatonsMultiSelectDropDown()
		$('#companySelect1').multiselect();
		loadCompanies();
		$('#transporterId').multiselect();
		transporterMultiSelectDropDown();
		commonInwardGoodsRegistration("","","","",organizations, companies, "", fromDate, toDate,1);
//		showInwardGoodsRegistrationList(data);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
//	}, 'json');
}
//= = = = = = = = Inward cases End = = = = = = = = //


//= = = = = = = = Inward Courier Start = = = = = = = = //
//fuction call inward courier view on dashboard with filter values
function inwardCourierCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	inwardCourierRegistrationListingOnDashboard(organizations,companies,stockists,fromDate,toDate);
}

//functio load inward courier view on dashboard
function inwardCourierRegistrationListingOnDashboard(organizations,companies,stockists,fromDate,toDate) {
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\",\"" + temp+ "\"";
	
	$('#loader').show();
//	$.post(contextApplicationPath+'/InwardCourierController/inwardCourierRegistrationListing',function(data){
		var html=""
			+"                <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                	  <h3><strong>Inward Courier Registration View</strong></h3>"
			+"				  </div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-12'>"
			+ "                        <div class='panel panel-default'>"
			+ "                            <div class='panel-body'>"
			/*+ "                                                  <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "                                                                <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
			+ "                                                            </div>"
			+ "                                                   </div>"*/
			+ " 												<div id='filterToggle'  style='display:none'>"	
			+ "                                <div class='row'>"
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+ "                                            <label class='form-label'><strong>Search By</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls'>"
			+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFuncCourier();'>"
		//	+"														  <option value='Organization'>Select Organization</option>"
			//+ "                                                       <option value='Company'>Comapny Name</option>"
		//	+ "                                                       <option value='Date'>Search By Date</option>"
		//	+ "                                                       <option value='Stockist'>Stockist</option>"
			+ "                                                       <option value='CourierName'>Courier Name</option>"
			+ "                                                       <option value='DeliveryPerson' >Delivery Person</option>"
			+"														  <option value='DocketNo' >Docket Number</option>"
			+ "                                                       <option value='State'>State Name</option>"
			+ "                                                       <option value='DistrictName'>District Name</option>"
			+ "                                                       <option value='Cityname'>City Name</option>"
			+ "                                                     </select>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"//fromDate toDate searchOption searchText
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group' id='searchForDiv'>"
			+ "                                            <label class='form-label'><strong>Search For</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls' id='searchControl'>"
			+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                            </div>"
			+ "        <div class='row'>"
			+ "          							  <div class='col-md-4'>"
			+ "                 							<div class='form-group'>"
			+ "                     							<label class='form-label'><strong>Organization</strong>"
			+ "                     							</label>"
			+ "                     							<span class='tips'></span>"
			+ "                     							<div class='controls' id='searchControl'>"
			+ "                          							<div id='organizationSelectDiv'>"
			+ "                               							<select name='organizationSelected[]' multiple id='organizationSelect'>"
			+ "                               							</select>"
			+ "							 							</div>"
			+ "                     							</div>"
			+ "                  							</div>"
			+ "              </div>"
			+" 													<div id='OthersToggle'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Company</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <div id='companySelectDiv'>"
			+ "                                                                    <select name='companySelected[]' multiple id='companySelect'>"
			+ "                                                                    </select>"
			+ "																		</div>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "              </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Stockist</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <div id='stockistSelectDiv'>"
			+ "                                                                    <select name='stockistSelected[]' multiple id='stockistSelect' >"
			+ "                                                                    </select>"
			+ "																	</div>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        </div>"
			+ "                                                        </div>"
		//	+ "                                                <div class='panel-body'>"
		//	+ "                                                  <div class='col-sm-9 col-sm-offset-3'>"
//			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
//			+ "                                                                <button class='btn btn-success m-b-10' onclick='toggleOthers()'>Others</button>"										
//			+ "                                                            </div>"
			+ "                                                    <div class='row'>"
			+ "                    <div class='col-md-4'>"
			+ "                        <div class='form-group'>"
			+ "                            <label class='form-label'><strong>Select Dates</strong>"
			+ "                            </label>"
			+ "                            <span class='tips'></span>"
			+ "                            <div class='controls'>"
			+ "                                <input type='text' id='fromDate' class='dateClassCommon form-control' type='text' placeholder='From (mm/dd/yyyy)' style='width: 49%; height: 36px; padding-left: 10px;float: left' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                                <input type='text' id='toDate' class='dateClassCommon form-control' type='text' placeholder='To (mm/dd/yyyy)' style='width: 49%; height: 36px; padding-left: 10px;float: left' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                            </div>"
			+ "                        </div>"
			+ "                    </div>"
			+ "                    </div>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-success m-b-10' id='others' onclick='searchInwardCourierRegistrationDetails()'>Show</button> "
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "																  <button class='btn btn-info m-b-10' onclick='toggleOthers()'>Others</button>  <button class='btn btn-info m-b-10'  onClick='generateExcelForInwardCourier()' >Excel</button>"//onClick='generateExcel()'
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                            </div>"
			+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "																  <button class='btn btn-info m-b-10'  onClick='generateExcelForInwardCourierController("+url+")' >Excel</button>"//onClick='generateExcel()'
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                    <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                    </div>"
			+ "                                </div>"
			+ "                              <div id='pagination' class='pagination'></div>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>"
			+ "                </div>";
		$('#main-content').empty();
		$('#main-content').html(html);
		$('#organizationSelect').multiselect();
		loadOrganizatonsMultiSelectDropDown();
		$('#companySelect').multiselect();
		loadCompaniesMultiSelectDropDown();
		$('#stockistSelect').multiselect();
		commonInwardCourierRegistrationController("", "", "", "", organizations, companies, stockists, fromDate, toDate, "", 1);
		$('#loader').hide();
}
//= = = = = = = = Inward Courier End = = = = = = = = //


//= = = = = = = = Outward Courier Start = = = = = = = = //
//fuction call outward courier view on dashboard with filter values
function outwardCourierCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	outwardCourierRegistrationListingOnDashboard(organizations,companies,stockists,fromDate,toDate);
}

//function load outward courier view on dashboard
function outwardCourierRegistrationListingOnDashboard(organizations,companies,stockists,fromDate,toDate) {
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	
	$('#loader').show();
	var i=1;
	var html=""
		+"                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                	  <h3><strong>Outward Courier Registration View</strong></h3>"
		+"				  </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-12'>"
		+"                        <div class='panel panel-default'>"
		+"                            <div class='panel-body'>"
		/*+ " <div class='col-sm-9 col-sm-offset-3'>"
		+ "       <div class='pull-right' style='padding-left: 10px;'>"
		+ "           <button class='btn btn-danger m-b-10' onclick='toggleFilter()'>Filters+</button>"										
		+ "       </div>"
		+ "  </div>"*/
		+ "  <div id='filterToggle'  style='display:none'>"
		+"                                <div class='row'>"
		+"                                    <div class='col-md-4'>"
		+"                                        <div class='form-group'>"
		+"                                            <label class='form-label'><strong>Search By</strong>"
		+"                                            </label>"
		+"                                            <span class='tips'></span>"
		+"                                            <div class='controls'>"
		+"													<select id='searchOption' class='form-control' class='form-control' onchange='changeFuncOutward();'>"
//		+"														  <option value='Organization'>Select Organization</option>"
	//	+ "                                                       <option value='Company'>Comapny Name</option>"
	//	+ "                                                       <option value='Date'>Search By Registration Date</option>"
		//+ "                                                       <option value='Stockist'>Stockist</option>"
		+ "                                                       <option value='SendingToDepot'>Sending To Depot</option>"
		+"														  <option value='CourierName'> Courier Name  </option>"
		+ "                                                       <option value='DocketNo'> Docket No.</option>"
		+ "                                                       <option value='DeliveryPerson'>Delivery Person  </option>"
		+ "                                                       <option value='CourierPerson'>Courier Person  </option>"
		//+ "                                                       <option value='Weight'>Weight  </option>"
		+ "                                                       <option value='Quantity'>Quantity  </option>"
		//+ "                                                       <option value='CHEQUENO'>CHEQUE NO </option>"
		//+ "                                                       <option value='BANKNAME'>BANK NAME </option>"
		+ "                                                       <option value='State'>State </option>"
		+ "                                                       <option value='district'>District</option>"
		+ "                                                       <option value='City'>City </option>"
		+ "                                                     </select>"		
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div class='col-md-4'>"
		+"                                        <div class='form-group'>"
		+"                                            <label class='form-label'><strong>Search For</strong>"
		+"                                            </label>"
		+"                                            <span class='tips'></span>"
		+"                                            <div class='controls' id='searchControl'>"
		+"                                                <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>"
		+ "        <div class='row'>"
		+ "            <div class='col-md-4'>"
		+ "                 <div class='form-group'>"
		+ "                     <label class='form-label'><strong>Organization</strong>"
		+ "                     </label>"
		+ "                     <span class='tips'></span>"
		+ "                     <div class='controls' id='searchControl'>"
		+ "                          <div id='organizationSelectDiv'>"
		+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
		+ "                               </select>"
		+ "							 </div>"
		+ "                     </div>"
		+ "                  </div>"
		+ "              </div>"
		+ "            <div class='col-md-4'>"
		+ "                 <div class='form-group'>"
		+ "                     <label class='form-label'><strong>Company</strong>"
		+ "                     </label>"
		+ "                     <span class='tips'></span>"
		+ "                     <div class='controls' id='searchControl'>"
		+ "                          <div id='companySelectDiv'>"
		+ "                               <select name='companySelected[]' multiple id='companySelect'>"
		+ "                               </select>"
		+ "							 </div>"
		+ "                     </div>"
		+ "                  </div>"
		+ "              </div>"
		+ "              <div class='col-md-4'>"
		+ "                   <div class='form-group'>"
		+ "                        <label class='form-label'><strong>Stockist</strong>"
		+ "                        </label>"
		+ "                        <span class='tips'></span>"
		+ "                        <div class='controls' id='searchControl'>"
		+ "                              <div id='stockistSelectDiv'>"
		+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
		+ "                                   </select>"
		+ "								 </div>"
		+ "                        </div>"
		+ "                    </div>"
		+ "              </div>"
		+ "              <div class='col-md-4'>"
		+ "                   <div class='form-group'>"
		+ "                       <label class='form-label'><strong>Date</strong>"
		+ "                       </label>"
		+ "                       <span class='tips'></span>"
		+ "                       <div class='controls'>"
		+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
		+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
		+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
		+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
		+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
		+ "                        </div>"
		+ "                   </div>"
		+ "                </div>"
		+ "             </div>"
		+"                                    <div class='col-sm-9 col-sm-offset-3'>"
		+"                                        <div class='pull-right'>"
		+"                                            <button class='btn btn-success m-b-10' onclick='searchoutwardCourierRegistration()'>Show</button> &nbsp;&nbsp;&nbsp;<button class='btn btn-info m-b-10'  onClick='generateExcelForOutwardCourier()' >Excel</button>"
		+"                                        </div>"
		+"                                    </div>"
		+"                 				</div>"
		+"                            <div class='row'>"
		+"                                    </div>"
		+"                                    <div class='col-sm-9 col-sm-offset-3'>"
		+"                                        <div class='pull-right'>"
		+"                                            <button class='btn btn-info m-b-10'  onClick='generateExcelForOutwardCourierController("+url+")' >Excel</button>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                            </div>"
		+"                        </div>"
		+ "                          <div id='pagination' class='pagination'></div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
		$('#main-content').empty();
		$('#main-content').html(html);
		$('#organizationSelect').multiselect();
		$('#companySelect').multiselect();
		$('#stockistSelect').multiselect();
		 loadOrganizatonsMultiSelectDropDown();
		 loadCompaniesMultiSelectDropDown();
		commonOutwardCourierRegistrationController("", "", "", "",organizations,companies,stockists,fromDate,toDate, 1);
}
//= = = = = = = = outward Courier End = = = = = = = = //


//= = = = = = = = Orders Received Start = = = = = = = = //
//fuction call Order Entry view on dashboard with filter values
function ordersReceivedCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	ViewOrderEntryRegistrationListOnDashboard(organizations,companies,stockists,fromDate,toDate);
}
//function load Order Entry view on dashboard
function ViewOrderEntryRegistrationListOnDashboard(organizations,companies,stockists,fromDate,toDate) {
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	
	$('#loader').show();
	
	var i = 1;
	var html = ""
			+ "                                <div class='tab-pane fade active in' id='tab2_7'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Order Entry Registration</strong></h3>"
			+ "                                    </div>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12'>"
			+ "                                            <div class='panel panel-default'>"
			+ "                                                <div class='panel-body'>"
			/*+ " <div class='col-sm-9 col-sm-offset-3'>"
			+ "       <div class='pull-right' style='padding-left: 10px;'>"
			+ "           <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
			+ "       </div>"
			+ "  </div>"*/
			+ "  <div id='filterToggle'  style='display:none'>"	
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search By</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutward();'>"
			//+ "                                                     		  	<option value='Stockist'>Stockist</option>"
			//+ "																	<option value='Organization'>Select Organization</option>"
			//+ "                                                      		 	<option value='Company'>Comapny Name</option>"
			//+ "                                                     		  	<option value='Date'>Search By Date</option>"
			// + " <option value='Stockist'>Stockist</option>"
			+ "                                                     		  	<option value='OrderId'>Order Id</option>"
			+ "																	<option value='OrderNo'> Order Number</option>"
			// + " <option value='OrderMode'> Order Mode</option>"
			+ "																</select>"
			// +" <input type='text' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search For</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <input id='searchText' type='text' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                     </div>"
			+ "        <div class='row'>"
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Organization</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='organizationSelectDiv'>"
			+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Company</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='companySelectDiv'>"
			+ "                               <select name='companySelected[]' multiple id='companySelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                        <label class='form-label'><strong>Stockist</strong>"
			+ "                        </label>"
			+ "                        <span class='tips'></span>"
			+ "                        <div class='controls' id='searchControl'>"
			+ "                              <div id='stockistSelectDiv'>"
			+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
			+ "                                   </select>"
			+ "								 </div>"
			+ "                        </div>"
			+ "                    </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                       <label class='form-label'><strong>Date</strong>"
			+ "                       </label>"
			+ "                       <span class='tips'></span>"
			+ "                       <div class='controls'>"
			+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
			+ "                        </div>"
			+ "                   </div>"
			+ "                </div>"
			+ "         </div>"
			+ "                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardViewOrderEntryRegistration()'>Show</button>&nbsp;&nbsp;&nbsp;<button class='btn btn-info m-b-10'  onClick='generateExcelForOutwardGoodsViewOrder()'>Excel</button>"
			+ "                                                            </div>"
			+ "                                                        </div>"

			+ "                                                        <div class='pull-right'>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "     </div>"
			+ "                                                <div class='row'>"
			+ "                                                    <div class='col-md-6'>"
			+ "                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "                                                                <button class='btn btn-info m-b-10'  onClick='generateExcelForOutwardGoodsViewOrderController("+url+")'>Excel</button>"
			+ "                                                            </div>"
			+ "                                                        </div>"

			+ "                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <table class='table table-striped table-hover'>"
			+ "                                                                <thead class='no-bd' id='outList'>"
			+ "                                                                    <tr>"
			+ "                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Order ID</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Order No</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Oranization</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Company</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Date</strong>"
			+ "                                                                        </th>"
			// +" <th style='text-align: center;'><strong>Order Mode</strong>"
			// +" </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Add Product</strong></th>"
			+ "                                                                        <th style='text-align: center;'><strong>Action</strong></th>"
			+ "                                                                    </tr>"
			+ "                                                                </thead>"
			+ "                                                                <tbody class='no-bd-y' id='search'>"
			+ "                                                                </tbody>"
			+ "																	</table>"
			+ "                                                        </div>"
			+ "                                                          <div id='pagination' class='pagination'></div>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                </div>";
	$('#main-content').empty();
	$('#main-content').html(html);

	$("#main-content").addClass("in");
	$(".commonDate").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	$('#organizationSelect').multiselect();
	$('#companySelect').multiselect();
	$('#stockistSelect').multiselect();
	loadOrganizatonsMultiSelectDropDown()
	loadCompaniesMultiSelectDropDown()
	commonOutwardViewOrderEntryRegistrationController("", "", "", "",organizations,companies,stockists,fromDate,toDate, 1);

}
//= = = = = = = = Orders Received End = = = = = = = = //


//= = = = = = = = Invoice Generated Start = = = = = = = = //
//fuction call Invoice Generated view on dashboard with filter values
function invoiceGeneratedCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	invoiceEntryAddedRegistrationListOnDashboard(organizations,companies,stockists,fromDate,toDate) ;
}

//function load Invoice Generated view on dashboard
function invoiceEntryAddedRegistrationListOnDashboard(organizations,companies,stockists,fromDate,toDate) {
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	
	$('#loader').show();
	var i = 1;
	// $.post(contextApplicationPath+'/OutWardGoodsController/getOutWardOrderEntryRegistrationOfProductStatusOneAndInvoiceStatusIsOne',
	// function(data) {
	var html = ""
			+ "                                <div class='tab-pane fade active in' id='tab2_7'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Invoice Entry Registration</strong></h3>"
			+ "                                    </div>"
			+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12'>"
			+ "                                            <div class='panel panel-default'>"
			+ "                                                <div class='panel-body'>"
			/*+ " <div class='col-sm-9 col-sm-offset-3'>"
			+ "       <div class='pull-right' style='padding-left: 10px;'>"
			+ "           <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
			+ "       </div>"
			+ "  </div>"*/
			+ "  <div id='filterToggle'  style='display:none'>"	
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search By</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
			//+ "                                                     		  	<option value='Stockist'>Stockist</option>"
			//+ "																	<option value='Organization'>Select Organization</option>"
			//+ "                                                      		 	<option value='Company'>Comapny Name</option>"
			//+ "                                                     		  	<option value='Date'>Search By Order Date</option>"
			// + " <option value='Stockist'>Stockist</option>"
			+ "                                                     		  	<option value='OrderId'>Order Id</option>"
			+ "																	<option value='OrderNo'> Order Number</option>"
			+ "                                                    			   	<option value='OrderMode'> Order Mode</option>"
			+ "                                                    			   	<option value='InvoiceNumber'> Invoice Number</option>"
			+ "                                                    			   	<option value='InvoiceDate'> Invoice Date</option>"
			+ "                                                    			   	<option value='GrossAmount'> Gross Amount</option>"
			+ "                                                    			   	<option value='NetAmount'> Net Amount</option>"
			+ "																</select>"
			// +" <input type='text' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search For</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <input id='searchText' type='text' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "        <div class='row'>"
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Organization</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='organizationSelectDiv'>"
			+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Company</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='companySelectDiv'>"
			+ "                               <select name='companySelected[]' multiple id='companySelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                        <label class='form-label'><strong>Stockist</strong>"
			+ "                        </label>"
			+ "                        <span class='tips'></span>"
			+ "                        <div class='controls' id='searchControl'>"
			+ "                              <div id='stockistSelectDiv'>"
			+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
			+ "                                   </select>"
			+ "								 </div>"
			+ "                        </div>"
			+ "                    </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                       <label class='form-label'><strong>Date</strong>"
			+ "                       </label>"
			+ "                       <span class='tips'></span>"
			+ "                       <div class='controls'>"
			+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
			+ "                        </div>"
			+ "                   </div>"
			+ "                </div>"
			+ "         </div>"
			+ "                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardInvoiceEntryAddedRegistrationList()'>Show</button>&nbsp;&nbsp;&nbsp;<button class='btn btn-info m-b-10'  onClick='generateExcelForOutwordInvoiceEntryView()' >Excel</button>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='pull-right'>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			
			+ "      </div>"
			+ "                                                <div class='row'>"
			+ "                                                    <div class='col-md-6'>"
			+ "                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "                                                                <button class='btn btn-info m-b-10'  onClick='generateExcelForOutwordInvoiceEntryViewController("+url+")' >Excel</button>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <table class='table table-striped table-hover'>"
			+ "                                                                <thead class='no-bd' id='a'>"
			+ "                                                                    <tr>"
			+ "                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Order ID</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Order No</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Oranization</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Company</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Date</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Order Mode</strong>"
			+ "                                                                        </th>"
			+ "                                                                        <th style='text-align: center;'><strong>Action</strong>"
			+ "                                                                    </tr>"
			+ "                                                                </thead>"
			+ "                                                                <tbody class='no-bd-y' id='selectedRecord'>"
			+ "                                                                </tbody>"
			+ "                                                                </table>"
			+ "                                                        </div>"
			+ "                                               <div id='pagination' class='pagination'></div>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                </div>";
	$('#main-content').empty();
	$('#main-content').html(html);
	$("#main-content").addClass("in");
	$(".commonDate").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	$('#organizationSelect').multiselect();
	$('#companySelect').multiselect();
	$('#stockistSelect').multiselect();
	loadOrganizatonsMultiSelectDropDown()
	loadCompaniesMultiSelectDropDown()
	// showInvoiceEntryAddedRegistrationList(data);
	commonOutwardInvoiceEntryAddedRegistrationListController("", "", "", "",organizations,companies,stockists,fromDate,toDate, 1);
	subTabsForOutWardInvoiceEntryRegistration(false);
	// }, 'json');
}
//= = = = = = = = Invoice Generated End = = = = = = = = //


//= = = = = = = = Cheques Received Start = = = = = = = = //
//fuction call Cheques Received view on dashboard with filter values
function chequesReceivedCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	StockistAdvancedChequeListOnDashboard(organizations,companies,stockists,fromDate,toDate);
}
//function load Cheques Received view on dashboard
function StockistAdvancedChequeListOnDashboard(organizations,companies,stockists,fromDate,toDate)
{
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	
var i=1;
$('#loader').show();
	var html=""
		+ "                                         <div class='tab-pane fade active in' id='tab1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Stockist Advanced Cheque Details</strong></h3>"
		+ "                                    </div>"
		+"<div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
    	/*+ " <div class='col-sm-9 col-sm-offset-3'>"
		+ "       <div class='pull-right' style='padding-left: 10px;'>"
		+ "           <button class='btn btn-danger m-b-10' onclick='toggleFilter()'>Filters+</button>"										
		+ "       </div>"
		+ "  </div>"*/
		+ "  <div id='filterToggle'  style='display:none'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		//+"                                                                        <option value='Organization'>Organization Name</option>"
		//+"                                                                        <option value='Company'>Company</option>"
		+"                                                                        <option value='CompanyBank'>Company Bank</option>"
		//+"                                                                        <option value='Stockist'>Stockist</option>"
		+"                                                                        <option value='StockistBank'>Stockist Bank</option>"
		+"                                                                        <option value='NoOfCheque'>No Of Cheque</option>"
		+"                                                                        <option value='ChequeNo'>Cheque No</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                     </div>"
		+ "        <div class='row'>"
		+ "            <div class='col-md-4'>"
		+ "                 <div class='form-group'>"
		+ "                     <label class='form-label'><strong>Organization</strong>"
		+ "                     </label>"
		+ "                     <span class='tips'></span>"
		+ "                     <div class='controls' id='searchControl'>"
		+ "                          <div id='organizationSelectDiv'>"
		+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
		+ "                               </select>"
		+ "							 </div>"
		+ "                     </div>"
		+ "                  </div>"
		+ "              </div>"
		+ "            <div class='col-md-4'>"
		+ "                 <div class='form-group'>"
		+ "                     <label class='form-label'><strong>Company</strong>"
		+ "                     </label>"
		+ "                     <span class='tips'></span>"
		+ "                     <div class='controls' id='searchControl'>"
		+ "                          <div id='companySelectDiv'>"
		+ "                               <select name='companySelected[]' multiple id='companySelect'>"
		+ "                               </select>"
		+ "							 </div>"
		+ "                     </div>"
		+ "                  </div>"
		+ "              </div>"
		+ "              <div class='col-md-4'>"
		+ "                   <div class='form-group'>"
		+ "                        <label class='form-label'><strong>Stockist</strong>"
		+ "                        </label>"
		+ "                        <span class='tips'></span>"
		+ "                        <div class='controls' id='searchControl'>"
		+ "                              <div id='stockistSelectDiv'>"
		+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
		+ "                                   </select>"
		+ "								 </div>"
		+ "                        </div>"
		+ "                    </div>"
		+ "              </div>"
		+ "              <div class='col-md-4'>"
		+ "                   <div class='form-group'>"
		+ "                       <label class='form-label'><strong>Date</strong>"
		+ "                       </label>"
		+ "                       <span class='tips'></span>"
		+ "                       <div class='controls'>"
		+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
		+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
		+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
		+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
		+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
		+ "                        </div>"
		+ "                   </div>"
		+ "                </div>"

		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchStockistAdvancedCheque(1)'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+ "																   <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onClick='generateExcelForStockistAdvancedCheque()' >Generate Excel </a>"//onClick='generateExcel()'
		+ "                                                            </div>"
		+ "                                                        </div>"
		+"      						</div>"
		+"      </div>"
		+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+ "																   <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onClick='generateExcelForStockistAdvancedChequeController("+url+")' >Generate Excel </a>"//onClick='generateExcel()'
		+ "                                                            </div>"
		+ "                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                 </div>"
		+"                                            </div>";
		$('#main-content').html(html);
		$("#main-content").addClass("in");
		$(".commonDate").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
		$('#organizationSelect').multiselect();
		$('#companySelect').multiselect();
		$('#stockistSelect').multiselect();
		 loadOrganizatonsMultiSelectDropDown();
		 loadCompaniesMultiSelectDropDown();
		subTabsForStockistAdvancedCheque(2);
		commonstockistAdvancedChequeList("","",organizations,companies,stockists,fromDate,toDate,1);
}
//= = = = = = = =Cheques Received End = = = = = = = = //


//= = = = = = = = Cheques Deposited Start = = = = = = = = //
//fuction call Cheques Deposited view on dashboard with filter values
function chequesDepositedCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	StockistChequeDepositEntryListOnDashboard(organizations,companies,stockists,fromDate,toDate);
}

//function load Cheques Deposited view on dashboard
function StockistChequeDepositEntryListOnDashboard(organizations,companies,stockists,fromDate,toDate) {
	
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	
	$('#loader').show();
	var i = 1;

	// $.post(contextApplicationPath+'/BankController/listStockistChequeDepositEntry',
	// function(data) {
	var html = ""
			+ "         <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "               <h3><strong>Stockist Cheque Deposit Entry</strong></h3>"
			+ "         </div>"
			+ "<div class='panel panel-default'>"
			+ "                           <div class='panel-body'>"
		/*	+ "  <div class='col-sm-9 col-sm-offset-3'>"
			+ "       <div class='pull-right' style='padding-left: 10px;'>"
			+ "           <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"
			+ "       </div>"
			+ "  </div>"*/
			+ "  <div id='filterToggle'  style='display:none'>"
			+ "                                                <div class='row'>"
			
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search By</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <select id='searchOption' class='form-control' class='form-control'  onchange='changeFunctionOutwardInvoiceEntry();'>"
			//+ "                                                                        <option value='Organization'>Organization Name</option>"
			//+ "                                                                        <option value='Stockist'>Stockist</option>"
			//+ "                                                                        <option value='Company'>Company</option>"
			+ "                                                                        <option value='CompanyBank'>Company Bank</option>"
			+ "                                                                        <option value='StockistBank'>Stockist Bank</option>"
			+ "                                                                        <option value='ChequeNo'>Cheque Number</option>"
			+ "                                                                        <option value='ChequeAmount'>Cheque Amount</option>"
			+ "                                                                    </select>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Search For</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls' id='searchControl'>"
			+ "                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "        <div class='row'>"
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Organization</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='organizationSelectDiv'>"
			+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Company</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='companySelectDiv'>"
			+ "                               <select name='companySelected[]' multiple id='companySelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                        <label class='form-label'><strong>Stockist</strong>"
			+ "                        </label>"
			+ "                        <span class='tips'></span>"
			+ "                        <div class='controls' id='searchControl'>"
			+ "                              <div id='stockistSelectDiv'>"
			+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
			+ "                                   </select>"
			+ "								 </div>"
			+ "                        </div>"
			+ "                    </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                       <label class='form-label'><strong>Date</strong>"
			+ "                       </label>"
			+ "                       <span class='tips'></span>"
			+ "                       <div class='controls'>"
			+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
			+ "                        </div>"
			+ "                   </div>"
			+ "                </div>"
			+ "         </div>"
			+ "                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right'>"
			+ "                                                                <button class='btn btn-success m-b-10' onclick='searchStockistDepositCheque()'>Show</button>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "																   <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onClick='generateExcelForDepositChequeEntries()' >Generate Excel </a>"//onClick='generateExcel()'
			+ "                                                            </div>"
			+ "                                                        </div>"
	
			+ "    </div>"
			+ "                                                       <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                            <div class='pull-right' style='padding-left: 10px;'>"
			+ "																   <a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onClick='generateExcelForDepositChequeEntriesController("+url+")' >Generate Excel </a>"//onClick='generateExcel()'
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                        </div>"
			+ "                                    					   <div id='pagination' class='pagination'></div>"
			+ "                                             </div>"
			+ "                                       </div>";
	$('#main-content').html(html);
	$("#main-content").addClass("in");
	$(".commonDate").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	$('#organizationSelect').multiselect();
	$('#companySelect').multiselect();
	$('#stockistSelect').multiselect();
	 loadOrganizatonsMultiSelectDropDown()
	 loadCompaniesMultiSelectDropDown();
	commonStockistDepositChequeController("", "", "","",organizations,companies,stockists,fromDate,toDate, 1);
}
//= = = = = = = = Cheques Deposited End = = = = = = = = //


//= = = = = = = = Cheques Returned Start = = = = = = = = //
//fuction call Cheques Returned view on dashboard with filter values
function chequesReturnedCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	stockistChequeReturnEntryOnDashboard(organizations,companies,stockists,fromDate,toDate);
}
//function load Cheques Returned view on dashboard
function stockistChequeReturnEntryOnDashboard(organizations,companies,stockists,fromDate,toDate) {
	
	var temp="";
	var url = "\""
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	
	$('#loader').show();
	var html = "                        <div class='tab-pane fade active in' id='tab3_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>Stockist Cheque Return Entry</strong></h3>"
			+ "                                    </div>"
			+ "      <div class='row'>"
			+ "          <div class='col-md-12'>"
			+ "               <div class='panel panel-default'>"
		/*	+ "   <div class='col-sm-9 col-sm-offset-3'>"  filter view start 
			+ "       <div class='pull-right' style='padding-left: 10px;'>"
			+ "           <button class='btn btn-danger m-b-10' onclick='toggleFilter()'>Filters+</button>"
			+ "       </div>"
			+ "  </div>"*/
			+ "  <div id='filterToggle'  style='display:none'>"
			+ "        <div class='row'>"
			+ "          <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Organization</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='organizationSelectDiv'>"
			+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "            </div>"
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Company</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='companySelectDiv'>"
			+ "                               <select name='companySelected[]' multiple id='companySelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                        <label class='form-label'><strong>Stockist</strong>"
			+ "                        </label>"
			+ "                        <span class='tips'></span>"
			+ "                        <div class='controls' id='searchControl'>"
			+ "                              <div id='stockistSelectDiv'>"
			+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
			+ "                                   </select>"
			+ "								 </div>"
			+ "                        </div>"
			+ "                    </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                       <label class='form-label'><strong>Date</strong>"
			+ "                       </label>"
			+ "                       <span class='tips'></span>"
			+ "                       <div class='controls'>"
			+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
			+ "                        </div>"
			+ "                   </div>"
			+ "                </div>"
			+ "         </div>"
			+ "         <div class='col-sm-9 col-sm-offset-3'>"
			+ "                <div class='pull-right' style='padding-left: 10px;'>"
			+ "                      <button class='btn btn-success m-b-10' onclick='stockistChequeReturnEntryDataLoader()'>Show</button>   <button class='btn btn-info m-b-10'  onClick='generateExcelForDepositEntriesReturn()' >Excel</button>"
			+ "                 </div>"
			+ "         </div>"
			+ "			<div class='pull-right'>"
			+ "         </div>"
			+ "    </div>" /* Filter view End */
		
			+ "                                                            <div class='row'>"
			+ "                                                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "         <div class='col-sm-9 col-sm-offset-3'>"
			+ "                <div class='pull-right' style='padding-left: 10px;'>"
			+ "                     <button class='btn btn-info m-b-10'  onClick='generateExcelForDepositEntriesReturnController("+url+")' >Excel</button>"
			+ "                 </div>"
			+ "         </div>"
			+ "                                                                        <div class='pull-right' style='margin: 10px 10px 0px 0px;'>"
			+ "                                                                            <button type='button' class='btn btn-success m-b-10' onclick='reDeposit()'>Deposited</button>"
			+ "                                                                        </div>"
			+ "                                                                    </div>"
			+ "                                                                    <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                                        <table class='table table-striped table-hover'>"
			+ "                                                                            <thead class='no-bd'>"
			+ "                                                                                <tr>"
			+ "                                                                                  <th style='text-align: center;'><strong>Select</strong>"
			+ "                                                                                  </th>"
			+ "                                                                                  <th style='text-align: center;'><strong>Sr No.</strong>"
			+ "                                                                                  </th>"
			+ "                                                                                  <th style='text-align: center;'><strong>Orgnization</strong>"
			+ "                                                                                  </th>"
			+ "                                                                                  <th style='text-align: center;'><strong>Company</strong>"
			+ "                                                                                  </th>"
			+ "                                                                                  <th style='text-align: center;'><strong>Company Bank</strong>"
			+ "                                                                                  </th>"
			+ "                                                                                  <th style='text-align: center;'><strong>Stockist</strong>"
			+ "                                                                                   </th>                                                    "
			+ "                                                                                  <th style='text-align: center;'><strong>Stockist Bank</strong>"
			+ "                                                                                  </th>  "
			+ "                                                                        			<th style='text-align: center;'><strong>Cheque Number</strong>"
			+ "                                                                        			</th>  "
			+ "                                                                        			<th style='text-align: center;'><strong>Cheque Date</strong>"
			+ "                                                                        			</th>  "
			+ "                                                                       			 <th style='text-align: center;'><strong>Amount</strong>"
			+ "                                                                       			 </th>  "
			+ "                                                                       			 <th style='text-align: center;'><strong>Return Reason</strong>"
			+ "                                                                       			 </th>  "
			+ "                                                                                </tr>"
			+ "                                                                            </thead>"
			+ "                                                                            <tbody class='no-bd-y' id='ChequeReturnEntryTBoady'>";
			+"                                                                            </tbody>"
			+ "                                                                        </table>"
			+ "                                                                    </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "                                                </div>"
			+ "                                            </div>";
	$('#main-content').html(html);
	$("#main-content").addClass("in");
	$(".commonDate").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	$('#organizationSelect').multiselect();
	$('#companySelect').multiselect();
	$('#stockistSelect').multiselect();
	loadOrganizatonsMultiSelectDropDown();
	loadCompaniesMultiSelectDropDown();
	commonstockistChequeReturnEntryDataLoader(organizations,companies,stockists,fromDate,toDate)
	$('#loader').hide();

}
//= = = = = = = = Cheques Returned End = = = = = = = = //


//= = = = = = = = Return Claims Start = = = = = = = = //
//fuction call Return Claims view on dashboard with filter values
function returnCliamsCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	viewInwardReturnGoodsRegListingOnDashboard(organizations,companies,stockists,fromDate,toDate);
}
//function load Return Claims view on dashboard
function viewInwardReturnGoodsRegListingOnDashboard(organizations,companies,stockists,fromDate,toDate){
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	
	$('#loader').show();
	//$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/inwardReturnGoodsRegListing', function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_2'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>Inward Return Goods Registration</strong></h3>"
			+"                                    </div>"
			+"                                    </div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-12'>"
			+ "                        <div class='panel panel-default'>"
			+ "                            <div class='panel-body'>"
			/*+ " <div class='col-sm-9 col-sm-offset-3'>"
			+ "       <div class='pull-right' style='padding-left: 10px;'>"
			+ "           <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
			+ "       </div>"
			+ "  </div>"*/
			+ "  <div id='filterToggle'  style='display:none'>"	
			+ "                                <div class='row'>"
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+ "                                            <label class='form-label'><strong>Search By</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls'>"
			+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFunc();'>"
			//+ "                                                       <option value='stockist'>Stockist</option>"
			//+"														  <option value='Organization'>Select Organization</option>"
			//+ "                                                       <option value='Company'>Comapny Name</option>"
			//+ "                                                       <option value='Date'>Search By Registration Date</option>"
			+ "                                                       <option value='Transporter'>Transporter </option>"
			+"														  <option value='LrNo' >LR No </option>"
			+ "                                                       <option value='ClaimNo'>Claim No </option>"
			+ "                                                       <option value='ClaimAmount'>Claim Amount </option>"
			+ "                                                       <option value='DriverName'>Driver Name</option>"
			+ "                                                       <option value='DeliveredEmployee'>Delivered Employee Name</option>"
			+ "                                                     </select>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"//fromDate toDate searchOption searchText
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group' id='searchForDiv'>"
			+ "                                            <label class='form-label'><strong>Search For</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div id='searchControl'>"
			+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search For'>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                 </div>"
+ "          <div class='row'>"
			
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Organization</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='organizationSelectDiv'>"
			+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Company</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='companySelectDiv'>"
			+ "                               <select name='companySelected[]' multiple id='companySelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                        <label class='form-label'><strong>Stockist</strong>"
			+ "                        </label>"
			+ "                        <span class='tips'></span>"
			+ "                        <div class='controls' id='searchControl'>"
			+ "                              <div id='stockistSelectDiv'>"
			+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
			+ "                                   </select>"
			+ "								 </div>"
			+ "                        </div>"
			+ "                    </div>"
			+ "               </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                       <label class='form-label'><strong>Date</strong>"
			+ "                       </label>"
			+ "                       <span class='tips'></span>"
			+ "                       <div class='controls'>"
			+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
			+ "                        </div>"
			+ "                   </div>"
			+ "                </div>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-success m-b-10' onclick='searchInwardCourierRegistrationDetails1()'>Show</button>&nbsp;&nbsp;&nbsp;<button class='btn btn-info m-b-10'  onClick='generateExcelForInwardReturnsGood()' >Excel</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+"                                    </div>"
			+ "     </div>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-info m-b-10'  onClick='generateExcelForInwardReturnsGoodController("+url+")' >Excel</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                    <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                            </div>"
			+"                                        </div>"
			+ "                                       <div id='pagination' class='pagination'></div>"
		
			+"                                </div>";
		$('#main-content').html(html);
		$("#main-content").addClass("in");
		$(".commonDate").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
		$('#organizationSelect').multiselect();
		$('#companySelect').multiselect();
		$('#stockistSelect').multiselect();
		 loadOrganizatonsMultiSelectDropDown();
		 loadCompaniesMultiSelectDropDown();
		subTabsForInwardReturnGoodsReg(false);
		//showInwardReturnGoodsRegListing(data);
		commonInwardCourierRegistrationDetailsController("", "", "", "",organizations,companies,stockists,fromDate,toDate, 1);

}
//= = = = = = = = Return Claims End = = = = = = = = //


//= = = = = = = = Credit Notes Start = = = = = = = = //
//fuction call Credit Notes view on dashboard with filter values
function creditNotesCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	viewCheckingDoneOnDashboard(organizations,companies,stockists,fromDate,toDate);
}
//function load Credit Notes view on dashboard
function viewCheckingDoneOnDashboard(organizations,companies,stockists,fromDate,toDate)
{
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	$('#loader').show();
    var i=1;
	
	//$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationCheckingController/loadInwardReturnGoodsRegistrationListUsingOrganizationIdWithCheckingDoneStatusOneCreditNoteStatusOne', function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_3'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>Credit Notes</strong></h3>"
			+"                                    </div>"
				+ "                <div class='row'>"
				+ "                    <div class='col-md-12'>"
				+ "                        <div class='panel panel-default'>"
				+ "                            <div class='panel-body'>"
				/*+ "<div class='col-sm-9 col-sm-offset-3'>"
				+ "       <div class='pull-right' style='padding-left: 10px;'>"
				+ "           <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
				+ "       </div>"
				+ "  </div>"*/
				+ "  <div id='filterToggle'  style='display:none'>"	
				+ "                                <div class='row'>"
				+ "                                    <div class='col-md-4'>"
				+ "                                        <div class='form-group'>"
				+ "                                            <label class='form-label'><strong>Search By</strong>"
				+ "                                            </label>"
				+ "                                            <span class='tips'></span>"
				+ "                                            <div class='controls'>"
				+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFunction();'>"
				//+ "                                                       <option value='stockist'>Stockist</option>"
				//+"														  <option value='Organization'>Select Organization</option>"
				//+ "                                                       <option value='Company'>Comapny Name</option>"
				//+ "                                                       <option value='Date'>Search By Registration Date</option>"
				+ "                                                       <option value='Transporter'>Transporter </option>"
				+ "                                                       <option value='CheckingDoneDate'>Search By Checking Done Date</option>"
				+"														  <option value='LrNo' >LR No </option>"
				+ "                                                       <option value='ClaimNo'>Claim No </option>"
				+ "                                                       <option value='ClaimAmount'>Claim Amount </option>"
				+ "                                                       <option value='DriverName'>Driver Name</option>"
				+ "                                                       <option value='DeliveredEmployee'>Delivered Employee Name</option>"
				+ "														  <option value='CreditNoteNo'>Credit Note No</option>"
				+ "														  <option value='CreditNoteAmount'>Credit Note Amount</option>"
				+ "                                                     </select>"
				+ "                                            </div>"
				+ "                                        </div>"
				+ "                                    </div>"//fromDate toDate searchOption searchText
				+ "                                    <div class='col-md-4'>"
				+ "                                        <div class='form-group' id='searchForDiv'>"
				+ "                                            <label class='form-label'><strong>Search For</strong>"
				+ "                                            </label>"
				+ "                                            <span class='tips'></span>"
				+ "                                            <div id='searchControl'>"
				+ "                                                <input id='searchText' type='text' class='form-control'>"
				+ "                                            </div>"
				+ "                                        </div>"
				+ "                                    </div>"
				+ "                               </div>"
				+ "        <div class='row'>"
				
				+ "            <div class='col-md-4'>"
				+ "                 <div class='form-group'>"
				+ "                     <label class='form-label'><strong>Organization</strong>"
				+ "                     </label>"
				+ "                     <span class='tips'></span>"
				+ "                     <div class='controls' id='searchControl'>"
				+ "                          <div id='organizationSelectDiv'>"
				+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
				+ "                               </select>"
				+ "							 </div>"
				+ "                     </div>"
				+ "                  </div>"
				+ "              </div>"
				
				+ "            <div class='col-md-4'>"
				+ "                 <div class='form-group'>"
				+ "                     <label class='form-label'><strong>Company</strong>"
				+ "                     </label>"
				+ "                     <span class='tips'></span>"
				+ "                     <div class='controls' id='searchControl'>"
				+ "                          <div id='companySelectDiv'>"
				+ "                               <select name='companySelected[]' multiple id='companySelect'>"
				+ "                               </select>"
				+ "							 </div>"
				+ "                     </div>"
				+ "                  </div>"
				+ "              </div>"
				+ "              <div class='col-md-4'>"
				+ "                   <div class='form-group'>"
				+ "                        <label class='form-label'><strong>Stockist</strong>"
				+ "                        </label>"
				+ "                        <span class='tips'></span>"
				+ "                        <div class='controls' id='searchControl'>"
				+ "                              <div id='stockistSelectDiv'>"
				+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
				+ "                                   </select>"
				+ "								 </div>"
				+ "                        </div>"
				+ "                    </div>"
				+ "              </div>"
				+ "              <div class='col-md-4'>"
				+ "                   <div class='form-group'>"
				+ "                       <label class='form-label'><strong>Date</strong>"
				+ "                       </label>"
				+ "                       <span class='tips'></span>"
				+ "                       <div class='controls'>"
				+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
				+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
				+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
				+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
				+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
				+ "                        </div>"
				+ "                   </div>"
				+ "                </div>"
				+ "         </div>"
				+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
				+ "                                        <div class='pull-right'>"
				+ "                                            <button class='btn btn-success m-b-10' onclick='searchInwardCourierRegistrationDetailsviewCheckingDone()'>Show</button>&nbsp;&nbsp;&nbsp;<button class='btn btn-info m-b-10'  onClick='generateExcelForCreditsandNotes()' >Excel</button>"
				+ "                                        </div>"
				+ "                                    </div>"
				+ "                              </div>"
				+ "                              </div>"
			+"                                                <div class='row'>"
			+"                                                    <div class='col-md-6'>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-info m-b-10'  onClick='generateExcelForCreditsandNotesController("+url+")' >Excel</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+ "                                       <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			
			+"                                                </div>"
			+"                                            </div>"
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
		$('#main-content').html(html);
		$("#main-content").addClass("in");
		$(".commonDate").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
		$('#organizationSelect').multiselect();
		$('#companySelect').multiselect();
		$('#stockistSelect').multiselect();
		 loadOrganizatonsMultiSelectDropDown();
		 loadCompaniesMultiSelectDropDown();
		commonInwardCourierRegistrationDetailsviewCheckingDoneController("","","","",organizations,companies,stockists,fromDate,toDate,1)
		//showviewCheckingDoneVivew(data);
	//}, 'json');
}
//= = = = = = = = Credit Notes End = = = = = = = = //


//= = = = = = = = Checking Pending Start = = = = = = = = //
//fuction call Checking Pending view on dashboard with filter values
function checkingPendingsCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	checkingDoneOnDashboard(organizations,companies,stockists,fromDate,toDate);
}
//function load Checking Pending view on dashboard
function checkingDoneOnDashboard(organizations,companies,stockists,fromDate,toDate)
{
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	
	$('#loader').show();
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_2'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>Checking Pending</strong></h3>"
			+"                                    </div>"
			+"                                    <div class='row'>"
			+"                                        <div class='col-md-6'>"
			+ "                        <div class='panel panel-default'>"
			+ "                            <div class='panel-body'>"
			/*+ " <div class='col-sm-9 col-sm-offset-3'>"
			+ "       <div class='pull-right' style='padding-left: 10px;'>"
			+ "           <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
			+ "       </div>"
			+ "  </div>"*/
			+ "  <div id='filterToggle'  style='display:none'>"	
			+ "                                <div class='row'>"
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+ "                                            <label class='form-label'><strong>Search By</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls'>"
			+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFunc();'>"
			//+ "                                                       <option value='stockist'>Stockist</option>"
			//+"														  <option value='Organization'>Select Organization</option>"
			//+ "                                                       <option value='Company'>Comapny Name</option>"
			//+ "                                                       <option value='Date'>Search By Registration Date</option>"
			+ "                                                       <option value='Transporter'>Transporter </option>"
			+"														  <option value='LrNo' >LR No </option>"
			+ "                                                       <option value='ClaimNo'>Claim No </option>"
			+ "                                                       <option value='ClaimAmount'>Claim Amount </option>"
			+ "                                                       <option value='DriverName'>Driver Name</option>"
			+ "                                                       <option value='DeliveredEmployee'>Delivered Employee Name</option>"
			+ "                                                     </select>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"//fromDate toDate searchOption searchText
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group' id='searchForDiv'>"
			+ "                                            <label class='form-label'><strong>Search For</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div id='searchControl'>"
			+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search For'>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                  </div>"
			+ "          <div class='row'>"
			
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Organization</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='organizationSelectDiv'>"
			+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Company</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='companySelectDiv'>"
			+ "                               <select name='companySelected[]' multiple id='companySelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                        <label class='form-label'><strong>Stockist</strong>"
			+ "                        </label>"
			+ "                        <span class='tips'></span>"
			+ "                        <div class='controls' id='searchControl'>"
			+ "                              <div id='stockistSelectDiv'>"
			+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
			+ "                                   </select>"
			+ "								 </div>"
			+ "                        </div>"
			+ "                    </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                       <label class='form-label'><strong>Date</strong>"
			+ "                       </label>"
			+ "                       <span class='tips'></span>"
			+ "                       <div class='controls'>"
			+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
			+ "                        </div>"
			+ "                   </div>"
			+ "                </div>"
			+ "         </div>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-success m-b-10' onclick='searchInwardCourierRegistrationDetailscheckingDone()'>Show</button>&nbsp;&nbsp;&nbsp;<button class='btn btn-info m-b-10'  onClick='generateExcelForCheckingPending()' >Excel</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "  </div>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-info m-b-10'  onClick='generateExcelForCheckingPendingController("+url+")' >Excel</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+"                                            <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                            </div>"
			+"                                        </div>"
			+ "                                       <div id='pagination' class='pagination'></div>"
			+"                                    </div>"
			+"                                </div>";
		$('#main-content').html(html);
		$("#main-content").addClass("in");
		$(".commonDate").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
		$('#organizationSelect').multiselect();
		$('#companySelect').multiselect();
		$('#stockistSelect').multiselect();
		 loadOrganizatonsMultiSelectDropDown();
		 loadCompaniesMultiSelectDropDown();
		commanShowCheckingDone("","","","",organizations,companies,stockists,fromDate,toDate,1);
}
//= = = = = = = = Checking Pending End = = = = = = = = //


//= = = = = = = = C N Pendings Start = = = = = = = = //
//fuction call C N Pendings view on dashboard with filter values
function cnPendingsCall(i) {
	var organizations = null;
	var companies = null;
	var stockists = null;
	var fromDate = null;
	var toDate = null;	
	year = $("#yearSelect").val();
	
	if ($('#organizationSelect').val() != null) {
		organizations = $('#organizationSelect').val().toString();
	} else {
		organizations ="";
	}

	if ($('#companySelect').val() != null) {
		companies = $('#companySelect').val().toString();
	} else {
		companies ="";
	}
	if ($('#stockistSelect').val() != null) {
		stockists = $('#stockistSelect').val().toString();
	} else {
		stockists ="";
	}
	
	fromDate=getStartDate(i,year);
	toDate=getEndDate(i,year);
	
	AddCreditNoteOnDashboard(organizations,companies,stockists,fromDate,toDate);
}
//function load C N Pendings view on dashboard
function AddCreditNoteOnDashboard(organizations,companies,stockists,fromDate,toDate)
{
	var temp="";
	var url = "\""+temp+ "\",\""+temp+ "\",\"" + temp + "\",\""+ temp + "\",\"" 
	+ organizations + "\",\"" + companies + "\",\"" + stockists + "\",\"" + fromDate + "\",\"" + toDate + "\"";
	
	$('#loader').show();
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_3'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>C N Pendings</strong></h3>"
			+"                                    </div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-12'>"
			+ "                        <div class='panel panel-default'>"
			+ "                            <div class='panel-body'>"
		/*	+ " <div class='col-sm-9 col-sm-offset-3'>"
			+ "       <div class='pull-right' style='padding-left: 10px;'>"
			+ "           <button class='btn btn-danger m-b-10' onclick='toggle()'>Filters+</button>"										
			+ "       </div>"
			+ "  </div>"*/
			+ "  <div id='filterToggle'  style='display:none'>"
			+ "                                <div class='row'>"
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+ "                                            <label class='form-label'><strong>Search By</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls'>"
			+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFunc();'>"
			//+ "                                                       <option value='stockist'>Stockist</option>"
			//+"														  <option value='Organization'>Select Organization</option>"
			//+ "                                                       <option value='Company'>Comapny Name</option>"
			//+ "                                                       <option value='Date'>Search By Registration Date</option>"
			+ "                                                       <option value='Transporter'>Transporter </option>"
			+ "                                                       <option value='CheckingDoneDate'>Search By Checking Done Date</option>"
			+"														  <option value='LrNo' >LR No </option>"
			+ "                                                       <option value='ClaimNo'>Claim No </option>"
			+ "                                                       <option value='ClaimAmount'>Claim Amount </option>"
			+ "                                                       <option value='DriverName'>Driver Name</option>"
			+ "                                                       <option value='DeliveredEmployee'>Delivered Employee Name</option>"
			+ "                                                     </select>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"//fromDate toDate searchOption searchText
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group' id='searchForDiv'>"
			+ "                                            <label class='form-label'><strong>Search For</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div id='searchControl'>"
			+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search For'>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                         </div>"
			+ "        <div class='row'>"
			
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Organization</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='organizationSelectDiv'>"
			+ "                               <select name='organizationSelected[]' multiple id='organizationSelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			
			+ "            <div class='col-md-4'>"
			+ "                 <div class='form-group'>"
			+ "                     <label class='form-label'><strong>Company</strong>"
			+ "                     </label>"
			+ "                     <span class='tips'></span>"
			+ "                     <div class='controls' id='searchControl'>"
			+ "                          <div id='companySelectDiv'>"
			+ "                               <select name='companySelected[]' multiple id='companySelect'>"
			+ "                               </select>"
			+ "							 </div>"
			+ "                     </div>"
			+ "                  </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                        <label class='form-label'><strong>Stockist</strong>"
			+ "                        </label>"
			+ "                        <span class='tips'></span>"
			+ "                        <div class='controls' id='searchControl'>"
			+ "                              <div id='stockistSelectDiv'>"
			+ "                                   <select name='stockistSelected[]' multiple id='stockistSelect' >"
			+ "                                   </select>"
			+ "								 </div>"
			+ "                        </div>"
			+ "                    </div>"
			+ "              </div>"
			+ "              <div class='col-md-4'>"
			+ "                   <div class='form-group'>"
			+ "                       <label class='form-label'><strong>Date</strong>"
			+ "                       </label>"
			+ "                       <span class='tips'></span>"
			+ "                       <div class='controls'>"
			+ "                             <input type='text' id='fromDateNew' class='commonDate form-control' type='text' placeholder='From' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputFromDate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "                             <input type='text' id='toDateNew' class='commonDate form-control' type='text' placeholder='To' style='width: 49%; height: 36px; padding-left: 10px;' parsley-type='dateIso' onClick = \"keyUpSampleToAndFrom('#inputTODate_Error_Msg','#greterFromDate_Error_Msg','#form_Error_Msg')\">"
			+ "							    <div id='greterFromDate_Error_Msg' class='validationError' style='width: 98%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
			+ "							    <div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>"
			+ "                        </div>"
			+ "                   </div>"
			+ "                </div>"
			+ "         </div>"
			+ "			<div class='pull-right'>"
			+ "         </div>"
		
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-success m-b-10' onclick='searchInwardCourierRegistrationDetailsAddCreditNote()'>Show</button>&nbsp;&nbsp;&nbsp;<button class='btn btn-info m-b-10'  onClick='generateExcelForCheckingDone()' >Excel</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+"   </div>"
			+"  								</div>"
			+"                                                        <div class='pull-right'>"
			+"                                                                                            <div class='row'>"
			+"                                                                                                <div class='col-md-3'>"
			+"                                                                                                    <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive5' onclick='addCreditNoteForm()'>Add Credit Note</button>"
			+"																										<div id='creditNoteForm'></div>"
			+"                                                                                                </div>"
			+"                                                                                            </div>"
			+"                                                        </div>"
			//+"  								</div>"
			+"                                                <div class='row'>"
			+"                                                    <div class='col-md-6'>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-info m-b-10'  onClick='generateExcelForCheckingDoneController("+url+")' >Excel</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                        </div>"
			+ "                                       		      <div id='pagination' class='pagination'></div>"
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>"
			+"                                        </div>"
			+"                                    </div>"
			+"                                </div>";
		$('#main-content').html(html);
		$("#main-content").addClass("in");
		$(".commonDate").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
		$('#organizationSelect').multiselect();
		$('#companySelect').multiselect();
		$('#stockistSelect').multiselect();
		 loadOrganizatonsMultiSelectDropDown();
		 loadCompaniesMultiSelectDropDown();
		commanCheckingDone("","","","",organizations,companies,stockists,fromDate,toDate,1);
}
//= = = = = = = = C N Pendings End = = = = = = = = //


//= = = = = = = = Common Functions Start = = = = = = = = //
//common function to set start date from month and year
function getStartDate(i,year) {
	var yr=parseInt(year);
	yr++;
	switch(i) {
	
	case 1: return "01/01/"+yr ;
	
	case 2: return "02/01/"+yr ;
	
	case 3: return "03/01/"+yr ;
	
	case 4: return "04/01/"+year ;
	
	case 5: return "05/01/"+year ;
	
	case 6: return "06/01/"+year ;
	
	case 7: return "07/01/"+year ;
	
	case 8: return "08/01/"+year ;
	
	case 9: return "09/01/"+year ;
	
	case 10: return "10/01/"+year ;
	
	case 11: return "11/01/"+year ;
	
	case 12: return "12/01/"+year ;
	
	}
	return "04/01/"+year ;
}

//common function to set end date from month and year
function getEndDate(i,year) {
	var yr=parseInt(year);
	yr++;
	switch(i) {
	
	case 1: return "01/31/"+yr ;
	
	case 2: if(yr % 4 == 0){
				return "02/29/"+yr ;
			}
			return "02/28/"+yr ;
			
	case 3: return "03/31/"+yr ;
	
	case 4: return "04/30/"+year ;
	
	case 5: return "05/31/"+year ;
	
	case 6: return "06/30/"+year ;
	
	case 7: return "07/31/"+year ;
	
	case 8: return "08/31/"+year ;
	
	case 9: return "09/30/"+year ;
	
	case 10: return "10/31/"+year ;
	
	case 11: return "11/30/"+year ;
	
	case 12: return "12/31/"+year ;
	}
	return "03/31/"+yr ;
}
//= = = = = = = = Common Functions End = = = = = = = = //