function validateProductField(errorMsgId,txtId,counter){
	if($('#'+txtId+counter).val() == '' )
	{
		$('#'+errorMsgId+counter).html("This field is required...");
		return false;
	}
	return true;
}
function lrNoVal(lrFormCounter)
{
	if($('#lrNoVal'+lrFormCounter+'').val() == '' )
	{
		//alert("In valid : "+$('#lrNoVal'+lrFormCounter+'').val());
		$('#lr_No_Error_Msg'+lrFormCounter+'').html("Please Enter LR No");
		return false;
	}
	return true;
}

function noOfcases(lrFormCounter)
{
	if($('#noOfCasesVal'+lrFormCounter+'').val() == '' )
	{
		$('#noOfCases_Error_Msg'+lrFormCounter+'').html("Please Enter No Of Cases");
		return false;
	}
	var num3 = /^[0-9,.]+$/
		if(!num3.test($('#noOfCasesVal'+lrFormCounter+'').val())) {
			$('#noOfCases_Error_Msg'+lrFormCounter+'').html("Error: Please input numeric value only");
			 return false;
		}
	return true;
}
function refOrClaimNo(lrFormCounter)
{
	if($('#refOrClainNoVal'+lrFormCounter+'').val() == '' )
	{
		$('#refOrClainNoVal_Error_Msg'+lrFormCounter+'').html("Please Enter Ref No/Claim No");
		return false;
	}
	return true;
}
function claimAmount(lrFormCounter)
{
	if($('#claimAmountVal'+lrFormCounter+'').val() == '' )
	{
		$('#claimAmountVal_Error_Msg'+lrFormCounter+'').html("Please Enter Claim Amount");
		return false;
	}
	var num3 = /^[0-9,.]+$/
		if(!num3.test($('#claimAmountVal'+lrFormCounter+'').val())) {
			$('#claimAmountVal_Error_Msg'+lrFormCounter+'').html("Error: Please input numeric value only");
			 return false;
}	return true;
}
function attachLr(lrFormCounter) 
{
	var counter = $('#lrFileAttachedCountId'+lrFormCounter).val();
	if(!(counter>0)){
		$('#attachLr_Error_Msg'+lrFormCounter+'').html("Please Select Atleast One LR File");
		return false;
	}
	return true;
}
function attachClaimCopy(lrFormCounter)
{
	var counter = $('#claimCopyFileAttachedCountId'+lrFormCounter).val();
	//alert("counter : "+counter);
	if(!(counter>0)){
		$('#attachClaimCopy_Error_Msg'+lrFormCounter+'').html("Please Select  Atleast One Claim Copy File");
		return false;
	}
	/*if($('#attachClaimCopy'+lrFormCounter+'_1').val() == '' )
	{
	$('#attachClaimCopy_Error_Msg'+lrFormCounter+'').html("Please Select Claim Copy File");
	return false;
	}*/
	return true;
}

function lrNoValKeyUp(lrFormCounter)
{
	$('#lr_No_Error_Msg'+lrFormCounter+'').html("");
}
function noOfcasesKeyUp(lrFormCounter)
{
	$('#noOfCases_Error_Msg'+lrFormCounter+'').html("");
}
function refOrClaimNoKeyUp(lrFormCounter)
{
	$('#refOrClainNoVal_Error_Msg'+lrFormCounter+'').html("");
}
function claimAmountKeyUp(lrFormCounter)
{
	$('#claimAmountVal_Error_Msg'+lrFormCounter+'').html("");
}
function attachLrKeyUp(lrFormCounter)
{
	$('#attachLr_Error_Msg'+lrFormCounter+'').html("");
}
function attachClaimCopyKeyUp(lrFormCounter)
{
	$('#attachClaimCopy_Error_Msg'+lrFormCounter+'').html("");
}

/********************************************inward.js*******************************************/

function stnNo(Divid)
{
	if($('#stnNo'+Divid+'').val() == '' )
	{
		$('#stnNo_Error_Msg'+Divid+'').html("Please Enter STN No");
		return false;
	}
	return true;
}
function stnImagePath(Divid)    //invoiceFileAttachedCountId
{
	var counter = $('#invoiceFileAttachedCountId'+Divid+'').val();
	if(!(counter>0)){
		$('#stnImagePath_Error_Msg'+Divid+'').html("Please Select Atleast One File");
		return false;
	}
	return true;
}
function stnDate(Divid)
{
	if($('#stnDate'+Divid+'').val() == '' )
	{
		$('#stnDate_Error_Msg'+Divid+'').html("Please Enter STN Date");
		return false;
	}
	return true;
}
function amount(Divid)
{
	if($('#amount'+Divid+'').val() == '' )
	{
		$('#amount_Error_Msg'+Divid+'').html("Please Enter Gross Amount");
		return false;
	}
	
	var num = /^[0-9,.]+$/
		if(!num.test($('#amount'+Divid+'').val())) {
			$('#amount_Error_Msg'+Divid+'').html("Error: Please input numeric value only");
			 return false;
		}
	return true;
}
function vatAmount(Divid)
{
	if($('#vatAmount'+Divid+'').val() == '' )
	{
		$('#vatAmount_Error_Msg'+Divid+'').html("Please Enter Vat Amount");
		return false;
	}
	var num = /^[0-9,.]+$/;
    if(!num.test($('#vatAmount'+Divid+'').val())) {
    	$('#vatAmount_Error_Msg'+Divid+'').html("Error: Please input numeric value only");
  
      return false;
    }
    return true;
}
function discountAmount(Divid)
{
	if($('#discountAmount'+Divid+'').val() == '' )
	{
		$('#discountAmount_Error_Msg'+Divid+'').html("Please Enter Discount Amount");
		return false;
	}
	var num = /^[0-9,.]+$/
	if(!num.test($('#discountAmount'+Divid+'').val())) {
		$('#discountAmount_Error_Msg'+Divid+'').html("Error: Please input numeric value only");
		 return false;
	}
	return true;
}
function netAmount(Divid)
{
	if($('#netAmount'+Divid+'').val() == '' )
	{
		$('#netAmount_Error_Msg'+Divid+'').html("Please Enter Net Amount");
		return false;
	}
	var num = /^[0-9,.]+$/
		if(!num.test($('#netAmount'+Divid+'').val())) {
			$('#netAmount_Error_Msg'+Divid+'').html("Error: Please input numeric value only");
			return false;
		}
	return true;
}
function stnNoKeyUp(Divid)
{
	$('#stnNo_Error_Msg'+Divid+'').html("");
}
function stnImagePathKeyUp(Divid)
{
	$('#stnImagePath_Error_Msg'+Divid+'').html("");
}
function stnDateKeyUp(Divid)
{
	$('#stnDate_Error_Msg'+Divid+'').html("");
}
function amountKeyUp(Divid)
{
	$('#amount_Error_Msg'+Divid+'').html("");
}
function vatAmountKeyUp(Divid)
{
	$('#vatAmount_Error_Msg'+Divid+'').html("");
}
function discountAmountKeyUp(Divid)
{
	$('#discountAmount_Error_Msg'+Divid+'').html("");
}
function netAmountKeyUp(Divid)
{
	$('#netAmount_Error_Msg'+Divid+'').html("");
}

/*****************************************************************************/

function invoiceNoId(INVOICE_DIV)
{
	if($('#invoiceNoId'+INVOICE_DIV+'').val() == '' )
	{
		$('#invoiceNoId_Error_Msg'+INVOICE_DIV+'').html("Please Enter Invoice No");
		return false;
	}
	return true;
}
function invoiceDate(INVOICE_DIV)
{
	if($('#invoiceDate'+INVOICE_DIV+'').val() == '' )
	{
		$('#invoiceDate_Error_Msg'+INVOICE_DIV+'').html("Please Enter Invoice Date");
		return false;
	}
	return true;
}
function netPayableAmt(INVOICE_DIV)
{
	if($('#netAmountId'+INVOICE_DIV+'').val() == '' )
	{
		$('#netAmountId_Error_Msg'+INVOICE_DIV+'').html("Please Enter Net Payable amount");
		return false;
	}
	return true;
}
function grossAmountId(INVOICE_DIV)
{
	if($('#grossAmountId'+INVOICE_DIV+'').val() == '' )
	{
		$('#grossAmountId_Error_Msg'+INVOICE_DIV+'').html("Please Enter Gross Amount");
		return false;
	}
	validateGrossAmount(INVOICE_DIV);
	return true;
}
function validateGrossAmount(INVOICE_DIV)
{
	if($('#grossAmountId'+INVOICE_DIV+'').val())
	{
	    if(!numberReg.test($('#grossAmountId'+INVOICE_DIV+'').val()))
	    {
	    	$('#grossAmountId_Error_Msg'+INVOICE_DIV+'').html("This value should be a valid number.");
	    	return false;
	    }
	}
	return true;
}
function taxAmountId(INVOICE_DIV)
{
	if($('#taxAmountId'+INVOICE_DIV+'').val() == '' )
	{
		$('#taxAmountId_Error_Msg'+INVOICE_DIV+'').html("Please Enter Tax Amount");
		return false;
	}
	validateTaxAmount(INVOICE_DIV);
	return true;
}
function validateTaxAmount(INVOICE_DIV)
{
	if($('#taxAmountId'+INVOICE_DIV+'').val())
	   {
		    if(!numberReg.test($('#taxAmountId'+INVOICE_DIV+'').val()))
		    {
		    	$('#taxAmountId_Error_Msg'+INVOICE_DIV+'').html("This value should be a valid number.");
		    	return false;
		    }
	   }
	return true;
}
function vatAmountId(INVOICE_DIV)
{
	if($('#vatAmountId'+INVOICE_DIV+'').val() == '' )
	{
	$('#vatAmountId_Error_Msg'+INVOICE_DIV+'').html("Please Enter Vat Amount");
	return false;
	}
	validateVatAmount(INVOICE_DIV);
	return true;
}
function validateVatAmount(INVOICE_DIV)
{
	if($('#vatAmountId'+INVOICE_DIV+'').val())
	   {
		    if(!numberReg.test($('#vatAmountId'+INVOICE_DIV+'').val()))
		    {
		    	$('#vatAmountId_Error_Msg'+INVOICE_DIV+'').html("This value should be a valid number.");
		    	return false;
		    }
	   }
	return true;
}
function discountO(INVOICE_DIV)
{
 if(!($('input[id=onclickyes'+INVOICE_DIV+']:checked').val()) && !($('input[id=onclickno'+INVOICE_DIV+']:checked').val()))
	{
		$('#discount_Error_Msg'+INVOICE_DIV+'').html("Please Select Discount");
		return false;
	}
	return true;
}

function discountONetAmountBlank(INVOICE_DIV)
{
	$('#onclickyes'+INVOICE_DIV+'').prop( 'checked', false );
	$('#onclickno'+INVOICE_DIV+'').prop('checked', false );
	$('#netAmountId'+INVOICE_DIV+'').val('');
	if($('#vatAmountId'+INVOICE_DIV+'').val() || $('#taxAmountId'+INVOICE_DIV+'').val() || $('#grossAmountId'+INVOICE_DIV+'').val())
		{
			validateGrossAmount(INVOICE_DIV);
			validateTaxAmount(INVOICE_DIV);
			validateVatAmount(INVOICE_DIV);
		}
}
function invoiceNoIdKeyUp(Divid)
{
	$('#invoiceNoId_Error_Msg'+INVOICE_DIV+'').html("");
}
function invoiceDateKeyUp(INVOICE_DIV)
{
	$('#invoiceDate_Error_Msg'+INVOICE_DIV+'').html("");
}
function grossAmountIdKeyUp(INVOICE_DIV)
{
	$('#grossAmountId_Error_Msg'+INVOICE_DIV+'').html("");
	discountONetAmountBlank(INVOICE_DIV);
}
function taxAmountIdKeyUp(INVOICE_DIV)
{
	$('#taxAmountId_Error_Msg'+INVOICE_DIV+'').html("");
	discountONetAmountBlank(INVOICE_DIV);
}
function vatAmountIdKeyUp(INVOICE_DIV)
{
	$('#vatAmountId_Error_Msg'+INVOICE_DIV+'').html("");
	discountONetAmountBlank(INVOICE_DIV);
}
function invoiceDiscountKeyUp(INVOICE_DIV)
{
	$('#discount_Error_Msg'+INVOICE_DIV+'').html("");
}
function netAmountIdKeyUp(INVOICE_DIV)
{
	$('#netAmountId_Error_Msg'+INVOICE_DIV+'').html("");
}
/**************************************OutstandingChequeDepositEntry************************************/

function invoiceDate1()
{
//	alert("In function InvoiceDate : ");
	if($('#inputFromDate').val() == '')
	{
		
	$('#inputFromDate_Error_Msg').html("Please Enter From Date");
	return false;
	}
	if( $('#inputTODate').val() == '')
		{
		$('#inputTODate_Error_Msg').html("Please Enter To Date");
		return false;
		}
//	validateGrossAmount(INVOICE_DIV);
	return true;
}

function GrossAmountAndNetAmount()
{
	if($('#inputFromData3').val() == '')
		{
		$('#inputFromData3_Error_Msg').html("Please Enter From Amount");
		return false;
		}
		if($('#inputTOData3').val() == '')
			{
			$('#inputTOData3_Error_Msg').html("Please Enter To Amount");
			return false;
			}
//		validateGrossAmount(INVOICE_DIV);
		return true;
		}
function organization()
{
	if($('#searchText').val() == '')
	{
	$('#searchText_Error_Msg').html("Please Enter Text For Search");
	return false;
	}
	return true;
}

function organizationKeyUp()
{
	$('#searchText_Error_Msg').html("");
}

function grossAmountKeyUp()
{
	$('#inputFromData3_Error_Msg').html("");
	$('#inputTOData3_Error_Msg').html("");
}
function invoiceDateKeyUp1()
{
	$('#inputFromDate_Error_Msg').html("");
	$('#inputTODate_Error_Msg').html("");
}


/********************* Inward Return : Add Product************************************/

function validateProductName(addProdPopUpCounter,counter)
{
	if($('#productName'+addProdPopUpCounter+'_'+counter+'').val() == '' )
	{
		$('#productName_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Please Enter Project Name");
		return false;
	}
	return true;
}

function productNameKeyUp(addProdPopUpCounter,counter)
{
	$('#productName_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("");
}

function validateClaimQuantity(addProdPopUpCounter,counter)
{
	if($('#claimQuantity'+addProdPopUpCounter+'_'+counter+'').val() == '' )
	{
		$('#claimQuantity_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Please Enter Claim Quantity");
		return false;
	}
	var num = /^[0-9,.]+$/
		if(!num.test($('#claimQuantity'+addProdPopUpCounter+'_'+counter+'').val())) {
			$('#claimQuantity_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Error: Please input numeric value only");
			 return false;
		}
	return true;
}
function claimQuantityKeyUp(addProdPopUpCounter,counter)
{
	$('#claimQuantity_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("");
}

function validateReceivedQuantity(addProdPopUpCounter,counter)
{
	if($('#receivedQuantity'+addProdPopUpCounter+'_'+counter+'').val() == '' )
	{
		$('#receivedQuantity_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Please Enter Received Quantity");
		return false;
	}
	var num = /^[0-9,.]+$/
		if(!num.test($('#receivedQuantity'+addProdPopUpCounter+'_'+counter+'').val())) {
			$('#receivedQuantity_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Error: Please input numeric value only");
			 return false;
		}
	return true;
}

function receivedQuantityKeyUp(addProdPopUpCounter,counter)
{
	$('#receivedQuantity_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("");
}



function validateBatch(addProdPopUpCounter,counter)
{
	if($('#batch'+addProdPopUpCounter+'_'+counter+'').val() == '' )
	{
		$('#batch_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Please Enter Batch");
		return false;
	}
	return true;
}

function batchKeyUp(addProdPopUpCounter,counter)
{
	$('#batch_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("");
}



function validateMFGDate(addProdPopUpCounter,counter)
{
	if($('#mfgDate'+addProdPopUpCounter+'_'+counter+'').val() == '' )
	{
		$('#mfgDate_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Please Enter MFG Date");
		return false;
	}
	return true;
}

function mfgDateKeyUp(addProdPopUpCounter,counter)
{
	$('#mfgDate_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("");
}

function validateExpiryDate(addProdPopUpCounter,counter)
{
	if($('#expiryDate'+addProdPopUpCounter+'_'+counter+'').val() == '' )
	{
		$('#expiryDate_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Please Enter Expiry Date");
		return false;
	}
	
return true;
}
function expiryDateKeyUp(addProdPopUpCounter,counter)
{
	$('#expiryDate_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("");
}

function validateMFGCompany(addProdPopUpCounter,counter)
{
	if($('#mfgCompany'+addProdPopUpCounter+'_'+counter+'').val() == '' )
	{
		$('#mfgCompany_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Please Enter MFG Company");
		return false;
	}
	return true;
}

function mfgCompanyKeyUp(addProdPopUpCounter,counter)
{
	$('#mfgCompany_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("");
}

function validateReason(addProdPopUpCounter,counter)
{
	if($('#reason'+addProdPopUpCounter+'_'+counter+'').val() == '' )
	{
		$('#reason_Error_msg'+addProdPopUpCounter+'_'+counter+'').html("Please Enter Reason");
		return false;
	}
	return true;
}

function reasonKeyUp(addProdPopUpCounter,counter)
{
	$('#reason_Error_msg'+addProdPopUpCounter+'_'+counter+'').html("");
}

function validateQuantityVarience(addProdPopUpCounter,counter)
{
	if($('#quantityVariance'+addProdPopUpCounter+'_'+counter+'').val() == '' )
	{
		$('#quantityVariance_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Please Enter Quantity Varience");
		return false;
	}
	var num = /^[0-9,.]+$/
		if(!num.test($('#quantityVariance'+addProdPopUpCounter+'_'+counter+'').val())) {
			$('#quantityVariance_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("Error: Please input numeric value only");
			 return false;
		}
	return true;
}

function quantityVarianceKeyUp(addProdPopUpCounter,counter)
{
	$('#quantityVariance_Error_Msg'+addProdPopUpCounter+'_'+counter+'').html("");
}


function validateProductName1(counter)
{
	if($('#productName'+counter+'').val() == '' )
	{
		$('#productName_Error_Msg'+counter+'').html("Please Enter Project Name");
		return false;
	}
	return true;
}

function productNameKeyUp1(counter)
{
	$('#productName_Error_Msg'+counter+'').html("");
}

function validateClaimQuantity1(counter)
{
	if($('#claimQuantity'+counter+'').val() == '' )
	{
		$('#claimQuantity_Error_Msg'+counter+'').html("Please Enter Claim Quantity");
		return false;
	}
}
function claimQuantityKeyUp1(counter)
{
	$('#claimQuantity_Error_Msg'+counter+'').html("");
}

function validateReceivedQuantity1(counter)
{
	if($('#receivedQuantity'+counter+'').val() == '' )
	{
		$('#receivedQuantity_Error_Msg'+counter+'').html("Please Enter Received Quantity");
		return false;
	}
	return true;
}

function receivedQuantityKeyUp1(counter)
{
	$('#receivedQuantity_Error_Msg'+counter+'').html("");
}


function validateBatch1(counter)
{
	if($('#batch'+counter+'').val() == '' )
	{
		$('#batch_Error_Msg'+counter+'').html("Please Enter Batch");
		return false;
	}
	return true;
}

function batchKeyUp1(counter)
{
	$('#batch_Error_Msg'+counter+'').html("");
}


function validateMFGDate1(counter)
{
	if($('#mfgDate'+counter+'').val() == '' )
	{
		$('#mfgDate_Error_Msg'+counter+'').html("Please Enter MFG Date");
		return false;
	}
	return true;
}

function mfgDateKeyUp1(counter)
{
	$('#mfgDate_Error_Msg'+counter+'').html("");
}

function validateExpiryDate1(counter)
{
	if($('#expiryDate'+counter+'').val() == '' )
	{
		$('#expiryDate_Error_Msg'+counter+'').html("Please Enter Expiry Date");
		return false;
	}
	if($('#expiryDate'+counter+'').val() !== '' )
	{
	$('#expiryDate_Error_Msg'+counter+'').html(" ");
return false;
} return true;
}

function expiryDateKeyUp1(counter)
{
	$('#expiryDate_Error_Msg'+counter+'').html("");
}

function data(counter)
{
	if($('#mfgDate').val()>=$('#expiryDate').val())
	{
		alert('Seleted TO Date should be greater than from Date');
	}
	return true;
}

function validateMFGCompany1(counter)
{
	if($('#mfgCompany'+counter+'').val() == '' )
	{
		$('#mfgCompany_Error_Msg'+counter+'').html("Please Enter MFG Company");
		return false;
	}
	return true;
}

function mfgCompanyKeyUp1(counter)
{
	$('#mfgCompany_Error_Msg'+counter+'').html("");
	
}

function validateReason1(counter)
{
	if($('#reason'+counter+'').val() == '' )
	{
		$('#reason_Error_msg'+counter+'').html("Please Enter Reason");
		return false;
	}
	return true;
}

function reasonKeyUp1(counter)
{
	$('#reason_Error_msg'+counter+'').html("");
}

function validateQuantityVarience1(counter)
{
	if($('#quantityVariance'+counter+'').val() == '' )
	{
		$('#quantityVariance_Error_Msg'+counter+'').html("Please Enter Quantity Varience");
		return false;
	}
	return true;
}

function quantityVarianceKeyUp1(counter)
{
	$('#quantityVariance_Error_Msg'+counter+'').html("");
}




function validateProductName3(dateid)
{
	if($('#receivedDate'+dateid+'').val() == '' )
	{
		$('#recDateId_Error_Msg'+dateid+'').html("Please Enter Project Name");
		return false;
	}
	return true;
}

function productNameKeyUp3(dateid)
{
	$('#recDateId_Error_Msg'+dateid+'').html("");
}

function errorMsgEmpty(controlId, errorDivId)
{//alert("errorMsgEmpty");
	if($('#'+controlId).val()!==""){
		$('#'+errorDivId).html("");}
}

//************************** Outward return **************************/timepicki-tim

//function attachOutwardClaimCopy(lrFormCounter)
//{
//	var counter = $('#claimCopyFileAttachedCountId'+lrFormCounter+'').val();
//	if(!(counter>0)){
//		$('#attachClaimCopy_Error_Msg'+lrFormCounter+'').html("Please Select  Atleast One Claim Copy File");
//		return false;
//	}
//	return true;
//}
//function attachOutwardClaimCopy(lrFormCounter)
//{
//	var counter = $('#claimCopyFileAttachedCountId'+lrFormCounter+'').val();
//	if(!(counter>0)){
//		$('#attachClaimCopy_Error_Msg'+lrFormCounter+'').html("Please Select  Atleast One Claim Copy File");
//		return false;
//	}
//	return true;
//}