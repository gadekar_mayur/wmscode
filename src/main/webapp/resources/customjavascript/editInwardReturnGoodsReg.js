function editInwardReturnGoodsReg(mainIndex,inwardRetGoodsRegId){
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/inwardReturnGoodsRegDetailsAndLrListing', {
		ID_NO : inwardRetGoodsRegId 
			} , function(data) {
				var i =1;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Inward Return Goods Registration Details</strong></h4>"
			        + "            </div>"
			        + "				 <form id='editInwardReturnGoodsReg' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationController/editInwardReturnGoodsRegistration' method='post'>"
			        + "			       <div class='panel-body'>"
			        + "    			       <div class='row'>"
			        + "       				   								<div class='col-md-4'>"
					+"            												 <div id='selectOrganization' class='form-group'>"
					+"                                                                <label class='form-label'><strong>Select Organization : - </strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>COMPANY NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select id='companyName' name='companyName' onchange='getStockist()' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Company</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>STOCKIST NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select id='StockistName' name='stockistName' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Stockist</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTER NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select name='transporter' id='transName' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Transporter</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DELIVERED EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select id='employeeDropDown' name='employeeName' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Employee</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                   <input class='dateClassCommon form-control' value='"+data.REG_DATE+"' type='text' name='regDate' placeholder='Select Date' style='height: 36px; padding-left: 10px;' required>"
					+"                                                                </div>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>" 
					+"                                                                  <input type='text'  id='timePicker' name='time' value='"+data.TIME+"' placeholder='Time' class='form-control' required>"
					+"                                                                </div>"
					+"                                                                </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='driverNo' value= '"+data.DRIVER_NO+"' placeholder='DRIVER NO' class='form-control' >"
					+"                                                                </div>"
					+"                                                            </div>                                             "            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NAME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='driverName' value='"+data.DRIVER+"' placeholder='DRIVER NAME ' class='form-control' >"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTATION CHARGES : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='transpCharges' value='"+data.TRANSPORTATION_CHARGES+"' placeholder='TRANSPORTATION CHARGES' class='form-control' parsley-type='number' >"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            </div>"
					+ "						 </div>"
					+"						 <input type='hidden' value='"+data.ID_NO+"' name='regId'>"
					+"                       <div class='modal-footer'>"
					+"                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editInwardReturnGoodsReg\")'>Update</button>"
					+"                           <button type='button' onClick='hidePopUp()' class='btn btn-success'>Cancel</button>"                                                                               
					+"                       </div>"
					+ " 			    </div>"
					+"				</form>"	
					+"						<div class='row'>"
					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>LR No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>No of Cases</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Amount</strong>"
					+"											</th>      "
					/*+"											<th style='text-align: center;'><strong>LR Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Copy</strong>"
					+"											</th>"*/
					+"											<th style='text-align: center;'><strong>Action</strong>"
					+"											</th>"
					+"										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
				var irgrLRArr = data.irgrLRArr;
					$(irgrLRArr).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ i +"</td>"
							+"                                         <td id='LR_NO_"+i+"'>"+element.LR_NO+"</td>"
							+"                                         <td id='NO_OF_CASES_"+i+"'>"+element.NO_OF_CASES+"</td>"
							+"                                         <td id='REF_CLAIM_NO_"+i+"'>"+element.REF_CLAIM_NO+"</td>"
							+"                                         <td id='CLAIM_AMOUNT_"+i+"'>"+element.CLAIM_AMOUNT+"</td>"
							//+"                                         <td>"+element.ATTACH_LR+"</td>"
							//+"                                         <td>"+element.ATTACH_CLAIM_COPY+"</td>"
							+"											<td style='width: 214px;'>"
							+"												<a class='edit btn btn-dark' onClick='editInwardReturnGoodsRegLrDetails("+element.LR_ID+","+i+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> "// <a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegLrDetails("+inwardRetGoodsRegId+","+element.LR_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>
							+"											</td>"
							+"										</tr>";
						i++;
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"							<div id='subPopUp'></div>"
					+"						</div>"
					+"             		<div class='modal-footer text-center'>"
			        + "                		<button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            		</div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $(".dateClassCommon").datepicker({
						changeMonth : true,
						changeYear : true,
						yearRange : "-100:+0"
					});
					$('#timePicker').timepicki();
				 displayEditOrganizationDropDown(2,data.ORG_ID);
				 getCompaniesDropdownForEdit(data.ORG_ID,data.COMPANY_ID);
				 getTransporterDropdownForEdit(data.ORG_ID,data.TRANSPORTER_ID);
				 loadEmployeeDropDownForEdit(data.ORG_ID,data.EMPLOYEE_ID);
				 getStockistForEdit(data.ORG_ID,data.COMPANY_ID,data.STOCKIST_ID);
				 ajaxEditInwardReturnGoodsReg("editInwardReturnGoodsReg",mainIndex);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
			}, 'json');

}
function ajaxEditInwardReturnGoodsReg(FormId,mainIndex)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						$('#ID_NO_'+mainIndex).html(data.ID_NO);
						$('#ORG_'+mainIndex).html(data.ORGANIZATION);
						$('#COMPANY_'+mainIndex).html(data.COMPANY);
						$('#STOCKIST_'+mainIndex).html(data.STOCKIST);
						$('#TRAN_'+mainIndex).html(data.TRANSPORTER);
						alert(data.MSG);
					}else
					alert(data.MSG);
					$('#loader').hide();
			}
		};
$('#' + FormId).ajaxForm(options);
}

function editInwardReturnGoodsRegLrDetails(LR_REG_ID,mainIndex){
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/LR_DetailedViewsAndLrProductList', {
		LR_ID : LR_REG_ID 
		} , function(data) {
			//'commonData' is a global variable
			commonData = data;
			productListArray = data.productsArr;
				var i = 0;
				var html=""
					+ "<div class='modal fade ' id='modal-responsive777' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive777\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Inward Return Goods Registration LR Details</strong></h4>"
			        + "            </div>"
			        + "  		   <div class='panel panel-default'>"
			        
			        + "			       <div class='panel-body'>"
			        + "				 <form id='editInwardReturnGoodsRegLrDetails' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationController/editInwardReturnGoodsRegLrDetails' method='post'>"
			        + "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
					+"                                        					<div class='form-group'>"
					+"                                                                <label class='form-label'><strong>LR NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='lrNo' value= '"+data.LR_NO+"' placeholder='LR NO' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>NO OF CASES: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='noOfCases' value= '"+data.NO_OF_CASES+"' placeholder='NO OF CASES' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					/*+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>LR COPY : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"  															  <div id='lrCopyImgId'>"
					+"                                                               	 <img  id='lrCopyImgId' onClick='showImagePopup(\""+data.ATTACH_LR_PATH+"\")' src='"+data.ATTACH_LR_PATH+"' alt='Smiley face' height='100' width='100'>"
					+"  															  </div>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input id='lrCopyId' type='file'  name='attachLr' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"*/
					+"                                                            <div class='form-group'>"
		            +"                                                     		    <button type='button' onclick='editLrCopyImagesForInwardReturnGoodsListPopup("+LR_REG_ID+")' class='btn btn-success'>Edit LR Copy Document</button>"
				    +"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='refOrClaimNo' value= '"+data.REF_CLAIM_NO+"' placeholder='CLAIM NO' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM AMOUNT : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='claimAmount' value= '"+data.CLAIM_AMOUNT+"' placeholder='CLAIM AMOUNT' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					/*+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM COPY : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"  															  <div id='lrClaimCopyImgId'>"
					+"                                                                  <img onClick='showImagePopup(\""+data.ATTACH_CLAIM_COPY_PATH+"\")' src='"+data.ATTACH_CLAIM_COPY_PATH+"' alt='Smiley face' height='100' width='100'>"//<label class='form-label'><strong>"+data.ATTACH_CLAIM_COPY_PATH+"</strong>"
					+"  															  </div>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input id='claimCopyId' type='file'  name='attachClaimCopy' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"*/
					+"                                                            <div class='form-group'>"
		            +"                                                     		    <button type='button' onclick='editClaimCopyImagesForInwardReturnGoodsListPopup("+LR_REG_ID+")' class='btn btn-success'>Edit Clain Copy Document</button>"
				    +"                                                            </div>"
					+ "						 </div>"
					+ " 				   <input type='hidden' name='LR_REG_ID' value='"+LR_REG_ID+"'>"
					+ " 			    </div>"
					+"                       <div class='modal-footer'>"
					+"                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editInwardReturnGoodsRegLrDetails\")'>Update</button>"
					+"                           <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-success'>Cancel</button>"                                                                               
					+"                       </div>"
					+" 					</form>"
					+"																<div class='row'>"
					+"																	<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"																		<table class='table table-striped table-hover'>"
					+"																			<thead class='no-bd'>"
					+"																				<tr>"
					+"																					<th style='text-align: center;'><strong>Sr No.</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Product Name</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Claim Quantity</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Received Quantity</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Batch</strong>"
					+"																					</th>"
					/*+"																					<th style='text-align: center;'><strong>MFG Date</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Expiry Date</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>MFG Company</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Quantity Variance</strong>"
					+"																					</th>"*/
					+"																					<th style='text-align: center;'><strong>Action</strong>"
					+"																					</th>"
					+"																				</tr>"
					+"																			</thead>"
					+"																			<tbody class='no-bd-y'>";
				
					$(productListArray).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ (i+1) +"</td>"
							+"                                         <td id='PRODUCT_NAME_"+i+"'>"+element.PRODUCT_NAME+"</td>"
							+"                                         <td id='CLAIM_QUANTITY_"+i+"'>"+element.CLAIM_QUANTITY+"</td>"
							+"                                         <td id='RECEIVED_QUANTITY_"+i+"'>"+element.RECEIVED_QUANTITY+"</td>"
							+"                                         <td id='BATCH_"+i+"'>"+element.BATCH+"</td>"
						/*	+"                                         <td>"+element.MFG_DATE+"</td>"
							+"                                         <td>"+element.EXPIRY_DATE+"</td>"
							+"                                         <td>"+element.MFG_COMPANY+"</td>"
							+"                                         <td>"+element.QUANTITY_VARIANCE+"</td>"*/
							+"											<td style='width: 214px;'>"
							+"												 <a class='edit btn btn-dark' onClick='editProductInfo("+i+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  "//<a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegProduct("+LR_REG_ID+","+element.PRODUCT_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>
							+"											</td>"
							+"										</tr>";
						i++;
					 });
					html+="																			</tbody>"
					+"																		</table>"
					+"																	</div>"
					+"														<div id='productDetailsPopup'></div>"
					+"																</div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
					+"															</div>"
					+"														</div>"
					+"													</div>"
					+"												</div>";
					 $("#subPopUp").html(html);
					 ajaxEditInwardReturnGoodsRegLrDetails("editInwardReturnGoodsRegLrDetails",mainIndex)
					 $("#modal-responsive777").addClass("in");
					 $("#modal-responsive777").attr("aria-hidden","false");
					 $("#modal-responsive777").css("display","block");
					}, 'json');
}
//
function ajaxEditInwardReturnGoodsRegLrDetails(FormId,mainIndex)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						$('#LR_NO_'+mainIndex).html(data.LR_NO);
						$('#NO_OF_CASES_'+mainIndex).html(data.NO_OF_CASES);
						$('#REF_CLAIM_NO_'+mainIndex).html(data.REF_CLAIM_NO);
						$('#CLAIM_AMOUNT_'+mainIndex).html(data.CLAIM_AMOUNT);
						var htmlText = "<img onClick='showImagePopup(\""+data.ATTACH_LR+"\")' src='"+data.ATTACH_LR+"' alt='Smiley face' height='100' width='100'>";
						$('#lrCopyImgId').html(htmlText);
						$('#lrCopyId').val('');
						
						var htmlText = "<img onClick='showImagePopup(\""+data.ATTACH_CLAIM_COPY+"\")' src='"+data.ATTACH_CLAIM_COPY+"' alt='Smiley face' height='100' width='100'>";
						$('#lrClaimCopyImgId').html(htmlText);
						$('#claimCopyId').val('');
						alert(data.MSG);
					}else
						alert(data.MSG);
				$('#loader').hide();
			}
		};
$('#' + FormId).ajaxForm(options);
}  
//LR Copy
function editLrCopyImagesForInwardReturnGoodsListPopup(lrDetailsId){
	editListOfImagesPopup(lrDetailsId,"attachLr",commonData.ATTACH_LR,"INWARD_RETURN_GOODS_LR_DETAILS_LR_IMAGE" , "/InwardReturnGoodsRegistrationController/updateInwardReturnGoodsLrRegLrImage");
}
//Image updation Ajax call after getting result from server
function ajaxInwardReturnGoodsLrRegLrImageUpdate(data){
	if(data.RESULT==true) {
		commonData.ATTACH_LR = data.ATTACH_LR;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Copy Image of Inward Return Goods LR Reg
function deleteInwardReturnGoodsLrRegLrImage(lrImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/deleteInwardReturnGoodsLrRegLrImage', {
			lrImageId : lrImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.ATTACH_LR[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//Claim Copy
function editClaimCopyImagesForInwardReturnGoodsListPopup(lrDetailsId){
	editListOfImagesPopup(lrDetailsId,"attachClaimCopy",commonData.ATTACH_CLAIM_COPY,"INWARD_RETURN_GOODS_LR_DETAILS_CLAIM_COPY_IMG" , "/InwardReturnGoodsRegistrationController/updateInwardReturnGoodsLrRegClaimCopyImage");
}
//Image updation Ajax call after getting result from server
function ajaxInwardReturnGoodsLrRegClaimCopyImageUpdate(data){
	if(data.RESULT==true) {
		commonData.ATTACH_CLAIM_COPY = data.ATTACH_CLAIM_COPY;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Claim Copy Image of Inward Return Goods LR Reg
function deleteInwardReturnGoodsLrRegClaimCopyImage(lrImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/deleteInwardReturnGoodsLrRegClaimCopyImage', {
			lrImageId : lrImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.ATTACH_CLAIM_COPY[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}


function checkDate6(dateType)
{ //alert($('#expiryDate').val());
//alert($('#mfgDate').val());
if($('#expiryDate').val()!=""&&$('#mfgDate').val()!="")
	if($('#expiryDate').val()<=$('#mfgDate').val())
	{
		alert("Manufacturing date should be less than expiry date...!");
		if(dateType==1)
			$('#mfgDate').val("");
		else if(dateType==2)
			$('#expiryDate').val("");
	}
}



//
function editProductInfo(index){
	var html="" 
		+ "<div class='modal fade ' id='modal-responsive123' >"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive123\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Company Product Details</strong></h4>"
        + "            </div>"
        + "  <div class='panel panel-default'>"
        
        + "<div class='panel-body'>"
        + "				 <form id='editProductInfo' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationController/editInwardReturnGoodsRegLreditProductDetails' method='post'>"
        + "    <div class='row'>"
        + "       <div class='col-md-4'>"
		+"                                        					<div class='form-group'>"
		+"                                                                <label class='form-label'><strong>PRODUCT NAME : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='productName' value= '"+productListArray[index].PRODUCT_NAME+"' placeholder='PRODUCT NAME' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"                                                            
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>CLAIM QUANTITY : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='claimQuantity' value= '"+productListArray[index].CLAIM_QUANTITY+"' placeholder='CLAIM QUANTITY' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>RECEIVED QUANTITY : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='receivedQuantity' value= '"+productListArray[index].RECEIVED_QUANTITY+"' placeholder='RECEIVED QUANTITY' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>BATCH : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='batch' value= '"+productListArray[index].BATCH+"' placeholder='BATCH' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>  "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>REASON : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='reason' value= '"+productListArray[index].REASON+"' placeholder='REASON' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                             <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>MFG Date : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='mfgDate' value= '"+productListArray[index].EDIT_MFG_DATE+"' placeholder='MFG Date' class='dateClassCommon form-control' id='mfgDate' onChange='checkDate6(1)' required>"
		+"                                                                </div>"
		+"                                                            </div>  "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>EXPIRY DATE : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='expiryDate' value= '"+productListArray[index].EDIT_EXPIRY_DATE+"' placeholder='EXPIRY DATE' class='dateClassCommon form-control' id='expiryDate' onChange='checkDate6(2)' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>MFG COMPANY : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='mfgCompany' value= '"+productListArray[index].MFG_COMPANY+"' placeholder='MFG COMPANY' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>                                             "            
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>QUANTITY VARIANCE : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='quantityVariance' value= '"+productListArray[index].QUANTITY_VARIANCE+"' placeholder='QUANTITY VARIANCE' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+ "</div>"
		+"												<input type='hidden' name='productRegId' value='"+productListArray[index].PRODUCT_ID+"'>"
		+ " </div>"//5
		+"                       <div class='modal-footer'>"
		+"                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editProductInfo\")'>Update</button>"
		+"                           <button type='button' onClick='hideSubPopUp(\"modal-responsive123\")' class='btn btn-success'>Cancel</button>"                                                                               
		+"                       </div>"
		+"		</form>"
		+ "</div>"//4
		+"            <div class='modal-footer text-center'>"
        + "                <button type='button' onClick='hideSubPopUp(\"modal-responsive123\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "            </div>"
        + "        </div>"
        + "    </div>"
        + "</div>";
	$('#productDetailsPopup').html(html);
	$(".dateClassCommon").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	ajaxEditProductInfo("editProductInfo",index);
	$("#modal-responsive123").addClass("in");
	$("#modal-responsive123").attr("aria-hidden","false");
	$("#modal-responsive123").css("display","block"); 
}

function ajaxEditProductInfo(FormId,mainIndex)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						//    
						$('#PRODUCT_NAME_'+mainIndex).html(data.PRODUCT_NAME);
						$('#CLAIM_QUANTITY_'+mainIndex).html(data.CLAIM_QUANTITY);
						$('#RECEIVED_QUANTITY_'+mainIndex).html(data.RECEIVED_QUANTITY);
						$('#BATCH_'+mainIndex).html(data.BATCH);
					    
						productListArray[mainIndex].PRODUCT_NAME=data.PRODUCT_NAME;
						productListArray[mainIndex].CLAIM_QUANTITY = data.CLAIM_QUANTITY;
						productListArray[mainIndex].RECEIVED_QUANTITY = data.RECEIVED_QUANTITY;
						productListArray[mainIndex].BATCH = data.BATCH;
						productListArray[mainIndex].REASON = data.REASON;
						productListArray[mainIndex].MFG_DATE = data.MFG_DATE;
						productListArray[mainIndex].EXPIRY_DATE = data.EXPIRY_DATE;
						productListArray[mainIndex].EDIT_MFG_DATE = data.EDIT_MFG_DATE;
						productListArray[mainIndex].EDIT_EXPIRY_DATE = data.EDIT_EXPIRY_DATE;
						productListArray[mainIndex].MFG_COMPANY = data.MFG_COMPANY;
						productListArray[mainIndex].QUANTITY_VARIANCE = data.QUANTITY_VARIANCE;
						hideSubPopUp("modal-responsive123");
						alert(data.MSG);
					}else
						alert(data.MSG);
				$('#loader').hide();
			}
		};
$('#' + FormId).ajaxForm(options);
}
var checkingDoneData;
//viewInwardReturnGoodsRegDetailsCheckingDoneDetailsView
function editInwardReturnGoodsRegDetailsAndCheckingDoneDetails(mainIndex,inwardRetGoodsRegId){
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/inwardReturnGoodsRegDetailsAndLrListingCheckingDoneView', {
		ID_NO : inwardRetGoodsRegId 
			} , function(data) {
				checkingDoneData=data;
				var i =1;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Inward Return Goods Registration Details</strong></h4>"
			        + "            </div>"
			        + "				 <form id='editInwardReturnGoodsReg' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationController/editInwardReturnGoodsRegistrationAndCheckingDone' method='post' enctype='multipart/form-data'>"
			        + "			       <div class='panel-body'>"
			        + "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
					+"            												 <div id='selectOrganization' class='form-group'>"
					+"                                                                <label class='form-label'><strong>Select Organization : - </strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>COMPANY NAME: - </strong>"
					+"                                                                </label>"	
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select id='companyName' name='companyName' onchange='getStockist()' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Company</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>STOCKIST NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select id='StockistName' name='stockistName' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Stockist</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTER NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select name='transporter' id='transName' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Transporter</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DELIVERED EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select id='employeeDropDown' name='employeeName' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Employee</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                   <input class='dateClassCommon form-control' value='"+data.REG_DATE+"' type='text' name='regDate' placeholder='Select Date' style='height: 36px; padding-left: 10px;' required>"
					+"                                                                </div>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>" 
					+"                                                                  <input type='text'  id='timePicker' name='time' value='"+data.TIME+"' placeholder='Time' class='form-control' required>"
					+"                                                                </div>"
					+"                                                                </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='driverNo' value= '"+data.DRIVER_NO+"' placeholder='DRIVER NO' class='form-control' >"
					+"                                                                </div>"
					+"                                                            </div>                                             "            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NAME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='driverName' value='"+data.DRIVER+"' placeholder='DRIVER NAME ' class='form-control' >"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTATION CHARGES : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='transpCharges' value='"+data.TRANSPORTATION_CHARGES+"' placeholder='TRANSPORTATION CHARGES' class='form-control' parsley-type='number' >"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            </div>"
					+ "						 </div>"
					+"						 <input type='hidden' value='"+data.ID_NO+"' name='regId'>"
					+"						 <div id='checkingDoneFormPopup'></div>"	
					+"                       <div class='modal-footer'>"
					+"                           <button type='Submit' onclick='return checkForOrganizationChange()' class='btn btn-primary'>Update</button>"
					+"                           <button type='button' onClick='hidePopUp()' class='btn btn-success'>Cancel</button>"                                                                               
					+"                       </div>"
					+ " 			    </div>"
					+"				</form>"	
			        + "            <div class='modal-header'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>LR List : - </strong></h4>"
			        + "            </div>"
					+"						<div class='row'>"
					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>LR No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>No of Cases</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Amount</strong>"
					+"											</th>      "
					/*+"											<th style='text-align: center;'><strong>LR Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Copy</strong>"
					+"											</th>"*/
					+"											<th style='text-align: center;'><strong>Action</strong>"
					+"											</th>"
					+"										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
				var irgrLRArr = data.irgrLRArr;
					$(irgrLRArr).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ i +"</td>"
							+"                                         <td id='LR_NO_"+i+"'>"+element.LR_NO+"</td>"
							+"                                         <td id='NO_OF_CASES_"+i+"'>"+element.NO_OF_CASES+"</td>"
							+"                                         <td id='REF_CLAIM_NO_"+i+"'>"+element.REF_CLAIM_NO+"</td>"
							+"                                         <td id='CLAIM_AMOUNT_"+i+"'>"+element.CLAIM_AMOUNT+"</td>"
							//+"                                         <td>"+element.ATTACH_LR+"</td>"
							//+"                                         <td>"+element.ATTACH_CLAIM_COPY+"</td>"
							+"											<td style='width: 214px;'>"
							+"												<a class='edit btn btn-dark' onClick='editInwardReturnGoodsRegLrDetails("+element.LR_ID+","+i+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> "// <a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegLrDetails("+inwardRetGoodsRegId+","+element.LR_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>
							+"											</td>"
							+"										</tr>";
						i++;
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"<div id='subPopUp'></div>"
					+"						</div>"
			        +"<div id='checkingDoneForm'>  </div>"
			        +"<div id='creditNoteForm'>  </div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $(".dateClassCommon").datepicker({
						changeMonth : true,
						changeYear : true,
						yearRange : "-100:+0"
					});
					$('#timePicker').timepicki();
				 displayEditOrganizationDropDown(2,data.ORG_ID);
				 getCompaniesDropdownForEdit(data.ORG_ID,data.COMPANY_ID);
				 getTransporterDropdownForEdit(data.ORG_ID,data.TRANSPORTER_ID);
				 loadEmployeeDropDownForEdit(data.ORG_ID,data.EMPLOYEE_ID);
				 getStockistForEdit(data.ORG_ID,data.COMPANY_ID,data.STOCKIST_ID);
				 ajaxEditInwardReturnGoodsRegCheckingDone("editInwardReturnGoodsReg",mainIndex);
				 editCheckingDoneForm(data);
				 if(data.CREDIT_NOTE_ID!=null)
					 editCreditNoteForm(data);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
			}, 'json');
}
function ajaxEditInwardReturnGoodsRegCheckingDone(FormId,mainIndex)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						$('#ID_NO_'+mainIndex).html(data.ID_NO);
						$('#ORG_'+mainIndex).html(data.ORGANIZATION);
						$('#COMPANY_'+mainIndex).html(data.COMPANY);
						$('#STOCKIST_'+mainIndex).html(data.STOCKIST);
						$('#TRAN_'+mainIndex).html(data.TRANSPORTER);
						hideSubPopUp("checkingDonePopup123");
						editCheckingDoneForm(data);
						//checkingDoneData -->     
						checkingDoneData.ORG_ID=data.ORG_ID;
						checkingDoneData.CHECKING_DONE_TIME=data.CHECKING_DONE_TIME;
						checkingDoneData.CHECKING_DONE_DATE=data.CHECKING_DONE_DATE;
						checkingDoneData.CHECKING_DONE_REMARK=data.CHECKING_DONE_REMARK;
						checkingDoneData.CHECKING_DONE_SLIP_IMG_PATH=data.CHECKING_DONE_SLIP_IMG_PATH;
						checkingDoneData.CHECKING_DONE_EMP_ID=data.CHECKING_DONE_EMP_ID;
						alert(data.MSG);
					}else
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
function editCreditNoteForm(data){
	var html= ""
		+ "				 <form id='editCreditNoteForm' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationController/editInwardReturnGoodsRegCreditNoteDetails' method='post' enctype='multipart/form-data'>"
        + "            <div class='modal-header'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Credit Note Details : -</strong></h4>"
        + "            </div>"
        + "			       <div class='panel-body'>"
        + "    			       <div class='row'>"
        + "       				   <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Credit Note No : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+ "                                                    				<input class='form-control'  value='"+data.CREDIT_NOTE_NO+"' type='text'  name='creditNoteNo' placeholder='Credit Note No' style='height: 36px; padding-left: 10px;' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Remark : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                   <input class='form-control' value='"+data.CREDIT_NOTE_REMARK+"' type='text'  name='creditNoteRemark' placeholder='Remark' style='height: 36px; padding-left: 10px;' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                             <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Credit Note Amount : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                   <input class='form-control' value='"+data.CREDIT_NOTE_AMOUNT+"' type='text' name='creditNoteAmount' placeholder='Credit Note Amount' style='height: 36px; padding-left: 10px;' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		/*+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Attach Credit Note: - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		   +"                                                                <div id='creditNoteAttachDoc'>"
		   +"                                                                <img onClick='showImagePopup(\""+data.CREDIT_NOTE_ATTACH+"\")' src='"+data.CREDIT_NOTE_ATTACH+"' alt='Smiley face' height='100' width='100'>"
		   +"                                                                </div>"
		+"                                                                <div class='controls'>" 
		+"                                                                  <input type='file' id='creditNoteAttach' name='creditNoteAttach'  class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"*/
		+"                                                        </div>"
		+ "						 </div>"
		+"						 <input type='hidden' value='"+data.CREDIT_NOTE_ID+"' name='creditNoteId'>"

        +"                                                            <div class='form-group'>"
        +"                                                     		    <button type='button' onclick='editCreditNoteAttachImagesListPopup("+data.CREDIT_NOTE_ID+")' class='btn btn-success'>EditCredit Note Attach Doc</button>"
		+"                                                            </div>"
		+"                       <div class='modal-footer'>"
		+"                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editCreditNoteForm\")'>Update</button>"
		+"                           <button type='button' onClick='hidePopUp()' class='btn btn-success'>Cancel</button>"                                                                               
		+"                       </div>"
		+ " 			    </div>"
		+"				</form>";
	$("#creditNoteForm").html(html);
	 $(".dateClassCommon").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
	$('#timePicker1').timepicki();
	ajaxEditCreditNoteForm("editCreditNoteForm");
	//loadEmployeeDropDownForEditCheckingDone(data.ORG_ID,data.CHECKING_DONE_EMP_ID);
}
function ajaxEditCreditNoteForm(FormId)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						/*var htmlText = "<img onClick='showImagePopup(\""+data.CREDIT_NOTE_ATTACH+"\")' src='"+data.CREDIT_NOTE_ATTACH+"' alt='Smiley face' height='100' width='100'>";
						$('#creditNoteAttachDoc').html(htmlText);
						$('#creditNoteAttach').val('');*/
						alert(data.MSG);
					}else
					alert(data.MSG);
				$('#loader').hide();
			}
		};
$('#' + FormId).ajaxForm(options);
}
//
function editCreditNoteAttachImagesListPopup(creditNoteId){
	editListOfImagesPopup(creditNoteId,"creditNoteAttach",checkingDoneData.CREDIT_NOTE_ATTACH,"INWARD_RETURN_GOODS_CREDIT_NOTE_ATTACH_IMG" , "/InwardReturnGoodsRegistrationController/updateCreditNoteRegCreditNoteAttachImage");
}
//Image updation Ajax call after getting result from server
function ajaxCreditNoteRegCreditNoteAttachImageUpdate(data){
	if(data.RESULT==true) {
		checkingDoneData.CREDIT_NOTE_ATTACH = data.CREDIT_NOTE_ATTACH;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteCreditNoteRegCreditNoteAttachImage(creditNoteAttachImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/deleteCreditNoteRegCreditNoteAttachImage', {
			creditNoteAttachImageId : creditNoteAttachImageId
		}, function(data) {
			if(data.RESULT==true){
				checkingDoneData.CREDIT_NOTE_ATTACH[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//
function editCheckingDoneForm(data){
	var html= ""
		+ "				 <form id='editCheckingDoneForm' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationController/editInwardReturnGoodsRegCheckingDoneDetails' method='post' enctype='multipart/form-data'>"
        + "            <div class='modal-header'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Checking Done Details : -</strong></h4>"
        + "            </div>"
        + "			       <div class='panel-body'>"
        + "    			       <div class='row'>"
        + "       				   <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Employee: - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+ "                                                    				<select  id='employeeDropDownForCheckingDone' name='checkingDoneEmployeeDetailsId' onchange='getStockist()' class='form-control' required>"
		+ "                                                        			   <option disabled selected> Select Employee</option>"
		+ "                                                    				</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Time: - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                   <input class='form-control' id='timePicker1' value='"+data.CHECKING_DONE_TIME+"' type='text'  name='checkingDoneTime' placeholder='Time' style='height: 36px; padding-left: 10px;' required>"
		+"                                                                </div>"
		+"                                                            </div>  "
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                             <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Date : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                   <input class='dateClassCommon form-control' value='"+data.CHECKING_DONE_DATE+"' type='text' name='checkingDoneDate' placeholder='Select Date' style='height: 36px; padding-left: 10px;' required>"
		+"                                                                </div>"
		+"                                                            </div>"   
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Remark : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>" 
		+"                                                                  <input type='text'   name='checkingDoneRemark' value='"+data.CHECKING_DONE_REMARK+"' placeholder='Remark' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-12'>"
		/*+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Attach Checking Slip: - </strong>"//CHECKING_DONE_SLIP_IMG_PATH
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		   +"                                                                <div id='checkingSlipDiv'>"
		   +"                                                                <img onClick='showImagePopup(\""+data.CHECKING_DONE_SLIP_IMG_PATH+"\")' src='"+data.CHECKING_DONE_SLIP_IMG_PATH+"' alt='Smiley face' height='100' width='100'>"
		   +"                                                                </div>"
		+"                                                                <div class='controls'>" 
		+"                                                                  <input type='file' id='checkingSlip' name='CheckingSlip'  class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"*/
        +"                                                            <div class='form-group'>"
        +"                                                     		    <button type='button' onclick='editCheckingDoneCheckingSlipImagesListPopup("+data.CHECKING_DONE_ID+")' class='btn btn-success'>Edit Checking Slip Doc</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+ "						 </div>"
		+"						 <input type='hidden' value='"+data.ID_NO+"' name='regId'>"
		+"                       <div class='modal-footer'>"
		+"                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editCheckingDoneForm\")'>Update</button>"
		+"                           <button type='button' onClick='hidePopUp()' class='btn btn-success'>Cancel</button>"                                                                               
		+"                       </div>"
		+ " 			    </div>"
		+"				</form>";
	$("#checkingDoneForm").html(html);
	 $(".dateClassCommon").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
	$('#timePicker1').timepicki();
	ajaxEditCheckingDoneForm("editCheckingDoneForm");
	loadEmployeeDropDownForEditCheckingDone(data.ORG_ID,data.CHECKING_DONE_EMP_ID);
}
function ajaxEditCheckingDoneForm(FormId)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						/*var htmlText = "<img onClick='showImagePopup(\""+data.CHECKING_DONE_SLIP_IMG_PATH+"\")' src='"+data.CHECKING_DONE_SLIP_IMG_PATH+"' alt='Smiley face' height='100' width='100'>";
						$('#checkingSlipDiv').html(htmlText);
						$('#checkingSlip').val('');*/
						
						checkingDoneData.CHECKING_DONE_TIME=data.CHECKING_DONE_TIME;
						checkingDoneData.CHECKING_DONE_DATE=data.CHECKING_DONE_DATE;
						checkingDoneData.CHECKING_DONE_REMARK=data.CHECKING_DONE_REMARK;
						//checkingDoneData.CHECKING_DONE_SLIP_IMG_PATH=data.CHECKING_DONE_SLIP_IMG_PATH;
						checkingDoneData.CHECKING_DONE_EMP_ID=data.CHECKING_DONE_EMP_ID;
						editCheckingDoneForm(checkingDoneData);
						alert(data.MSG);
					}else
					alert(data.MSG);
					$('#loader').hide();
			}
		};
$('#' + FormId).ajaxForm(options);
}
//
function editCheckingDoneCheckingSlipImagesListPopup(checkingDoneId){
	editListOfImagesPopup(checkingDoneId,"CheckingSlip",checkingDoneData.CHECKING_DONE_SLIP_IMG_PATH,"INWARD_RETURN_GOODS_CHECKING_DONE_CHECKING_SLIP" , "/InwardReturnGoodsRegistrationController/updateCheckingDoneRegCheckingSlipImage");
}
//Image updation Ajax call after getting result from server
function ajaxCheckingDoneCheckingSlipImageUpdate(data){
	if(data.RESULT==true) {
		checkingDoneData.CHECKING_DONE_SLIP_IMG_PATH = data.CHECKING_DONE_SLIP_IMG_PATH;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteCheckingDoneCheckingSlipImage(checkingSlipImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/deleteCheckingDoneCheckingSlipImage', {
			checkingSlipImageId : checkingSlipImageId
		}, function(data) {
			if(data.RESULT==true){
				checkingDoneData.CHECKING_DONE_SLIP_IMG_PATH[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//
function checkForOrganizationChange(){
	validateFormDetails('#editInwardReturnGoodsReg');
	existOrgVal = $("#OrgName").val();
	if(existOrgVal!=checkingDoneData.ORG_ID)
	{
		checkingDonePopup(checkingDoneData,existOrgVal);
		return false;
	}
	$('#loader').hide();
}
function checkingDonePopup(data,orgId){
	var html=""
		+"                                                        <div class='modal fade' id='checkingDonePopup123' aria-hidden='true'>"
		+"                                                            <div class='col-md-13'>"
		+"                                                                <div class='modal-content'>"
		+"                                                                    <div class='modal-header'>"
		+"                                                                        <input value='x' type='button' class='close' onClick='hideSubPopUp(\"checkingDonePopup123\")' aria-hidden='true'>"
		+"                                                                        <h4 class='modal-title' id='myModalLabel'><strong>Update Checking Done Details</strong></h4>"
		+"                                                                    </div>"
		+"                                                                    <div class='modal-body'>"
		+ "    			       <div class='row'>"
        + "       				   <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Employee: - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+ "                                                    				<select  id='employeeDropDownForCheckingDone' name='checkingDoneEmployeeDetailsId' class='form-control'>"
		+ "                                                        			   <option disabled selected> Select Employee</option>"
		+ "                                                    				</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Time: - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                   <input class='form-control' id='timePicker1' value='"+data.CHECKING_DONE_TIME+"' type='text'  name='checkingDoneTime' placeholder='Time' style='height: 36px; padding-left: 10px;'>"
		+"                                                                </div>"
		+"                                                            </div>  "
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                             <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Date : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                   <input class='dateClassCommon form-control' value='"+data.CHECKING_DONE_DATE+"' type='text' name='checkingDoneDate' placeholder='Select Date' style='height: 36px; padding-left: 10px;'>"
		+"                                                                </div>"
		+"                                                            </div>  "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Remark : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>" 
		+"                                                                  <input type='text'   name='checkingDoneRemark' value='"+data.CHECKING_DONE_REMARK+"' placeholder='Remark' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		//+"                                                        <div class='col-md-12'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Attach Checking Slip: - </strong>"//CHECKING_DONE_SLIP_IMG_PATH
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		   +"                                                                <img onClick='showImagePopup(\""+data.CHECKING_DONE_SLIP_IMG_PATH+"\")' src='"+data.CHECKING_DONE_SLIP_IMG_PATH+"' alt='Smiley face' height='100' width='100'>"
		+"                                                                <div class='controls'>" 
		+"                                                                  <input type='file'  name='CheckingSlip' value=''  class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		//+"                                                        </div>"//checkingDoneEmployeeDetailsId checkingDoneTime checkingDoneDate checkingDoneRemark CheckingSlip
		+ "						 </div>"
		+"                       <div class='modal-footer'>"
		+"                           <button type='Submit' class='btn btn-primary'>Update</button>"
		+"                           <button type='button' onClick='hideSubPopUp(\"checkingDonePopup123\")' class='btn btn-success'>Cancel</button>"                                                                               
		+"                       </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>";
	//$('#addProductPopUp').empty();
	 $('#checkingDoneFormPopup').append(html);
	 $(".dateClassCommon").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
	});
	$('#timePicker1').timepicki();
	loadEmployeeDropDownForEditCheckingDone(orgId,data.CHECKING_DONE_EMP_ID);
	$("#checkingDonePopup123").addClass("in");
	$("#checkingDonePopup123").attr("aria-hidden","false");
	$("#checkingDonePopup123").css("display","block");
}