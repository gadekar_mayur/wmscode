
function AddBank()
{
	var html=""
		+"<div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class=''>"
		+"                            <ul id='myTab2' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#tab2_1' data-toggle='tab' onclick='StockistAdvancedCheque()'><h5><strong>Stockist Advanced Cheque </strong></h5></a></li>"
		+"                                <li class=''><a href='#tab2_2' data-toggle='tab' onclick='stockistChequeDepositEntry()'><h5><strong>Stockist Cheque Deposit Entry</strong></h5></a></li>"
		+"                                <li class=''><a href='#tab2_5' data-toggle='tab' onclick='outstandingChequeDepositEntryDue()'><h5><strong>Outstanding Cheque Deposit Entry</strong></h5></a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	StockistAdvancedCheque();
}


function subTabsForStockistAdvancedCheque(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addStockAdvCheqTabEntry1' class=''><a href='#tab1_1' data-toggle='tab' onclick='StockistAdvancedCheque()'><h5><strong>Add Stockist Advanced Cheque</strong></h5></a></li>"
			+ "                                            <li id='viewStockAdvCheqTabEntry2' class=''><a href='#tab1_2' data-toggle='tab' onclick='StockistAdvancedChequeList()'><h5><strong>View Stockist Advanced Cheque</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForStockistAdvancedCheque').html(html);
	if (index == 1) {
		$('#addStockAdvCheqTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewStockAdvCheqTabEntry2').addClass("active");
	}
}


function StockistAdvancedCheque()
{
	$('#loader').show();
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_1'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Add Stockist Advanced Cheque </strong></h3>"
		+"                                    </div>"
		+ "<div id='subTabsForStockistAdvancedCheque'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"<form id='addStockistAdvancedCheque' action='"+contextApplicationPath+"/BankController/saveStockistAdvancedCheque' method='post'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"<div id='organiztionDropDown'></div>"
		+"                                            			  	  <div class='form-group'>"
		+"                                                			      <label class='col-lg-12 control-label' style='padding-left: 0px;'><strong></strong></label>"
		+"                                                				  <div class='col-lg-12'>"
		+"                                                    				  <div class='pos-rel'>"
		+"                                                        				  <input tabindex='13'  type='radio' value='Company' onchange='changeCompanyRadio(1)'  name='company_stokist_crit'   class='parsley-validated classname' required>"
		+"                                                        				  <label class='p-l-40' for='flat-checkbox-1'>Company</label>"
		+"                                                    				  </div>"
		+"                                                    				  <div class='pos-rel'>"
		+"                                                        				  <input tabindex='14'  type='radio' value='Stockist' name='company_stokist_crit' onchange='showStockistDropdownHtml(2)'   class='parsley-validated classname' required>"
		+"                                                          			  <label class='p-l-40' for='flat-checkbox-2'>Stockist</label>"
		+"                                                    				  </div>"
		+"                                                				  </div>"
		+"                                            				  </div>"
		+"<div id='selectStockist' class='form-group'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Cheque No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"<div id='moreChequeNo'>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='Cheque No' name='chequeNo' required>"
		+"                                                                </div>"
		+"</div>"
		+"                                                            </div>"
		+"                                                            <div class='pull-left'>"
		+"                                                                <button type='button' class='btn btn-primary m-b-10'  onclick='addMoreChequeNo()'>Add More</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+ "  <div id='companyDropDown'></div>           "
		+"                                                            <div class='form-group' style='height: 70px;'>"
		+"                                                                <label class='form-label'><strong>Bank</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"<div id='bankList'>"
		+"                                                                <select class='form-control' class='form-control'  name='bankID' required>"
		+"                                                                    <option selected disabled >Select Bank</option>"
		+"                                                                </select>"
		+"</div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Date</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='datepicker form-control' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' id='date' name='date' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button type='submit' class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addStockistAdvancedCheque\")'>Add</button>"
		+"                                                                <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                            </div>"
		+"                                                         </div>"
		+"                                                         </div>"
		+"                                                     </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"										</form>"
		+"                                                </div>"
		
		
		//+"											  	  <div id='StockistAdvancedChequeList'>"
		//+"											  	  </div>"	
		+"                                            </div>"
		+"                                        </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForStockistAdvancedCheque(1);
	BankStockistAdvancedChequeOrganiztionDropDown();
	$("#date").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	ajaxAddStockistAdvancedCheque('addStockistAdvancedCheque');
	$('#loader').hide();
}


/*function ajaxAddStockistAdvancedCheque(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					chequeCounter=1;
					StockistAdvancedCheque();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}*/

function ajaxAddStockistAdvancedCheque(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					if (element.MSG == 'User Name already exists') {
						$('#UserName_Error').html(element.MSG);
						ajaxAddStockistAdvancedCheque('addStockistAdvancedCheque');
						$('#loader').hide();
					} else {
						$('#' + FormId)[0].reset();
						alert(element.MSG);
						$('#loader').hide();
					}
					chequeCounter=1;
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

/*function ajaxAddOrganization(FormId) {
	var options = {
		success : function(json) {
			$(json).each(function(index, element) {
				if (element.MSG == 'User Name already exists') {
					$('#UserName_Error').html(element.MSG);
					ajaxAddOrganization('addOrganizationDetails');
					$('#loader').hide();
				} else {
					$('#' + FormId)[0].reset();
					alert(element.MSG);
					$('#loader').hide();
				}
			});
		}
	};
	$('#' + FormId).ajaxForm(options);
}*/

var chequeCounter=1;
function addMoreChequeNo()
{
	var html="<div id='chequeNo"+chequeCounter+"' class='form-group'><div class='controls'><input id='chequeNoId' name='chequeNo' type='text' class='form-control'><a href='#' onClick='removeChequeNoInBank("+chequeCounter+")'>X</a></div></div>";
	$('#moreChequeNo').append(html);
	chequeCounter++;
}

function removeChequeNoInBank(fieldId)
{
	$("#chequeNo"+fieldId).remove();
}

function changeCompanyRadio(index)
{
	if(index==1)
		{
			$('#selectStockist').empty();
			
			
			loadAllCompanyUsignOrgnizationId($("#orgId").val(),index);
		}
	if(index==3)
	{
		$('#selectStockist').empty();
		
		loadAllCompanyUsignOrgnizationId($("#orgId").val(),index);
	}
}

function loadCompanyBank(index)
{
	var companyId=$('#comapnyId').val();
	var orgId=$('#orgId').val();
	if(orgId==-1)
		{
		alert("Please select orgnization");
		$('.classname').prop('checked', false);
		}
	else if(companyId==-1)
		{
		alert("please select company");
		$('.classname').prop('checked', false);
		}
	else
		{
		
			if(index==1)
				{
				var html="<select class='form-control' class='form-control' id='selectedbankId' name='bankID' required>"
					+"<option selected disabled value='-1'>Select Bank</option>";
					$.post(contextApplicationPath+'/BankController/getCompanyBankUsignCompanyIdAndOrgId',{organizationId:orgId,companyId:companyId}, function(data) {
						$(data).each(function(index, element) {
							html+="<option value='"+element.COMPANY_BANK_ID+"'>"+element.BANK_NAME+"</option>";
						}); 
						html+="</select>";
						$('#bankList').html(html);
					}, 'json');
				}
			else
				{
				var html="<select class='form-control' class='form-control' id='selectedbankId' name='bankID' onchange='CompanyBankChange()' required>"
					+"<option selected disabled value='-1'>Select Bank</option>";
					$.post(contextApplicationPath+'/BankController/getCompanyBankUsignCompanyIdAndOrgId',{organizationId:orgId,companyId:companyId}, function(data) {
						$(data).each(function(index, element) {
							html+="<option value='"+element.COMPANY_BANK_ID+"'>"+element.BANK_NAME+"</option>";
						}); 
						$('#bankList').html(html);
						html+="</select>";
					}, 'json');
				}
		
		}
}


function showStockistDropdownHtml(index)
{
		var html = "<label class='form-label'><strong>List of Company Stockist</strong>"
				+ "</label>"
				+ "<span class='tips'></span>"
				+ "<div class='controls'>"
				+ " <select id='StockistName' name='stockistId' onchange=\"inBankStockistChange('"+index+"')\" class='form-control' class='form-control' required>"
				+ "  <option selected disabled>Select Stockist</option>"
				+ " </select>"
				+ "</div>";
	$('#selectStockist').empty();
	$('#selectStockist').html(html);
	//$('#bankList').html('');
	loadAllCompanyUsignOrgnizationId($("#orgId").val(),index);
}

function inBankStockistChange(index)
{
	// load stockist bank
	var orgId = $('#orgId').val();
	var compId = $('#comapnyId').val();
	var stockistId=$('#StockistName').val();
//	if(index==1)
//		{
//		var html="<select class='form-control' class='form-control'  id='selectedbankId' name='bankID'>"
//			+"<option value='-1'>Select Bank</option>";
//		$.post(contextApplicationPath+'/BankController/getStockistBankUsignCompanyIdAndOrgIdAndStockistId',{organizationId:orgId,companyId:compId,stockistId:stockistId}, function(data) {
//			$(data).each(function(index, element) {
//				html+="<option value='"+element.STOCKIST_BANK_DETAILS_ID+"'>"+element.BANK_NAME+"</option>";
//			});
//			html+="</select>";
//			$('#bankList').html(html);
//		}, 'json');
//		}
	if(index==2)
		{
		var html="<select class='form-control' class='form-control'  id='selectedbankId' name='bankID' required>"
			+"<option selected disabled value='-1'>Select Bank</option>";
		$.post(contextApplicationPath+'/BankController/getStockistBankUsignCompanyIdAndOrgIdAndStockistId',{organizationId:orgId,companyId:compId,stockistId:stockistId}, function(data) {
			$(data).each(function(index, element) {
				html+="<option value='"+element.STOCKIST_BANK_DETAILS_ID+"'>"+element.BANK_NAME+"</option>";
			});
			html+="</select>";
			$('#bankList').html(html);
		}, 'json');
		}
	else
		{
		var html="<select class='form-control' class='form-control'  id='selectedbankId' name='bankID' onchange='stockistBankChange()' required>"
			+"<option selected disabled value='-1'>Select Bank</option>";
		$.post(contextApplicationPath+'/BankController/getStockistBankUsignCompanyIdAndOrgIdAndStockistId',{organizationId:orgId,companyId:compId,stockistId:stockistId}, function(data) {
			$(data).each(function(index, element) {
				html+="<option value='"+element.STOCKIST_BANK_DETAILS_ID+"'>"+element.BANK_NAME+"</option>";
			});
			html+="</select>";
			$('#bankList').html(html);
		}, 'json');
		}
	
}

function getStockistDropDown()
{ $('#orderNo').val("");
	var orgId = $('#orgId').val();
	var compId = $('#comapnyId').val();
	if(orgId==null||orgId==undefined || orgId==-1)
		{
		alert("Please select orgnization");
		$('.classname').prop('checked', false);
		orgId=0;
		}
	else if(compId==null||compId==undefined || compId==-1)
		{
		alert("Please select company");
		$('.classname').prop('checked', false);
		compId=0;
		}
	else
		{
		$.post(contextApplicationPath+'/StockistController/getStockist', {
			orgId : orgId,
			compId : compId
		}, function(data) {
			//$('#loader').hide();
			var counter = 0;
			var optionHtml = "<option disabled selected> Select Stockist</option>";
			for (counter = 0; counter < data.stockistArray.length; counter++) {
				optionHtml += "<option value='" + data.stockistArray[counter].stockistId + "'>"
				+ data.stockistArray[counter].stockistName + "</option>";
			}
			$("#StockistName").empty();
			$("#StockistName").append(optionHtml);
		}, 'json');
		}
}


function BankStockistAdvancedChequeOrganiztionDropDown()
{
	var userType='';
	var html="<div class='form-group' style='height: 58px;'>"
	+"<label class='form-label'><strong>Select Organization</strong>"
	+"</label>"
	+"<span class='tips'></span>"
	+"<div class='controls'>"
	+"<select class='form-control' class='form-control' id='orgId' name='selectedOrganiztionNameId' onchange='BankStockistAdvancedChequeOrganiztionChange()' required>" 
	+"<option selected disabled>Select Organiztion</option>";
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
		$(data).each(function(index, element) {
			userType=element.USER_TYPE;
			if(element.USER_TYPE=='SuperAdmin')
				{
				html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
				}
			else
				{
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='selectedOrganiztionNameId' id='orgId' required>" +
						""+element.ORG_NAME+"";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
		content+="</div>"
		+"</div>";
		if(userType=='SuperAdmin')
			{
			$('#organiztionDropDown').html(html);
			}
		else
			{
			$('#organiztionDropDown').html(content);
			}
	}, 'json');
}



function loadAllCompanyUsignOrgnizationId(orgId,index)
{
	$.post(contextApplicationPath+'/CompanyController/getCompanies',{orgId : orgId }, function(data) {
		html="<div class='form-group'>"
			+"<label class='form-label'><strong>Select Company</strong>"
			+"</label>"
			+"<span class='tips'></span>"
			+"<div class='controls'>"
			+"<select class='form-control' class='form-control' name='comapnyId' id='comapnyId' onchange='companyChange("+index+")' required>"
			+"<option selected disabled>Select Company</option>";
		for (var i = 0; i < data.companyArray.length; i++)
		{
			html += "<option value='" + data.companyArray[i].compId + "'>"+ data.companyArray[i].compName + "</option>";
		}
		html+="</select>"
		+"</div>"
		+"</div>";
		$('#companyDropDown').html(html);
	}, 'json');
}

function BankStockistAdvancedChequeOrganiztionChange()
{
//	loadAllCompanyUsignOrgnizationId($("#orgId").val());
	$('.classname').prop('checked', false);
}

function companyChange(index)
{
	//load company bank
	if(index==1)
		{
		loadCompanyBank(index);
		}
	if(index==2)
		{
		getStockistDropDown();
		}
	if(index==3)
		{
		loadCompanyBank(index);
		}
	else if(index==4)
		{
		getStockistDropDown();
		}
}


function StockistAdvancedChequeList()
{
var i=1;
$('#loader').show();
//$.post(contextApplicationPath+'/BankController/listAdvancedChequeInformation', function(data) {
	var html=""
		+ "                                         <div class='tab-pane fade active in' id='tab1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Stockist Advanced Cheque Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForStockistAdvancedCheque'></div>"
		+"<div class='panel panel-default'>";
     html +="                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
		+"                                                                        <option value='Company'>Company</option>"
		+"                                                                        <option value='CompanyBank'>Company Bank</option>"
		+"                                                                        <option value='Stockist'>Stockist</option>"
		+"                                                                        <option value='StockistBank'>Stockist Bank</option>"
		+"                                                                        <option value='NoOfCheque'>No Of Cheque</option>"
		+"                                                                        <option value='ChequeNo'>Cheque No</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchStockistAdvancedCheque(1)'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                                                    </div>";
  	 html +="                                                 </div>"
		+"                                            </div>";
		$('#myTabContent').html(html);
		subTabsForStockistAdvancedCheque(2);
		commonstockistAdvancedChequeList("","",1);
//		stockistAdvancedChequeListView(data);
//	}, 'json');
}

function commonstockistAdvancedChequeList(searchOption,searchText,from){
	$.post(contextApplicationPath+ '/BankController/searchStockistAdvancedCheque',{
		searchOption : searchOption,
		searchText : searchText,
		from : from
		},
		function(data) {
			var url = "commonstockistAdvancedChequeList(\""+searchOption+"\",\""+searchText+"\",";
			stockistAdvancedChequeListView(data,from,url);
		}, 'json');
}
function stockistAdvancedChequeListView(data,from,url) {
	var i = 0;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html = ""
		+ "                                                            <table class='table table-striped table-hover'>"
		+ "                                                                <thead class='no-bd'>"
		+ "                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Orgnization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Bank</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>                                                    "
		+"                                                                        <th style='text-align: center;'><strong>Stockist Bank</strong>"
		+"                                                                        </th>  "
		+"                                                                        <th style='text-align: center;'><strong>No of Cheque</strong>"
		+"                                                                        </th>  "
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+ "                                                                    </tr>"
		+ "                                                                </thead>"
		+ "                                                                <tbody class='no-bd-y'>";

		$(data).each(function(index, element) 
		{
//			alert("Data :"+data);
			if((data.length-2)<i)
				return false;
		html += " <tr style='text-align: center;' id='stockistAdvanceChequeEntry_"+element.ADV_CHEQUE_INFORMATION_ID+"'>"
		+"<td>"+ SR_NO +"</td>"
		+"<td>"+element.ORG+"</td>"
		+"<td>"+element.COMPNAY+"</td>"
		+"<td>"+element.COMPANY_BANK+"</td>"
		+"<td>"+element.STOCKIST+"</td>"
		+"<td>"+element.STOCKIST_BANK+"</td>"
		+"<td id='noOfCheque_"+element.ADV_CHEQUE_INFORMATION_ID+"'>"+element.NO_OF_CHEQUE+"</td>"
//		+"<td><a class='edit btn btn-blue' href='#' onclick=\"viewAdvanceCheque("+element.ADV_CHEQUE_INFORMATION_ID+")\"><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark' href='#' onclick='editAdvanceCheque("+element.ADV_CHEQUE_INFORMATION_ID+")'><i class='fa fa-pencil-square-o'></i></a><a class='delete btn btn-danger' href='#' onclick='deleteAdvanceCheque("+element.ADV_CHEQUE_INFORMATION_ID+")'><i class='fa fa-times-circle'></i> </a></td>"
		+"<td><a class='edit btn btn-blue' href='#' onclick=\"viewAdvanceCheque("+element.ADV_CHEQUE_INFORMATION_ID+")\"><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark' href='#' onclick='editAdvanceCheque("+element.ADV_CHEQUE_INFORMATION_ID+")'><i class='fa fa-pencil-square-o'></i></a></td>"
		+"</tr>";
		i++;
		SR_NO++;
	
		});
		html += "                                                                </tbody>"
			+ "                                                            </table>";
	  $('#searchResult').html(html);
		 paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
	  $('#loader').hide();
}

function viewAdvanceCheque(AdvanceChequeid)
{
	viewAdvanceChequePopUp(AdvanceChequeid);
}

function editAdvanceCheque(AdvanceChequeid)
{
	
	editAdvanceChequeFunction(AdvanceChequeid)
}

function searchStockistAdvancedCheque(from) {
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if (!(Boolean(searchText))) {
		alert("Please Enter the search text");
		return false
	}
	commonstockistAdvancedChequeList(searchOption,searchText,from);
//	$.post(contextApplicationPath+ '/BankController/searchStockistAdvancedCheque',{
//		searchOption : searchOption,
//		searchText : searchText
//		},
//		function(data) {
//			stockistAdvancedChequeListView(data);
//		}, 'json');
}


