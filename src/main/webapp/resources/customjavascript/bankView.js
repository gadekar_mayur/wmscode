function viewAdvanceChequePopUp(AdvanceChequeid)
{

	var j =1;
	var html="" 
					+ "<div class='modal fade in' id='modal-responsive' >"
		            + "    <div class='col-md-13'>"
		            + "        <div class='modal-content'>"
		            + "            <div class='modal-header'>"
		            + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		            + "                <h4 class='modal-title' id='myModalLabel'><strong>View Advance Cheque Information</strong></h4>"
		            + "            </div>"
		            + "  <div class='panel panel-default'>"
		            + "<div class='panel-body'>"
		            + "    <div class='row'>"
		            + "       <div class='col-md-4'>";
	$.post(contextApplicationPath+'/BankController/ViewStockistAdvanceCheck',{AdvanceChequeid:AdvanceChequeid }, function(data) {
		$(data).each(function(index,element){
			
			   html+= "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Organization Name : - </strong>"
		            + "               </label>"
		            + "				  <label class='form-label'><strong>"+element.ORG_NAME+"</strong>"
		            + "                </label>"
		            + "           </div>"
		            + "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Company Name : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.COMP_NAME+"</strong>"
		            + "                </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>"
		            + "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Company Bank Name : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.COMPANY_BANK+"</strong>"
		            + "                </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>"
		            + "           <div class='form-group'>"
		            + "                <label class='form-label'><strong>Submitted Date : - </strong>"
		            + "                </label>"
		            + "				 <label class='form-label'><strong>"+element.SUBMITTED_DATE+"</strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "           </div>";
		          html+= "       </div>"
		            + "       <div class='col-md-4'>"
		            + "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Stockist : - </strong>"
		            + "               </label>"
		            + "				  <label class='form-label'><strong>"+element.STOCKIST+"</strong>"
		            + "                </label>"
		            + "           </div>"
		            + "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Stockist Bank Name : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.BANK+"</strong>"
		            + "                </label>"
		            + "                <span class='tips'></span>"
		            + "           </div>"
		            + "          <div class='form-group'>"
		            + "               <label class='form-label'><strong>Number Of Cheques : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.NO_OF_CHEQUE+"</strong>"
		            + "                </label>"
		            + "               <span class='tips'></span>"
		            + "            </div>"
		            + "          <div class='form-group'>"
		            + "               <label class='form-label'><strong>Courier Received Date : - </strong>"
		            + "               </label>"
		            + "				 <label class='form-label'><strong>"+element.COURIER_RECEVIE_DATE+"</strong>"
		            + "                </label>"
		            + "               <span class='tips'></span>"
		            + "            </div>"
		            + " </div>"
		            + " </div>"
		            + "</div>";
		      	
		     	var chequeInfo=element.CHEQUE_ARRAY;
		      	if(chequeInfo.length > 0)
		      		{
		         html +="                                                <div class='panel-body'>"
				   +"                                                    <div class='row'>"
				   +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
				   +"                                                            <table class='table table-striped table-hover'>"
				   +"                                                                <thead class='no-bd'>"
					+"                                                                    <tr>"
					+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Cheque Number </strong>"
					+"                                                                        </th>"
					+"                                                                    </tr>"
					+"                                                                </thead>"
					+"                                                                <tbody class='no-bd-y'>";
		      
				 $(chequeInfo).each(function(index,element){
				html+="                                                                    <tr style='text-align: center;'>"
				+"                                                                        <td>"+ j++ +"</td>"
				+"                                                                        <td>"+element.CHEQUE_NO+"</td>"
				+"                                                                    </tr>"
				 });
		html+="                                                                </tbody>"
		+"                                                            </table>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>";
		      		}

	});
		html+= "            <div class='modal-footer text-center'>"
            + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
            + "            </div>"
            + "        </div>"
            + "    </div>"
            + "</div>";
	$('#popup').html(html);
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block"); 
}, 'json');	


}


