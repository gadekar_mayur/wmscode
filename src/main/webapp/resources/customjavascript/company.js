//company registration sub menu
function AddCompany(){
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>Company Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class=''>"
		+"                            <ul id='myTab2' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#tab2_1' onClick='addCompanyDetails()' data-toggle='tab'>Add Company Details</a></li>"
		+"                                <li class=''><a href='#tab2_2' onClick='addBankToCompany()' data-toggle='tab'>Company Bank</a></li>"
		+"                                <li class=''><a href='#tab2_3' onClick='assignOrganizationBankToCompany()' data-toggle='tab'>Organization Bank</a></li>"
		+"                                <li class=''><a href='#tab2_4' onClick='addExecutivesToCompany()' data-toggle='tab'>Add Company Executive</a></li>"
		+"                                <li class=''><a href='#tab2_5' onClick='addCompanyDepot()' data-toggle='tab'>Add Depot.</a></li>"
		+"                                <li class=''><a href='#tab2_6' onClick='addCompanyProduct()'data-toggle='tab'>Add Product</a></li>"
		+"                                <li class=''><a href='#tab2_8' onClick='companyCreditAndDiscount()' data-toggle='tab'>Credit & Discount</a></li>"
		+"                                <li class=''><a href='#tab2_9' onClick='uploadCompanyDocuments()' data-toggle='tab'>Upload Document</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').empty();
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	addCompanyDetails();
}

function subTabsForAddCompany(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addCompanyTabEntry1' class=''><a href='#tab2_1' data-toggle='tab' onclick='addCompanyDetails()'><h5><strong>Add Company Details</strong></h5></a></li>"
			+ "                                            <li id='viewCompanyTabEntry2' class=''><a href='#tab2_1_1' data-toggle='tab' onclick='companyDetailsListing()'><h5><strong>View Company Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCompany').html(html);
	if (index == 1) {
		$('#addCompanyTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewCompanyTabEntry2').addClass("active");
	}
}
function validateFormDetails(formId)
{
	var z = $(formId).parsley('validate');
	if(z==true)
		{
		//showLoader();
		$('#loader').show();
		}
}

//company registration form
function addCompanyDetails(){
	var html=""
		+ "                                         <div class='tab-pane fade active in' id='tab2_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Company Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompany'></div>"
		+ "                                    <div class='row'>"
			+ "                                        <div class='col-md-12'>"
			+ "                                            <div class='panel panel-default'>"
			+ "                                                <div class='panel-body'>"
		+"<form id='saveCompany' action='"+contextApplicationPath+"/CompanyController/addCompanyDetails' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div id='selectOrganization' class='form-group'>"
		+"                                                            </div>"                                                            
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Company Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id = 'companyName' name='companyName' placeholder='Company Name'  class='form-control' required parsley-type='varcharCompanyName' >"
		+"                                                                </div>"
		+"                                                            </div>"
		+ "                                          <div class='form-group'>"
		+ "                                               <label class='form-label'><strong>State*</strong>"
		+ "                                               </label>"
		+ "                                               <span class='tips'></span>"
		+ "                                               <div class='controls'>"
		+ "                                               	<select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control' required>"
		+ "														<option disabled selected> Select State</option>"
		+ "													</select>"
		+ "                                                </div>"
		+ "                                           </div>"
		+ "                                           <div class='form-group' style='height: 70px;'>"
		+ "                                            	<label class='form-label'><strong>District*</strong>"
		+ "                                              </label>"
		+ "                                              <span class='tips'></span>"
		+ "                                              <div class='controls'>"
		+ "                                              	<select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control' required>"
		+ "                                                  	<option disabled selected> Select District</option>"
		+ "                                                  </select>"
		+ "                                              </div>"
		+ "                                           </div>"
		+ "                                           <div class='form-group'>"
		+ "                                                <label class='form-label'><strong>City*</strong>"
		+ "                                                </label>"
		+ "                                                <span class='tips'></span>"
		+ "                                                <div class='controls'>"
		+ "                                                    <select name='city' id='cityName' class='form-control' required>"
		+ "                                                        <option disabled selected> Select City</option>"
		+ "                                                    </select>"
		+ "                                                </div>"
		+ "                                           </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Contact Person*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='contactPerson' placeholder='Contact Person' class='form-control' required parsley-type='varcharPersonName'>"//
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Mobile No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='mobile' placeholder='Mobile No' class='form-control' parsley-type='onlynumber'>" //parsley-maxlength='12' parsley-type='phone'  parsley-minlength='10'
		+"                                                                </div>"
		+"                                                            </div>  "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Website</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='website' placeholder='Website' class='form-control' parsley-type='url'>"
		+"                                                                </div>"
		+"                                                            </div>    "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Fax No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='fax' placeholder='Fax No' class='form-control'  parsley-maxlength='15' parsley-type='onlynumber'>"
		+"                                                                </div>"
		+"                                                            </div>                                             "   
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Service Tax No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='service_tax_no' placeholder='Service Tax No' class='form-control'  parsley-maxlength='10'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>PAN No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                   <input type='text' id = 'pan_no' name='pan_no' placeholder='PAN No' class='form-control' required  parsley-maxlength='10'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Role Type*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div  id='roleTypeDropDown' class='controls'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Address*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='textarea' id = 'address' name='address' placeholder='Address' class='form-control' required parsley-type='alphanum'>"//parsley-type = 'varcharWithAddr'
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Telephone*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id = 'telephone' name='telephone' placeholder='Telephone' class='form-control' required  parsley-type='onlynumber'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Email ID*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id = 'email'  name='email'placeholder='Email ID' class='form-control' parsley-type='email' required>"
		+"                                                                </div>"
		+"                                                            </div>  "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>VAT*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id = 'vat' name='vat' placeholder='VAT' class='form-control' required  parsley-maxlength='12'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>CST</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text'name='cst'  placeholder='CST' class='form-control'  parsley-maxlength='12'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>User Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='uname' id='uname' placeholder='User Name' class='form-control' required>"
		+"                                                                </div>"
		+"																  <div id='error' class='validationError' ></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Password*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='password' name='password' placeholder='Password' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#saveCompany\")' >Add</button>"
		+"                                                                    <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"										        </form>"	
		+"                                            </div>"
//		+"											  <div id='companyListing'>"
//		+"											  </div>"	
		+"                                        </div>          "                             
		+"                                    </div>"
		+"                                </div>";
	
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForAddCompany(1);	
	displayOrganizationDropDown(0);
	stateDropDownList();
	loadRoleTypeDropDown();
	ajaxAddCompanyDetails('saveCompany');
	$('#loader').hide();
}




//function test()
//{
//	var z=$('#saveCompany').parsley('validate');
//////	if(document.getElementById('roleTypeDropDown').selectedIndex ==  0)  
////	{
////	alert("Please Enter Address");
////	}
//	
//}
//ajax call for add company
function ajaxAddCompanyDetails(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {},
			success : function(data) {
				if(data.result==true){
					$('#'+ formId)[0].reset();
//					companyDetailsListing();
					alert(data.MSG);
					$('#loader').hide();
				}
				else
				{
					if(data.FLAG==true){
						alert("User Name already exists");
						$('#error').html(data.MSG);
						$('#loader').hide();
					}
					else
					{
						alert(data.MSG);
						$('#loader').hide();
					}
				}
			}
	};
	$('#saveCompany').ajaxForm(options);
}

//registered company listing
function companyDetailsListing(){
	$('#loader').show();
	
		var html  = ""
		+ "<div class='tab-pane fade active in' id='tab2_1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Company Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompany'></div>"
		+ "                                    <div class='row'>"
		+ "                                        <div class='col-md-12'>";
		//$.post(contextApplicationPath+'/CompanyController/companyDetailsListing',function(data){
		html+= "                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+"                                                                        <option value='Organization' >Organization Name</option>"
		+"                                                                        <option value='Company'>Company Name</option>"
		+"                                                                        <option value='ContactPerson'>Contact Person</option>"
		//+"                                                                        <option value='Mobile'>Mobile No.</option>"
		+"                                                                        <option value='Email'>Email ID</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchCompanyDetails()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='companyDetails' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+ "                                                       <div id='pagination' class='pagination'></div>"
		+"                                                </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                </div>"
		+"                                            </div>";
	$('#myTabContent').html(html);
	subTabsForAddCompany(2);
//	$('#companyListing').empty();
	$('#companyListing').html(html);
	//companyDetailsListingView(data);
	commonCompanyDetailsController("", "", 1);
	$('#loader').hide();
	//}, 'json');
}


function searchCompanyDetails(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText.trim()))){
		alert("Please Enter the search text");
		return false
	}
	commonCompanyDetailsController(searchOption, searchText, 1);
}


function commonCompanyDetailsController(searchOption, searchText, from){
	$.post(contextApplicationPath+'/CompanyController/searchCompanyDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonCompanyDetailsController(\""+searchOption+"\",\""+searchText+"\",";
		companyDetailsListingView(data, from, url);
	}, 'json');
}

function companyDetailsListingView(data, from, url){
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=1;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>                                                        "            
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>                                 "
/*		+"                                                                        <th style='text-align: center;'><strong>Company ID</strong>"
		+"                                                                        </th>"
*/		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Contact Person</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Mobile No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Email ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
																		for (counter = 0; counter < data.companyArray.length; counter++,SR_NO++) {
																		   html+="<tr style='text-align: center;'>"
																				+"  <td>"+SR_NO+"</td>  "
																				+"  <td>"+data.companyArray[counter].ORG_NAME+"</td>            "                                      
																				+"  <td id='COMPANY_NAME"+counter+"'>"+data.companyArray[counter].COMPANY_NAME+"</td>"
																				+"  <td id='CONTACT_PERSON"+counter+"'>"+data.companyArray[counter].CONTACT_PERSON+"</td>"
																				+"  <td id='MOBILE"+counter+"'>"+data.companyArray[counter].MOBILE+"</td>"
																				+"  <td id='EMAIL"+counter+"'>"+data.companyArray[counter].EMAIL+"</td>"
																				+"  <td><a class='edit btn btn-blue' onClick='viewCompanyDetails("+data.companyArray[counter].COMPANY_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' onClick='editCompanyDetails("+data.companyArray[counter].COMPANY_ID+","+counter+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteCompanyDetails("+data.companyArray[counter].COMPANY_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
																				+"</tr>";
																		}
	html+="                                                             </tbody>"
		+"                                                            </table>";
	$('#companyDetails').html(html);
	paginationView(from,data.paginationCount,url);
}
//Add by nutan
//view company details
function viewCompanyDetails(companyId){
	ViewCompanyDetailsPopUp(companyId);
}
//edit company details
function editCompanyDetails(companyId,index){
	editCompanyDetailsPopUp(companyId,index);
}
//delete company details
function deleteCompanyDetails(companyId){
	//alert("deleteCompanyDetails = "+companyId)
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CompanyController/deleteCompanyDetails', {
			companyId : companyId
		}, function(data) {
				companyDetailsListing();
				alert(data.MSG);
		}, 'json');
    }
}

function subTabsForAddCompanyBank(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addCompanyBankTabEntry1' class=''><a href='#tab2_2' data-toggle='tab' onclick='addBankToCompany()'><h5><strong>Add Company Bank Details</strong></h5></a></li>"
			+ "                                            <li id='viewCompanyBankTabEntry2' class=''><a href='#tab2_2_1' data-toggle='tab' onclick='companyBankListing()'><h5><strong>View Company Bank Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCompanyBank').html(html);
	if (index == 1) {
		$('#addCompanyBankTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewCompanyBankTabEntry2').addClass("active");
	}
}


//Add company bank details
function addBankToCompany(){
	var html=""
		+ "                                <div class='tab-pane fade active in' id='tab2_2'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Company Bank Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyBank'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"													<form id='addCompantBank' action='"+contextApplicationPath+"/CompanyController/addCompanyBankDetails' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div id='selectOrganization' class='form-group'>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Bank Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='bankName' placeholder='Bank Name' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Account No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='accNo' placeholder='Account No' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Company*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='companyName' name='companyName' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Company</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>  "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Branch*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='branch' placeholder='Branch' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>IFSC Code*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' name='ifsc' placeholder='IFSC Code' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addCompantBank\")'>Add</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"													</form>"	
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='companyBankListing'>"
//		+"											  </div>"	
		+"                                        </div>              "                              
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForAddCompanyBank(1);
	displayOrganizationDropDown(1);
	ajaxAddBankToCompany('addCompantBank');
//	companyBankListing();
}
//ajax call
function ajaxAddBankToCompany(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.result=true){
					$('#'+ formId)[0].reset();
//					companyBankListing();
					alert("Company Bank Details Added Successfully");
					$('#loader').hide();
				}
				else
				{
					alert("Add Company Bank Details Failed");
					$('#loader').hide();
				}
			}
	};
	$('#addCompantBank').ajaxForm(options);
}
//company bank details listing
function companyBankListing(){
	$('#loader').show();
//	$.post(contextApplicationPath+'/CompanyController/companyBankDetailsListing',function(data){
	var html=""
		+ "                                         <div class='tab-pane fade active in' id='tab2_2_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Company Bank Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyBank'></div>"
		+ "                                    <div class='row'>"
		+ "                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
		+"                                                                        <option value='BankName'>Bank Name</option>"
		+"                                                                        <option value='Company'>Company Name</option>"
		+"                                                                        <option value='Branch'>Branch</option>"
		+"                                                                        <option value='AccountNo'>Account No</option>"
		+"                                                                        <option value='IFSC'>IFSC Code</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchCompanyBankDetails()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		//+ "                                                  <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+ "                                                  <div id='pagination' class='pagination'></div>"
		+"                                                </div>"
		+"                                            </div>";
	$('#myTabContent').html(html);
	subTabsForAddCompanyBank(2);
	commonCompanyBankListingViewController("", "", 1);
//	companyBankListingView(data);
	$('#companyBankListing').empty();
	$('#companyBankListing').html(html);
	$('#loader').hide();
//	}, 'json');
}

function commonCompanyBankListingViewController(searchOption, searchText, from)
{
	$.post(contextApplicationPath+'/CompanyController/searchCompanyBankDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonCompanyBankListingViewController(\""+searchOption+"\",\""+searchText+"\",";
		companyBankListingView(data, from, url);
	}, 'json');
}
function companyBankListingView(data, from, url){
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	commonData = data;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Bank Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Branch</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Account No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>IFSC Code</strong>"
		+"                                                                        </th>                                                    "
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
																		for (var counter = 0; counter < data.length-1; counter++) {
																			if((data.length-1)<counter)
																			break;
																			   html+="<tr style='text-align: center;'>"
																					+"   <td>"+SR_NO+"</td>"
																					+"   <td>"+data[counter].ORG_NAME+"</td>"
																					+"   <td>"+data[counter].COMPANY_NAME+"</td>"
																					+"   <td id='BANK_NAME"+counter+"'>"+data[counter].BANK_NAME+"</td>"
																					+"   <td id='BRANCH"+counter+"'>"+data[counter].BRANCH+"</td>"
																					+"   <td id='ACCOUNT_NO"+counter+"'>"+data[counter].ACCOUNT_NO+"</td>"
																					+"   <td id='IFSC"+counter+"'>"+data[counter].IFSC+"</td>"
																					+"   <td><a class='edit btn btn-dark' onClick='editCompanyBankDetails("+data[counter].COMPANY_BANK_ID+","+counter+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteCompanyBankDetails("+data[counter].COMPANY_BANK_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
																					+"</tr>";  SR_NO++;
																		}
	html+="                                                                </tbody>"
		+"                                                            </table>";
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
}

//Delete company bank details
function deleteCompanyBankDetails(companyBankId){
	//alert("Delete = "+companyBankId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CompanyController/deleteCompanyBankDetails', {
			companyBankId : companyBankId
		}, function(data) {
				companyBankListing();
				alert(data.MSG);
		}, 'json');
    }
}
//
function searchCompanyBankDetails(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commonCompanyBankListingViewController(searchOption, searchText, 1);
//	$.post(contextApplicationPath+'/CompanyController/searchCompanyBankDetails', {
//		searchOption : searchOption,
//		searchText : searchText
//	}, function(data) {
//		companyBankListingView(data);
//	}, 'json');
}

function subTabsForAddOrganizationBankInCompany(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addOrgBankTabEntry1' class=''><a href='#tab2_3' data-toggle='tab' onclick='assignOrganizationBankToCompany()'><h5><strong>Add Organization Bank Details</strong></h5></a></li>"
			+ "                                            <li id='viewOrgBankTabEntry2' class=''><a href='#tab2_3_1' data-toggle='tab' onclick='assignedOrganizationBankToCompanyListing()'><h5><strong>View Organization Bank Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddOrganizationBankInCompany').html(html);
	if (index == 1) {
		$('#addOrgBankTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewOrgBankTabEntry2').addClass("active");
	}
}
//Assign organization bank to company
function assignOrganizationBankToCompany(){
	var html=""
	+ "                                <div class='tab-pane fade active in' id='tab2_3'>"
	+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
	+ "                                        <h3><strong>Add Organization Bank Details</strong></h3>"
	+ "                                    </div>"
	+ "<div id='subTabsForAddOrganizationBankInCompany'></div>"
		+"                                    <div class='row'>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"													<form id='addOrgBankToCompany' action='"+contextApplicationPath+"/CompanyController/addOrganizationBankToCompany' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div id='selectOrganization' class='form-group'>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Bank*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"<div id='selectOrgBankList'><select id='selectOrgBank' name='orgBanks' class='form-control' multiple='multiple' style='display: none;' required></select></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Company*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='companyName' name='company' onchange='multiSelectOrganizationBankDropDown()' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Company</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addOrgBankToCompany\")'>Add Bank</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"													</form>"
		+"                                                </div>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='orgBabkToCompListing'>"
//		+"											  </div>"	
		+"                                        </div>     "                                       
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForAddOrganizationBankInCompany(1);
	displayOrganizationDropDown(1);
	$('#selectOrgBank').multiselect({
		header: "Select Bank"
	});
	ajaxAssignOrgBankToCompany('addOrgBankToCompany');
//	assignedOrganizationBankToCompanyListing();
}
//ajax call
function ajaxAssignOrgBankToCompany(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.result=true){
					$('#'+ formId)[0].reset();
//					assignedOrganizationBankToCompanyListing();
					$('#selectOrgBank').html('');
					alert("Organization Bank assigned to Company Successfully");
					$('#loader').hide();
				}
				else{
					alert("Operation Failed");
					$('#loader').hide();
				}
			}
	};
	$('#addOrgBankToCompany').ajaxForm(options);
}

//get org bank list  
function multiSelectOrganizationBankDropDown(){
	var orgId = $('#OrgName').val();
	var companyId = $('#companyName').val();
	var optionHtml = "<select id='selectOrgBank' name='orgBanks' class='form-control' multiple='multiple' style='display: none;'>";
	$.post(contextApplicationPath+'/CompanyController/organiztionBankDropdown', {
		orgId : orgId,
		companyId : companyId
	}, function(data) {
		var counter = 0;
		for (counter = 0; counter < data.orgBankArray.length; counter++) {
			var stat=false;
			for (var j = 0; j < data.companyOrgBankArray.length; j++) {
				if(data.companyOrgBankArray[j].ORG_BANK_ID==data.orgBankArray[counter].ORG_BANK_ID)
					stat=true;
			}
			if(stat)
				optionHtml += "<option selected='selected' value='" + data.orgBankArray[counter].ORG_BANK_ID + "'>"+ data.orgBankArray[counter].BANK_NAME + "</option>";
			else
				optionHtml += "<option value='" + data.orgBankArray[counter].ORG_BANK_ID + "'>"+ data.orgBankArray[counter].BANK_NAME + "</option>";
		}
		optionHtml +="</select>";
		
		$("#selectOrgBankList").empty();
		$("#selectOrgBankList").html(optionHtml);
		$('#selectOrgBank').multiselect({
			header: "Select Bank"
		});
	}, 'json');
}
//organization bank to company listing
function assignedOrganizationBankToCompanyListing(){
	$('#loader').show();
	//$.post(contextApplicationPath+'/CompanyController/getOrgBankToCompanyList',function(data){
	var html=""
		+ "                                         <div class='tab-pane fade active in' id='tab2_3_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Organization Bank Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddOrganizationBankInCompany'></div>"
		+ "                                    <div class='row'>"
		+ "                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
		//+"                                                                        <option>Bank Name</option>"
		+"                                                                        <option value='Company' >Company Name</option>"
		//+"                                                                        <option>Branch</option>"
		//+"                                                                        <option>Account No</option>"
		//+"                                                                        <option>IFSC Code</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchAssignedBankDetailsOfOrgToCompany()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		
		+"                                                    </div>"
		+"                                                     <div id='pagination' class='pagination'></div>"
		+"                                                </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	commonAssignedBankDetailsOfOrgToCompanyController("", "", 1);
	subTabsForAddOrganizationBankInCompany(2);
	$('#orgBabkToCompListing').empty();
	$('#orgBabkToCompListing').html(html);
	//assignedOrganizationBankToCompanyListingView(data);
	//$('#loader').hide();
	//}, 'json');
}

function assignedOrganizationBankToCompanyListingView(data, from, url){
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
//	var counter = 0;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
		for (counter = 0; counter < data.companyArray.length; counter++) {
			   html+="<tr style='text-align: center;'>"
					+"  <td>"+(SR_NO)+"</td>  "
					+"  <td>"+data.companyArray[counter].ORG_NAME+"</td>            "                                      
					+"  <td>"+data.companyArray[counter].COMPANY_NAME+"</td>"
					+"  <td></a><a class='edit btn btn-blue' onclick = 'viewCompanyOrgBankDetails("+data.companyArray[counter].COMPANY_ID+")'href='javascript:;'><i class='fa fa-external-link'> </i></a>"// <a class='edit btn btn-dark' onClick='editOrgBankToCompanyList("+data.companyArray[counter].COMPANY_ID+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteOrgBankToCompanyList("+data.companyArray[counter].COMPANY_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
					+"</tr>";
			   SR_NO++;}
		//	});
		+"                                                                </tbody>"
		+"                                                            </table>";
	$('#searchResult').html(html);
	$('#loader').hide();
	paginationView(from,data.paginationCount,url);
}
//
function searchAssignedBankDetailsOfOrgToCompany(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commonAssignedBankDetailsOfOrgToCompanyController(searchOption, searchText, 1);
}

function commonAssignedBankDetailsOfOrgToCompanyController(searchOption, searchText, from)
{
	$.post(contextApplicationPath+'/CompanyController/searchAssignedBankDetailsOfOrgToCompany', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonAssignedBankDetailsOfOrgToCompanyController(\""+searchOption+"\",\""+searchText+"\",";
		assignedOrganizationBankToCompanyListingView(data, from, url);
	}, 'json');
}
//View CompanyOrgBank Details
//Add by nutan
function viewCompanyOrgBankDetails(companyBankId)
{
	viewCompanyOrganizationBankDetails(companyBankId)
	}
//
function editOrgBankToCompanyList(companyId){
	alert("Edit = "+companyId);
}
//
function deleteOrgBankToCompanyList(companyId){
	alert("Delete = "+companyId);
}

function subTabsForAddCompanyExecutive(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addCompExecutiveTabEntry1' class=''><a href='#tab2_4' data-toggle='tab' onclick='addExecutivesToCompany()'><h5><strong>Add Company Executive Details</strong></h5></a></li>"
			+ "                                            <li id='viewCompExecutiveTabEntry2' class=''><a href='#tab2_4_1' data-toggle='tab' onclick='addExecutivesToCompanyListing()'><h5><strong>View Company Executive Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCompanyExecutive').html(html);
	if (index == 1) {
		$('#addCompExecutiveTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewCompExecutiveTabEntry2').addClass("active");
	}
}

//add Executives For Company 
function addExecutivesToCompany(){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_4'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Company Executive Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyExecutive'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"									                 <form id='saveExecutive' action='"+contextApplicationPath+"/CompanyController/addExecutivesToCompany' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div id='selectOrganization' class='form-group'>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-1' class='control-label'>Executive Name*</label>"
		+"                                                                <input type='text' name='executiveName' class='form-control' id='companyExecutiveNameId' placeholder='Executive Name' required>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-4' class='control-label'>Mobile*</label>"
		+"                                                                <input type='text' name='mobile' class='form-control' id='companyExecutiveMobileId' placeholder='Mobile' parsley-type='onlynumber' required>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Company*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='companyName' name='companyName' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Company</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-1' class='control-label'>Designation*</label>"
		+"                                                                <input type='text' name='designation' class='form-control' id='companyExecutiveDesignationId' placeholder='Designation' required>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-5' class='control-label'>Email ID*</label>"
		+"                                                                <input type='text' name='email' class='form-control' id='companyExecutiveEmailId' placeholder='Email ID' required>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                   <button type='submit'  class='btn btn-primary' data-dismiss='modal' onclick='validateFormDetails(\"#saveExecutive\")'>Save</button>"
		+"                                                                    <button type='reset' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"  												 </form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='executivesListing'>"
//		+"											  </div>"	
		+"                                        </div>     "                                       
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForAddCompanyExecutive(1);
	ajaxAddExecutivesToCompany('saveExecutive');
	displayOrganizationDropDown(1);
//	addExecutivesToCompanyListing();
}
function demo(){
	alert("Demo");
}
//ajax call
function ajaxAddExecutivesToCompany(formId){
	var options = {
			dataType : 'json',
			success : function(data) {
				if(data.result=true){
					$('#loader').hide();
					$('#'+ formId)[0].reset();
//					addExecutivesToCompanyListing();
					alert("Company Executive Details Added Successfully");
					$('#loader').hide();
				}
				else{
					alert("Add Company Executive Details Failed");
					$('#loader').hide();
				}
			}
	};
	$('#saveExecutive').ajaxForm(options);
}
//Company Excecutives Listing
function addExecutivesToCompanyListing(){
	$('#loader').show();
	//$.post(contextApplicationPath+'/CompanyController/companyExecutivesListing',function(data){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_4_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Company Executive Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyExecutive'></div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
		+"                                                                        <option value='Company'>Company Name</option>"
		+"                                                                        <option value='ExecName'>Name</option>"
		+"                                                                        <option value='Designation'>Designation</option>"
		//+"                                                                        <option value='Mobile'>Mobile No</option>"
		+"                                                                        <option value='Email'>Email ID</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchCompanyExecutiveDetails()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		
		+"                                                    </div>"
		+"                                                     <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>";
		$('#myTabContent').empty();
		$('#myTabContent').html(html);
		commonCompanyExecutiveDetailsController("", "", 1);
		subTabsForAddCompanyExecutive(2);
		$('#executivesListing').empty();
		$('#executivesListing').html(html);
	//executivesToCompanyListingView(data);
	//$('#loader').hide();
//	}, 'json');
}
function executivesToCompanyListingView(data, from, url){
	//commonData is a global var , it is used at edit form (do not delete the below assignment)
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	commonData=data;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Designation</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Mobile No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Email ID</strong>"
		+"                                                                        </th>                                                   " 
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                        </div>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>"
																			for (var counter = 0; counter < data.length-1; counter++) {
																			    html+="<tr style='text-align: center;'>"
																					+"<tr style='text-align: center;'>"
																					+"  <td>"+SR_NO+"</td>"
																					+"  <td>"+data[counter].ORG_NAME+"</td>"
																					+"  <td>"+data[counter].COMPANY_NAME+"</td>"
																					+"  <td id='EXECUTIVE_NAME"+counter+"'>"+data[counter].EXECUTIVE_NAME+"</td>"
																					+"  <td id='DESIGNATION"+counter+"'>"+data[counter].DESIGNATION+"</td>"
																					+"  <td id='MOBILE"+counter+"'>"+data[counter].MOBILE+"</td>"
																					+"  <td id='EMAIL"+counter+"'>"+data[counter].EMAIL+"</td>"
																					+"  <td><a class='edit btn btn-dark' onClick='editCompanyExecutiveDetails("+data[counter].EXECUTIVE_ID+","+counter+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteCompanyExecutive("+data[counter].EXECUTIVE_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a></td>"
																					+"  </tr>";
																			    SR_NO++;
																			}
		+"                                                                </tbody>"
		+"                                                            </table>";
		
	$('#searchResult').html(html);
	$('#loader').hide();
	paginationView(from,data[data.length-1].paginationCount,url);
}
//
function searchCompanyExecutiveDetails(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commonCompanyExecutiveDetailsController(searchOption, searchText, 1);
}

function commonCompanyExecutiveDetailsController(searchOption, searchText, from){
	$.post(contextApplicationPath+'/CompanyController/searchCompanyExecutiveDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonCompanyExecutiveDetailsController(\""+searchOption+"\",\""+searchText+"\",";
		executivesToCompanyListingView(data, from, url);
	}, 'json');
}

//
function deleteCompanyExecutive(companyExeId){
	//alert("Delete Executive = "+companyExeId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CompanyController/deleteCompanyExecutive', {
			companyExeId : companyExeId
		}, function(data) {
				addExecutivesToCompanyListing();
				alert(data.MSG);
		}, 'json');
    }
}

function subTabsForAddCompanyDepot(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addCompDepotTabEntry1' class=''><a href='#tab2_5' data-toggle='tab' onclick='addCompanyDepot()'><h5><strong>Add Company Depot Details</strong></h5></a></li>"
			+ "                                            <li id='viewCompDepotTabEntry2' class=''><a href='#tab2_5_1' data-toggle='tab' onclick='companyDepotListing()'><h5><strong>View Company Depot Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCompanyDepot').html(html);
	if (index == 1) {
		$('#addCompDepotTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewCompDepotTabEntry2').addClass("active");
	}
}
function validateCompanyDepot()
{
	if(document.getElementById('depoName').value == '' )
	{
	alert("Please Enter Depot name");
	return false;
	}
	if(document.getElementById('companyName').value == '' )
		{
		alert("Please Enter Company name");
		return false;
		}
	if(document.getElementById('address').value == '' )
	{
	alert("Please Enter Address");
	return false;
	}
	if(document.getElementById('telephone').value == '' )
	{
	alert("Please Enter Telephone Number");
	return false;
	}
	if(document.getElementById('email').value == '' )
	{
	alert("Please Enter Email address");
	return false;
	}
	if(document.getElementById('vat').value == '' )
	{
	alert("Please Enter Vat Number");
	return false;
	}
	if(document.getElementById('pan_no').value == '' )
	{
	alert("Please Enter Pan Number");
	return false;
	}
	return true;
}

//function validateDepot()
//{
//	var z=$('#saveCompanyDepo').parsley('validate');
//////	if(document.getElementById('roleTypeDropDown').selectedIndex ==  0)  
////	{
////	alert("Please Enter Address");
////	}
//	
//}
//Add company depo form
function addCompanyDepot(){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_5'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Company Depot Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyDepot'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"									                 <form id='saveCompanyDepo' action='"+contextApplicationPath+"/CompanyController/addCompanyDepo' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div id='selectOrganization' class='form-group'>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-1' class='control-label'>Depo Name*</label>"
		+"                                                                <input type='text' id = 'depoName' name='depoName'  class='form-control' id='field-1' placeholder='Depo Name' required>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-2' class='control-label'>Designation</label>"
		+"                                                                <input type='text' name='designation' class='form-control' id='field-2' placeholder='Designation' >"
		+"                                                            </div>"
		+"                                                            <div class='form-group' style='height: 70px;'>"
		+"                                                                <label class='form-label'><strong>District</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select District*</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-6' class='control-label'>Telephone*</label>"
		+"                                                                <input type='text' name='telephone' class='form-control' id='field-6' placeholder='Telephone' parsley-type='onlynumber' required>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-8' class='control-label'>Email ID*</label>"
		+"                                                                <input type='text' name='email' class='form-control' id='field-8' placeholder='Email ID' required>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-9' class='control-label'>Website</label>"
		+"                                                                <input type='text' name='website' class='form-control' id='field-9' placeholder='Website'>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Company*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='companyName' name='company' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Company</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-2' class='control-label'>Contact Person*</label>"
		+"                                                                <input type='text' name='contactPerson' class='form-control' id='field-10' placeholder='Conact Person' required>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>State</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control' required>"
		+"																		<option disabled selected> Select State*</option>"
	    +"																	</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group' style='height: 70px;'>"
		+"                                                                <label class='form-label'><strong>City*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select name='city' id='cityName' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select City</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-5' class='control-label'>Address*</label>"
		+"                                                                <input type='texareat' name='address' class='form-control' id='field-5' placeholder='Address' required>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-7' class='control-label'>Mobile No</label>"
		+"                                                                <input type='text' name='mobile' class='form-control' id='field-7' placeholder='Mobile No' parsley-type='onlynumber'>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                   <button type='submit' class='btn btn-primary' data-dismiss='modal' onclick='validateFormDetails(\"#saveCompanyDepo\")'>Save</button>"
		+"                                                                    <button type='reset' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>  "
		+"     												</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='companyDepotListing'>"
//		+"											  </div>"	
		+"                                        </div>                          "                  
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForAddCompanyDepot(1);
	displayOrganizationDropDown(1);
	stateDropDownList();
	ajaxAddCompanyDepot('saveCompanyDepo');
//	companyDepotListing();
}

//ajax call
function ajaxAddCompanyDepot(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.result=true){
					$('#'+ formId)[0].reset();
//					companyDepotListing();
					alert("Company Depot Details Added Successfully");
					$('#loader').hide();
				}
				else{
					alert("Add Company Depot Details Failed");
					$('#loader').hide();
				}
			}
	};
	$('#saveCompanyDepo').ajaxForm(options);
}

//
function companyDepotListing(){
	$('#loader').show();
	//$.post(contextApplicationPath+'/CompanyController/companyDepoListing',function(data){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_5_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Company Depot Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyDepot'></div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
		+"                                                                        <option value='Company'>Company Name</option>"
		+"                                                                        <option value='DepoName'>Depo Name</option>"
		+"                                                                        <option value='ContactPerson'>Contact Person</option>"
		//+"                                                                        <option value='Mobile'>Mobile No</option>"
		+"                                                                        <option value='Email'>Email ID</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchCompanyDepotDetails()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		
		+"                                                    </div>"
		+"                                                     <div id='pagination' class='pagination'></div>"
		+"                                                </div>"
		+"                                            </div>";
	$('#myTabContent').empty();	
	$('#myTabContent').html(html);
	commonCompanyDepotDetailsController("","",1);
	subTabsForAddCompanyDepot(2);
	$('#companyDepotListing').empty();
	$('#companyDepotListing').html(html);
	//companyDepotListingView(data);
	//$('#loader').hide();
//	}, 'json');
}
function companyDepotListingView(data, from, url){
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Depo Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Contact Person</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Mobile No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Email ID</strong>"
		+"                                                                        </th>"                                                    
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
																			for (var counter = 0; counter < data.companyDepoArr.length; counter++) {
																				html+="<tr style='text-align: center;'>"
																					+"  <td>"+SR_NO+"</td>"
																					+"  <td>"+data.companyDepoArr[counter].ORG_NAME+"</td>"
																					+"  <td>"+data.companyDepoArr[counter].COMPANY_NAME+"</td>"
																					+"  <td id='DEPO_NAME"+counter+"'>"+data.companyDepoArr[counter].DEPO_NAME+"</td>"
																					+"  <td id='CONTACT_PERSON"+counter+"'>"+data.companyDepoArr[counter].CONTACT_PERSON+"</td>"
																					+"  <td id='MOBILE"+counter+"'>"+data.companyDepoArr[counter].MOBILE+"</td>"
																					+"  <td id='EMAIL"+counter+"'>"+data.companyDepoArr[counter].EMAIL+"</td>"
																					+"  <td id='LINKS"+counter+"'><a class='edit btn btn-blue' onClick='viewCompanyDepo("+data.companyDepoArr[counter].DEPO_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark' onClick='editCompanyDepoDetails("+data.companyDepoArr[counter].DEPO_ID+","+counter+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteCompanyDepo("+data.companyDepoArr[counter].DEPO_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a></td>"
																					+"</tr>";
																				SR_NO++;}
		+"                                                                </tbody>"
		+"                                                            </table>";
$('#searchResult').html(html);
$('#loader').hide();
paginationView(from,data.paginationCount,url);
}
//
function searchCompanyDepotDetails(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commonCompanyDepotDetailsController(searchOption,searchText,1);
}

function commonCompanyDepotDetailsController(searchOption,searchText,from)
{
	$.post(contextApplicationPath+'/CompanyController/searchCompanyDepotDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonCompanyDepotDetailsController(\""+searchOption+"\",\""+searchText+"\",";
		companyDepotListingView(data, from, url);
	}, 'json');
}
//Add by nutan
//view CompanyDepot
function viewCompanyDepo(companyDepoId)
{
	 viewCompanyDepotPopup(companyDepoId)
}
//Edit depo details
function editCompanyDepo(depoId){
	alert("Edit = "+depoId);
}
//Delete Depo Details
function deleteCompanyDepo(depoId){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CompanyController/deleteCompanyDepo', {
			depoId : depoId
		}, function(data) {
				companyDepotListing();
				alert(data.MSG);
		}, 'json');
    }
}

function subTabsForAddCompanyProduct(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addCompProductTabEntry1' class=''><a href='#tab2_6' data-toggle='tab' onclick='addCompanyProduct()'><h5><strong>Add Company Product Details</strong></h5></a></li>"
			+ "                                            <li id='viewCompProductTabEntry2' class=''><a href='#tab2_6_1' data-toggle='tab' onclick='companyProductListing()'><h5><strong>View Company Product Details</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCompanyProduct').html(html);
	if (index == 1) {
		$('#addCompProductTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewCompProductTabEntry2').addClass("active");
	}
}

//function validateCompanyProduct()
//{
//	var z=$('#addProduct').parsley('validate');
//}
//add company product
function addCompanyProduct(){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_6'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Company Product Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyProduct'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"													<form id='addProduct' action='"+contextApplicationPath+"/CompanyController/addCompanyProduct' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div id='selectOrganization' class='form-group'>"
		+"                                                            </div>"
		+"                                                        </div>                                "                            
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Company*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='companyName' name='company' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Company</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-2' class='control-label'>Product Name*</label>"
		+"                                                                <input type='text' name='productName' class='form-control' id='companyProductNameId' placeholder='Product Name' required>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-2' class='control-label'>Product Code</label>"
		+"                                                                <input type='text' name='productCode' class='form-control' id='companyProductCodeId' placeholder='Product Code' required>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-3' class='control-label'>Type</label>"
		+"                                                                <input type='text' name='productType' class='form-control' id='companyProductTypeId' placeholder='TYPE' required>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-3' class='control-label'>MRP*</label>"
		+"                                                                <input type='text' name='mrp' class='form-control' id='companyProductMrpId' placeholder='MRP' parsley-type='number' required>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-3' class='control-label'>PTD*</label>"
		+"                                                                <input type='text' name='ptd' class='form-control' id='companyProductPtdId' placeholder='PTD' parsley-type='number' required>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-5' class='control-label'>Box Size*</label>"
		+"                                                                <input type='texareat' name='boxSize' class='form-control' id='companyProductBoxSizeId' placeholder='Box Size' parsley-type='onlynumber' required>"
		+"                                                            </div>"
/*		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"*/
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-6' class='control-label'>Unit Size*</label>"
		+"                                                                <input type='text' name='unitSize' class='form-control' id='companyProductUniteSizeId' placeholder='Unit Size' parsley-type='onlynumber' required>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                   <button type='submit' class='btn btn-primary' data-dismiss='modal' onclick='validateFormDetails(\"#addProduct\")'>Save</button>"
		+"                                                                    <button type='reset' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"													</form>"
		/*+"                                                    <h4 style='text-align: center;'>Or</h4>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-6'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-1' class='control-label'>For Bulk Upload</label>"
		+"                                                                <input type='file' class='form-control' id='field-11'>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                   <button type='submit' class='btn btn-primary' data-dismiss='modal' onclick=''>Save</button>"
		+"                                                                    <button type='reset' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"*/
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='companyProductListing'>"
//		+"											  </div>"	
		+"                                        </div>                               "             
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForAddCompanyProduct(1);
	displayOrganizationDropDown(1);
	ajaxAddCompanyProduct('addProduct');
}
//ajax call
function ajaxAddCompanyProduct(formId){
	var options = {
			dataType : 'json',
			success : function(data) {
				$('#loader').hide();
				if(data.result=true){
					$('#'+ formId)[0].reset();
//					companyProductListing();
					alert("Company Product  Added Successfully");
					$('#loader').hide();
				}
				else{
					alert("Add Company Product Failed");
					$('#loader').hide();
				}
			}
	};
	$('#addProduct').ajaxForm(options);
}

//company product listing
function companyProductListing(){
	$('#loader').show();
	//$.post(contextApplicationPath+'/CompanyController/companyProductListing',function(data){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_6'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Company Product Details</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyProduct'></div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' onchange='onChangeSearchOption()'>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
		+"                                                                        <option value='Company'>Company Name</option>"
		+"                                                                        <option value='ProductName'>Product Name</option>"
		+"                                                                        <option value='PTD'>PTD</option>"
		+"                                                                        <option value='Type'>Type/Form</option>"
		+"                                                                        <option value='MRP'>MRP</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div id='searchControl' class='controls'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchCompanyProductDetails()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+ "                                                        <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>";
		$('#myTabContent').html(html);
		subTabsForAddCompanyProduct(2);
		$('#companyProductListing').empty();
	$('#companyProductListing').html(html);
	//companyProductListingView(data);
	commonCompanyProductDetailsController("", "", 1);
	$('#loader').hide();
	//}, 'json');
}

function companyProductListingView(data, from, url){
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Product</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>PTD</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>MRP/Form</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>TYPE</strong>"
		+"                                                                        </th>                                      "              
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>"
																			for (var i = 0; i < data.companyProductArr.length; i++,SR_NO++) {
																				html+="<tr style='text-align: center;'>"
																					+"  <td>"+SR_NO+"</td>"
																					+"  <td>"+data.companyProductArr[i].ORG_NAME+"</td>"
																					+"  <td>"+data.companyProductArr[i].COMPANY_NAME+"</td>"
																					+"  <td id='PRODUCT_NAME"+i+"'>"+data.companyProductArr[i].PRODUCT_NAME+"</td>"
																					+"  <td id='PTD"+i+"'>"+data.companyProductArr[i].PTD+" Rs</td>"
																					+"  <td id='MRP"+i+"'>"+data.companyProductArr[i].MRP+" Rs</td>"
																					+"  <td id='TYPE"+i+"'>"+data.companyProductArr[i].TYPE+"</td>"
																					+"  <td id='LINKS"+i+"'><a class='edit btn btn-blue' onClick='viewCompanyProduct("+data.companyProductArr[i].PRODUCT_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark' onClick='editCompanyProductDetails("+data.companyProductArr[i].PRODUCT_ID+","+i+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> <a class='delete btn btn-danger' onClick='deleteCompanyProduct("+data.companyProductArr[i].PRODUCT_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a></td>"
																					+"</tr>";
																			}
		+"                                                                </tbody>"
		+"                                                            </table>";
	$('#searchResult').html(html);
	paginationView(from,data.paginationCount,url);
}
//
function onChangeSearchOption(){
	var searchOption = $('#searchOption').val();
	var html="";
	if(searchOption=='MRP'){
		html+="                                                                    <input id='fromMrp' type='text' class='form-control' placeholder='From MRP' style='width: 49%; margin-right: 1%;float: left;'>"
		+"                                                                    <input id='toMrp' type='text' class='form-control' placeholder='To MRP' style='width: 49%;'>";
	}else if(searchOption=='PTD')
		html+="                                                                    <input id='fromPtd' type='text' class='form-control' placeholder='From PTD' style='width: 49%; margin-right: 1%;float: left;'>"
			+"                                                                    <input id='toPtd' type='text' class='form-control' placeholder='To PTD' style='width: 49%;'>";
	else{
		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control'>";
	}
	$("#searchControl").html(html);
}
function searchCompanyProductDetails(){
	var searchOption = $('#searchOption').val();
	var searchText;
	if(searchOption=='MRP'){
		var toMrp = $('#toMrp').val();
		var fromMrp = $('#fromMrp').val();
		if(Boolean(fromMrp)&&Boolean(toMrp)){
			if(isNaN(fromMrp)||isNaN(toMrp)){
			    alert("Please Enter Number...!");
			    return false;
			}
			else{
				if(parseFloat(fromMrp)>=parseFloat(toMrp)){
					alert("From MRP must be less than To MRP...!"); 
					return false;
				}else
					searchText =parseFloat(fromMrp) +","+ parseFloat(toMrp);
			}
		}else{
			alert("Please enter both values for From MRP and To MRP");
			return false;
		}
	}
	else if(searchOption=='PTD'){
		var fromPtd = $('#fromPtd').val();
		var toPtd = $('#toPtd').val();
		if(Boolean(fromPtd)&&Boolean(toPtd)){
			if(isNaN(fromPtd)||isNaN(toPtd)){
				alert("Please Enter Number...!");
			    return false;
			}
			else{
				if(parseFloat(fromPtd)>=parseFloat(toPtd)){
					alert("From PTD must be less than To PTD...!");
					return false;
				}else
					searchText = parseFloat(fromPtd) +","+ parseFloat(toPtd);
			}
		}else{
			alert("Please enter both values for From PTD and To PTD");
			return false;
		}
	}
	else{
		searchText = $('#searchText').val();
		if(!(Boolean(searchText))){
			alert("Please Enter the search text");
			return false
		}
		
	}
	commonCompanyProductDetailsController(searchOption, searchText, 1);
}

function commonCompanyProductDetailsController(searchOption, searchText, from){
	$.post(contextApplicationPath+'/CompanyController/searchCompanyProductDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		if(data.result!=false){
			var url = "commonCompanyProductDetailsController(\""+searchOption+"\",\""+searchText+"\",";
			companyProductListingView(data, from, url);
		}
	}, 'json');
}
//view product
function viewCompanyProduct(productId){
	viewCompanyProductPopup(productId);
	
}

//view product
function deleteCompanyProduct(productId){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CompanyController/deleteCompanyProduct', {
			productId : productId
		}, function(data) {
				companyProductListing();
				alert(data.MSG);
		}, 'json');
    }
}
function organizationDropDownForCredit_Discount(index){
	$.post(contextApplicationPath+'/StockistController/organizationListDropdown',function(data){
		var html ="";
		if(data.orgArray.length>1){
			html+="<label class='form-label'><strong>Select Organization*</strong>"
				+" </label>"
				+" <span class='tips'></span>"
				html+=" <div class='controls'>";
			
				html+=" <select id='OrgName' name='orgName' onchange='getCompanyDropDownForCredit_Discount()' class='form-control' class='form-control' required>";
			
			html+="<option disabled selected> Select Organization</option>";
					for (var i = 0; i < data.orgArray.length; i++) {
					html += "<option value='" + data.orgArray[i].orgId + "'>"+ data.orgArray[i].orgName + "</option>";
					}
			html+="</select>"
				+"</div>";	
		}else if(data.orgArray.length>0){
			html+="<label class='form-label'><strong>Your Organization</strong>"
				+"</label>"
				+"<span class='tips'></span>"
				+"<div class='controls'>"
				+"<label class='form-label'><strong></strong>"+data.orgArray[0].orgName+"</label>"
				+"<input type='hidden' value='"+data.orgArray[0].orgId+"' name='orgName' id='OrgName'>"
				+"</div>";	
		}
		//$('#selectOrganization').empty();
		$('#selectOrganization').html(html);
		getCompanyDropDownForCredit_Discount();
		/*switch(index){
		case 1 : getCompanyDropDownForCredit_Discount();break;
		}*/
	}, 'json');
}
function getCompanyDropDownForCredit_Discount(){
	var orgId = $('#OrgName').val();
	$.post(contextApplicationPath+'/CompanyController/getCompanyDropDownForCredit_Discount', {
		orgId : orgId
	}, function(data) {
		//$('#loader').hide();
		var counter = 0;
		var optionHtml = "<option disabled selected> Select Company</option>";
		for (counter = 0; counter < data.companyArray.length; counter++) {
			optionHtml += "<option value='" + data.companyArray[counter].compId + "'>"
			+ data.companyArray[counter].compName + "</option>";
		}
		$("#companyName").empty();
		$("#companyName").append(optionHtml);
	}, 'json');
}

function subTabsForAddCompanyCreditAndDiscount(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addcompanyCreditAndDiscountTabEntry1' class=''><a href='#tab2_8' data-toggle='tab' onclick='companyCreditAndDiscount()'><h5><strong>Create Credit And Discount</strong></h5></a></li>"
			+ "                                            <li id='viewcompanyCreditAndDiscountTabEntry2' class=''><a href='#tab2_8_1' data-toggle='tab' onclick='companyCreditAndDiscountListing()'><h5><strong>View Credit And Discount</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCompanyCreditAndDiscount').html(html);
	if (index == 1) {
		$('#addcompanyCreditAndDiscountTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewcompanyCreditAndDiscountTabEntry2').addClass("active");
	}
}

//Add company Credit And Discount
function companyCreditAndDiscount(){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_8'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Create Credit And Discount</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyCreditAndDiscount'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"													<form id='creditAndDiscount' action='"+contextApplicationPath+"/CompanyController/addCompanyCreditAndDiscount' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div id='selectOrganization' class='form-group'>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Company*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='companyName' name='company' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Company</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover dataTable' id='table-editable'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong></strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Days</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Percentage %</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>"
		+"                                                                    <tr style='text-align: center;'>"
		+"                                                                        <td><strong>Local*</strong></td>"
		+"                                                                        <td><input type='text' name='localDays' class='form-control' placeholder='Days' parsley-type='onlynumber' required></td>"
		+"                                                                        <td><input type='text' name='localPercentage' class='form-control' placeholder='Percentage %' parsley-type='number' required></td>"
		+"                                                                    </tr>"
		+"                                                                    <tr style='text-align: center;'>"
		+"                                                                        <td><strong>Out Side Station*</strong></td>"
		+"                                                                        <td><input type='text' name='outsideDays' class='form-control' placeholder='Days' parsley-type='onlynumber' required></td>"
		+"                                                                        <td><input type='text' name='outsidePercentage' class='form-control' placeholder='Percentage %' parsley-type='number' required></td>"
		+"                                                                    </tr>"
		+"                                                                </tbody>"
		+"                                                            </table>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#creditAndDiscount\")' >Submit</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"													</form>"
		+"                                                </div>"
//		+"											    <div id='companyCreditAndDiscountList'>"
//		+"                                            </div>"
		+"                                        </div>"                             
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForAddCompanyCreditAndDiscount(1);
	organizationDropDownForCredit_Discount(1);
	ajaxcompanyCreditAndDiscount('creditAndDiscount');
//	companyCreditAndDiscountListing();
}
//ajax call
function ajaxcompanyCreditAndDiscount(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.result=true){
					$('#'+ formId)[0].reset();
					getCompanyDropDownForCredit_Discount();
					alert("Company Credit And Discount added Successfully");
//					companyCreditAndDiscountListing();
					$('#loader').hide();
				}
				else{
					alert("Operation Failed");
					$('#loader').hide();
				}
			}
	};
	$('#creditAndDiscount').ajaxForm(options);
}

function companyCreditAndDiscountListing(){
	$('#loader').show();
	//$.post(contextApplicationPath+'/CompanyController/companyCreditAndDiscountListing', function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_8_1'>"
			+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                                        <h3><strong>View Credit And Discount</strong></h3>"
			+ "                                    </div>"
			+ "<div id='subTabsForAddCompanyCreditAndDiscount'></div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group' style='height: 58px;'>"
			+"                                                                <label class='form-label'><strong>Search By</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
			+"                                                                        <option value='Organization'>Organization Name</option>"
			+"                                                                        <option value='CompanyName'>Company Name</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Search For</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
			+"                                                            <div class='pull-right'>"
			+"                                                                <button class='btn btn-success m-b-10' onclick='searchCreditAndDiscount()'>Show</button>"
			+"                                                            </div>"                                                      
			+"                                                        </div>"
			+"                                                        <div id='creditAndDiscountList' class='col-md-12 col-sm-12 col-xs-12 table-responsive'></div>"
			+"                                                    </div>"
			+"                                                        <div id='pagination' class='pagination'></div>" 
			+"                                                    </div>"
			+"                                                </div>"
			+"                                            </div>";
		$('#myTabContent').html(html);
		subTabsForAddCompanyCreditAndDiscount(2);
		$('#companyCreditAndDiscountList').html(html);
		//companyCreditAndDiscountListingView(data);
		$('#loader').hide();
		commonCreditAndDiscountController("", "" , 1);
    //}, 'json');	
}

function companyCreditAndDiscountListingView(data,from,url){	
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	commonData=data;
	var i = 0;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Local Days</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Local Percentage</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Outstation Days</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Outstation Percentage</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
																		//$(data).each(function(index,element){
	                                                                  $(data).each(function(index, element) {
		                                                                  if((data.length-2)<i)
			                                                                   return false; 
																//for (i = 0; i < data.length; i++) {
																			html+="<tr style='text-align: center;'>"
																				+"  <td>"+SR_NO+"</td>"
																				+"  <td>"+data[i].ORG_NAME+"</td>"
																				+"  <td>"+data[i].COMPANY+"</td>"
																				+"  <td id='LOCAL_DAYS_"+i+"'>"+data[i].LOCAL_DAYS+"</td>"
																				+"  <td id='LOCAL_PER_"+i+"'>"+data[i].LOCAL_PER+"</td>"
																				+"  <td id='OUTSTATION_DAYS_"+i+"'>"+data[i].OUTSTATION_DAYS+"</td>"
																				+"  <td id='OUTSTATION_PER_"+i+"'>"+data[i].OUTSTATION_PER+"</td>"
																				+"  <td id='LINKS_"+i+"'><a class='edit btn btn-dark' href='#' onclick='editCompanyCreditAndDiscount("+data[i].CREDIT_DISCOUNT_ID+","+i+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick='deleteCreditAndDiscountDetails("+data[i].CREDIT_DISCOUNT_ID+")'><i class='fa fa-times-circle'></i> </a></td>"
																				+" </tr>";

																			
																			
																			i++;
																			SR_NO++;
																			 })                                                                  
	html+="                                                                </tbody>"
		+"                                                            </table>";
	$('#creditAndDiscountList').html(html);
	paginationView(from,data[data.length-1].paginationCount,url);
}                                                                

////////////////vijay/////
function searchCreditAndDiscount(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText.trim()))){
		alert("Please Enter the search text");
		return false
	}
	commonCreditAndDiscountController(searchOption,searchText,1);
}
function commonCreditAndDiscountController(searchOption,searchText,from)
    {
		$.post(contextApplicationPath+'/CompanyController/searchCreditAndDiscount', {searchOption : searchOption,searchText : searchText, from : from
		}, function(data) {
			
			var url = "commonCreditAndDiscountController(\""+searchOption+"\",\""+searchText+"\",";
			companyCreditAndDiscountListingView(data,from,url);
		}, 'json');
    }

function deleteCreditAndDiscountDetails(ID){
	//alert("Delete :-"+ID);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CompanyController/deleteCompanyCreditAndDiscountDetails', {ID:ID
		}, function(data) {
			
			companyCreditAndDiscountListing();
			getCompanyDropDownForCredit_Discount();
			alert(data.MSG);
		}, 'json');
    }
}

function subTabsForAddCompanyUploadDocument(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addcompanyUploadDocumentTabEntry1' class=''><a href='#tab2_9' data-toggle='tab' onclick='uploadCompanyDocuments()'><h5><strong>Company Upload Document</strong></h5></a></li>"
			+ "                                            <li id='viewcompanyUploadDocumentTabEntry2' class=''><a href='#tab2_9_1' data-toggle='tab' onclick='uploadedCompanyDocumentListing()'><h5><strong>View Company Uploaded Document</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCompanyUploadDocument').html(html);
	if (index == 1) {
		$('#addcompanyUploadDocumentTabEntry1').addClass("active");
	} else if (index == 2) 
	{
		$('#viewcompanyUploadDocumentTabEntry2').addClass("active");
	}
}

//upload company documents
function uploadCompanyDocuments(){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_9'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Company Upload Document</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyUploadDocument'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"													<form id='uploadDocs' action='"+contextApplicationPath+"/CompanyController/uploadCompanyDocs' method='post'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div id='selectOrganization' class='form-group'>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Document Type*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																	<div id='documentList'></div>"
		+"                                                                </div>"
		+"                                                            </div>     "                                                      
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Company*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='companyName' name='company' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Company</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"                                 
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Browse File*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' name='documentImage' class='form-control' id='field-2'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='companyDocumentImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='companyDocumentImageAddMoreButtonId'><button onclick=\"addFileConrol('companyDocumentImageDivId','companyDocumentImageAddMoreButtonId','documentImage',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#uploadDocs\")'>Submit</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"													</form>"
		+"                                                </div>"
		+"                                            </div>"
//		+"											  <div id='uploadedCompanyDocsListing'>"
//		+"											  </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForAddCompanyUploadDocument(1);
	displayOrganizationDropDown(1);
	loadDocumentTypeList();
	ajaxUploadCompanyDocuments('uploadDocs')
//	uploadedCompanyDocumentListing();
}
//ajax call
function ajaxUploadCompanyDocuments(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
				$('#loader').hide();
				if(data.result=true){
					$('#'+ formId)[0].reset();
//					uploadedCompanyDocumentListing();
					alert("Company Document uploaded Successfully");
					$('#loader').hide();
				}
				else{
					alert("Operation Failed");
					$('#loader').hide();
				}
			}
	};
	$('#uploadDocs').ajaxForm(options);
}
//company documents list
function uploadedCompanyDocumentListing(){
	$('#loader').show();
//	$.post(contextApplicationPath+'/CompanyController/companyDocsListing',function(data){
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_9'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Company Uploaded Document </strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCompanyUploadDocument'></div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group' style='height: 58px;'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' class='form-control'>"
		+"                                                                        <option value='Organization'>Organization Name</option>"
		+"                                                                        <option value='Company'>Company Name</option>"
		+"                                                                        <option value='DocumentType'>Document Type</option>"
		/*+"                                                                        <option>Document Code</option>"
		+"                                                                        <option>File Name</option>"*/
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchUploadedCompanyDocsDetails()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                        </div>"
		+ "                                                        <div id='pagination' class='pagination'></div>"
		+ "                                   					 </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>";
	$('#myTabContent').html(html);
	subTabsForAddCompanyUploadDocument(2);
	commonUploadedCompanyDocumentController("", "", 1);
//	uploadedCompanyDocumentListingView(data);
	$('#uploadedCompanyDocsListing').empty();
	$('#uploadedCompanyDocsListing').html(html);
	$('#loader').hide();
//	}, 'json');
}
function commonUploadedCompanyDocumentController(searchOption, searchText, from)
{
	$.post(contextApplicationPath+'/CompanyController/searchUploadedCompanyDocsDetails', {
		searchOption : searchOption,
		searchText : searchText,
		from : from
	}, function(data) {
		var url = "commonUploadedCompanyDocumentController(\""+searchOption+"\",\""+searchText+"\",";
		uploadedCompanyDocumentListingView(data,from,url);
	}, 'json');
}
//
function uploadedCompanyDocumentListingView(data,from,url){
	//alert("uploadedCompanyDocumentListingView");
	//urlForEdit & commomData is global var
	urlForEdit = url;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	commonData = data;
	var i = 0;
	var html=""
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Document Type</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>File Name</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
																		$(data).each(function(index, element) {
																			if((data.length-2)<i)
																				return false;
																			html+="<tr style='text-align: center;'>"
																				+"    <td>"+SR_NO+"</td>"
																				+"  <td>"+element.ORG_NAME+"</td>"
																				+"  <td>"+element.COMPANY_NAME+"</td>"
																				+"  <td>"+element.DOCUMENT_TYPE+"</td>"
																				//+"  <td>"+data.companyDocArr[i].FILE_NAME+"</td>"
																				+"<td>"
																				+"<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showCompanyDocumentFileListPopup("+i+")'>"
																				//+"<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showImagePopup(\""+data.companyDocArr[i].FILE_NAME+"\")'>"
																				+"</td>"
																				+"  <td><a class='edit btn btn-dark' onClick='editCompanyDocs("+i+","+from+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteCompanyDocs("+element.DOCUMENT_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a></td>"
																				+"  </tr>";
																			i++;
																			SR_NO++;
																		});
	html+="                                                                </tbody>"
		+"                                                            </table>";
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);
}
//
function showCompanyDocumentFileListPopup(index){
	showListOfImagesPopup(commonData[index].FILE_NAME);
}
function searchUploadedCompanyDocsDetails(){
	var searchOption = $('#searchOption').val();
	var searchText = $('#searchText').val();
	if(!(Boolean(searchText))){
		alert("Please Enter the search text");
		return false
	}
	commonUploadedCompanyDocumentController(searchOption, searchText, 1);
//	$.post(contextApplicationPath+'/CompanyController/searchUploadedCompanyDocsDetails', {
//		searchOption : searchOption,
//		searchText : searchText
//	}, function(data) {
//		uploadedCompanyDocumentListingView(data);
//	}, 'json');
}

function editCompanyDocs(index, from){
	editCompanyDocumentForm(index, from)
}
//
function deleteCompanyDocs(companyDocId){
//	alert(companyDocId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CompanyController/deleteCompanyDocs', {
			companyDocId : companyDocId
		}, function(data) {
				uploadedCompanyDocumentListing();
				alert(data.MSG);
		}, 'json');
    }
}

