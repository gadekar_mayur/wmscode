var invoiceEntryStatus = false;
function editOrderEntryRegistrationFunction(OutWardOrderEntryRegistrationId)
{
	invoiceEntryStatus = true;
	var i=1;
	var j =1;
	var html="" 
					+ "<div class='modal fade in' id='modal-responsive' >"
		            + "    <div class='col-md-13'>"
		            + "        <div class='modal-content'>"
		            + "            <div class='modal-header'>"
		            + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		            + "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Outward Order Entry Registration Details</strong></h4>"
		            + "            </div>"
		            + "  <div class='panel panel-default'>"
		            + "<form id='editOutwardGoodsOrderEntryReg' action='"+contextApplicationPath+"/OutWardGoodsController/editOutwardGoodsOrderEntryRegistartion' method='post'>"
		            +"<input type='hidden' name='outWardOrderEntryRegistrationId' value='"+OutWardOrderEntryRegistrationId+"'>"
		            + "<div class='panel-body'>"
		            + "    <div class='row'>"
		            + "       <div class='col-md-4'>";
	$.post(contextApplicationPath+'/OutWardGoodsController/ViewAllDetailsOfOutwardGoodsRegistration',{OutWardOrderEntryRegistrationId : OutWardOrderEntryRegistrationId}, function(data) {
		$(data).each(function(index,element){
			//commonData is global var
			commonData = element;
			   html+= "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Organization Name : - </strong>"
		            + "               </label>"
		            + "                <span class='tips'></span>"
		            + "                <div class='controls'>"
					+ "                <input class='dateClassCommon form-control' disabled value='"+element.ORG_NAME+"' type='text' required>"
					+ "                </div>"
		            + "           </div>"
		            + "           <div class='form-group'>"
		            + "               <label class='form-label'><strong>Company Name : - </strong>"
		            + "               </label>"
		            + "                <span class='tips'></span>"
		            + "                <div class='controls'>"
					+ "                <input class='dateClassCommon form-control' disabled value='"+element.COMP_NAME+"' type='text' required>"
					+ "                </div>"
		            + "           </div>"
		            + "           <div class='form-group'>"
		            + "                <label class='form-label'><strong>Mode : - </strong>"
		            + "                </label>"
		            + "                <span class='tips'></span>"
		            +"<div id='orderModeDropDown'></div>"
		            + "           </div>  "
		            + "           <div class='form-group'>"
		            + "               <label class='form-label'><strong>Remark : - </strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "                <div class='controls'>"
					+ "                <input class='dateClassCommon form-control' value='"+element.REMARK+"' type='text' name='Remark' placeholder='Remark'>"
					+ "                </div>"
		            + "            </div>";
		          html+= "       </div>"
		            + "       <div class='col-md-4'>"
		            + "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Stockist : - </strong>"
		            + "               </label>"
		            + "               <span class='tips'></span>"
		            + "                <div class='controls'>"
					+ "                <input class='dateClassCommon form-control' disabled value='"+element.STOCKIST+"' type='text' name='Remark' required>"
					+ "                </div>"
		            + "           </div>"
		            + "          <div class='form-group'>"
		            + "               <label class='form-label'><strong>Order Date : - </strong>"
		            + "               </label>"
		            + "                <div class='controls'>"
					+ "                <input class='dateClassCommon form-control'  value='"+element.DATE+"' type='text' name='orderDate' id='orderDate' placeholder='Order Date' required>"
					+ "                </div>"
		            + "               <span class='tips'></span>"
		            + "            </div>"
		            + "            <div class='form-group'>"
		            + "                <label class='form-label'><strong>Order No. : - </strong>"
		            + "               </label>"
		            + "                <span class='tips'></span>"
		            + "                <div class='controls'>"
					+ "                <input class='dateClassCommon form-control' value='"+element.ORDER_NUM+"' type='text' name='orderNo' placeholder='Order Number'>"
					+ "                </div>"
		            + "           </div>"
					/*+"            <div class='form-group'>"
					+"            <label class='form-label'><strong>Order Copy : - </strong>"
					+"				<span class='tips'></span>"
					+"              <div id='imgDiv'>"
					+"              <img onClick='showImagePopup(\""+element.ORDER_COPY+"\")' src='"+element.ORDER_COPY+"' alt='Smiley face' height='100' width='100'>"
					+"              </div>"
					+"              </label>"
					+"              <span class='tips'></span>"
					+"              <input type='file' id='ORDER_FILE'  name='orderCopy' class='form-control'>"
					+"              </label>"
					+"              </div>"*/
		            +"              <div class='form-group'>"
		            +"              	<button type='button' onclick='editOutwardOrderCopyImagesListPopup("+OutWardOrderEntryRegistrationId+")' class='btn btn-success'>Edit Order Copy Document</button>"
				    +"              </div>"
		            + " </div>"
		            + " </div>"
		            + "</div>"
		            + "                       <div class='modal-footer'>"
					+ "                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editOutwardGoodsOrderEntryReg\")'>Update</button>"
					+ "                           <button type='button' onClick='hidePopUp()' class='btn btn-success'>Cancel</button>"                                                                               
					+ "                       </div>"
					+"</form>";
		          
		          
		      	var productInfo=element.PRODUCT_ARRAY;
		      	if(productInfo.length > 0)
		      		{
		       html +="                                                <div class='panel-body'>"
			        +"                                                    <div class='row'>"
				    + "                                                      <h5 class='modal-title' id='myModalLabel'><strong>Product Information</strong>"
		            + "            									        </h5>"
				    +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
				    +"                                                            <table class='table table-striped table-hover'>"
				    +"                                                                <thead class='no-bd'>"
					+"                                                                    <tr>"
					+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Product Name</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Quantity</strong>"
					+"                                                                        </th>"
					+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
					+"                                                                        </th>"
					+"                                                                    </tr>"
					+"                                                                </thead>"
					+"                                                                <tbody class='no-bd-y'>";
		      
				 $(productInfo).each(function(index,element){
				html+="                                                                    <tr style='text-align: center;' id='product_"+element.OUTWARD_PRODUCT_ORDER_ENRTY_REG_ID+"'>"
				+"                                                                        <td>"+ i++ +"</td>"
				+"                                                                        <td>"+element.PRODUCT_NAME+"</td>"
				+"                                                                        <td id='cases_"+element.OUTWARD_PRODUCT_ORDER_ENRTY_REG_ID+"'>"+element.QUANTITY+"</td>"
				+"																		  <td style='width: 214px;'>"
				+"																			<a class='edit btn btn-dark' onClick=\"editOutWardGoodsRegProductDetails('"+OutWardOrderEntryRegistrationId+"','"+element.OUTWARD_PRODUCT_ORDER_ENRTY_REG_ID+"','"+element.PRODUCT_NAME+"','"+element.QUANTITY+"')\"><i class='fa fa-pencil-square-o'></i></a> <a class='delete btn btn-danger' href='#' onclick=\"deleteOutWardGoodsRegProductDetails('"+element.OUTWARD_PRODUCT_ORDER_ENRTY_REG_ID+"')\"><i class='fa fa-times-circle'></i> </a> "
				+"														    				</td>"
				+"                                                                    </tr>"
				 });
				 html +="                                                                </tbody>"
					  +"                                                            </table>"
					  +"                                                        </div>"
					  +"                                                    </div>"
					  +"													<div id='subPopUpForProduct'></div>"
					  +"                                                </div>";
		      		}

		      	html+="                       <div class='modal-footer'>"
					+ "                           <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive11' onclick='addInvoice("+OutWardOrderEntryRegistrationId+","+element.PERCENTAGE+","+element.ORG_ID+")'>Add More Invoices</button>"
					+ "							  <div id='invoiceEntryPopup'></div>"
					+ "                       </div>";
		     	var invoiceInfo=element.INVOICE_ARRAY;
		      	if(invoiceInfo.length > 0)
		      		{
		         html +="                                                <div class='panel-body'>"
				      +"                                                    <div class='row'>"
				      + "                                                      <h5 class='modal-title' id='myModalLabel'><strong>Invoice Information</strong>"
		              + "            									        </h5>"
		              +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		              +"                                                            <table class='table table-striped table-hover'>"
		              +"                                                                <thead class='no-bd'>"
		              +"                                                                    <tr>"
		              +"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>Invoice Number</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>Invoice Date</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>Gross Amount</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>Tax Amount</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>Vat Amount</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>Net Amount</strong>"
		              +"                                                                        </th>"
		              +"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		              +"                                                                        </th>"
		              +"                                                                    </tr>"
		              +"                                                                </thead>"
		              +"                                                                <tbody class='no-bd-y'>";
		      
				 $(invoiceInfo).each(function(index,element){
				  html+="                                                                    <tr style='text-align: center;' id='invoice_"+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+"'>"
				   	  +"                                                                        <td>"+ j++ +"</td>"
				   	  +"                                                                        <td id='ivoiceNo_"+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+"'>"+element.INVOICE_NO+"</td>"
				   	  +"                                                                        <td id='invoiceDate_"+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+"'>"+element.INVOICE_DATE+"</td>"
				   	  +"                                                                        <td id='grossAmount_"+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+"'>"+element.GROSS_AMOUNT+"</td>"
				   	  +"                                                                        <td id='taxAmount_"+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+"'>"+element.TAX_AMOUNT+"</td>"
				   	  +"                                                                        <td id='vatAmount_"+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+"'>"+element.VAT_AMOUNT+"</td>"
				   	  +"                                                                        <td id='netAmount_"+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+"'>"+element.NET_AMOUNT+"</td>"
					  +"																		<td style='width: 214px;'>"
					  +"																		<a class='edit btn btn-dark' onClick=\"editOutWardGoodsInvoiceDetails('"+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+"')\"><i class='fa fa-pencil-square-o'></i></a> ";
				  	if(element.DISPATCH_STATUS=='No')
				  		{
				  		html+=" <a class='delete btn btn-danger' href='#' onclick=\"deleteOutWardGoodsInvoiceDetails('"+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+"')\"><i class='fa fa-times-circle'></i> </a>";
				  		}
					  html+="														    			 </td>"
				   	  +"                                                                    </tr>"
				 });
				 html+="                                                                </tbody>"
					 +"                                                            </table>"
					 +"                                                        </div>"
					 +"                                                    </div>"
					 +"													<div id='subPopUpForInvoiceEdit'></div>"
					 +"                                                </div>";
		      		}

	
		html+= "            <div class='modal-footer text-center'>"
            + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
            + "            </div>"
            + "        </div>"
            + "    </div>"
            + "</div>";
	$('#popup').html(html);
	getOrderModeForEdit(element.ORDER_MODE_ID);
	$("#orderDate").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	ajaxEditOutWardGoodsReg("editOutwardGoodsOrderEntryReg");
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block"); 
		});
}, 'json');	
}

function ajaxEditOutWardGoodsReg(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					alert(element.MSG);
					$('#loader').hide();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}
//
function editOutwardOrderCopyImagesListPopup(OutWardOrderEntryRegistrationId){
	editListOfImagesPopup(OutWardOrderEntryRegistrationId,"orderCopy",commonData.ORDER_COPY,"OUTWARD_GOODS_ORDER_COPY_IMAGE" , "/OutWardGoodsController/updateOutwardGoodsRegOrderCopyImage");
}
//Image updation Ajax call after getting result from server
function ajaxOutwardOrderCopyImageUpdate(data){
	if(data.RESULT==true) {
		commonData.ORDER_COPY = data.ORDER_COPY;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteOutwardOrderCopyImage(orderCopyImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OutWardGoodsController/deleteOutwardGoodsRegistrationOrderCopyImage', {
			orderCopyImageId : orderCopyImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.ORDER_COPY[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}

function getOrderModeForEdit(orderModeId)
{
	$.post(contextApplicationPath+'/OrderModeController/getOrderMode', function(data) {
		html="<select class='form-control' class='form-control' name='orderModeName' id='orderModeId'>"
			+"<option disabled value='-1'>Select Order Mode</option>";
		$(data).each(function(index, element) {
			if(element.ORDER_MODE_ID==orderModeId)
			html+="<option value='"+element.ORDER_MODE_ID+"' selected='selected'>"+element.ORDER_MODE_NAME+"</option>";
			else
			html+="<option value='"+element.ORDER_MODE_ID+"'>"+element.ORDER_MODE_NAME+"</option>";
		});
		html+="</select>";
		$('#orderModeDropDown').html(html);
	}, 'json');
}

function deleteOutWardGoodsRegProductDetails(OutWardProductOrderEnteryRegistrationId)
{
	
	var r = confirm("Do you want to Delete Product...!");
    if (r == true) 
    {
		$.post(contextApplicationPath+'/OutWardGoodsController/deleteOutWardOrderProduct',{OutWardProductOrderEnteryRegistrationId : OutWardProductOrderEnteryRegistrationId} ,function(data) {
			$(data).each(function(index, element) {
				$('#product_'+element.OutWardProductOrderEnteryRegistrationId+'').remove();
				alert(element.MSG);
			}); 
		}, 'json');
    }
}


function deleteOutWardGoodsInvoiceDetails(OUTWARD_ORDER_INVOICE_ENTRY_REG_ID)
{
	var r = confirm("Do you want to Delete Invoice...!");
    if (r == true) 
    {
		$.post(contextApplicationPath+'/OutWardGoodsController/deleteOutWardOrderInvoiceEntry',{OUTWARD_ORDER_INVOICE_ENTRY_REG_ID : OUTWARD_ORDER_INVOICE_ENTRY_REG_ID} ,function(data) {
			$(data).each(function(index, element) {
				$('#invoice_'+element.OUTWARD_ORDER_INVOICE_ENTRY_REG_ID+'').remove();
				alert(element.MSG);
			}); 
		}, 'json');
    }
}

function editOutWardGoodsRegProductDetails(OutWardOrderEntryRegistrationId,OutWardProductOrderEnteryRegistrationId,productName,quantity)
{
	
	// load latest product quantity
	var html="<div class='modal fade in' id='modal-responsive777' >"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive777\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Order Product</strong></h4>"
        + "            </div>";
	$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardOrderProductQuantity',{OutWardProductOrderEnteryRegistrationId:OutWardProductOrderEnteryRegistrationId}, function(data) {
		$(data).each(function(index, element) {
	            html+= "  				<div class='panel panel-default' style='border:0px;'>"
	            +"<form id='editOutWardOrderProduct' action='"+contextApplicationPath+"/OutWardGoodsController/updatetOutWardOrderProduct' method='post'>"
	            +"<input type='hidden' name='outWardOrderEntryRegistrationId' value='"+OutWardOrderEntryRegistrationId+"'>"
	            +"<input type='hidden' name='OutWardProductOrderEnteryRegistrationId' value='"+OutWardProductOrderEnteryRegistrationId+"'>"
	            + "						 <div class='panel-body'>"
	            + "    						  <div class='row'>"
				+"<div id='moreProductHtml'>";
			html+="								   <div id='from'>"
			 	+"                                               <div class='col-md-4'>"
			 	+"                                                    <div class='form-group' style='height: 56px;'>"
			 	+"                                                         <label class='form-label'><strong>Product</strong>"
			 	+"                                                         </label>"
			 	+"                                                         <span class='tips'></span>"
			 	+"                                                         <div class='controls'>"
			 	+"               											 <input class='dateClassCommon form-control' disabled value='"+productName+"' type='text' required>"
			 	+"                                                         </div>"
			 	+"                                                    </div>"
			 	+"                                               </div>"
			 	+"                                               <div class='col-md-4'>"
			 	+"                                                   <div class='form-group'>"
			 	+"                                                        <label class='form-label'><strong>Quantity</strong>"
			 	+"                                                        </label>"
			 	+"                                                        <span class='tips'></span>"
			 	+"                                                        <div class='controls'>"
			 	+"                                                             <input type='text' class='form-control' placeholder='Quantity' name='quantity' value='"+element.QUANTITY+"' parsley-type='onlynumber' required>"
			 	+"                                                        </div>"
			 	+"                                                   </div>"
			 	+"                                              </div>"
			 	+"             							   </div>"
			 	+"</div>"
			 	+ "                                   </div>"
			 	+ "    				 	              <div class='row'>"
			 	+ "                                    <div class='modal-footer text-center'>"
			 	+ "                                         <button type='submit' class='btn btn-danger' data-dismiss='modal' onclick='validateFormDetails(\"#editOutWardOrderProduct\")'>Update</button>"
			 	+ "                                         <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			 	+ "                                    </div>"
			 	+ "                               </div>"
			 	+ "                          </div>"
			 	+"</form>"
			 	+ "                     </div>"
			 	+ "          </div>"
			 	+ "     </div>"
			 	+ "</div>";
		});
		 $('#subPopUpForProduct').html(html);
		 ajaxEdidProductInOrder("editOutWardOrderProduct");
		 $("#modal-responsive777").addClass("in");
		 $("#modal-responsive777").attr("aria-hidden","false");
		 $("#modal-responsive777").css("display","block");
	}, 'json');
}

function ajaxEdidProductInOrder(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					alert(element.MSG);
					$('#loader').hide();
					$('#modal-responsive777').remove();
					$('#cases_'+element.OutWardProductOrderEnteryRegistrationId+'').html(element.quantity);
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function editOutWardGoodsInvoiceDetails(outWardOrderInvoiceEnteryRegistrationId)
{
	var INVOICE_DIV=1;
	var html="<div class='modal fade' id='modal-responsive777' aria-hidden='true'>"
		+"	<div class='col-md-13'>"
		+"		<div class='modal-content'>"
		+"			<div class='modal-header'>"
		+"				<button type='button' class='close' data-dismiss='modal' aria-hidden='true' onClick='hideSubPopUp(\"modal-responsive777\")'>×</button>"
		+"				<h4 class='modal-title' id='myModalLabel'><strong>Invoice Entry</strong></h4>"
		+"			</div>";
	$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardOrderInvoiceEnteryRegistrationObj',{outWardOrderInvoiceEnteryRegistrationId:outWardOrderInvoiceEnteryRegistrationId}, function(data) {
		$(data).each(function(index, element) {
			html+="<form id='updateInvoiceEntry' name='updateInvoiceEntry' action='"+contextApplicationPath+"/OutWardGoodsController/updateInvoiceEntry' method='post'>"
			 +"<input type='hidden' name='outWardOrderInvoiceEnteryRegistrationId' value='"+outWardOrderInvoiceEnteryRegistrationId+"'>"
//			 +"<input type='hidden' name='organizationId' value='"+organizationId+"'>"
//			 +"<input type='hidden' name='radioDiscount' value='' id='radioDiscountId'>"
			+"			<div class='modal-body' style=' text-align: left;'>"
			+"				<div class='row'>"
			+"<div id='moreInvoiceHtmlDiv'>"
			+"<div id='from'>"
			+"					<div class='col-md-4'>"
			+"						<div class='form-group'>"
			+"							<label for='field-1' class='control-label'>Invoice No</label></br>"
			+"							<input type='text' class='form-control'  name='invoiceNo' value='"+element.INVOICE_NO+"' id='invoiceNoId"+INVOICE_DIV+"' required>"
			+"						</div>"
			+"					</div>"
			+"					<div class='col-md-4'>"
			+"						<div class='form-group'>"
			+"							<label for='field-2' class='control-label'>Date</label>"
			+"							</br>"
			+"							<input class='datepicker form-control invoiceDate' type='text' value='"+element.INVOICE_DATE+"' style='height: 36px; padding-left: 10px;' name='invoiceDate' required>"
			+"						</div>"
			+"					</div>"
			+"					<div class='col-md-4'>"
			+"						<div class='form-group'>"
			+"							<label for='field-3' class='control-label'>Gross Amount</label>"
			+"							</br>"
			+"							<input type='text' class='form-control'  name='grossAmount' value='"+element.GROSS_AMOUNT+"' id='grossAmountId"+INVOICE_DIV+"'>"//onkeyup=\"handleChangeDiscount("+element.OutWardOrderEnteryRegistrationId+","+INVOICE_DIV+","+element.PERCENTAGE+")\"
			+"						</div>"
			+"					</div>"
			+"					<div class='col-md-4'>"
			+"						<div class='form-group'>"
			+"							<label for='field-5' class='control-label'>Tax Amount</label></br>"
			+"							<input type='text' class='form-control' name='taxAmount' value='"+element.TAX_AMOUNT+"' id='taxAmountId"+INVOICE_DIV+"' >" //onkeyup=\"handleChangeDiscount("+element.OutWardOrderEnteryRegistrationId+","+INVOICE_DIV+","+element.PERCENTAGE+")\"
			+"						</div>"
			+"					</div>"
			+"					<div class='col-md-4'>"
			+"						<div class='form-group'>"
			+"							<label for='field-3' class='control-label'>VAT Amount</label>"
			+"							</br>"
			+"							<input type='text' class='form-control'   name='vatAmount' value='"+element.VAT_AMOUNT+"' id='vatAmountId"+INVOICE_DIV+"'>" //onkeyup=\"handleChangeDiscount("+element.OutWardOrderEnteryRegistrationId+","+INVOICE_DIV+","+element.PERCENTAGE+")\" 
			+"						</div>"
			+"					</div>"
			+"					<div class='col-md-4'>"
			+"						<div class='form-group'>"
			+"							<label for='field-6' class='control-label'>Discount</label></br>"
			+"							<div class='col-lg-12'>"
			+"								<div class='pos-rel' style=' margin-top: 16px;'>"
			+"									<input   type='radio' class='rdoNumber' id='discount_yes' name='discount'  value='Yes' required>"//onclick=\"handleChangeDiscount("+element.OutWardOrderEnteryRegistrationId+","+INVOICE_DIV+","+element.PERCENTAGE+")\"
			+"									<label class='p-l-40' for='flat-checkbox-1'>Yes</label>"
			+"								</div>"
			+"								<div class='pos-rel' style=' margin-left: 100px; margin-top: -22px;'>"
			+"									<input   type='radio' class='rdoNumber' id='discount_no' name='discount'  value='No' required>"//onclick=\"handleChangeDiscount("+element.OutWardOrderEnteryRegistrationId+","+INVOICE_DIV+","+element.PERCENTAGE+")\"
			+"									<label class='p-l-40' for='flat-checkbox-2'>No</label>"
			+"								</div>"
			+"							</div>"
			+"						</div>"
			+"					</div>"
			+"					<div class='col-md-12'>"
			+"						<div class='form-group'>"
			+"							<label for='field-6' class='control-label'>Net Amount</label></br>"
			+"							<input type='text' class='form-control' name='netAmount' value='"+element.NET_AMOUNT+"' id='netAmountId"+INVOICE_DIV+"' parsley-type='number' required>"//readonly
			+"						</div>"
			+"					</div>"
			+"				</div>"
			+"</div>"
			+"</div>"
			+"				<div class='modal-footer text-center'>"
			+"					<button type='submit' class='btn btn-primary' onclick='validateFormDetails(\"#updateInvoiceEntry\")'>Submit</button>"
			+"					<button type='button' class='btn btn-danger'  onClick='hideSubPopUp(\"modal-responsive777\")'>Cancel</button>"
			+"				</div>"
			+"</form>"
			+"			</div>"
			+"		</div>"
			+"	</div>"
			+"</div>";
			$('#subPopUpForInvoiceEdit').html(html);
			$("#modal-responsive777").addClass("in");
			$("#modal-responsive777").attr("aria-hidden","false");
			$("#modal-responsive777").css("display","block");
			$(".invoiceDate").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
			
			if(element.DISCOUNT_YES_OR_NO=='Yes')
				{
				$("#discount_yes").prop("checked", true);
				}
			else
				{
				$("#discount_no").prop("checked", true);
				}
			ajaxEditInvoice('updateInvoiceEntry');
		});
	}, 'json');
}



function ajaxEditInvoice(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					alert(element.MSG);
					$('#loader').hide();
					$('#modal-responsive777').remove();
					$('#ivoiceNo_'+element.outWardOrderInvoiceEnteryRegistrationId+'').html(element.INVOICE_NO);
					$('#grossAmount_'+element.outWardOrderInvoiceEnteryRegistrationId+'').html(element.GROSS_AMOUNT);
					$('#taxAmount_'+element.outWardOrderInvoiceEnteryRegistrationId+'').html(element.TAX_AMOUNT);
					$('#vatAmount_'+element.outWardOrderInvoiceEnteryRegistrationId+'').html(element.VAT_AMOUNT);
					$('#netAmount_'+element.outWardOrderInvoiceEnteryRegistrationId+'').html(element.NET_AMOUNT);
					$('#invoiceDate_'+element.outWardOrderInvoiceEnteryRegistrationId+'').html(element.INVOICE_DATE);
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}


function changeDiscount(OutWardOrderEntryRegistrationId,invoiceDivNo,percentage)
{
	if($('.rdoNumber:checked').val()=='No')
	{
	var netAmount=parseFloat($("#grossAmountId"+invoiceDivNo+"").val())+parseFloat($("#taxAmountId"+invoiceDivNo+"").val())+parseFloat($("#vatAmountId"+invoiceDivNo+"").val());
	$("#netAmountId"+invoiceDivNo+"").val(parseFloat(netAmount).toFixed(4));
	}
else
	{
	var totalAmount=parseFloat($("#grossAmountId"+invoiceDivNo+"").val())+parseFloat($("#taxAmountId"+invoiceDivNo+"").val())+parseFloat($("#vatAmountId"+invoiceDivNo+"").val());
	var perAmount=(parseFloat(totalAmount)*percentage)/100;
	var netAmount=parseFloat(totalAmount)-parseFloat(perAmount);
	$("#netAmountId"+invoiceDivNo+"").val(parseFloat(netAmount).toFixed(4));
	}
}

function editTripEntryFunction(tripEntryId)
{
	var html="                                                <div class='modal fade' id='modal-responsive' aria-hidden='true'>"
		+"                                                    <div class='modal-dialog modal-lg'>"
		+"                                                        <div class='modal-content'>"
		+"                                                            <div class='modal-header'>"
		+"                                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true' onclick='hidePopUp()'>×</button>"
		+"                                                                <h4 class='modal-title' id='myModalLabel'><strong>Edit Trip Entry</strong></h4>"
		+"                                                            </div>";	
	$.post(contextApplicationPath+'/OutWardGoodsController/editOutWardTrip',{tripEntryId : tripEntryId}, function(data) {
		$(data).each(function(index, element) {
			html+="<form id='editTripEntry' action='"+contextApplicationPath+"/OutWardGoodsController/updateOutWardTrip' method='post'>"
			+"<input type='hidden'  value='"+element.ORG_ID+"' id='orgId' name='organizationId'>"
			+"<input type='hidden'  value='"+element.CARTING_AGENT_ID+"'  name='oldCartingAgentId'>"
			+"<input type='hidden'  value='"+tripEntryId+"'  name='tripEntryId'>"
			+"                                                            <div class='modal-body' style=' text-align: left;'>"
			+"                                                                <div class='row'>"
			+"                                                                    <div class='col-md-4'>"
			+"                                                                        <div class='form-group' style='height: 58px;'>"
			+"                                                                            <label for='field-1' class='control-label'><strong>Organization Name</strong></label></br>"
			+"                                                                            <div class='controls'>"
			+"                                                                            <input type='text' class='form-control'  value='"+element.ORG+"' readonly>"
			+"                                                                            </div>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='col-md-4'>"
			+"                                                                        <div class='form-group' style='height: 58px;'>"
			+"                                                                            <label for='field-1' class='control-label'><strong>Select Carting Agent</strong></label></br>"
			+"                                                                            <div class='controls'>"
			+"<div id='CartingAgentDropDown'></div>"
			+"                                                                            </div>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='col-md-4'>"
			+"                                                                        <div class='form-group'>"
			+"                                                                            <label for='field-6' class='control-label'><strong>Total Organization Trip Count</strong></label></br>"
			+"                                                                            <input type='text' class='form-control' id='org_trip_count_id' value='"+element.ORG_TRIP_COUNT+"' readonly>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='col-md-4'>"
			+"                                                                        <div class='form-group'>"
			+"                                                                            <label for='field-6' class='control-label'><strong>Carting Agent Trip Count</strong></label></br>"
			+"                                                                            <input type='text' class='form-control' id='carting_agent_trip_count_id' value='"+element.CARTING_AGENT_TRIP_COUNT+"' readonly>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='col-md-4'>"
			+"                                                                        <div class='form-group'>"
			+"                                                                            <label for='field-2' class='control-label'><strong>Loading Charges</strong></label>"
			+"                                                                            <input type='text' class='form-control' value='"+element.LOADING_CHARGES+"' name='loadingCharges' placeholder='Loading Charges'>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='col-md-4'>"
			+"                                                                        <div class='form-group'>"
			+"                                                                            <label for='field-5' class='control-label'><strong>Dispatch By</strong></label></br>"
			+"                                                                            <input type='text' class='form-control' id='field-4' value='"+element.DISPATCH_BY+"' name='dispatchBy' required>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='col-md-4'>                                                                        "
			+"                                                                        <div class='form-group'>"
			+"                                                                            <label for='field-3' class='control-label'><strong>Vehicle No</strong></label>"
			+"                                                                            <input type='text' class='form-control' id='VehicleNo' value='"+element.VEHICLE_NO+"' name='VehicleNo' readonly>"
			+"                                                                        </div>                                                                    "
			+"                                                                    </div>"
			+"                                                                    <div class='col-md-4'>"
			+"                                                                        <div class='form-group'>"
			+"                                                                            <label for='field-3' class='control-label'><strong>Driver No</strong></label>"
			+"                                                                            <input type='text' class='form-control' id='DriverNo' value='"+element.DRIVER_NO+"' name='DriverNo' readonly>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='col-md-4'>"
			+"                                                                        <div class='form-group'>"
			+"                                                                            <label for='field-3' class='control-label'><strong>No of Cases</strong></label>"
			+"                                                                            <input type='text' class='form-control' id='field-5' value='"+element.NO_OF_CASES+"' value='200' readonly name='totalNoOfCases'>"
			+"                                                                        </div>"
			+"                                                                    </div>";
		html+="                                                                </div>     "                                                       
			+"                                                                <div class='modal-footer text-center'>"
			+"                                                                    <button type='submit' class='btn btn-primary' onclick='validateFormDetails(\"#editTripEntry\")'>Update</button>"
			+"                                                                    <button type='button' class='btn btn-danger' data-dismiss='modal' onclick='hidePopUp()'>Cancel</button>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"</form>"
			+"                                                        </div>"
			+"                                                    </div>"
			+"                                                </div>";
		$('#popup').html(html);
		editOrgnizationCartingAgentUsignOrgId(element.ORG_ID,element.CARTING_AGENT_ID);
		ajaxEdidOutWardTripEntry("editTripEntry");
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block");
		}); 
	}, 'json');
}

function ajaxEdidOutWardTripEntry(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					alert(element.MSG);
					$('#modal-responsive').remove();
					viewTripEntry();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function editOrgnizationCartingAgentUsignOrgId(organizationId,selectedCartingAgentId)
{
	var html="<select class='form-control' class='form-control' name='cartingAgentId' id='cartingAgentId' onchange='laodCartingAgentTripCount()'>"
		+"<option disabled value='-1'>Select Carting Agent</option>";
			$.post(contextApplicationPath+'/CartingAgentController/cartingAgentListingUsignOrgId',{organizationId : organizationId} ,function(data) {
				$(data).each(function(index, element) {
					if(selectedCartingAgentId==element.CARTING_AGENT_ID)
						{
						html+="<option selected='selected' value='"+element.CARTING_AGENT_ID+"'>"+element.CARTING_AGENT_NAME+"</option>";
						}
					else
						{
						html+="<option value='"+element.CARTING_AGENT_ID+"'>"+element.CARTING_AGENT_NAME+"</option>";
						}
				}); 
				html+="</select>";
				$('#CartingAgentDropDown').html(html);
			}, 'json');
}

function editLRDetails(index)
{
	var html="" 
		+ "<div class='modal fade in' id='modal-responsive' >"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        +"<form id='editLrDetails' action='"+contextApplicationPath+"/OutWardGoodsController/updateLrDetails' method='post'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>View LR Details</strong></h4>"
        + "            </div>"
        + "  <div class='panel panel-default'>"
		+"<input type='hidden'  value='"+commonData[index].OUTWARD_GATEPASS_ID+"' id='OutWardGatePassId' name='OutWardGatePassId'>"
        + "<div class='panel-body'>"
        + "    <div class='row'>"
        + "       <div class='col-md-4'>";
   html+= "            <div class='form-group'>"
        + "                <label class='form-label'><strong>LR No : - </strong>"
        + "               </label>"
        + "                <span class='tips'></span>"
        + "                <div class='controls'>"
		+ "                <input class='dateClassCommon form-control' name='lrno' value='"+commonData[index].LR_NO+"' type='text' required>"
		+ "                </div>"
        + "           </div>";
      html+= "       </div>"
		+"            <div class='col-md-4'>"
		/*+"            <label class='form-label'><strong>Order Copy : - </strong>"
		+"				<span class='tips'></span>"
		+"              <div id='imgDiv'>"
		+"              <img onClick='showImagePopup(\""+LRImagePath+"\")' src='"+LRImagePath+"' alt='Smiley face' height='100' width='100'>"
		+"              </div>"
		+"              </label>"
		+"              <span class='tips'></span>"
		+"              <input type='file' id='lrImage'  name='lrImage' class='form-control'>"
		+"              </label>"*/
		+ "               <label class='form-label'><strong>Update LR Document :-</strong>"
	    + "               </label>"
		+ "                <span class='tips'></span>"
        +"              <div class='form-group'>"
        +"                  <button type='button' onclick='editOutwardAddLrImagesListPopup("+commonData[index].OUTWARD_GATEPASS_ID+","+index+")' class='btn btn-success'>Edit Claim Pictures Images</button>"
	    +"              </div>"
		+"              </div>"
        + " </div>"
        + " </div>"
        + "</div>"
        + "                                    <div class='modal-footer text-center'>"
	 	+ "                                         <button type='submit' class='btn btn-danger' data-dismiss='modal' onclick='validateFormDetails(\"#editLrDetails\")'>Update</button>"
	 	+ "                                         <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	 	+ "                                    </div>"
        + "        </div>"
    	+ "    </div>"
    	 +"</form>"
        + "</div>";
       
$('#popup').html(html);
ajaxEdidOutWardLrDetails("editLrDetails");
$("#modal-responsive").addClass("in");
$("#modal-responsive").attr("aria-hidden","false");
$("#modal-responsive").css("display","block"); 
}

function ajaxEdidOutWardLrDetails(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					alert(element.MSG);
					$('#modal-responsive').remove();
					viewGetPassLRList();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}
//
function editOutwardAddLrImagesListPopup(outwardGatePassId,index){
	//commonData2 is global var
	commonData2 = index;
	editListOfImagesPopup(outwardGatePassId,"lrImage",commonData[index].LR_IMAGE_PATH,"OUTWARD_GATE_PASS_LR_IMAGE" , "/OutWardGoodsController/updateOutwardGoodsGatePassLrImage");
}
//
function ajaxOutwardAddLrImageUpdate(data){
	if(data.RESULT==true) {
		commonData[commonData2].LR_IMAGE_PATH = data.LR_IMAGE_PATH;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Claim Picture Image of Inward Goods Reg
function deleteOutwardGoodsGatePassLrImage(lrImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OutWardGoodsController/deleteOutwardGoodsGatePassLrImage', {
			lrImageId : lrImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData[commonData2].LR_IMAGE_PATH[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}

function editCasesFunction(OutWardDispatchEnteryRegistrationId,noOfCases)
{
	var html="" 
		+ "<div class='modal fade in' id='modal-responsive' >"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        +"<form id='editNoOfCases' action='"+contextApplicationPath+"/OutWardGoodsController/updateNoOfCases' method='post'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Cases</strong></h4>"
        + "            </div>"
        + "  <div class='panel panel-default'>"
		+"<input type='hidden'  value='"+OutWardDispatchEnteryRegistrationId+"' id='OutWardDispatchEnteryRegistrationId' name='OutWardDispatchEnteryRegistrationId'>"
        + "<div class='panel-body'>"
        + "    <div class='row'>"
        + "       <div class='col-md-4'>";
   html+= "            <div class='form-group'>"
        + "                <label class='form-label'><strong>No of Cases : - </strong>"
        + "               </label>"
        + "                <span class='tips'></span>"
        + "                <div class='controls'>"
		+ "                <input class='dateClassCommon form-control' name='noOfCases' placeholder='No of Cases' value='"+noOfCases+"' type='text' parsley-type='onlynumber'>"
		+ "                </div>"
        + "           </div>";
   html+= "       </div>"
        + " </div>"
        + " </div>"
        + "</div>"
        + "                                    <div class='modal-footer text-center'>"
	 	+ "                                         <button type='submit' class='btn btn-danger' data-dismiss='modal' onclick='validateFormDetails(\"#editNoOfCases\")'>Update</button>"
	 	+ "                                         <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	 	+ "                                    </div>"
        + "        </div>"
    	+ "    </div>"
    	 +"</form>"
        + "</div>";
       
$('#popup').html(html);
ajaxEdidOutWardNoOfCases("editNoOfCases");
$("#modal-responsive").addClass("in");
$("#modal-responsive").attr("aria-hidden","false");
$("#modal-responsive").css("display","block");
}

function ajaxEdidOutWardNoOfCases(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					alert(element.MSG);
					$('#modal-responsive').remove();
					$('#noOfCases_'+element.OutWardDispatchEnteryRegistrationId+'').html(element.noOfCases);
					viewDispatchEntryRegistrationCases();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}