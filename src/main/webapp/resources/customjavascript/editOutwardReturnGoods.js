function editOutwardReturnGoodsRegDetails(mainIndex,ID_NO){
	$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/outwardReturnGoodsRegDetailsAndLrListing', {
		ID_NO : ID_NO ,
		editStatus : true
			} , function(data) {
				commonData = data;
				var i =1;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Outward Return Goods Registration Details</strong></h4>"
			        + "            </div>"
			        //+ "				 <form id='editOutwardReturnGoodsReg' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationController/editInwardReturnGoodsRegistration' method='post'>"
			        + "			       <div class='panel-body'>"
			        + "				 <form id='editOutwardReturnGoodsReg' action='"+contextApplicationPath+"/OutwardReturnGoodsRegistrationController/editOutwardReturnGoodsRegistration' method='post'>"
			        + "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
			        +"            												 <div id='selectOrganization' class='form-group'>"
					+"                                                                <label class='form-label'><strong>Select Organization : - </strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>COMPANY NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select id='companyName' name='companyName' onchange='getStockist()' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Company</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>STOCKIST NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select id='StockistName' name='stockistName' class='form-control'>"
					+ "                                                        			   <option disabled selected> Select Stockist</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTER NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select name='transporter' id='transName' class='form-control' required>"
					+ "                                                        			   <option disabled selected> Select Transporter</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DELIVERED EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+ "                                                    				<select id='employeeDropDown' name='employeeName' class='form-control'>"
					+ "                                                        			   <option disabled selected> Select Employee</option>"
					+ "                                                    				</select>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                   <input class='dateClassCommon form-control' value='"+data.REG_DATE+"' type='text' name='regDate' placeholder='Select Date' style='height: 36px; padding-left: 10px;' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>" 
					+"                                                                  <input type='text'  id='timePicker' name='time' value='"+data.TIME+"' placeholder='Time' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='driverNo' value= '"+data.DRIVER_NO+"' placeholder='DRIVER NO' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>                                             "            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NAME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='driverName' value='"+data.DRIVER+"' placeholder='DRIVER NAME ' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTATION CHARGES : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='transpCharges' value='"+data.TRANSPORTATION_CHARGES+"' placeholder='TRANSPORTATION CHARGES' parsley-type='number' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REASON : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='reason' value='"+data.REASON+"' placeholder='REASON' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"
					+ "						 </div>"
					+ " 			    </div>"
					+"						 <input type='hidden' value='"+data.ID_NO+"' name='regId'>"
					+"                       <div class='modal-footer'>"
					+"                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editOutwardReturnGoodsReg\")'>Update</button>"
					+"                           <button type='button' onClick='hidePopUp()' class='btn btn-success'>Cancel</button>"                                                                               
					+"                       </div>"
					+"				</form>"
					+"						<div class='row'>"
					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>LR No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>No of Cases</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Amount</strong>"
					+"											</th>      "
					+"											<th style='text-align: center;'><strong>LR Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Copy</strong>"
					+"											</th>                                           "
					+"											<th style='text-align: center;'><strong>Action</strong>"
					+"											</th>"
					+"										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
				var orgrLRArr = data.orgrLRArr;
					$(orgrLRArr).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ i +"</td>"
							+"                                         <td id='LR_NO_"+i+"'>"+element.LR_NO+"<input type='hidden' id='hid_LR_NO_"+i+"' value='"+element.LR_NO+"'></td>"
							+"                                         <td id='NO_OF_CASES_"+i+"'>"+element.NO_OF_CASES+"<input type='hidden' id='hid_NO_OF_CASES_"+i+"' value='"+element.NO_OF_CASES+"'></td>"
							+"                                         <td id='REF_CLAIM_NO_"+i+"'>"+element.REF_CLAIM_NO+"<input type='hidden' id='hid_REF_CLAIM_NO_"+i+"' value='"+element.REF_CLAIM_NO+"'></td>"
							+"                                         <td id='CLAIM_AMOUNT_"+i+"'>"+element.CLAIM_AMOUNT+"<input type='hidden' id='hid_CLAIM_AMOUNT_"+i+"' value='"+element.CLAIM_AMOUNT+"'></td>"
							+"                                         <td id='ATTACH_LR_"+i+"'>"
							+"												<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showOutwardLrDocImagesListPopup("+(i-1)+")'>"
							+"												<input type='hidden' id='hid_ATTACH_LR_"+i+"' value='"+element.ATTACH_LR+"'>"
							+" 										   </td>"
							+"                                         <td id='ATTACH_CLAIM_COPY_"+i+"'>"
							+"												<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showOutwardClaimCopyDocImagesListPopup("+(i-1)+")'>"
							+"												<input type='hidden' id='hid_ATTACH_CLAIM_COPY_"+i+"' value='"+element.ATTACH_CLAIM_COPY+"'>"
							+" 										   </td>"
							+"											<td style='width: 214px;'>"
							+"											  <a class='edit btn btn-dark' onclick='editOutwardReturnGoodsRegLrDetails("+element.LR_ID+","+i+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteOutwardReturnGoodsRegLrDetails("+ID_NO+","+element.LR_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"//<a class='edit btn btn-blue' onClick='viewOutwardReturnGoodsReg_LR_Details("+element.LR_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a>
							+"											</td>"
							+"										</tr>";
						 i++;
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"<div id='subPopUp'></div>"
					+"						</div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $(".dateClassCommon").datepicker({
						changeMonth : true,
						changeYear : true,
						yearRange : "-100:+0"
					});
				 $('#timePicker').timepicki();
				 ajaxEditOutwardReturnGoodsReg("editOutwardReturnGoodsReg",mainIndex)
				 displayEditOrganizationDropDown(2,data.ORG_ID);
				 getCompaniesDropdownForEdit(data.ORG_ID,data.COMPANY_ID);
				 getTransporterDropdownForEdit(data.ORG_ID,data.TRANSPORTER_ID);
				 loadEmployeeDropDownForEdit(data.ORG_ID,data.EMPLOYEE_ID);
				 getStockistForEdit(data.ORG_ID,data.COMPANY_ID,data.STOCKIST_ID);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
			}, 'json');
}
function ajaxEditOutwardReturnGoodsReg(FormId,mainIndex)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						$('#ID_NO_'+mainIndex).html(data.ID_NO);
						$('#ORG_'+mainIndex).html(data.ORGANIZATION);
						$('#COMPANY_'+mainIndex).html(data.COMPANY);
						$('#STOCKIST_'+mainIndex).html(data.STOCKIST);
						$('#TRAN_'+mainIndex).html(data.TRANSPORTER);
						alert(data.MSG);
						$('#loader').hide();
					}else
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
function editOutwardReturnGoodsRegLrDetails(LR_REG_ID,mainIndex){
				var i = 0;
				var html=""
					+ "<div class='modal fade ' id='modal-responsive777' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive777\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Outward Return Goods Registration LR Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"
			        + "				 <form id='editOutwardReturnGoodsRegLrDetails' action='"+contextApplicationPath+"/OutwardReturnGoodsRegistrationController/editOutwardReturnGoodsRegLrDetails' method='post'>"
			        + "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
					+"                                        					<div class='form-group'>"
					+"                                                                <label class='form-label'><strong>LR NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='lrNo' value='"+$("#hid_LR_NO_"+mainIndex).val()+"' placeholder='LR NO' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"      //hid_LR_NO_  hid_NO_OF_CASES_  hid_REF_CLAIM_NO_  hid_CLAIM_AMOUNT_  hid_ATTACH_LR_  hid_ATTACH_CLAIM_COPY_                                                      
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>NO OF CASES: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='noOfCases' value='"+$("#hid_NO_OF_CASES_"+mainIndex).val()+"' placeholder='NO OF CASES' class='form-control' parsley-type='onlynumber' required>"
					+"                                                                </div>"
					+"                                                            </div>"/*
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>LR COPY : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"  															  <div id='lrCopyImgId'>"
					+"                                                               	 <img id='lrCopyImgId' onClick='showImagePopup(\""+$("#hid_ATTACH_LR_"+mainIndex).val()+"\")' src='"+$("#hid_ATTACH_LR_"+mainIndex).val()+"' alt='Smiley face' height='100' width='100'>"
					+"  															  </div>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input id='lrCopyId' type='file'  name='attachLr' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>"*/// 
					+"                                                            <div class='form-group'>"
		            +"                                                     		    <button type='button' onclick='editLrCopyImagesForOutwardReturnGoodsListPopup("+LR_REG_ID+","+(mainIndex-1)+")' class='btn btn-success'>Edit LR Copy Document</button>"
				    +"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='refOrClaimNo' value= '"+$("#hid_REF_CLAIM_NO_"+mainIndex).val()+"' placeholder='CLAIM NO' class='form-control' required>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM AMOUNT : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input type='text'  name='claimAmount' value= '"+$("#hid_CLAIM_AMOUNT_"+mainIndex).val()+"' placeholder='CLAIM AMOUNT' class='form-control' parsley-type='number' required>"
					+"                                                                </div>"
					+"                                                            </div>    "/*
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM COPY : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"  															  <div id='lrClaimCopyImgId'>"
					+"                                                                  <img onClick='showImagePopup(\""+$("#hid_ATTACH_CLAIM_COPY_"+mainIndex).val()+"\")' src='"+$("#hid_ATTACH_CLAIM_COPY_"+mainIndex).val()+"' alt='Smiley face' height='100' width='100'>"//<label class='form-label'><strong>"+data.ATTACH_CLAIM_COPY_PATH+"</strong>"
					+"  															  </div>"
					+"                                                                <div class='controls'>"
					+"                                                                  <input id='claimCopyId' type='file'  name='attachClaimCopy' class='form-control'>"
					+"                                                                </div>"
					+"                                                            </div>                                             "*/    
					+"                                                            <div class='form-group'>"
		            +"                                                     		    <button type='button' onclick='editClaimCopyImagesForOutwardReturnGoodsListPopup("+LR_REG_ID+","+(mainIndex-1)+")' class='btn btn-success'>Edit Claim Copy Document</button>"
				    +"                                                            </div>"        
					+ "						 </div>"
					+ " 				   <input type='hidden' name='LR_REG_ID' value='"+LR_REG_ID+"'>"
					+ " 			    </div>"
					+"                       <div class='modal-footer'>"
					+"                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editOutwardReturnGoodsRegLrDetails\")'>Update</button>"
					+"                           <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-success'>Cancel</button>"                                                                               
					+"                       </div>"
					+" 					</form>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
					+"														</div>"
					+"													</div>"
					+"												</div>";
					 $("#subPopUp").html(html);
					 ajaxEditOutwardReturnGoodsRegLrDetails("editOutwardReturnGoodsRegLrDetails",mainIndex)
					 $("#modal-responsive777").addClass("in");
					 $("#modal-responsive777").attr("aria-hidden","false");
					 $("#modal-responsive777").css("display","block");
					
}
function ajaxEditOutwardReturnGoodsRegLrDetails(FormId,mainIndex)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						$('#LR_NO_'+mainIndex).html(data.LR_NO+"<input type='hidden' id='hid_LR_NO_"+mainIndex+"' value='"+data.LR_NO+"'>");
						$('#NO_OF_CASES_'+mainIndex).html(data.NO_OF_CASES+"<input type='hidden' id='hid_NO_OF_CASES_"+mainIndex+"' value='"+data.NO_OF_CASES+"'>");
						$('#REF_CLAIM_NO_'+mainIndex).html(data.REF_CLAIM_NO+"<input type='hidden' id='hid_REF_CLAIM_NO_"+mainIndex+"' value='"+data.REF_CLAIM_NO+"'>");
						$('#CLAIM_AMOUNT_'+mainIndex).html(data.CLAIM_AMOUNT+"<input type='hidden' id='hid_CLAIM_AMOUNT_"+mainIndex+"' value='"+data.CLAIM_AMOUNT+"'>");
						/*var htmlText = "<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showImagePopup(\""+data.ATTACH_LR+"\")'>"
							+"<input type='hidden' id='hid_ATTACH_LR_"+mainIndex+"' value='"+data.ATTACH_LR+"'>";
						$('#ATTACH_LR_'+mainIndex).html(htmlText);*/
						
						/*var htmlText = "<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showImagePopup(\""+data.ATTACH_CLAIM_COPY+"\")'>"
							+"<input type='hidden' id='hid_ATTACH_CLAIM_COPY_"+mainIndex+"' value='"+data.ATTACH_CLAIM_COPY+"'>";
						$('#ATTACH_CLAIM_COPY_'+mainIndex).html(htmlText);*/
						hideSubPopUp("modal-responsive777");
						alert(data.MSG);
						$('#loader').hide();
					}else
						alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
//LR Copy 
function editLrCopyImagesForOutwardReturnGoodsListPopup(lrDetailsId,index){
	//commonData2 global data var
	commonData2 = index;
	editListOfImagesPopup(lrDetailsId,"attachLr",commonData.orgrLRArr[index].ATTACH_LR,"OUTWARD_RETURN_GOODS_LR_DETAILS_LR_IMAGE" , "/OutwardReturnGoodsRegistrationController/updateOutwardReturnGoodsLrRegLrImage");
}
//Image updation Ajax call after getting result from server
function ajaxOutwardReturnGoodsLrRegLrImageUpdate(data){
	if(data.RESULT==true) {
		commonData.orgrLRArr[commonData2].ATTACH_LR = data.ATTACH_LR;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Copy Image of Outward Return Goods LR Reg
function deleteOutwardReturnGoodsLrRegLrImage(lrImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/deleteOutwardReturnGoodsLrRegLrImage', {
			lrImageId : lrImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.orgrLRArr[commonData2].ATTACH_LR[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//Claim Copy
function editClaimCopyImagesForOutwardReturnGoodsListPopup(lrDetailsId,index){
	//commonData2 global data var
	commonData2 = index;
	editListOfImagesPopup(lrDetailsId,"attachClaimCopy",commonData.orgrLRArr[index].ATTACH_CLAIM_COPY,"OUTWARD_RETURN_GOODS_LR_DETAILS_CLAIM_COPY_IMG" , "/OutwardReturnGoodsRegistrationController/updateOutwardReturnGoodsLrRegClaimCopyImage");
}
//Image updation Ajax call after getting result from server
function ajaxOutwardReturnGoodsLrRegClaimCopyImageUpdate(data){
	if(data.RESULT==true) {
		commonData.orgrLRArr[commonData2].ATTACH_CLAIM_COPY = data.ATTACH_CLAIM_COPY;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete Claim Copy Image of Inward Return Goods LR Reg
function deleteOutwardReturnGoodsLrRegClaimCopyImage(claimCopyImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/deleteOutwardReturnGoodsLrRegClaimCopyImage', {
			claimCopyImageId : claimCopyImageId
		}, function(data) {
			if(data.RESULT==true){
				commonData.orgrLRArr[commonData2].ATTACH_CLAIM_COPY[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
//
function editOutwardReturnGoodsRegCreditNoteInfo(index){
	//global var commonData2
	commonData2  = index;
	var html="" 
		+ "<div class='modal fade ' id='modal-responsive123' >"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive123\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Outward Return Goods Credit Note Details</strong></h4>"
        + "            </div>"
        + "				 <form id='editCreditNoteDetails' action='"+contextApplicationPath+"/OutwardReturnGoodsRegCreditNoteController/editOutwardReturnGoodsRegCreditNote' method='post'>"
        + "					<div class='panel-body'>"
        + "    			       <div class='row'>"
        + "       				   <div class='col-md-4'>"
        +"            												 <div id='selectOrganization' class='form-group'>"
		+"                                                                <label class='form-label'><strong>Select Organization : - </strong>"
		+"                                                                </label>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Credit Note No: - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='creditNoteNo' value='"+CreditNoteListArray[index].CREDIT_NOTE_NO+"' placeholder='Credit Note No' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"/*
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Attach Credit Note : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"  															  <div id='lrCopyImgId'>"
		+"                                                               	 <img id='lrCopyImgId' onClick='showImagePopup(\""+CreditNoteListArray[index].ATTACH+"\")' src='"+CreditNoteListArray[index].ATTACH+"' alt='Smiley face' height='100' width='100'>"
		+"  															  </div>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input id='lrCopyId' type='file'  name='attachCreditNote' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"*/
        +"                                                            <div class='form-group'>"
        +"                                                     		    <button type='button' onclick='editOutwardCreditNoteAttachedImagesListPopup("+CreditNoteListArray[index].CREDIT_NOTE_ID+")' class='btn btn-success'>Edit LR Images</button>"
		   +"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                             <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Ref No/Claim No : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='refNoOrClaimNo' value= '"+CreditNoteListArray[index].REF_CLAIM_NO+"' placeholder='Ref No/Claim No' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Credit Note Amount : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input type='text'  name='creditNoteAmount' value='"+CreditNoteListArray[index].CREDIT_NOTE_AMOUNT+"' placeholder='Credit Note Amount' class='form-control' parsley-type='number' required>"
		+"                                                                </div>"
		+"                                                            </div>    "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Remark : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                  <input  type='text'  name='remark' value='"+CreditNoteListArray[index].REMARK+"' placeholder='Remark' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>" 
		+"						 									  <input type='hidden' value='"+CreditNoteListArray[index].CREDIT_NOTE_ID+"' name='creditNoteId'>"
		+"						 									  <input type='hidden' value='"+CreditNoteListArray[index].ORG_ID+"' name='orgName'>"
		+ "						 </div>"
		+ "					</div>"
		+"                       <div class='modal-footer'>"
		+"                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editCreditNoteDetails\")'>Update</button>"
		+"                           <button type='button' onClick='hideSubPopUp(\"modal-responsive123\")' class='btn btn-success'>Cancel</button>"                                                                               
		+"                       </div>"
        + "        </div>"
        +"		</form>"
        + "</div>";
	$('#creditNoteInfo').html(html);
	ajaxEditOutwardReturnGoodsRegCreditNoteInfo("editCreditNoteDetails",index);
	 displayEditOrganizationDropDown(0,CreditNoteListArray[index].ORG_ID);
	$("#modal-responsive123").addClass("in");
	$("#modal-responsive123").attr("aria-hidden","false");
	$("#modal-responsive123").css("display","block"); 
}
function ajaxEditOutwardReturnGoodsRegCreditNoteInfo(FormId,mainIndex)
{
	var options = {
			success : function(data) {
					if(data.RESULT==true){
						//ORG_  CREDIT_NOTE_NO_  CREDIT_NOTE_AMOUNT_  REF_CLAIM_NO_
						$('#ORG_'+mainIndex).html(data.ORG);
						$('#CREDIT_NOTE_NO_'+mainIndex).html(data.CREDIT_NOTE_NO);
						$('#CREDIT_NOTE_AMOUNT_'+mainIndex).html(data.CREDIT_NOTE_AMOUNT);
						$('#REF_CLAIM_NO_'+mainIndex).html(data.REF_CLAIM_NO);
						//ORG_ID ORG CREDIT_NOTE_NO CREDIT_NOTE_AMOUNT REF_CLAIM_NO REMARK ATTACH
						CreditNoteListArray[mainIndex].ORG_ID = data.ORG_ID;
						CreditNoteListArray[mainIndex].ORG = data.ORG;
						CreditNoteListArray[mainIndex].CREDIT_NOTE_NO = data.CREDIT_NOTE_NO;
						CreditNoteListArray[mainIndex].CREDIT_NOTE_AMOUNT = data.CREDIT_NOTE_AMOUNT;
						CreditNoteListArray[mainIndex].REF_CLAIM_NO = data.REF_CLAIM_NO;
						CreditNoteListArray[mainIndex].REMARK = data.REMARK;
						//CreditNoteListArray[mainIndex].ATTACH = data.ATTACH;
						hideSubPopUp("modal-responsive123");
						alert(data.MSG);
						$('#loader').hide();
					}else
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
function editOutwardCreditNoteAttachedImagesListPopup(creditNoteId){
	//commonData2 is used as 'index'
	editListOfImagesPopup(creditNoteId,"attachCreditNote",CreditNoteListArray[commonData2].ATTACH,"OUTWARD_RETURN_GOODS_CREDIT_NOTE_ATTACH_IMGAGE" , "/OutwardReturnGoodsRegCreditNoteController/updateOutwardGoodsRegCreditNoteAttachImage");
}
//Image updation Ajax call after getting result from server
function ajaxOutwardReturnGoodsCreditNoteAttachImageUpdate(data){
	if(data.RESULT==true) {
		CreditNoteListArray[commonData2].ATTACH = data.ATTACH;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteOutwardReturnGoodsCreditNoteAttachImage(creditNoteAttachImageId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OutwardReturnGoodsRegCreditNoteController/deleteOutwardGoodsRegCreditNoteAttachImage', {
			creditNoteAttachImageId : creditNoteAttachImageId
		}, function(data) {
			if(data.RESULT==true){
				CreditNoteListArray[commonData2].ATTACH[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}