function editAdvanceChequeFunction(AdvanceChequeid)
{
	var j =1;
	var html="" 
					+ "<div class='modal fade in' id='modal-responsive' >"
		            + "    <div class='col-md-13'>"
		            + "        <div class='modal-content'>"
		            + "            <div class='modal-header'>"
		            + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		            + "                <h4 class='modal-title' id='myModalLabel'><strong>View Advance Cheque Information</strong></h4>"
		            + "            </div>"
		            + "  <div class='panel panel-default'>"
		            + "<form id='editStockistAdvanceCheck' action='"+contextApplicationPath+"/BankController/updateStockistAdvanceCheck' method='post'>"
		            + "<div class='panel-body'>"
		            + "    <div class='row'>"
		            + "       <div class='col-md-4'>";
					$.post(contextApplicationPath+'/BankController/editStockistAdvanceCheck',{AdvanceChequeid : AdvanceChequeid}, function(data) {
					$(data).each(function(index,element){
			    html+="            <div class='form-group'>"
			    	+"				<input type='hidden' name='advancedChequeInformationId' value='"+element.ADVANCE_CHEQUE_INFO_ID+"'> "			
		            + "                <label class='form-label'><strong>Organization Name : - </strong>"
		            + "               </label>"
		            + "                <span class='tips'></span>"
		            + "                <div class='controls'>"
					+ "                <input class='dateClassCommon form-control' disabled value='"+element.ORG_NAME+"' type='text'>"
					+ "                </div>"
		            + "           </div>"
		            + "           <div class='form-group'>"
		            + "               <label class='form-label'><strong>Company Name : - </strong>"
		            + "               </label>"
		            + "                <span class='tips'></span>"
		            + "                <div class='controls'>"
					+ "                <input class='dateClassCommon form-control' disabled value='"+element.COMP_NAME+"' type='text'>"
					+ "                </div>"
		            + "           </div>";
			    	if(element.COMPANY_BANK_STATUS=='Yes')
			    	{
					 html+= "           <div class='form-group'>"
			         + "               <label class='form-label'><strong>Company Bank : - </strong>"
			         + "               </label>"
			         + "                <span class='tips'></span>"
			         + "                <div class='controls'>"
					 + "                <input class='dateClassCommon form-control' disabled value='"+element.COMPANY_BANK+"' type='text' >"
					 + "                </div>"
			         + "           </div>";
			    	}  
		          html+= "       </div>"
		            + "       <div class='col-md-4'>";
		            if(element.STOCKIST_STATUS=='Yes')
		            	{
			            html+="           <div class='form-group'>"
				        + "               <label class='form-label'><strong>Stockist : -  </strong>"
				        + "               </label>"
				        + "               <span class='tips'></span>"
				        + "                <div class='controls'>"
						+ "                <input class='dateClassCommon form-control' disabled value='"+element.STOCKIST+"' type='text'>"
						+ "                </div>"
				        + "            </div>"
				        + "           <div class='form-group'>"
				         + "               <label class='form-label'><strong>Stockist Bank : - </strong>"
				         + "               </label>"
				         + "                <span class='tips'></span>"
				         + "                <div class='controls'>"
						 + "                <input class='dateClassCommon form-control' disabled value='"+element.STOCKIST_BANK+"' type='text' >"
						 + "                </div>"
				         + "           </div>";
		            	}
			            html+= "          <div class='form-group'>"
			            + "               <label class='form-label'><strong>Advanced Cheque Date : - </strong>"
			            + "               </label>"
			            + "                <div class='controls'>"
						+ "                <input class='dateClassCommon form-control'  value='"+element.DATE+"' type='text' name='advanceChequeDate' id='date' required>"
						+ "                </div>"
			            + "               <span class='tips'></span>"
			            + "            </div>"
			            + " </div>"
			            + " </div>"
			            + "</div>"
			            + "                       <div class='modal-footer'>"
						+ "                           <button type='Submit' class='btn btn-primary' onclick='validateFormDetails(\"#editStockistAdvanceCheck\")'>Update</button>"
						+ "                           <button type='button' onClick='hidePopUp()' class='btn btn-success'>Cancel</button>"                                                                               
						+ "                       </div>"
						+"</form>";
			
			     	var chequeInfo=element.CHEQUE_ARRAY;
			      	if(chequeInfo.length > 0)
			      		{
			       html +="                                                <div class='panel-body'>"
					    +"                                                    <div class='row'>"
					    +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					    +"                                                            <table class='table table-striped table-hover'>"
					    +"                                                                <thead class='no-bd'>"
						+"                                                                    <tr>"
						+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Cheque Number </strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
						+"                                                                        </th>"
						+"                                                                    </tr>"
						+"                                                                </thead>"
						+"                                                                <tbody class='no-bd-y'>";
					 $(chequeInfo).each(function(index,element){
					html+="<tr style='text-align: center;' id='cheque_"+element.CHEQUE_NO_INFO_ID+"'>"
						+"<td>"+ j++ +"</td>"
						+"<td id='ChequeNo_"+element.CHEQUE_NO_INFO_ID+"'>"+element.CHEQUE_NO+"</td>"
						+"<td id='oldChequeNoId_"+element.CHEQUE_NO_INFO_ID+"'>"
						+"<a class='edit btn btn-dark' onclick=\"editChequeNumberInformation('"+element.CHEQUE_NO_INFO_ID+"','"+element.CHEQUE_NO+"')\">"
						+"<i class='fa fa-pencil-square-o'></i>"
						+"</a>";
					if(element.DEPOSIT_STATUS==0)
						{
						html+=" <a class='delete btn btn-danger' href='#' onclick=\"deleteCheque('"+element.CHEQUE_NO_INFO_ID+"')\"><i class='fa fa-times-circle'></i> </a>";
						}
						
						html+="</td>"
						+"                                                                    </tr>"
					 });
					html+="                                                                </tbody>"
					+"                                                            </table>"
					+"                                                        </div>"
					+"                                                    </div>"
					+"<div id='subPopUpForChequeNumberInfo'></div>"
					+"                                                </div>";
					}
				html+= "            <div class='modal-footer text-center'>"
		            + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
		            + "            </div>"
		            + "        </div>"
		            + "    </div>"
		            + "</div>";
	$('#popup').html(html);
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block"); 
	$("#date").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
//	if(element.COMPANY_BANK_STATUS=='Yes')
//		{
//		editCompanyBank(element.COMPANY_BANK_ID,element.ORG_ID,element.COMPANY_ID);
//		}
//	else
//		{
//		editStokistBank(element.ORG_ID,element.COMPANY_ID,element.STOCKIST_ID,element.STOCKIST_BANK_ID);
//		}
		});
		ajaxEditStockistAdvancedCheque('editStockistAdvanceCheck');
}, 'json');	
}

function ajaxEditStockistAdvancedCheque(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					alert(element.MSG);
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}



function deleteCheque(CHEQUE_NO_INFO_ID)
{
	//alert(CHEQUE_NO_INFO_ID);
	var r = confirm("Do you want to Delete Cheque...!");
    if (r == true) 
    {
		$.post(contextApplicationPath+'/BankController/deleteCheque',{CHEQUE_NO_INFO_ID : CHEQUE_NO_INFO_ID} ,function(data) {
			$(data).each(function(index, element) {
				if(element.CRITERIA=='DELETE_STOCKIST_ADV_CHEQUE_ENTRY')
					{
					$('#cheque_'+element.CHEQUE_NO_INFO_ID+'').remove();
					$('#stockistAdvanceChequeEntry_'+element.AdvancedChequeInformationId+'').remove();
					alert(element.MSG);
					}
				else
					{
					$('#cheque_'+element.CHEQUE_NO_INFO_ID+'').remove();
					$('#noOfCheque_'+element.AdvancedChequeInformationId+'').html(element.NO_OF_CHEQUE);
					alert(element.MSG);
					}
			}); 
		}, 'json');
    }
}


function editCompanyBank(selectedCompanyBankId,orgId,companyId)
{
	
	var html="<select class='form-control' class='form-control' id='selectedbankId' name='bankID'>"
		+"<option value='-1'>Select Bank</option>";
		$.post(contextApplicationPath+'/BankController/getCompanyBankUsignCompanyIdAndOrgId',{organizationId:orgId,companyId:companyId}, function(data) {
			$(data).each(function(index, element) {
				if(selectedCompanyBankId==element.COMPANY_BANK_ID)
					{
					html+="<option selected value='"+element.COMPANY_BANK_ID+"'>"+element.BANK_NAME+"</option>";
					}
				else
					{
					html+="<option value='"+element.COMPANY_BANK_ID+"'>"+element.BANK_NAME+"</option>";
					}
			}); 
			html+="</select>";
			$('#bankList').html(html);
		}, 'json');
}


function editStokistBank(orgId,compId,stockistId,selectedBankDetailsId)
{
	var html="<select class='form-control' class='form-control'  id='selectedbankId' name='bankID'>"
		+"<option value='-1'>Select Bank</option>";
	$.post(contextApplicationPath+'/BankController/getStockistBankUsignCompanyIdAndOrgIdAndStockistId',{organizationId:orgId,companyId:compId,stockistId:stockistId}, function(data) {
		$(data).each(function(index, element) {
			if(selectedBankDetailsId==element.STOCKIST_BANK_DETAILS_ID)
				{
				html+="<option selected value='"+element.STOCKIST_BANK_DETAILS_ID+"'>"+element.BANK_NAME+"</option>";
				}
			else
				{
				html+="<option value='"+element.STOCKIST_BANK_DETAILS_ID+"'>"+element.BANK_NAME+"</option>";
				}
		});
		html+="</select>";
		$('#bankList').html(html);
	}, 'json');
}


function editChequeNumberInformation(ChequeNumberInfoId,ChequeNumber)
{
			var html="<div class='modal fade in' id='modal-responsive777' >"
		        + "    <div class='col-md-13'>"
		        + "        <div class='modal-content'>"
		        + "            <div class='modal-header'>"
		        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive777\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		        + "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Cheque Number Information</strong></h4>"
		        + "            </div>";
	
	            html+= "  				<div class='panel panel-default' style='border:0px;'>"
	            +"<form id='editChequrNumberInfo' action='"+contextApplicationPath+"/BankController/updatetChequeNumberInfo' method='post'>"
	            +"<input type='hidden' name='ChequeNumberInfoId' value='"+ChequeNumberInfoId+"'>"
	            + "						 <div class='panel-body'>"
	            + "    						  <div class='row'>";
			html+="								   <div id='from'>"
			 	+"                                               <div class='col-md-4'>"
			 	+"                                                   <div class='form-group'>"
			 	+"                                                        <label class='form-label'><strong>Cheque Number</strong>"
			 	+"                                                        </label>"
			 	+"                                                        <span class='tips'></span>"
			 	+"                                                        <div class='controls'>"
			 	+"                                                             <input type='text' class='form-control'  name='ChequeNumber' value='"+ChequeNumber+"' required>"
			 	+"                                                        </div>"
			 	+"                                                   </div>"
			 	+"                                              </div>"
			 	+"</div>"
			 	+ "                                   </div>"
			 	+ "    				 	              <div class='row'>"
			 	+ "                                    <div class='modal-footer text-center'>"
			 	+ "                                         <button type='submit' class='btn btn-danger' onclick='validateFormDetails(\"#editChequrNumberInfo\")'>Update</button>"
			 	+ "                                         <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			 	+ "                                    </div>"
			 	+ "                               </div>"
			 	+ "                          </div>"
			 	+"</form>"
			 	+ "                     </div>"
			 	+ "          </div>"
			 	+ "     </div>"
			 	+ "</div>";
		 $('#subPopUpForChequeNumberInfo').html(html);
		 ajaxEditChequeNumber("editChequrNumberInfo");
		 $("#modal-responsive777").addClass("in");
		 $("#modal-responsive777").attr("aria-hidden","false");
		 $("#modal-responsive777").css("display","block");
}

function ajaxEditChequeNumber(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#modal-responsive777').remove();
					alert(element.MSG);
					$('#ChequeNo_'+element.CHEQUE_NO_INFO_ID+'').html(element.CHEQUE_NO);
					var html="<a class='edit btn btn-dark' onclick=\"editChequeNumberInformation('"+element.CHEQUE_NO_INFO_ID+"','"+element.CHEQUE_NO+"')\">"
					+"<i class='fa fa-pencil-square-o'></i>"
					$('#oldChequeNoId_'+element.CHEQUE_NO_INFO_ID+'').html(html);
					+"</a>";
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}