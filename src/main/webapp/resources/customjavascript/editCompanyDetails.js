function editCompanyDetailsPopUp(companyId,mainIndex)
{

	var html="" 
		+ "<div class='modal fade in' id='modal-responsive' >"
	    + "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Company Details</strong></h4>"
		+ "            </div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>";
	$.post(contextApplicationPath+'/CompanyController/EditCompanyDetails', {companyId : companyId}, function(data) {
			$(data).each(function(index,element){
			html+="<form id='editCompanyDetails' action='"+contextApplicationPath+"/CompanyController/UpdateCompanyDetails' method='post'>"
			+"<input type='hidden' name='companyId' value='"+companyId+"'>"
		html+="                                                   <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Organization Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' disabled  value='"+element.ORG_NAME+"' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"                                                            
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Company Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' id='companyNameId' name='companyName' value='"+element.COMPANY_NAME+"' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>State*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control' required>"
			+"																		<option disabled selected> Select State</option>"
		    +"																	</select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>District*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control' required>"
			+"                                                                        <option disabled selected> Select District</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>City*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select name='city' id='cityName' class='form-control' class='form-control' required>"
			+"                                                                        <option disabled selected> Select City</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Contact Person</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' id='contactPersonId' name='contactPerson'  value='"+element.CONTACT_PERSON+"' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Mobile No</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' id='mobileNoId' name='mobile' value='"+element.MOBILE+"' class='form-control' parsley-type='onlynumber'>"
			+"                                                                </div>"
			+"                                                            </div>  "
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Website</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='website' value='"+element.WEBSITE+"' class='form-control' parsley-type='url'>"
			+"                                                                </div>"
			+"                                                            </div>    "
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Fax No</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='fax' value='"+element.FAX_NO+"' class='form-control' parsley-type='onlynumber'>"
			+"                                                                </div>"
			+"                                                            </div>                                             " 
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Service Tax No</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='service_tax_no' value='"+element.TAX_NO+"' class='form-control'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>PAN No*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                   <input type='text' name='pan_no' value='"+element.PAN_NO+"' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
//			+"                                                            <div class='form-group'>"
//			+"                                                                <label class='form-label'><strong>Role*</strong>"
//			+"                                                                </label>"
//			+"                                                                <span class='tips'></span>"
//			+"                                                                <div class='controls'>"
//			+"																	<div id='roleTypeDropDown'></div>";
//			+"                                                                </div>"
//			+"                                                                </div>"
//			+"                                                                <label class='form-label'><strong>Role*</strong>"
//			+"                                                                </label>"
			+"                                                            <div id='roleTypeDropDownEdit'></div>"
			+"                                                            <div id='roleType' class='form-group'>"
			+"                                                            </div>"    
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Address*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='textarea' name='address' value='"+element.ADDRESS+"' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Telephone*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='telephone' value='"+element.TELEPHONE+"' class='form-control' parsley-type='onlynumber' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Email ID*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' id='emailId' name='email' value='"+element.EMAIL_ID+"' class='form-control' required parsley-type='email'>"
			+"                                                                </div>"
			+"                                                            </div>  "
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>VAT*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='vat'  value='"+element.VAT+"' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>CST</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='cst'   value='"+element.CTS+"' class='form-control'>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>User Name*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' name='uname'   value='"+element.USER_NAME+"' class='form-control' required>"
			+"                                                                </div>"
			+"																  <div id='error' class='validationError' ></div>"
			+"                                                            </div>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Password*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='password' name='password'  value='"+element.PWD+"' class='form-control' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                    </div>"
			+ "            <div class='modal-footer text-center'>"
			+ "                <button type='submit'  class='btn btn-danger' onclick='validateFormDetails(\"#editCompanyDetails\")'>Update</button>"
	        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	        + "            </div>"
			+"</form>";
			
			html+="                                                </div>"
			    +"                                            </div>"
	            + "        </div>"
	            + "    </div>"
	            + "</div>";
			$('#popup').html(html);
			$("#modal-responsive").addClass("in");
			$("#modal-responsive").attr("aria-hidden","false");
			$("#modal-responsive").css("display","block"); 
			loadRoleTypeDropDownEdit(element.ROLE_TYPE_ID);
			stateDropDownListForEdit(element.STATE_ID);
			getDistrictForEdit(element.DISTRICT_ID,element.STATE_ID);
			getCitiesForEdit(element.DISTRICT_ID,element.CITY_ID);
			ajaxEditCompanyDetails("editCompanyDetails",mainIndex);
		});
	}, 'json');	
}
function ajaxEditCompanyDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						$('#COMPANY_NAME'+mainIndex).html($('#companyNameId').val());
						$('#CONTACT_PERSON'+mainIndex).html($('#contactPersonId').val());
						$('#MOBILE'+mainIndex).html($('#mobileNoId').val());
						$('#EMAIL'+mainIndex).html($('#emailId').val());
						$('#modal-responsive').remove();
						$('#loader').hide();
						alert(data.MSG);
					}else if(data.RESULT==false){
						$('#error').html(data.MSG);
						$('#loader').hide();
						alert(data.MSG);
					}
					else{
						$('#loader').hide();
						alert("Operation Failed....!");
					}
					
			}
		};
$('#' + FormId).ajaxForm(options);
}
//Edit company bank details
function editCompanyBankDetails(companyBankId,index)
{
	//alert("editCompanyBankDetails");
	var html=""
		+ "<div class='modal fade in' id='modal-responsive' >"
	    + "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Organization</strong></h4>"
		+ "            </div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"													<form id='editCompanyBankDetails' action='"+contextApplicationPath+"/CompanyController/updateCompanyBankDetails' method='post'>"
		+"													  <input type='hidden' name='companyBankId' value='"+companyBankId+"'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Organization Name :-</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' value='"+ commonData[index].ORG_NAME +"' class='form-control' disabled>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Branch*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id='bankBranchNameId' value='"+ commonData[index].BRANCH +"' class='form-control' name='branchName' required>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>IFSC Code*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text'  id='bankIfscCodeId' value='"+ commonData[index].IFSC +"' class='form-control' name='ifscCode' required>"
		+"                                                                </div>"
		+"                                                            </div>      "                                                 
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Company Name :-</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' value='"+ commonData[index].COMPANY_NAME +"' class='form-control' disabled>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Bank Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id='bankNameId'  value='"+ commonData[index].BANK_NAME +"' class='form-control' name='bankName' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Account No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id='bankAccoutNoId' value='"+ commonData[index].ACCOUNT_NO +"' class='form-control' name='accountNo' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+ "            										  <div class='modal-footer text-center'>"
		+ "                										<button type='submit'  class='btn btn-danger' onclick='validateFormDetails(\"#editCompanyBankDetails\")'>Update</button>"
        + "                										<button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "            										  </div>"
		+"													  </form>"
		+ "                                                </div>"
	    + "                                            </div>"
        + "        </div>"
        + "    </div>"
        + "</div>";
		$('#popup').html(html);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
			//loadOrganiztionDropDownForEdit(element.ORG_ID);
		ajaxEditCompanyBankDetails('editCompanyBankDetails',index);
}
function ajaxEditCompanyBankDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						commonData[mainIndex].BANK_NAME = $('#bankNameId').val();
						commonData[mainIndex].BRANCH = $('#bankBranchNameId').val();
						commonData[mainIndex].ACCOUNT_NO = $('#bankAccoutNoId').val();
						commonData[mainIndex].IFSC =  $('#bankIfscCodeId').val();
						
						$('#BANK_NAME'+mainIndex).html($('#bankNameId').val());
						$('#BRANCH_NAME'+mainIndex).html($('#bankBranchNameId').val());
						$('#ACCOUNT_NO'+mainIndex).html($('#bankAccoutNoId').val());
						$('#IFSC_CODE'+mainIndex).html($('#bankIfscCodeId').val());
						$('#modal-responsive').remove();
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
//
function editCompanyExecutiveDetails(companyExeId,index)
{
	//alert("editCompanyExecutiveDetails");
	var html=""
		+ "<div class='modal fade in' id='modal-responsive' >"
	    + "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Organization</strong></h4>"
		+ "            </div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"													<form id='editCompanyExecutiveDetails' action='"+contextApplicationPath+"/CompanyController/updateCompanyExecutiveDetails' method='post'>"
		+"													  <input type='hidden' name='companyExecutiveId' value='"+companyExeId+"'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Organization Name :-</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' value='"+ commonData[index].ORG_NAME +"' class='form-control' disabled>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Executive Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id='executiveNameId' value='"+ commonData[index].EXECUTIVE_NAME +"' class='form-control' name='executiveName' placeholder='Executive Name' required>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Mobile*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text'  id='mobileId' value='"+ commonData[index].MOBILE +"' class='form-control' name='mobile' placeholder='Mobile' parsley-type='onlynumber' required>"
		+"                                                                </div>"
		+"                                                            </div>      "                                                 
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Company Name :-</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' value='"+ commonData[index].COMPANY_NAME +"' class='form-control' disabled>"
		+"                                                                </div>"
		+"                                                            </div> "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Designation*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id='designationId'  value='"+ commonData[index].DESIGNATION +"' name='designation' class='form-control' placeholder='Designation' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Email ID*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' id='emailId' value='"+ commonData[index].EMAIL +"' name='email' class='form-control' placeholder='Email ID' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+ "            										  <div class='modal-footer text-center'>"
		+ "                										<button type='submit'  class='btn btn-danger'  onclick='validateFormDetails(\"#editCompanyExecutiveDetails\")'>Update</button>"
        + "                										<button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "            										  </div>"
		+"													  </form>"
		+ "                                                </div>"
	    + "                                            </div>"
        + "        </div>"
        + "    </div>"
        + "</div>";
		$('#popup').html(html);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
		ajaxEditCompanyExecutiveDetails('editCompanyExecutiveDetails',index);
}
function ajaxEditCompanyExecutiveDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						commonData[mainIndex].EXECUTIVE_NAME = $('#executiveNameId').val();
						commonData[mainIndex].DESIGNATION = $('#designationId').val();
						commonData[mainIndex].MOBILE = $('#mobileId').val();
						commonData[mainIndex].EMAIL =  $('#emailId').val();
						
						$('#EXECUTIVE_NAME'+mainIndex).html($('#executiveNameId').val());
						$('#DESIGNATION'+mainIndex).html($('#designationId').val());
						$('#MOBILE'+mainIndex).html($('#mobileId').val());
						$('#EMAIL'+mainIndex).html($('#emailId').val());
						$('#modal-responsive').remove();
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
//
function editCompanyDepoDetails(companyDepoId,mainIndex)
{
	//alert("editCompanyExecutiveDetails");
	$.post(contextApplicationPath+'/CompanyController/viewCompanyDepot', {companyDepoId : companyDepoId , editStatus : true}, function(data) {
		$(data).each(function(index,element){
			var html=""
				+ "<div class='modal fade in' id='modal-responsive' >"
			    +"    <div class='col-md-13'>"
				+"        <div class='modal-content'>"
				+"            <div class='modal-header'>"
				+"                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
				+"                <h4 class='modal-title' id='myModalLabel'><strong>View Organization</strong></h4>"
				+"            </div>"
				+"                                            <div class='panel panel-default'>"
				+"                                                <div class='panel-body'>"
				+"													<form id='editCompanyDepoDetails' action='"+contextApplicationPath+"/CompanyController/updateCompanyDepoDetails' method='post'>"
				+"													  <input type='hidden' name='companyDepoId' value='"+companyDepoId+"'>"
				+"                                                    <div class='row'>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-1' class='control-label'>Organization Name*</label>"
				+"                                                                <input type='text' value='"+element.ORG_NAME+"' disabled class='form-control' required>"
				+"                                                            </div>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-1' class='control-label'>Depo Name*</label>"
				+"                                                                <input type='text' id='depoNameId' name='depoName' value='"+element.DEPOT_NAME+"' class='form-control'  placeholder='Depo Name' required>"
				+"                                                            </div>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-2' class='control-label'>Designation</label>"
				+"                                                                <input type='text' name='designation' value='"+element.DESIGNATION+"' class='form-control' id='field-2' placeholder='Designation'>"
				+"                                                            </div>"
				+"                                                            <div class='form-group' style='height: 58px;'>"
				+"                                                                <label class='form-label'><strong>District*</strong>"
				+"                                                                </label>"
				+"                                                                <span class='tips'></span>"
				+"                                                                <div class='controls'>"
				+"                                                                    <select id='districtName' name='district' onchange='getCities()' class='form-control' class='form-control'>"
				+"                                                                        <option disabled selected> Select District</option>"
				+"                                                                    </select>"
				+"                                                                </div>"
				+"                                                            </div>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-6' class='control-label'>Telephone*</label>"
				+"                                                                <input type='text' name='telephone' value='"+element.TELEPHONE+"' class='form-control' id='field-6' placeholder='Telephone' parsley-type='onlynumber' required>"
				+"                                                            </div>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-8' class='control-label'>Email ID*</label>"
				+"                                                                <input type='text' name='email' id='emailId' value='"+element.EMAIL_ID+"' class='form-control'  placeholder='Email ID' required>"
				+"                                                            </div>"
				+"                                                            <div class='form-group'>" 
				+"                                                                <label for='field-9' class='control-label'>Website</label>"
				+"                                                                <input type='text' name='website' value='"+element.WEB_SITE+"' class='form-control' id='field-9' placeholder='Website'>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-1' class='control-label'>Company Name*</label>"
				+"                                                                <input type='text' disabled value='"+element.COMPANY_NAME+"' class='form-control' required>"
				+"                                                            </div>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-2' class='control-label'>Contact Person*</label>"
				+"                                                                <input type='text' id='contactPersonId' name='contactPerson' value='"+element.CONTACT_PERSON+"' class='form-control' placeholder='Conact Person' required>"
				+"                                                            </div>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label class='form-label'><strong>State*</strong>"
				+"                                                                </label>"
				+"                                                                <span class='tips'></span>"
				+"                                                                <div class='controls'>"
				+"                                                                    <select name='state' id='stateName' onchange='getDistrict()' class='form-control' class='form-control'>"
				+"																		<option disabled selected> Select State</option>"
			    +"																	</select>"
				+"                                                                </div>"
				+"                                                            </div>"
				+"                                                            <div class='form-group' style='height: 58px;'>"
				+"                                                                <label class='form-label'><strong>City*</strong>"
				+"                                                                </label>"
				+"                                                                <span class='tips'></span>"
				+"                                                                <div class='controls'>"
				+"                                                                    <select name='city' id='cityName' class='form-control' class='form-control'>"
				+"                                                                        <option disabled selected> Select City</option>"
				+"                                                                    </select>"
				+"                                                                </div>"
				+"                                                            </div>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-5' class='control-label'>Address*</label>"
				+"                                                                <input type='texareat' name='address' value='"+element.ADDRESS+"' class='form-control' id='field-5' placeholder='Address' required>"
				+"                                                            </div>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-7' class='control-label'>Mobile No</label>"
				+"                                                                <input type='text' name='mobile' id='mobileId' value='"+element.MOBILE+"' class='form-control' placeholder='Mobile No' parsley-type='onlynumber'>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                    </div>"
				+"                                                    <div class='row'>"
				+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
				+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
				+"                                                                <div class='pull-right'>"
				+"                                                                   <button type='submit' class='btn btn-primary' data-dismiss='modal' onclick='validateFormDetails(\"#editCompanyDepoDetails\")'>Update</button>"
				+"                                                                   <button type='button'  onClick='hidePopUp()' class='btn btn-danger'>Cancel</button>"
				+"                                                                </div>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                    </div>  "
				+"													  </form>"
				+ "                                                </div>"
			    + "                                            </div>"
		        + "        </div>"
		        + "    </div>"
		        + "</div>";
				$('#popup').html(html);
				stateDropDownListForEdit(element.STATE_ID);
				getDistrictForEdit(element.DISTRICT_ID,element.STATE_ID);
				getCitiesForEdit(element.DISTRICT_ID,element.CITY_ID);
				ajaxEditCompanyDepoDetails('editCompanyDepoDetails',index);
				$("#modal-responsive").addClass("in");
				$("#modal-responsive").attr("aria-hidden","false");
				$("#modal-responsive").css("display","block"); 
			});
		}, 'json');	
}
function ajaxEditCompanyDepoDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						//DEPO_NAME CONTACT_PERSON MOBILE EMAIL LINKS
						//depoNameId contactPersonId mobileId emailId
						$('#DEPO_NAME'+mainIndex).html($('#depoNameId').val());
						$('#CONTACT_PERSON'+mainIndex).html($('#contactPersonId').val());
						$('#MOBILE'+mainIndex).html($('#mobileId').val());
						$('#EMAIL'+mainIndex).html($('#emailId').val());
						if(data.InsertNewRecordFlag==true){
							var htmlText = "<a class='edit btn btn-blue' onClick='viewCompanyDepo("+data.DEPO_ID+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> <a class='edit btn btn-dark' onClick='editCompanyDepoDetails("+data.DEPO_ID+","+mainIndex+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteCompanyDepo("+data.DEPO_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>";
							$('#LINKS'+mainIndex).html(htmlText);
						}
						$('#modal-responsive').remove();
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
$('#' + FormId).ajaxForm(options);
}
//
function editCompanyProductDetails(companyProductId,mainIndex)
{
	//alert("editCompanyProductDetails");
	$.post(contextApplicationPath+'/CompanyController/viewCompanyProduct', {companyProductId : companyProductId}, function(data) {
		$(data).each(function(index,element){
			var html=""
				+ "<div class='modal fade in' id='modal-responsive' >"
			    + "    <div class='col-md-13'>"
				+ "        <div class='modal-content'>"
				+ "            <div class='modal-header'>"
				+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
				+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Organization</strong></h4>"
				+ "            </div>"
				+"                                            <div class='panel panel-default'>"
				+"                                                <div class='panel-body'>"
				+"													<form id='editCompanyProductDetails' action='"+contextApplicationPath+"/CompanyController/updateCompanyProductDetails' method='post'>"
				+"													  <input type='hidden' name='companyProductId' value='"+companyProductId+"'>"
				+"                                                    <div class='row'>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label class='form-label'><strong>Organization Name*</strong>"
				+"                                                                </label>"
				+"                                                                <span class='tips'></span>"
				+"                                                                <div class='controls'>"
				+"                                                                  <input type='text' value='"+element.ORG_NAME+"' disabled class='form-control'>"
				+"                                                                </div>"
				+"                                                            </div>"
				+"                                                        </div>                                "                            
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label class='form-label'><strong>Company Name*</strong>"
				+"                                                                </label>"
				+"                                                                <span class='tips'></span>"
				+"                                                                <div class='controls'>"
				+"                                                                  <input type='text' value='"+element.COMPANY_NAME+"' disabled class='form-control'>"
				+"                                                                </div>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-2' class='control-label'>Product Name*</label>"
				+"                                                                <input type='text' id='productNameId' name='productName' value='"+element.PRODUCT_NAME+"' class='form-control' placeholder='Product Name' required>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-2' class='control-label'>Product Code</label>"
				+"                                                                <input type='text' name='productCode' value='"+element.PRODUCT_CODE+"' class='form-control' id='field-2' placeholder='Product Code' required>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-3' class='control-label'>Type</label>"
				+"                                                                <input type='text' id='productTypeId' name='productType' value='"+element.TYPE+"' class='form-control' placeholder='TYPE' required>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-3' class='control-label'>MRP*</label>"
				+"                                                                <input type='text'name='mrp' id='mrpId' value='"+element.MRP+"' class='form-control' id='field-3' placeholder='MRP' parsley-type='number' required>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-3' class='control-label'>PTD*</label>"
				+"                                                                <input type='text' name='ptd' id='ptdId' value='"+element.PTD+"' class='form-control' id='field-3' placeholder='PTD' parsley-type='number' required>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-5' class='control-label'>Box Size*</label>"
				+"                                                                <input type='texareat' name='boxSize' value='"+element.BOX_SIZE+"' class='form-control' id='field-5' placeholder='Box Size' parsley-type='onlynumber' required>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                        <div class='col-md-4'>"
				+"                                                            <div class='form-group'>"
				+"                                                                <label for='field-6' class='control-label'>Unit Size*</label>"
				+"                                                                <input type='text' name='unitSize' value='"+element.UNIT_SIZE+"' class='form-control' id='field-6' placeholder='Unit Size' parsley-type='onlynumber' required>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                    </div>"
				+"                                                    <div class='row'>"
				+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
				+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
				+"                                                                <div class='pull-right'>"
				+"                                                                   <button type='submit' class='btn btn-primary' data-dismiss='modal' onclick='validateFormDetails(\"#editCompanyProductDetails\")'>Update</button>"
				+"                                                                    <button type='button' class='btn btn-danger' onClick='hidePopUp()'>Cancel</button>"
				+"                                                                </div>"
				+"                                                            </div>"
				+"                                                        </div>"
				+"                                                    </div>"
				+"													  </form>"
				+ "                                                </div>"
			    + "                                            </div>"
		        + "        </div>"
		        + "    </div>"
		        + "</div>";
				$('#popup').html(html);
				ajaxEditCompanyProductDetails('editCompanyProductDetails',index);
				$("#modal-responsive").addClass("in");
				$("#modal-responsive").attr("aria-hidden","false");
				$("#modal-responsive").css("display","block");
			});
		}, 'json');	
}
function ajaxEditCompanyProductDetails(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						$('#PRODUCT_NAME'+mainIndex).html($('#productNameId').val());
						$('#PTD'+mainIndex).html($('#ptdId').val());
						$('#MRP'+mainIndex).html($('#mrpId').val());
						$('#TYPE'+mainIndex).html($('#productTypeId').val());
						if(data.InsertNewProductFlag==true){
							//alert("New Product added..! Id = "+data.PRODUCT_ID);
							var htmlText = "<a class='edit btn btn-blue' onClick='viewCompanyProduct("+data.PRODUCT_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark' onClick='editCompanyProductDetails("+data.PRODUCT_ID+","+mainIndex+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> <a class='delete btn btn-danger' onClick='deleteCompanyProduct("+data.PRODUCT_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>";
							$('#LINKS'+mainIndex).html(htmlText);
						}
						$('#modal-responsive').remove();
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
	$('#loader').hide();
$('#' + FormId).ajaxForm(options);
}
//
function editCompanyCreditAndDiscount(CREDIT_DISCOUNT_ID,index)
{
	//$('#ORG_NAME_'+index).html("Sid");
	//alert("editCompanyProductDetails");
	var html=""
		+ "<div class='modal fade in' id='modal-responsive' >"
	    + "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Organization</strong></h4>"
		+ "            </div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"													<form id='editCompanyCreditAndDiscount' action='"+contextApplicationPath+"/CompanyController/updateCompanyCreditAndDiscountDetails' method='post'>"
		+"													  <input type='hidden' name='creditAndDiscountId' value='"+CREDIT_DISCOUNT_ID+"'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Organization Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' value='"+commonData[index].ORG_NAME+"' class='form-control' disabled><input type='hidden' name='orgId' value='"+commonData[index].ORG_ID+"'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Company Name*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' value='"+commonData[index].COMPANY+"' class='form-control' disabled><input type='hidden' name='companyId' value='"+commonData[index].COMPANY_ID+"'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover dataTable' id='table-editable'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong></strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Days</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Percentage %</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>"
		+"                                                                    <tr style='text-align: center;'>"
		+"                                                                        <td><strong>Local*</strong></td>"
		+"                                                                        <td><input type='text' id='localDaysId' name='localDays' value='"+commonData[index].LOCAL_DAYS+"' class='form-control' placeholder='Days' parsley-type='onlynumber' required></td>"
		+"                                                                        <td><input type='text' id='localPercentageId'  name='localPercentage' value='"+commonData[index].LOCAL_PER+"' class='form-control' placeholder='Percentage %' parsley-type='number' required></td>"
		+"                                                                    </tr>"
		+"                                                                    <tr style='text-align: center;'>"
		+"                                                                        <td><strong>Out Side Station*</strong></td>"
		+"                                                                        <td><input type='text' id='outsideDaysId'  name='outsideDays' value='"+commonData[index].OUTSTATION_DAYS+"' class='form-control' placeholder='Days' parsley-type='onlynumber' required></td>"
		+"                                                                        <td><input type='text' id='outsidePercentageId'  name='outsidePercentage' value='"+commonData[index].OUTSTATION_PER+"' class='form-control' placeholder='Percentage %' parsley-type='number' required></td>"
		+"                                                                    </tr>"
		+"                                                                </tbody>"
		+"                                                            </table>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                   <button type='submit' class='btn btn-primary' data-dismiss='modal' onclick='validateFormDetails(\"#editCompanyCreditAndDiscount\")'>Update</button>"
		+"                                                                   <button type='button' class='btn btn-danger' onClick='hidePopUp()'>Cancel</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"													  </form>"
		+ "                                                </div>"
	    + "                                            </div>"
        + "        </div>"
        + "    </div>"
        + "</div>";
		$('#popup').html(html);
		ajaxEditCompanyCreditAndDiscount('editCompanyCreditAndDiscount',index);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block");
}
function ajaxEditCompanyCreditAndDiscount(FormId,mainIndex)
{
	var options = {
			dataType : 'json',
			success : function(data) {
					if(data.RESULT==true){
						if(data.CREDIT_DISCOUNT_ID!=null||data.CREDIT_DISCOUNT_ID!=undefined){
							commonData[mainIndex].LOCAL_DAYS = $('#localDaysId').val();
							commonData[mainIndex].LOCAL_PER = $('#localPercentageId').val();
							commonData[mainIndex].OUTSTATION_DAYS = $('#outsideDaysId').val();
							commonData[mainIndex].OUTSTATION_PER = $('#outsidePercentageId').val();
							
							$('#LOCAL_DAYS_'+mainIndex).html($('#localDaysId').val());
							$('#LOCAL_PER_'+mainIndex).html($('#localPercentageId').val());
							$('#OUTSTATION_DAYS_'+mainIndex).html($('#outsideDaysId').val());
							$('#OUTSTATION_PER_'+mainIndex).html($('#outsidePercentageId').val());
							//alert("New record inserted..."+data.CREDIT_DISCOUNT_ID);
							var htmlText = "<a class='edit btn btn-dark' href='#' onclick='editCompanyCreditAndDiscount("+data.CREDIT_DISCOUNT_ID+","+mainIndex+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick='deleteCreditAndDiscountDetails("+data.CREDIT_DISCOUNT_ID+")'><i class='fa fa-times-circle'></i> </a>";
							$('#LINKS_'+mainIndex).html(htmlText);
						}
						$('#modal-responsive').remove();
					}
					$('#loader').hide();
					alert(data.MSG);
			}
		};
  $('#' + FormId).ajaxForm(options);
}
function loadRoleTypeDropDownEdit(roleId)
{
	var html="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Role Type*</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>"
		+"<select class='form-control' class='form-control' id='roleTypeIdEdit' name='roleTypeId'>" 
		+"<option disabled selected>Select Employee Role</option>";
	
//		+"<option value='-1'>Select Roll</option>";
//	 html+="<select class='form-control' class='form-control' name='roleType'>"
	
	$.post(contextApplicationPath+'/EmployeeController/loadRoleType', function(data) {
		$(data).each(function(index,element){
			if(roleId==element.ROLE_TYPE_ID)
			html+="<option selected='selected' value='"+element.ROLE_TYPE_ID+"'>"+element.ROLE_TYPE+"</option>";
			else
			html+= "<option value='" + element.ROLE_TYPE_ID+ "'>"+ element.ROLE_TYPE + "</option>";
		});
		html+=" </select>";
		$('#roleTypeDropDownEdit').html(html);
	}, 'json');
}
//
function editCompanyDocumentForm(index, from)
{
		var html = ""
			+ "<div class='modal fade in' id='modal-responsive' >"
	        + "    <div class='col-md-13'>"
	        + "        <div class='modal-content'>"
	        + "            <div class='modal-header'>"
	        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	        + "                <h4 class='modal-title' id='myModalLabel'><strong>Update Company Upload Document Details</strong></h4>"
	        + "            </div>"
	        + "												<form id='editCompanyDocumentForm' action='"+contextApplicationPath+"/CompanyController/updateCompanyDocumentInfo' method='post'>"
			+ "                                                <div class='panel-body'>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-4'>"
			+ "															   <input type='hidden' name='companyDocId' value='"+commonData[index].DOCUMENT_ID+"'>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Organization Name</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "                                                                    <input disabled type='text' value='"+commonData[index].ORG_NAME+"' class='form-control'>"
			+ "                                                                </div>"
			+ "                                                            </div>"
            +"                                                            <div class='form-group'>"
            +"                                                     		    <button type='button' onclick='editCompanyDocImagesListPopup("+commonData[index].DOCUMENT_ID+","+index+")' class='btn btn-success'>Edit Company Document Images</button>"
		    +"                                                            </div>"
			+ "                                                        </div>"
			+ "                                                        <div class='col-md-4'>"
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>Select Company*</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <select id='companyName' name='company' class='form-control' class='form-control'>"
			+"                                                                        <option disabled selected> Select Company</option>"
			+"                                                                    </select>"
			+"                                                                </div>"
			+"                                                            </div>"
			+ "                                                            <div class='form-group'>"
			+ "                                                                <label class='form-label'><strong>Document Type*</strong>"
			+ "                                                                </label>"
			+ "                                                                <span class='tips'></span>"
			+ "                                                                <div class='controls'>"
			+ "																		<div id='documentList'></div>"
	        + "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                    <div class='row'>"
			+ "                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "                                                            <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                                                <div class='pull-right'>"
			+ "                                                                    <button class='btn btn-primary m-b-10' onclick='showLoader()'>Submit</button>"
			+ "                                                                    <button type='reset' onclick='hidePopUp()' class='btn btn-danger m-b-10'>Cancel</button>"
			+ "                                                                </div>"
			+ "                                                            </div>"
			+ "                                                        </div>"
			+ "                                                    </div>"
			+ "                                                </div>"
			+ "												</form>"
			+ "                            </div>"
			+ "                        </div>" 
			+ "                    </div>";
		$('#popup').html(html);
		loadDocumentTypeListForEdit(commonData[index].DOCUMENT_TYPE_ID);
		getCompaniesDropdownForEdit(commonData[index].ORG_ID,commonData[index].COMPANY_ID);
		ajaxEditCompanyDocumentForm("editCompanyDocumentForm",index, from);
		$("#modal-responsive").addClass("in");
		$("#modal-responsive").attr("aria-hidden","false");
		$("#modal-responsive").css("display","block"); 
}
function ajaxEditCompanyDocumentForm(FormId,index, from) {
	var options = {
		success : function(data) {
				alert(data.MSG);
				if(data.RESULT==true){
					commonData[index].DOCUMENT_TYPE_ID = data.DOCUMENT_TYPE_ID;
					commonData[index].DOCUMENT_TYPE = data.DOCUMENT_TYPE;
					commonData[index].COMPANY_NAME = data.COMPANY_NAME;
					commonData[index].COMPANY_ID = data.COMPANY_ID;
					uploadedCompanyDocumentListingView(commonData, from,urlForEdit);
					//uploadedCompanyDocumentListing();
				}
				hidePopUp();
				$('#loader').hide();
		}
	};
	$('#' + FormId).ajaxForm(options);
}
//
function editCompanyDocImagesListPopup(documentId, index){
	//commonData2 is global var
	commonData2 = index ;
	editListOfImagesPopup(documentId,"documentImage",commonData[index].FILE_NAME,"COMPANY_UPLOAD_DOCUMENT_IMAGE" , "/CompanyController/updateCompanyUploadDocumentImage");
}
//Image updation Ajax call after getting result from server
function ajaxCompanyDocumentImageUpdate(data){
	if(data.RESULT==true) {
		commonData[commonData2].FILE_NAME = data.FILE_NAME;
		hideSubPopUp("editImagePopup7");
	}
	alert(data.MSG);
}
//Delete LR Image of Inward Goods Reg
function deleteCompanyDocumentImage(companyDocumentPathId,image_arrayIndex){
	var r = confirm("Do you want to Delete Document...!");
    if (r == true) {
		$.post(contextApplicationPath+'/CompanyController/deleteCompanyDocumentFileImage', {
			companyDocumentPathId : companyDocumentPathId
		}, function(data) {
			if(data.RESULT==true){
				commonData[commonData2].FILE_NAME[image_arrayIndex].IMAGE_PATH="";
				$("#spanId_"+image_arrayIndex).remove();
			}
			alert(data.MSG);
		}, 'json');
    }
}
/*//for edit org drop down
function loadOrganiztionDropDownForEdit(orgId)
{
	var userType='';
	var html="<div class='form-group' style='height: 58px;'>"
	+"<label class='form-label'><strong>Select Organization</strong>"
	+"</label>"
	+"<span class='tips'></span>"
	+"<div class='controls'>"
	+"<select class='form-control' class='form-control' id='organiztionId' name='organiztionNameId'>" 
	+"<option value='-1'>Select Organiztion</option>";
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
		$(data).each(function(index, element) {
			userType=element.USER_TYPE;
			if(element.USER_TYPE=='SuperAdmin')
				{
					if(orgId==element.ORG_ID)
						html+="<option selected='selected' value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
					else
						html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
				}
			else
				{
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='organiztionNameId'>" +
						""+element.ORG_NAME+"";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
		content+="</div>"
		+"</div>";
		if(userType=='SuperAdmin')
			{
			$('#organiztionDropDown').html(html);
			}
		else
			{
			$('#organiztionDropDown').html(content);
			}
		
	}, 'json');
}*/
		