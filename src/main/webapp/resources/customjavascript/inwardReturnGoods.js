function AddInwardReturnGoods(){
	var html=""
		+"<div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class=''>"
		+"                            <ul id='myTab2' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#tab2_1' onclick='inwardReturnGoodsRegistration()' data-toggle='tab'><h5><strong>Inward Return Goods Registration</strong></h5></a></li>"
		+"                                <li class=''><a href='#' data-toggle='tab' onclick=\"checkingDone()\"><h5><strong>Checking Pending</strong></h5></a></li>"
		+"                                <li class=''><a href='#' data-toggle='tab' onclick=\"AddCreditNote()\"><h5><strong>Checking Done</strong></h5></a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu' + lastClickId).hide();
	inwardReturnGoodsRegistration();
}

//
function subTabsForInwardReturnGoodsReg(flag){
	var html = ""
		+"                                        <ul  class='nav nav-tabs nav-dark'>"
		+"                                            <li id='mySubTabs1' class=''><a href='#tab2_3' onClick='inwardReturnGoodsRegistration()' data-toggle='tab'><h5><strong>Registration</strong></h5></a></li>"
		+"                                            <li id='mySubTabs2' class=''><a href='#tab2_4' onClick='viewInwardReturnGoodsRegListing()' data-toggle='tab'><h5><strong>View</strong></h5></a></li>"
		+"                                        </ul>";
	$('#subTabs').html(html);
	if(flag)
		$('#mySubTabs1').addClass("active");
	else
		$('#mySubTabs2').addClass("active");
	//inwardReturnGoodsRegistration();
}
//Global Variable for LR Form
var lrFormCounter;
var lrNoArr=[];
var noOfCasesArr=[];
var refOrClaimNoArr=[];
var claimAmountArr=[];
var openedLrFormTab ;
var LR_EDIT_STATUS=false;

//Global var for Add Product Product
var MultiArray = new Array();
var Counter;
var editStatus = false;
//currnetly opened popup
var openedProductTab;
//for detail view of product
var productListArray;

//Inward Return Goods Registration Tabs
function inwardReturnGoodsRegistration(){
	//reset global var
	lrFormCounter = 0;
	lrNoArr=[];
	noOfCasesArr=[];
	refOrClaimNoArr=[];
	claimAmountArr=[];	
	
	MultiArray = new Array();
	editStatus = false;
	openedProductTab="";
	Counter = 0;
	
	var html=""
		+"								<form id='saveInwardReturnGoodsReg' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationController/saveInwardReturnGoodsRegistration' method='post' enctype='multipart/form-data'>"
		+"                                <div class='tab-pane fade active in' id='tab2_1'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Inward Return Goods Registration</strong></h3>"
		+"                                    </div>"
		+"							  		  <div id='subTabs'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='cal1'>"
		+"                                            <div class='col-md-6'>"
		+"                                                <div class='panel panel-default'>"
		+"                                                    <div class='panel-body'>"
		+"                                                        <div class='row'>"
		+"                                                            <div class='col-md-12 col-sm-12 col-xs-12'>"
		/*+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>ID No</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='text' placeholder='ID No' class='form-control'>"
		+"                                                                    </div>"
		+"                                                                </div>"*/
		+"                                                            	  <div id='selectOrganization' class='form-group'>"
		+"                                                            	  </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Date*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input class='dateClassCommon form-control' type='text' id='date' name='regDate' placeholder='Select Date' style='height: 36px; padding-left: 10px;' onchange=\"errorMsgEmpty('date','dateError_msg')\">"
		+"												 							 <div id='dateError_msg' class='validationError' style='color:red'></div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Time*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='text'  id='timePicker' name='time' placeholder='Time' class='form-control' focusout=\"errorMsgEmpty('timePicker','timeError_msg')\">"
		+"                                                <div id='timeError_msg' class='validationError' style='color:red'></div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Company*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+ "                                                    					<select id='companyName' name='companyName' onchange='getStockist()' class='form-control' required>"
		+ "                                                        					<option disabled selected> Select Company</option>"
		+ "                                                    					</select>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Stockist*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                    <select id='StockistName' name='stockistName' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Stockist</option>"
		+"                                                                    </select>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Driver Name</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <input type='text' name='driverName' placeholder='Driver Name' class='form-control' >"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Delivered Employee*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <select id='employeeDropDown' name='employeeName'  class='form-control' required>"
		+"                                                                            <option disabled selected>Select Employee</option>"
		+"                                                                        </select>"
		+"                                                                    </div>"
		+"                                                                </div>"                                                       
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Transporter Name*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                    <select name='transporter' id='transName' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Transporter</option>"
		+"                                                                    </select>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Transportation Charges</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='text' name='transpCharges' placeholder='Transportation Charges' class='form-control' parsley-type='onlynumber' >"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Driver No</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='text' name='driverNo' placeholder='Driver No' class='form-control' >"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                        <div class='cal1'>"
		//LR NO Form
		+"										  <div id='lrNoForm'></div>"
		//LR NO Form
		//LR NO Form Listing
		+"                                            <div id='lrNoFormListing' class='col-md-6'>"
		+"                                            </div>"
		//LR NO Form Listing
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                <div class='pull-right'>"
		+"                                                    <button class='btn btn-primary m-b-10' onclick='return validateInwardsReturnGoodsForm(\"#saveInwardReturnGoodsReg\")'>Save</button>"//onclick='validateLrForm()'
		/*+"                                                    <button class='btn btn-success m-b-10' onclick='javascript:$('#form1').parsley('validate');'>Save & Mail</button>"*/
		+"                                                    <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>"
		+"							</form>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	subTabsForInwardReturnGoodsReg(true);
	$(".dateClassCommon").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	$('#timePicker').timepicki();
	dynamicLrNoForm();
	displayOrgDropDown(1);
	ajaxInwardReturnGoodsRegistrationForm('saveInwardReturnGoodsReg');
	
	//lrNoFormListing();
}
//




function ajaxInwardReturnGoodsRegistrationForm(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/},
			success : function(data) {
				if(data.result==true){
					inwardReturnGoodsRegistration();
					alert(data.MSG);
				}
				else{
					alert("Operation Failed");
				}
				$('#loader').hide();
			}
	};
	$('#saveInwardReturnGoodsReg').ajaxForm(options);
}

function validateInwardsReturnGoodsForm(formId)
{
	var z = $(formId).parsley('validate');
	var flag =  0;
	
	if($("#timePicker").val()==""||$("#timePicker").val()==undefined||$("#date").val()==""||$("#date").val()==undefined){
		var flag=false;
		if($("#timePicker").val()==""||$("#timePicker").val()==undefined){
			$("#timeError_msg").html("This value is required.");flag=true; 
		}
		if($("#date").val()==""||$("#date").val()==undefined){
			$("#dateError_msg").html("This value is required.");flag=true;
		}
		if(flag){
			$('#loader').hide();
			return false;
		}   
		return true;
	}
	if(z==true)
		{
		return validateLrForm();
//			  false;
		}
	return false;
}

function validateLrForm(){
	if(!(lrNoArr.length>0))
	{
		alert("Please Add at least One Entry for LR Form.....!");
		return false;
	}else{

		var emptyStat = true; 
		for(var i=0;i<lrNoArr.length;i++)
		{
			if(!(lrNoArr[i]==""))
			{
				emptyStat=false;break;
			}
		}
		if(emptyStat)
			{
			alert("Please Add at lest One Entry for LR Form.....!");
			return false;
			}
		else
			{
			showLoader();
        	return true;
			}
	}
}

function dynamicLrNoForm(){
	//alert("lrNo"+lrFormCounter);
	var lrDivId = "lrNo"+lrFormCounter;
	openedLrFormTab = lrFormCounter;
	if( $('#'+lrDivId).length ){
		//alert("Already exists");
		$("#"+lrDivId).show();
		lrNoFormListing();
	}else{
		Counter = 0;
		var html=""
		+"                                            <div id='lrNo"+lrFormCounter+"' class='col-md-6'>"
		+"                                                <div class='panel panel-default'>"
		+"                                                    <div class='panel-body'>"
		+"                                                        <div class='row'>"
		+"                                                            <div class='col-md-12 col-sm-12 col-xs-12'>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>LR No*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='text' name='lrNo' id='lrNoVal"+lrFormCounter+"' placeholder='LR No' class='form-control' onkeyup =\"lrNoValKeyUp("+lrFormCounter+")\">"
		+ "																	  <div id='lr_No_Error_Msg"+lrFormCounter+"' class='validationError' style='color:red'></div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>No of Cases*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='text' name='noOfCases' id='noOfCasesVal"+lrFormCounter+"' placeholder='No of Cases' class='form-control' onkeyup =\"noOfcasesKeyUp("+lrFormCounter+")\" >"
		+ "																	  <div id='noOfCases_Error_Msg"+lrFormCounter+"' class='validationError' style='color:red'></div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Ref No/Claim No*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='text' name='refOrClaimNo' id='refOrClainNoVal"+lrFormCounter+"' placeholder='Ref No/Claim No' class='form-control' onkeyup =\"refOrClaimNoKeyUp("+lrFormCounter+")\">"
		+"																	  <div id='refOrClainNoVal_Error_Msg"+lrFormCounter+"' class='validationError' style='color:red'></div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Claim Amount*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='text' name='claimAmount' id='claimAmountVal"+lrFormCounter+"' placeholder='Claim Amount' class='form-control' onkeyup =\"claimAmountKeyUp("+lrFormCounter+")\">"
		+"																	  <div id='claimAmountVal_Error_Msg"+lrFormCounter+"' class='validationError' style='color:red'></div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div  class='row'>"
		+"                                                            <div id='lrNoAddProduct"+lrFormCounter+"' class='col-md-3'>"
		+"                                                                <input type='button' value='Add Product'  class='btn btn-info m-b-10' onClick='addProductPopUp("+lrFormCounter+","+Counter+","+true+")' >"
		+"                                                            </div>"
		+"                                                        </div>"
		//pop up Add Product
		+"														  <div id='addProductPopUp"+lrFormCounter+"'></div>"
		//pop up Add Product
		+"                                                        <div class='row'>"
		+"                                                            <div class='col-md-12 col-sm-12 col-xs-12'>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Attach LR*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='file' name='attachLr' class='form-control' onchange='onLrImageChange(\"attachLr"+lrFormCounter+"_1\","+lrFormCounter+","+false+")' id='attachLr"+lrFormCounter+"_1'>"
		//+ "																	  <div id='attachLr_Error_Msg"+lrFormCounter+"' class='validationError' style='color:red'></div>"
		+"                                                                    </div>"
		+"                                                               </div>"
		+"                                                               <div id='lrImageDivId"+lrFormCounter+"'></div>"
		+"                                                               <div class='form-group'>"
		+"                                                				    <div id='lrImageAddMoreButtonId"+lrFormCounter+"'><button onclick='addFileConrolFor_LR_IMG("+lrFormCounter+", 1)' class='btn btn-primary'>Add More Files</button></div>"
		+"                                                               </div>"
		+"                                                                <div class='form-group'>"
		+"                                                                    <label class='form-label'><strong>Attach Claim Copy*</strong>"
		+"                                                                    </label>"
		+"                                                                    <span class='tips'></span>"
		+"                                                                    <div class='controls'>"
		+"                                                                        <input type='file' name='attachClaimCopy' class='form-control' onchange='onClaimCopyImageChange(\"attachClaimCopy"+lrFormCounter+"_1\","+lrFormCounter+","+false+")' id='attachClaimCopy"+lrFormCounter+"_1' >"
		//+ "																	  <div id='attachClaimCopy_Error_Msg"+lrFormCounter+"' class='validationError' style='color:red'></div>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                               <div id='claimCopyImageDivId"+lrFormCounter+"'></div>"
		+"                                                               <div class='form-group'>"
		+"                                                				    <div id='claimCopyImageAddMoreButtonId"+lrFormCounter+"'><button onclick='addFileConrolForClaimCopyIMG("+lrFormCounter+", 1)' class='btn btn-primary'>Add More Files</button></div>"
		+"                                                               </div>"
		+"                                                            </div>"
		+"															<input type='hidden' id='lrFileAttachedCountId"+lrFormCounter+"' value='0' name='lrFileAttachedCounter'>"
		+"															<input type='hidden' id='claimCopyFileAttachedCountId"+lrFormCounter+"' value='0' name='claimCopyFileAttachedCounter'>"
		+"                                                        </div>"
		+"                                                        <div class='row'>"
		+"                                                            <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                    <div class='pull-right'>"
		+"                                                                        <input type='button' value='Add' class='btn btn-primary m-b-10' onclick=\"validateLrNoDiv('"+lrFormCounter+"')\">" // validateLrNoDiv(lrFormCounter)
		+"                                                                        <input type='button' value='Cancel' type='reset' class='btn btn-danger m-b-10'>"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>         "
		+"                                                    </div>    "
		+"                                                </div>"
		+"                                            </div>";
		//$('#lrNoForm').empty();
		$('#lrNoForm').append(html);
	}
}

function validateLrNoDiv(lrFormCounter)
{
	/*if(lrNoVal(lrFormCounter))
		{
		if(noOfcases(lrFormCounter))
			{
			if(refOrClaimNo(lrFormCounter))
				{
				if(claimAmount(lrFormCounter))
					{
					if( attachLr(lrFormCounter))
						{
						if(attachClaimCopy(lrFormCounter))
							{
							submitLr_No_Form(lrFormCounter);
							return true;
							}
						}
					}
				}
			}
		}*/
	var status=true;
	if(!lrNoVal(lrFormCounter))
		status=false;
	if(!noOfcases(lrFormCounter))
		status=false;
	if(!refOrClaimNo(lrFormCounter))
		status=false;
	if(!claimAmount(lrFormCounter))
		status=false;
	/*if(!attachLr(lrFormCounter))
		status=false;		
	if(!attachClaimCopy(lrFormCounter))
		status=false;	*/
	if(status)
		submitLr_No_Form(lrFormCounter);
	else
		return false;
}

//function for LR File control
function addFileConrolFor_LR_IMG(lrFormdivId,index){
	//alert("addFileConrol "+index);
	index++;
	var html="                                                    <div id='lrImageDivId"+lrFormdivId+"_"+index+"' class='form-group'>"
	+"                                                                <div class='controls' style='width: 85%; float: left;'>"
	+"                                                                    <input type='file' class='form-control' name='attachLr' onchange='onLrImageChange(\"attachLr"+lrFormdivId+"_"+index+"\","+lrFormdivId+","+false+")' id='attachLr"+lrFormdivId+"_"+index+"'>"
	+"                                                            	  </div>"
	+"                                                                <div class='controls'>"
	+"                                                				  	  <div><a class='delete btn btn-danger' onclick='onLrImageFileDelete(\"lrImageDivId"+lrFormdivId+"_"+index+"\",\"attachLr"+lrFormdivId+"_"+index+"\","+lrFormdivId+")' style=' margin-left: 3px; width: 55px;height: 39px;'><i class='fa fa-times-circle' style='margin-left: -3px; margin-top: 3px;'></i> </a></div>"
	+"                                                            	  </div>"
	+"                                                            </div>";
	$('#lrImageDivId'+lrFormdivId).append(html);
	html="<button onclick='addFileConrolFor_LR_IMG("+lrFormdivId+","+index+")' class='btn btn-primary'>Add More Files</button>";
	$('#lrImageAddMoreButtonId'+lrFormdivId).html(html);
}
//function for claim Copy File control
function addFileConrolForClaimCopyIMG(lrFormdivId,index){ 
	//alert("addFileConrol "+index);
	index++;
	var html="                                                    <div id='claimCopyImageDivId"+lrFormdivId+"_"+index+"' class='form-group'>"
	+"                                                                <div class='controls' style='width: 85%; float: left;'>"
	+"                                                                    <input type='file' class='form-control' name='attachClaimCopy' onchange='onClaimCopyImageChange(\"attachClaimCopy"+lrFormdivId+"_"+index+"\","+lrFormdivId+","+false+")' id='attachClaimCopy"+lrFormdivId+"_"+index+"'>"
	+"                                                            	  </div>"
	+"                                                                <div class='controls'>"
	+"                                                				  	  <div><a class='delete btn btn-danger' onclick='onClaimCopyImageFileDelete(\"claimCopyImageDivId"+lrFormdivId+"_"+index+"\",\"attachClaimCopy"+lrFormdivId+"_"+index+"\","+lrFormdivId+")' style=' margin-left: 3px; width: 55px;height: 39px;'><i class='fa fa-times-circle' style='margin-left: -3px; margin-top: 3px;'></i> </a></div>"
	+"                                                            	  </div>"
	+"                                                            </div>";
	$('#claimCopyImageDivId'+lrFormdivId).append(html);
	html="<button onclick='addFileConrolForClaimCopyIMG("+lrFormdivId+","+index+")' class='btn btn-primary'>Add More Files</button>";
	$('#claimCopyImageAddMoreButtonId'+lrFormdivId).html(html);
}
//lrFileAttachedCountId lrFileAttachedCounter , claimCopyFileAttachedCountId claimCopyFileAttachedCounter
function onLrImageChange(id,lrFormdivId,selectStatus){
	//alert("onLrImageChange id="+id+",lrFormdivId="+lrFormdivId+",selectStatus="+selectStatus);
	var fileVal = $('#'+id).val();
	if(fileVal!=""&&fileVal!=undefined){
		if(!selectStatus){
			$("#"+id).attr("onchange", "onLrImageChange(\""+id+"\","+lrFormdivId+","+true+")");
			var counter = $('#lrFileAttachedCountId'+lrFormdivId).val();
			$('#lrFileAttachedCountId'+lrFormdivId).val(++counter);
		}
	}
	else{
		if(selectStatus){
			$("#"+id).attr("onchange", "onLrImageChange(\""+id+"\","+lrFormdivId+","+false+")");
			var counter = $('#lrFileAttachedCountId'+lrFormdivId).val();
			$('#lrFileAttachedCountId'+lrFormdivId).val(--counter);
		}
	}
	attachLrKeyUp(lrFormdivId);
}
function onLrImageFileDelete(id, imgId, lrFormdivId){
	//alert("onLrImageFileDelete id="+id+",lrFormdivId="+lrFormdivId+",imgId="+imgId);
	var fileData = $('#'+imgId).val();
	if(fileData!="" && fileData!=undefined){
		var counter = $('#lrFileAttachedCountId'+lrFormdivId).val();
		//alert("onLrImageFileDelete Before counter="+counter);
		$('#lrFileAttachedCountId'+lrFormdivId).val(--counter);
		//alert("onLrImageFileDelete after counter="+counter);
	}
	hideSubPopUp(id);
}
function onClaimCopyImageChange(id, lrFormdivId, selectStatus){
	//alert("onClaimCopyImageChange id="+id+",lrFormdivId="+lrFormdivId+",selectStatus="+selectStatus);
	var fileVal = $('#'+id).val();
	if(fileVal!=""&&fileVal!=undefined){
		if(!selectStatus){
			$("#"+id).attr("onchange", "onClaimCopyImageChange(\""+id+"\","+lrFormdivId+","+true+")");
			var counter = $('#claimCopyFileAttachedCountId'+lrFormdivId).val();
			$('#claimCopyFileAttachedCountId'+lrFormdivId).val(++counter);
		}
	}
	else{
		if(selectStatus){
			$("#"+id).attr("onchange", "onClaimCopyImageChange(\""+id+"\","+lrFormdivId+","+false+")");
			var counter = $('#claimCopyFileAttachedCountId'+lrFormdivId).val();
			$('#claimCopyFileAttachedCountId'+lrFormdivId).val(--counter);
		}
	}
	attachClaimCopyKeyUp(lrFormdivId);
}
function onClaimCopyImageFileDelete(id, imgId, lrFormdivId){
	//alert("onClaimCopyImageFileDelete id="+id+",lrFormdivId="+lrFormdivId+",imgId="+imgId);
	var fileData = $('#'+imgId).val();
	if(fileData!="" && fileData!=undefined){
		var counter = $('#claimCopyFileAttachedCountId'+lrFormdivId).val();
		//alert("onClaimCopyImageFileDelete Before counter="+counter);
		$('#claimCopyFileAttachedCountId'+lrFormdivId).val(--counter);
		//alert("onClaimCopyImageFileDelete after counter="+counter);
	}
	hideSubPopUp(id);
}
//
function submitLr_No_Form(formDivId){
	
	/*if(MultiArray[formDivId]!=undefined||MultiArray[formDivId]!=null){
		if(MultiArray[formDivId].length>0){
			//
			var emptyStat = true; 
			for(var i=0;i<MultiArray[formDivId].length;i++)
			{
				//if(MultiArray[addProdPopUpCounter] != undefined && MultiArray[addProdPopUpCounter].length>0){
				if(MultiArray[formDivId][i]!=undefined ){
					if(!(MultiArray[formDivId][i][0]==""))
					{
						emptyStat=false;break;
					}
				}
			}
			if(emptyStat)
				{
				alert("Please add atleast one Product for LR Form Entry.....!");
				}
			else{*/
				
				//alert($("#lrNoVal"+formDivId).val());
				if( $("#lrNoVal"+formDivId).val()==""|| $("#noOfCasesVal"+formDivId).val()==""||$("#refOrClainNoVal"+formDivId).val()==""||$("#claimAmountVal"+formDivId).val()=="")
				{	
					alert("Please Fill All the fields of LR Form...!");
					return false;
				}
				else{
				lrNoArr[formDivId] = $("#lrNoVal"+formDivId).val();
				noOfCasesArr[formDivId] = $("#noOfCasesVal"+formDivId).val();
				refOrClaimNoArr[formDivId] = $("#refOrClainNoVal"+formDivId).val();
				claimAmountArr[formDivId] = $("#claimAmountVal"+formDivId).val();
				lrNoFormListing();
				//initialization
				Counter = 0;
				}
				$('#lrNo'+formDivId).hide();
				if(LR_EDIT_STATUS==true)
					LR_EDIT_STATUS=false;
				else
					lrFormCounter++;
				
				dynamicLrNoForm();
			/*}
		}else
			alert("Please add atleast one Product for LR Form Entry.....!");
	}
	else{
		alert("Please add atleast one Product for LR Form Entry.....!");
	}*/
}
//LR No Form Listing
function lrNoFormListing(){
	
	var html = ""
		+"                                                <div class='panel panel-default'>"
		+"                                                    <div class='panel-body'>"
		+"                                                        <div class='row'>"
		+"                                                            <div class='col-md-6'>"
		+"                                                                <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                    <table class='table table-striped table-hover' style=' margin-left: -40px;'>"
		+"                                                                        <thead class='no-bd'>"
		+"                                                                            <tr>"
		+"                                                                                <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>LR No</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>No of Cases</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>Ref No/Claim No</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>Claim Amount</strong>"
		+"                                                                                </th>"
		/*+"                                                                                <th style='text-align: center;'><strong>Attach LR</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>Attach Claim Copy</strong>"
		+"                                                                                </th>"*/
		+"                                                                                <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                </th>"
		+"                                                                            </tr>"
		+"                                                                        </thead>"
		+"                                                                        <tbody class='no-bd-y'>";
																					var rowCount = 0;
																					for (var counter = 0; counter < lrNoArr.length; counter++) {
																						if(lrNoArr[counter]!=""){
																						html+="<tr style='text-align: center;'>"
																							+" <td>"+(rowCount+1)+"</td>"
																							+" <td>"+lrNoArr[counter]+"</td>"
																							+" <td>"+noOfCasesArr[counter]+"</td>"
																							+" <td>"+refOrClaimNoArr[counter]+"</td>"
																							+" <td>"+claimAmountArr[counter]+"</td>"
																							+" <td><a class='edit btn btn-dark' onClick='editLrNoFormEnrty("+counter+")'  href='javascript:;'><i class='fa fa-external-link'></i></a> <a class='delete btn btn-danger' onClick='deleteLrNoFormEnrty("+counter+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>" //<td><a class='edit btn btn-blue' onClick='editLrNoFormEnrty("+counter+")'  href='javascript:;'><i class='fa fa-external-link'></i></a>
																							+"</tr>";
																						rowCount++;
																						}
																					}
	html+="                                                                        </tbody>"
		+"                                                                    </table>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div> "
		+"                                                    </div>"
		+"                                                </div>";
	$('#lrNoFormListing').html(html);
}

//for edit LR FORM
function editLrNoFormEnrty(entryNo){
	$("#lrNo"+openedLrFormTab).hide();
	$("#lrNo"+entryNo).show();
	alert("Form Is Ready To Edit");
	LR_EDIT_STATUS=true;
	openedLrFormTab = entryNo;
}
//
function deleteLrNoFormEnrty(entryNo){
	//alert("deleteLrNoFormEnrty");
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$("#lrNo"+entryNo).remove();
		lrNoArr[entryNo] = "";
		noOfCasesArr[entryNo] = "";
		refOrClaimNoArr[entryNo] = "";
		claimAmountArr[entryNo]="";
		lrNoFormListing();
    }
}


function checkDate2(index,dateType)
{  
	if($('#expiryDate'+index).val()!=""&&$('#mfgDate'+index).val()!="")
		if($('#expiryDate'+index).val()<=$('#mfgDate'+index).val())
		{
			alert("Manufacturing date should be less than expiry date...!");
			if(dateType==1)
				$('#mfgDate'+index).val("");
			else if(dateType==2)
				$('#expiryDate'+index).val("");
		}
}

//productName claimQuantity receivedQuantity batch mfgDate expiryDate mfgCompany reason quantityVariance
function addProductPopUp(addProdPopUpCounter,counter,stat){
	Counter = counter;
	var popUpId = "popUpId"+addProdPopUpCounter+"_"+counter;
	openedProductTab = counter;
	if( $('#'+popUpId).length ){
		//alert("Already exists");
		$("#"+popUpId).addClass("in");
		 $("#"+popUpId).attr("aria-hidden","false");
		 $("#"+popUpId).css("display","block");
		 addProductListing(addProdPopUpCounter,counter);
	}else{
		var html=""
			+"                                                        <div class='modal fade' id='"+popUpId+"' aria-hidden='true'>"
			+"                                                            <div class='col-md-13'>"
			+"                                                                <div class='modal-content'>"
			+"                                                                    <div class='modal-header'>"
			+"                                                                        <input value='x' type='button' class='close' onClick='hidePopUpAddProduct(\""+popUpId+"\","+addProdPopUpCounter+","+counter+")' aria-hidden='true'>"
			+"                                                                        <h4 class='modal-title' id='myModalLabel'><strong>Add Inward Return Product</strong></h4>"
			+"                                                                    </div>"
			+"                                                                    <div class='modal-body'>"
			+"                                                                        <div class='row'>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-1' class='control-label'>Product Name</label>"
			+"                                                                                    <input type='text' id='productName"+addProdPopUpCounter+"_"+counter+"' name='productName' class='form-control' id='field-1' placeholder='Product Name'  onkeyup =\"productNameKeyUp("+addProdPopUpCounter+","+counter+")\">"
			+ "																	                  <div id='productName_Error_Msg"+addProdPopUpCounter+"_"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-2' class='control-label'>Claim Quantity</label>"
			+"                                                                                    <input type='text' id='claimQuantity"+addProdPopUpCounter+"_"+counter+"' name='claimQuantity' class='form-control' id='field-2' placeholder='Claim Quantity' onkeyup =\"claimQuantityKeyUp("+addProdPopUpCounter+","+counter+")\">"
			+ "																	                  <div id='claimQuantity_Error_Msg"+addProdPopUpCounter+"_"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-3' class='control-label'>Received Quantity</label>"
			+"                                                                                    <input type='text' id='receivedQuantity"+addProdPopUpCounter+"_"+counter+"' name='receivedQuantity' class='form-control' id='field-3' placeholder='Received Quantity' onkeyup =\"receivedQuantityKeyUp("+addProdPopUpCounter+","+counter+")\">"
			+ "																	                  <div id='receivedQuantity_Error_Msg"+addProdPopUpCounter+"_"+counter+"' class='validationError' style='color:red'></div>"
			
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-4' class='control-label'>Batch</label>"
			+"                                                                                    <input type='text' id='batch"+addProdPopUpCounter+"_"+counter+"' name='batch' class='form-control' id='field-4' placeholder='Batch' onkeyup =\"batchKeyUp("+addProdPopUpCounter+","+counter+")\">"
			+ "																	                  <div id='batch_Error_Msg"+addProdPopUpCounter+"_"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-5' class='control-label'>MFG Date</label>"
			+"                                                                                    <input id='mfgDate"+addProdPopUpCounter+"_"+counter+"' onChange=\"checkDate2('"+addProdPopUpCounter+"_"+counter+"',1)\" name='mfgDate' class='dateClassCommon form-control' id='field-5' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' onkeyup =\"mfgDateKeyUp("+addProdPopUpCounter+","+counter+")\">"
			+ "																	                  <div id='mfgDate_Error_Msg"+addProdPopUpCounter+"_"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-6' class='control-label'>Expiry Date</label>"
			+"                                                                                    <input id='expiryDate"+addProdPopUpCounter+"_"+counter+"' onChange=\"checkDate2('"+addProdPopUpCounter+"_"+counter+"',2)\" name='expiryDate' class='dateClassCommon form-control' id='field-6' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' onkeyup =\"expiryDateKeyUp("+addProdPopUpCounter+","+counter+")\">"
			+ "																	                  <div id='expiryDate_Error_Msg"+addProdPopUpCounter+"_"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-7' class='control-label'>MFG Company</label>"
			+"                                                                                    <input id='mfgCompany"+addProdPopUpCounter+"_"+counter+"' name='mfgCompany' type='text' class='form-control' id='field-7' placeholder='MFG Company' onkeyup =\"mfgCompanyKeyUp("+addProdPopUpCounter+","+counter+")\">"
			+ "																	                  <div id='mfgCompany_Error_Msg"+addProdPopUpCounter+"_"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-8' class='control-label'>Reason</label>"
			+"                                                                                    <input type='text' id='reason"+addProdPopUpCounter+"_"+counter+"' name='reason' class='form-control' id='field-8' placeholder='Reason' onkeyup =\"reasonKeyUp("+addProdPopUpCounter+","+counter+")\">"
			+"																	                  <div id='reason_Error_msg"+addProdPopUpCounter+"_"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-6'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-9' class='control-label'>Quantity Variance</label>"
			+"                                                                                    <input type='text' id='quantityVariance"+addProdPopUpCounter+"_"+counter+"' name='quantityVariance' class='form-control' id='field-9' placeholder='Quantity Variance' onkeyup =\"quantityVarianceKeyUp("+addProdPopUpCounter+","+counter+")\">"
			+ "																	                  <div id='quantityVariance_Error_Msg"+addProdPopUpCounter+"_"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                    <input type='hidden' name='lrFormIndex' value='"+addProdPopUpCounter+"'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='modal-footer text-center'>"
//			+"                                                                        <button type='button' class='btn btn-primary' onClick='submitAddProductForm("+addProdPopUpCounter+","+counter+")'>Add</button>"
			+"                                                                        <button type='button' class='btn btn-primary' onClick='validateInwardReturnAddProductFormDetails("+addProdPopUpCounter+","+counter+")'>Add</button>"
			+"                                                                    </div>"
			+"                                                                    <div class='row'>"
			//listing
			+"                                                                        <div id='productListing"+addProdPopUpCounter+"_"+counter+"' class='col-md-6'>"
			+"                                                                        </div>"
			//listing
			+"                                                                    </div>"
			+"                                                                    <div class='modal-footer text-center'>"
			+"                                                                        <input type='button' value='Submit' class='btn btn-primary' onClick='submitAddProduct(\""+popUpId+"\","+addProdPopUpCounter+","+counter+")'>"
			+"                                                                        <input type='button' value='Cancel' class='btn btn-danger' onClick='hidePopUpAddProduct(\""+popUpId+"\","+addProdPopUpCounter+","+counter+")'>"
			+"                                                                    </div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>";
		//$('#addProductPopUp').empty();
		 $('#addProductPopUp'+addProdPopUpCounter).append(html);
		 $(".dateClassCommon").datepicker({
				changeMonth : true,
				changeYear : true,
				yearRange : "-100:+0"
			});
		 if(stat){
			 $("#"+popUpId).addClass("in");
			 $("#"+popUpId).attr("aria-hidden","false");
			 $("#"+popUpId).css("display","block");
			 addProductListing(addProdPopUpCounter,counter);
			 
		 }
		 var htmlText = "<input type='button' value='Add Product' class='btn btn-info m-b-10' onClick='addProductPopUp("+addProdPopUpCounter+","+Counter+","+true+")'>";
		 $("#lrNoAddProduct"+addProdPopUpCounter).html(htmlText);
	}
}

function validateInwardReturnAddProductFormDetails(addProdPopUpCounter,counter)
{
	var status=true;
	if(!validateProductName(addProdPopUpCounter,counter))
		status=false;
	if(!validateClaimQuantity(addProdPopUpCounter,counter))
		status=false;
	if(!validateReceivedQuantity(addProdPopUpCounter,counter))
		status=false;
	if(!validateClaimQuantity(addProdPopUpCounter,counter))
		status=false;
	if(!validateBatch(addProdPopUpCounter,counter))
		status=false;
	if(!validateMFGDate(addProdPopUpCounter,counter))
		status=false;
	if(!validateExpiryDate(addProdPopUpCounter,counter))
		status=false;
	if(!validateMFGCompany(addProdPopUpCounter,counter))
		status=false;
	if(!validateReason(addProdPopUpCounter,counter))
		status=false;
	if(!validateQuantityVarience(addProdPopUpCounter,counter))
		status=false;
	if(status)
		return submitAddProductForm(addProdPopUpCounter,counter);
	else
		return false;
}


function hidePopUpAddProduct(popUpId,addProdPopUpCounter,counter){
		var x = addProdPopUpCounter+"_"+counter;
		
		if(MultiArray.length>0){
			if(MultiArray[addProdPopUpCounter] != undefined && MultiArray[addProdPopUpCounter].length>0){
				if(MultiArray[addProdPopUpCounter][counter]!=undefined ){
					$("#productName"+x).val(MultiArray[addProdPopUpCounter][counter][0]);
					$("#claimQuantity"+x).val(MultiArray[addProdPopUpCounter][counter][1]);
					$("#receivedQuantity"+x).val(MultiArray[addProdPopUpCounter][counter][2]);
					$("#batch"+x).val(MultiArray[addProdPopUpCounter][counter][3]);
					$("#mfgDate"+x).val(MultiArray[addProdPopUpCounter][counter][4]);
					$("#expiryDate"+x).val(MultiArray[addProdPopUpCounter][counter][5]);
					$("#mfgCompany"+x).val(MultiArray[addProdPopUpCounter][counter][6]);
					$("#reason"+x).val(MultiArray[addProdPopUpCounter][counter][7]);
					$("#quantityVariance"+x).val(MultiArray[addProdPopUpCounter][counter][8]);
				}
			}
		}
		 $("#"+popUpId).removeClass("in");
		 $("#"+popUpId).attr("aria-hidden","true");
		 $("#"+popUpId).css("display","none"); 
}
function submitAddProduct(popUpId,addProdPopUpCounter,counter){
	//editStatus = true;
	if(editStatus == true){
		editStatus = false;
	submitAddProductForm(addProdPopUpCounter,counter);
	}
	var popUpId = "popUpId"+addProdPopUpCounter+"_"+openedProductTab;
		 $("#"+popUpId).removeClass("in");
		 $("#"+popUpId).attr("aria-hidden","true");
		 $("#"+popUpId).css("display","none"); 
}

function validateInwardReturnGoodsAddProduct(addProdPopUpCounter,counter){
	var status=true;
	if(!validateProductField("productName_ErrorMsg","productName",addProdPopUpCounter+"_"+counter))
		status=false;
	if(!validateProductField("claimQuantity_ErrorMsg","claimQuantity",addProdPopUpCounter+"_"+counter))
		status=false;
	if(!validateProductField("receivedQuantity_ErrorMsg","receivedQuantity",addProdPopUpCounter+"_"+counter))
		status=false;
	if(!validateProductField("batch_ErrorMsg","batch",addProdPopUpCounter+"_"+counter))
		status=false;
	if(!validateProductField("mfgDate_ErrorMsg","mfgDate",addProdPopUpCounter+"_"+counter))
		status=false;
	if(!validateProductField("expiryDate_ErrorMsg","expiryDate",addProdPopUpCounter+"_"+counter))
		status=false;
	if(!validateProductField("mfgCompany_ErrorMsg","mfgCompany",addProdPopUpCounter+"_"+counter))
		status=false;
	if(!validateProductField("reason_ErrorMsg","reason",addProdPopUpCounter+"_"+counter))
		status=false;
	if(!validateProductField("quantityVariance_ErrorMsg","quantityVariance",addProdPopUpCounter+"_"+counter))
		status=false;
	if(status)
		submitAddProduct(popUpId,addProdPopUpCounter,counter);
	else
		return false;
	
}
//
function submitAddProductForm(addProdPopUpCounter,counter){
	
	//alert("addProdPopUpCounter : "+ addProdPopUpCounter+  "  counter : "+counter);
	var popUpId = "popUpId"+addProdPopUpCounter+"_"+counter;
	var x = addProdPopUpCounter+"_"+counter;
	//alert("submitAddProductForm = "+popUpId);
	//alert("addProdPopUpCounter,counter ="+addProdPopUpCounter+" "+counter);
	//hidePopUpAddProduct(popUpId,false);
//	if($("#productName"+x).val()=="")
//	{	
//		alert("Empty Fields");
//	}
//	else{
		$("#"+popUpId).removeClass("in");
		$("#"+popUpId).attr("aria-hidden","true");
		$("#"+popUpId).css("display","none");
		//MultiArray[addProdPopUpCounter] = new Array();
		if(MultiArray[addProdPopUpCounter]==null||MultiArray[addProdPopUpCounter]==undefined)
		{
			MultiArray[addProdPopUpCounter] = new Array();
		}
		var new_Array = new Array();
		new_Array[0] = $("#productName"+x).val();
		//alert("new_Array[0] = "+new_Array[0]);
		new_Array[1] = $("#claimQuantity"+x).val();
		new_Array[2] = $("#receivedQuantity"+x).val();
		new_Array[3] = $("#batch"+x).val();
		new_Array[4] = $("#mfgDate"+x).val();
		new_Array[5] = $("#expiryDate"+x).val();
		new_Array[6] = $("#mfgCompany"+x).val();
		new_Array[7] = $("#reason"+x).val();
		new_Array[8] = $("#quantityVariance"+x).val();
		
		MultiArray[addProdPopUpCounter][counter]=new Array();
		MultiArray[addProdPopUpCounter][counter] = new_Array;
		
		if(editStatus){
			 editStatus = false;
		}else{
		//glbal var increment
		 Counter++;
		}
		addProductPopUp(addProdPopUpCounter,Counter,true);
//	}
}

//Product Listing
function addProductListing(addProdPopUpCounter,row){
	html=""
		+"                                                                            <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                                <table class='table table-striped table-hover'>"
		+"                                                                                    <thead class='no-bd'>"
		+"                                                                                        <tr>"
		+"                                                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Product Name</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Claim Quantity</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Received Quantity</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Batch</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>MFG Date</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Expiry Date</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>MFG Company</strong>"
		+"                                                                                            </th>"
		/*+"                                                                                            <th style='text-align: center;'><strong>Quantity Variance</strong>"
		+"                                                                                            </th>"*/
		+"                                                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                            </th>"
		+"                                                                                        </tr>"
		+"                                                                                    </thead>"
		+"                                                                        <tbody class='no-bd-y'>";
																					var rowCount = 0;
																					for(var i = 0;i<MultiArray.length;i++){
																						if(i==addProdPopUpCounter){
																						for(var j = 0;j<MultiArray[i].length;j++){
																							if(MultiArray[i][j][0]!=""){
																							html+="<tr style='text-align: center;'>"
																								+" <td>"+(rowCount+1)+"</td>"
																								+" <td>"+MultiArray[i][j][0]+"</td>"
																								+" <td>"+MultiArray[i][j][1]+"</td>"
																								+" <td>"+MultiArray[i][j][2]+"</td>"
																								+" <td>"+MultiArray[i][j][3]+"</td>"
																								+" <td>"+MultiArray[i][j][4]+"</td>"
																								+" <td>"+MultiArray[i][j][5]+"</td>"
																								+" <td>"+MultiArray[i][j][6]+"</td>"
																								+" <td><a class='edit btn btn-dark' onClick='editProductFormEnrty("+i+","+j+","+addProdPopUpCounter+","+j+")'  href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> <a class='delete btn btn-danger' onClick='deleteProductFormEnrty("+i+","+j+","+addProdPopUpCounter+","+row+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
																								+"</tr>";
																							rowCount++;
																							}
																						}
																						}
																						}
    html+="                                                                        </tbody>"
		+"                                                                                </table>"
		+"                                                                            </div>";
	$('#productListing'+addProdPopUpCounter+"_"+row).html(html);
}

function deleteProductFormEnrty(i,j,addProdPopUpCounter,row){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		MultiArray[i][j][0]="";
		addProductListing(addProdPopUpCounter,row);
		var id = i+"_"+j;
		$('#popUpId'+id).remove();
		//alert('popUpId'+id+' removed' );
    }
}

function editProductFormEnrty(i,j,addProdPopUpCounter,row){
	editStatus = true;
	var hidePopIpId =  "popUpId"+addProdPopUpCounter+"_"+openedProductTab;
	var popUpId = "popUpId"+i+"_"+j;
	openedProductTab=j;
	//hide current popup
	 $("#"+hidePopIpId).removeClass("in");
	 $("#"+hidePopIpId).attr("aria-hidden","true");
	 $("#"+hidePopIpId).css("display","none");
	//show edit popup
	$("#"+popUpId).addClass("in");
	$("#"+popUpId).attr("aria-hidden","false");
	$("#"+popUpId).css("display","block");
	//alert(popUpId+"  Edit");
	addProductListing(addProdPopUpCounter,j);
}

//View Inward Return Goods Registration Details at first time (1)
function viewInwardReturnGoodsRegListing(){
	$('#loader').show();
	//$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/inwardReturnGoodsRegListing', function(data) {
		var html=""
			+"                                <div class='tab-pane fade active in' id='tab2_2'>"
			+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
			+"                                        <h3><strong>Inward Return Goods Registration</strong></h3>"
			+"                                    </div>"
			+"             						  <div id='subTabs'></div>"
			+"                                    </div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-12'>"
			+ "                        <div class='panel panel-default'>"
			+ "                            <div class='panel-body'>"
			+ "                                <div class='row'>"
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+ "                                            <label class='form-label'><strong>Search By</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div class='controls'>"
			+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFunc();'>"
			+ "                                                       <option value='stockist'>Stockist</option>"
			+"														  <option value='Organization'>Select Organization</option>"
			+ "                                                       <option value='Company'>Comapny Name</option>"
			+ "                                                       <option value='Date'>Search By Registration Date</option>"
			+ "                                                       <option value='Transporter'>Transporter </option>"
			+"														  <option value='LrNo' >LR No </option>"
			+ "                                                       <option value='ClaimNo'>Claim No </option>"
			+ "                                                       <option value='ClaimAmount'>Claim Amount </option>"
			+ "                                                       <option value='DriverName'>Driver Name</option>"
			+ "                                                       <option value='DeliveredEmployee'>Delivered Employee Name</option>"
			+ "                                                     </select>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"//fromDate toDate searchOption searchText
			+ "                                    <div class='col-md-4'>"
			+ "                                        <div class='form-group' id='searchForDiv'>"
			+ "                                            <label class='form-label'><strong>Search For</strong>"
			+ "                                            </label>"
			+ "                                            <span class='tips'></span>"
			+ "                                            <div id='searchControl'>"
			+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search For'>"
			+ "                                            </div>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
			+ "                                        <div class='pull-right'>"
			+ "                                            <button class='btn btn-success m-b-10' onclick='searchInwardCourierRegistrationDetails1()'>Show</button>"
			+ "                                        </div>"
			+ "                                    </div>"
			+ "                                    <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                            </div>"
			+"                                        </div>"
			+ "                                       <div id='pagination' class='pagination'></div>"
			+"                                    </div>"
			+"                                </div>";
		$('#myTabContent').html(html);
		subTabsForInwardReturnGoodsReg(false);
		//showInwardReturnGoodsRegListing(data);
		commonInwardCourierRegistrationDetailsController("", "", "", "", 1);
	//}, 'json');
}

//function to Searching for the  Inward Return Goods Registration Details(2)
function changeFunc() {
	var selectedValue=$('#searchOption').val();
	var html="";
	
		if(selectedValue=="ClaimAmount")
		{
    		html+="<input id='inputFromData3' type='text' class='form-control' placeholder='From Amount' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTOData3' type='text' class='form-control' placeholder='To Amount' style='width: 49%;'>";
		}
    	else if(selectedValue=="Date" || selectedValue=="CheckingDoneDate")
    	{
    		html+="<input id='inputFromDate' type='text' class='dateClassCommon form-control' placeholder='From Date' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTODate' type='text' class='dateClassCommon form-control' placeholder='To Date' style='width: 49%;'>";
    	}
    	else
    	{
    		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control'>";
    	} 	
		$("#searchControl").empty();
		$("#searchControl").html(html);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
 }

function searchInwardCourierRegistrationDetails1(){
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	if(selectedOption=="ClaimAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (selectedOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonInwardCourierRegistrationDetailsController(selectedOption, searchText, fromSpecialData, toSpecialData, 1);
}

function commonInwardCourierRegistrationDetailsController(selectedOption, searchText, fromSpecialData, toSpecialData,from){
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/searchInwardReturnGoodsRegListing',
			{
		selectedValue:selectedOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		searchOption:'simpleSearch' , 
		from : from
		}, function(data) {
			var url = "commonInwardCourierRegistrationDetailsController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showInwardReturnGoodsRegListing(data, from, url);
	}, 'json');
}
//showing Inward Return Goods Registration Details with commn function (3)
function showInwardReturnGoodsRegListing(data, from, url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i = 0 ;
//	$('#loader').show();
	var html=""
		+ "                                        <table class='table table-striped table-hover'>"
		+ "                                            <thead class='no-bd'>"
		+ "                                                <tr>"
		+"                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Company</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Transporter</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                            </th>"
		+ "                                                </tr>"
		+ "                                            </thead>"
		+ "                                            <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			 return false;
		html+="                                                       <tr style='text-align: center;'>"
			+"                                                            <td>"+ SR_NO +"</td>"
			+"                                                            <td id='ORG_"+i+"'>"+element.ORG+"</td>"
			+"                                                            <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
			+"                                                            <td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
			+"                                                            <td id='TRAN_"+i+"'>"+element.TRAN+"</td>"
			+"  														  <td><div id='lrListPopUp"+element.ID_NO+"'></div><a class='btn btn-info m-b-10' onClick='viewInwardReturnGoodsRegDetailsCheckingDoneView("+element.ID_NO+")' href='javascript:;'><i class='fa fa-external-link'></i></a>  <a class='delete btn btn-danger m-b-10' onClick='deleteInwardReturnGoodsReg("+element.ID_NO+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"//<a class='edit btn btn-dark m-b-10' href='#' onclick='editInwardReturnGoodsReg("+i+","+element.ID_NO+")'><i class='fa fa-pencil-square-o'></i></a>
			+"                                                        </tr>";
		i++;SR_NO++;
	}); 
	html+="                                                    </tbody>"
		+"                                                </table>";
	$('#searchResult').html(html);
	$('#loader').hide();
	$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	paginationView(from,data[data.length-1].paginationCount,url);
}



function viewInwardReturnGoodsRegDetails(ID_NO){
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/inwardReturnGoodsRegDetailsAndLrListing', {
		ID_NO : ID_NO 
			} , function(data) {
				var i =1;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Inward Return Goods Registration Details</strong></h4>"
			        + "            </div>"
			        + "  		   <div class='panel panel-default'>"
			        + "			       <div class='panel-body'>"
			        + "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
					+"                                        					<div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION ID : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ID_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>ORGANIZATION NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ORGANIZATION+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>COMPANY NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                               <label class='form-label'><strong>"+data.COMPANY+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>STOCKIST NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.STOCKIST+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTER NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DELIVERED EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.EMPLOYEE_NAME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION DATE (MM/DD/YYYY) : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.REG_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.TIME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>    "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.DRIVER_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>                                             "            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NAME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.DRIVER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTATION CHARGES : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTATION_CHARGES+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>SUBMIT DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.SUBMIT_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+ "						 </div>"
					+ " 			    </div>"
					
			
					+"						<div class='row'>"
					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>LR No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>No of Cases</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Amount</strong>"
					+"											</th>      "
					/*+"											<th style='text-align: center;'><strong>LR Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Copy</strong>"
					+"											</th>"*/
					+"											<th style='text-align: center;'><strong>Action</strong>"
					+"											</th>"
					+"										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
				var irgrLRArr = data.irgrLRArr;
					$(irgrLRArr).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ i++ +"</td>"
							+"                                         <td >"+element.LR_NO+"</td>"
							+"                                         <td >"+element.NO_OF_CASES+"</td>"
							+"                                         <td>"+element.REF_CLAIM_NO+"</td>"
							+"                                         <td>"+element.CLAIM_AMOUNT+"</td>"
							//+"                                         <td>"+element.ATTACH_LR+"</td>"
							//+"                                         <td>"+element.ATTACH_CLAIM_COPY+"</td>"
							+"											<td style='width: 214px;'>"
							+"												<a class='edit btn btn-blue' onClick='viewInwardReturnGoodsReg_LR_Details("+element.LR_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a></i>  <a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegLrDetails("+ID_NO+","+element.LR_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"// <a class='edit btn btn-dark' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>
							+"											</td>"
							+"										</tr>";
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"<div id='subPopUp'></div>"
					+"						</div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
					+ "			   </div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
			}, 'json');
}
//
/*function viewInwardReturnGoodsRegDetailsCheckingDoneDetailsView(ID_NO){
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/viewInwardReturnGoodsRegDetailsCheckingDoneDetailsView', {
		ID_NO : ID_NO 
			} , function(data) {
				var i =1;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Inward Return Goods Registration Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"
			        + "    			       <div class='row'>"
			        + "       				   								  <div class='col-md-4'>"
					+"                                        					  <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION ID : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ID_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>ORGANIZATION NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ORGANIZATION+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>COMPANY NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                               <label class='form-label'><strong>"+data.COMPANY+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>STOCKIST NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.STOCKIST+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTER NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DELIVERED EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.EMPLOYEE_NAME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION DATE (MM/DD/YYYY) : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.REG_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.TIME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>    "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.DRIVER_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>                                             "            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NAME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.DRIVER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTATION CHARGES : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTATION_CHARGES+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>SUBMIT DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.SUBMIT_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+ "						 								  </div>"
					+ " 			    	</div>"
			        + "            	   </div>"

			        + "                <div class='modal-header'>"
			        + "                	    <h4 class='modal-title' id='myModalLabel'><strong>LR List :-</strong></h4>"
			        + "            	   </div>"
			        + "			       <div class='panel-body'>"			
					+"						<div class='row'>"

					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>LR No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>No of Cases</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Amount</strong>"
					+"											</th>      "
					+"											<th style='text-align: center;'><strong>LR Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Action</strong>"
					+"											</th>"
					+"										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
				var irgrLRArr = data.irgrLRArr;
					$(irgrLRArr).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ i++ +"</td>"
							+"                                         <td >"+element.LR_NO+"</td>"
							+"                                         <td >"+element.NO_OF_CASES+"</td>"
							+"                                         <td>"+element.REF_CLAIM_NO+"</td>"
							+"                                         <td>"+element.CLAIM_AMOUNT+"</td>"
							+"                                         <td>"+element.ATTACH_LR+"</td>"
							+"                                         <td>"+element.ATTACH_CLAIM_COPY+"</td>"
							+"											<td style='width: 214px;'>"
							+"												<a class='edit btn btn-blue' onClick='viewInwardReturnGoodsReg_LR_Details("+element.LR_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a></i> <a class='edit btn btn-dark' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegLrDetails("+ID_NO+","+element.LR_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
							+"											</td>"
							+"										</tr>";
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"							<div id='subPopUp'></div>"
			        + "            		    </div>"
					+ "			       </div>"
			        + "            <div class='modal-header'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Checking Done Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"			
					+"						<div class='row'>"
					+"														  <div class='col-md-4'>"
					+"                                        					  <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CHECKING_DONE_EMP_NAME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DATE (MM/DD/YYYY) : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CHECKING_DONE_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CHECKING_DONE_TIME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"														  <div class='col-md-4'>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REMARK : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                               <label class='form-label'><strong>"+data.CHECKING_DONE_REMARK+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>ATTACH CHECKING SLIP: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"																  <img onClick='showImagePopup(\""+data.CHECKING_DONE_SLIP_IMG_PATH+"\")' src='"+data.CHECKING_DONE_SLIP_IMG_PATH+"' alt='Smiley face' height='100' width='100'>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"						</div>"
					+"             			<div class='modal-footer text-center'>"
			        + "                		<button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            		</div>"
					+ "			   </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
			}, 'json');
}*/

function viewInwardReturnGoodsRegDetailsCheckingDoneView(ID_NO){
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/inwardReturnGoodsRegDetailsAndLrListingCheckingDoneView', {
		ID_NO : ID_NO 
			} , function(data) {
				commonData = data;
				var i = 0;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Inward Return Goods Registration Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"
			        + "    			       <div class='row'>"
			        + "       				   								  <div class='col-md-4'>"
					+"                                        					  <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION ID : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ID_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>ORGANIZATION NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ORGANIZATION+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>COMPANY NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                               <label class='form-label'><strong>"+data.COMPANY+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>STOCKIST NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.STOCKIST+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTER NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DELIVERED EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.EMPLOYEE_NAME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.REG_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.TIME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>    "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.DRIVER_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>                                             "            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NAME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.DRIVER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTATION CHARGES : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTATION_CHARGES+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>SUBMIT DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.SUBMIT_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+ "						 								  </div>"
					+ " 			    	</div>"
			        + "                 <div class='modal-header'>"
			        + "                     <h4 class='modal-title' id='myModalLabel'><strong>LR List :</strong></h4>"
			        + "            	   	</div>"	
					+"						<div class='row'>"
					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>LR No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>No of Cases</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Amount</strong>"
					+"											</th>      "
					+"											<th style='text-align: center;'><strong>LR Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Action</strong>"
					+"											</th>"
					+"										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
				var irgrLRArr = data.irgrLRArr;
			
					$(irgrLRArr).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ (i+1) +"</td>"
							+"                                         <td >"+element.LR_NO+"</td>"
							+"                                         <td >"+element.NO_OF_CASES+"</td>"
							+"                                         <td>"+element.REF_CLAIM_NO+"</td>"
							+"                                         <td>"+element.CLAIM_AMOUNT+"</td>"
							+"                                         <td><img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showLrDocImagesListPopup("+i+")'></td>"//onclick='showImagePopup(\""+element.ATTACH_LR+"\")'
							+"                                         <td><img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showClaimCopyDocImagesListPopup("+i+")'></td>"//onclick='showImagePopup(\""+element.ATTACH_CLAIM_COPY+"\")'
							+"											<td style='width: 214px;'>"
							+"												<a class='edit btn btn-blue' onClick='viewInwardReturnGoodsReg_LR_Details("+element.LR_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a></i>  <a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegLrDetails("+ID_NO+","+element.LR_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"//<a class='edit btn btn-dark' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> 
							+"											</td>"
							+"										</tr>";
						i++;
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"							<div id='subPopUp'></div>"
					+ " 			    	</div>"
			        + "            		</div>";
			        if(data.CHECKING_DONE_EMP_NAME!=null){
			    html+= "           <div class='modal-header'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Checking Done Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"			
					+"						<div class='row'>"
					+"														  <div class='col-md-4'>"
					+"                                        					  <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CHECKING_DONE_EMP_NAME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CHECKING_DONE_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CHECKING_DONE_TIME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"														  <div class='col-md-4'>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REMARK : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                               <label class='form-label'><strong>"+data.CHECKING_DONE_REMARK+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>ATTACH CHECKING SLIP: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"																  <img onClick='showCheckingSlipDocImagesListPopup()' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"						</div>"
					+ "			   </div>";
			        }
			        if(data.CREDIT_NOTE_ID!=null){
			    html+="            <div class='modal-header'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Credit Note Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"			
					+"						<div class='row'>"
					+"														  <div class='col-md-4'>"
					+"                                        					  <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CREDIT_NOTE_NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CREDIT_NOTE_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CREDIT_NOTE_AMOUNT: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CREDIT_NOTE_AMOUNT+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"														  <div class='col-md-4'>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REMARK: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                               <label class='form-label'><strong>"+data.CREDIT_NOTE_REMARK+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CREDIT_NOTE_ATTACH: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"																  <img onClick='showCreditNoteDocImagesListPopup()' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"						</div>";
			        }
					html+="             			<div class='modal-footer text-center'>"
			        + "                		<button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            		</div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
			}, 'json');
}
// 
function showLrDocImagesListPopup(index){
	showListOfImagesPopup(commonData.irgrLRArr[index].ATTACH_LR);
}
function showClaimCopyDocImagesListPopup(index){
	showListOfImagesPopup(commonData.irgrLRArr[index].ATTACH_CLAIM_COPY);
}
function showLrDocForLrDetailedviewImagesListPopup(){
	showListOfImagesPopup(commonData.irgrLRArr[index].ATTACH_LR);
}
function showClaimCopyDocForLrDetailedviewImagesListPopup(){
	showListOfImagesPopup(commonData.irgrLRArr[index].ATTACH_CLAIM_COPY);
}
function showCheckingSlipDocImagesListPopup(){
	showListOfImagesPopup(commonData.CHECKING_DONE_SLIP_IMG_PATH);
}
function showCreditNoteDocImagesListPopup(){
	showListOfImagesPopup(commonData.CREDIT_NOTE_ATTACH);
}
//
function viewInwardReturnGoodsRegDetailsForCheckingPending(ID_NO){
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/inwardReturnGoodsRegDetailsAndLrListingForCheckingPending', {
		ID_NO : ID_NO 
			} , function(data) {
				var i =1;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Inward Return Goods Registration Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"
			        + "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
					+"                                        					<div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION ID : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ID_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>ORGANIZATION NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ORGANIZATION+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>COMPANY NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                               <label class='form-label'><strong>"+data.COMPANY+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>STOCKIST NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.STOCKIST+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTER NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DELIVERED EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.EMPLOYEE_NAME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.REG_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.TIME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>    "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.DRIVER_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>                                             "            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NAME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.DRIVER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTATION CHARGES : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTATION_CHARGES+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>SUBMIT DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.SUBMIT_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+ "						 </div>"
					+ " 			    </div>"
					
			
					+"						<div class='row'>"
					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>LR No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>No of Cases</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Amount</strong>"
					+"											</th>      "
					/*+"											<th style='text-align: center;'><strong>LR Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Copy</strong>"
					+"											</th>                                           "*/
					+"											<th style='text-align: center;'><strong>Add Product</strong>"
					+"											</th>                                           "
					+"											<th style='text-align: center;'><strong>Action</strong>"
					+"											</th>"
					+"										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
				var irgrLRArr = data.irgrLRArr;
					$(irgrLRArr).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ i++ +"</td>"
							+"                                         <td >"+element.LR_NO+"</td>"
							+"                                         <td >"+element.NO_OF_CASES+"</td>"
							+"                                         <td>"+element.REF_CLAIM_NO+"</td>"
							+"                                         <td>"+element.CLAIM_AMOUNT+"</td>"
							/*+"                                         <td>"+element.ATTACH_LR+"</td>"
							+"                                         <td>"+element.ATTACH_CLAIM_COPY+"</td>"*/
							+"										   <td>";
																		if(!(element.HAS_PRODUCT))
																			html+="<a class='edit btn btn-warning' onClick='addProductPopupForCheckingPending("+ID_NO+","+element.LR_ID+")' href='javascript:;'><i><b>+</b></i></a>";
						html+="											</td>"
							+"											<td style='width: 214px;'>"
							+"												<a class='edit btn btn-blue' onClick='viewInwardReturnGoodsReg_LR_Details("+element.LR_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a></i>  <a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegLrDetails("+ID_NO+","+element.LR_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"//<a class='edit btn btn-dark' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>
							+"											</td>"
							+"										</tr>";
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"<div id='subPopUp'></div>"
					+"						</div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
			}, 'json');
}

function deleteInwardReturnGoodsRegLrDetails(inwardReturnGoodsRegId,LR_ID){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/deleteInwardReturnGoodsRegLrDetails', {
			LR_ID : LR_ID
		}, function(data) {
			hidePopUp();
			viewInwardReturnGoodsRegDetails(inwardReturnGoodsRegId);
			alert(data.MSG);
		}, 'json');
    }
}
///////////////////////////////Add Product///////////////////////////////////////////
function addProductPopupForCheckingPending(ID_NO,LR_ID){
	//global variable initialization
	Counter=0;
	openedProductTab=Counter;
	MultiArray = new Array();
	var html=""
		+"                                                        <div class='modal fade' id='addProductPopup7' aria-hidden='true'>"
		+"															<form id='saveProductForm' action='"+contextApplicationPath+"/InwardReturnGoodsRegistrationCheckingController/addProductsToInwardReturnGoodsLRInCheckingPending' method='post'>"
		+"                                                            <div class='col-md-13'>"
		+"                                                                <div class='modal-content'>"
		+"                                                                    <div class='modal-header'>"
		+"                                                                        <input value='x' type='button' class='close' aria-hidden='true' onClick='hideSubPopUp(\"addProductPopup7\")'>"//onClick='hidePopUpAddProduct(\""+popUpId+"\","+addProdPopUpCounter+","+counter+")'
		+"                                                                        <h4 class='modal-title' id='myModalLabel'><strong>Add Inward Return Product</strong></h4>"
		+"                                                                    </div>"
		+"																	  <div id='addProduct'></div>"
		+"                                                                    <div class='row'>"
		//listing
		+"                                                                        <div id='productListing' class='col-md-6'>"
		+"                                                                        </div>"
		//listing
		+"                                                                    </div>"
		+"                                                                    <div class='modal-footer text-center'>"
		+"                                                                        <input type='hidden' name='lrId' value='"+LR_ID+"'>"
		+"                                                                        <input type='submit' value='Submit' class='btn btn-primary'  >"//onClick='submitAddProduct(\""+popUpId+"\","+addProdPopUpCounter+","+counter+")'
		+"                                                                        <input type='button' value='Cancel' onClick='hideSubPopUp(\"addProductPopup7\")' class='btn btn-danger' >"
		+"                                                                    </div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"															</form>"
		+"                                                        </div>";
	//$('#addProductPopUp').empty();
	 $('#subPopUp').append(html);
	 $(".dateClassCommon").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
	 addProductFormContaints(Counter);
	 ajaxAddProductCheckingPending(ID_NO,"saveProductForm");
	 $("#addProductPopup7").addClass("in");
	 $("#addProductPopup7").attr("aria-hidden","false");
	 $("#addProductPopup7").css("display","block");
}
//
function ajaxAddProductCheckingPending(ID_NO,formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/ },
			success : function(data) {
					alert(data.MSG);
					if(data.result==true){
						//hideSubPopUp("addProductPopup7");
						viewInwardReturnGoodsRegDetailsForCheckingPending(ID_NO);
						Counter=0;
						openedProductTab="";
						MultiArray = [];
					}
			}
	};
	$('#'+formId).ajaxForm(options);
}
function validateAddProductFormCheckingPending(counter){
	var status=true;
	if(!validateProductName1(counter))
		status=false;
	if(!validateClaimQuantity1(counter))
		status=false;
	if(!validateReceivedQuantity1(counter))
		status=false;
	if(!validateClaimQuantity1(counter))
		status=false;
	if(!validateBatch1(counter))
		status=false;
	if(!validateMFGDate1(counter))
		status=false;
	if(!validateExpiryDate1(counter))
		status=false;
	if(!validateMFGCompany1(counter))
		status=false;
	if(!validateReason1(counter))
		status=false;
	if(!validateQuantityVarience1(counter))
		status=false;
	if(status)
		return addProductFormCheckingPending(counter);
	else
		return false;
}
function addProductFormCheckingPending(counter){
	validateFormDetails('#editInwardReturnGoodsReg');
	if($("#productName"+counter).val()=="")
	{	
		alert("Empty Fields");
	}
	else{
		if(MultiArray[counter]==null||MultiArray[counter]==undefined)
		{
			MultiArray[counter] = new Array();
		}
		var new_Array = new Array();
		new_Array[0] = $("#productName"+counter).val();
		new_Array[1] = $("#claimQuantity"+counter).val();
		new_Array[2] = $("#receivedQuantity"+counter).val();
		new_Array[3] = $("#batch"+counter).val();
		new_Array[4] = $("#mfgDate"+counter).val();
		new_Array[5] = $("#expiryDate"+counter).val();
		new_Array[6] = $("#mfgCompany"+counter).val();
		new_Array[7] = $("#reason"+counter).val();
		new_Array[8] = $("#quantityVariance"+counter).val();
		MultiArray[counter] = new_Array;
		if(editStatus){
			 editStatus = false;
		}else{
		//glbal var increment
		 Counter++;
		}
		$("#product"+counter).hide();
		addProductFormContaints(Counter);
		addProductListingCheckingPending();
	}
	$('#loader').hide();
}

//Product Listing
function addProductListingCheckingPending(){
	html=""
		+"                                                                            <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                                <table class='table table-striped table-hover'>"
		+"                                                                                    <thead class='no-bd'>"
		+"                                                                                        <tr>"
		+"                                                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Product Name</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Claim Quantity</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Received Quantity</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Batch</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>MFG Date</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Expiry Date</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>MFG Company</strong>"
		+"                                                                                            </th>"
		/*+"                                                                                            <th style='text-align: center;'><strong>Quantity Variance</strong>"
		+"                                                                                            </th>"*/
		+"                                                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                            </th>"
		+"                                                                                        </tr>"
		+"                                                                                    </thead>"
		+"                                                                        <tbody class='no-bd-y'>";
																					var rowCount = 0;
																						for(var i = 0;i<MultiArray.length;i++){
																							if(MultiArray[i]!=null){
																							html+="<tr style='text-align: center;'>"
																								+" <td>"+(rowCount+1)+"</td>"
																								+" <td>"+MultiArray[i][0]+"</td>"
																								+" <td>"+MultiArray[i][1]+"</td>"
																								+" <td>"+MultiArray[i][2]+"</td>"
																								+" <td>"+MultiArray[i][3]+"</td>"
																								+" <td>"+MultiArray[i][4]+"</td>"
																								+" <td>"+MultiArray[i][5]+"</td>"
																								+" <td>"+MultiArray[i][6]+"</td>"
																								+" <td><a class='edit btn btn-dark' onClick='editProductEntryCheckingPending("+i+")'  href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> <a class='delete btn btn-danger' onClick='deleteProductEntryCheckingPending("+i+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
																								+"</tr>";
																							rowCount++;
																							}
																						}
																						
    html+="                                                                        </tbody>"
		+"                                                                                </table>"
		+"                                                                            </div>";
	$('#productListing').html(html);
}
function editProductEntryCheckingPending(id){
	editStatus = true;
	$("#product"+openedProductTab).hide();
	$("#product"+id).show();
	openedProductTab=id;
}
function deleteProductEntryCheckingPending(id){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		if(openedProductTab!=id){
			MultiArray[id]=null;
			$("#product"+id).remove();
			addProductListingCheckingPending();
		}
		else{
			alert("Fistly save the changes of current product");
		}
    }
}

function checkDate(index,dateType)
{ 
	//alert(($('#expiryDate'+index)).val());
	if($('#expiryDate'+index).val()!=""&&$('#mfgDate'+index).val()!="")
	if($('#expiryDate'+index).val()<=$('#mfgDate'+index).val())
		{
		alert("Manufacturing date should be less than expiry date ");
		if(dateType==1)
			$('#mfgDate'+index).val("");
		else if(dateType==2)
			$('#expiryDate'+index).val("");
		}
	
}


function addProductFormContaints(counter){
	openedProductTab=counter;
	if($("#product"+counter).length ){
		alert("Already Exist");
		$("#product"+counter).show();
	}
	else{
		var html=""
			+"                                                                  <div id='product"+counter+"'>"
			+"                                                                    <div class='modal-body'>"
			+"                                                                        <div class='row'>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-1' class='control-label'>Product Name</label>"
			+"                                                                                    <input type='text' id='productName"+counter+"' name='productName' class='form-control' id='field-1' placeholder='Product Name' onkeyup =\"productNameKeyUp1("+counter+")\">"//"+addProdPopUpCounter+"_"+counter+"
			+ "																	                  <div id='productName_Error_Msg"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-2' class='control-label'>Claim Quantity</label>"
			+"                                                                                    <input type='text' id='claimQuantity"+counter+"' name='claimQuantity' class='form-control' id='field-2' placeholder='Claim Quantity' onkeyup =\"claimQuantityKeyUp1("+counter+")\">"
			+ "																	                  <div id='claimQuantity_Error_Msg"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-3' class='control-label'>Received Quantity</label>"
			+"                                                                                    <input type='text' id='receivedQuantity"+counter+"' name='receivedQuantity' class='form-control' id='field-3' placeholder='Received Quantity' onkeyup =\"receivedQuantityKeyUp1("+counter+")\">"
			+ "																	                  <div id='receivedQuantity_Error_Msg"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-4' class='control-label'>Batch</label>"
			+"                                                                                    <input type='text' id='batch"+counter+"' name='batch' class='form-control' id='field-4' placeholder='Batch' onkeyup =\"batchKeyUp1("+counter+")\">"
			+ "																	                  <div id='batch_Error_Msg"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-5' class='control-label'>MFG Date</label>"
			+"                                                                                    <input id='mfgDate"+counter+"' name='mfgDate' class='dateClassCommon form-control' onChange='checkDate("+counter+",1)' id='field-5' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' onkeyup =\"mfgDateKeyUp1("+counter+")\">" 
			+"																	                  <div id='mfgDate_Error_Msg"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-6' class='control-label'>Expiry Date</label>"
			+"                                                                                    <input id='expiryDate"+counter+"' name='expiryDate' onChange='checkDate("+counter+",2)' class='dateClassCommon form-control' id='field-6' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' onkeyup =\"expiryDateKeyUp1("+counter+")\">"
			+ "																	                  <div id='expiryDate_Error_Msg"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-7' class='control-label'>MFG Company</label>"
			+"                                                                                    <input id='mfgCompany"+counter+"' name='mfgCompany' type='text' class='form-control' id='field-7' placeholder='MFG Company' onkeyup =\"mfgCompanyKeyUp1("+counter+")\">"
			+ "																	                  <div id='mfgCompany_Error_Msg"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-8' class='control-label'>Reason</label>"
			+"                                                                                    <input type='text' id='reason"+counter+"' name='reason' class='form-control' id='field-8' placeholder='Reason' onkeyup =\"reasonKeyUp1("+counter+")\">"
			+"																	                  <div id='reason_Error_msg"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-6'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-9' class='control-label'>Quantity Variance</label>"
			+"                                                                                    <input type='text' id='quantityVariance"+counter+"' name='quantityVariance' class='form-control' id='field-9' placeholder='Quantity Variance' onkeyup =\"quantityVarianceKeyUp1("+counter+")\">"
			+ "																	                  <div id='quantityVariance_Error_Msg"+counter+"' class='validationError' style='color:red'></div>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='row'>"
			//listing
			+"                                                                        <div id='productListing"+counter+"' class='col-md-6'>"
			+"                                                                        </div>"
			//listing
			+"                                                                    </div>"
			+"                                                                    <div id='addProductButton' class='modal-footer text-center'>"
			+"                                                                        <button type='button' onClick='validateAddProductFormCheckingPending("+counter+")' class='btn btn-primary' >Add</button>"//onClick='submitAddProductForm("+addProdPopUpCounter+","+counter+")'
			+"                                                                    </div>"
			+"                                                                  </div>";
		$("#addProduct").append(html);
		$("#product"+counter).show();
		$(".dateClassCommon").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "-100:+0"
		});
	}
}
///////////////////////////////Add Product END///////////////////////////////////////////

function viewInwardReturnGoodsReg_LR_Details(LR_ID){
	//alert("viewInwardReturnGoodsReg_LR_Details");
	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/LR_DetailedViewsAndLrProductList', {
		LR_ID : LR_ID 
		} , function(data) {
			//commonData2 is global var
			commonData2 = data;
			productListArray = data.productsArr;
				var i = 0;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive777' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive777\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Inward Return Goods Registration LR Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"
			        + "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
					+"                                        					<div class='form-group'>"
					+"                                                                <label class='form-label'><strong>LR NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.LR_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>NO OF CASES: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.NO_OF_CASES+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>LR COPY : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <img onClick='showLrDocForLrDetailedviewImagesListPopup()' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.REF_CLAIM_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM AMOUNT : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.CLAIM_AMOUNT+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>    "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM COPY : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <img onClick='showClaimCopyDocForLrDetailedviewImagesListPopup()' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"//<label class='form-label'><strong>"+data.ATTACH_CLAIM_COPY_PATH+"</strong>"
					+"                                                            </div>                                             "            
					+ "						 </div>"
					+ " 			    </div>"
					
			
					+"																<div class='row'>"
					+"																	<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"																		<table class='table table-striped table-hover'>"
					+"																			<thead class='no-bd'>"
					+"																				<tr>"
					+"																					<th style='text-align: center;'><strong>Sr No.</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Product Name</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Claim Quantity</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Received Quantity</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Batch</strong>"
					+"																					</th>"
					/*+"																					<th style='text-align: center;'><strong>MFG Date</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Expiry Date</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>MFG Company</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Quantity Variance</strong>"
					+"																					</th>"*/
					+"																					<th style='text-align: center;'><strong>Action</strong>"
					+"																					</th>"
					+"																				</tr>"
					+"																			</thead>"
					+"																			<tbody class='no-bd-y'>";
				
					$(productListArray).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ (i+1) +"</td>"
							+"                                         <td >"+element.PRODUCT_NAME+"</td>"
							+"                                         <td >"+element.CLAIM_QUANTITY+"</td>"
							+"                                         <td>"+element.RECEIVED_QUANTITY+"</td>"
							+"                                         <td>"+element.BATCH+"</td>"
						/*	+"                                         <td>"+element.MFG_DATE+"</td>"
							+"                                         <td>"+element.EXPIRY_DATE+"</td>"
							+"                                         <td>"+element.MFG_COMPANY+"</td>"
							+"                                         <td>"+element.QUANTITY_VARIANCE+"</td>"*/
							+"											<td style='width: 214px;'>"
							+"												<a class='edit btn btn-blue' onClick='viewProductInfo("+i+")' href='javascript:;'><i class='fa fa-external-link'></i></a></i>   <a class='delete btn btn-danger' onClick='deleteInwardReturnGoodsRegLRProduct("+LR_ID+","+element.PRODUCT_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"//<a class='edit btn btn-dark' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>
							+"											</td>"
							+"										</tr>";
						i++;
					 });
					html+="																			</tbody>"
					+"																		</table>"
					+"																	</div>"
					+"														<div id='productDetailsPopup'></div>"
					+"																</div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
					+"															</div>"
					+"														</div>"
					+"												</div>";
					 $("#subPopUp").html(html);
					 $("#modal-responsive777").addClass("in");
					 $("#modal-responsive777").attr("aria-hidden","false");
					 $("#modal-responsive777").css("display","block");
					}, 'json');
}
//
function showLrDocForLrDetailedviewImagesListPopup(){
	showListOfImagesPopup(commonData2.ATTACH_LR);
}
function showClaimCopyDocForLrDetailedviewImagesListPopup(){
	showListOfImagesPopup(commonData2.ATTACH_CLAIM_COPY);
}
//
function deleteInwardReturnGoodsRegLRProduct(LR_ID,PRODUCT_ID){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/deleteInwardReturnGoodsRegLRProduct', {
			PRODUCT_ID : PRODUCT_ID
		}, function(data) {
			alert("PRODUCT_ID"+PRODUCT_ID);
			viewInwardReturnGoodsReg_LR_Details(LR_ID);
			alert(data.MSG);
		}, 'json');
    }
}
//
function viewProductInfo(index){
	var html=""
	+ "<div class='modal fade ' id='modal-responsive123' >"
    + "    <div class='col-md-13'>"
    + "        <div class='modal-content'>"
    + "            <div class='modal-header'>"
    + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive123\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
    + "                <h4 class='modal-title' id='myModalLabel'><strong>Company Product Details</strong></h4>"
    + "            </div>"
    + " 		   <div class='panel panel-default'>"
    + "                  <div class='panel-body'>"
    + "                       <div class='row'>"
    + "                             <div class='col-md-4'>"
	+"                    					<div class='form-group'>"
	+"                                         <label class='form-label'><strong>PRODUCT NAME : - </strong>"
	+"                                         </label>"
	+"                                         <span class='tips'></span>"
	+"                                         <label class='form-label'><strong>"+productListArray[index].PRODUCT_NAME+"</strong>"
	+"                                         </label>"
	+"                                      </div>"                                                            
	+"                                      <div class='form-group'>"
	+"                                         <label class='form-label'><strong>CLAIM QUANTITY : - </strong>"
	+"                                         </label>"
	+"                                         <span class='tips'></span>"
	+"                                         <label class='form-label'><strong>"+productListArray[index].CLAIM_QUANTITY+"</strong>"
	+"                                         </label>"
	+"                                      </div>"
	+"                                      <div class='form-group'>"
	+"                                           <label class='form-label'><strong>RECEIVED QUANTITY : - </strong>"
	+"                                           </label>"
	+"                                           <span class='tips'></span>"
	+"                                           <label class='form-label'><strong>"+productListArray[index].RECEIVED_QUANTITY+"</strong>"
	+"                                           </label>"
	+"                                      </div>"
	+"                                      <div class='form-group'>"
	+"                                           <label class='form-label'><strong>BATCH : - </strong>"
	+"                                           </label>"
	+"                                           <span class='tips'></span>"
	+"                                           <label class='form-label'><strong>"+productListArray[index].BATCH+"</strong>"
	+"                                           </label>"
	+"                                      </div>  "
	+"                                      <div class='form-group'>"
	+"                                           <label class='form-label'><strong>REASON : - </strong>"
	+"                                           </label>"
	+"                                           <span class='tips'></span>"
	+"                                           <label class='form-label'><strong>"+productListArray[index].REASON+"</strong>"
	+"                                           </label>"
	+"                                      </div>"
	+"                              </div>"
	+"                              <div class='col-md-4'>"
	+"                                      <div class='form-group'>"
	+"                                            <label class='form-label'><strong>MFG Date : - </strong>"
	+"                                            </label>"
	+"                                            <span class='tips'></span>"
	+"                                            <label class='form-label'><strong>"+productListArray[index].MFG_DATE+"</strong>"
	+"                                            </label>"
	+"                                      </div>  "
	+"                                      <div class='form-group'>"
	+"                                            <label class='form-label'><strong>EXPIRY DATE : - </strong>"
	+"                                            </label>"
	+"                                            <span class='tips'></span>"
	+"                                            <label class='form-label'><strong>"+productListArray[index].EXPIRY_DATE+"</strong>"
	+"                                            </label>"
	+"                                      </div>    "
	+"                                      <div class='form-group'>"
	+"                                            <label class='form-label'><strong>MFG COMPANY : - </strong>"
	+"                                            </label>"
	+"                                            <span class='tips'></span>"
	+"                                            <label class='form-label'><strong>"+productListArray[index].MFG_COMPANY+"</strong>"
	+"                                            </label>"
	+"                                      </div>                                             "            
	+"                                      <div class='form-group'>"
	+"                                            <label class='form-label'><strong>QUANTITY VARIANCE : - </strong>"
	+"                                            </label>"
	+"                                            <span class='tips'></span>"
	+"                                            <label class='form-label'><strong>"+productListArray[index].QUANTITY_VARIANCE+"</strong>"
	+"                                            </label>"
	+"                                      </div>"
	+ "                           </div>"
	+ " 					</div>"
	+ "				  </div>"
	+"            				  <div class='modal-footer text-center'>"
    + "                                       <button type='button' onClick='hideSubPopUp(\"modal-responsive123\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
    + "                           </div>"
    + "        </div>"
    + "    </div>"
    + "</div>";
$('#productDetailsPopup').html(html);
$("#modal-responsive123").addClass("in");
$("#modal-responsive123").attr("aria-hidden","false");
$("#modal-responsive123").css("display","block"); 
}
//
function hideSubPopUp(popupId){
	$("#"+popupId).remove();
}
//
function deleteInwardReturnGoodsReg(ID_NO){
    var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
    	$.post(contextApplicationPath+'/InwardReturnGoodsRegistrationController/deleteInwardReturnGoodsReg', {
    		ID_NO : ID_NO
    	}, function(data) {
    		viewInwardReturnGoodsRegListing();
    		alert(data.MSG);
    	}, 'json');
    }
}

//This function used at many places so dont change its code(in Inward & Outward return goods reg)...Thank you
function displayOrgDropDown(index){
	$.post(contextApplicationPath+'/StockistController/organizationListDropdown',function(data){
		//alert("displayOrganizationDropDown");
		var html ="";
		if(data.orgArray.length>1){
			html+="<label class='form-label'><strong>Select Organization*</strong>"
				+" </label>"
				+" <span class='tips'></span>"
				html+=" <div class='controls'>";
			if(index==1)
				html+=" <select id='OrgName' name='orgName' onchange='getCredential()' class='form-control' class='form-control' required>";
			
			html+="<option disabled selected> Select Organization</option>";
					for (var i = 0; i < data.orgArray.length; i++) {
					html += "<option value='" + data.orgArray[i].orgId + "'>"+ data.orgArray[i].orgName + "</option>";
					}
			html+="</select>"
				+"</div>";	
		}else if(data.orgArray.length>0){
			html+="<label class='form-label'><strong>Your Organization</strong>"
				+"</label>"
				+"<span class='tips'></span>"
				+"<div class='controls'>"
				+"<label class='form-label'><strong></strong>"+data.orgArray[0].orgName+"</label>"
				+"<input type='hidden' value='"+data.orgArray[0].orgId+"' name='orgName' id='OrgName'>"
				+"</div>";	
		}
		//$('#selectOrganization').empty();
		$('#selectOrganization').html(html);
		switch(index){
		case 1 : getCredential();break;
		
		}
		return 0;
	}, 'json');
}
//for special 2 way requirement
function getCredential(){
	var organizationId = $('#OrgName').val();
	
	getCompanies();
	getTransporter();
	if(organizationId==undefined)
		loadEmployeeDropDown(-1);
	else
		loadEmployeeDropDown(organizationId);
	getStockist();
	
}
function loadEmployeeDropDown(organizationId)
{
var html="<select class='form-control' class='form-control' name='employeeDetailsId'>"
	+"<option disabled selected>Select Employee</option>";
		$.post(contextApplicationPath+'/EmployeeController/loadEmployeeList',{organizationId : organizationId} ,function(data) {
			$(data).each(function(index, element) {
				html+="<option value='"+element.EMP_ID+"'>"+element.EMP_NAME+"</option>";
			}); 
			html+="</select>";
			$('#employeeDropDown').html(html);
		}, 'json');

}