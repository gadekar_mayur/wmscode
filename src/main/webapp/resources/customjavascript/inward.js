
var STNNO=[];
var GrossAmount=[];
var NetAmount=[];
var Divid=0;

function AddInwardGoods()
{
	var html=""
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>Inward Goods</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class='tabcordion'>"
		+"                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active'><a href='#tab1_1' data-toggle='tab' onclick=\"InwardGoodsRegistration()\">Inward Goods Registration</a></li>"
		+"                                <li class=''><a href='#tab1_2' data-toggle='tab' onclick=\"InwardGoodsRegistrationList()\">Inward Goods Registration View</a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	InwardGoodsRegistration();
}



function InwardGoodsRegistration()
{
	$('#loader').show();
	
	Divid=0;
	STNNO=[];
	GrossAmount=[];
	NetAmount=[];
	var html="            <div class='tab-pane fade active in' id='tab2_1'>"
		+"                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                	  <h3><strong>Inward Goods Registration</strong></h3>"
		+"				  </div>"
		+"                <div class='row'>"
		+"                    <div class='col-md-12'>"
		+"                        <div class='panel panel-default'>"
		+"<form id='addInwardGoodsRegistration' action='"+contextApplicationPath+"/InwardGoodsRegistrationController/saveInwardGoodsRegistration' method='post' enctype='multipart/form-data'>"
		+"                            <div class='panel-body'>"
		+"                                <div class='row'>"
		+"                                    <div class='cal1'>"
		+"                                        <div class='col-md-6'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12'>"
		+"<div id='organiztionDropDown'></div>"
		+"															<div class='form-group'>"
		+"																<label class='form-label'><strong>Select Company*</strong></label>"
		+"																<span class='tips'></span>"
		+"																<div class='controls'>"
		+"																	<select class='form-control' class='form-control' name='comapnyId' id='comapnyId' onchange=\"showCompanyDepo()\" required>"
		+"																		<option selected disabled>Select Company</option>"
	    +"																	</select>"
		+"																</div>"
		+"															</div>";
		//html+="<div id='companyDropDown'></div>";
		html+="                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Sending Depo*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																	<select id='sendingDepoId' class='form-control' name='sendingDepo' required>"
		+"																		<option selected disabled>Select Company</option>"
		+"																	</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Transporter*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																	<select class='form-control' id='transporterDDId' name='transporterId' parsley-required='true'>"
		+"																	   <option selected disabled>Select Transporter</option>"
		+"																	</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Date*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='datepicker form-control' type='text' name='date' placeholder='Select Date' style='height: 36px; padding-left: 10px;' id='date' onchange=\"errorMsgEmpty('date','dateError_msg')\">"
		+"                                                                </div>"
		+"																  <div id='dateError_msg' class='validationError' style='color:red'></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>No of Cases*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='No of Cases' name='NoofCases' class='form-control' parsley-type='onlynumber'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>LR No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='LR No' class='form-control' name='LRNo' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' name='LRNoImage'>"
		+"                                                            	  </div>"
		+"                                                            </div>"
		+"                                                            <div id='lrImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='lrImageAddMoreButtonId'><button onclick=\"addFileConrol('lrImageDivId','lrImageAddMoreButtonId','LRNoImage',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Driver Name</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='textarea' placeholder='Driver Name' class='form-control' name='driverName'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Unloading Charges</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Unloding Charges' class='form-control' name='unloadingCharges' parsley-type='number'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Unloader Name</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Unloader Name' name='unloaderName' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Time*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Time' class='form-control' name='time' id='timePicker' onfocusout=\"errorMsgEmpty('timePicker','timeError_msg')\">"
		+"                                                                </div>"
		+"																  <div id='timeError_msg' class='validationError' style='color:red'></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Received By*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Received By' class='form-control' name='receivedBy' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Transportation Charges</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Transportation Charges' class='form-control' name='transportationCharges' parsley-type='number'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Hamali Charges</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Hamali Charges' name='hamaliCharges' class='form-control' parsley-type='number'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Remark</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='textare' placeholder='Remark' name='remark' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div class='cal1'>"
		+"                                        <div class='col-md-6'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-heading'>"
		+"                                                    <h3 class='panel-title'><strong>Invoice Details</strong></h3>"
		+"                                                </div>"
		+"                                                <div class='panel-body'>"
		+"<div id='invoiceDetailsEentry'>"
		+"<div id='from_"+ Divid +"'>"
		+"                                                    <div class='row' >"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12'>"
		//
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>STN No*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='STN No' class='form-control' name='stnNo' id='stnNo"+Divid+"' onkeyup =\"stnNoKeyUp("+Divid+")\">"
		+ "																	  <div id='stnNo_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' name='stnImagePath' onchange='onStnImageChange(\"stnImagePath"+Divid+"_1\","+false+")' id='stnImagePath"+Divid+"_1' >"//for 1st time stn img no id starts with '1'
	//	+ "																	  <div id='stnImagePath_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                            	  </div>"
		+"                                                            </div>"
		+"                                                            <div id='stnImageDivId"+Divid+"'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='stnImageAddMoreButtonId"+Divid+"'><button onclick='addFileConrolForSTN_NO_IMG(1)' class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		//
		/*+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>STN No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='STN No' class='form-control' name='stnNo' id='stnNo"+Divid+"'>"
		+"                                                                    <input type='file' class='form-control' style='margin-top: 6px;' name='stnImagePath' id='stnImagePath'>"
		+"                                                                </div>"
		+"                                                            </div>"*/
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>STN Date*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='datepicker form-control stnDate' type='text' placeholder='STN Date' style='height: 36px; padding-left: 10px;' name='stnDate' id='stnDate"+Divid+"' OnChange =\"stnDateKeyUp("+Divid+")\">"
		+ "																	  <div id='stnDate_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Gross Amount*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Gross Amount' class='form-control' name='amount' id='amount"+Divid+"' onkeyup =\"amountKeyUp("+Divid+")\">"
		+ "																	  <div id='amount_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Vat Amount*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Vat Amount' class='form-control' name='vatAmount' id='vatAmount"+Divid+"' onkeyup =\"vatAmountKeyUp("+Divid+")\">"
		+ "																	  <div id='vatAmount_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Discount Amount*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Discount Amount' class='form-control' name='discountAmount' id='discountAmount"+Divid+"' onkeyup =\"discountAmountKeyUp("+Divid+")\">"
		+ "																	  <div id='discountAmount_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Net Amount*</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Net Amount' class='form-control' name='netAmount' id='netAmount"+Divid+"' onkeyup =\"netAmountKeyUp("+Divid+")\">"
		+ "																	  <div id='netAmount_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"															<input type='hidden' id='invoiceFileAttachedCountId"+Divid+"' value='0' name='invoiceFileAttachedCount' >"
		+"                                                        </div>"
		+"                                                    </div>"
		+"</div>"
		+"</div>";
		
		
		html+="                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right' id='addMoreButton'>"
		+"<input type='button' onclick=\" return validateInvoiceDetails('"+Divid+"')\" value='Add'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"<div id='InvoiceDetailsList'></div>";		
		
		
		html+="                                    </div>"
		+"                                </div>"
		+"                                <div class='row'>"
		+"                                    <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                            <div class='pull-right'>"
		+"                                                <button class='btn btn-primary m-b-10' onclick=\"return validateInwardsGoodsForm('#addInwardGoodsRegistration')\">Save</button>"
		/*+"                                                <button class='btn btn-success m-b-10' onclick='#'>Save & Mail</button>"*/
		+"                                                <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>"
		+"                            </div>"
		+"</form>"	
		+"                        </div>"
		+"                    </div>"
		+"                </div>"
		+"		 	  </div>";
	$('#myTabContent').html(html);
	 $('#timePicker').timepicki();
	loadInwardGoodsRegistrationOrganiztionDropDown();
	$("#date").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	$(".stnDate").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	ajaxAddInwardGoodsRegistration('addInwardGoodsRegistration');
	$('#loader').hide();
}
function ajaxAddInwardGoodsRegistration(FormId)
{
	var options = {
			beforeSubmit: function() {showLoader();},
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					/*$('#'+ FormId)[0].reset();
					alert(element.MSG);
					Divid=0;
					STNNO=[];
					GrossAmount=[];
					NetAmount=[];*/
					alert(element.MSG);
					InwardGoodsRegistration();
					$('#loader').hide();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}
function validateInwardsGoodsForm(formId)
{
	var z = $(formId).parsley('validate');
	var flag =  0;
	
	if($("#timePicker").val()==""||$("#timePicker").val()==undefined||$("#date").val()==""||$("#date").val()==undefined){
		var flag=false;
		if($("#timePicker").val()==""||$("#timePicker").val()==undefined){
			$("#timeError_msg").html("This value is required.");flag=true;
		}
		if($("#date").val()==""||$("#date").val()==undefined){
			$("#dateError_msg").html("This value is required.");flag=true;
		}
		if(flag){
			$('#loader').hide();
			return false;
		}
		return true;
	}
	if(z==true)
	{
	return Validation();
	}
	return false;
}

//function for STN NO. File control
function addFileConrolForSTN_NO_IMG(index){
	//alert("addFileConrol "+index);
	index++;
	var html="                                                    <div id='stnImageDivId"+Divid+"_"+index+"' class='form-group'>"
	+"                                                                <div class='controls' style='width: 85%; float: left;'>"
	+"                                                                    <input type='file' class='form-control' name='stnImagePath' onchange='onStnImageChange(\"stnImagePath"+Divid+"_"+index+"\","+false+")' id='stnImagePath"+Divid+"_"+index+"' parsley-required>"
	+ "																	  <div id='stnImagePath_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
	+"                                                            	  </div>"
	+"                                                                <div class='controls'>"
	+"                                                				  	  <div><a class='delete btn btn-danger' onclick='onStnImageFileDelete(\"stnImageDivId"+Divid+"_"+index+"\",\"stnImagePath"+Divid+"_"+index+"\")' style=' margin-left: 3px; width: 55px;height: 39px;'><i class='fa fa-times-circle' style='margin-left: -3px; margin-top: 3px;'></i> </a></div>"
	+"                                                            	  </div>"
	+"                                                            </div>";
	$('#stnImageDivId'+Divid).append(html);
	html="<button onclick='addFileConrolForSTN_NO_IMG("+index+")' class='btn btn-primary'>Add More Files</button>";
	$('#stnImageAddMoreButtonId'+Divid).html(html);
}

 //common function for generation of file input control
function addFileConrol(fileCtrlAppendDivId,AddMoreButtonDivId,fileName,index){
	//alert("file name : "+fileName);
	var html="                                                    <div id='"+fileCtrlAppendDivId+index+"' class='form-group'>"
	+"                                                                <div class='controls' style='width: 85%; float: left;'>"
	+"                                                                    <input type='file' class='form-control' name='"+fileName+"'>"
	+"                                                            	  </div>"
	+"                                                                <div class='controls'>"
	+"                                                				  	  <div><a class='delete btn btn-danger' onclick='hideSubPopUp(\""+fileCtrlAppendDivId+index+"\")' style=' margin-left: 3px; width: 55px;height: 39px;'><i class='fa fa-times-circle' style='margin-left: -3px; margin-top: 3px;'></i> </a></div>"
	+"                                                            	  </div>"
	+"                                                            </div>";
	$('#'+fileCtrlAppendDivId).append(html);
	html="<button onclick='addFileConrol(\""+fileCtrlAppendDivId+"\",\""+AddMoreButtonDivId+"\",\""+fileName+"\","+(index+1)+")' class='btn btn-primary'>Add More Files</button>";
	$('#'+AddMoreButtonDivId).html(html);
}
function onStnImageChange(id,selectStatus){
	var fileVal = $('#'+id).val();
	if(fileVal!=""&&fileVal!=undefined){
		if(!selectStatus){
			$("#"+id).attr("onchange", "onStnImageChange(\""+id+"\","+true+")");
			var counter = $('#invoiceFileAttachedCountId'+Divid).val();
			$('#invoiceFileAttachedCountId'+Divid).val(++counter);
		}
	}
	else{
		if(selectStatus){
			$("#"+id).attr("onchange", "onStnImageChange(\""+id+"\","+false+")");
			var counter = $('#invoiceFileAttachedCountId'+Divid).val();
			$('#invoiceFileAttachedCountId'+Divid).val(--counter);
		}
	}
	 stnImagePathKeyUp(Divid);
}
function onStnImageFileDelete(id,imgId){
	var fileData = $('#'+imgId).val();
	if(fileData!="" && fileData!=undefined){
		var counter = $('#invoiceFileAttachedCountId'+Divid).val();
		$('#invoiceFileAttachedCountId'+Divid).val(--counter);
	}
	hideSubPopUp(id);
}
/* function addFileConrol(index){addFileConrol(fileCtrlAppendDivId,AddMoreButtonDivId,fileName,index)
	alert("addFileConrol "+index);
	var html="<div id='lrImageDivId"+index+"'><input type='file' class='form-control' style='margin-top: 6px;' name='LRNoImage'><a onclick='hideSubPopUp(\"lrImageDivId"+index+"\")' > X </a></div>";
	$('#lrImageDivId').append(html);
	html="<button onclick='addFileConrol("+(index+1)+")' class='btn btn-primary'>Add More Files</button>";
	$('#lrImageAddMoreButtonId').html(html);
}*/
 
function Validation()
{
	  if(!(STNNO.length>0)){
	        alert("Please Add at least One Entry for LR Form.....!");
	        return false;
	    }
	  else
	  {
	        var emptyStat = true;
	        for(var i=0;i<STNNO.length;i++)
	        {
	            if(!(STNNO[i]==""))
	            {
	                emptyStat=false;break;
	            }
	        }
	        if(emptyStat)
	        {
	            alert("Please Add at least One Entry for LR Form.....!");
	            return false;
	        }
	        else
			{
			showLoader();
        	return true;
			}
	    }
	
	
//	if(STNNO.length>0)
//		{
//		return true;
//		}
//	else
//		{
//		alert("At least one Invoice Details is required");
//		return false;
//		}
}
function addMoreInvoiceDetails()
{
	var y=1;
	var divNo=0;
	STNNO[Divid]=$("#stnNo"+Divid+"").val()
	GrossAmount[Divid]=$("#amount"+Divid+"").val();
	NetAmount[Divid]=$("#netAmount"+Divid+"").val();
	console.log(STNNO);
	console.log(GrossAmount);
	console.log(NetAmount);
	$('#from_'+Divid).hide();
	var InvoiceListHtml="                                        <div class='col-md-6'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>STN No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Gross Amount</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Net Amount</strong>"
		+"                                                                        </th>                                                  "
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";

	for (var s = 0; s < STNNO.length; s++) 
		{
		if(STNNO[s]=="")
			{
			 
			}	
		else
			{
			InvoiceListHtml+="<tr style='text-align: center;' id='InvoiceList"+ s +"'>"
			+"<td>"+ y++ +"</td>"
			+"<td>"+STNNO[s]+"</td>"
			+"<td>"+GrossAmount[s]+"</td>"
			+"<td>"+NetAmount[s]+"</td>"
			+"<td><a class='delete btn btn-danger' href='#' onclick='RemoveInwardGoodsRegistrationInvoiceDetails("+s+")'><i class='fa fa-times-circle'></i> </a></td>"
			+"</tr>";
			}
	
		}
	InvoiceListHtml+="                                                                </tbody>"
		+"                                                            </table>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>" 
		$('#InvoiceDetailsList').html(InvoiceListHtml);
	
		var html="<div id='from_"+ ++Divid +"'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12'>"
		//
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>STN No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='STN No' class='form-control' name='stnNo' id='stnNo"+Divid+"' onkeyup =\"stnNoKeyUp("+Divid+")\">"
		+"																	  <div id='stnNo_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' class='form-control' name='stnImagePath' onchange='onStnImageChange(\"stnImagePath"+Divid+"_1\","+false+")' id='stnImagePath"+Divid+"_1'>"//for 1st time stn img no id starts with '1'
		+ "																	  <div id='stnImagePath_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                            	  </div>"
		+"                                                            </div>"
		+"                                                            <div id='stnImageDivId"+Divid+"'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='stnImageAddMoreButtonId"+Divid+"'><button onclick='addFileConrolForSTN_NO_IMG(1)' class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		/*+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>STN No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='STN No' class='form-control' name='stnNo' id='stnNo"+Divid+"'>"
		+"                                                                    <input type='file' class='form-control' style='margin-top: 6px;' name='stnImagePath' id='stnImagePath'>"
		+"                                                                </div>"
		+"                                                            </div>"*/
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>STN Date</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='datepicker form-control stnDate' type='text' placeholder='STN Date' style='height: 36px; padding-left: 10px;' id='stnDate"+Divid+"' name='stnDate' OnChange =\"stnDateKeyUp("+Divid+")\">"
		+ "																	  <div id='stnDate_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Gross Amount</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Gross Amount' class='form-control' name='amount' id='amount"+Divid+"' onkeyup =\"amountKeyUp("+Divid+")\">"
		+ "																	  <div id='amount_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Vat Amount</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Vat Amount' class='form-control' name='vatAmount' id='vatAmount"+Divid+"'   onkeyup =\"vatAmountKeyUp("+Divid+")\">"
		+ "																	  <div id='vatAmount_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Discount Amount</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Discount Amount' class='form-control' name='discountAmount' id='discountAmount"+Divid+"' onkeyup =\"discountAmountKeyUp("+Divid+")\">"
		+ "																	  <div id='discountAmount_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Net Amount</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Net Amount' class='form-control' name='netAmount' id='netAmount"+Divid+"' onkeyup =\"netAmountKeyUp("+Divid+")\">"
		+ "																	  <div id='netAmount_Error_Msg"+Divid+"' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"															<input id='invoiceFileAttachedCountId"+Divid+"' type='hidden' value='0' name='invoiceFileAttachedCount'>"
		+"                                                        </div>"
		+"                                                    </div>"
		
		
		+"</div>";
		$('#invoiceDetailsEentry').append(html);
		$(".stnDate").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
		
		var htmlbutton="<input type='button' onclick=\"validateInvoiceDetails('"+Divid+"')\" value='Add'>";
		$('#addMoreButton').html(htmlbutton);
}
function validateInvoiceDetails(Divid)
{
	var status=true;
	if(!stnNo(Divid))
		status=false;
	/*if(!stnImagePath(Divid))
		status=false;*/
	if(!stnDate(Divid))
		status=false;
	if(!amount(Divid))
		status=false;
	if(!vatAmount(Divid))
		status=false;		
	if(!discountAmount(Divid))
		status=false;	
	if(!netAmount(Divid))
		status=false;
	if(status)
		{
		addMoreInvoiceDetails();
		}
	else
		return false;
}

function RemoveInwardGoodsRegistrationInvoiceDetails(divId)
{
	console.log(divId);
	$('#InvoiceList'+divId).remove();
	$('#from_'+divId).remove();
	 STNNO[divId] = "";
	 GrossAmount[divId] = "";
	 NetAmount[divId] = "";
}



function loadInwardGoodsRegistrationOrganiztionDropDown()
{
	var userType='';
	var html="<div class='form-group'>"
	+"<label class='form-label'><strong>Select Organization*</strong>"
	+"</label>"
	+"<span class='tips'></span>"
	+"<div class='controls'>"
	+"<select class='form-control' class='form-control' id='orgId' name='selectedOrganiztionNameId' onchange='InwardGoodsRegistrationOrganizationChange()' required>" 
	+"<option selected disabled>Select Organiztion</option>";
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
		$(data).each(function(index, element) {
			userType=element.USER_TYPE;
			if(element.USER_TYPE=='SuperAdmin')
				{
				html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
				}
			else
				{
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='selectedOrganiztionNameId' id='orgId'>" +
						""+element.ORG_NAME+"";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
		content+="</div>"
		+"</div>";
		if(userType=='SuperAdmin')
			{
			$('#organiztionDropDown').html(html);
			}
		else
			{
			$('#organiztionDropDown').html(content);
			loadTransporterListForInward($("#orgId").val());
			loadAllCompany($("#orgId").val());
			}
	}, 'json');
}
function loadTransporterListForInward(TransOrgId)
{
	var html=""  // parsley-mincheck='1' parsley-maxcheck='1'
		+"<option selected disabled>Select Transporter</option>";
			$.post(contextApplicationPath+'/TransporterDetailsController/transporterDetailsListUsingOrgId',{organizationId : TransOrgId} ,function(data) {
				$(data).each(function(index, element) {
					html+="<option value='"+element.TRAN_DETAILS_ID+"'>"+element.TRAN_NAME+"</option>";
				});
				$("#transporterDDId").empty();
				$('#transporterDDId').append(html);
			}, 'json');
}
function InwardGoodsRegistrationOrganizationChange()
{
	loadTransporterListForInward($("#orgId").val());
	loadAllCompany($("#orgId").val());
}

function loadAllCompany(orgId)
{
	$.post(contextApplicationPath+'/CompanyController/getCompanies',{orgId : orgId }, function(data) {
		var optionHtml="<option selected disabled>Select Company</option>";
		for (var i = 0; i < data.companyArray.length; i++)
		{
			optionHtml += "<option value='" + data.companyArray[i].compId + "'>"+ data.companyArray[i].compName + "</option>";
		}
		$("#comapnyId").empty();
		$("#comapnyId").append(optionHtml);
		showCompanyDepo();
	}, 'json');
}

function showCompanyDepo()
{
	
	var html=""
	+"<option selected disabled>Select Depo</option>";
	$.post(contextApplicationPath+'/InwardGoodsRegistrationController/loadCompanyDepo',{orgId : $("#orgId").val() ,companyId : $("#comapnyId").val()}, function(data) {
		$(data).each(function(index,element){
			html+="<option value='"+element.COMP_DEPO_ID+"'>"+element.COMP_DEPO_NAME+"</option>"
		});
		$("#sendingDepoId").empty();
		$('#sendingDepoId').append(html);
	}, 'json');
}
function InwardGoodsRegistrationList()
{
	
	$('#loader').show();
//	$.post(contextApplicationPath+'/InwardGoodsRegistrationController/loadInwardGoodsRegistrationList', function(data) {
	var html=""
		+"            <div  id='tab2_2'>"
		+"                <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                	  <h3><strong>Inward Goods Registration View</strong></h3>"
		+"				  </div>"
		+"                <div class='row'>"
		+"                   <div class='col-md-12'>"
		+"                        <div class='panel panel-default'>"
		+"                            <div class='panel-body'>"
		+"                                <div class='row'>"
		+"                                    <div class='col-md-4'>"
		+"                                        <div class='form-group'>"
		+"                                            <label class='form-label'><strong>Search By</strong>"
		+"                                            </label>"
		+"                                            <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select id='searchOption' class='form-control' onchange='showDatePickerField()'>"
		+"                                                                        <option value='Organization' >Organization Name</option>"
		+"                                                                        <option value='Transporter'>Transporter</option>"
		+"                                                                        <option value='Company'>Company Name</option>"
		+"                                                                        <option value='LRNo'>LR No.</option>"
		+"                                                                        <option value='SendingDepo'>Sending Depo</option>"
		+"                                                                        <option value='DriverName'>Driver Name</option>"
		+"                                                                        <option value='RegDate'>Inward Registration Date</option>"
		+"                                                                        <option value='STNNo'>STN No</option>"
		+"                                                                        <option value='STNNetAmount'>STN Net Amount</option>"
		+"                                                                        <option value='STNDate'>STN Date</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div class='col-md-4'>"
		+"                                        <div class='form-group'>"
		+"                                            <label class='form-label'><strong>Search For</strong>"
		+"                                            </label>"
		+"                                            <span class='tips'></span>"
		+"                                            <div id='searchControl' class='controls'>"
		+"                                                <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div class='col-sm-9 col-sm-offset-3'>"
		+"                                        <div class='pull-right'>"
		+"                                            <button class='btn btn-success m-b-10' onclick='searchInwardGoodsRegDetails(1)'>Show</button>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                    <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                    </div>"
		+"                                </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>"
		+"		 	  </div>";
		$('#myTabContent').html(html);
		commonInwardGoodsRegistration("","","","",1);
//		showInwardGoodsRegistrationList(data);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
//	}, 'json');
}

function commonInwardGoodsRegistration(searchOption,searchText,fromSpecialData,toSpecialData,from){
	$.post(contextApplicationPath+'/InwardGoodsRegistrationController/searchInwardGoodsRegDetails',
			{
		selectedValue:searchOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		from : from
		}, function(data) {
			var url = "commonInwardGoodsRegistration(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showInwardGoodsRegistrationList(data,from,url);// to call the function which shows the searched item
	}, 'json');
}

//hello1
function showDatePickerField()
{
	var selectedValue=$('#searchOption').val();
	var html="";
		if(selectedValue=="STNNetAmount")
		{
    		html+="<input id='inputFromData3' type='text' class='form-control' placeholder='From Amount' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTOData3' type='text' class='form-control' placeholder='To Amount' style='width: 49%;'>";
		}
    	else if(selectedValue=="STNDate"||selectedValue=="RegDate")
    	{
    		html+="<input id='inputFromDate' type='text' class='dateClassCommon form-control' placeholder='From Date' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTODate' type='text' class='dateClassCommon form-control' placeholder='To Date' style='width: 49%;'>";
    	}
    	else
    	{
    		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control'>";
    	} 	
		$("#searchControl").empty();
		$("#searchControl").html(html);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
}

function searchInwardGoodsRegDetails(from)
{ 
	var searchOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	if(searchOption=="STNNetAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
			 toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (searchOption=="STNDate"||searchOption=="RegDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
//	alert("searchOption In serch : "+searchOption);
	 commonInwardGoodsRegistration(searchOption,searchText,fromSpecialData,toSpecialData,from);
//	$.post(contextApplicationPath+'/InwardGoodsRegistrationController/searchInwardGoodsRegDetails',
//			{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData
//		}, function(data) {
//			showInwardGoodsRegistrationList(data);// to call the function which shows the searched item
////		$('#searchResult').html(html);
//	}, 'json');
}
function showInwardGoodsRegistrationList(data,from,url)
{
	//$('#loader').show();
	var i=0;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html=""
		+"                                        <table class='table table-striped table-hover'>"
		+"                                            <thead class='no-bd'>"
		+"                                                <tr>"
		+"                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>Transporter</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>Company</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>LR No</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>Sending Depo</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>Driver Name</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>Reg Date(DD/MM/YYYY)</strong>"
		+"                                                    </th>"
		+"                                                    <th style='text-align: center;'><strong>Action</strong>"
		+"                                                </tr>"
		+"                                            </thead>"
		+"                                            <tbody class='no-bd-y'>";
	$(data).each(function(index,element){
		if((data.length-2)<i)
			return false;
		html+="                                                <tr style='text-align: center;'>"
			+"                                                    <td>"+ SR_NO +"</td>"
			+"                                                    <td><label id='org_"+i+"'>"+element.ORG_NAME+"</label></td>"
			+"                                                    <td ><label id='trans_"+i+"'>"+element.TRANS+"</label></td>"
			+"                                                    <td ><label id='company_"+i+"'>"+element.COMPANY+"</label></td>"
			+"                                                    <td ><label id='lrNo_"+i+"'>"+element.LR_NO+"</label></td>"
			+"                                                    <td><label id='depo_"+i+"'>"+element.DEPO+"</label></td>"
			+"                                                    <td><label id='driver_"+i+"'>"+element.DRIVER_NAME+"</label></td>"
			+"                                                    <td><label id='regDate_"+i+"'>"+element.REG_DATE+"</label></td>"
			+"                                                    <td style='width: 215px;'>"
			+"                                                        <a class='edit btn btn-blue' href='#' onclick=\"viewInwardGoodsRegistration("+element.GOODS_REGISTRATION_ID+","+element.CLAIM_LETTER_STATUS+")\"><i class='fa fa-external-link'></i></a><a  style='float: left; margin-right: 4px;' class='edit btn btn-dark' href='#' onclick=\"editInwardGoodsRegistration("+i+","+element.GOODS_REGISTRATION_ID+")\"><i class='fa fa-pencil-square-o'></i></a>  <a  style='float: left;' class='delete btn btn-danger' href='#' onclick=\"deleteInwardGoodsRegistration("+element.GOODS_REGISTRATION_ID+")\"><i class='fa fa-times-circle'></i> </a></td>"
			+"                                                </tr>";
		i++;
		SR_NO++;
	});
	html+="                                            </tbody>"
		+"                                        </table>";
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
	$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	$('#loader').hide();
}



//function searchInwardGoodsRegDetails(from)
//{
//    var selectedOption=$('#searchOption').val();
//    var fromSpecialData=null;
//    var toSpecialData=null;
//    var searchText=null;
//    if(selectedOption=="STNNetAmount")
//    {
//         if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
//         {
//             alert("it is not a number");
//             return false;
//   
//         }
//         else
//         {
//             fromSpecialData=$('#inputFromData3').val();
//             toSpecialData=$('#inputTOData3').val();
//         }   
//    }
//    else if (selectedOption=="STNDate")
//    {
//        if($('#inputTODate').val()>=$('#inputFromDate').val())
//        {
//            fromSpecialData=$('#inputFromDate').val();
//            toSpecialData=$('#inputTODate').val();
//        }
//        else
//        {
//            alert('Seleted TO Date should be greater than from Date');
//            return false;
//        }
//       
//    }
//    else
//    {
//        if(!(Boolean($('#searchText').val())))
//        {
//            alert("Please Enter the search text");
//            return false
//        }
//        searchText=$('#searchText').val();
//    }
//    commonInwardGoodsRegistration(searchOption,searchText,fromSpecialData,toSpecialData,from);
////    $.post(contextApplicationPath+'/InwardGoodsRegistrationController/searchInwardGoodsRegDetails',
////            {selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData
////        }, function(data) {
////            showInwardGoodsRegistrationList(data);// to call the function which shows the searched item
//////        $('#searchResult').html(html);
////    }, 'json');
//}

function deleteInwardGoodsRegistration(inwardGoodsRegistrationId)
{
	//alert("in deleteInwardGoodsRegistration="+inwardGoodsRegistrationId);
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/InwardGoodsRegistrationController/deleteInwardGoodsRegistration', {
			inwardGoodsRegistrationId : inwardGoodsRegistrationId
		}, function(data) {
			InwardGoodsRegistrationList();
			alert(data.MSG);
		}, 'json');
    }
	
}
function viewInwardGoodsRegistration(inwardGoodsRegistrationId,claimLetterStatus)
{
	AddClaimLetter(inwardGoodsRegistrationId,claimLetterStatus);
}
/*function editInwardGoodsRegistration(inwardGoodsRegistrationId)
{
	alert("in edit InwardGoodsRegistration="+inwardGoodsRegistrationId);
}*/

function AddClaimLetter(inwardGoodsRegistrationId,claimLetterStatus)
{
	var i=0;
	var html="                                                        <div class='modal fade' id='modal-responsive' tabindex='-1' role='dialog' aria-labelledby='myLargeModalLabel' aria-hidden='true'>"
		+"                                                            <div class='modal-dialog modal-lg'>"
		+"                                                                <div class='modal-content'>"
		+"                                                                    <div class='modal-header'>"
		+"                                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true' onclick='hidePopUp()'>&times;</button>"
		+"                                                                        <h4 class='modal-title' id='myModalLabel'>View Inward Goods</h4>"
		+"                                                                    </div>";
	$.post(contextApplicationPath+'/InwardGoodsRegistrationController/loadInwardGoodsRegistrationUsignId',{inwardGoodsRegistrationId : inwardGoodsRegistrationId}, function(data) {
		$(data).each(function(index,element){
			//commonData for show images (Dont delete this statement)
			commonData = element;
			 html+="                                                                    <div class='modal-body'>"
			   + "    																	  <div class='row'>"
	           +"       												  <div class='col-md-4'>"
			   +"            												 <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Organization Name: - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.ORG_NAME+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Transporter : -</strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.TRAN+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Company : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.COMP+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Sending Depo : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.DEPO+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>No of Cases : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.NO_OF_CASES+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Registration Date : -</strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.REG_DATE+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>LR No : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.LR_NO+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"            												 <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>LR File : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"//
			   //+"                                                                <img onClick='showImagePopup(\""+element.LR_IMAGE_PATH+"\")' src='"+element.LR_IMAGE_PATH+"' alt='Smiley face' height='100' width='100'>"
			   +"                                                                <img onClick='showLrImagesListPopup()' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                        </div>"
			   +"       												 <div class='col-md-4'>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Unloading Charges : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.UNLOADING_CHARGES+" Rs</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Driver Name : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.DRIVER_NAME+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Unloader  Name : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.UNLOADER_NAME+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Time : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.TIME+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Received By : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.RECEIVED_BY+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Transporter Charges : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.TRAN_CHARGES+" Rs</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Hamali Charges : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.HAMALI_CHAR+" Rs</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                            <div class='form-group'>"
			   +"                                                                <label class='form-label'><strong>Remark : - </strong>"
			   +"                                                                </label>"
			   +"                                                                <span class='tips'></span>"
			   +"                                                                <label class='form-label'><strong>"+element.REMARK+"</strong>"
			   +"                                                                </label>"
			   +"                                                            </div>"
			   +"                                                        </div>"
			   +"													  </div>"
			   +"                                                   </div>"
			   +"                                                <div class='panel-body'>"
			   +"                                                    <div class='row'>"
			   +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			   +"                                                            <table class='table table-striped table-hover'>"
			   +"                                                                <thead class='no-bd'>"
				+"                                                                    <tr>"
				+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
				+"                                                                        </th>"
				+"                                                                        <th style='text-align: center;'><strong>STN No</strong>"
				+"                                                                        </th>"
				+"                                                                        <th style='text-align: center;'><strong>STN Image</strong>"
				+"                                                                        </th>"
				+"                                                                        <th style='text-align: center;'><strong>STN Date</strong>"
				+"                                                                        </th>"
				+"                                                                        <th style='text-align: center;'><strong>Gross Amount</strong>"
				+"                                                                        </th> "
				+"                                                                        <th style='text-align: center;'><strong>Vat Amount</strong>"
				+"                                                                        </th> "
				+"                                                                        <th style='text-align: center;'><strong>Discount Amount</strong>"
				+"                                                                        </th> "
				+"                                                                        <th style='text-align: center;'><strong>Net Amount</strong>"
				+"                                                                        </th> "
				+"                                                                    </tr>"
				+"                                                                </thead>"
				+"                                                                <tbody class='no-bd-y'>";
						var invoiceDetails=element.INVOICE_DETAILS;
						 $(invoiceDetails).each(function(index,element){
						html+="                                                                    <tr style='text-align: center;'>"
						+"                                                                        <td>"+ (i+1) +"</td>"
						+"                                                                        <td>"+element.STN_NO+"</td>"
						+"                                                                        <td><img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showStnImagesListPopup("+i+")'></td>"
						+"                                                                        <td>"+element.STN_Date+"</td>"
						+"                                                                        <td>"+element.Gross_Amount+" Rs </td>"
						+"                                                                        <td>"+element.Vat_Amount+" Rs</td>"
						+"                                                                        <td>"+element.Discount_Amount+" Rs</td>"
						+"                                                                        <td>"+element.Net_Amount+" Rs</td>"
						+"                                                                    </tr>";
						i++;
						 });
				html+="                                                                </tbody>"
				+"                                                            </table>"
				+"                                                        </div>"
				+"                                                    </div>"
				+"                                                </div>";
			   
		
				//CLAIM_LETTER_STATUS
		if(element.CLAIM_LETTER_STATUS==0)
			{
			html+="<form id='addClaimLetter' action='"+contextApplicationPath+"/InwardGoodsRegistrationController/saveClaimLetter' method='post' enctype='multipart/form-data'>"
			+"<input type='hidden' name='inwardGoodsRegistrationId' value='"+inwardGoodsRegistrationId+"'>"
			+"                                                                    <div class='modal-body'>"
			+"                                                                        <div class='row'>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-1' class='control-label'>Upload Claim Letter</label>"
			+"                                                                                    <input type='file' class='form-control' id='claimLetterId' name='claimLetter'>"
			+"                                                                                </div>"
			+"                                                            					  <div id='claimLetterImageDivId'></div>"
			+"                                                            					  <div class='form-group'>"
			+"                                                				  					<div id='claimLetterImageAddMoreButtonId'><button onclick=\"addFileConrol('claimLetterImageDivId','claimLetterImageAddMoreButtonId','claimLetter',1)\" class='btn btn-primary'>Add More Files</button></div>"
			+"                                                            					  </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-1' class='control-label'>Upload Pictures</label>"
			+"                                                                                    <input type='file' class='form-control' id='uploadPicturesId' name='uploadPictures'>"
			+"                                                                                </div>"
			+"                                                            					  <div id='claimPicturesImageDivId'></div>"
			+"                                                            					  <div class='form-group'>"
			+"                                                				  					<div id='claimPicturesImageAddMoreButtonId'><button onclick=\"addFileConrol('claimPicturesImageDivId','claimPicturesImageAddMoreButtonId','uploadPictures',1)\" class='btn btn-primary'>Add More Files</button></div>"
			+"                                                            					  </div>"
			+"                                                                            </div>"
			+"                                                                        </div>"
			+"                                                                        <div class='row'>"
			+"                                                                            <div class='col-md-6'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-2' class='control-label'>Add Description</label>"
			+"                                                                                    <textarea class='form-control' id='descId' placeholder='Add Description' name='description' required></textarea>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='modal-footer'>"
			+"                                                                        <button type='Submit' class='btn btn-primary'>Save</button>"
			//+"                                                                        <button type='button' class='btn btn-success'>Email</button>"                                                                               
			+"                                                                    </div>"
			+"</form>";
			}
		else{
			html+=""
				+"                                                                    <div class='modal-body'>"
				+"                                                                        <div class='row'>"
				+"                                                                            <div class='col-md-4'>"
				+"                                                                                <div class='form-group'>"
				+"                                                                                    <label for='field-1' class='control-label'><b style='font-size: medium;'>Claim Letter Details :-</b></label>"
				+"                                                                                </div>"
				+"                                                                            </div>"
				+"																		  </div>"
				+"                                                                        <div class='row'>"
				+"                                                                            <div class='col-md-4'>"
				+"                                                                                <div class='form-group'>"
				+"                                                                                    <label for='field-1' class='control-label'>Upload Claim Letter</label>"
				+"                                                                                    <span class='tips'></span>"
				+"                                                                					  <img onClick='showClaimLetterImagesListPopup()' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"
				+"                                                                                </div>"
				+"                                                                            </div>"
				+"                                                                            <div class='col-md-4'>"
				+"                                                                                <div class='form-group'>"
				+"                                                                                    <label for='field-1' class='control-label'>Upload Claim Pictures</label>"
				+"                                                                                    <span class='tips'></span>"
				+"                                                                					  <img onClick='showClaimPicturesImagesListPopup()' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"
				+"                                                                                </div>"
				+"                                                                            </div>"
				+"                                                                        </div>"
				+"                                                                        <div class='row'>"
				+"                                                                            <div class='col-md-6'>"
				+"                                                                                <div class='form-group'>"
				+"                                                                                    <label for='field-2' class='control-label'><strong>Add Description :-</strong></label>"
				+"                                                                                    <span class='tips'></span>"
				+"                                                                                    <label class='form-label'>"+element.CLAIM_DESC+""
				+"                                                                                </div>"
				+"                                                                            </div>"
				+"                                                                        </div>"
				+"                                                                    </div>";
		}
		html+="                                                                    <div class='modal-footer'>"
			+"                                                                        <button type='button' class='btn btn-danger' data-dismiss='modal' onclick='hidePopUp()'>Close</button>"                                                                                   
			+"                                                                    </div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>";
			$('#popup').html(html);
			if(element.CLAIM_LETTER_STATUS==0)
			{
			ajaxAddClaimLetter('addClaimLetter');
			}
			$("#modal-responsive").addClass("in");
			$("#modal-responsive").attr("aria-hidden","false");
			$("#modal-responsive").css("display","block");
		});
	}, 'json');
}
function ajaxAddClaimLetter(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					hidePopUp();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function showLrImagesListPopup(){
		showListOfImagesPopup(commonData.LR_IMAGE_PATH);
}
function showStnImagesListPopup(index){
	showListOfImagesPopup(commonData.INVOICE_DETAILS[index].STN_Image);
}
function showClaimLetterImagesListPopup(){
	showListOfImagesPopup(commonData.CLAIM_LETTER);
}
function showClaimPicturesImagesListPopup(){
	showListOfImagesPopup(commonData.CLAIM_UPLOAD_PICTURE);
}