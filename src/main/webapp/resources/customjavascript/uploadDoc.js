function excelUpload()
{
	var html = ""
		+"   		<div class='col-md-4 page-title'> <i class='icon-custom-left'></i>"
		+"                  <h3><strong>Upload Document</strong></h3>"
		+"              </div>"
		+"              <div class='row'>"
		+"                  <div class='col-md-6'>"
		+"                      <div class='panel panel-default'>"
		+"                          <div class='panel-body'>"
		+"	                            <form id='uploadZoneReportFormId' action='"+ contextApplicationPath+"/cnf/uploadZoneReport' method='post' enctype='multipart/form-data'>"
		+"                              <div class='row'>"
		+"                                  <div class='col-md-4'>"
		+"                                      <div class='form-group'>"
		+"                                          <label class='form-label'><strong>Document Name</strong>"
		+"                                          </label>"
		+"                                          <span class='tips'></span>"
		+"                                          <div class='controls'>"
		+"                                              <select id='selectName' name='reportName' class='form-control' class='form-control'>"
		+"                                                  <option value='OUTSTANDING'>Select Report </option>"
		//+"                                                  <option value='OUTSTANDING'>Outstanding Report</option>"
		//+"                                                  <option value='PENDING'>Pending Sales Report</option>"
		+"                                                  <option value='LR'>LR Report</option>"
		//+"                                                  <option value='STOCK'>Stock Report</option>"
	   // +"                                                  <option value='DISTRIBUTOR'>Distributor </option>"
	    //+"                                                  <option value='PRODUCT'>Product </option>"
	    //+"                                                  <option value='EMPLOYEE'>Employee </option>"
		+"                                              </select>"
		+"                                          </div>"
		+"                                      </div>"
		+"                                  </div>"
		+"                                  <div class='col-md-4'>"
		+"                                      <div class='form-group'>"
		+"                                          <label class='form-label'><strong>Select Excel Sheet </strong>"
		+"                                          </label>"
		+"                                          <span class='tips'></span>"
		+"                                          <div class='controls'>"
		+"                                              <input type='file' id='uploadExcelFile' name='uploadReportExcelFile' class='form-control' required>"
		+"                                          </div>"
		+"                                      </div>"
		+"                                  </div>"
		+"                                  <div class='col-sm-9 col-sm-offset-3'>"
		+"                                      <div class='pull-right'>"
		+"                                          <button type='button' onclick='return validateUploadExcelZoneReportForm(\"#uploadZoneReportFormId\")' id='uploadExcelSubBtn' name='uploadExcelSubBtn' class='btn btn-info m-b-10'>Submit</button>"
		+"                                          <button id='uploadExcelResetBtn' name='uploadExcelResetBtn' type='reset' class='btn btn-danger m-b-10'>Reset</button>"
		+"                                     </div>"
		+"                                  </div>"
		+"                              </div>"
		+"                              </form>"
		+"                          </div>"
		+"                      </div>"
		+"                  </div>"
		+"              </div>"
	$("#main-content").html(html); 
	ajaxAddZoneReport('uploadZoneReportFormId');
}      

function validateUploadExcelZoneReportForm(formId)
{
	showLoader();
   	$('#uploadZoneReportFormId').submit();
	return true;
}

function ajaxAddZoneReport(FormId)
{
	var options = {
		dataType: 'json',
		success : function(data) {
			      hideLoader();
		          alert(data.msg);      
		}
	};
	alert(options);
	$('#' + FormId).ajaxForm(options);
}	
