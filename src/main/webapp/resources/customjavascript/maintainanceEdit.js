function stateEditPopUp(stateId)
{
	var html="" 
		+ "<div class='modal fade in' id='modal-responsive' >"
	    + "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>Edit State</strong></h4>"
		+ "            </div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>";
	$.post(contextApplicationPath+'/MaintenanceController/EditStateRegistrationDetails', {stateId : stateId}, function(data) {
			$(data).each(function(index,element){ 
		html+="<form id='editStateDetails' action='"+contextApplicationPath+"/MaintenanceController/updateState' method='post'>"
		+"<input type='hidden' name='organizationId' value='"+element.ORG_ID+"'>"
		+"<input type='hidden' name='stateId' value='"+stateId+"'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+ "                                        <div class='form-group'>"
		+"                                                                <label class='form-label'><strong> Organization : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <label class='form-label'><strong>"+element.ORG_NAME+"</strong>"
		+"                                                                </label>"
		+"                                                            </div>"  
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>State Name</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' value='"+element.STATE_NAME+"' name='stateName' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                       </div>"
		+"                                                    </div>"
		+ "            <div class='modal-footer text-center'>"
		+ "                <button type='submit'  class='btn btn-danger' onclick='validateFormDetails(\"#editStateDetails\")'>Update</button>"
        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "            </div>"
		+"</form>";
		
		html+="                                                </div>"
		    +"                                            </div>"
            + "        </div>"
            + "    </div>"
            + "</div>";
				$('#popup').html(html);
				ajaxEditStateDetails('editStateDetails');
				$("#modal-responsive").addClass("in");
				$("#modal-responsive").attr("aria-hidden","false");
				$("#modal-responsive").css("display","block"); 
				
				$('#loader').hide();
			});
	}, 'json');	
}

function ajaxEditStateDetails(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					if(element.RESULT==true)
					{
						alert(element.MSG);
						$('#modal-responsive').remove();
						stateList(true,true);
					}
					else
					{
						alert(element.MSG);
					}
					$('#loader').hide();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}



function districtEditPopUp(districtId)
{
		var html="" 
			+ "<div class='modal fade in' id='modal-responsive' >"
		    + "    <div class='col-md-13'>"
			+ "        <div class='modal-content'>"
			+ "            <div class='modal-header'>"
			+ "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			+ "                <h4 class='modal-title' id='myModalLabel'><strong>Edit District</strong></h4>"
			+ "            </div>"
			+"                                            <div class='panel panel-default'>"
			+"                                                <div class='panel-body'>";
	$.post(contextApplicationPath+'/MaintenanceController/EditDitrictRegistrationDetails', {districtId : districtId}, function(data) {
			$(data).each(function(index,element){
			html+="<form id='editDistrictDetails' action='"+contextApplicationPath+"/MaintenanceController/updateDistrict' method='post'>"
			+"<input type='hidden' name='districtId' value='"+districtId+"'>"
			+"<input type='hidden' name='stateId' value='"+element.STATE_ID+"'>"
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+"                                                                <label class='form-label'><strong> Organization : - </strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <label class='form-label'><strong>"+element.ORG_NAME+"</strong>"
			+"                                                                </label>"
			+"                                                            </div>"  
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>District Name</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' class='form-control' value='"+element.DISTRICT_NAME+"' name='districtName' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>"
			+"                                                        <div class='col-md-4'>"
			+ "                                        <div class='form-group'>"
			+"                                                                <label class='form-label'><strong> State : - </strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <label class='form-label'><strong>"+element.STATE_NAME+"</strong>"
			+"                                                                </label>"
			+"                                                            </div>"                                                             
			+"                                                       </div>"
			+"                                                       </div>"
			+ "            <div class='modal-footer text-center'>"
			+ "                <button type='submit'  class='btn btn-danger' onclick='validateFormDetails(\"#editDistrictDetails\")' >Update</button>"
	        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	        + "            </div>"
			+"</form>";
		html+="                                                </div>"
		    +"                                            </div>"
            + "        </div>"
            + "    </div>"
            + "</div>";
				$('#popup').html(html);
				$("#modal-responsive").addClass("in");
				$("#modal-responsive").attr("aria-hidden","false");
				$("#modal-responsive").css("display","block"); 
				ajaxEditDistrictDetails('editDistrictDetails');
				$('#loader').hide();
			});
	}, 'json');	
}

function ajaxEditDistrictDetails(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					if(element.RESULT==true)
					{
						alert(element.MSG);
						$('#modal-responsive').remove();
						stateList(false,true);
					}
					else
					{
						alert(element.MSG);
					}
				$('#loader').hide();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function stateDropDownListforEdit(stateId)
{
	var html="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Select State</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>"
		+"<select class='form-control' class='form-control' id='stateId' name='stateId'>" ;
	$.post(contextApplicationPath+'/StateController/stateDropDownList', {stateId : stateId}, function(data) {
		$(data).each(function(index,element){
			if(stateId==element.STATE_ID)
				{
				html+="<option selected='selected' value='"+element.STATE_ID+"'>"+element.STATE_NAME+"</option>";
		        }
			else
				{
				html+="<option value='"+element.STATE_ID+"'>"+element.STATE_NAME+"</option>";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
	$('#stateDropDown').html(html);
}, 'json');
}


function cityEditPopUp(orgName,stateName,distName,districtId,cityId)
{
	var html="" 
		+ "<div class='modal fade in' id='modal-responsive112233' >"
	    + "    <div class='col-md-13'>"
		+ "        <div class='modal-content'>"
		+ "            <div class='modal-header'>"
		+ "                <input value='x' onClick='hideSubPopUp(\"modal-responsive112233\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
		+ "                <h4 class='modal-title' id='myModalLabel'><strong>Edit Cities</strong></h4>"
		+ "            </div>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>";
	$.post(contextApplicationPath+'/MaintenanceController/EditCityRegistrationDetails', {cityId : cityId}, function(data) {
			$(data).each(function(index,element){
			html+="<form id='editCityDetails' action='"+contextApplicationPath+"/MaintenanceController/updateCity' method='post'>"
			+"<input type='hidden' name='cityId' value='"+cityId+"'>"//
			+"<input type='hidden' name='districtId' value='"+element.DISTRICT_ID+"'>"
			
			
			+"                                                    <div class='row'>"
			+"                                                        <div class='col-md-4'>"
			+ "                                                          <div class='form-group'>"
			+"                                                                <label class='form-label'><strong> Organization : - </strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <label class='form-label'><strong>"+element.ORG+"</strong>"
			+"                                                                </label>"
			+"                                                            </div>"  
			+ "                                        <div class='form-group'>"
			+"                                                                <label class='form-label'><strong> District : - </strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <label class='form-label'><strong>"+element.DISTRICT_NAME+"</strong>"
			+"                                                                </label>"
			+"                                                            </div>"  
			+"                                                       </div>"
			+"                                                       <div class='col-md-4'>"
			+ "                                                         <div class='form-group'>"
			+"                                                                <label class='form-label'><strong> State : - </strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <label class='form-label'><strong>"+element.STATE_NAME+"</strong>"
			+"                                                                </label>"
			+"                                                            </div>"  
			+"                                                            <div class='form-group'>"
			+"                                                                <label class='form-label'><strong>City Name</strong>"
			+"                                                                </label>"
			+"                                                                <span class='tips'></span>"
			+"                                                                <div class='controls'>"
			+"                                                                    <input type='text' class='form-control' value='"+element.CITY_NAME+"' name='cityName' required>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                       </div>"
			+"                                                    </div>"
			+ "            <div class='modal-footer text-center'>"
			+ "                <button type='submit'  class='btn btn-danger' onclick='validateFormDetails(\"#editCityDetails\")'>Update</button>"
	        + "                <button type='button' onClick='hideSubPopUp(\"modal-responsive112233\")'  class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	        + "            </div>"
			+"</form>";
			
			html+="                                                </div>"
			    +"                                            </div>"
	            + "        </div>"
	            + "    </div>"
	            + "</div>";
					$('#cityEditPopup').html(html);
					$("#modal-responsive112233").addClass("in");
					$("#modal-responsive112233").attr("aria-hidden","false");
					$("#modal-responsive112233").css("display","block"); 
					ajaxEditCityDetails(orgName,stateName,distName,districtId,'editCityDetails');
					$('#loader').hide();
		
				});
		}, 'json');	
}

function ajaxEditCityDetails(orgName,stateName,distName,districtId,FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					if(element.RESULT==true)
					{
						alert(element.MSG);
						$('#modal-responsive112233').remove();
						$('#modal-responsive123').remove();
						viewCityList(orgName,stateName,distName,districtId);
					}
					else
					{
						alert(element.MSG);
					}
				$('#loader').hide();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}
function loadDistrictDropDownListForEditCity(districtId)
{
	var html="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Select District</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>"
		+"<select class='form-control' class='form-control' id='districtId' name='districtId'>" ;
	$.post(contextApplicationPath+'/DistrictController/districtDropDownList', {districtId : districtId}, function(data) {
		$(data).each(function(index,element){
			if(districtId==element.DISTRICT_ID)
				{
				
				alert("District Id : "+districtId+"and "+element.DISTRICT_ID);
				html+="<option selected='selected' value='"+element.DISTRICT_ID+"'>"+element.DISTRICT_NAME+"</option>";
		        }
			else
				{
				html+="<option value='"+element.DISTRICT_ID+"'>"+element.DISTRICT_NAME+"</option>";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
			alert("In district drop down list");
	$('#districtDropDown').html(html);
}, 'json');
}
