function AddOutWardGoods()
{
	var html=""
		+"<div class='row'>"
		+"                    <div class='col-md-6'>"
		+"                        <div class=''>"
		+"                            <ul id='myTab2' class='nav nav-tabs nav-dark'>"
		+"                                <li class='active' id='AddOrderEntryRegistration'><a href='#' data-toggle='tab' onclick=\"AddOrderEntryRegistration()\"><h5><strong>Order Entry Registration</strong></h5></a></li>"
		+"                                <li class='' id='invoiceEntryRegistrationListId'><a href='#tab2_2' data-toggle='tab' onclick=\"invoiceEntryRegistrationList()\"><h5><strong>Invoice Entry Registration</strong></h5></a></li>"
		+"                                <li class=''><a href='#tab2_3' data-toggle='tab' onclick=\"dispatchEntryRegistrationList()\"><h5><strong>Dispatch Entry Registration</strong></h5></a></li>"
		+"                                <li class=''><a href='#tab2_4' data-toggle='tab' onclick=\"tripList()\"><h5><strong>Trip</strong></h5></a></li>"
		+"                                <li class=''><a href='#tab2_5' data-toggle='tab' onclick=\"getPassList()\"><h5><strong>LR Updation</strong></h5></a></li>"
		+"                            </ul>"
		+"                            <div id='myTabContent' class='tab-content'>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                </div>";
	$('#main-content').html(html);
	$('#subMenu'+lastClickId).hide();
	AddOrderEntryRegistration();
}

function subTabsForOutWardGoodsReg(index)
{
	 var html = ""
	  +"                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
	  +"                                            <li id='outWordOrderTabEnetry1' class=''><a href='#tab2_6' data-toggle='tab' onclick='AddOrderEntryRegistration()'><h5><strong>Add Order</strong></h5></a></li>"
	  +"                                            <li id='outWordOrderTabEnetry2' class=''><a href='#tab2_7' data-toggle='tab' onclick='OrderEntryRegistrationList()'><h5><strong>Add Order Product</strong></h5></a></li>"
	  +"                                            <li id='outWordOrderTabEnetry3' class=''><a href='#tab2_7' data-toggle='tab' onclick='ViewOrderEntryRegistrationList()'><h5><strong>View Order</strong></h5></a></li>"
	  +"                                        </ul>";
	 $('#subTabsForOrderEntryRegistration').html(html);
	 		if(index==1)
	 			{
	 			$('#outWordOrderTabEnetry1').addClass("active");
	 			}
	 		else if(index==2)
	 			{
				$('#outWordOrderTabEnetry2').addClass("active");
	 			}
	 		else if(index==3)
	 			{
	 			$('#outWordOrderTabEnetry3').addClass("active");
	 			}
}


function AddOrderEntryRegistration()
{	//var flag=false
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_1'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Order Entry Registration</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOrderEntryRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"<form id='addOutWardOrdeEntryRegistration' action='"+contextApplicationPath+"/OutWardGoodsController/saveOrderEntryRegistration' method='post' enctype='multipart/form-data'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                            	  <div id='organiztionDropDown' class='form-group'>"
		+"                                                            	  </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Order No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='Order No' name='orderNo' id='orderNo'  onKeyup='checkName()' disabled='disabled' required>"
		+"																  <div id='error' class='validationError' style='color:red' ></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Mode</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"  															 <div id='orderModeDropDown' class='controls'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Remark</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' class='form-control' placeholder='Remark' name='remark'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"																<div id='companyDropDown' onchange='check()'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Stockist</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <select  id='StockistName' name='stockistId' class='form-control' class='form-control' required>"
		+"                                                                        <option disabled selected> Select Stockist</option>"
		+"                                                                    </select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Date</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='datepicker form-control stnDate' type='text' placeholder='Order Date' style='height: 36px; padding-left: 10px;' name='orderDate' id='orderDate' onchange=\"errorMsgEmpty('orderDate','dateError_msg')\" required>"
		+"																  <div id='dateError_msg' class='validationError' style='color:red'></div>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Order Copy</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input class='form-control' type='file' style=' padding-left: 10px;' name='orderCopy'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='orderCopyImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='orderCopyImageAddMoreButtonId'><button onclick=\"addFileConrol('orderCopyImageDivId','orderCopyImageAddMoreButtonId','orderCopy',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='col-md-12 col-sm-12 col-xs-12 table-responsive' style=' margin-top: 10PX;'>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right'>"
		+"                                                                 <button class='btn btn-primary m-b-10' id='submit' onclick='return validateFormDetails(\"#addOutWardOrdeEntryRegistration\")'>Save</button>"
		+"                                                                <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"</form>"
		+"                                            </div>"	
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').html(html);
	subTabsForOutWardGoodsReg(1);
	loadOrderMode();
	loadOutWardGoodsRegistrationOrganiztionDropDown();
	$("#orderDate").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	ajaxAddOutWardOrdeEntryRegistration('addOutWardOrdeEntryRegistration');
}
																				
function checkName()
{  
	var orderNo= $("#orderNo").val();
	var companyId=$("#comapnyId").val();
	//alert(orderNo+" "+companyId);
	$.post(contextApplicationPath+'/OutWardGoodsController/checkName', {orderNo : orderNo,companyId : companyId}, function(data) {
		$(data).each(function(index, element) {
				if(element.FLAG==true){
					commonData=true;
					$("#error").html(element.MSG);
				}else{
					$("#error").html("");
					commonData=false;
				}
			});
		}, 'json');
}

function check() {
	if(document.getElementById('comapnyId').value != "")
	   document.getElementById('orderNo').disabled = false;
	else
	   document.getElementById('orderNo').disabled = true;
	}

function ajaxAddOutWardOrdeEntryRegistration(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					if(element.STATUS=='false')
						{
						alert(element.MSG);
						}
					else
						{
						$('#'+ FormId)[0].reset();
						$("#dialog-confirm").html("Do you want to add product or not?");
					    // Define the Dialog and its properties.
					    $("#dialog-confirm").dialog({
					        resizable: false,
					        modal: true,
					        title: "Product Verification",
					        buttons: {
					            "Yes": function () 
					            {
					            	OrderEntryRegistrationList();
					                $(this).dialog('close');
					            },
					            "No": function () 
					            {
					                $(this).dialog('close');
					                invoiceEntryRegistrationList();
					                $('#AddOrderEntryRegistration').removeClass("active");
					                $('#invoiceEntryRegistrationListId').addClass("active");
					            }
					        }
					    });
						}
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function loadOutWardGoodsRegistrationOrganiztionDropDown()
{
	var userType='';
	var html="<div class='form-group'>"
	+"<label class='form-label'><strong>Select Organization</strong>"
	+"</label>"
	+"<span class='tips'></span>"
	+"<div class='controls'>"
	+"<select class='form-control' class='form-control' id='orgId' name='selectedOrganiztionNameId' onchange='OutGoodsRegistrationOrganizationChange()' required>" 
	+"<option selected disabled>Select Organiztion</option>";
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/organizationDropDownList', function(data) {
		$(data).each(function(index, element) {
			userType=element.USER_TYPE;
			if(element.USER_TYPE=='SuperAdmin')
				{
				html+="<option value='"+element.ORG_ID+"'>"+element.ORG_NAME+"</option>";
				}
			else
				{
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='selectedOrganiztionNameId' id='orgId'>" +
						""+element.ORG_NAME+"";
				}
		}); 
		html+=" </select>"
			+"</div>"
			+"</div>";
		content+="</div>"
		+"</div>";
		if(userType=='SuperAdmin')
			{
			$('#organiztionDropDown').html(html);
			}
		else
			{
			$('#organiztionDropDown').html(content);
			OutWardLoadAllCompany($("#orgId").val());
//			getStockistByOrg();
			}
	}, 'json');
}

function OutWardLoadAllCompany(orgId)
{
	$.post(contextApplicationPath+'/CompanyController/getCompanies',{orgId : orgId }, function(data) {
		html="<div class='form-group'>"
			+"<label class='form-label'><strong>Select Company</strong>"
			+"</label>"
			+"<span class='tips'></span>"
			+"<div class='controls'>"
			+"<select class='form-control' class='form-control' name='comapnyId' id='comapnyId' onchange='getStockistDropDown()' required>"
			+"<option selected disabled>Select Company</option>";
		for (var i = 0; i < data.companyArray.length; i++)
		{
			html += "<option value='" + data.companyArray[i].compId + "'>"+ data.companyArray[i].compName + "</option>";
		}
		html+="</select>"
		+"</div>"
		+"</div>";
		$('#companyDropDown').html(html);
	}, 'json');
}
function OutGoodsRegistrationOrganizationChange()
{
	OutWardLoadAllCompany($("#orgId").val());
//	getStockistByOrg();
}

function loadOrderMode()
{
	$.post(contextApplicationPath+'/OrderModeController/getOrderMode', function(data) {
		html="<select class='form-control' class='form-control' name='orderModeName' id='orderModeId' required>"
			+"<option selected disabled>Select Order Mode</option>";
		$(data).each(function(index, element) {
			html+="<option value='"+element.ORDER_MODE_ID+"'>"+element.ORDER_MODE_NAME+"</option>";
		});
		html+="</select>";
		$('#orderModeDropDown').html(html);
	}, 'json');
}

function OrderEntryRegistrationList()
{
	$('#loader').show();
	var i=1;
	//$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardOrderEntryRegistrationOfProductStatusZero', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_3'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Order Entry Registration</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOrderEntryRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutward();'>"
		+"                                                     		  	<option value='Stockist'>Stockist</option>"
		+"														 		 	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
		+ "                                                     		  	<option value='Date'>Search By Date</option>"
	//	+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+"																  	<option value='OrderNo'> Order Number</option>"
		+ "                                                    			   	<option value='OrderMode'> Order Mode</option>"
		+"																</select>"	
		//+"                                                                    <input type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardOrderEntryRegistration()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd' id='a'>"
		+"                                                                    <tr>"
		+"                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Order Id</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Company</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Order Mode</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Date</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                            </th>"
		+"                                                            </tr>"
		+"                                                            </thead>"
		+"                                                                <tbody class='no-bd-y' id='orderEntrySearch'>"
		+"                                                                </tbody>"
		+"                                                            </table>"
		+"                                                        </div>"
		+"                                                     <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	commonOutwardOrderEntryRegistrationController("", "", "", "", 1);
	subTabsForOutWardGoodsReg(2);
	//}, 'json');
}

//////Vijay
function searchOutwardOrderEntryRegistration(){
	$('#orderEntrySearch').html('');
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if (selectedOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonOutwardOrderEntryRegistrationController(selectedOption, searchText, fromSpecialData, toSpecialData, 1);
}

function commonOutwardOrderEntryRegistrationController(selectedOption, searchText, fromSpecialData, toSpecialData, from){
	$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardOrderEntryRegistration',
			{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData,from:from
		}, function(data) {
			var url = "commonOutwardOrderEntryRegistrationController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			ShowOutwardOrderEntryRegistration(data,from,url); 
	}, 'json');
}

function changeFunctionOutward() {
	var selectedValue=$('#searchOption').val();
	var html="";
		if(selectedValue=="Date")
    	{
    		html+="<input id='inputFromDate' type='text' class='dateClassCommon form-control' placeholder='From Date' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTODate' type='text' class='dateClassCommon form-control' placeholder='To Date' style='width: 49%;'>";
    	}
    	else
    	{
    		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control'>";
//    			 + "	<div id='searchText_Error_Msg' class='validationError' style='color:red'></div>";
    	} 	
/*		$("#searchControl").empty();
*/		$("#searchControl").html(html);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
 }

function ShowOutwardOrderEntryRegistration(data,from,url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=1;
	var html=""
		/*+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Order Id</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Company</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Order Mode</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Date</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                            </th>"
		+"                                                            </tr>"
		+"                                                            </thead>"
		+"                                                            <tbody class='no-bd-y'>";*/
	$(data).each(function(index, element) {
		pageCount=element.paginationCount;
		if((data.length-1)<i)
			return false;
		html+="                                                                    <tr style='text-align: center;'>"
			+"                                                                        <td>"+SR_NO+"</td>"
			+"                                                                        <td id='ORDER_ID_"+i+"'>"+ element.ORDER_ID +"</td>"
			+"                                                                        <td id='ORDER_NO_"+i+"'>"+ element.ORDER_NO +"</td>"
			+"                                                                        <td id='ORG_"+i+"'>"+element.ORG+"</td>"
			+"                                                                        <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
			+"                                                                        <td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
			+"                                                                        <td id='ORDER_MODE_"+i+"'>"+element.ORDER_MODE+"</td>"
			+"                                                                        <td id='DATE_"+i+"'>"+element.DATE+"</td>"
			+"                                                                        <td><button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive5' onclick=\"AddOrderProductInOrder("+element.OutWardOrderEntryRegistrationId+","+element.COMPANY_ID+",'"+element.ORDER_NO+"','FromAddOrderProduct')\">Add Order Product</button>"
			+"                                                                    </tr>";
		i++;SR_NO++;
	}); 
	$('#orderEntrySearch').append(html);
	$('#loader').hide();
	paginationViewMore(from,pageCount,url);
}

var PRODUCT_DIV=0;
function AddOrderProductInOrder(outWardOrderEntryRegistrationId,companyId,orderNo,criteria)
{	PRODUCT_NAME=[];
QUANTITY=[];
PRODUCT_DIV=0;
var html="" 
	+ "<div class='modal fade in' id='modal-responsive' >"
	            + "    <div class='col-md-13'>"
	            + "        <div class='modal-content'>"
	            + "            <div class='modal-header'>"
	            + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	            + "                <h4 class='modal-title' id='myModalLabel'><strong>Add Order Product</strong></h4>"
	            + "            </div>"
	            + "            <div class='modal-header'>"
	            + "                <h5 class='modal-title' id='myModalLabel'><strong>Order No :- "+orderNo+"</strong></h5>"
	            + "            </div>"
	            + "  				<div class='panel panel-default' style='border:0px;'>"
	            +"<form id='AddOutWardOrderProduct' action='"+contextApplicationPath+"/OutWardGoodsController/saveOutWardOrderProduct' method='post'>"
	            +"<input type='hidden' name='outWardOrderEntryRegistrationId' value='"+outWardOrderEntryRegistrationId+"'>"
	            +"<input type='hidden' name='criteria' value='"+criteria+"'>"
	            + "						 <div class='panel-body'>"
	            + "    						  <div class='row'>"
				+"<div id='moreProductHtml'>";
		 html+="								   <div id='from_"+PRODUCT_DIV +"'>"
		+"                                               <div class='col-md-4'>"
		+"                                                    <div class='form-group' style='height: 56px;'>"
		+"                                                         <label class='form-label'><strong>Product</strong>"
		+"                                                         </label>"
		+"                                                         <span class='tips'></span>"
		+"                                                         <div class='controls'>"
		+"																<div id='productList_"+ PRODUCT_DIV +"'></div>"
		+"                                                         </div>"
		+"                                                    </div>"
		+"                                               </div>"
		+"                                               <div class='col-md-4'>"
		+"                                                   <div class='form-group'>"
		+"                                                        <label class='form-label'><strong>Quantity</strong>"
		+"                                                        </label>"
		+"                                                        <span class='tips'></span>"
		+"                                                        <div class='controls'>"
		+"                                                             <input type='text' class='form-control' placeholder='Quantity' name='quantity' id='quantity_"+PRODUCT_DIV+"'>"
		+"																  <div id='quantity_Error_Msg"+PRODUCT_DIV+"' class='validationError' style='color:red'></div>"
		+"                                                        </div>"
		+"                                                   </div>"
		+"                                              </div>"
		+"             							   </div>"
		+"</div>"
        + "                                   </div>"
        +"                                    <input type='button' class='btn btn-info m-b-10 pull-right' onclick='moreProductHtmlContent("+companyId+")' data-target='#modal-responsive5' data-toggle='modal' value='Add'>"            
        + "    				 	              <div class='row'>"
		+"                                         <div id='outWardProductAddedHtml'></div>"
        + "                                   </div>"
        + "    				 	              <div class='row'>"
		+ "                                    <div class='modal-footer text-center'>"
		+ "                                         <button type='submit' class='btn btn-danger' data-dismiss='modal' onclick='return validateAddProductFormDetails()'>Submit</button>"
        + "                                         <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "                                    </div>"
        + "                               </div>"
        + "                          </div>"
        +"</form>"
        + "                     </div>"
        + "          </div>"
        + "     </div>"
        + "</div>";
$('#popup').html(html);
ajaxAddOrderProduct('AddOutWardOrderProduct');
loadProductList(companyId);
$("#modal-responsive").addClass("in");
$("#modal-responsive").attr("aria-hidden","false");
$("#modal-responsive").css("display","block");
}

function quantity(PRODUCT_DIV)
{  
	if($('#quantity_'+PRODUCT_DIV+'').val() == "" )
	{
		$('#quantity_Error_Msg'+PRODUCT_DIV+'').html("Please Enter Project Name");
		return false;
	}
	return true;
}

function quantityKeyvalidate(PRODUCT_DIV)
{
	$('#quantity_Error_Msg'+PRODUCT_DIV+'').html("");
}

function quantityup(PRODUCT_DIV)
{  //alert("hiii");
alert($('#quantity_'+index).val())
$('#quantity_Error_Msg'+PRODUCT_DIV+'').html("");
//var num3 = /^[0-9,.]+$/
//	if(!num3.test($('#quantity_'+index+'').val())) {
//		alert("enter numeric only");
//		($('#quantity_'+index).val(""))
//		 return false;
//	}
return true;
}


function ajaxAddOrderProduct(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					PRODUCT_NAME=[];
					QUANTITY=[];
					PRODUCT_DIV=0;
					$("#modal-responsive").remove();
					if(element.CRITERIA=='FromAddOrderProduct')
						{
						OrderEntryRegistrationList();
						}
					else if(element.CRITERIA=='FromMoreProduct')
						{
						ViewOrderEntryRegistrationList();
						}
					else
						{
						invoiceEntryRegistrationList();
						}
					
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}
function validateAddProductFormDetails(){
	var ProductStatus=false;
	for (var i=0; i<PRODUCT_NAME.length; i++)
	{
		if(PRODUCT_NAME[i]=="")
			{
			ProductStatus=false;
			}
		else
			{
			ProductStatus=true;
			break;
			}
	}
	if(ProductStatus==true)
		{
		return true;
		}
	else
		{
		alert("Please add atleast one product...!");
		return false;
		}
}
var PRODUCT_NAME=[];
var QUANTITY=[];
function moreProductHtmlContent(companyId)
{
	//if("Select Product"==$("#productName_"+PRODUCT_DIV).val()||$("#productName_"+PRODUCT_DIV).val()==""||$("#productName_"+PRODUCT_DIV).val()==undefined||$("#quantity_"+PRODUCT_DIV+"").val()==""||$("#quantity_"+PRODUCT_DIV+"").val()==undefined){
		//alert("Please Select Product and Enter quantity....!");
		//return false;
	//	$('#quantity_Error_Msg'+PRODUCT_DIV+'').html("Error: Please input numeric value only");return false;
	//	if($('#quantity_'+PRODUCT_DIV+'').val() == "" )
	//	{
	//		$('#quantity_Error_Msg'+PRODUCT_DIV+'').html("Enter quantity");
	//		return false;
	//	}
		if("Select Product"==$("#productName_"+PRODUCT_DIV).val()||$("#productName_"+PRODUCT_DIV).val()==""||$("#productName_"+PRODUCT_DIV).val()==undefined)
			{
			alert("select product");
			return false;
			}
		if($("#quantity_"+PRODUCT_DIV+"").val()==""||$("#quantity_"+PRODUCT_DIV+"").val()==undefined)
			{
			
			$('#quantity_Error_Msg'+PRODUCT_DIV+'').html("Error: Please enter qantity");
			return false;
			}
	
	var y=1;
	var divNo=0;
	PRODUCT_NAME[PRODUCT_DIV]=$("#productName_"+PRODUCT_DIV+" option:selected").text();
			
	QUANTITY[PRODUCT_DIV]=$("#quantity_"+PRODUCT_DIV+"").val()
	var num = /^[0-9]+$/
			if(!num.test($('#quantity_'+PRODUCT_DIV+'').val())) {
				$('#quantity_Error_Msg'+PRODUCT_DIV+'').html("Error: Please input numeric value only");
				 return false;
			}
    $('#from_'+PRODUCT_DIV).hide();
	console.log(PRODUCT_NAME);
	console.log(QUANTITY);
	var ProductListHtml="                                                     <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                         <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                         </th>"
		+"                                                                         <th style='text-align: center;'><strong>Product</strong>"
		+"                                                                         </th>"
		+"                                                                         <th style='text-align: center;'><strong>Quantity</strong>"
		+"                                                                         </th>"
		+"                                                                         <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                         </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
	
	for (var s = 0; s < PRODUCT_NAME.length; s++) 
	{
	if(PRODUCT_NAME[s]=="")
		{
		
		}
	else
		{
		ProductListHtml+=" <tr style='text-align: center;' id='ProductList"+s+"'>"
			+" <td>"+y+++"</td>"
			+" <td>"+PRODUCT_NAME[s]+"</td>"
			+" <td >"+QUANTITY[s]+"</td>"
			+" <td><a class='delete btn btn-danger' href='#' onclick='RemoveOrderProduct("+s+")'><i class='fa fa-times-circle'></i> </a></td>"
			+" </tr>";
		}
	}
	
	ProductListHtml+="                                                                </tbody>"
		+"                                                            </table>"
		+"                                                        </div>";
	$('#outWardProductAddedHtml').html(ProductListHtml);
	
	
	
			var html="								   <div id='from_"+ ++PRODUCT_DIV +"'>"
			+"                                               <div class='col-md-4'>"
			+"                                                    <div class='form-group' style='height: 56px;'>"
			+"                                                         <label class='form-label'><strong>Product</strong>"
			+"                                                         </label>"
			+"                                                         <span class='tips'></span>"
			+"                                                         <div class='controls'>"
			+"																<div id='productList_"+ PRODUCT_DIV +"'></div>"
			+"                                                         </div>"
			+"                                                    </div>"
			+"                                               </div>"
			+"                                               <div class='col-md-4'>"
			+"                                                   <div class='form-group'>"
			+"                                                        <label class='form-label'><strong>Quantity</strong>"
			+"                                                        </label>"
			+"                                                        <span class='tips'></span>"
			+"                                                        <div class='controls'>"
			+"                                                             <input type='text' class='form-control' placeholder='Quantity' name='quantity' id='quantity_"+PRODUCT_DIV+"'>"
			+"                                                        </div>"
			+"                                                   </div>"
			+"                                              </div>"
			+"             							   </div>"
			+ "                                   </div>";
			$('#moreProductHtml').append(html);
			loadProductList(companyId);
}

function RemoveOrderProduct(divId)
{
	$('#ProductList'+divId).remove();
	$('#from_'+divId).remove();
	PRODUCT_NAME[divId] = "";
	QUANTITY[divId] = "";
}

function loadProductList(comapnyId)
{
	html="<select class='form-control' class='form-control' name='productName' id='productName_"+PRODUCT_DIV+"'>"
		+"<option>Select Product</option>";
	$.post(contextApplicationPath+'/OutWardGoodsController/getProductListUsignCompanyId',{companyId:comapnyId}, function(data) {
		$(data).each(function(index, element) {
			html+="<option value='"+element.PRODUCT_ID+"'>"+element.PRODUCT_NAME+"</option>";
		});
		html+="</select>";
		$("#productList_"+ PRODUCT_DIV+"").html(html);
	}, 'json');
}


// Start Invoice Entry Registration coding
function subTabsForOutWardInvoiceEntryRegistration(flag)
{
	 var html = ""
	  +"                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
	  +"                                            <li id='outWardInvoiceEntryRegistrationTab1' class=''><a href='#tab2_6' data-toggle='tab' onclick='invoiceEntryRegistrationList()'><h5><strong>Add Invoice</strong></h5></a></li>"
	  +"                                            <li id='outWardInvoiceEntryRegistrationTab2' class=''><a href='#tab2_7' data-toggle='tab' onclick='invoiceEntryAddedRegistrationList()'><h5><strong>View</strong></h5></a></li>"
	  +"                                        </ul>";
	 $('#subTabsForInvoiceEntryRegistration').html(html);
	 		if(flag)
	 			$('#outWardInvoiceEntryRegistrationTab1').addClass("active");
	 		else
				$('#outWardInvoiceEntryRegistrationTab2').addClass("active");
}

///////////////// Vijay
function invoiceEntryRegistrationList()
{
	$('#loader').show();
	var i=1;
	//$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardOrderEntryRegistrationOfProductStatusOne', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_2'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Add Invoice Entry Registration</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForInvoiceEntryRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutward();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+"														 		 	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
		//+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                     		  	<option value='Date'>Search By Date</option>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+"																  	<option value='OrderNo'> Order Number</option>"
		+"																</select>"	
		//+"                                                                    <input type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardinvoiceEntryRegistration()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		//Mayur for upload invoice
		+" <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
	//	+"                                                    <div class='row'>"
		+"	                            <form id='uploadReportFormId' action='"+ contextApplicationPath+"/UploadDocumentController/uploadReport' method='post' enctype='multipart/form-data'>"
		+"<div class='col-md-4'>"
		+"                                      <div class='form-group'>"
		+"                                          <label class='form-label'><strong>Select Excel Sheet </strong>"
		+"                                          </label>"
		+"                                          <span class='tips'></span>"
		+"                                          <div class='controls'>"
		+"                                              <input type='file' id='uploadExcelFile' name='uploadReportExcelFile' class='form-control' required>"
		+"                                          </div>"
		+"                                    </div>"
		+"</div>"
		+"<div class='col-md-4'>"
		+"                                      <div class='form-group'>"
		+"                                          <label class='form-label'><strong><h1>       </h1> </strong>"
		+"                                          </label>"
		+"                                          <span class='tips'></span>"
		+"                                          <div class='controls'>"
		+"                                          <button type='button' onclick='return validateUploadExcelZoneReportForm(\"#uploadReportFormId\")' id='uploadExcelSubBtn' name='uploadExcelSubBtn' class='btn btn-info m-b-10'>Submit</button>"
		+"                                          <button id='uploadExcelResetBtn' name='uploadExcelResetBtn' type='reset' class='btn btn-danger m-b-10'>Reset</button>"
		+"                                     </div>"
		+"                                      </div>"
		+"                                  </div>"
		+"</form>"
		+"</div>"
		+"</div>"
		+"</div>"
		+"</div>"
		//--------------------------------------
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd' id='a'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Oranization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Date</strong>"
		+"                                                                        </th>"
//		+"                                                                        <th style='text-align: center;'><strong>Order Mode</strong>"
//		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Add Invoice</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Add Product</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong></th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y' id='selectedRecord'>"
		+"                                                                </tbody>"
		+"																</table>"
		+"                                                        </div>"
		+"														  <div id='invoiceEntryPopup'></div>"
		+"   														<div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	//showInvoiceEntryRegistrationList(data);
	commonOutwardinvoiceEntryRegistrationController("","","","",1);
	subTabsForOutWardInvoiceEntryRegistration(true);
	ajaxAddReport('uploadReportFormId');
//	}, 'json');
}
function validateUploadExcelZoneReportForm(formId)
{
	showLoader();
   	$('#uploadReportFormId').submit();
	return true;
}

function ajaxAddReport(FormId)
{
	var options = {
		dataType: 'json',
		success : function(data) {
			$('#loader').hide();
		          alert(data.msg);      
		}
	};
	$('#' + FormId).ajaxForm(options);
}	
function showInvoiceEntryRegistrationList(data,from,url)
{SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=1;
	var html=""
	/*	+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Oranization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Date</strong>"
		+"                                                                        </th>"
//		+"                                                                        <th style='text-align: center;'><strong>Order Mode</strong>"
//		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Add Invoice</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Add Product</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong></th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";*/
	$(data).each(function(index, element)
			{
		pageCount=element.paginationCount;
		if((data.length-1)<i)
			return false;
			html+="<tr style='text-align: center;' id='OutWardOrderEntry_"+element.OutWardOrderEntryRegistrationId+"'>"
				+" <td>"+ SR_NO +"</td>"
				+" <td id='ORDER_ID_"+i+"'>"+element.ORDER_ID+"</td>"
				+" <td id='ORDER_NO_"+i+"'>"+element.ORDER_NO+"</td>"
				+" <td id='ORG_"+i+"'>"+element.ORG+"</td>"
				+" <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
				+" <td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
				+" <td id='DATE_"+i+"'>"+element.DATE+"</td>"
				+" <td>"
				+" <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive11' onclick='addInvoice("+element.OutWardOrderEntryRegistrationId+","+element.PERCENTAGE+","+element.ORANIZATION_ID+")'>+</button>"
				+" </td>";
				if(element.PRODUCT_STATUS==0)
					{
					html+="<td><button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive5' onclick=\"AddOrderProductInOrder("+element.OutWardOrderEntryRegistrationId+","+element.COMPANY_ID+",'"+element.ORDER_NO+"','FromAddInvoice')\">+</button></td>";
					}
				else
					{
					html+="<td>-</td>";
					}
				
				html+=" <td><a class='edit btn btn-blue' href='#' onclick='viewOrderEntryRegistration("+element.OutWardOrderEntryRegistrationId+")'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' href='#' onclick='editOrderEntryRegistration("+element.OutWardOrderEntryRegistrationId+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick='deleteOrderEntryRegistration("+element.OutWardOrderEntryRegistrationId+")'><i class='fa fa-times-circle'></i> </a>"
				+" </tr>";
			i++	;SR_NO++
		});
		

	$('#selectedRecord').append(html);
	$('#loader').hide();
	paginationViewMore(from,pageCount,url);
	
}

function searchOutwardinvoiceEntryRegistration(){
	$('#selectedRecord').html(''); 
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if (selectedOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false;
		}
		searchText=$('#searchText').val();
	}
	commonOutwardinvoiceEntryRegistrationController(selectedOption,searchText,fromSpecialData,toSpecialData,1);
}
	
	function commonOutwardinvoiceEntryRegistrationController(selectedOption,searchText,fromSpecialData,toSpecialData,from)
	{
	$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardinvoiceEntryRegistration',
		{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData,from:from
	}, function(data) {
		
		var url = "commonOutwardinvoiceEntryRegistrationController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
		showInvoiceEntryRegistrationList(data,from,url); // to call the function which shows the searched item
	//$('#searchResult').html(html);
}, 'json');
}

function viewOrderEntryRegistration(OutWardOrderEntryRegistrationId)
{
	 viewOrderEntryRegistrationPopUp(OutWardOrderEntryRegistrationId);
}

function editOrderEntryRegistration(OutWardOrderEntryRegistrationId)
{
//	alert("in edit Order Entry Registration="+OutWardOrderEntryRegistrationId);
	editOrderEntryRegistrationFunction(OutWardOrderEntryRegistrationId);
}
function deleteOrderEntryRegistration(OutWardOrderEntryRegistrationId)
{
	var r = confirm("Do you want to Delete Order...!");
    if (r == true) 
    {
		$.post(contextApplicationPath+'/OutWardGoodsController/deleteOrder',{OutWardOrderEntryRegistrationId : OutWardOrderEntryRegistrationId} ,function(data) {
			$(data).each(function(index, element) {
				$('#OutWardOrderEntry_'+element.OutWardOrderEntryRegistrationId+'').remove();
				alert(element.MSG);
			}); 
		}, 'json');
    }
}

function addInvoice(OutWardOrderEntryRegistrationId,percentage,organizationId)
{
	 INVOICE_DIV=0;
	 INVOICE_NO=[];
	 GROSS_AMOUNT=[];
	 TAX_AMOUNT=[];
	 NET_AMOUNT=[];
	 DISCOUNT=[];
	var html="<div class='modal fade' id='modal-responsive1234' aria-hidden='true'>"
	+"	<div class='col-md-13'>"
	+"		<div class='modal-content'>"
	+"			<div class='modal-header'>"
	+"				<button type='button' class='close' data-dismiss='modal' aria-hidden='true' onclick='hideSubPopUp(\"modal-responsive1234\")'>x</button>"
	+"				<h4 class='modal-title' id='myModalLabel'><strong>Invoice Entry</strong></h4>"
	+"			</div>"
	+"<form id='saveInvoiceEntry' action='"+contextApplicationPath+"/OutWardGoodsController/saveInvoiceEntry' method='post'>"
	 +"<input type='hidden' name='outWardOrderEntryRegistrationId' value='"+OutWardOrderEntryRegistrationId+"'>"
	 +"<input type='hidden' name='organizationId' value='"+organizationId+"'>"
	 +"<input type='hidden' name='radioDiscount' value='' id='radioDiscountId'>"
	+"			<div class='modal-body' style=' text-align: left;'>"
	+"				<div class='row'>"
	+"<div id='moreInvoiceHtmlDiv'>"
	+"<div id='from_"+INVOICE_DIV +"'>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-1' class='control-label'>Invoice No</label></br>"
	+"							<input type='text' class='form-control'  placeholder='Invoice No' name='invoiceNo' id='invoiceNoId"+INVOICE_DIV+"' onchange =\"invoiceNoIdKeyUp("+INVOICE_DIV+")\">"
	+ "							<div id='invoiceNoId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-2' class='control-label'>Date</label>"
	+"							</br>"
	+"							<input class='datepicker form-control invoiceDate' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' name='invoiceDate' id='invoiceDate"+INVOICE_DIV+"' onchange =\"invoiceDateKeyUp("+INVOICE_DIV+")\" parsley-required='true'>"
	+ "							<div id='invoiceDate_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red' ></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-3' class='control-label'>Gross Amount</label>"
	+"							</br>"
	+"							<input type='text' class='form-control'  placeholder='Gross Amount' name='grossAmount' id='grossAmountId"+INVOICE_DIV+"'  >"//onkeyup =\"grossAmountIdKeyUp("+INVOICE_DIV+")\"
	//+ "							<div id='grossAmountId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"					<div class='form-group'>"
	+"							<label for='field-5' class='control-label'>Tax Amount</label></br>"
	+"							<input type='text' class='form-control'  placeholder='Tax Amount' name='taxAmount' id='taxAmountId"+INVOICE_DIV+"'>"//onkeyup =\"taxAmountIdKeyUp("+INVOICE_DIV+")\"
	//+ "							<div id='taxAmountId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-3' class='control-label'>VAT Amount</label>"
	+"							</br>"
	+"							<input type='text' class='form-control'  placeholder='VAT Amount' name='vatAmount'  id='vatAmountId"+INVOICE_DIV+"' >"//onkeyup =\"vatAmountIdKeyUp("+INVOICE_DIV+")\"
	//+ "							<div id='vatAmountId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-6' class='control-label'>Discount</label></br>"
	+"							<div class='col-lg-12'>"
	+"								<div class='pos-rel' style=' margin-top: 16px;'>"
	//+"									<input   type='radio' class='rdoNumber' name='discount' id='onclickyes"+INVOICE_DIV+"' value='Yes' onclick=\"handleChangeDiscount("+OutWardOrderEntryRegistrationId+","+INVOICE_DIV+","+percentage+")\" >" // onkeyup =\"discountKeyUp("+INVOICE_DIV+")\"
	+"									<input   type='radio' class='rdoNumber' name='discount' id='onclickyes"+INVOICE_DIV+"' value='Yes' onchange =\"invoiceDiscountKeyUp("+INVOICE_DIV+")\">"
	+"									<label class='p-l-40' for='flat-checkbox-1'>Yes</label>"
	+"								</div>"
	+"								<div class='pos-rel' style=' margin-left: 100px; margin-top: -22px;'>"
	//+"									<input   type='radio' class='rdoNumber' name='discount' id='onclickno"+INVOICE_DIV+"' value='No' onclick=\"handleChangeDiscount("+OutWardOrderEntryRegistrationId+","+INVOICE_DIV+","+percentage+")\" >"
	+"									<input   type='radio' class='rdoNumber' name='discount' id='onclickno"+INVOICE_DIV+"' value='No' onchange =\"invoiceDiscountKeyUp("+INVOICE_DIV+")\">"
	+"									<label class='p-l-40' for='flat-checkbox-2'>No</label>"
	+"								</div>"
	+ "									<div id='discount_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"							</div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-12'>"
	+"						<div class='form-group'>"
	+"							<label for='field-6' class='control-label'>Net Amount</label></br>"
	+"							<input type='text' class='form-control'  placeholder='Net Amount' name='netAmount' id='netAmountId"+INVOICE_DIV+"' onchange =\"netAmountIdKeyUp("+INVOICE_DIV+")\" >"//readonly
	+ "							<div id='netAmountId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"				</div>"
	+"</div>"
	+"</div>"
	+"				<div class='modal-footer text-center'>"
	+"					<button type='button' class='btn btn-info'  onclick='validateInvoice("+OutWardOrderEntryRegistrationId+","+percentage+")'>Add </button>"  //addMoreInvoiceHtml("+OutWardOrderEntryRegistrationId+","+percentage+")
	+"				</div>"
	+"<div id='addedInvoiceList'></div>"
	+"				<div class='modal-footer text-center'>"
	+"					<button type='submit' class='btn btn-primary' onclick='return validateInvoiceSubmitButton()'>Submit</button>" // 
	+"					<button type='button' class='btn btn-danger' data-dismiss='modal' onclick='hideSubPopUp(\"modal-responsive1234\")'>Cancel</button>"
	+"				</div>"
	+"</form>"
	+"			</div>"
	+"		</div>"
	+"	</div>"
	+"</div>";
	$('#invoiceEntryPopup').html(html);
	$("#modal-responsive1234").addClass("in");
	$("#modal-responsive1234").attr("aria-hidden","false");
	$("#modal-responsive1234").css("display","block");
	$(".invoiceDate").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	ajaxAddInvoice('saveInvoiceEntry');
}

function ajaxAddInvoice(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					 INVOICE_DIV=0;
					 INVOICE_NO=[];
					 GROSS_AMOUNT=[];
					 TAX_AMOUNT=[];
					 NET_AMOUNT=[];
					 DISCOUNT=[];
					$("#modal-responsive").remove();
					if(invoiceEntryStatus==false)
					 invoiceEntryRegistrationList();
					else
						invoiceEntryAddedRegistrationList();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function validateInvoiceSubmitButton(){
if(!(INVOICE_NO.length>0)){
    alert("Please Add at least One Invoice Entry.....!");
    return false;
}
else
{
    var emptyStat = true;
    for(var i=0;i<INVOICE_NO.length;i++)
    {
        if(!(INVOICE_NO[i]==""))
        {
            emptyStat=false;break;
        }
    }
    if(emptyStat)
    {
        alert("Please Add at least One Invoice Entry.....!");
        return false;
    }
}
showLoader();
}

function validateInvoice(OutWardOrderEntryRegistrationId,percentage)
{
	var status=true;
	if(!invoiceNoId(INVOICE_DIV))
		status=false;
	if(!invoiceDate(INVOICE_DIV))
		status=false;
	/*if(!grossAmountId(INVOICE_DIV))
		status=false;
	if(!validateGrossAmount(INVOICE_DIV))
		status=false;
	if(!taxAmountId(INVOICE_DIV))
		status=false;
	if(!validateTaxAmount(INVOICE_DIV))
		status=false;
	if(!vatAmountId(INVOICE_DIV))
		status=false;
	if(!validateVatAmount(INVOICE_DIV))
		status=false;*/
	if(!netPayableAmt(INVOICE_DIV))
		status=false;
	if(!discountO(INVOICE_DIV))
		status=false;	
	if(status)
		return addMoreInvoiceHtml(OutWardOrderEntryRegistrationId,percentage);
	else
		return false;
}
function handleChangeDiscount(OutWardOrderEntryRegistrationId,invoiceDivNo,percentage)
{
	if($('input[id=onclickno'+INVOICE_DIV+']:checked').val()=='No')
		{
		var netAmount=parseFloat($("#grossAmountId"+invoiceDivNo+"").val())+parseFloat($("#taxAmountId"+invoiceDivNo+"").val())+parseFloat($("#vatAmountId"+invoiceDivNo+"").val());
		$("#netAmountId"+invoiceDivNo+"").val(parseFloat(netAmount).toFixed(4));
		}
	else 
		{
		var totalAmount=parseFloat($("#grossAmountId"+invoiceDivNo+"").val())+parseFloat($("#taxAmountId"+invoiceDivNo+"").val())+parseFloat($("#vatAmountId"+invoiceDivNo+"").val());
		var perAmount=(parseFloat(totalAmount)*percentage)/100;
		var netAmount=parseFloat(totalAmount)-parseFloat(perAmount);
		$("#netAmountId"+invoiceDivNo+"").val(parseFloat(netAmount).toFixed(4));
	}
	invoiceDiscountKeyUp(INVOICE_DIV);
}

function addMoreInvoiceHtml(OutWardOrderEntryRegistrationId,percentage)
{
	var y=1;
	 $('#from_'+INVOICE_DIV).hide();
	 INVOICE_NO[INVOICE_DIV]=$("#invoiceNoId"+INVOICE_DIV+"").val();
	 GROSS_AMOUNT[INVOICE_DIV]=$("#grossAmountId"+INVOICE_DIV+"").val();
	 TAX_AMOUNT[INVOICE_DIV]=$("#taxAmountId"+INVOICE_DIV+"").val();
	 NET_AMOUNT[INVOICE_DIV]=$("#netAmountId"+INVOICE_DIV+"").val();
	 DISCOUNT[INVOICE_DIV]= $('.rdoNumber:checked').val(); //$('input[name=discount'+INVOICE_DIV+']:checked'
	 $('#radioDiscountId').val(DISCOUNT);
	 console.log(INVOICE_NO);
	 console.log(NET_AMOUNT);
	 console.log(DISCOUNT);
	 var AddedInvoiceHtml="				<div class='row'>"
			+"              	<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                  	<table class='table table-striped table-hover'>"
			+"                      	<thead class='no-bd'>"
			+"                          	<tr>"
			+"                              	<th style='text-align: center;'><strong>Sr No.</strong>"
			+"                               	</th>"
			+"                               	<th style='text-align: center;'><strong>Invoice No</strong>"
			+"                               	</th>"
			+"                               	<th style='text-align: center;'><strong>Gross Amount</strong>"
			+"                               	</th>"
			+"                               	<th style='text-align: center;'><strong>Tax Amount</strong>"
			+"                               	</th>"
			+"                               	<th style='text-align: center;'><strong>Net Amount</strong>"
			+"                               	</th>"
			+"                               	<th style='text-align: center;'><strong>Action</strong>"
			+"                               	</th>"
			+"                          	</tr>"
			+"                      	</thead>"
			+"                      	<tbody class='no-bd-y'>";
			
			
			for (var s = 0; s < INVOICE_NO.length; s++) 
			{
			if(INVOICE_NO[s]=="")
				{
				
				}
			else
				{
				AddedInvoiceHtml+="                          	<tr style='text-align: center;' id='InvoiceTableRow"+s+"'>"
					+"                               	<td>"+y+++"</td>"
					+"                               	<td>"+INVOICE_NO[s]+"</td>"
					+"                               	<td>"+GROSS_AMOUNT[s]+"</td>"
					+"                               	<td>"+TAX_AMOUNT[s]+"</td>"
					+"                               	<td>"+NET_AMOUNT[s]+"</td>"
					+"                               	<td><a class='delete btn btn-danger' href='#' onclick='RemoveAddedInvoice("+s+")'><i class='fa fa-times-circle'></i> </a></td>"
					+"                          	</tr>";
				}
			}
			AddedInvoiceHtml+="                      	</tbody>"
			+"                  	</table>"
			+"              	</div>"
			+"              </div>";
			$('#addedInvoiceList').html(AddedInvoiceHtml);
	 
	 
	 // add move invoice html
	var html="<div id='from_"+ ++INVOICE_DIV +"'>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-1' class='control-label'>Invoice No</label></br>"
	+"							<input type='text' class='form-control'  placeholder='Invoice No' name='invoiceNo' id='invoiceNoId"+INVOICE_DIV+"' onchange =\"invoiceNoIdKeyUp("+INVOICE_DIV+")\">"
	+ "							<div id='invoiceNoId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-2' class='control-label'>Date</label>"
	+"							</br>"
	+"							<input class='datepicker form-control invoiceDate' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;' name='invoiceDate' id='invoiceDate"+INVOICE_DIV+"' onchange =\"invoiceDateKeyUp("+INVOICE_DIV+")\">"
	+ "							<div id='invoiceDate_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red' ></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-3' class='control-label'>Gross Amount</label>"
	+"							</br>"
	+"							<input type='text' class='form-control'  placeholder='Gross Amount' name='grossAmount' id='grossAmountId"+INVOICE_DIV+"'  >"//onkeyup =\"grossAmountIdKeyUp("+INVOICE_DIV+")\"
	//+ "							<div id='grossAmountId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-5' class='control-label'>Tax Amount</label></br>"
	+"							<input type='text' class='form-control'  placeholder='Tax Amount' name='taxAmount' id='taxAmountId"+INVOICE_DIV+"'>"//onkeyup =\"taxAmountIdKeyUp("+INVOICE_DIV+")\"
	//+ "							<div id='taxAmountId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-3' class='control-label'>VAT Amount</label>"
	+"							</br>"
	+"							<input type='text' class='form-control'  placeholder='VAT Amount' name='vatAmount'  id='vatAmountId"+INVOICE_DIV+"' >"//onkeyup =\"vatAmountIdKeyUp("+INVOICE_DIV+")\"
	//+ "							<div id='vatAmountId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-4'>"
	+"						<div class='form-group'>"
	+"							<label for='field-6' class='control-label'>Discount</label></br>"
	+"							<div class='col-lg-12'>"
	+"								<div class='pos-rel' style=' margin-top: 16px;'>"
	//+"									<input   type='radio' class='rdoNumber' name='discount' id='onclickyes"+INVOICE_DIV+"' value='Yes' onclick=\"handleChangeDiscount("+OutWardOrderEntryRegistrationId+","+INVOICE_DIV+","+percentage+")\" >" // onkeyup =\"discountKeyUp("+INVOICE_DIV+")\"
	+"									<input   type='radio' class='rdoNumber' name='discount' id='onclickyes"+INVOICE_DIV+"' value='Yes' onchange =\"invoiceDiscountKeyUp("+INVOICE_DIV+")\">"
	+"									<label class='p-l-40' for='flat-checkbox-1'>Yes</label>"
	+"								</div>"
	+"								<div class='pos-rel' style=' margin-left: 100px; margin-top: -22px;'>"
	//+"									<input   type='radio' class='rdoNumber' name='discount' id='onclickno"+INVOICE_DIV+"' value='No' onclick=\"handleChangeDiscount("+OutWardOrderEntryRegistrationId+","+INVOICE_DIV+","+percentage+")\" >"
	+"									<input   type='radio' class='rdoNumber' name='discount' id='onclickno"+INVOICE_DIV+"' value='No' onchange =\"invoiceDiscountKeyUp("+INVOICE_DIV+")\">"
	+"									<label class='p-l-40' for='flat-checkbox-2'>No</label>"
	+"								</div>"
	+ "									<div id='discount_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"							</div>"
	+"						</div>"
	+"					</div>"
	+"					<div class='col-md-12'>"
	+"						<div class='form-group'>"
	+"							<label for='field-6' class='control-label'>Net Amount</label></br>"
	+"							<input type='text' class='form-control'  placeholder='Net Amount' name='netAmount' id='netAmountId"+INVOICE_DIV+"' onchange =\"netAmountIdKeyUp("+INVOICE_DIV+")\" >"//readonly
	+ "							<div id='netAmountId_Error_Msg"+INVOICE_DIV+"' class='validationError' style='color:red'></div>"
	+"						</div>"
	+"					</div>"
	+"				</div>"
	+"</div>";
	$('#moreInvoiceHtmlDiv').append(html);
	$(".invoiceDate").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
}

function RemoveAddedInvoice(divId)
{
	$('#InvoiceTableRow'+divId).remove();
	$('#from_'+divId).remove();
	INVOICE_NO[divId] = "";
	GROSS_AMOUNT[divId] = "";
	TAX_AMOUNT[divId] = "";
	GROSS_AMOUNT[divId] = "";
	NET_AMOUNT[divId] = "";
	DISCOUNT[divId]="-";
	$('#radioDiscountId').val(DISCOUNT);
}

function invoiceEntryAddedRegistrationList()
{
	$('#loader').show();
	var i=1;
	//$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardOrderEntryRegistrationOfProductStatusOneAndInvoiceStatusIsOne', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_7'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>View Invoice Entry Registration</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForInvoiceEntryRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+"																	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
		+ "                                                     		  	<option value='Date'>Search By Order Date</option>"
	//	+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+"																	<option value='OrderNo'> Order Number</option>"
		+ "                                                    			   	<option value='OrderMode'> Order Mode</option>"
		+ "                                                    			   	<option value='InvoiceNumber'> Invoice Number</option>"
		+ "                                                    			   	<option value='InvoiceDate'> Invoice Date</option>"
		+ "                                                    			   	<option value='GrossAmount'> Gross Amount</option>"
		+ "                                                    			   	<option value='NetAmount'> Net Amount</option>"
		+"																</select>"	
		//+"                                                                    <input type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardInvoiceEntryAddedRegistrationList()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd' id='a'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Oranization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Date</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order Mode</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y' id='selectedRecord'>"
		+"                                                                </tbody>"
		+"                                                                </table>"
		+"                                                        </div>"
		+"                                               <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	//showInvoiceEntryAddedRegistrationList(data);
	commonOutwardInvoiceEntryAddedRegistrationListController("","","","",1);
	subTabsForOutWardInvoiceEntryRegistration(false);
	//}, 'json');
}

function showInvoiceEntryAddedRegistrationList(data,from,url)
{  
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=1;
	var html=""
		var pageCount;
		/*+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Oranization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Date</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order Mode</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";*/
	$(data).each(function(index, element) {
		pageCount=element.paginationCount;
		if((data.length-1)<i)
			return false;
			html+="<tr style='text-align: center;'>"
				+" <td>"+SR_NO+"</td>"
				+" <td id='ORDER_ID_"+i+"'>"+element.ORDER_ID+"</td>"
				+" <td id='ORDER_NO_"+i+"'>"+element.ORDER_NO+"</td>"
				+" <td id='ORG_"+i+"'>"+element.ORG+"</td>"
				+" <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
				+" <td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
				+" <td id='DATE_"+i+"'>"+element.DATE+"</td>"
				+" <td >"+element.ORDER_MODE+"</td>"
				+" <td><a class='edit btn btn-blue' href='#' onclick='viewOrderEntryRegistration("+element.OutWardOrderEntryRegistrationId+")'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' href='#' onclick='editOrderEntryRegistration("+element.OutWardOrderEntryRegistrationId+")'><i class='fa fa-pencil-square-o'></i></a>  <td>"//<a class='delete btn btn-danger' href='#' onclick='deleteOrderEntryRegistration("+element.OutWardOrderEntryRegistrationId+")'><i class='fa fa-times-circle'></i> </a>
				+" </tr>";
			i++; SR_NO++;
		});
		
		/*if(!data.length>0){
			html+= "<h1> <strong>No such record found</strong></h1>";   // Code If there are no matching records
		}*/
	$('#selectedRecord').append(html);
	$('#loader').hide();
	paginationViewMore(from,pageCount,url);
}

function changeFunctionOutwardInvoiceEntry()
{
	var selectedValue=$('#searchOption').val();
	var html="";
		if(selectedValue=="GrossAmount" || selectedValue=="NetAmount" || selectedValue=="ChequeAmount" )
		{
    		html+="<input id='inputFromData3' type='text' class='form-control' placeholder='From Amount' style='width: 49%'; margin-right: 1%;float: left;' onkeyup =\"grossAmountKeyUp()\">"
    			+" <input id='inputTOData3' type='text' class='form-control' placeholder='To Amount' style='width: 49%'; onkeyup =\"grossAmountKeyUp()\">"
	    		+"<div id='inputFromData3_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
	    		+"<div id='inputTOData3_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>";
		}
    	else if(selectedValue=="Date")
    	{
    		html+="<input id='inputFromDate' type='text' class='dateClassCommon form-control' placeholder='From Date' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTODate' type='text' class='dateClassCommon form-control' placeholder='To Date' style='width: 49%;'>";
    	}
		
    	else if(selectedValue=="InvoiceDate")
    	{
    		html+="<input id='inputFromDate' type='text' class='dateClassCommon form-control' placeholder='From Date' style='width: 49%; margin-right: 1%;float: left' onclick =\"invoiceDateKeyUp()\">"// <div id='fromDate' class='controls'>

    			+" <input id='inputTODate' type='text' class='dateClassCommon form-control' placeholder='To Date' style='width: 49%' onclick =\"invoiceDateKeyUp()\">"
    		+"<div id='inputFromDate_Error_Msg' class='validationError' style='width: 49%; margin-right: 1%; color : red;float: left'></div>"
    		+"<div id='inputTODate_Error_Msg' class='validationError'  style='width: 49%; margin-right: 1%;color : red;float: right'></div>";
    	}
    	else if(selectedValue=="NoOfCases")
		{
    		html+="<input id='inputFromData3' type='text' class='form-control' placeholder='From Case' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTOData3' type='text' class='form-control' placeholder='To Case' style='width: 49%;'>";
		}
    	else
    	{
    		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control' onkeyup =\"organizationKeyUp()\" >"
    			 + "	<div id='searchText_Error_Msg' class='validationError' style='color:red'></div>";
    	} 	
		$("#searchControl").html(html);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
}

function searchOutwardInvoiceEntryAddedRegistrationList()
{
	$('#selectedRecord').html('');
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if(selectedOption=="GrossAmount" || selectedOption=="NetAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
				if(fromSpecialData>toSpecialData){
					alert("'From Value' should be less than 'To Value'.....");
					return false;
				}
		 }	
//		 if($('#inputTOData3').val()>=$('#inputFromData3').val())
//			{
//			 fromSpecialData=$('#inputFromData3').val();
//				toSpecialData=$('#inputTOData3').val();
//			}
//			else
//			{
//				alert('Seleted TO Amount should be greater than from Amount');
//				return false;
//			}
	}
	else if (selectedOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
	}
	
	else if (selectedOption=="InvoiceDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
	}
	
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonOutwardInvoiceEntryAddedRegistrationListController(selectedOption,searchText,fromSpecialData,toSpecialData,1);
}

function commonOutwardInvoiceEntryAddedRegistrationListController(selectedOption,searchText,fromSpecialData,toSpecialData,from)
{
	$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardInvoiceEntryAddedRegistrationList',
		{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData,from:from
	}, function(data) {
		var url = "commonOutwardInvoiceEntryAddedRegistrationListController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
		showInvoiceEntryAddedRegistrationList(data,from,url); // to call the function which shows the searched item

		
}, 'json');
}

//Start Dispatch Entry Registration coding
function subTabsForOutWardDispatchEntryRegistration(flag)
{
	 var html = ""
	  +"                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
	  +"                                            <li id='outWardDispatchEntryRegistrationTab1' class=''><a href='#tab2_6' data-toggle='tab' onclick='dispatchEntryRegistrationList()'><h5><strong>Add Cases</strong></h5></a></li>"
	  +"                                            <li id='outWardDispatchEntryRegistrationTab2' class=''><a href='#tab2_7' data-toggle='tab' onclick='viewDispatchEntryRegistrationCases()'><h5><strong>View Cases</strong></h5></a></li>"
	  +"                                            <li id='outWardDispatchEntryRegistrationTab3' class=''><a href='#tab2_7' data-toggle='tab' onclick='viewPrintStickerList()'><h5><strong>Print Sticker List</strong></h5></a></li>"
	  +"                                        </ul>";
	 $('#subTabsForOutWardDispatchEntryRegistration').html(html);
	 		if(flag==1)
	 			{
	 			$('#outWardDispatchEntryRegistrationTab1').addClass("active");
	 			}
	 		else if(flag==2)
	 			{
				$('#outWardDispatchEntryRegistrationTab2').addClass("active");
	 			}
	 		else
	 			{
	 			$('#outWardDispatchEntryRegistrationTab3').addClass("active");
	 			}
	 		
}

///////////////// Vijay
function dispatchEntryRegistrationList()
{
	$('#loader').show();
	var i=1;
	var html=""
		+"                               	 <div class='tab-pane fade active in' id='tab2_3'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Add Cases Dispatch Entry Registration</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOutWardDispatchEntryRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                    			   	<option value='InvoiceNumber'> Invoice Number</option>"
		+"																	<option value='OrderNo'> Order Number</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+"																	<option value='Organization'>Organization Name</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
	//	+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                    			   	<option value='NetAmount'> Net Amount</option>"
		+ "                                                     		  	<option value='Date'>Search By Order Date</option>"
		+ "                                                    			   	<option value='InvoiceDate'> Invoice Date</option>"
		//+ "                                                    			   	<option value='DaysCount'> Days Count</option>"
		+"																</select>"	
		//+"                                                                    <input type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardDispatchEntryRegistrationList()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"														 <div class='row' style='float: right; margin-right: 10px;'>"
		+"                                                        <strong>Selected Count :- </strong><input type='text' style='width:46px' readonly id='AddCasesCart'/>"
		+"                                                        <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive7' onclick='clearAddCases()'>Clear Count</button>"
		+"                                                        <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive21' onclick='AddCases()'>Add Case</button>"
		+"                                                        </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Select</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Invoice No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order Id</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Net Amount</strong>"
		+"                                                                        </th>"
//		+"                                                                        <th style='text-align: center;'><strong>Transporter</strong>"
//		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order Date</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Invoice Date</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Days Count</strong>"
		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y' id='selectedRecord'>"
		+"                                                                </tbody>"
		+"                                                                <tbody class='no-bd-y' id='nonSelectedRecord'>"
		+"                                                                </tbody>"
		+"                                                            </table>"
		+"                                                        </div>"
		+ "                                                       <div id='pagination' class='pagination'></div>"
		+"                                                	</div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	commonDispatchEntryRegistrationController("","","","",1);
	subTabsForOutWardDispatchEntryRegistration(1);
}
function clearAddCases()
{
	outWardAddCasesArray=[];
	selectedAddCasesArray=[];
	dispatchEntryRegistrationList();
}

var outWardAddCasesArray=[];
var selectedAddCasesArray=[];
function showdispatchEntryRegistrationList(data,from,url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=1;
	var html=""
		var pageCount;
		$(data).each(function(index, element) 
				{
			pageCount=element.paginationCount;
			var id=element.OUTWARD_INVOICE_ENTRY_REGISTRATION_ID;
			if ($.inArray(id, outWardAddCasesArray) < 0) 
			{
			html+="<tr style='text-align: center;' id='search_"+element.OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"'>"
				+"<td>"
				+"<div class='div_checkbox'>"
				+"<input type='checkbox' id='checkbox_"+element.OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"' onclick='AddCasesCheckboxEvent("+element.OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+")' style='opacity: 1 !important;' name='OrderInvoiceEnteryRegistrationId' value='"+element.OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"'>"
				+"<input type='hidden' id='org_"+element.OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"' value='"+element.ORG_ID+"'>"
				+"<input type='hidden' id='stockist_"+element.OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"' value='"+element.STOCKIST_ID+"'>"
				+"<input type='hidden' id='company_"+element.OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"' value='"+element.COMPANY_ID+"'>"
				+"</div>"
				+"</td>"
				+"<td>"+SR_NO+"</td>"
				+"<td >"+element.INVOICE_NO+"</td>"
				+"<td >"+element.ORDER_NO+"</td>"
				+"<td >"+element.ORDER_ID+"</td>"
				+"<td >"+element.ORGANIZATION+"</td>"
				+"<td >"+element.COMPANY+"</td>"
				+"<td >"+element.STOCKIST+"</td>"
				+"<td >"+element.NET_AMOUNT+"</td>"
				+"<td >"+element.ORDER_DATE+"</td>"
				+"<td >"+element.INVOICE_DATE+"</td>"
				+"<td >"+element.DAYS_COUNT+"</td>"
				+"</tr>";
				i++,SR_NO++;
			}
			
			$("#selectedRecord").html('')
	          for(var i = 0; i < selectedAddCasesArray.length; i++)
	          {
	        	  $("#selectedRecord").append(selectedAddCasesArray[i]);
	        	  $("#checkbox_"+outWardAddCasesArray[i]+"").prop('checked', true);
	          }
			
		});
			
			$('#nonSelectedRecord').append(html);
			$('#AddCasesCart').val(outWardAddCasesArray.length);
			$('#loader').hide();
			paginationViewMore(from,pageCount,url);
}


function AddCasesCheckboxEvent(OUTWARD_INVOICE_ENTRY_REGISTRATION_ID)
{
	if(!!~outWardAddCasesArray.indexOf(OUTWARD_INVOICE_ENTRY_REGISTRATION_ID))
	{
	console.log("presnent");
	console.log(outWardAddCasesArray);
	var i = outWardAddCasesArray.indexOf(OUTWARD_INVOICE_ENTRY_REGISTRATION_ID);
	var html="<tr id='selected_"+OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"' style='text-align: center;'>"
	var test = $("#selected_"+OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"").html();
	html+=test+"</tr>";
	var j = selectedAddCasesArray.indexOf(html);
		if(i != -1 && j != -1) 
		{
			outWardAddCasesArray.splice(i, 1);
			selectedAddCasesArray.splice(j, 1);
			$("#selected_"+OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"").remove();
			console.log(outWardAddCasesArray);
			console.log(selectedAddCasesArray);
		}
	}
else
	{
	console.log("not presnent");
	var html="<tr id='selected_"+OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"' style='text-align: center;'>"
	var test = $("#search_"+OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"").html();
	html+=test+"</tr>";
	$('#selectedRecord').append(html);
	$("#checkbox_"+OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"").prop('checked', true);
	$("#search_"+OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"").remove();
	
	selectedAddCasesArray.push(html);
	outWardAddCasesArray.push(OUTWARD_INVOICE_ENTRY_REGISTRATION_ID);
	console.log(selectedAddCasesArray);
	console.log(outWardAddCasesArray);
	}
 $('#AddCasesCart').val(outWardAddCasesArray.length);
}



Array.prototype.allValuesSame = function() {

    for(var i = 1; i < this.length; i++)
    {
        if(this[i] !== this[0])
            return false;
    }
    return true;
}

function AddCases()
{
	var i=1;
	var checkboxesValues = document.getElementsByName('OrderInvoiceEnteryRegistrationId');
	var vals = "";
	var orgValues="";
	var stockistValue="";
	var companyValue="";
	for (var i=0, n=checkboxesValues.length;i<n;i++) {
	  if (checkboxesValues[i].checked) 
	  {
	  orgValues +=","+$("#org_"+checkboxesValues[i].value+"").val();
	  stockistValue +=","+$("#stockist_"+checkboxesValues[i].value+"").val();
	  companyValue +=","+$("#company_"+checkboxesValues[i].value+"").val();
	  vals += ","+checkboxesValues[i].value;
	  }
	}
	if (vals) vals = vals.substring(1);
	if(orgValues) orgValues=orgValues.substring(1);
	if(stockistValue) stockistValue=stockistValue.substring(1);
	if(companyValue) companyValue=companyValue.substring(1);
//	alert(vals);
	if(vals=="")
	{
	alert("Please select at least one check box");
	}
else
	{
	var temp = new Array();
	var stockistArray=new Array();
	var companyArray=new Array();
	// this will return an array with strings "1", "2", etc.
	temp = orgValues.split(",");
	stockistArray=stockistValue.split(",");
	companyArray=companyValue.split(",");
	if(temp.allValuesSame())
		{
			if(stockistArray.allValuesSame())
				{
					if(companyArray.allValuesSame())
						{
						var html="                                                                            <div class='modal fade' id='modal-responsive' aria-hidden='true'>"
						+"                                                                                <div class='modal-dialog modal-lg'>"
						+"<form id='addCases' action='"+contextApplicationPath+"/OutWardGoodsController/saveCases' method='post'>"
						+"<input type='hidden' name='stockistId' value='"+stockistArray[0]+"'>"
						+"<input type='hidden' name='companyId' value='"+companyArray[0]+"'>"
						+"                                                                                    <div class='modal-content'>"
						+"                                                                                        <div class='modal-header'>"
						+"                                                                                            <button type='button' class='close' data-dismiss='modal' aria-hidden='true' onclick='hidePopUp()'>X</button>"
						+"                                                                                            <h4 class='modal-title' id='myModalLabel'><strong>Add Case</strong></h4>"
						+"                                                                                        </div>"
						+"                                                                                        <div class='modal-body' style=' text-align: left;'>"
						+"                                                                                            <div class='row'>"
						+"                                                                                                <div class='col-md-12'>"
						+"                                                                                                    <div class='panel panel-default'>"
						+"                                                                                                        <div class='panel-body'>"
						+"                                                                                                            <div class='row'>    "                                
						+"                                                                                                                <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
						+"                                                                                                                    <table class='table table-striped table-hover'>"
						+"                                                                                                                        <thead class='no-bd'>"
						+"                                                                    <tr>"
						+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Invoice No</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Order No</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Order Id</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Organization</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Net Amount</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Order Date</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Invoice Date</strong>"
						+"                                                                        </th>"
						+"                                                                        <th style='text-align: center;'><strong>Days Count</strong>"
						+"                                                                        </th>"
						+"                                                                    </tr>"
						+"                                                                                                                        </thead>"
						+"                                                                                                                        <tbody class='no-bd-y'>";
					$.post(contextApplicationPath+'/OutWardGoodsController/loadSelectedOutWardOrderInvoiceEnteryRegistration',{selectedOutWardOrderInvoiceEnteryRegistrationId : vals}, function(data) {
						$(data).each(function(index, element) 
						{
							html+="<tr style='text-align: center;'>"
								+"<input type='hidden' name='OutWardOrderInvoiceEnteryRegistrationId' value='"+element.OUTWARD_INVOICE_ENTRY_REGISTRATION_ID+"'>"
								+"<input type='hidden' name='organizationId' value='"+element.ORG_ID+"'>"
								+"<td>"+ i++ +"</td>"
								+"<td>"+element.INVOICE_NO+"</td>"
								+"<td>"+element.ORDER_NO+"</td>"
								+"<td>"+element.ORDER_ID+"</td>"
								+"<td>"+element.ORGANIZATION+"</td>"
								+"<td>"+element.STOCKIST+"</td>"
								+"<td>"+element.NET_AMOUNT+"</td>"
								+"<td>"+element.COMPANY+"</td>"
								+"<td>"+element.ORDER_DATE+"</td>"
								+"<td>"+element.INVOICE_DATE+"</td>"
								+"<td>"+element.DAYS_COUNT+"</td>"
								+"</tr>";	
						});
						html+="                                                                                                                        </tbody>"
							+"                                                                                                                    </table>"
							+"                                                                                                                </div>"
							+"                                                                                                            </div>"
							+"                                                                                                        </div>"
							+"                                                                                                    </div>"
							+"                                                                                                </div> "
							+"                                                                                                <div class='col-md-12'>"
							+"                                                                                                    <div class='form-group'>"
							+"                                                                                                        <label for='field-6' class='control-label'>No of Cases</label></br>"
							+"                                                                                                        <input type='text' class='form-control' id='field-7' placeholder='No of Cases' name='noOfCases' required parsley-type='number'>"
							+"                                                                                                    </div>"
							+"                                                                                                </div>"
							+"                                                                                            </div>"
							+"                                                                                        </div>"
							+"                                                                                        <div class='modal-footer text-center'>"
							+"                                                                                            <button type='submit' class='btn btn-primary' onclick='validateFormDetails(\"#addCases\")'>Dispatch</button>"
							+"                                                                                            <button type='button' class='btn btn-danger' data-dismiss='modal' onclick='hidePopUp()'>Cancel</button>"
							+"                                                                                        </div>"
							+"                                                                                    </div>"
							+"</from>"
							+"                                                                                </div>"
							+"                                                                            </div>";
						$('#popup').html(html);
						$("#modal-responsive").addClass("in");
						$("#modal-responsive").attr("aria-hidden","false");
						$("#modal-responsive").css("display","block");
						ajaxAddCases('addCases');
					}, 'json');
						}
					else
						{
						alert("Please select same company");
						}
				}
			else
				{
				alert("Please select same stockist");
				}
		}
	else 
		{
		alert("Please select same orgnization invoice");
		}
	}
}

function ajaxAddCases(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					outWardAddCasesArray=[];
					selectedAddCasesArray=[];
					dispatchEntryRegistrationList();
					$("#modal-responsive").remove();
					
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function searchOutwardDispatchEntryRegistrationList()
{
	$('#nonSelectedRecord').html('');
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if(selectedOption=="GrossAmount" || selectedOption=="NetAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
		 }
		 else
		 {
			fromSpecialData=$('#inputFromData3').val();
			toSpecialData=$('#inputTOData3').val();
			if(fromSpecialData>toSpecialData){
				alert("'From Value' should be less than 'To Value'.....");
				return false;
			}
		 }	
	}
	else if (selectedOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
	}
	
	else if (selectedOption=="InvoiceDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonDispatchEntryRegistrationController(selectedOption,searchText,fromSpecialData,toSpecialData ,1);
}

function commonDispatchEntryRegistrationController(selectedOption,searchText,fromSpecialData,toSpecialData, from)
{
	$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardDispatchEntryRegistrationList',
			{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData,from : from
		}, function(data) {
			var url = "commonDispatchEntryRegistrationController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showdispatchEntryRegistrationList(data,from,url); // to call the function which shows the searched item
	
		$('#loader').hide();
	 }, 'json');
}

////////////////////////////////////  search code for outward order -dispatch entry registration /////////////////////
function viewDispatchEntryRegistrationCases()
{
	$('#loader').show();
	var i=1;
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_3'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>View Cases Dispatch Entry Registration</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOutWardDispatchEntryRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+"																	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
	//	+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                     		  	<option value='NoOfCases'>No. of Cases</option>"
		+ "                                                    			   	<option value='InvoiceNumber'> Invoice Number</option>"
		+"																	<option value='OrderNo'> Order Number</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+ "                                                    			   	<option value='NetAmount'> Net Amount</option>"
		//+ "                                                     		  	<option value='Date'>Search By Order Date</option>"
		+ "                                                    			   	<option value='InvoiceDate'> Invoice Date</option>"
		//+ "                                                    			   	<option value='DaysCount'> Days Count</option>"
		+"																</select>"	
		//+"                                                                    <input type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardViewDispatchEntryRegistrationList()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                        <div class='row' style='float: right; margin-right: 24px;'>"
		+"                                                        <strong>Selected Count :- </strong><input type='text' style='width:46px' readonly id='viewCasesCart'/>"
		+"                                                        <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive7' onclick=\"clearViewCases('fromViewCases')\" >Clear Count</button>"
		+"                                                          <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive21' onclick='printSticker()'>Print Sticker</button>"
		+"                                                        </div>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Select</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>No of Cases</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
//		+"                                                                        <th style='text-align: center;'><strong>Print</strong>"
//		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                 <tbody class='no-bd-y' id='selectedRecord'>"
		+"                                                                 </tbody>"
		+"                                                                 <tbody class='no-bd-y' id='nonSelectedRecord'>"
		+"                                                                 </tbody>"
		+"                                                            </table>"
		+"                                                        </div>"
		+"                                                     <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	commonOutwardViewDispatchEntryRegistrationListController("", "", "", "", 1);
	subTabsForOutWardDispatchEntryRegistration(2);
}

function clearViewCases(option)
{
	 outWardViewCasesArray=[];
	 selectedViewCasesArray=[];
	 if(option=='fromViewCases')
		 {
		 viewDispatchEntryRegistrationCases();
		 }
	 else
		 {
		 viewPrintStickerList();
		 }
	 
}

var outWardViewCasesArray=[];
var selectedViewCasesArray=[];

function ShowDispatchEntryRegistrationCases(data,from,url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=1;
	var html="";
	var pageCount;
		$(data).each(function(index, element) 
				{
			pageCount=element.paginationCount;
			var id=element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID;
			if ($.inArray(id, outWardViewCasesArray) < 0) 
			{
			html+="<tr style='text-align: center;' id='dispatchEntry_"+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"'>"
				+"<td><input type='checkbox' id='checkbox_"+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"' onclick='ViewCasesCheckboxEvent("+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+")' style='opacity: 1 !important;' name='OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID' value='"+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"'></td>"
				+"<input type='hidden' id='org_"+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"' value='"+element.ORG_ID+"'>"
				+"<td>"+SR_NO+"</td>"
				+"<td id='ORG_"+i+"'>"+element.ORG+"</td>"
				+"<td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
				+"<td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
				+"<td id='NO_OF_CASES_"+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"'>"+element.NO_OF_CASES+"</td>"
				+"<td><a class='edit btn btn-blue' href='#' onclick='viewCases("+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+")'><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark' href='#' onclick=\"editCases('"+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"','"+element.NO_OF_CASES+"')\"><i class='fa fa-pencil-square-o'></i></a>  ";
				if(element.TRIP_STATUS==0)
					{
					html+="<a class='delete btn btn-danger' href='#' onclick='deleteCases("+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+")'><i class='fa fa-times-circle'></i> </a>";
					}
				html+="</td>"
//				+"<td>"
//				+"<a class='btn btn-info' href='#' onClick='printSticker("+element.OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+")'>Print Sticker</a></td>"
				+"</tr>";
				i++,SR_NO++;
			}
			
			$("#selectedRecord").html('')
	          for(var j = 0; j < selectedViewCasesArray.length; j++)
	          {
	        	  $("#selectedRecord").append(selectedViewCasesArray[j]);
	        	  $("#checkbox_"+outWardViewCasesArray[j]+"").prop('checked', true);
	          }
			
			
		});
		$('#nonSelectedRecord').append(html);
		$('#viewCasesCart').val(outWardViewCasesArray.length);
		$('#loader').hide();
		paginationViewMore(from,pageCount,url);
}

function ViewCasesCheckboxEvent(OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID)
{
	if(!!~outWardViewCasesArray.indexOf(OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID))
	{
	console.log("presnent");
	console.log(outWardAddCasesArray);
	var i = outWardViewCasesArray.indexOf(OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID);
	var html="<tr id='selected_"+OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"' style='text-align: center;'>"
	var test = $("#selected_"+OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"").html();
	html+=test+"</tr>";
	var j = selectedViewCasesArray.indexOf(html);
		if(i != -1 && j != -1) 
		{
			outWardViewCasesArray.splice(i, 1);
			selectedViewCasesArray.splice(j, 1);
			$("#selected_"+OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"").remove();
			console.log(outWardViewCasesArray);
			console.log(selectedViewCasesArray);
		}
	}
else
	{
	console.log("not presnent");
	var html="<tr id='selected_"+OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"' style='text-align: center;'>"
	var test = $("#dispatchEntry_"+OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"").html();
	html+=test+"</tr>";
	$('#selectedRecord').append(html);
	$("#checkbox_"+OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"").prop('checked', true);
	$("#dispatchEntry_"+OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID+"").remove();
	
	selectedViewCasesArray.push(html);
	outWardViewCasesArray.push(OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID);
	console.log(selectedViewCasesArray);
	console.log(outWardViewCasesArray);
	}
 $('#viewCasesCart').val(outWardViewCasesArray.length);
}



// show list Print sticker 

function viewPrintStickerList()
{
	$('#loader').show();
	var i=1;
	//$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardDispatchEnteryRegistrationOfStatusOne', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_3'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>View Cases Dispatch Entry Registration</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOutWardDispatchEntryRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+"																	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
		//+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                     		  	<option value='NoOfCases'>No. of Cases</option>"
		+ "                                                    			   	<option value='InvoiceNumber'> Invoice Number</option>"
		+"																	<option value='OrderNo'> Order Number</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+ "                                                    			   	<option value='NetAmount'> Net Amount</option>"
		//+ "                                                     		  	<option value='Date'>Search By Order Date</option>"
		+ "                                                    			   	<option value='InvoiceDate'> Invoice Date</option>"
		//+ "                                                    			   	<option value='DaysCount'> Days Count</option>"
		+"																</select>"	
		//+"                                                                    <input type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardViewDispatchEntryPrintStickerList()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                        <div class='row' style='float: right; margin-right: 24px;'>"
		+"                                                        <strong>Selected Count :- </strong><input type='text' style='width:46px' readonly id='viewCasesCart'/>"
		+"                                                        <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive7' onclick=\"clearViewCases('fromPrintSticketList')\">Clear Count</button>"
		+"                                                            <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive21' onclick='printSticker()'>Print Sticker</button>"
		+"                                                        </div>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Select</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>No of Cases</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                        </th>"
//		+"                                                                        <th style='text-align: center;'><strong>Print</strong>"
//		+"                                                                        </th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                 <tbody class='no-bd-y' id='selectedRecord'>"
		+"                                                                 </tbody>"
		+"                                                                 <tbody class='no-bd-y' id='nonSelectedRecord'>"
		+"                                                                 </tbody>"
		+"                                                            </table>"
		+"                                                        </div>"
		+"                                                     <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	commonOutwardViewDispatchEntryPrintStickerListController("", "", "", "", 1);
	//ShowDispatchEntryRegistrationCases(data);
	subTabsForOutWardDispatchEntryRegistration(3);
	//}, 'json');
}


function commonOutwardViewDispatchEntryPrintStickerListController(selectedOption, searchText, fromSpecialData, toSpecialData, from)
{
$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardViewDispatchEntryPrintStickerList',
	{
	selectedValue : selectedOption, searchText : searchText, fromSpecialData : fromSpecialData, toSpecialData : toSpecialData, from:from
}, function(data) {
	var url = "commonOutwardViewDispatchEntryPrintStickerListController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
	ShowDispatchEntryRegistrationCases(data,from,url) // to call the function which shows the searched item
	//$('#searchResult').html(html);
}, 'json');
}


function searchOutwardViewDispatchEntryPrintStickerList()
{
	$('#nonSelectedRecord').html('');
	var selectedOption = $('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	if(selectedOption=="NetAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if(selectedOption=="NoOfCases")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (selectedOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Selected TO Date should be greater than from Date');
			return false;
		}
	}
	else if (selectedOption=="InvoiceDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Selected TO Date should be greater than from Date');
			return false;
		}
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonOutwardViewDispatchEntryPrintStickerListController(selectedOption, searchText, fromSpecialData, toSpecialData, 1);
}










function printSticker()
{
	var vals = "";
	var checkboxesValues = document.getElementsByName('OUTWARD_DISPATCH_ENTERY_REGISTRATION_ID');
	var orgValues="";
	for (var i=0, n=checkboxesValues.length;i<n;i++) 
	{
		  if (checkboxesValues[i].checked) 
		  {
		  orgValues +=","+$("#org_"+checkboxesValues[i].value+"").val();
		  vals += ","+checkboxesValues[i].value;
		  }
	}
	if (vals) vals = vals.substring(1);
	if(orgValues) orgValues=orgValues.substring(1);
		if(vals=="")
		{
		alert("Please select at least one check box");
		}
		else
		{
			var temp = new Array();
			temp = orgValues.split(",");
			if(temp.allValuesSame())
				{
				window.location.href = contextApplicationPath+'/OutWardGoodsController/printStickerDetailsReport?outWardDispatchEnteryRegistrationId='+vals+''
				}
			else
				{
				alert("Please select same orgnization");
				}
		}
}


function searchOutwardViewDispatchEntryRegistrationList()
{
	$('#nonSelectedRecord').html('');
	var selectedOption = $('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	if(selectedOption=="NetAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if(selectedOption=="NoOfCases")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (selectedOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Selected TO Date should be greater than from Date');
			return false;
		}
	}
	else if (selectedOption=="InvoiceDate")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Selected TO Date should be greater than from Date');
			return false;
		}
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonOutwardViewDispatchEntryRegistrationListController(selectedOption, searchText, fromSpecialData, toSpecialData, 1);
}	
	
function commonOutwardViewDispatchEntryRegistrationListController(selectedOption, searchText, fromSpecialData, toSpecialData, from)
	{
	$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardViewDispatchEntryRegistrationList',
		{
		selectedValue : selectedOption, searchText : searchText, fromSpecialData : fromSpecialData, toSpecialData : toSpecialData, from:from
	}, function(data) {
		var url = "commonOutwardViewDispatchEntryRegistrationListController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
		ShowDispatchEntryRegistrationCases(data,from,url) // to call the function which shows the searched item
		//$('#searchResult').html(html);
}, 'json');
}

function viewCases(OutWardDispatchEnteryRegistrationId)
{
	 viewDispatchCasesPopUp(OutWardDispatchEnteryRegistrationId);
}

function editCases(OutWardDispatchEnteryRegistrationId,noOfCases)
{
	editCasesFunction(OutWardDispatchEnteryRegistrationId,noOfCases);
}

function deleteCases(OutWardDispatchEnteryRegistrationId)
{
	var r = confirm("Do you want to Delete Cases...!");
    if (r == true) 
    {
		$.post(contextApplicationPath+'/OutWardGoodsController/deleteCases',{OutWardDispatchEnteryRegistrationId : OutWardDispatchEnteryRegistrationId} ,function(data) {
			$(data).each(function(index, element) {
				$('#dispatchEntry_'+element.OutWardDispatchEnteryRegistrationId+'').remove();
				alert(element.MSG);
			}); 
		}, 'json');
    }
}

//Start Trip coding 
function subTabsForOutWardTrip(flag)
{
	 var html = ""
	  +"                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
	  +"                                            <li id='outWardTripTab1' class=''><a href='#tab2_6' data-toggle='tab' onclick='tripList()'><h5><strong>Add Trip</strong></h5></a></li>"
	  +"                                            <li id='outWardTripTab2' class=''><a href='#tab2_7' data-toggle='tab' onclick='viewTripEntry()'><h5><strong>View Trip</strong></h5></a></li>"
	  +"                                        </ul>";
	 $('#subTabsForOutWardTrip').html(html);
	 		if(flag)
	 			$('#outWardTripTab1').addClass("active");
	 		else
				$('#outWardTripTab2').addClass("active");
}

function tripList()
{
	
	$('#loader').show();
	var i=1;
	
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_4'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Trip</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOutWardTrip'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardTrip();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
	//	+"																	<option value=''>Select Option</option>"
		+"																	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
	//	+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                     		  	<option value='Transporter'>Transporter</option>"
		+ "                                                     		  	<option value='InvoiceNo'>Invoice Number</option>"
		+"																	<option value='NoOfCases'> No Of Cases</option>"
		+"																</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardAddTrip()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row' style='float: right; margin-right: 10px;'>"
		+"                                                        <strong>Selected Count :- </strong><input type='text' style='width:46px' readonly id='addTripCart'/>"
		+"                                                        <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive7' onclick='clearAddTrip()'>Clear Count</button>"
		+"                                                        <button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive7' onclick='addTripEntry()'>Trip Entry</button>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                                    <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+"                                                                        <table class='table table-striped table-hover'>"
			+"                                                                            <thead class='no-bd' id='sudhakar'>"
			+"                                                                                <tr>"
			+"                                                                                    <th style='text-align: center;'><strong>Select</strong>"
			+"                                                                                    </th>"
			+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
			+"                                                                                    </th>"
			+"                                                                                    <th style='text-align: center;'><strong>Organization</strong>"
			+"                                                                                    </th>"
			+"                                                                                    <th style='text-align: center;'><strong>Compnay</strong>"
			+"                                                                                    </th>"
			+"                                                                                    <th style='text-align: center;'><strong>Stockist</strong>"
			+"                                                                                    </th>"
			+"                                                                                    <th style='text-align: center;'><strong>Transporter</strong>"
			+"                                                                                    </th>"
			+"                                                                                    <th style='text-align: center;'><strong>Invoice No</strong>"
			+"                                                                                    </th>"
			+"                                                                                    <th style='text-align: center;'><strong>No of cases</strong>"
			+"                                                                                    </th>"
			+"                                                                                    <th style='text-align: center;'><strong>Submit Date</strong>"
			+"                                                                                    </th>"
			+"                                                                                </tr>"
			+"                                                                            </thead>"
			+"                                                                            <tbody class='no-bd-y' id='selectedRecord'>"
			+"                                                                            </tbody>"
			+"                                                                            <tbody class='no-bd-y' id='nonSelectedRecord'>"
			+"                                                                            </tbody>"
			+"                                                                        </table>"
		+"                                                        </div>"
		+"                                                               <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	commonsearchOutwardAddTripController("","","","",1);
	subTabsForOutWardTrip(true);
}

function clearAddTrip()
{
	 outWardTripArray=[];
	 selectedTripArray=[];
	 tripList();
}

var outWardTripArray=[];
var selectedTripArray=[];
function showTripList(data,from,url)
{
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=1;
	var html1='';
	var pageCount;
	$(data).each(function(index, element) {
		pageCount=element.paginationCount;
			var id=element.DispatchEnteryRegistrationId;
			if ($.inArray(id, outWardTripArray) < 0) 
				{
			          html1+="<tr style='text-align: center;' id='search_"+element.DispatchEnteryRegistrationId+"'>"
							+" <td>"
							+"<div class='div_checkbox'>"
							+"<input type='checkbox' id='checkbox_"+element.DispatchEnteryRegistrationId+"' name='DispatchEnteryRegistrationId' onclick='tripEntryCheckBoxEvent("+element.DispatchEnteryRegistrationId+")' value='"+element.DispatchEnteryRegistrationId+"'style='opacity: 1 !important;'>"
							+"<input type='hidden' id='org_"+element.DispatchEnteryRegistrationId+"' value='"+element.ORG_ID+"'>"
							+"</div>"
							+"</td>"
							+"<td>"+SR_NO+"</td>"
							+"<td>"+element.ORG+"</td>"
							+"<td>"+element.COMPNAY+"</td>"
							+"<td>"+element.STOCKIST+"</td>"
							+"<td>"+element.TRANSPORTER+"</td>";
			                var invoiceDetails=element.INVOICE_DETAILS;
			                console.log("invoice_details"+element.INVOICE_DETAILS);
							/*if(invoiceDetails.length > 0)
								{
								html1+="<td>";
								 $(invoiceDetails).each(function(index,element){
									 html1+=element.INVOICE_NO+",";
										 });
								 html1+="</td>";
								}*/
			                //code added from previous code
			                html1 += "<td>";
							$(invoiceDetails)
									.each(
											function(index, element) {
												html1 += "<a href='#' onclick='showMoreInvoice("
														+ JSON
																.stringify(invoiceDetails)
														+ ")' style='color:#2a6496'><strong>"
														+ element.INVOICE_NO
														+ "[+]</strong></a>";
												return false;
											});
							html1 += "</td>";
			                /////////////
							html1+="<td>"+element.NO_OF_CASES+"</td>"
							+"<input type='hidden' id='noOfCases_"+element.DispatchEnteryRegistrationId+"' value='"+element.NO_OF_CASES+"'>"
							+"<td>"+element.SUBMIT_DATE+"</td>"
							+"</tr>";i++;SR_NO++;
							console.log("no presnt");
			    }
		          $("#selectedRecord").html('')
		          for(var i = 0; i < selectedTripArray.length; i++)
		          {
		        	  $("#selectedRecord").append(selectedTripArray[i]);
		        	  $("#checkbox_"+outWardTripArray[i]+"").prop('checked', true);
		          }
		          
				
		});
		$('#nonSelectedRecord').append(html1);
		$('#addTripCart').val(outWardTripArray.length);
		$('#loader').hide();
		paginationViewMore(from,pageCount,url);

}

function showMoreInvoice(invoiceDetails) {
	// $(invoiceDetails).each(function(index,element){
	// alert(element.INVOICE_NO);
	// });
	var i = 1;
	var html = ""
			+ "<div class='modal fade ' id='modal-responsive123' >"
			+ "    <div class='col-md-13'>"
			+ "        <div class='modal-content'>"
			+ "            <div class='modal-header'>"
			+ "                <input value='x' onClick='hideSubPopUp(\"modal-responsive123\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			+ "                <h4 class='modal-title' id='myModalLabel'><strong>Invoice Details</strong></h4>"
			+ "            </div>"
			+ "  		   <div class='panel panel-default'>"
			+ "			       <div class='panel-body'>"
			+ "						<div class='row'>"
			+ "							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
			+ "								<table class='table table-striped table-hover'>"
			+ "									<thead class='no-bd'>"
			+ "										<tr>"
			+ "											<th style='text-align: center;'><strong>Sr No.</strong>"
			+ "											</th>"
			+ "											<th style='text-align: center;'><strong>Invoice Number</strong>"
			+ "											</th>" + "										</tr>" + "									</thead>"
			+ "									<tbody class='no-bd-y'>";
	$(invoiceDetails)
			.each(
					function(index, element) {
						html += "                                    <tr style='text-align: center;'>"
								+ "                                         <td>"
								+ i++
								+ "</td>"
								+ "                                         <td >"
								+ element.INVOICE_NO
								+ "</td>"
								+ "										</tr>";
					});

	html += "									</tbody>"
			+ "								</table>"
			+ "							</div>"
			+ "						</div>"
			+ "             <div class='modal-footer text-center'>"
			+ "                <button type='button' onClick='hideSubPopUp(\"modal-responsive123\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			+ "            </div>" + "			   </div>" + "        </div>"
			+ "    </div>" + "</div>";
	$("#popup").html(html);
	$("#modal-responsive123").addClass("in");
	$("#modal-responsive123").attr("aria-hidden", "false");
	$("#modal-responsive123").css("display", "block");
}


function tripEntryCheckBoxEvent(DispatchEnteryRegistrationId)
{
	if(!!~outWardTripArray.indexOf(DispatchEnteryRegistrationId))
		{
		console.log("presnent");
		console.log(outWardTripArray);
		var i = outWardTripArray.indexOf(DispatchEnteryRegistrationId);
		var html="<tr id='selected_"+DispatchEnteryRegistrationId+"' style='text-align: center;'>"
		var test = $("#selected_"+DispatchEnteryRegistrationId+"").html();
		html+=test+"</tr>";
		var j = selectedTripArray.indexOf(html);
			if(i != -1 && j != -1) 
			{
				outWardTripArray.splice(i, 1);
				selectedTripArray.splice(j, 1);
				$("#selected_"+DispatchEnteryRegistrationId+"").remove();
				console.log(outWardTripArray);
				console.log(selectedTripArray);
			}
		}
	else
		{
		console.log("not presnent");
		var html="<tr id='selected_"+DispatchEnteryRegistrationId+"' style='text-align: center;'>"
		var test = $("#search_"+DispatchEnteryRegistrationId+"").html();
		html+=test+"</tr>";
		$('#selectedRecord').append(html);
		$("#checkbox_"+DispatchEnteryRegistrationId+"").prop('checked', true);
		$("#search_"+DispatchEnteryRegistrationId+"").remove();
		
		selectedTripArray.push(html);
		outWardTripArray.push(DispatchEnteryRegistrationId);
		console.log(outWardTripArray);
		console.log(selectedTripArray);
		}
	 $('#addTripCart').val(outWardTripArray.length);
}


function paginationViewMore(selectedPage, pageCount,url) {
	if(pageCount>0){
		var pageCounter = selectedPage;
		var html = "<ul>";
		if(selectedPage<pageCount)
			html += "<li><a href='#' onclick='"+url+(selectedPage+1)+")'>Next</a></li>";
		html += "</ul>";
		$('#pagination').html(html);
	}
	else
		$('#pagination').html("");
}




function changeFunctionOutwardTrip()
{
	var selectedValue=$('#searchOption').val();
	var html="";
	
		if(selectedValue=="NoOfCases" || selectedValue=="LoadingCharges")
		{
    		html+="<input id='inputFromData3' type='text' class='form-control' placeholder='From ' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTOData3' type='text' class='form-control' placeholder='To ' style='width: 49%;'>";
		}
		
		else if(selectedValue=="Date")
    	{
    		html+="<input id='inputFromDate' type='text' class='dateClassCommon form-control' placeholder='From Date' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTODate' type='text' class='dateClassCommon form-control' placeholder='To Date' style='width: 49%;'>";
    	}
		
    	else
    	{
    		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control' name=''>";
    	} 	
		$("#searchControl").html(html);
		$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
}

function searchOutwardAddTrip()
{
	
	$('#nonSelectedRecord').html('');
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if(selectedOption=="NoOfCases")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 	fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
		 commonsearchOutwardAddTripController(selectedOption,searchText,fromSpecialData,toSpecialData,1);	 
	 }
	 else
	 {
		if(!(Boolean($('#searchText').val())))
		{
//			alert("Please Enter the search text");
			selectedOption='';
			commonsearchOutwardAddTripController(selectedOption,searchText,fromSpecialData,toSpecialData,1);
		}
		else
		{
		searchText=$('#searchText').val();
		commonsearchOutwardAddTripController(selectedOption,searchText,fromSpecialData,toSpecialData,1);
		}
	 }
	
}



function commonsearchOutwardAddTripController(selectedOption,searchText,fromSpecialData,toSpecialData,from)
{
	$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardAddTrip',
		{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData,from:from
	}, function(data) {
		var url = "commonsearchOutwardAddTripController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
		showTripList(data,from,url); // to call the function which shows the searched item

	}, 'json');
}

function addTripEntry()
{
	var checkboxesValues = document.getElementsByName('DispatchEnteryRegistrationId');
	var vals = "";
	var orgValues="";
	var noOfCases="";
	for (var i=0, n=checkboxesValues.length;i<n;i++) {
	  if (checkboxesValues[i].checked) 
	  {
	  orgValues +=","+$("#org_"+checkboxesValues[i].value+"").val();
	  noOfCases +=","+$("#noOfCases_"+checkboxesValues[i].value+"").val();
	  vals += ","+checkboxesValues[i].value;
	  }
	}
	if (vals) vals = vals.substring(1);
	if(orgValues) orgValues=orgValues.substring(1);
	if(vals=="")
	{
	alert("Please select at least one check box");
	}
	else
	{
	var temp = new Array();
	// this will return an array with strings "1", "2", etc.
	temp = orgValues.split(",");
	var organizationId=temp[0];
		if(temp.allValuesSame())
			{
			// calculate no of cases
//			alert(noOfCases);
			var noOfCasesArray=new Array();
			noOfCasesArray= noOfCases.split(",");
			sumNoOfCases = 0;
			$.each(noOfCasesArray,function(){sumNoOfCases+=parseFloat(this) || 0;});
			//load stockist,carting agent, carting agent trip count,org trip count
			$.post(contextApplicationPath+'/OutWardGoodsController/getStockistUsignDispatchEnteryRegistrationId',{dispatchEnteryRegistrationId:vals}, function(data) {
				var html="                                                <div class='modal fade' id='modal-responsive' aria-hidden='true'>"
					+"                                                    <div class='modal-dialog modal-lg'>"
					+"                                                        <div class='modal-content'>"
					+"                                                            <div class='modal-header'>"
					+"                                                                <button type='button' class='close' data-dismiss='modal' aria-hidden='true' onclick='hidePopUp()'>×</button>"
					+"                                                                <h4 class='modal-title' id='myModalLabel'><strong>Trip Entry</strong></h4>"
					+"                                                            </div>"
					+"<form id='addTripEntry' action='"+contextApplicationPath+"/OutWardGoodsController/saveTripEntry' method='post'>"
					+"<input type='hidden'  value='"+organizationId+"' name='organizationId'>"
					+"<input type='hidden'  value='"+vals+"' name='DispatchEnteryRegistrationId'>"
					+"                                                            <div class='modal-body' style=' text-align: left;'>"
					+"                                                                <div class='row'>"
					+"                                                                    <div class='col-md-4'>"
					+"                                                                        <div class='form-group'>"
					+"<div id='organiztionDropDown'></div>"
					+"                                                                        </div>"
					+"                                                                    </div>"
					+"                                                                    <div class='col-md-4'>"
					+"                                                                        <div class='form-group'>"
					+"                                                                            <label for='field-1' class='control-label'><strong>Select Carting Agent</strong></label></br>"
					+"                                                                            <div class='controls'>"
					+"<div id='CartingAgentDropDown'></div>"
					+"                                                                            </div>"
					+"                                                                        </div>"
					+"                                                                    </div>"
					+"                                                                    <div class='col-md-4'>"
					+"                                                                        <div class='form-group'>"
					+"                                                                            <label for='field-6' class='control-label'><strong>Total Organization Trip Count</strong></label></br>"
					+"                                                                            <input type='text' class='form-control' id='org_trip_count_id' value='' placeholder='Organization Trip Count' readonly>"
					+"                                                                        </div>"
					+"                                                                    </div>"
					+"                                                                    <div class='col-md-4'>"
					+"                                                                        <div class='form-group'>"
					+"                                                                            <label for='field-6' class='control-label'><strong>Carting Agent Trip Count</strong></label></br>"
					+"                                                                            <input type='text' class='form-control' id='carting_agent_trip_count_id' value='' placeholder='Carting Agent Trip Count' readonly>"
					+"                                                                        </div>"
					+"                                                                    </div>"
					+"                                                                    <div class='col-md-4'>"
					+"                                                                        <div class='form-group'>"
					+"                                                                            <label for='field-2' class='control-label'><strong>Loading Charges</strong></label>"
					+"                                                                            <input type='text' class='form-control' id='field-2' placeholder='Loading Charges' name='loadingCharges' parsley-type='number'>"
					+"                                                                        </div>"
					+"                                                                    </div>"
					+"                                                                    <div class='col-md-4'>"
					+"                                                                        <div class='form-group'>"
					+"                                                                            <label for='field-5' class='control-label'><strong>Dispatch By</strong></label></br>"
					+"                                                                            <input type='text' class='form-control' id='field-4' placeholder='Dispatch By' name='dispatchBy' required>"
					+"                                                                        </div>"
					+"                                                                    </div>"
					+"                                                                    <div class='col-md-4'>                                                                        "
					+"                                                                        <div class='form-group'>"
					+"                                                                            <label for='field-3' class='control-label'><strong>Vehicle No</strong></label>"
					+"                                                                            <input type='text' class='form-control' id='VehicleNo' placeholder='Vehicle No' name='VehicleNo' readonly>"
					+"                                                                        </div>                                                                    "
					+"                                                                    </div>"
					+"                                                                    <div class='col-md-4'>"
					+"                                                                        <div class='form-group'>"
					+"                                                                            <label for='field-3' class='control-label'><strong>Driver No</strong></label>"
					+"                                                                            <input type='text' class='form-control' id='DriverNo' placeholder='Driver No' name='DriverNo' readonly>"
					+"                                                                        </div>"
					+"                                                                    </div>"
					+"                                                                    <div class='col-md-4'>"
					+"                                                                        <div class='form-group'>"
					+"                                                                            <label for='field-3' class='control-label'><strong>No of Cases</strong></label>"
					+"                                                                            <input type='text' class='form-control' id='field-5' placeholder='No of Cases' value='"+sumNoOfCases+"' readonly name='totalNoOfCases'>"
					+"                                                                        </div>"
					+"                                                                    </div>";
//					+"                                                                    <div class='col-md-4'>"
//					+"                                                                        <div class='form-group'>"
//					+"                                                                            <label for='field-3' class='control-label'><strong>Transporter Name</strong></label>";
				$(data).each(function(index, element) {
//					html+="<input type='text' class='form-control' id='field-5' value='"+element.TRANSPORTER+"' readonly style='margin:5px;'>"
					html+="<input type='hidden' name='transporterId' value='"+element.TRANSPORTER_ID+"'>";
				}); 
//				html+="                                                                        </div>"
//					+"                                                                    </div>"
				html+="                                                                </div>     "                                                       
					+"                                                                <div class='modal-footer text-center'>"
					+"                                                                    <button type='submit' class='btn btn-primary' onclick='validateFormDetails(\"#addTripEntry\")'>Submit</button>"
					+"                                                                    <button type='button' class='btn btn-danger' data-dismiss='modal' onclick='hidePopUp()'>Cancel</button>"
					+"                                                                </div>"
					+"                                                            </div>"
					+"</form>"
					+"                                                        </div>"
					+"                                                    </div>"
					+"                                                </div>";
				$('#popup').html(html);
				loadOutWardTripOrganiztionDropDown(organizationId);
				loadOrgizationTripCount(organizationId);
				ajaxAddTrip('addTripEntry');
				$("#modal-responsive").addClass("in");
				$("#modal-responsive").attr("aria-hidden","false");
				$("#modal-responsive").css("display","block");
			}, 'json');
			}
		else
			{
			alert("Please select same orgnization invoice");
			}
	}
}

function ajaxAddTrip(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert("Your Trip No is="+element.TRIP_NO)
					alert(element.MSG);
					outWardTripArray=[];
					selectedTripArray=[];
					tripList();
					$("#modal-responsive").remove();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function loadOrgizationTripCount(organizationId)
{
	var orgTripCount;
	$.post(contextApplicationPath+'/OutWardGoodsController/getOrganizationTripCountObjUsignOrgId',{organizationId : organizationId} ,function(data) {
		$(data).each(function(index, element) {
			orgTripCount=element.ORG_TRIP_COUNT;
		}); 
		$('#org_trip_count_id').val(orgTripCount)
	}, 'json');
}

function loadOrgnizationCartingAgentUsignOrgId(organizationId)
{
	var html="<select class='form-control' class='form-control' name='cartingAgentId' id='cartingAgentId' onchange='laodCartingAgentTripCount()' required>"
		+"<option selected disabled>Select Carting Agent</option>";
			$.post(contextApplicationPath+'/CartingAgentController/cartingAgentListingUsignOrgId',{organizationId : organizationId} ,function(data) {
				$(data).each(function(index, element) {
					html+="<option value='"+element.CARTING_AGENT_ID+"'>"+element.CARTING_AGENT_NAME+"</option>";
				}); 
				html+="</select>";
				$('#CartingAgentDropDown').html(html);
			}, 'json');
}

function laodCartingAgentTripCount()
{
	var cartingAgentTripCount;
	var driverNo;
	var vehicleNo;
	$.post(contextApplicationPath+'/OutWardGoodsController/getCartingAgentTripCountObjUsignOrgIdAndCartingAgentId',{cartingAgentId : $('#cartingAgentId').val(),organizationId : $("#orgId").val()} ,function(data) {
		$(data).each(function(index, element) {
			cartingAgentTripCount=element.CARTING_AGENT_TRIP_COUNT;
			driverNo=element.DRIVER_NO;
			vehicleNo=element.VEHICLE_NO;
		}); 
		$('#carting_agent_trip_count_id').val(cartingAgentTripCount)
		$('#DriverNo').val(driverNo)
		$('#VehicleNo').val(vehicleNo)
	}, 'json');
}

function loadOutWardTripOrganiztionDropDown(organizationId)
{
	var content="<div class='form-group' style='height: 58px;'>"
		+"<label class='form-label'><strong>Your Organization</strong>"
		+"</label>"
		+"<span class='tips'></span>"
		+"<div class='controls'>";
	$.post(contextApplicationPath+'/OrganizationController/ViewAllDetailsOfOrganization',{organizationId:organizationId}, function(data) {
		$(data).each(function(index, element) {
				content+="<input type='hidden' value='"+element.ORG_ID+"' name='selectedOrganiztionNameId' id='orgId'>" +
						""+element.ORG_NAME+"";
		}); 
		content+="</div>"
		+"</div>";
		$('#organiztionDropDown').html(content);
		loadOrgnizationCartingAgentUsignOrgId($("#orgId").val());
//		loadTransporterList($("#orgId").val());
	}, 'json');
}

function viewTripEntry()
{
	$('#loader').show();
	var i=1;
	//$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardTripEntryOfStatusOne', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_4'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Trip</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOutWardTrip'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																	<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardTrip();'>"
		+ "                                                     		  		<option value='StockistName'>Stockist Name</option>"
		+"																		<option value='TripNo'>Trip Number</option>"
		+"																		<option value='LoadingCharges'>Loading Charges</option>"
		+"																		<option value='Organization'>Select Organization</option>"
		+"																		<option value='TransporterName'>Transporter Name</option>"
		+"																		<option value='DispatchBy'>Dispatch By</option>"
		+"																		<option value='VehicleNo'>Vehicle Number</option>"
		+"																		<option value='DriverNo'>Driver Number</option>"
		+ "                                                     		  		<option value='NoOfCases'>No. of Cases</option>"
		+ "                                                      		 		<option value='CartingAgent'>Carting Agent</option>"
		+ "                                                     		  		<option value='Date'>Date</option>"
		//+ "                                                     		  		<option value='StockistName'>Stockist Name</option>"
		+ "                                                    			   		<option value='InvoiceNumber'> Invoice Number</option>"
		//+"																		<option value='PayableAmount'> Payable Amount</option>"
		+ "                                                     		  		<option value='CompanyName'>Company Name</option>"
		+"																	</select>"	
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardViewTripList()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                        <table class='table table-striped table-hover'>"
		+"                                                                            <thead class='no-bd'>"
		+"                                                                                <tr>"
//		+"                                                                                    <th style='text-align: center;'><strong>Select</strong>"
//		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Trip No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Loading Charges</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Transporter Name</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Dispatch By</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Vehical No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Driver No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>No of Cases</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                </tr>"
		+"                                                                            </thead>"
		+"                                                                <tbody class='no-bd-y' id='searchTrip'>"
		+"                                                                </tbody>"
		+"																				</table>"	
		+"                                                     </div>"
		+"                    							   <div id='pagination' class='pagination'></div>"
		+"                                                </div>"
		+"                                             </div>"
		+"                                        </div>"
		+"                                      </div>"
		+"                                    </div>"
		+"                                  </div>"
		+"                               </div>"
		+"                            </div>"
		+"                          </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	//ShowViewTripEntry(data);
	commonsearchOutwardViewTripListController("","","","",1);
	subTabsForOutWardTrip(false);
	//}, 'json');
}

function ShowViewTripEntry(data,from,url)
{		SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=1;
	var html=""
		var pageCount;
		/*+"                                                                        <table class='table table-striped table-hover'>"
		+"                                                                            <thead class='no-bd'>"
		+"                                                                                <tr>"
//		+"                                                                                    <th style='text-align: center;'><strong>Select</strong>"
//		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Trip No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Loading Charges</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Transporter Name</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Dispatch By</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Vehical No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Driver No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>No of Cases</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                </tr>"
		+"                                                                            </thead>"*/
	//	+"                                                                            <tbody class='no-bd-y'>";
	
	$(data).each(function(index, element) {
		pageCount=element.paginationCount;
		if((data.length-1)<i)
			return false;
			html+="<tr style='text-align: center;'>"
//				+" <td>"
//				+"<div class='div_checkbox'>"
//				+"<input type='checkbox' id='ctest' style='opacity: 1 !important;' value='"+element.OUTWARD_TRIP_ID+"'>"
//				+"</div>"
//				+"</td>"
				+"<td>"+SR_NO+"</td>"
				+"<td id='TRIP_NO_"+i+"'>"+element.TRIP_NO+"</td>"
				+"<td id='LOADING_CHARGES_"+i+"'>"+element.LOADING_CHARGES+"</td>"
				+"<td id='ORG_NAME_"+i+"'>"+element.ORG_NAME+"</td>"
				+"<td id='TRAN_"+i+"'>"+element.TRAN+" [+]</td>"
				+"<td id='DISPATCH_BY_"+i+"'>"+element.DISPATCH_BY+"</td>"
				+"<td id='VEHICAL_NO_"+i+"'>"+element.VEHICAL_NO+"</td>"
				+"<td id='DRIVER_NO_"+i+"'>"+element.DRIVER_NO+"</td>"
				+"<td id='NO_OF_CASES_"+i+"'>"+element.NO_OF_CASES+"</td>"
				+"<td><a class='edit btn btn-blue' href='#' onclick=\"viewTripEntryDetails('"+element.OUTWARD_TRIP_ID+"','"+element.TRIP_NO+"','"+element.NO_OF_CASES+"','"+element.VEHICAL_NO+"','"+element.DISPATCH_BY+"','"+element.LOADING_CHARGES+"','"+element.DRIVER_NO+"','"+element.CARTING_AGENT+"','"+element.ORG_NAME+"')\"><i class='fa fa-external-link'></i></a><a class='edit btn btn-dark' href='#' onclick='editTripEntry("+element.OUTWARD_TRIP_ID+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick='deleteTripEntry("+element.OUTWARD_TRIP_ID+")'><i class='fa fa-times-circle'></i> </a>  <a class='delete btn btn-warning' href='#' onclick='printTripEntry("+element.OUTWARD_TRIP_ID+")'><i class='fa fa-print'></i> </a></td>"
				+"</tr>";
			i++;SR_NO++;
		});
		
		$('#searchTrip').append(html);
		$('#loader').hide();
		paginationViewMore(from,pageCount,url);
}

function searchOutwardViewTripList()
{	
$('#searchTrip').html('');
	var selectedOption = $('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if(selectedOption=="LoadingCharges")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
		 }
		 else
		 {
			 	fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if(selectedOption=="NoOfCases")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (selectedOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Selected TO Date should be greater than from Date');
			return false;
		}
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonsearchOutwardViewTripListController(selectedOption,searchText,fromSpecialData,toSpecialData,1);
	 }
	
	
	function commonsearchOutwardViewTripListController(selectedOption,searchText,fromSpecialData,toSpecialData,from)
	{
	$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardViewTripList',
		{
		selectedValue : selectedOption, searchText : searchText, fromSpecialData : fromSpecialData, toSpecialData : toSpecialData,from:from
	}, function(data) {
		var url = "commonsearchOutwardViewTripListController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
		ShowViewTripEntry(data,from,url); // to call the function which shows the searched item
		//$('#searchResult').html(html);
}, 'json');
}

function deleteTripEntry(tripEntryId)
{
	var r = confirm("Do you want to Delete Entry...!");
	if (r == true) {
		$.post(contextApplicationPath+'/OutWardGoodsController/deleteTripEntry', {
			tripEntryId : tripEntryId
		}, function(data) {
			tripList();
			alert(data.MSG);
		}, 'json');
	}
	
}

function editTripEntry(tripEntryId)
{
//	alert("in edit TripEntry==="+tripEntryId);
	editTripEntryFunction(tripEntryId);
}

// start codign get pass
function subTabsForOutWardGetPass(flag)
{
	 var html = ""
	  +"                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
	  +"                                            <li id='outWordGetPassTabEnetry1' class=''><a href='#tab2_6' data-toggle='tab' onclick='getPassList()'><h5><strong>Add LR</strong></h5></a></li>"
	  +"                                            <li id='outWordGetPassTabEnetry2' class=''><a href='#tab2_7' data-toggle='tab' onclick='viewGetPassLRList()'><h5><strong>View LR</strong></h5></a></li>"
	  +"                                        </ul>";
	 $('#subTabsForOutWardGetpass').html(html);
	 		if(flag)
	 			$('#outWordGetPassTabEnetry1').addClass("active");
	 		else
				$('#outWordGetPassTabEnetry2').addClass("active");
}

function getPassList()
{
	$('#loader').show();
	var i=1;
	//$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardDispatchEntryListOfStatusOneAndTripStatusOne', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_5'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>LR Updation</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOutWardGetpass'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																	<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		 	 	<option value='Stockist'>Stockist</option>"
	//	+ "                                                     		 	 	<option value='Stockist'>Stockist</option>"
		+ "                                                    				   	<option value='TripNo'> Trip Number</option>"
		+"																		<option value='Organization'>Organization</option>"
		+ "                                                      		 		<option value='Company'>Company Name</option>"
		//+ "                                                     		 	 	<option value='Stockist'>Stockist</option>"
		+ "                                                     		 	 	<option value='NoOfCases'>No. of Cases</option>"
		+ "                                                    			  	 	<option value='DispatchBy'>Dispatch By</option>"
		+"																	</select>"	
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardGetPassList()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"																<div class='pull-right'>"
		+"                                                      	 	 </div>"
		+"                                                    </div>"
		+"                                                </div>     "                                           
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                        <table class='table table-striped table-hover'>"
		+"                                                                            <thead class='no-bd' id='a'>"
		+"                                                                                <tr>"
		+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Trip No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Dispatch Date</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>No of Cases</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Invoice No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Dispatch By</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                </tr>"
		+"                                                                            </thead>"
		+"                                                                <tbody class='no-bd-y' id='selectedRecord'>"
		+"                                                                </tbody>"
		+"                                                                            </table>"
		+"                                                        </div>"
		+"                                           <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
//	showgetPassList(data);
	commonsearchOutwardGetPassListController("","","","",1);
	subTabsForOutWardGetPass(true);
//}, 'json');
}
		
	function showgetPassList(data,from,url)
	{
		SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
		var i=0;
		var html=""
			var pageCount;
		/*SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
		var i=1;
		var html=""
		+"                                                                        <table class='table table-striped table-hover'>"
		+"                                                                            <thead class='no-bd'>"
		+"                                                                                <tr>"
		+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Trip No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Dispatch Date</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>No of Cases</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Invoice No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Dispatch By</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                </tr>"
		+"                                                                            </thead>"
		+"                                                                            <tbody class='no-bd-y'>";
	*/
		
		//comonData is global var
		commonData = data;
		console.log(commonData);
		$(data).each(function(index, element) {
			pageCount=element.paginationCount;
			if((data.length-2)<i)
				return false;
			html+="<tr style='text-align: center;'>"
				+"<td>"+SR_NO+"</td>"
				+"<td id='TRIP_NO_"+i+"'>"+element.TRIP_NO+"</td>"
				+"<td id='ORG_NAME_"+i+"'>"+element.ORG_NAME+"</td>"
				+"<td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
				+"<td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
				+"<td id='DATE"+i+"'>"+element.DATE+"</td>"
//				+"<td>"+element.TRAN+"</td>"
				+"<td id='NO_OF_CASES_"+i+"'>"+element.NO_OF_CASES+"</td>"
				+"<td id='INVOICE_NO"+i+"'>"+element.INVOICE_NO+"</font></td>"
				+"<td id='DISPATCH_BY_"+i+"'>"+element.DISPATCH_BY+"</td>"
				
				+"<td>"
				+"<a class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive25' onclick='addGetPassLR("+element.OUTWARD_DISPATCH_ENTRY_REG_ID+","+element.ORG_ID+")'>Add LR</a><a class='edit btn btn-blue' href='#' onclick='viewAddLRDetails("+i+")' ><i class='fa fa-external-link'></i></a></td>"
//				+"<a class='btn btn-info' href='#'>Print Sticker</a></td>"
		//		+"<td><a class='edit btn btn-blue' href='#' onclick='viewAddLRDetails("+i+")' ><i class='fa fa-external-link'></i></a></td>"
				+"</tr>";				
			i++;SR_NO++;
		});
		$('#selectedRecord').append(html);
		$('#loader').hide();
		paginationViewMore(from,pageCount,url);
}
	
	
	function viewAddLRDetails(index)
	{	var i=0;
		var html="" 
			
	+ "													<div class='modal fade in' id='modal-responsive' >"
	+ "    														<div class='col-md-13'>"
	+ "     														   <div class='modal-content'>"
	+ "       																	     <div class='modal-header'>"
	+ "           																		     <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
	//+ "           																			     <h4 class='modal-title' id='myModalLabel'><strong>View LR Details</strong></h4>"
	+ "           																	 </div>"
	+ "  															  <div class='panel panel-default'>"
	+ "																 <div class='panel-body'>"
	+ "   											   		 				<div class='row'>"
	+ "     																	  <div class='col-md-4'>";
	+ "          															 </div>";
							html+= "						        </div>"
	
	  html+= "        										   <div class='modal-header'>"
	+ "              												  <h4 class='modal-title' id='myModalLabel'><strong>ORDER INFORMATION</strong></h4>"
	+ "           											   </div>"
	+ "			    													   <div class='panel-body'>"			
	+"																					<div class='row'>"
	+"														  <div class='col-md-4'>"
	+"                                        					  <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>ORGANIZATION NAME : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                                <label class='form-label'><strong>"+commonData[index].ORG_NAME+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"                                                            
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>COMPANY NAME : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                                <label class='form-label'><strong>"+commonData[index].COMPANY_NAME+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"                                                            
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>ORDER NAME : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                                <label class='form-label'><strong>"+commonData[index].ORDER_NAME+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+ "          <div class='form-group'>"
	+ "               <label class='form-label'><strong>Date : - </strong>"
	+ "               </label>"
	+ "				 <label class='form-label'><strong>"+commonData[index].ORDER_DATE+"</strong>"
	+ "                </label>"
	+ "               <span class='tips'></span>"
	+ "            </div>"
	+"                                                        </div>"
	+"														  <div class='col-md-4'>"
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>STOCKIST NAME : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                               <label class='form-label'><strong>"+commonData[index].STOCKIST_NAME+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+ "            <div class='form-group'>"
	+ "                <label class='form-label'><strong>ORDER NO : - </strong>"
	+ "               </label>"
	+ "				  <label class='form-label'><strong>"+commonData[index].ORDER_NO+"</strong>"
	+ "                </label>"
	+ "           </div>"
	
	  +"            <div class='form-group'>"
		+"            <label class='form-label'><strong>Order Copy : - </strong>"
		+"				<span class='tips'></span>"
		+"              <div id='imgDiv'>"
		+"              <img onClick='showOutwardOrderCopyImagesListPopupAddLr("+index+")' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"//<img onClick='showImagePopup(\""+element.ORDER_COPY+"\")' src='"+element.ORDER_COPY+"' alt='Smiley face' height='100' width='100'>"
		+"              </div>"
		+"              </div>";
	  html+= "       </div>"
	+"						</div>"
	+ "			   </div>"
/*var i=1;
	var invoiceInfo=commonData[index].Array;
		if(invoiceInfo.length > 0)
			{
		
	+ "                 <div class='modal-header'>"
	+ "                     <h4 class='modal-title' id='myModalLabel'><strong>Invoice Information :</strong></h4>"
	+ "            	   	</div>"	
	+"						<div class='row'>"
	+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
	+"								<table class='table table-striped table-hover'>"
	+"									<thead class='no-bd'>"
	+"										<tr>"
	+"											<th style='text-align: center;'><strong>Sr No.</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>INVOICE No</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>INVOICE DATE</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>GROSS AMOUNT</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>TAX AMOUNT</strong>"
	+"											</th>      "
	+"											<th style='text-align: center;'><strong>VAT AMOUNT</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>DISCOUNT</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>NET AMOUNT</strong>"
	+"											</th>"
	+"										</tr>"
	+"									</thead>";

	//var irgrLRArr = commonData.irgrLRArr;

	$(invoiceInfo).each(function(index)
	{
		html+="                                    <tr style='text-align: center;'>"
	+"                                         <td>"+ i++ +"</td>"
	+"                                         <td >"+commonData[index].invoice+"</td>"
	+"                                         <td >"+commonData[index].INVOICE_DATE+"</td>"
	+"                                         <td>"+commonData[index].GROSS_AMT+"</td>"
	+"                                         <td>"+commonData[index].TAX_AMT+"</td>"
	+"                                         <td>"+commonData[index].VAT_AMT+"</td>"
	+"                                         <td>"+commonData[index].DISCOUNT+"</td>"
	+"                                         <td>"+commonData[index].NET_AMT+"</td>"
	+"											</td>"
	+"										</tr>";
			//i++;
		 });
		
			html+="									</tbody>"
	+"								</table>"
	+"							</div>"
	+"							<div id='subPopUp'></div>"
	+ " 			    	</div>"
	+ " 			    	</div>"
	+ "            		</div>";
		
		
			};*/
			
			
			var i=1;
			var invoiceInfo=commonData[index].INVOICE_LR_ARRAY;
			
				if(invoiceInfo.length > 0)
	      		{
	         html +="                                                <div class='panel-body'>"
			   +"                                                    <div class='row'>"
			    + "                                                      <h5 class='modal-title' id='myModalLabel'><strong>Invoice Information</strong>"
	            + "            									        </h5>"
	            +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
	            +"                                                            <table class='table table-striped table-hover'>"
	            +"                                                                <thead class='no-bd'>"
				+"                                                                    <tr>"
				+"											<th style='text-align: center;'><strong>Sr No.</strong>"
				+"											</th>"
				+"											<th style='text-align: center;'><strong>INVOICE No</strong>"
				+"											</th>"
				+"											<th style='text-align: center;'><strong>INVOICE DATE</strong>"
				+"											</th>"
				+"											<th style='text-align: center;'><strong>GROSS AMOUNT</strong>"
				+"											</th>"
				+"											<th style='text-align: center;'><strong>TAX AMOUNT</strong>"
				+"											</th>      "
				+"											<th style='text-align: center;'><strong>VAT AMOUNT</strong>"
				+"											</th>"
				+"											<th style='text-align: center;'><strong>DISCOUNT</strong>"
				+"											</th>"
				+"											<th style='text-align: center;'><strong>NET AMOUNT</strong>"
				+"											</th>"
				+"                                                                    </tr>"
				+"                                                                </thead>"
				+"                                                                <tbody class='no-bd-y'>";
	      
	         $(invoiceInfo).each(function(index,element){
			html+="                                                                    <tr style='text-align: center;'>"
				+"                                         <td>"+ i++ +"</td>"
				+"                                         <td >"+element.INVOICE+"</td>"
				+"                                         <td >"+element.IN_DATE+"</td>"
				+"                                         <td>"+element.GROSS_AMT+"</td>"
				+"                                         <td>"+element.TAX_AMT+"</td>"
				+"                                         <td>"+element.VAT_AMT+"</td>"
				+"                                         <td>"+element.DISCOUNT+"</td>"
				+"                                         <td>"+element.NET_AMT+"</td>"
			+"                                                                    </tr>"
			 });
	html+="                                                                </tbody>"
	+"                                                            </table>"
	+"                                                        </div>"
	+"                                                    </div>"
	+"                                                </div>";
	      		}
			
			
			
			
	   html+= "           <div class='modal-header'>"
	+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Outward Order Dispatch Entry Registration Details</strong></h4>"
	+ "            </div>"
	+ "			       <div class='panel-body'>"			
	+"						<div class='row'>"
	+"														  <div class='col-md-4'>"
	+"                                        					  <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>ORGANIZATION NAME : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                                <label class='form-label'><strong>"+commonData[index].ORG_NAME+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"                                                            
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>COMPANY NAME : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                                <label class='form-label'><strong>"+commonData[index].COMPANY+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"                                                            
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>STOCKIST : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                                <label class='form-label'><strong>"+commonData[index].STOCKIST+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+"                                                        </div>"
	+"														  <div class='col-md-4'>"
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>NO OF CASES : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                               <label class='form-label'><strong>"+commonData[index].NO_OF_CASES+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>SUBMITTED DATE: - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                               <label class='form-label'><strong>"+commonData[index].DATE+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+"                                                        </div>"
	+"						</div>"
	+ "			   </div>";

	  
	   
	   html+= "           <div class='modal-header'>"
	+ "                <h4 class='modal-title' id='myModalLabel'><strong>Gate Pass Details</strong></h4>"
	+ "            </div>"
	+ "			       <div class='panel-body'>"			
	+"						<div class='row'>"
	+"														  <div class='col-md-4'>"
	+"                                        					  <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>TRIP_NO : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                                <label class='form-label'><strong>"+commonData[index].TRIP_NO+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"                                                            
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>VEHICAL NO : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                                <label class='form-label'><strong>"+commonData[index].VEHICAL_NO+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"                                                            
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>LOADING CHARGES : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                                <label class='form-label'><strong>"+commonData[index].LOADING_CHARGES+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+"                                                        </div>"
	+"														  <div class='col-md-4'>"
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>TOTAL TRIP CASES : - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                               <label class='form-label'><strong>"+commonData[index].TRIP_CASE+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>DISPATCHED BY: - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                               <label class='form-label'><strong>"+commonData[index].DISPATCH_BY+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>CARTING AGENT: - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                               <label class='form-label'><strong>"+commonData[index].CARTING+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+"                                                            <div class='form-group'>"
	+"                                                                <label class='form-label'><strong>DRIVER NO: - </strong>"
	+"                                                                </label>"
	+"                                                                <span class='tips'></span>"
	+"                                                               <label class='form-label'><strong>"+commonData[index].DRIVER_NO+"</strong>"
	+"                                                                </label>"
	+"                                                            </div>"
	+"                                                        </div>"
	+"						</div>"
	html+="            <div class='modal-footer text-center'>"
		   + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
		   + "            </div>"
	+ "			   </div>"
	    + " </div>"
	+ " </div>"
	+ " </div>"
	+ "</div>";
	/*html+="            <div class='modal-footer text-center'>"
	+ "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	+ "            </div>"*/
	+ "        </div>"
	+ "    </div>"
	+ "</div>"
	$('#popup').html(html);
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block"); 
	}
	
	function showOutwardOrderCopyImagesListPopupAddLr(index){
		showListOfImagesPopup(commonData[index].ORDER_COPY);}
	
function searchOutwardGetPassList()
{
		$('#selectedRecord').html('');  
		var selectedOption = $('#searchOption').val();
		var fromSpecialData=null;
		var toSpecialData=null;
		var searchText=null;
		
		if(selectedOption=="NoOfCases")
		{
			 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
			 {
				 alert("it is not a number");
				 return false;
			 }
			 else
			 {
				 	fromSpecialData=$('#inputFromData3').val();
					toSpecialData=$('#inputTOData3').val();
			 }	
		}
		else
		{
			if(!(Boolean($('#searchText').val())))
			{
				alert("Please Enter the search text");
				return false
			}
			searchText=$('#searchText').val();
		}
		commonsearchOutwardGetPassListController(selectedOption,searchText,fromSpecialData,toSpecialData,1);
	}
function commonsearchOutwardGetPassListController(selectedOption,searchText,fromSpecialData,toSpecialData,from)
	{
		$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardGetPassList',
			{
			selectedValue : selectedOption, searchText : searchText, fromSpecialData : fromSpecialData, toSpecialData : toSpecialData,from:from
		}, function(data) {
			var url = "commonsearchOutwardGetPassListController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showgetPassList(data,from,url); // to call the function which shows the searched item
	}, 'json');
}

function addGetPassLR(outwardDispatchEntryRegId,organizationId)
{
	var html="                                                                                        <div class='modal fade' id='modal-responsive' aria-hidden='true'>"
	+"                                                                                            <div class='modal-dialog modal-lg'>"
	+"<form id='addGetPassLR' action='"+contextApplicationPath+"/OutWardGoodsController/saveGetPassLR' method='post'>"
	 +"<input type='hidden' name='organizationId' value='"+organizationId+"'>"
	 +"<input type='hidden' name='outwardDispatchEntryRegId' value='"+outwardDispatchEntryRegId+"'>"
	+"                                                                                                <div class='modal-content'>"
	+"                                                                                                    <div class='modal-header'>"
	+"                                                                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true' onclick='hidePopUp()'>×</button>"
	+"                                                                                                        <h4 class='modal-title' id='myModalLabel'><strong>Add LR</strong></h4>"
	+"                                                                                                    </div>"
	+"                                                                                                    <div class='modal-body'>"
	+"                                                                                                        <div class='row'>"
	+"                                                                                                            <div class='col-md-4'>"
	+"                                                                                                                <div class='form-group'>"
	+"                                                                                                                    <label for='field-1' class='control-label'>Add LR No</label></br>"
	+"                                                                                                                    <input type='text' class='form-control' id='field-1' placeholder='LR No' name='LRNo' required>"
	+"                                                                                                                </div>"
	+"                                                                                                            </div>"
	+"                                                                                                            <div class='col-md-4'>"
	+"                                                                                                                <div class='form-group'>"
	+"                                                                                                                    <label for='field-2' class='control-label'>Attach LR</label></br>"
	+"                                                                                                                    <input type='file' class='form-control' id='field-2' name='LRImage'>"
	+"                                                                                                                </div>"
	+"                                                            													<div id='lrImageDivId'></div>"
	+"                                                           													<div class='form-group'>"
	+"                                                				 													<div id='lrImageAddMoreButtonId'><button onclick=\"addFileConrol('lrImageDivId','lrImageAddMoreButtonId','LRImage',1)\" class='btn btn-primary'>Add More Files</button></div>"
	+"                                                           													 </div>"
	+"                                                                                                            </div>"
	+"                                                                                                        </div>"
	+"                                                                                                    </div>"
	+"                                                                                                    <div class='modal-footer text-center'>"
	+"                                                                                                        <button type='submit' class='btn btn-primary' data-dismiss='modal' onclick='validateFormDetails(\"#addGetPassLR\")'>Submit</button>"
	+"                                                                                                        <button type='button' class='btn btn-danger' data-dismiss='modal' onclick='hidePopUp()'>Cancel</button>"
	+"                                                                                                    </div>"
	+"                                                                                                </div>"
	+"</form>"
	+"                                                                                            </div>"
	+"                                                                                        </div>";
	$('#popup').html(html);
	ajaxAddGetPassLR('addGetPassLR');
	$("#modal-responsive").addClass("in");
	$("#modal-responsive").attr("aria-hidden","false");
	$("#modal-responsive").css("display","block");
}

function ajaxAddGetPassLR(FormId)
{
	var options = {
			success : function(json) {
				$(json).each(function(index, element) {
					$('#loader').hide();
					$('#'+ FormId)[0].reset();
					alert(element.MSG);
					getPassList();
					$("#modal-responsive").remove();
				});
			}
		};
$('#' + FormId).ajaxForm(options);
}

function viewGetPassLRList()
{
	$('#loader').show();
	//$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardDispatchEntryOfGetPassStatusOne', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_5'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>LR Updation</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOutWardGetpass'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																	<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutwardInvoiceEntry();'>"
		+ "                                                     		 	 	<option value='Stockist'>Stockist</option>"
		+ "                                                    				   	<option value='TripNo'> Trip Number</option>"
		+"																		<option value='Organization'>Organization</option>"
		+ "                                                      		 		<option value='Company'>Company Name</option>"
		//+ "                                                     		 	 	<option value='Stockist'>Stockist</option>"
		+ "                                                    			  	 	<option value='Transporter'>Transporter</option>"
		+ "                                                    			  	 	<option value='InvoiceNo'>Invoice Number</option>"
		+ "                                                     		 	 	<option value='NoOfCases'>No. of Cases</option>"
		+ "                                                     		 	 	<option value='LRNo'>LR Number</option>"
		+"																	</select>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchViewGetPassLRList()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"															<div class='pull-right'>"
		+"                                                      	</div>"
		+"                                                    </div>"
		+"                                                </div>"                                           
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                        <table class='table table-striped table-hover'>"
		+"                                                                            <thead class='no-bd' id='a'>"
		+"                                                                                <tr>"
		+"                                                                                    <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Trip No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Transporter</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Invoice No</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>No of Cases</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Dispatch Date</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>LR NO</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>LR Image</strong>"
		+"                                                                                    </th>"
		+"                                                                                    <th style='text-align: center;'><strong>Action</strong></th>"
//		+"                                                                                    <th style='text-align: center;'><strong>Print</strong></th>"
		+"                                                                                </tr>"
		+"                                                                            </thead>"
		+"                                                                <tbody class='no-bd-y' id='lrViewSearch'>"
		+"                                                                </tbody>"
		+"                                                                            </table>"
		+"                                                    </div>"
		+"                                                        <div id='pagination' class='pagination'></div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	//showGetPassLRList(data);
	commonsearchViewGetPassLRListController("","","","",1);
	subTabsForOutWardGetPass(false);
//}, 'json');
}
	
	function showGetPassLRList(data,from,url)
	{SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
		var i=0;
		var html=""
			var pageCount;
		//comonData is global var
		commonData = data;
		$(data).each(function(index, element) {
			pageCount=element.paginationCount;
			if((data.length-2)<i)
				return false;
			html+="<tr style='text-align: center;'>"
				+"<td>"+ SR_NO +"</td>"
				+"<td id='TRIP_NO_"+i+"'>"+element.TRIP_NO+"</td>"
				+"<td id='ORG_NAME_"+i+"'>"+element.ORG_NAME+"</td>"
				+"<td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
				+"<td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
				+"<td id='TRAN_"+i+"'>"+element.TRAN+"</td>"
				+"<td id='INVOICE_NO_"+i+"'>"+element.INVOICE_NO+"</td>"
				+"<td id='NO_OF_CASES_"+i+"'>"+element.NO_OF_CASES+"</td>"
				+"<td id='DATE"+i+"'>"+element.DATE+"</td>"
				+"<td id='LR_NO"+i+"'>"+element.LR_NO+"</td>"
				+"<td><img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' style='cursor: pointer' onClick='showOutwardAddLR_LrImagesListPopup("+i+")'>"
				//+"<td><a class='edit btn btn-blue' href='#' onclick=\"viewLRDetails('"+element.OUTWARD_GATEPASS_ID+"','"+element.LR_NO+"','"+element.LR_IMAGE_PATH+"')\"><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark' href='#' onclick=\"editLRDetails('"+element.OUTWARD_GATEPASS_ID+"','"+element.LR_NO+"','"+element.LR_IMAGE_PATH+"')\"><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteLRDetails('"+element.OUTWARD_GATEPASS_ID+"')\"><i class='fa fa-times-circle'></i> </a></td>"
				+"<td><a class='edit btn btn-blue' href='#' onclick='viewLRDetails("+i+")'><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark' href='#' onclick='editLRDetails("+i+")'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' href='#' onclick=\"deleteLRDetails('"+element.OUTWARD_GATEPASS_ID+"')\"><i class='fa fa-times-circle'></i> </a></td>"
//				+"<td style='width: 252px;'>"
//				+"<a class='btn btn-info' href='#' onClick='printSticker("+element.OUTWARD_DISPATCH_ENTERY_REG_ID+")'>Print Sticker</a></td>"
				+"</tr>";
			i++;SR_NO++;
		});
		$('#lrViewSearch').append(html);
		$('#loader').hide();
		paginationViewMore(from,pageCount,url);	
}
	
	function searchViewGetPassLRList()
	{  
		$('#lrViewSearch').html('');
		var selectedOption = $('#searchOption').val();
		var fromSpecialData=null;
		var toSpecialData=null;
		var searchText=null;
		
		if(selectedOption=="NoOfCases")
		{
			 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
			 {
				 alert("it is not a number");
				 return false;
			 }
			 else
			 {
				 	fromSpecialData=$('#inputFromData3').val();
					toSpecialData=$('#inputTOData3').val();
			 }	
		}
		else
		{
			if(!(Boolean($('#searchText').val())))
			{
				alert("Please Enter the search text");
				return false
			}
			searchText=$('#searchText').val();
		}
		commonsearchViewGetPassLRListController(selectedOption,searchText,fromSpecialData,toSpecialData,1);
	}
	
	function commonsearchViewGetPassLRListController(selectedOption,searchText,fromSpecialData,toSpecialData,from)
		{
		$.post(contextApplicationPath+'/OutWardGoodsController/searchViewGetPassLRList',
			{
			selectedValue : selectedOption, searchText : searchText, fromSpecialData : fromSpecialData, toSpecialData : toSpecialData,from:from
		}, function(data) {
			var url = "commonsearchViewGetPassLRListController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showGetPassLRList(data,from,url); // to call the function which shows the searched item
	}, 'json');
}

////////Vijay
	function ViewOrderEntryRegistrationList()
	{ $('#loader').show();
	$('#loader').show();
	var i=1;
	//$.post(contextApplicationPath+'/OutWardGoodsController/getOutWardOrderEntryRegistrationListOfStatusOne', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_7'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>View Order Entry Registration</strong></h3>"
		+"                                    </div>"
		+"<div id='subTabsForOrderEntryRegistration'></div>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search By</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"																<select id='searchOption' class='form-control' class='form-control' onchange='changeFunctionOutward();'>"
		+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+"																	<option value='Organization'>Select Organization</option>"
		+ "                                                      		 	<option value='Company'>Comapny Name</option>"
		+ "                                                     		  	<option value='Date'>Search By Date</option>"
		//+ "                                                     		  	<option value='Stockist'>Stockist</option>"
		+ "                                                     		  	<option value='OrderId'>Order Id</option>"
		+"																	<option value='OrderNo'> Order Number</option>"
		//+ "                                                    			   	<option value='OrderMode'> Order Mode</option>"
		+"																</select>"	
		//+"                                                                    <input type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Search For</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls' id='searchControl'>"
		+"                                                                    <input id='searchText' type='text' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                            <div class='pull-right' style='padding-left: 10px;'>"
		+"                                                                <button class='btn btn-success m-b-10' onclick='searchOutwardViewOrderEntryRegistration()'>Show</button>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='pull-right'>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                                        <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd' id='outList'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Oranization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Date</strong>"
		+"                                                                        </th>"
//		+"                                                                        <th style='text-align: center;'><strong>Order Mode</strong>"
//		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Add Product</strong></th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong></th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y' id='search'>"
		+"                                                                </tbody>"
		+"																	</table>"	
		+"                                                        </div>"
		+"                                                          <div id='pagination' class='pagination'></div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	//showOrderEntryRegistrationList(data);
	commonOutwardViewOrderEntryRegistrationController("","","","",1);
	subTabsForOutWardGoodsReg(3);
	//}, 'json');
	//}, 'json');
	}

//////// Vijay
function showOrderEntryRegistrationList(data,from,url)
{   
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var i=0;
	var html=""
		var pageCount;
	/*	+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
		+"                                                                        <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order ID</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Order No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Oranization</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Date</strong>"
		+"                                                                        </th>"
//		+"                                                                        <th style='text-align: center;'><strong>Order Mode</strong>"
//		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Add Product</strong></th>"
		+"                                                                        <th style='text-align: center;'><strong>Action</strong></th>"
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";*/
	
	$(data).each(function(index, element) {
		pageCount=element.paginationCount;
		if((data.length-2)<i)
			return false;
			html+="<tr style='text-align: center;' id='OutWardOrderEntry_"+element.OutWardOrderEntryRegistrationId+"'>"
				+" <td>"+SR_NO+"</td>"
				+" <td id='ORDER_ID_"+i+"'>"+element.ORDER_ID+"</td>"
				+" <td id='ORDER_NO_"+i+"'>"+element.ORDER_NO+"</td>"
				+" <td id='ORG_"+i+"'>"+element.ORG+"</td>"
				+" <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
				+" <td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
				+" <td id='DATE_"+i+"'>"+element.DATE+"</td>"
//				+" <td >"+element.ORDER_MODE+"</td>"
				+"<td><button class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive5' onclick=\"AddOrderProductInOrder('"+element.OutWardOrderEntryRegistrationId+"','"+element.COMPANY_ID+"','"+element.ORDER_NO+"','FromMoreProduct')\">+</button></td>"
				+"<td><a class='edit btn btn-blue' href='#' onclick='viewOrderEntryRegistration("+element.OutWardOrderEntryRegistrationId+")'><i class='fa fa-external-link'></i></a>  <a class='edit btn btn-dark' href='#' onclick='editOrderEntryRegistration("+element.OutWardOrderEntryRegistrationId+")'><i class='fa fa-pencil-square-o'></i></a>";
				if(element.INVOICE_STATUS==0)
					{
					html+="<a class='delete btn btn-danger' href='#' onclick='deleteOrderEntryRegistration("+element.OutWardOrderEntryRegistrationId+")'><i class='fa fa-times-circle'></i> </a>";
					}
				html+="</td>"
				+" </tr>";
			i++;SR_NO++;
		});
		
		
		$('#search').append(html);
		$('#loader').hide();
		paginationViewMore(from,pageCount,url);
}

////// Vijay
function searchOutwardViewOrderEntryRegistration(){
	$('#search').html('');
	var selectedOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	
	if (selectedOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonOutwardViewOrderEntryRegistrationController(selectedOption,searchText,fromSpecialData,toSpecialData,1)
}

	function commonOutwardViewOrderEntryRegistrationController(selectedOption,searchText,fromSpecialData,toSpecialData,from)
	{
		$.post(contextApplicationPath+'/OutWardGoodsController/searchOutwardViewOrderEntryRegistration',
				{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData,from:from
			}, function(data) {
				var url = "commonOutwardViewOrderEntryRegistrationController(\""+selectedOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
				showOrderEntryRegistrationList(data,from,url); // to call the function which shows the searched item
			//$('#searchResult').html(html);
		}, 'json');
}

function viewTripEntryDetails(tripEntryId,tripNo,noOfCases,vehicleNo,dispatchBy,loadingCharges,driverNo,cartingAgent,orgnization)
{
	var i=1;
	var html="<div class='modal fade' id='modal-responsive' tabindex='-1' role='dialog' aria-labelledby='myLargeModalLabel' aria-hidden='true'>"
		+"                                                            <div class='modal-dialog modal-lg'>"
		+"                                                                <div class='modal-content'>"
		+"                                                                    <div class='modal-header'>"
		+"                                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true' onclick='hidePopUp()'>&times;</button>"
		+"                															  <div class='col-md-5'>"
		+"                                                                        			<h4 class='modal-title' id='myModalLabel'>Gate Pass Details</h4>"
        +"            	  															  </div>"
		+"                															  <div class='col-md-5'>"
		+"                                                                        			<h4 class='modal-title' id='myModalLabel'>"+orgnization+"</h4>"
        +"            	  															  </div>"
        +"				  														  </br>"
		+"                                                                    </div>"
		+"            <div class='modal-header'>"
		+"                <div class='col-md-4' style='width:33%;'>"
        +"                     <h5 class='modal-title' id='myModalLabel'>Trip No :- "+tripNo+"</h5>"
        +"            	  </div>"
		+"                <div class='col-md-4' style='width:33%;'>"
        +"                     <h5 class='modal-title' id='myModalLabel'>Total Trip Cases :- "+noOfCases+"</h5>"
        +"            	  </div>"
		+"                <div class='col-md-4' style='width:33%;'>"
        +"                     <h5 class='modal-title' id='myModalLabel'>Carting Agent :- "+cartingAgent+"</h5>"
        +"            	  </div>"
        +"				  </br>"
    	+"            </div>"
		+"            <div class='modal-header'>"
		+"                <div class='col-md-4' style='width:33%;'>"
        +"                     <h5 class='modal-title' id='myModalLabel'>Vehical No :- "+vehicleNo+"</h5>"
        +"            	  </div>"
		+"                <div class='col-md-4' style='width:33%;'>"
        +"                     <h5 class='modal-title' id='myModalLabel'>Dispatch By :- "+dispatchBy+"</h5>"
        +"            	  </div>"
		+"                <div class='col-md-4' style='width:33%;'>"
        +"                     <h5 class='modal-title' id='myModalLabel'>Driver No :- "+driverNo+"</h5>"
        +"            	  </div>"
        +"				  </br>"
        +"            </div>"
		+"            <div class='modal-header'>"
		+"                <div class='col-md-4' style='width:33%;'>"
        +"                     <h5 class='modal-title' id='myModalLabel'>Loading Charges :- "+loadingCharges+"</h5>"
        +"            	  </div>"
        +"				  </br>"
    	+"            </div>";
	html+="                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <table class='table table-striped table-hover'>"
		+"                                                                <thead class='no-bd'>"
		+"                                                                    <tr>"
//		+"                                                                        <th style='text-align: center;'><strong>Transporter</strong>"
//		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Date</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Stockist Name</strong>"
		+"                                                                        </th> "
		+"                                                                        <th style='text-align: center;'><strong>Invoice No</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Payble Amount</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>Company Name</strong>"
		+"                                                                        </th> "
		+"                                                                        <th style='text-align: center;'><strong>Cases</strong>"
		+"                                                                        </th>"
		+"                                                                        <th style='text-align: center;'><strong>LR Number</strong>"
		+"                                                                        </th> "
		+"                                                                    </tr>"
		+"                                                                </thead>"
		+"                                                                <tbody class='no-bd-y'>";
	
	$.post(contextApplicationPath+'/OutWardGoodsController/ViewTripDetails', {tripEntryId : tripEntryId},function(data) {
		data.sort(SortByName);
	    var flag=1;
	    var temp;
	    var temp1=0;
	    var totalcases=0;
		$(data).each(function(index, element) {
			if(flag==1)
				{
					temp=element.TRANSPORTER;
				}
			if(temp!=element.TRANSPORTER || flag==1)
				{
				if(temp1!=0)
					{
					html+="<tr style='text-align: center;'><td style='background: #EAEAEA;'><h5 class='modal-title'><strong>TOTAL CASES : </strong></h5></td><td style='background: #EAEAEA;'><strong>"+totalcases+"</strong></td></tr>";
					totalcases=0;
					}
					html+="<tr><td COLSPAN=8 style='text-align: center;background: #EAEAEA;'> <div style='text-align:center; padding: 1%;'><h5 class='modal-title' id='myModalLabel'><strong>Transporter : "+element.TRANSPORTER+"</strong></h5></div></td></tr>";
					flag=0;
				}
			temp=element.TRANSPORTER;
			html+="<tr style='text-align: center;'>"
			+"<td>"+ element.TRIP_DATE +"</td>"
			+"<td>"+element.STOCKIST+"</td>"
			var invoiceDetails=element.INVOICE_DETAILS;
			if(invoiceDetails.length > 0)
				{
				html+="<td>";
				 $(invoiceDetails).each(function(index,element){
					html+=element.INVOICE_NO+",";
						 });
				 html+="</td>";
				}
			totalcases=totalcases+element.NO_OF_CASES;
			html+="<td>"+element.PAYBLE_AMOUNT+"</td>"
			+"<td>"+element.COMPNAY+"</td>"
			+"<td>"+element.NO_OF_CASES+"</td>"
			+"<td>-</td>"
			+"</tr>";
			temp1=1;
		});
		
html+="<tr style='text-align: center;'><td style='background: #EAEAEA;'><h5 class='modal-title'><strong>TOTAL CASES : </strong></h5></td><td style='background: #EAEAEA;'><strong>"+totalcases+"</strong></td></tr>";		
html+="                                                                </tbody>"
+"                                                            </table>"
+"                                                        </div>"
+"                                                    </div>"
+"                                                </div>";


html+="                                                                    <div class='modal-footer'>"
+"                                                                        <button type='button' class='btn btn-danger' data-dismiss='modal' onclick='hidePopUp()'>Close</button>"                                                                                   
+"                                                                    </div>"
+"                                                                </div>"
+"                                                            </div>"
+"                                                        </div>";
$('#popup').html(html);
$("#modal-responsive").addClass("in");
$("#modal-responsive").attr("aria-hidden","false");
$("#modal-responsive").css("display","block");
	}, 'json');
}

function SortByName(x,y) {
    return ((x.TRANSPORTER == y.TRANSPORTER) ? 0 : ((x.TRANSPORTER > y.TRANSPORTER) ? 1 : -1 ));
  }

function printTripEntry(OUTWARD_TRIP_ID)
{
//	alert("OUTWARD_TRIP_ID==="+OUTWARD_TRIP_ID);
//		window.open(' "'+contextApplicationPath+'"/OutWardGoodsController/PrintTripDetailsReport?outwardTripId="'+OUTWARD_TRIP_ID+'" ');
		window.location.href = contextApplicationPath+'/OutWardGoodsController/PrintTripDetailsReport?outwardTripId='+OUTWARD_TRIP_ID+''
}



function viewLRDetails(index)
{	var i=0;
	var html="" 
+ "													<div class='modal fade in' id='modal-responsive' >"
+ "    														<div class='col-md-13'>"
+ "     														   <div class='modal-content'>"
+ "       																	     <div class='modal-header'>"
+ "           																		     <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
+ "           																			     <h4 class='modal-title' id='myModalLabel'><strong>View LR Details</strong></h4>"
+ "           																	 </div>"
+ "  															  <div class='panel panel-default'>"
+ "																 <div class='panel-body'>"
+ "   											   		 				<div class='row'>"
+ "     																	  <div class='col-md-4'>";
											html+= "          					  <div class='form-group'>"
+ "                																			<label class='form-label'><strong>LR No : - </strong>"
+ "             																			  </label>"
+ "              																			  <span class='tips'></span>"
+ "              																				  <div class='controls'>"
+ "             																			   <input class='dateClassCommon form-control' disabled value='"+commonData[index].LR_NO+"' type='text'>"
+ "              																  </div>"
+ "          															 </div>";
						html+= "						        </div>"
+"          												    <div class='form-group'>"
+"         																			   <label class='form-label'><strong>Order Copy : - </strong>"
+"																							<span class='tips'></span>"
+"            																	  <div id='imgDiv'>"
+"           																		   <img onClick='showOutwardAddLR_LrImagesListPopup("+index+")' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"//<img onClick='showImagePopup(\""+LRImagePath+"\")' src='"+LRImagePath+"' alt='Smiley face' height='100' width='100'>"
+"            																	  </div>"
+"           																		   </label>"
+"          																		    <span class='tips'></span>"
//					+"              <input type='file' id='ORDER_FILE'  name='orderCopy' class='form-control'>"
+"         																			     </label>"
+"         													     </div>"


  html+= "        										   <div class='modal-header'>"
+ "              												  <h4 class='modal-title' id='myModalLabel'><strong>ORDER INFORMATION</strong></h4>"
+ "           											   </div>"
+ "			    													   <div class='panel-body'>"			
+"																					<div class='row'>"
+"														  <div class='col-md-4'>"
+"                                        					  <div class='form-group'>"
+"                                                                <label class='form-label'><strong>ORGANIZATION NAME : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                                <label class='form-label'><strong>"+commonData[index].ORG_NAME+"</strong>"
+"                                                                </label>"
+"                                                            </div>"                                                            
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>COMPANY NAME : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                                <label class='form-label'><strong>"+commonData[index].COMPANY_NAME+"</strong>"
+"                                                                </label>"
+"                                                            </div>"                                                            
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>ORDER NAME : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                                <label class='form-label'><strong>"+commonData[index].ORDER_NAME+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+ "          <div class='form-group'>"
+ "               <label class='form-label'><strong>Date : - </strong>"
+ "               </label>"
+ "				 <label class='form-label'><strong>"+commonData[index].ORDER_DATE+"</strong>"
+ "                </label>"
+ "               <span class='tips'></span>"
+ "            </div>"
+"                                                        </div>"
+"														  <div class='col-md-4'>"
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>STOCKIST NAME : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                               <label class='form-label'><strong>"+commonData[index].STOCKIST_NAME+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+ "            <div class='form-group'>"
+ "                <label class='form-label'><strong>ORDER NO : - </strong>"
+ "               </label>"
+ "				  <label class='form-label'><strong>"+commonData[index].ORDER_NO+"</strong>"
+ "                </label>"
+ "           </div>"

	+"            <div class='form-group'>"
	+"            <label class='form-label'><strong>Order Copy : - </strong>"
	+"				<span class='tips'></span>"
	+"              <div id='imgDiv'>"
	+"              <img onClick='showOutwardOrderCopyImagesListPopup2("+index+")' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"//<img onClick='showImagePopup(\""+element.ORDER_COPY+"\")' src='"+element.ORDER_COPY+"' alt='Smiley face' height='100' width='100'>"
	+"              </div>"
	+"              </div>";
  html+= "       </div>"
+"						</div>"
+ "			   </div>"
/*var k=1;
var invoiceInfo=commonData[index].CHECK
	if(invoiceInfo.length > 0)
		{
+ "                 <div class='modal-header'>"
+ "                     <h4 class='modal-title' id='myModalLabel'><strong>Invoice Information :</strong></h4>"
+ "            	   	</div>"	
+"						<div class='row'>"
+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
+"								<table class='table table-striped table-hover'>"
+"									<thead class='no-bd'>"
+"										<tr>"
+"											<th style='text-align: center;'><strong>Sr No.</strong>"
+"											</th>"
+"											<th style='text-align: center;'><strong>INVOICE No</strong>"
+"											</th>"
+"											<th style='text-align: center;'><strong>INVOICE DATE</strong>"
+"											</th>"
+"											<th style='text-align: center;'><strong>GROSS AMOUNT</strong>"
+"											</th>"
+"											<th style='text-align: center;'><strong>TAX AMOUNT</strong>"
+"											</th>      "
+"											<th style='text-align: center;'><strong>VAT AMOUNT</strong>"
+"											</th>"
+"											<th style='text-align: center;'><strong>DISCOUNT</strong>"
+"											</th>"
+"											<th style='text-align: center;'><strong>NET AMOUNT</strong>"
+"											</th>"
+"										</tr>"
+"									</thead>";
$(invoiceInfo).each(function(){

	html+="                                    <tr style='text-align: center;'>"
+"                                         <td>"+ k++ +"</td>"
+"                                         <td >"+commonData[index].INVOICE_NO+"</td>"
+"                                         <td >"+commonData[index].DATE+"</td>"
+"                                         <td>"+commonData[index].GROSS_AMT+"</td>"
+"                                         <td>"+commonData[index].TAX_AMT+"</td>"
+"                                         <td>"+commonData[index].VAT_AMT+"</td>"
+"                                         <td>"+commonData[index].DISCOUNT+"</td>"
+"                                         <td>"+commonData[index].NET_AMT+"</td>"
+"											</td>"
+"										</tr>";
});k++;
*/

var k=1;
var invoiceInfo=commonData[index].INVOICE_VIEW;
	if(invoiceInfo.length > 0)
		{
 html +="                                                <div class='panel-body'>"
   +"                                                    <div class='row'>"
    + "                                                      <h5 class='modal-title' id='myModalLabel'><strong>Invoice Information</strong>"
    + "            									        </h5>"
    +"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
    +"                                                            <table class='table table-striped table-hover'>"
    +"                                                                <thead class='no-bd'>"
	+"                                                                    <tr>"
	+"											<th style='text-align: center;'><strong>Sr No.</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>INVOICE No</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>INVOICE DATE</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>GROSS AMOUNT</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>TAX AMOUNT</strong>"
	+"											</th>      "
	+"											<th style='text-align: center;'><strong>VAT AMOUNT</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>DISCOUNT</strong>"
	+"											</th>"
	+"											<th style='text-align: center;'><strong>NET AMOUNT</strong>"
	+"											</th>"
	+"                                                                    </tr>"
	+"                                                                </thead>"
	+"                                                                <tbody class='no-bd-y'>";

 $(invoiceInfo).each(function(index,element){
html+="                                                                    <tr style='text-align: center;'>"
	+"                                         <td>"+ k++ +"</td>"
	+"                                         <td >"+element.INVOICE+"</td>"
	+"                                         <td >"+element.IN_DATE+"</td>"
	+"                                         <td>"+element.GROSS_AMT+"</td>"
	+"                                         <td>"+element.TAX_AMT+"</td>"
	+"                                         <td>"+element.VAT_AMT+"</td>"
	+"                                         <td>"+element.DISCOUNT+"</td>"
	+"                                         <td>"+element.NET_AMT+"</td>"
+"                                                                    </tr>"
 });
html+="                                                                </tbody>"
+"                                                            </table>"
+"                                                        </div>"
+"                                                    </div>"
+"                                                </div>";
		}


	
   html+= "           <div class='modal-header'>"
+ "                <h4 class='modal-title' id='myModalLabel'><strong>View Outward Order Dispatch Entry Registration Details</strong></h4>"
+ "            </div>"
+ "			       <div class='panel-body'>"			
+"						<div class='row'>"
+"														  <div class='col-md-4'>"
+"                                        					  <div class='form-group'>"
+"                                                                <label class='form-label'><strong>ORGANIZATION NAME : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                                <label class='form-label'><strong>"+commonData[index].ORG_NAME+"</strong>"
+"                                                                </label>"
+"                                                            </div>"                                                            
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>COMPANY NAME : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                                <label class='form-label'><strong>"+commonData[index].COMPANY+"</strong>"
+"                                                                </label>"
+"                                                            </div>"                                                            
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>STOCKIST : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                                <label class='form-label'><strong>"+commonData[index].STOCKIST+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+"                                                        </div>"
+"														  <div class='col-md-4'>"
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>NO OF CASES : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                               <label class='form-label'><strong>"+commonData[index].NO_OF_CASES+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>SUBMITTED DATE: - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                               <label class='form-label'><strong>"+commonData[index].DATE+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+"                                                        </div>"
+"						</div>"
+ "			   </div>";

   html+= "           <div class='modal-header'>"
+ "                <h4 class='modal-title' id='myModalLabel'><strong>Gate Pass Details</strong></h4>"
+ "            </div>"
+ "			       <div class='panel-body'>"			
+"						<div class='row'>"
+"														  <div class='col-md-4'>"
+"                                        					  <div class='form-group'>"
+"                                                                <label class='form-label'><strong>TRIP_NO : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                                <label class='form-label'><strong>"+commonData[index].TRIP_NO+"</strong>"
+"                                                                </label>"
+"                                                            </div>"                                                            
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>VEHICAL NO : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                                <label class='form-label'><strong>"+commonData[index].VEHICAL_NO+"</strong>"
+"                                                                </label>"
+"                                                            </div>"                                                            
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>LOADING CHARGES : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                                <label class='form-label'><strong>"+commonData[index].LOADING_CHARGES+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+"                                                        </div>"
+"														  <div class='col-md-4'>"
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>TOTAL TRIP CASES : - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                               <label class='form-label'><strong>"+commonData[index].TRIP_CASE+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>DISPATCHED BY: - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                               <label class='form-label'><strong>"+commonData[index].DISPATCH_BY+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>CARTING AGENT: - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                               <label class='form-label'><strong>"+commonData[index].CARTING+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+"                                                            <div class='form-group'>"
+"                                                                <label class='form-label'><strong>DRIVER NO: - </strong>"
+"                                                                </label>"
+"                                                                <span class='tips'></span>"
+"                                                               <label class='form-label'><strong>"+commonData[index].DRIVER_NO+"</strong>"
+"                                                                </label>"
+"                                                            </div>"
+"                                                        </div>"
+"						</div>"
html+="            <div class='modal-footer text-center'>"
	   + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
	   + "            </div>"
+ "			   </div>"
    + " </div>"
+ " </div>"
+ " </div>"
+ "</div>";
/*html+="            <div class='modal-footer text-center'>"
+ "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
+ "            </div>"*/
+ "        </div>"
+ "    </div>"
+ "</div>"
$('#popup').html(html);
$("#modal-responsive").addClass("in");
$("#modal-responsive").attr("aria-hidden","false");
$("#modal-responsive").css("display","block"); 
}


function showOutwardOrderCopyImagesListPopup2(index){
	showListOfImagesPopup(commonData[index].ORDER_COPY);
}


function deleteLRDetails(OUTWARD_GATEPASS_ID)
{
	var r = confirm("Do you want to Delete LR Details Entry...!");
  if (r == true) 
  {
  	$.post(contextApplicationPath+'/OutWardGoodsController/deleteLrDetails',{OUTWARD_GATEPASS_ID : OUTWARD_GATEPASS_ID},
  			function(data) {
  				$(data).each(function(index, element) {
  					viewGetPassLRList();
  					alert(element.MSG);
  				});
  			}, 'json');
  }
}

//
function showOutwardAddLR_LrImagesListPopup(index){
	showListOfImagesPopup(commonData[index].LR_IMAGE_PATH);
}
