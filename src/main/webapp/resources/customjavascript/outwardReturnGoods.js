/**Global VAriables*/
//Global Variable for LR Form
var LR_FORM_COUNTER;
var LR_NO_ARR=[];
var NO_OF_CASES_ARR=[];
var REF_OR_CLAIM_NO_ARR=[];
var CLAIM_AMOUNT_ARR=[];
var OPENED_LR_FORM_TAB;
var LR_FORM_EDIT_STATUS=false;

//for credit note view details
var CreditNoteListArray;
//Global var for Add Product Product
//var PRODUCT_MULTIARRAY ;
//var POPUP_COUNTER;
//var PRODUCT_EDIT_STATUS ;
//currnetly opened popup
//var OPENED_PRODUCT_TAB;
//for detail view of product
//var PRODUCT_LIST_ARRAY;

function AddOutwardReturnGoodsReg() {
	var html = ""
			+ "<div class='page-title'> <i class='icon-custom-left'></i>"
			+ "                    <h3><strong>Outward Return Goods Registration</strong></h3>"
			+ "                </div>"
			+ "                <div class='row'>"
			+ "                    <div class='col-md-6'>"
			+ "                        <div class='tabcordion'>"
			+ "                            <ul id='myTab' class='nav nav-tabs nav-dark'>"
			+ "                                <li class='active'><a href='#tab1_1' data-toggle='tab' onclick='outwardReturnGoodsRegistration()'>Registration</a></li>"
			+ "                                <li class=''><a href='#tab1_2' data-toggle='tab' onclick='outwardReturnGoodsRegListing()'>Registration View</a></li>"
			+ "                                <li class=''><a href='#tab1_3' data-toggle='tab' onclick='addOutwardReturnGoodsRegCreditNote()'>Credit Note</a></li>"
			+ "                            </ul>"
			+ "                            <div id='myTabContent' class='tab-content'>"
			+ "                            </div>"
			+ "                        </div>" + "                    </div>"
			+ "                </div>";
	$('#main-content').html(html);
	$('#subMenu' + lastClickId).hide();
	outwardReturnGoodsRegistration();
}

function outwardReturnGoodsRegistration(){
	//RESET GLOBLE VARIABLE
	LR_FORM_COUNTER=0;
	LR_NO_ARR=[];
	NO_OF_CASES_ARR=[];
	REF_OR_CLAIM_NO_ARR=[];
	CLAIM_AMOUNT_ARR=[];
	
	//PRODUCT_MULTIARRAY = new Array();
	//PRODUCT_EDIT_STATUS = false;
	//OPENED_PRODUCT_TAB="";
	//POPUP_COUNTER = 0;
	
	var html=""
		+"								<form id='saveOutwardReturnGoodsReg' action='"+contextApplicationPath+"/OutwardReturnGoodsRegistrationController/saveOutwardReturnGoodsRegistration' method='post' enctype='multipart/form-data'>"
		+"<div class='page-title'> <i class='icon-custom-left'></i>"
		+"                    <h3><strong>Outward Return Goods Registration</strong></h3>"
		+"                </div>"
		+"                <div class='row'>"
		+"                    <div class='cal1'>"
		+"                        <div class='col-md-6'>"
		+"                            <div class='panel panel-default'>"
		+"                                <div class='panel-body'>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12 col-sm-12 col-xs-12'>"
		/*+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>ID No</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' placeholder='ID No' class='form-control'>"
		+"                                                </div>"
		+"                                            </div>"*/
		+"                                                            	  <div id='selectOrganization' class='form-group'>"
		+"                                                            	  </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Date</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input class='dateClassCommon form-control' type='text' id='date' name='regDate' placeholder='Select Date' style='height: 36px; padding-left: 10px;' onchange=\"errorMsgEmpty('date','dateError_msg')\">"
		+"                                                </div>"
		+"												  <div id='dateError_msg' class='validationError' style='color:red'></div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Time</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' id='timePicker' name='time' placeholder='Time' class='form-control' focusout=\"errorMsgEmpty('timePicker','timeError_msg')\">"
		+"                                                </div><div id='timeError_msg' class='validationError' style='color:red'></div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Company</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <select id='companyName' name='companyName'  onchange='getStockist()' class='form-control' required>"
		+"                                                        <option disabled selected> Select Company</option>"
		+"                                                    </select>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Stockist</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <select id='StockistName' name='stockistName' class='form-control'>"
		+"                                                        <option disabled selected> Select Stockist</option>"
		+"                                                    </select>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Driver Name</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"												  	<input type='text' name='driverName' placeholder='Driver Name' class='form-control'>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Delivered By</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <select id='employeeDropDown' name='employeeName'' class='form-control'>"
		+"                                                    	<option disabled selected>Select Employee</option>"
		+"                                                    </select>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Transporter Name</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <select name='transporter' id='transName' class='form-control' class='form-control' required>"
		+"                                                    	<option disabled selected>Select Transporter</option>"
		+"                                                    </select>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Transportation Charges</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='transpCharges' placeholder='Transportation Charges' parsley-type='number' class='form-control'>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Driver No</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='driverNo' placeholder='Driver No' class='form-control'>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Reason</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='outwardReturnReason' placeholder='Reason' class='form-control'>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>"
		+"                            </div>"
		+"                        </div>"
		+"                    </div>"
		+"                    <div class='cal1'>"
		//
		+"						  <div id='lrForm'></div>"
		+"						  <div id='lrFormEntryListing'></div>"
		//
		+"                    </div>"
		+"                </div>"
		+"                <div class='row'>"
		+"                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                <div class='pull-right'>"
		+"                                                    <button class='btn btn-primary m-b-10' onclick='return validateOutwardsReturnGoodsForm(\"#saveOutwardReturnGoodsReg\")'>Save</button>"
		/*+"                                                    <button class='btn btn-success m-b-10' onclick='javascript:$('#form1').parsley('validate');'>Save & Mail</button>"*/
		+"                                                    <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                </div>"
		+"							</form>";
	$('#myTabContent').empty();
	$('#myTabContent').html(html);
	$(".dateClassCommon").datepicker({
		changeMonth : true,
		changeYear : true,
		yearRange : "-100:+0"
	});
	$('#timePicker').timepicki();
	ajaxOutwardReturnGoodsRegistrationForm("saveOutwardReturnGoodsReg");
	LRFormGeneration();
	displayOrgDropDown(1);
}


function validateOutwardsReturnGoodsForm(formId)
{
	var z = $(formId).parsley('validate');
	var flag =  0;
	if($("#timePicker").val()==""||$("#timePicker").val()==undefined||$("#date").val()==""||$("#date").val()==undefined){
		var flag=false;
		if($("#timePicker").val()==""||$("#timePicker").val()==undefined){
			$("#timeError_msg").html("This value is required.");flag=true;
		}
		if($("#date").val()==""||$("#date").val()==undefined){
			$("#dateError_msg").html("This value is required.");flag=true;
		}
		if(flag){
			$('#loader').hide();
			return false;
		}
		return true;
	}

	if(z==true)
		{
		return validate_LR_Form();
//			  false;  'return validateInwardsReturnGoodsForm(\"#saveInwardReturnGoodsReg\")'
		}
	return false;
}



function ajaxOutwardReturnGoodsRegistrationForm(formId){
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/},
			success : function(data) {
				$('#loader').hide();
				if(data.result==true){
					outwardReturnGoodsRegistration();
					alert(data.MSG);
				}
				else{
					alert("Operation Failed");
				}
			}
	};
	$('#saveOutwardReturnGoodsReg').ajaxForm(options);
}
function validate_LR_Form(){
	if(!(NO_OF_CASES_ARR.length>0))
	{
		alert("Please Add atleast One Entry for LR Form.....!");
		return false;
	}else{

		var emptyStat = true; 
		for(var i=0;i<NO_OF_CASES_ARR.length;i++)
		{
			if(!(NO_OF_CASES_ARR[i]==""))
			{
				emptyStat=false;break;
			}
		}
		if(emptyStat)
			{
			alert("Please Add atleast One Entry for LR Form.....!");
			return false;
			}
		else
			{
			showLoader();
			return true;
			}

	}
}
//
function LRFormGeneration(){
	var lrDivId = "lrNo"+LR_FORM_COUNTER;
	OPENED_LR_FORM_TAB = LR_FORM_COUNTER;
	if( $('#'+lrDivId).length ){
		//alert("Already exists");
		$("#"+lrDivId).show();
		LRFormEntryListing();
	}else{
		COUNTER=0;
	var html=""
		+"                        <div id='lrNo"+LR_FORM_COUNTER+"' class='col-md-6'>"
		+"                            <div class='panel panel-default'>"
		+"                                <div class='panel-body'>"
		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12 col-sm-12 col-xs-12'>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>LR No</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='lrNo' id='lrNoVal"+LR_FORM_COUNTER+"' placeholder='LR No' class='form-control' onkeyup =\"lrNoValKeyUp("+LR_FORM_COUNTER+")\">"
		//+ "																	  <div id='lr_No_Error_Msg"+LR_FORM_COUNTER+"' class='validationError' style='color:red'></div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>No of Cases</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='noOfCases' id='noOfCasesVal"+LR_FORM_COUNTER+"' placeholder='No of Cases' class='form-control' onkeyup =\"noOfcasesKeyUp("+LR_FORM_COUNTER+")\" >"
		+ "																	  <div id='noOfCases_Error_Msg"+LR_FORM_COUNTER+"' class='validationError' style='color:red'></div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Ref No/Claim No</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='refOrClaimNo' id='refOrClainNoVal"+LR_FORM_COUNTER+"' placeholder='Ref No/Claim No' class='form-control'  onkeyup =\"refOrClaimNoKeyUp("+LR_FORM_COUNTER+")\">"
		+ "																	  <div id='refOrClainNoVal_Error_Msg"+LR_FORM_COUNTER+"' class='validationError' style='color:red'></div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Claim Amount</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='text' name='claimAmount' id='claimAmountVal"+LR_FORM_COUNTER+"' placeholder='Claim Amount' class='form-control' parsley-type='number' onkeyup =\"claimAmountKeyUp("+LR_FORM_COUNTER+")\">"
		+ "																	  <div id='claimAmountVal_Error_Msg"+LR_FORM_COUNTER+"' class='validationError' style='color:red'></div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Attach Claim Copy</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='file' name='attachClaimCopy' class='form-control' onchange='onClaimCopyImageChange(\"attachClaimCopy"+LR_FORM_COUNTER+"_1\","+LR_FORM_COUNTER+","+false+")' id='attachClaimCopy"+LR_FORM_COUNTER+"_1' >"
		//+ "														<div id='attachClaimCopy_Error_Msg"+LR_FORM_COUNTER+"' class='validationError' style='color:red'></div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div id='claimCopyImageDivId"+LR_FORM_COUNTER+"'></div>"
		+"                                            <div class='form-group'>"
		+"                                            	<div id='claimCopyImageAddMoreButtonId"+LR_FORM_COUNTER+"'><button onclick='addFileConrolForClaimCopyIMG("+LR_FORM_COUNTER+", 1)' class='btn btn-primary'>Add More Files</button></div>"
		+"                                            </div>"
		+"                                            <div class='form-group'>"
		+"                                                <label class='form-label'><strong>Attach LR</strong>"
		+"                                                </label>"
		+"                                                <span class='tips'></span>"
		+"                                                <div class='controls'>"
		+"                                                    <input type='file' name='attachLr' class='form-control' onchange='onLrImageChange(\"attachLr"+LR_FORM_COUNTER+"_1\","+LR_FORM_COUNTER+","+false+")' id='attachLr"+LR_FORM_COUNTER+"_1'>"
		//+ "																	  <div id='attachLr_Error_Msg"+LR_FORM_COUNTER+"' class='validationError' style='color:red'></div>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                            <div id='lrImageDivId"+LR_FORM_COUNTER+"'></div>"
		+"                                            <div class='form-group'>"
		+"                                            	<div id='lrImageAddMoreButtonId"+LR_FORM_COUNTER+"'><button onclick='addFileConrolFor_LR_IMG("+LR_FORM_COUNTER+", 1)' class='btn btn-primary'>Add More Files</button></div>"
		+"                                            </div>"
		+"                                        </div>"
		+"										  <input type='hidden' id='lrFileAttachedCountId"+LR_FORM_COUNTER+"' value='0' name='lrFileAttachedCounter'>"
		+"											<input type='hidden' id='claimCopyFileAttachedCountId"+LR_FORM_COUNTER+"' value='0' name='claimCopyFileAttachedCounter'>"
		+"                                    </div>"
		/*+"                                    <div class='row'>"
		+"                                        <div id='lrNoAddProduct"+LR_FORM_COUNTER+"' class='col-md-3'>"
		+"                                            <input type='button' value='Add Product'  class='btn btn-info m-b-10' onClick='addPoductFormPopup("+LR_FORM_COUNTER+","+POPUP_COUNTER+","+true+")'>"
		+"                                        </div>"
		+"                                    </div>"*/
		/*//
		+"									  <div id='addProductPopUp"+LR_FORM_COUNTER+"'></div>"
		//
		+"   							      <div class='row'>"
		+"                                        <div class='col-md-3'>"
		+"                                            <input type='button' value='Add Credit Note' class='btn btn-info m-b-10' data-toggle='modal' data-target='#modal-responsive"+LR_FORM_COUNTER+"'>"
		+"                                        </div>"
		+"                                    </div>"
		//popup add credit note
		+"                                    <div class='modal fade' id='modal-responsive"+LR_FORM_COUNTER+"' aria-hidden='true'>"
		+"                                        <div class='col-md-13'>"
		+"                                            <div class='modal-content'>"
		+"                                                <div class='modal-header'>"
		+"                                                    <input type='button' class='close' data-dismiss='modal' aria-hidden='true' value='x'>"
		+"                                                    <h4 class='modal-title' id='myModalLabel'><strong>Add Credit Note</strong></h4>"
		+"                                                </div>"
		+"                                                <div class='modal-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-1' class='control-label'>Credit Note No</label></br>"
		+"                                                                <input type='text' id='creditNoteNo"+LR_FORM_COUNTER+"' name='creditNoteNo' class='form-control' id='field-1' placeholder='Credit Note No'>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-2' class='control-label'>Credit Note Amount</label></br>"
		+"                                                                <input type='text' id='creditNoteAmount"+LR_FORM_COUNTER+"' name='creditNoteAmount' class='form-control' id='field-2' placeholder='Credit Note Amount'>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label for='field-3' class='control-label'>Attach Credit Note</label>"
		+"                                                                <input type='file' id='attachCreditNote"+LR_FORM_COUNTER+"' name='attachCreditNote' class='form-control' id='field-3'>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"                                                <div class='modal-footer text-center'>"
		+"                                                    <button type='button' class='btn btn-primary' data-dismiss='modal'>Submit</button>"
		+"                                                    <button type='button' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		//end popup add credit note
*/		+"                                    <div class='row'>"
		+"                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                <div class='pull-right'>"
		+"                                                    <input type='button' value='Add' class='btn btn-primary m-b-10' onclick=\"validateOutwardLrNoDiv('"+LR_FORM_COUNTER+"')\">"
		+"                                                    <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                </div>"
		+"                                            </div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>    "
		+"                            </div>"
		+"                        </div>";
	$('#lrForm').append(html);
	//LRFormEntryListing();
	}
}

function validateOutwardLrNoDiv(LR_FORM_COUNTER)
{
	
	var status=true;
	/*if(!lrNoVal(LR_FORM_COUNTER))
		status=false;*/
	if(!noOfcases(LR_FORM_COUNTER))
		status=false;
	if(!refOrClaimNo(LR_FORM_COUNTER))
		status=false;
	if(!claimAmount(LR_FORM_COUNTER))
		status=false;
	/*if(!attachClaimCopy(LR_FORM_COUNTER))
		status=false;		
	if(!attachLr(LR_FORM_COUNTER))
		status=false;	*/
	if(status)
		submitLR_FORM(LR_FORM_COUNTER);
	else
		return false;
}

//function for LR File control
function addFileConrolFor_LR_IMG(lrFormdivId,index){//stnImageDivId 
	//alert("addFileConrol "+index);
	index++;
	var html="                                                    <div id='lrImageDivId"+lrFormdivId+"_"+index+"' class='form-group'>"
	+"                                                                <div class='controls' style='width: 85%; float: left;'>"
	+"                                                                    <input type='file' class='form-control' name='attachLr' onchange='onLrImageChange(\"attachLr"+lrFormdivId+"_"+index+"\","+lrFormdivId+","+false+")' id='attachLr"+lrFormdivId+"_"+index+"'>"
	+"                                                            	  </div>"
	+"                                                                <div class='controls'>"
	+"                                                				  	  <div><a class='delete btn btn-danger' onclick='onLrImageFileDelete(\"lrImageDivId"+lrFormdivId+"_"+index+"\",\"attachLr"+lrFormdivId+"_"+index+"\","+lrFormdivId+")' style=' margin-left: 3px; width: 55px;height: 39px;'><i class='fa fa-times-circle' style='margin-left: -3px; margin-top: 3px;'></i> </a></div>"
	+"                                                            	  </div>"
	+"                                                            </div>";
	$('#lrImageDivId'+lrFormdivId).append(html);
	html="<button onclick='addFileConrolFor_LR_IMG("+lrFormdivId+","+index+")' class='btn btn-primary'>Add More Files</button>";
	$('#lrImageAddMoreButtonId'+lrFormdivId).html(html);
}
function onLrImageChange(id,lrFormdivId,selectStatus){
	//alert("onLrImageChange id="+id+",lrFormdivId="+lrFormdivId+",selectStatus="+selectStatus);
	var fileVal = $('#'+id).val();
	if(fileVal!=""&&fileVal!=undefined){
		if(!selectStatus){
			$("#"+id).attr("onchange", "onLrImageChange(\""+id+"\","+lrFormdivId+","+true+")");
			var counter = $('#lrFileAttachedCountId'+lrFormdivId).val();
			$('#lrFileAttachedCountId'+lrFormdivId).val(++counter);
		}
	}
	else{
		if(selectStatus){
			$("#"+id).attr("onchange", "onLrImageChange(\""+id+"\","+lrFormdivId+","+false+")");
			var counter = $('#lrFileAttachedCountId'+lrFormdivId).val();
			$('#lrFileAttachedCountId'+lrFormdivId).val(--counter);
		}
	}
	attachLrKeyUp(lrFormdivId);
}
function onLrImageFileDelete(id, imgId, lrFormdivId){
	//alert("onLrImageFileDelete id="+id+",lrFormdivId="+lrFormdivId+",imgId="+imgId);
	var fileData = $('#'+imgId).val();
	if(fileData!="" && fileData!=undefined){
		var counter = $('#lrFileAttachedCountId'+lrFormdivId).val();
		//alert("onLrImageFileDelete Before counter="+counter);
		$('#lrFileAttachedCountId'+lrFormdivId).val(--counter);
		//alert("onLrImageFileDelete after counter="+counter);
	}
	hideSubPopUp(id);
}

//
function submitLR_FORM(formDivId){
	
	//if(PRODUCT_MULTIARRAY [formDivId]!=undefined||PRODUCT_MULTIARRAY [formDivId]!=null){
		//if(PRODUCT_MULTIARRAY [formDivId].length>0){
			//
			//var emptyStat = true; 
			/*for(var i=0;i<PRODUCT_MULTIARRAY [formDivId].length;i++)
			{
				if(PRODUCT_MULTIARRAY [formDivId][i]!=undefined ){
					if(!(PRODUCT_MULTIARRAY [formDivId][i][0]==""))
					{
						emptyStat=false;break;
					}
				}
			}*/
			/*if(emptyStat)
				{
				alert("Please add atleast one Product for LR Form Entry.....!");
				}
			else{*/
				/*var creditNoteNo = $("#creditNoteNo"+formDivId).val();
				var creditNoteAmount = $("#creditNoteAmount"+formDivId).val();
				var attachCreditNote = $("#attachCreditNote"+formDivId).val();
				if(creditNoteNo.trim()==""||creditNoteAmount.trim()==""||attachCreditNote==""){
					alert("Please fill all fields of Add Credit Note....!");
				}
				else{*/
					/*if($("#lrNoVal"+formDivId).val()=="")
					{
						alert("Empty Fields");
						return false;
					}*/
					$('#lrNo'+formDivId).hide();
					if(LR_FORM_EDIT_STATUS==true)
						LR_FORM_EDIT_STATUS=false;
					else
						LR_FORM_COUNTER++;
					
					LRFormGeneration();
					LR_NO_ARR[formDivId] = $("#lrNoVal"+formDivId).val();
					NO_OF_CASES_ARR[formDivId] = $("#noOfCasesVal"+formDivId).val();
					REF_OR_CLAIM_NO_ARR[formDivId] = $("#refOrClainNoVal"+formDivId).val();
					CLAIM_AMOUNT_ARR[formDivId] = $("#claimAmountVal"+formDivId).val();
					LRFormEntryListing();
					//initialization
					//POPUP_COUNTER = 0;
				//}
			//}
		/*}else
			alert("Please add atleast one Product for LR Form Entry.....!");
	}*/
	/*else{
		alert("Please add atleast one Product for LR Form Entry.....!");
	}*/
}
//LR No Form Listing
function LRFormEntryListing(){
	var html = ""
		+"                                                <div class='panel panel-default'>"
		+"                                                    <div class='panel-body'>"
		+"                                                        <div class='row'>"
		+"                                                            <div class='col-md-6'>"
		+"                                                                <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                    <table class='table table-striped table-hover' style=' margin-left: -40px;'>"
		+"                                                                        <thead class='no-bd'>"
		+"                                                                            <tr>"
		+"                                                                                <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>LR No</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>No of Cases</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>Ref No/Claim No</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>Claim Amount</strong>"
		+"                                                                                </th>"
		/*+"                                                                                <th style='text-align: center;'><strong>Attach LR</strong>"
		+"                                                                                </th>"
		+"                                                                                <th style='text-align: center;'><strong>Attach Claim Copy</strong>"
		+"                                                                                </th>"*/
		+"                                                                                <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                </th>"
		+"                                                                            </tr>"
		+"                                                                        </thead>"
		+"                                                                        <tbody class='no-bd-y'>";
																					var rowCount = 0;
																					for (var counter = 0; counter < LR_NO_ARR.length; counter++) {
																						if(NO_OF_CASES_ARR[counter]!=""){
																						html+="<tr style='text-align: center;'>"
																							+" <td>"+(rowCount+1)+"</td>"
																							+" <td>"+LR_NO_ARR[counter]+"</td>"
																							+" <td>"+NO_OF_CASES_ARR[counter]+"</td>"
																							+" <td>"+REF_OR_CLAIM_NO_ARR[counter]+"</td>"
																							+" <td>"+CLAIM_AMOUNT_ARR[counter]+"</td>"
																							+" <td><a class='edit btn btn-dark' onClick='editLr_No_FormEnrty("+counter+")'  href='javascript:;'><i class='fa fa-external-link'></i></a> <a class='delete btn btn-danger' onClick='deleteLr_No_FormEnrty("+counter+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
																							+"</tr>";
																						rowCount++;
																						}
																					}
	html+="                                                                        </tbody>"
		+"                                                                    </table>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div> "
		+"                                                    </div>"
		+"                                                </div>";
	$('#lrFormEntryListing').html(html);
}

//for edit LR FORM
function editLr_No_FormEnrty(entryNo){
	$("#lrNo"+OPENED_LR_FORM_TAB).hide();
	$("#lrNo"+entryNo).show();
	alert("Form Is Ready To Edit");
	LR_FORM_EDIT_STATUS=true;
	OPENED_LR_FORM_TAB = entryNo;
}
//
function deleteLr_No_FormEnrty(entryNo){
	//alert("deleteLrNoFormEnrty");
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$("#lrNo"+entryNo).remove();
		LR_NO_ARR[entryNo] = "";
		NO_OF_CASES_ARR[entryNo] = "";
		REF_OR_CLAIM_NO_ARR[entryNo] = "";
		CLAIM_AMOUNT_ARR[entryNo]="";
		LRFormEntryListing();
    }
}
//addProdPopUpCounter => LR form Id
/*function addPoductFormPopup(addProdPopUpCounter,counter,stat){
	POPUP_COUNTER = counter;
	var popUpId = "popUpId"+addProdPopUpCounter+"_"+counter;
	OPENED_PRODUCT_TAB = counter;
	if( $('#'+popUpId).length ){
		//alert("Already exists");
		$("#"+popUpId).addClass("in");
		 $("#"+popUpId).attr("aria-hidden","false");
		 $("#"+popUpId).css("display","block");
		 addProduct_Listing(addProdPopUpCounter,counter);
	}else{
		var html=""
			+"                                                        <div class='modal fade' id='"+popUpId+"' aria-hidden='true'>"
			+"                                                            <div class='col-md-13'>"
			+"                                                                <div class='modal-content'>"
			+"                                                                    <div class='modal-header'>"
			+"                                                                        <input value='x' type='button' class='close' onClick='hide_PopUp_AddProduct(\""+popUpId+"\","+addProdPopUpCounter+","+counter+")' aria-hidden='true'>"
			+"                                                                        <h4 class='modal-title' id='myModalLabel'><strong>Add Inward Return Product</strong></h4>"
			+"                                                                    </div>"
			+"                                                                    <div class='modal-body'>"
			+"                                                                        <div class='row'>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-1' class='control-label'>Product Name</label>"
			+"                                                                                    <input type='text' id='productName"+addProdPopUpCounter+"_"+counter+"' name='productName' class='form-control' id='field-1' placeholder='Product Name'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-2' class='control-label'>Claim Quantity</label>"
			+"                                                                                    <input type='text' id='claimQuantity"+addProdPopUpCounter+"_"+counter+"' name='claimQuantity' class='form-control' id='field-2' placeholder='Claim Quantity'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-3' class='control-label'>Received Quantity</label>"
			+"                                                                                    <input type='text' id='receivedQuantity"+addProdPopUpCounter+"_"+counter+"' name='receivedQuantity' class='form-control' id='field-3' placeholder='Received Quantity'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-4' class='control-label'>Batch</label>"
			+"                                                                                    <input type='text' id='batch"+addProdPopUpCounter+"_"+counter+"' name='batch' class='form-control' id='field-4' placeholder='Batch'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-5' class='control-label'>MFG Date</label>"
			+"                                                                                    <input id='mfgDate"+addProdPopUpCounter+"_"+counter+"' name='mfgDate' class='dateClassCommon form-control' id='field-5' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-6' class='control-label'>Expiry Date</label>"
			+"                                                                                    <input id='expiryDate"+addProdPopUpCounter+"_"+counter+"' name='expiryDate' class='dateClassCommon form-control' id='field-6' type='text' placeholder='Select Date' style='height: 36px; padding-left: 10px;'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-7' class='control-label'>MFG Company</label>"
			+"                                                                                    <input id='mfgCompany"+addProdPopUpCounter+"_"+counter+"' name='mfgCompany' type='text' class='form-control' id='field-7' placeholder='MFG Company'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-4'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-8' class='control-label'>Reason</label>"
			+"                                                                                    <input type='text' id='reason"+addProdPopUpCounter+"_"+counter+"' name='reason' class='form-control' id='field-8' placeholder='Reason'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                            <div class='col-md-6'>"
			+"                                                                                <div class='form-group'>"
			+"                                                                                    <label for='field-9' class='control-label'>Quantity Variance</label>"
			+"                                                                                    <input type='text' id='quantityVariance"+addProdPopUpCounter+"_"+counter+"' name='quantityVariance' class='form-control' id='field-9' placeholder='Quantity Variance'>"
			+"                                                                                    <input type='hidden' name='lrFormIndex' value='"+addProdPopUpCounter+"'>"
			+"                                                                                </div>"
			+"                                                                            </div>"
			+"                                                                        </div>"
			+"                                                                    </div>"
			+"                                                                    <div class='modal-footer text-center'>"
			+"                                                                        <button type='button' class='btn btn-primary' onClick='submit_AddProduct_Form("+addProdPopUpCounter+","+counter+")'>Add</button>"
			+"                                                                    </div>"
			+"                                                                    <div class='row'>"
			//listing
			+"                                                                        <div id='productListing"+addProdPopUpCounter+"_"+counter+"' class='col-md-6'>"
			+"                                                                        </div>"
			//listing
			+"                                                                    </div>"
			+"                                                                    <div class='modal-footer text-center'>"
			+"                                                                        <input type='button' value='Submit' class='btn btn-primary' onClick='submit_AddProduct(\""+popUpId+"\","+addProdPopUpCounter+","+counter+")'>"
			+"                                                                        <input type='button' value='Cancel' class='btn btn-danger' onClick='hide_PopUp_AddProduct(\""+popUpId+"\","+addProdPopUpCounter+","+counter+")'>"
			+"                                                                    </div>"
			+"                                                                </div>"
			+"                                                            </div>"
			+"                                                        </div>";
		//$('#addProductPopUp').empty();
		 $('#addProductPopUp'+addProdPopUpCounter).append(html);
		 $(".dateClassCommon").datepicker({
				changeMonth : true,
				changeYear : true,
				yearRange : "-100:+0"
			});
		 if(stat){
			 $("#"+popUpId).addClass("in");
			 $("#"+popUpId).attr("aria-hidden","false");
			 $("#"+popUpId).css("display","block");
			 addProduct_Listing(addProdPopUpCounter,counter);
		 }
		 var htmlText = "<input type='button' value='Add Product' class='btn btn-info m-b-10' onClick='addPoductFormPopup("+addProdPopUpCounter+","+POPUP_COUNTER+","+true+")'>";
		 $("#lrNoAddProduct"+addProdPopUpCounter).html(htmlText);
	}
}
function hide_PopUp_AddProduct(popUpId,addProdPopUpCounter,counter){
	var x = addProdPopUpCounter+"_"+counter;
	
	if(PRODUCT_MULTIARRAY.length>0){
		if(PRODUCT_MULTIARRAY[addProdPopUpCounter] != undefined && PRODUCT_MULTIARRAY[addProdPopUpCounter].length>0){
			if(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter]!=undefined ){
				$("#productName"+x).val(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter][0]);
				$("#claimQuantity"+x).val(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter][1]);
				$("#receivedQuantity"+x).val(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter][2]);
				$("#batch"+x).val(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter][3]);
				$("#mfgDate"+x).val(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter][4]);
				$("#expiryDate"+x).val(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter][5]);
				$("#mfgCompany"+x).val(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter][6]);
				$("#reason"+x).val(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter][7]);
				$("#quantityVariance"+x).val(PRODUCT_MULTIARRAY[addProdPopUpCounter][counter][8]);
			}
		}
	}
	 $("#"+popUpId).removeClass("in");
	 $("#"+popUpId).attr("aria-hidden","true");
	 $("#"+popUpId).css("display","none"); 
}
//
function submit_AddProduct(popUpId,addProdPopUpCounter,counter){
	if(PRODUCT_EDIT_STATUS == true){
		PRODUCT_EDIT_STATUS = false;
		submit_AddProduct_Form(addProdPopUpCounter,counter);
	}

	var popUpId = "popUpId"+addProdPopUpCounter+"_"+OPENED_PRODUCT_TAB;
	 $("#"+popUpId).removeClass("in");
	 $("#"+popUpId).attr("aria-hidden","true");
	 $("#"+popUpId).css("display","none"); 
}
//
function submit_AddProduct_Form(addProdPopUpCounter,counter){
	
	var popUpId = "popUpId"+addProdPopUpCounter+"_"+counter;
	var x = addProdPopUpCounter+"_"+counter;

	if($("#productName"+x).val()=="")
	{	
		alert("Empty Fields");
	}
	else{
		$("#"+popUpId).removeClass("in");
		$("#"+popUpId).attr("aria-hidden","true");
		$("#"+popUpId).css("display","none");
		
		if(PRODUCT_MULTIARRAY[addProdPopUpCounter]==null||PRODUCT_MULTIARRAY[addProdPopUpCounter]==undefined)
		{
			PRODUCT_MULTIARRAY[addProdPopUpCounter] = new Array();
		}
		var new_Array = new Array();
		new_Array[0] = $("#productName"+x).val();
		//alert("new_Array[0] = "+new_Array[0]);
		new_Array[1] = $("#claimQuantity"+x).val();
		new_Array[2] = $("#receivedQuantity"+x).val();
		new_Array[3] = $("#batch"+x).val();
		new_Array[4] = $("#mfgDate"+x).val();
		new_Array[5] = $("#expiryDate"+x).val();
		new_Array[6] = $("#mfgCompany"+x).val();
		new_Array[7] = $("#reason"+x).val();
		new_Array[8] = $("#quantityVariance"+x).val();
		
		PRODUCT_MULTIARRAY[addProdPopUpCounter][counter]=new Array();
		PRODUCT_MULTIARRAY[addProdPopUpCounter][counter] = new_Array;
		
		if(PRODUCT_EDIT_STATUS){
			PRODUCT_EDIT_STATUS = false;
		}else{
			//glbal var increment
			POPUP_COUNTER++;
		}
		addPoductFormPopup(addProdPopUpCounter,POPUP_COUNTER,true);
	}
}
//Product Listing
function addProduct_Listing(addProdPopUpCounter,row){
	html=""
		+"                                                                            <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                                                <table class='table table-striped table-hover'>"
		+"                                                                                    <thead class='no-bd'>"
		+"                                                                                        <tr>"
		+"                                                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Product Name</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Claim Quantity</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Received Quantity</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Batch</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>MFG Date</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Expiry Date</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>MFG Company</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Quantity Variance</strong>"
		+"                                                                                            </th>"
		+"                                                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                                                            </th>"
		+"                                                                                        </tr>"
		+"                                                                                    </thead>"
		+"                                                                        <tbody class='no-bd-y'>";
																					var rowCount = 0;
																					for(var i = 0;i<PRODUCT_MULTIARRAY.length;i++){
																						if(i==addProdPopUpCounter){
																						for(var j = 0;j<PRODUCT_MULTIARRAY[i].length;j++){
																							if(PRODUCT_MULTIARRAY[i][j][0]!=""){
																							html+="<tr style='text-align: center;'>"
																								+" <td>"+(rowCount+1)+"</td>"
																								+" <td>"+PRODUCT_MULTIARRAY[i][j][0]+"</td>"
																								+" <td>"+PRODUCT_MULTIARRAY[i][j][1]+"</td>"
																								+" <td>"+PRODUCT_MULTIARRAY[i][j][2]+"</td>"
																								+" <td>"+PRODUCT_MULTIARRAY[i][j][3]+"</td>"
																								+" <td>"+PRODUCT_MULTIARRAY[i][j][4]+"</td>"
																								+" <td>"+PRODUCT_MULTIARRAY[i][j][5]+"</td>"
																								+" <td>"+PRODUCT_MULTIARRAY[i][j][6]+"</td>"
																								+" <td><a class='edit btn btn-dark' onClick='edit_ProductFormEnrty("+i+","+j+","+addProdPopUpCounter+","+j+")'  href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> <a class='delete btn btn-danger' onClick='delete_ProductFormEnrty("+i+","+j+","+addProdPopUpCounter+","+row+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
																								+"</tr>";
																							rowCount++;
																							}
																						}
																						}
																						}
    html+="                                                                        </tbody>"
		+"                                                                                </table>"
		+"                                                                            </div>";
	$('#productListing'+addProdPopUpCounter+"_"+row).html(html);
}

function delete_ProductFormEnrty(i,j,addProdPopUpCounter,row){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
    	PRODUCT_MULTIARRAY[i][j][0]="";
    	addProduct_Listing(addProdPopUpCounter,row);
		var id = i+"_"+j;
		$('#popUpId'+id).remove();
    }
}

function edit_ProductFormEnrty(i,j,addProdPopUpCounter,row){
	//alert("edit_ProductFormEnrty");
	PRODUCT_EDIT_STATUS = true;
	var hidePopIpId =  "popUpId"+addProdPopUpCounter+"_"+OPENED_PRODUCT_TAB;
	var popUpId = "popUpId"+i+"_"+j;
	OPENED_PRODUCT_TAB=j;
	//hide current popup
	 $("#"+hidePopIpId).removeClass("in");
	 $("#"+hidePopIpId).attr("aria-hidden","true");
	 $("#"+hidePopIpId).css("display","none");
	//show edit popup
	$("#"+popUpId).addClass("in");
	$("#"+popUpId).attr("aria-hidden","false");
	$("#"+popUpId).css("display","block");
	//alert(popUpId+"  Edit");
	addProduct_Listing(addProdPopUpCounter,j);
}*/

//Main function to show outward Return GoodsReg 
function outwardReturnGoodsRegListing(){
	$('#loader').show();
	var i = 1 ;
	//alert("viewInwardReturnGoodsReg");
//	$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/outwardReturnGoodsRegListing', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab2_2'>"
		+"                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+"                                        <h3><strong>Outward Return Goods Registration</strong></h3>"
		+"                                    </div>"
		+"             						  <div id='subTabs'></div>"
		+"                                    </div>"
		+"                                    <div class='row'>"
		+ "                    <div class='col-md-12'>"
		+ "                        <div class='panel panel-default'>"
		+ "                            <div class='panel-body'>"
		+ "                                <div class='row'>"
		+ "                                    <div class='col-md-4'>"
		+ "                                        <div class='form-group'>"
		+ "                                            <label class='form-label'><strong>Search By</strong>"
		+ "                                            </label>"
		+ "                                            <span class='tips'></span>"
		+ "                                            <div class='controls'>"
		+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFunction();'>"
		+ "                                                       <option value='Stockist'>Stockist</option>"
		+"														  <option value='Organization'>Select Organization</option>"
		+ "                                                       <option value='Company'>Comapny Name</option>"
		+ "                                                       <option value='Date'>Search By Registration Date</option>"
	//	+ "                                                       <option value='Stockist'>Stockist</option>"
		+ "                                                       <option value='Transporter'>Transporter </option>"
		+"														  <option value='LrNo' >LR No </option>"
		+ "                                                       <option value='ClaimNo'>Claim No </option>"
		+ "                                                       <option value='ClaimAmount'>Claim Amount </option>"
		+ "                                                       <option value='DriverName'>Driver Name</option>"
		+ "                                                       <option value='DeliveredEmployee'>Delivered Employee Name</option>"
		+ "                                                     </select>"
		+ "                                            </div>"
		+ "                                        </div>"
		+ "                                    </div>"//fromDate toDate searchOption searchText
		+ "                                    <div class='col-md-4'>"
		+ "                                        <div class='form-group' id='searchForDiv'>"
		+ "                                            <label class='form-label'><strong>Search For</strong>"
		+ "                                            </label>"
		+ "                                            <span class='tips'></span>"
		+ "                                            <div id='searchControl'>"
		+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+ "                                            </div>"
		+ "                                        </div>"
		+ "                                    </div>"
		+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                        <div class='pull-right'>"
		+ "                                            <button class='btn btn-success m-b-10' onclick='searchoutwardReturnGoodsRegListing()'>Show</button>"
		+ "                                        </div>"
		+ "                                    </div>"
		+"                                                <div class='row'>"
		+"                                                    <div class='col-md-6'>"
		+"                                            <div  id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                            </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
		$('#myTabContent').html(html);
		commonOutwardReturnGoodsRegController("", "", "", "", 1);
//		showoutwardReturnGoodsRegListing(data);//calling another function to show dynamic data 
//	}, 'json');
}
function commonOutwardReturnGoodsRegController(searchOption, searchText, fromSpecialData, toSpecialData, from)
{
	$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/searchOutReturnGoodsRegListing',
			{
		searchOption:searchOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		from : from
			
			}, function(data) {
				var url = "commonOutwardReturnGoodsRegController(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
				showoutwardReturnGoodsRegListing(data, from, url);// to call the function which shows the searched item
	}, 'json');
}

//hello
function searchoutwardReturnGoodsRegListing()
{
	var searchOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	

	if(searchOption=="ClaimAmount")
	{
		 if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }	
	}
	else if (searchOption=="Date")
	{
		if($('#inputTODate').val()>=$('#inputFromDate').val())
		{
			fromSpecialData=$('#inputFromDate').val();
			toSpecialData=$('#inputTODate').val();
		}
		else
		{
			alert('Seleted TO Date should be greater than from Date');
			return false;
		}
		
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	
	commonOutwardReturnGoodsRegController(searchOption, searchText, fromSpecialData, toSpecialData, 1);
//	$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/searchOutReturnGoodsRegListing',
//		{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData
//		
//		}, function(data) {
//			showoutwardReturnGoodsRegListing(data);// to call the function which shows the searched item
//	$('#searchResult').html(html);
//}, 'json');
	
}
function showoutwardReturnGoodsRegListing(data, from, url)
{
	var i = 0 ;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	var html=""
		+"                                                <table class='table table-striped table-hover'>"
		+"                                                    <thead class='no-bd'>"
		+"                                                        <tr>"
		+"                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>ID No</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Company</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Stockist</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Transporter</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                            </th>"
		+"                                                        </tr>"
		+"                                                    </thead>"
		+"                                                    <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="                                                       <tr style='text-align: center;'>"
			+"                                                            <td>"+ SR_NO +"</td>"
			+"                                                            <td id='ID_NO_"+i+"'>"+element.ID_NO+"</td>"
			+"                                                            <td id='ORG_"+i+"'>"+element.ORGANIZATION+"</td>"
			+"                                                            <td id='COMPANY_"+i+"'>"+element.COMPANY+"</td>"
			+"                                                            <td id='STOCKIST_"+i+"'>"+element.STOCKIST+"</td>"
			+"                                                            <td id='TRAN_"+i+"'>"+element.TRAN+"</td>"
			+"  														  <td></div><a class='btn btn-info m-b-10' onClick='viewOutwardReturnGoodsRegDetails("+element.ID_NO+")' href='javascript:;'><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark  m-b-10' onClick='editOutwardReturnGoodsRegDetails("+i+","+element.ID_NO+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> <a class='delete btn btn-danger  m-b-10' onClick='deleteOutwardReturnGoodsReg("+element.ID_NO+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
			+"                                                        </tr>";
		i++;
		SR_NO++;
	}); 
	html+="                                                    </tbody>"
		+"                                                </table>";
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
	$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
	$('#loader').hide();
}


function deleteOutwardReturnGoodsReg(ID_NO){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
    	$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/deleteOutwardReturnGoodsReg', {
    		outwardRetGoodsRegId : ID_NO
    	}, function(data) {
    		outwardReturnGoodsRegListing();
    		alert(data.MSG);
    	}, 'json');
    }
}
//view details of Outward Reg Entry
function viewOutwardReturnGoodsRegDetails(ID_NO){
	$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/outwardReturnGoodsRegDetailsAndLrListing', {
		ID_NO : ID_NO 
			} , function(data) {
				//commonData global var
				commonData = data;
				var i =0;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hidePopUp()' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Outward Return Goods Registration Details</strong></h4>"
			        + "            </div>"
			        + "			       <div class='panel-body'>"
			        + "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
					+"                                        					<div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION ID : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ID_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>ORGANIZATION NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.ORGANIZATION+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>COMPANY NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                               <label class='form-label'><strong>"+data.COMPANY+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>STOCKIST NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.STOCKIST+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTER NAME: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DELIVERED EMPLOYEE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.EMPLOYEE_NAME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REGISTRATION DATE (MM/DD/YYYY): - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.REG_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TIME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.TIME+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>    "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.DRIVER_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>                                             "            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>DRIVER NAME : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.DRIVER+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>TRANSPORTATION CHARGES : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.TRANSPORTATION_CHARGES+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>SUBMIT DATE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.SUBMIT_DATE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>REASON:-</strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.REASON+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+ "						 </div>"
					+ " 			    </div>"
	
					
			
					+"						<div class='row'>"
					+"							<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"								<table class='table table-striped table-hover'>"
					+"									<thead class='no-bd'>"
					+"										<tr>"
					+"											<th style='text-align: center;'><strong>Sr No.</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>LR No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>No of Cases</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim No</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Amount</strong>"
					+"											</th>      "
					+"											<th style='text-align: center;'><strong>LR Copy</strong>"
					+"											</th>"
					+"											<th style='text-align: center;'><strong>Claim Copy</strong>"
					+"											</th>                                           "
					+"											<th style='text-align: center;'><strong>Action</strong>"
					+"											</th>"
					+"										</tr>"
					+"									</thead>"
					+"									<tbody class='no-bd-y'>";
				var orgrLRArr = data.orgrLRArr;
					$(orgrLRArr).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ i+1 +"</td>"
							+"                                         <td >"+element.LR_NO+"</td>"
							+"                                         <td >"+element.NO_OF_CASES+"</td>"
							+"                                         <td>"+element.REF_CLAIM_NO+"</td>"
							+"                                         <td>"+element.CLAIM_AMOUNT+"</td>"
							+"                                         <td>"
							+"												<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showOutwardLrDocImagesListPopup("+i+")'>"//
							+" 										   </td>"
							+"                                         <td>" 
							+"												<img src='../resources/images/fileIcon.png' alt='Smiley face' height='31px' width='31px' onClick='showOutwardClaimCopyDocImagesListPopup("+i+")'>"
							+" 										   </td>"
							+"											<td style='width: 214px;'>"
							+"											   <a class='delete btn btn-danger' onClick='deleteOutwardReturnGoodsRegLrDetails("+ID_NO+","+element.LR_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"//<a class='edit btn btn-dark' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='edit btn btn-blue' onClick='viewOutwardReturnGoodsReg_LR_Details("+element.LR_ID+")' href='javascript:;'><i class='fa fa-external-link'></i></a>
							+"											</td>"
							+"										</tr>";
						i++;
					 });
					
				html+="									</tbody>"
					+"								</table>"
					+"							</div>"
					+"<div id='subPopUp'></div>"
					+"						</div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hidePopUp()' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
			        + "        </div>"
			        + "    </div>"
			        + "</div>";
				 $("#popup").html(html);
				 $("#modal-responsive").addClass("in");
				 $("#modal-responsive").attr("aria-hidden","false");
				 $("#modal-responsive").css("display","block");
			}, 'json');
}
//
function showOutwardLrDocImagesListPopup(index){
	showListOfImagesPopup(commonData.orgrLRArr[index].ATTACH_LR);
}
function showOutwardClaimCopyDocImagesListPopup(index){
	showListOfImagesPopup(commonData.orgrLRArr[index].ATTACH_CLAIM_COPY);
}
/*function viewOutwardReturnGoodsReg_LR_Details(LR_ID){
	//alert("viewOutwardReturnGoodsReg_LR_Details");
	$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/OutwardLR_DetailedViewsAndLrProductList', {
		LR_ID : LR_ID 
		} , function(data) {
			productListArray = data.productsArr;
				var i = 0;
				var html="" 
					+ "<div class='modal fade ' id='modal-responsive777' >"
			        + "    <div class='col-md-13'>"
			        + "        <div class='modal-content'>"
			        + "            <div class='modal-header'>"
			        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive777\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
			        + "                <h4 class='modal-title' id='myModalLabel'><strong>Outward Return Goods Registration LR Details</strong></h4>"
			        + "            </div>"
			        + "  		   <div class='panel panel-default'>"
			        + "			       <div class='panel-body'>"
			        + "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
					+"                                        					<div class='form-group'>"
					+"                                                                <label class='form-label'><strong>LR NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.LR_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>NO OF CASES: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.NO_OF_CASES+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>LR COPY : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                               <label class='form-label'><strong>"+data.ATTACH_LR_PATH+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.REF_CLAIM_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM AMOUNT : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.CLAIM_AMOUNT+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>    "
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CLAIM COPY : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                 <label class='form-label'><strong>"+data.ATTACH_CLAIM_COPY_PATH+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>                                             "            
					+ "						 </div>"
					+ " 			    </div>"
					//credit note details
					+ "    			       <div class='row'>"
			        + "       				   <div class='col-md-4'>"
					+"                                        					<div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CREDIT NOTE NO : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CREDIT_NOTE_NO+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"                                                            
					+"                                                            <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>CREDIT NOTE AMOUNT: - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CREDIT_NOTE_AMOUNT+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>"
					+"                                                        </div>"
					+"                                                        <div class='col-md-4'>"
					+"                                                             <div class='form-group'>"
					+"                                                                <label class='form-label'><strong>ATTACH CREDIT NOTE : - </strong>"
					+"                                                                </label>"
					+"                                                                <span class='tips'></span>"
					+"                                                                <label class='form-label'><strong>"+data.CREDIT_NOTE_IMAGE+"</strong>"
					+"                                                                </label>"
					+"                                                            </div>  "
					+ "						 </div>"
					+ " 			    </div>"
					//
					+"																<div class='row'>"
					+"																	<div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
					+"																		<table class='table table-striped table-hover'>"
					+"																			<thead class='no-bd'>"
					+"																				<tr>"
					+"																					<th style='text-align: center;'><strong>Sr No.</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Product Name</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Claim Quantity</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Received Quantity</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Batch</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>MFG Date</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Expiry Date</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>MFG Company</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Quantity Variance</strong>"
					+"																					</th>"
					+"																					<th style='text-align: center;'><strong>Action</strong>"
					+"																					</th>"
					+"																				</tr>"
					+"																			</thead>"
					+"																			<tbody class='no-bd-y'>";
				
					$(productListArray).each(function(index, element) {
						html+="                                    <tr style='text-align: center;'>"
							+"                                         <td>"+ (i+1) +"</td>"
							+"                                         <td >"+element.PRODUCT_NAME+"</td>"
							+"                                         <td >"+element.CLAIM_QUANTITY+"</td>"
							+"                                         <td>"+element.RECEIVED_QUANTITY+"</td>"
							+"                                         <td>"+element.BATCH+"</td>"
							+"                                         <td>"+element.MFG_DATE+"</td>"
							+"                                         <td>"+element.EXPIRY_DATE+"</td>"
							+"                                         <td>"+element.MFG_COMPANY+"</td>"
							+"                                         <td>"+element.QUANTITY_VARIANCE+"</td>"
							+"											<td style='width: 214px;'>"
							+"												<a class='edit btn btn-blue' onClick='viewProductInfo("+i+")' href='javascript:;'><i class='fa fa-external-link'></i></a></i> <a class='edit btn btn-dark' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a>  <a class='delete btn btn-danger' onClick='deleteOutwardReturnGoodsRegProduct("+LR_ID+","+element.PRODUCT_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
							+"											</td>"
							+"										</tr>";
						i++;
					 });
					html+="																			</tbody>"
					+"																		</table>"
					+"																	</div>"
					+"														<div id='productDetailsPopup'></div>"
					+"																</div>"
					+"             <div class='modal-footer text-center'>"
			        + "                <button type='button' onClick='hideSubPopUp(\"modal-responsive777\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
			        + "            </div>"
					+"															</div>"
					+"														</div>"
					+"													</div>"
					+"												</div>";
					 $("#subPopUp").html(html);
					 $("#modal-responsive777").addClass("in");
					 $("#modal-responsive777").attr("aria-hidden","false");
					 $("#modal-responsive777").css("display","block");
					}, 'json');
}*/
function deleteOutwardReturnGoodsRegLrDetails(inwardReturnGoodsRegId,LR_ID){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/deleteOutwardReturnGoodsRegLrDetails', {
			LR_ID : LR_ID
		}, function(data) {
			alert(data.MSG);
			hidePopUp();
			viewOutwardReturnGoodsRegDetails(true,inwardReturnGoodsRegId);
		}, 'json');
    }
}
//
/*function deleteOutwardReturnGoodsRegProduct(LR_ID,PRODUCT_ID){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OutwardReturnGoodsRegistrationController/deleteOutwardReturnGoodsRegLRProduct', {
			PRODUCT_ID : PRODUCT_ID
		}, function(data) {
			viewOutwardReturnGoodsReg_LR_Details(LR_ID);
			alert(data.MSG);
		}, 'json');
    }
}
*/

function subTabsForAddCrediteNote(index) {
	var html = ""
			+ "                                        <ul id='myTab2' class='nav nav-tabs nav-dark'>"
			+ "                                            <li id='addCreditNoteTabEntry1' class=''><a href='#tab1_1' data-toggle='tab' onclick='addOutwardReturnGoodsRegCreditNote()'><h5><strong>Add Credit Note </strong></h5></a></li>"
			+ "                                            <li id='viewCreditNoteTabEntry2' class=''><a href='#tab1_1_1' data-toggle='tab' onclick='outwardReturnGoodsRegCreditNoteListing()'><h5><strong>View Credit Note</strong></h5></a></li>"
			+ "                                        </ul>"; 	
	$('#subTabsForAddCrediteNote').html(html);
	if (index == 1) {
		$('#addCreditNoteTabEntry1').addClass("active");
	} 
	else if (index == 2) 
	{
		$('#viewCreditNoteTabEntry2').addClass("active");
	}
}


function addOutwardReturnGoodsRegCreditNote(){
	// ,  , 
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>Add Credit Note</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCrediteNote'></div>"
		+"                                    <div id='creditNoteList' class='row'>"
		+"                                        <div class='col-md-12'>"
		+"                                            <div class='panel panel-default'>"
		+"	<form id='addCreditNote' action='"+contextApplicationPath+"/OutwardReturnGoodsRegCreditNoteController/addOutwardReturnGoodsRegCreditNote' method='post' enctype='multipart/form-data'>"												
		+"                                                <div class='panel-body'>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-4'>"
		+"			 <div id='organiztionDropDown' style='height: 84px;' required></div>"
		+"															 <div class='form-group'>"	
		+"                                                                <label class='form-label'><strong>Credit Note No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Credit Note No' name='creditNoteNo' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Credit Note Amount</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Credit Note Amount' name='creditNoteAmount' class='form-control' parsley-type='number' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Ref No/Claim No</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='textarea' placeholder='Ref No/Claim No' name='refNoOrClaimNo' class='form-control' required>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Attach Credit Note</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='file' placeholder='Attach Credit Note' name='attachCreditNote' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                            <div id='attachCreditNoteImageDivId'></div>"
		+"                                                            <div class='form-group'>"
		+"                                                				  <div id='attachCreditNoteImageAddMoreButtonId'><button onclick=\"addFileConrol('attachCreditNoteImageDivId','attachCreditNoteImageAddMoreButtonId','attachCreditNote',1)\" class='btn btn-primary'>Add More Files</button></div>"
		+"                                                            </div>"
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>Remark</strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <div class='controls'>"
		+"                                                                    <input type='text' placeholder='Remark' name='remark' class='form-control'>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                    <div class='row'>"
		+"                                                        <div class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                                            <div class='col-sm-9 col-sm-offset-3'>"
		+"                                                                <div class='pull-right'>"
		+"                                                                    <button class='btn btn-primary m-b-10' onclick='validateFormDetails(\"#addCreditNote\")'>Submit</button>"
		+"                                                                    <button type='reset' class='btn btn-danger m-b-10'>Cancel</button>"
		+"                                                                </div>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                    </div>"
		+"                                                </div>"
		+"												</form>"												
		+"                                            </div>"
		//+"											  <div id='creditNoteList'>"
		//+"											  </div>"			
		+"                                        </div>"
		+"                                    </div>"
		+"                                </div>";
	
	$('#myTabContent').html(html);
	subTabsForAddCrediteNote(1);
	loadOrganiztionDropDown();
	ajaxOutwardReturnGoodsRegCreditNote("addCreditNote");
	
	//outwardReturnGoodsRegCreditNoteListing();
	//$('#loader').hide();
}
function ajaxOutwardReturnGoodsRegCreditNote(formId){
	
	var options = {
			dataType : 'json',
			beforeSubmit: function() {/* $('#loader').show(100);*/},
			success : function(data) {
				$('#loader').hide();
				if(data.result==true){
					$('#'+ formId)[0].reset();
					$('#error').html("");
					$('#loader').hide();
//					addOutwardReturnGoodsRegCreditNote();
					alert(data.MSG);
				}
			
			}
	};
	$('#'+formId).ajaxForm(options);
}


//hello1
function outwardReturnGoodsRegCreditNoteListing(){
	$('#loader').show();
	var i = 0 ;
	//alert("outwardReturnGoodsRegCreditNoteListing");
//	$.post(contextApplicationPath+'/OutwardReturnGoodsRegCreditNoteController/outwardReturnGoodsRegCreditNoteListing', function(data) {
	var html=""
		+"                                <div class='tab-pane fade active in' id='tab1_1_1'>"
		+ "                                    <div class='page-title'> <i class='icon-custom-left'></i>"
		+ "                                        <h3><strong>View Credit Note</strong></h3>"
		+ "                                    </div>"
		+ "<div id='subTabsForAddCrediteNote'></div>"
		+"                                    <div class='row'>"
		+ "                    <div class='col-md-12'>"
		+ "                        <div class='panel panel-default'>"
		+ "                            <div class='panel-body'>"
		+ "                                <div class='row'>"
		+ "                                    <div class='col-md-4'>"
		+ "                                        <div class='form-group'>"
		+ "                                            <label class='form-label'><strong>Search By</strong>"
		+ "                                            </label>"
		+ "                                            <span class='tips'></span>"
		+ "                                            <div class='controls'>"
		+ "                                                     <select id='searchOption' class='form-control' class='form-control' onchange='changeFuncReturnGoodsRegCreditNoteListing();'>"
		+"														  <option value='Organization'>Select Organization</option>"
		+ "                                                       <option value='CreditNoteNo'>Credit Note No</option>"
		+ "                                                       <option value='CreditNoteAmount'>Credit Note Amount</option>"
		+ "                                                       <option value='ClaimNo'>Ref No/Claim No</option>"
		+ "                                                     </select>"
		+ "                                            </div>"
		+ "                                        </div>"
		+ "                                    </div>"//fromDate toDate searchOption searchText
		+ "                                    <div class='col-md-4'>"
		+ "                                        <div class='form-group' id='searchForDiv'>"
		+ "                                            <label class='form-label'><strong>Search For</strong>"
		+ "                                            </label>"
		+ "                                            <span class='tips'></span>"
		+ "                                            <div id='searchControl'>"
		+ "                                                <input id='searchText' type='text' class='form-control' placeholder='Search Text'>"
		+ "                                            </div>"
		+ "                                        </div>"
		+ "                                    </div>"
		+ "                                    <div class='col-sm-9 col-sm-offset-3'>"
		+ "                                        <div class='pull-right'>"
		+ "                                            <button class='btn btn-success m-b-10' onclick='searchoutwardReturnGoodsRegCreditNoteListing()'>Show</button>"
		+ "                                        </div>"
		+ "                                    </div>"		
		+"                                            <div id='searchResult' class='col-md-12 col-sm-12 col-xs-12 table-responsive'>"
		+"                                            </div>"
		+"											<div id='creditNoteInfo'></div>"
		+"                                        </div>"
		+ "                                    <div id='pagination' class='pagination'></div>"
		+"                                    </div>"
		+"                                </div>";
		$('#myTabContent').html(html);
		subTabsForAddCrediteNote(2);
		commonOutwardReturnGoodsRegCreditNoteController("", "", "", "", 1);
	//		$('#creditNoteList').html(html);
//			showoutwardReturnGoodsRegCreditNoteListing(data);
			$('#loader').hide();
//	}, 'json');
}
function commonOutwardReturnGoodsRegCreditNoteController(searchOption, searchText, fromSpecialData, toSpecialData, from)
{
	$.post(contextApplicationPath+'/OutwardReturnGoodsRegCreditNoteController/searchOutwardReturnGoodsRegCreditNote',
			{
		searchOption:searchOption,
		searchText:searchText,
		fromSpecialData:fromSpecialData,
		toSpecialData:toSpecialData,
		from : from
		}, function(data) {
			var url = "commonOutwardReturnGoodsRegCreditNoteController(\""+searchOption+"\",\""+searchText+"\",\""+fromSpecialData+"\",\""+toSpecialData+"\",";
			showoutwardReturnGoodsRegCreditNoteListing(data, from, url) // to call the function which shows the searched item
		//$('#searchResult').html(html);
	}, 'json');
}
function changeFuncReturnGoodsRegCreditNoteListing() {
	var selectedValue=$('#searchOption').val();
	var html="";
		if(selectedValue=="CreditNoteAmount")
		{
    		html+="<input id='inputFromData3' type='text' class='form-control' placeholder='From Amount' style='width: 49%; margin-right: 1%;float: left;'>"
    			+" <input id='inputTOData3' type='text' class='form-control' placeholder='To Amount' style='width: 49%;'>";
		}
       	else
    	{
    		html+="<input id='searchText' type='text' placeholder='Search Text' class='form-control'>";
    	} 	
		$("#searchControl").empty();
		$("#searchControl").html(html);
 }
function searchoutwardReturnGoodsRegCreditNoteListing()
{
	var searchOption=$('#searchOption').val();
	var fromSpecialData=null;
	var toSpecialData=null;
	var searchText=null;
	if(searchOption=="CreditNoteAmount")
	{
		if(isNaN($('#inputFromData3').val()) || isNaN($('#inputTOData3').val()))
		 {
			 alert("it is not a number");
			 return false;
	
		 }
		 else
		 {
			 fromSpecialData=$('#inputFromData3').val();
				toSpecialData=$('#inputTOData3').val();
		 }
		 	
	}
	else
	{
		if(!(Boolean($('#searchText').val())))
		{
			alert("Please Enter the search text");
			return false
		}
		searchText=$('#searchText').val();
	}
	commonOutwardReturnGoodsRegCreditNoteController(searchOption, searchText, fromSpecialData, toSpecialData, 1);
//	$.post(contextApplicationPath+'/OutwardReturnGoodsRegCreditNoteController/searchOutwardReturnGoodsRegCreditNote',
//		{selectedValue:selectedOption,searchText:searchText,fromSpecialData:fromSpecialData,toSpecialData:toSpecialData
//	}, function(data) {
//		showoutwardReturnGoodsRegCreditNoteListing(data) // to call the function which shows the searched item
//	//$('#searchResult').html(html);
//}, 'json');
}

function showoutwardReturnGoodsRegCreditNoteListing(data, from, url)
{
	var i = 0 ;
	SR_NO = (from-1)*NO_Of_RECORDS_PER_PAGE+1;
	CreditNoteListArray = data;
	var html=""
		+"                                                <table class='table table-striped table-hover'>"
		+"                                                    <thead class='no-bd'>"
		+"                                                        <tr>"
		+"                                                            <th style='text-align: center;'><strong>Sr No.</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Organization</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Credit Note No</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Credit Note Amount</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Ref No/Claim No</strong>"
		+"                                                            </th>"
		+"                                                            <th style='text-align: center;'><strong>Action</strong>"
		+"                                                            </th>"
		+"                                                        </tr>"
		+"                                                    </thead>"
		+"                                                    <tbody class='no-bd-y'>";
	$(data).each(function(index, element) {
		if((data.length-2)<i)
			return false;
		html+="                                                       <tr style='text-align: center;'>"
			+"                                                            <td>"+ SR_NO +"</td>"
			+"                                                            <td id='ORG_"+i+"'>"+element.ORG+"</td>"
			+"                                                            <td id='CREDIT_NOTE_NO_"+i+"'>"+element.CREDIT_NOTE_NO+"</td>"
			+"                                                            <td id='CREDIT_NOTE_AMOUNT_"+i+"'>"+element.CREDIT_NOTE_AMOUNT+"</td>"
			+"                                                            <td id='REF_CLAIM_NO_"+i+"'>"+element.REF_CLAIM_NO+"</td>"
			+"  														  <td><a class='btn btn-info m-b-10' onClick='outwardReturnGoodsRegCreditNoteInfo("+i+")' href='javascript:;'><i class='fa fa-external-link'></i></a> <a class='edit btn btn-dark m-b-10' onClick='editOutwardReturnGoodsRegCreditNoteInfo("+i+")' href='javascript:;'><i class='fa fa-pencil-square-o'></i></a> <a class='delete btn btn-danger m-b-10' onClick='deleteOutwardReturnGoodsRegCreditNote("+element.CREDIT_NOTE_ID+")' href='javascript:;'><i class='fa fa-times-circle'></i> </a>"
			+"                                                        </tr>";
		i++;
		SR_NO++;
	}); 
	html+="                                                    </tbody>"
		+"                                                </table>";
	$('#searchResult').html(html);
	 paginationView(from,data[data.length-1].paginationCount,url);//from, totalPages, url
	$(".dateClassCommon").datepicker({changeMonth: true,changeYear: true,yearRange: "-100:+0" });
}

function deleteOutwardReturnGoodsRegCreditNote(ID){
	var r = confirm("Do you want to Delete Entry...!");
    if (r == true) {
		$.post(contextApplicationPath+'/OutwardReturnGoodsRegCreditNoteController/deleteOutwardReturnGoodsRegCreditNote', {
			ID : ID
		}, function(data) {
			outwardReturnGoodsRegCreditNoteListing();
			alert(data.MSG);
		}, 'json');
    }
}
function outwardReturnGoodsRegCreditNoteInfo(index){
	var html=""
		+ "<div class='modal fade ' id='modal-responsive123' >"
        + "    <div class='col-md-13'>"
        + "        <div class='modal-content'>"
        + "            <div class='modal-header'>"
        + "                <input value='x' onClick='hideSubPopUp(\"modal-responsive123\")' type='button' class='close' data-dismiss='modal' aria-hidden='true'>"
        + "                <h4 class='modal-title' id='myModalLabel'><strong>Company Product Details</strong></h4>"
        + "            </div>"
        + "<div class='panel-body'>"
        + "    											<div class='row'>"
        + "       												<div class='col-md-4'>"
		+"                                        					<div class='form-group'>"
		+"                                                                <label class='form-label'><strong>ORGANIZATION : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <label class='form-label'><strong>"+CreditNoteListArray[index].ORG+"</strong>"
		+"                                                                </label>"
		+"                                                            </div>"                                                            
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>CREDIT NOTE AMOUNT : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <label class='form-label'><strong>"+CreditNoteListArray[index].CREDIT_NOTE_AMOUNT+"</strong>"
		+"                                                                </label>"
		+"                                                            </div>"//  
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>REMARK : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <label class='form-label'><strong>"+CreditNoteListArray[index].REMARK+"</strong>"
		+"                                                                </label>"
		+"                                                            </div>"
		+"                                                        </div>"
		+"                                                        <div class='col-md-4'>"
		+"                                                             <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>CREDIT NOTE NO : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                <label class='form-label'><strong>"+CreditNoteListArray[index].CREDIT_NOTE_NO+"</strong>"
		+"                                                                </label>"
		+"                                                            </div>  "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>REF NO/CLAIM NO : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                 <label class='form-label'><strong>"+CreditNoteListArray[index].REF_CLAIM_NO+"</strong>"
		+"                                                                </label>"
		+"                                                            </div>    "
		+"                                                            <div class='form-group'>"
		+"                                                                <label class='form-label'><strong>ATTACH CREDIT NOTE : - </strong>"
		+"                                                                </label>"
		+"                                                                <span class='tips'></span>"
		+"                                                                 <img onClick='showCreditNoteAttachedImagesListPopup("+index+")' src='../resources/images/fileIcon.png' alt='Smiley face' height='100' width='100'>"//<img onClick='showImagePopup(\""+CreditNoteListArray[index].ATTACH+"\")' src='"+CreditNoteListArray[index].ATTACH+"' alt='Smiley face' height='100' width='100'>"
		+"                                                                </label>"
		+"                                                            </div>                                             "            
		+ "														  </div>"
		+ "						 							</div>"
		+ "											</div>"
		+"            <div class='modal-footer text-center'>"
        + "                <button type='button' onClick='hideSubPopUp(\"modal-responsive123\")' class='btn btn-danger' data-dismiss='modal'>Cancel</button>"
        + "            </div>"
        + "        </div>"
        + "    </div>";
	$('#creditNoteInfo').html(html);
	$("#modal-responsive123").addClass("in");
	$("#modal-responsive123").attr("aria-hidden","false");
	$("#modal-responsive123").css("display","block"); 
}
//
function showCreditNoteAttachedImagesListPopup(index){
	showListOfImagesPopup(CreditNoteListArray[index].ATTACH);
}